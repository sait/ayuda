+++
title = "Grupos de Usuarios / Catálogo de Niveles"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=i1KRNMFLy_s&index=2&list=PLhfBtfV09Ai6MVckTTAICCQ0E1u1n3N7_" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="7. B Grupos de usuarios-catalogos de niveles.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Uno de los primeros y principales pasos al estar en el proceso de implementación y configuración de SAIT, está relacionado básicamente con la definición de Grupos de Usuarios / Catalogo de Niveles.

Un grupo o nivel se puede definir como un perfil de usuario(s) los cuales se les puede otorgar ciertos permisos o actividades a desarrollar dentro del sistema, esto en función al papel que desempeñan dentro de la empresa.

Al instalar SAIT Software Administrativo de manera automática se crean grupos de usuarios o niveles, los cuales se mencionan a continuación:

#### NOTA: Dependiendo de la versión de SAIT la clave puede ser 1,2,3, siendo 1, el nivel más alto es decir Supervisor General.


![IMG](1.PNG)

<h2 class="text-primary">MENÚS</h2>

* [Ventas](#ventas)
* [Ventas2](#ventas2)
* [Caja](#caja)
* [Inventario](#inventario)
* [Inventario2](#inventario2)
* [CXC](#cxc)
* [Compras](#compras)
* [Cxp](#cxp)
* [Bancos](#bancos)
* [Utilerías](#utilerias)


## VENTAS


![IMG](2.PNG)

Permiso | Explicación de Permiso 
---|---
Menú de ventas  |	Permite contar con acceso a el modulo de ventas.
Datos generales de clientes |	Permite contar con acceso a los datos generales del catalogo de clientes (Dirección, RFC, Contacto en ventas, etc). Nos permite grabar, modificar y eliminar clientes. Esta opción se localiza en Ventas / Clientes.
Datos de crédito de clientes |	Permite contar con acceso a los datos de crédito del catalogo de clientes (Días de Crédito, Limite de Crédito, Lista de precios a usar, etc.), para poder habilitar este acceso se debe tener habilitado la opción anterior DATOS GENERALES DE CLIENTES. Esta opción se localiza en Ventas / Clientes.
Reporte de clientes |	Permite tener acceso a los reportes de clientes, en los cuales se cuenta con las opciones de Catalogo, Acumulados, Etiquetas. Esta opción se localiza en Ventas / Reportes de Clientes.
Vendedores |	Permite tener acceso al catalogo de vendedores, podrá agregar, eliminar y modificar vendedores. Esta opción se localiza en Ventas / Vendedores.
Definir promociones |	Permite tener acceso a definir promociones, podrá agregar, eliminar y modificar promociones. Esta opción se localiza en Ventas / Descuentos y Promociones.
Registro de ventas |	Permite tener acceso a registrar documentos de ventas (Facturas, Notas de Venta, Pedidos, Cotizaciones, Remisiones, Prefactura). Esta opción se localiza en Ventas / Registro de ventas o puede accesar a ella presionando F4 en su teclado.
Registrar devoluciones |	Permite tener acceso a registrar devoluciones y notas de crédito electrónicas. Esta opción se localiza en Ventas / Devoluciones y notas de crédito.
Registrar devoluciones de facturas pagadas |	Permite tener acceso a registrar devoluciones y notas de crédito electrónicas de facturas ya pagadas. Esta opción se localiza en Ventas / Devoluciones y notas de crédito.
Recibir pedidos por palm |	Permite recibir los pedidos de los clientes desde la palm. Esta opción se localiza en Ventas / Recibir pedidos por palm.
Consulta general de documentos |	Permite realizar la consulta de varios documentos ya sea por rango de fechas, rango de folios, cliente, sucursal o vendedor. Esta opción se localiza en Ventas / Consultas Generales.
Ver totales |	Permite ver los totales en la consulta general de documento. Esta opción se localiza en Ventas / Consultas Generales.
Requiere autorización para cancelar documentos o hacer devoluciones |	Al momento de realizar cancelaciones o devoluciones al usuario le pedirá una clave de autorización.
Consulta individual de documentos |	Permite realizar la consulta de un documento de venta. Esta opción se localiza en Ventas / Consulta Individual.
Reportes de ventas |	Permite tener acceso a los reportes que se encuentran en Ventas / Reportes de Ventas, y a los de Ventas / Reportes de Documentos.
Actualizar tipo de cambio |	Permite actualizar el tipo de cambio para las ventas. Esta opción se encuentra en Ventas / Tipo de cambio.
Actualizar siguientes folios |	Permite actualizar los folios de los documentos y catálogos del sistema. Esta opción se encuentra en Ventas / Siguientes Folios.
Cancelar documentos |	Permite poder cancelar los documentos de ventas.
Reimprimir documentos |	Permite poder reimprimir los documentos de venta.
Modificar folios |	Permite modificar el folio a los documentos de venta ya elaborados, también permite modificar el vendedor.
Modificar cotizaciones y pedidos |	Permite realizar modificaciones a las cotizaciones y pedidos.
Facturar prefacturas |	Permite convertir en facturas las prefacturas.
Gráfica de ventas |	Permite tener acceso a la gráfica de ventas. Esta opción se localiza en Ventas / Gráfica de Ventas.  


## VENTAS2

![IMG](3.PNG)

Permiso | Explicación de Permiso 
---|---
Permitir vender aun y cuando no haya existencia |		Permite vender y realizar salidas de artículos sin tener existencia. Si no esta habilitada la opción no se podrá vender o realizar salidas sin existencias.
Bloquear búsqueda de series |		Bloque la búsqueda de series al vender, para obligar al usuario a capturar los números de series.
Saltar al siguiente renglón automáticamente |		Habilita la opción que permite brincar el curso después de capturar el articulo. Esta opción es muy útil cuando se realizan ventas con código de barras, muy practica para el giro de abarrotes y autoservicios.
Iniciar documento en cuadricula de artículos |		Habilita la opción para que al abrir la ventana de ventas el cursor se posicione en la cuadricula de artículos.
Mostrar columna de precio neto |		Muestra la columna de precio neto por cada una de las partidas del documento, esto es tomando en cuenta el descuento y el impuesto.
Mostrar columna de % de impuesto |		Muestra la columna de impuesto partidas del documento, esta opción permite modificar el impuesto del articulo.
Mostrar capturar pago, antes de procesar notas y facturas |		Habilita la opción para que antes de procesar la nota o factura se capture la forma de pago, esta opción de habilita cuando en la empresa se utiliza el modulo de caja.
Mostrar permitir duplicar clientes con mismo RFC |		Permite agregar aun y cuando ya exista a otro cliente con ese mismo RFC.
Modificar sucursal 	|	Permite modificar la sucursal al realizar un documento de venta.
Modificar folio |		Permite modificar el folio al realizar un documento de venta.
Modificar fecha |		Permite modificar la fecha al realizar un documento de venta.
Modificar tipo de cambio |		Permite modificar el tipo de cambio al realizar un documento de venta.
Modificar vendedor |	 	Permite modificar el vendedor al realizar un documento de venta.
Modificar % de descuento |		Permite capturar descuento al realizar un documento de venta.
Modificar precios |		Permite modificar los precio al realizar un documento de venta.
Capturar facturas |		Permite realizar facturas.
Capturar remisiones 	|	Permite realizar remisiones.
Capturar pedidos |		Permite realizar pedidos.
Capturar cotizaciones |		Permite realizar cotizaciones.
Capturar prefacturas |		Permite realizar prefacturas.
Capturar notas de venta |		Permite realizar notas de venta.
Documento de venta forma de pago crédito |		Permite realizar documentos a crédito
Documento de venta forma de pago contado |		permite realizar documentos de contado
Documento de venta divisa pesos |		Permite realizar documentos en pesos
Documento de venta divisa dolares |		Permite realizar documentos en dolares
Devoluciones en crédito |		Permite realizar devoluciones a crédito
Devoluciones en efectivo |		Permite realizar devoluciones en efectivo 


## CAJA

![IMG](4.PNG)

Permiso | Explicación de Permiso 
---|---
Menú de caja |	Permite tener acceso a el modulo de caja
Abrir corte |	Permite abrir corte
Cerrar corte 	|Permite cerrar corte
Registrar ventas |	Permite tener acceso a registrar ventas en el modulo de caja
Manejar apartados en dolares |	Permite realizar apartados en dolares (ya no disponible para su uso)
Manejar notas a crédito en dolares  |	Permite manejar notas de crédito en dolares (ya no disponible para su uso)
Permitir devolución de efectivo |	Permite realizar devoluciones de efectivo
Permitir devolución generando n. de crédito |	permite realizar devoluciones generando nota de crédito
Permitir eliminar partidas capturadas  |	Permite eliminar partidas y modificar partidas
Obligar escanear tarjeta del cliente |	Obliga a escanear la tarjeta del cliente para realizar ventas
Recibir pagos |	Habilita la opción de Caja / Recibir pagos (contado) y Caja / Recibir pagos (crédito)
Otras entradas / salidas de efectivo |	Permite realizar otras entradas y salidas de efectivo en Caja / Otras salidas de efectivo y Caja / Otras entradas de Efectivo
Cancelar movimientos |	Permite cancelar los movimientos de caja (Nota, Factura, Apartado, Pago, Salida y entrada de efectivo), esto es en el menú Caja / Cancelar Movimientos
Entregar corte de caja | 	Permite realizar la entrega del el corte de caja
Consultar movimientos |	Permite realizar la consulta individual y general de movimientos en caja.
Consultar cortes de caja |	Permite realizar la consulta consulta general e individual de cortes en caja.
Modificar corte de caja |	Permite que al consultar el corte de caja se pueda modificar, solo se puede modificar en efectivo entregado, es decir no se pueden realizar modificaciones en las ventas del corte. Esto es solo para corregir errores al contar el efectivo encaja.
Definir tipos de movimientos |	Permite definir tipos de movimientos en caja
Obligar abrir corte para hacer facturas |	Obliga al usuario abrir corte para realizar ventas 

## INVENTARIO

![IMG](5.PNG)

Permiso | Explicación de Permiso 
---|---
Menú De Inventarios |	Permite definir acceso a el modulo de Inventarios en el menú principal de SAIT.
Catalogo De Artículos |	Permite al usuario ingresar al catálogo de artículos en el cual puedes dar de alta, modificar, eliminar, consular la información de un artículo.
Unidades 	| Permite el acceso a realizar diferentes conversiones de unidad de medida, las cuales se utilizan para las ventas, de tal manera, que también le puedes ingresar el valor monetario que se le otorgara a la nueva unidad de medida de determinado artículo.
Reporte De Artículos |	Permite el acceso a consultas de reportes sobre los artículos, además, permite el desarrollo de reportes y otorgar los permisos de consultas a determinado usuario.
Múltiples Códigos |	Permite al usuario ingresar a una clave principal de un artículo, distintos códigos de barras, los cuales, facilitan la identificación de un artículo .
Actualizar Precios |	Otorga el premiso a modificación de precios (precios de lista, por volumen, por zona, por cliente y precios por sucursal de clientes).
Actualizar Costos |	Permite el acceso a modificación de costos de reposición y máximo costo del artículo.
Modif. Masiva De Precios |	Permite realizar la actualización masiva de precios y costos.
Actualizar Rápida |	Permite al usuario el acceso a la ventana de actualización rápida, en la cual, permite la modificación de la información de los artículos de manera rápida.
Artículos Sustitutos |	Permite el acceso a registrarle a un articulo principal, una aserie de artículos de menor calidad llamados artículos sustitutos.
Calcular Máximos Y Mínimos |	Permite el acceso a calcular máximos y mínimos según las ventas.
Consulta De Números De Series |	Te proporciona acceso a realizar consultas de series que se tienen registrados en SAIT.
Modificar Números De Series |	Permite acceso al usuario para la modificación de series ingresadas a un determinado articulo en SAIT.
Tipos De Movimientos Al Inventario |	Permite acceso a generar los diferentes tipos de movimientos en el inventario (Salida por merma, Entrada por ajuste a inventario, Realizar traspasos de mercancía, etc.).
Ajuste Rápido De Existencias |	Permite al usuario realizar un ajuste de existencias en el inventario a un artículo de manera rápida.
Kardex De Articulo |	Otorga el acceso a mirar todo el movimiento que se ha generado con un determinado articulo en inventario.
Consultas de Mov. Al Inventario |	Permite acceso a Consultar los diferentes tipos de documentos de movimiento al inventario, así, como la cancelación del documento.
Verificar Existencias |	Permite el acceso a Verificar que tus existencias sean las correctas, según los movimientos en el inventario.
Corregir Existencias |	Permite realizar la corrección de las existencias en tu inventario, cuando no concuerdan los movimientos con la existencia.
Reportes de Mov. Al inventario 	|
Reportes de Mov. Con saldo 	|
Mostrar Costos |	Permite mostrar y modificar los costos del articulo en los movimientos al inventario.

Permiso | Explicación de Permiso
---|--- 
Registro de Entradas y Salidas |
Movimientos Al Inventario |	Permite el acceso al registro de documentos de entradas y salidas al inventario(Entrada por ajuste, salida por fabricación, salida por merma, etc.).
Capturar Referencia |	Habilita el Campo de captura referencia al registrar un documento por entrada o salida al inventario.
Capturar Pedimento De Importación |	Habilita el Campo para captura numero de pedimento al registrar un documento por entrada o salida al inventario.
Detener Si Realiza Salidas Sin Existencias |	Habilita la opción de no registrar una salida del inventario, si el artículo no cuenta con existencia en el inventario.
Permitir Modificar Folios |	Permite modificar el folio del movimiento a realizar en el inventario. 

## INVENTARIO2

![IMG](6.PNG)

Permiso | Explicación de Permiso 
---|---
Clasificación De Artículos | 	Permite el acceso al usuario para dar de alta las diferentes clasificaciones que se manejaran en el catálogo de artículos.
Líneas De Artículos |	Habilita el acceso para ingresar el catalogo de Líneas que clasificarán determinados artículos
Familias De Artículos |	Habilita el acceso para ingresar el catalogo de Familias que clasificaran determinados artículos
Categorías De Artículos |	Habilita el acceso al usuario para ingresar el catalogo de Categorías que clasificaran determinados artículos
Departamento De Artículos |	Habilita el acceso para ingresar el catalogo de Departamentos que clasificaran determinados artículos.

Permiso | Explicación de Permiso
---|--- 
Proceso Toma de Inventario |
Proceso De Inventario |	Permite el acceso para el proceso del ingreso del inventario final o inicial (Documentos toma de inventario, Reportes, Ajustar existencias, Mandar los artículos y programas a lectoras portátil).
Agregar Documentos De Toma De Inventario |	Permite el acceso para registrar en un documento todo el conteo de las existencias que se genere de un inventario, lo cual al tener ingresado este documento se realiza un ajuste de existencias en base al conteo y automáticamente ingresa el conteo a SAIT.
Modificar Documentos De Toma De Inventario |	Permite el acceso para modificar determinado documento de un inventario ya procesado.
Borrar Documentos De Toma De Inventario |	Permite el acceso para Eliminar determinado documento de un inventario ya procesado.
Reportes De Toma De Inventario |	Permite el acceso a consultar reportes de procesos de toma de inventario, en el cual, puedes dar de alta más reportes o delimitar que usuarios pueden consultar.
Ajustar Existencias Físicas |	Permite ajustar tu inventario en base a los documentos ingresados de toma de procesos o poner tu inventario en ceros.

Permiso | Explicación de Permiso
---|--- 
Sucursales y Almacenes |
Consultar Existencia En Sucursales |	Permite el acceso a realizar consultas de existencias de un artículo en las sucursales/Almacenes.
Consultar Trasferencias Entre Sucursales |	Permite el acceso a realizar consultas de Transferencias entre las sucursales, en la cual, puedes aceptar el trapazo, consultarlo y eliminarlo.
Definir Máximos Y Mínimos |	Permite definir Máximos y mínimos de un artículo por sucursal, lo cual lo encuentras consultar existencias en sucursal/ almacenes.

Permiso | Explicación de Permiso
---|--- 
Catálogo de Articulos |
Grabar Artículos |	Permite el acceso para poder grabar o modificar un artículo.
Editar Costos |	Permite el acceso a modificar el último costo y el costo de reposición directamente en el catalogo de Artículos.
Inicializar Campos |	Habilita la opción de que al entrar al catálogo de artículos inicialice todos los campos en blanco.
Editar Ultimas Compras |	Permite al usuario modificar el proveedor y el último costo en el catálogo de artículo.
Borrar Movimientos Del Inventario |	Permite al usuario eliminar los movimientos en el kardex del artículo o modificar la capa del movimiento. 

## CXC

![IMG](7.PNG)

Permiso | Explicación de Permiso 
---|---
Menú de Cxc |	Permite acceder a las ventanas del menú de Cobranza
Capturar fecha en movimientos |	Permite capturar la fecha al registrar abonos a los clientes.
Cargos a cuentas por cobrar |	Permite acceder a la ventana para registrar cargos a los clientes.
Abonos a cuentas por cobrar 	|Permite acceder a la ventana para registrar abonos a los clientes. Esta ventana registra un abono a un solo documento.
Abonos múltiples |	Permite acceder a las ventanas de "Registrar Pagos de los Clientes" y "Captura Masiva de Pagos". Desde estas 2 ventanas se pueden registrar pagos a varias facturas.
Cancelar anticipos y notas de crédito |	Permite acceder a la ventana para cancelar anticipos y notas de crédito con saldo.
Actualización de vencimientos |	Permite acceder a la ventana para actualizar la fecha de vencimiento de las cuentas por cobrar. La fecha de vencimiento se graba al procesar facturas de crédito de acuerdo a la fecha de la factura y los días de crédito del cliente. La ventana de "Actualizar Vencimientos" permite modificar el vencimiento una vez que haya procesado la factura.
Estado de cuenta global |	Permite consultar el estado de cuenta de un cliente. En esta ventana se muestran todos los cargos y abonos registrados a un cliente, así como el saldo actual. La consulta puede enviarse a Excel.
Borrar movimientos de cxc |	Habilita el botón para eliminar movimientos al consultar el estado de cuenta del cliente.
Reporte de cuentas por cobrar |	Permite acceder a los reportes de cobranza.
Conceptos de cuentas por cobrar |	Permite acceder a la ventana para definir los diferentes conceptos de cargo y abono en cobranza.
Verificar saldos de clientes |	Permite acceder a la ventana para detectar inconsistencias en el estado de cuenta de los clientes. En caso de detectar alguna, permite corregirlas.
Caja-Recepción de pagos |	Permite acceder a la ventana para registrar hasta 4 distintas formas de pagos a un mismo documento. (ya no disponible para su uso).
Corte de caja |	Permite registrar e imprimir el corte de caja de los abonos realizados en cobranza. Este corte de caja es independiente del que se realiza en el menú de Caja, ya que involucra únicamente abonos realizados en cobranza. El corte se realiza por usuario que registro los movimientos y fecha en que fueron registrados.
Modificar corte de caja  |	Permite modificar el dinero entregado al registrar un corte de caja. También permite re-imprimir un corte. 

## COMPRAS

![IMG](8.PNG)

Permiso | Explicación de Permiso 
---|---
Menu de compras |	Permite al usuario contar con el acceso a visualizar la pestaña de COMPRAS dentro del menú principal de SAIT.
Catálogo de proveedores |	Permite al usuario contar con el acceso al Catálogo de Proveedores, el cual permite Agregar a un Nuevo Proveedor, Modificar o Eliminar un proveedor ya existente.
Reportes de proveedores |	Permite el acceso a una serie de reportes, relacionados con el catálogo de proveedores al usuario.
Registrar documentos de compras |	Permite al usuario registrar/generar facturas de compras.
Registrar devolución de compras |	Permite al usuario capturar y/o generar devoluciones a compras que se encuentran actualmente almacenadas en el modulo de compras.
Cancelar documentos |	Permite al usuario realizar cancelaciones de documentos de compras. (Compras u Ordenes de Compras)
Reimprimir documentos |	Permite al usuario contar con la facultad de reimprimir o mandar por correo electrónico los documentos de compras.
Consultas generales |	Permite al usuario contar con el acceso necesario para consultar de manera general documentos de compras.
Consulta individual de compras |	Permite al usuario contar con el acceso necesario para consultar de manera individual un documento de compra.
Reportes de compras |	Permite al usuario contar con el acceso para consultar y generar nuevos reportes de compras.
Impresión de etiquetas |	Permite al usuario contar con el acceso necesario para generar la impresión de etiquetas para los artículos.
Ordenes de compra |	Permite al usuario contar con el acceso para capturar ordenes de compras o pedidos al proveedor.
Facturas de compra |	Permite al usuario contar con el acceso para capturar Facturas de Compras al proveedor.
Precio de venta |	Permite al usuario contar con el acceso necesario para permitir capturar el PRECIO DE VENTA al momento de estar registrando la compra. Esta opción solo está disponible cuando NO SE MANEJA el servicio de enlace de sucursales.
Compras de otra sucursal |	Permite al usuario capturar compras remotas pertenecientes de otra sucursal, con el objetivo de afectar la sucursal en cuestión.
Ocligar uso de código de barras |	Al habilitar la opción, obliga al usuario que al momento de estar registrando algún documento de compra, se utilice el lector óptico para capturar los artículos a incluir en el documento.
No permitir modificar costo |	Limita al usuario poder modificar o actualizar el costo unitario del artículo al momento de estar capturando algún documento de compra.
No repetir números de serie |	Validación disponible de que al momento de estar capturando una factura de compra, se valida que no exista previamente la seria a agregar en el sistema.
Bloquear cantidad al generar pedidos |	Validación disponible que al momento de generar un pedido automático desde la ventana de Registro de Compras, se indica la cantidad a solicitar en el pedido, no permita al usuario modificar la cantidad a solicitar.
Subtotal |	Permite al usuario capturar el campo de SUBTOTAL, al momento de estar registrando algún documento de compra.
Descuentos |	Permite al usuario capturar el campo de DESCUENTOS, al momento de estar capturando algún documento de compra.
Impuestos |	Permite al usuario capturar el campo de IMPUESTOS, al momento de capturando algún documento de compra.
Permitir diferencias |	Permite al usuario registrar los totales por documento de compra y permite el almacenamiento del documento aunque exista diferencia entre el total de partidas capturadas, en contra de los totales capturados por el cliente. 

## CXP

![IMG](9.PNG)

Permiso | Explicación de Permiso 
---|---
Menú de cuentas por pagar |	Permite acceder a las ventanas del menú de Cuentas por Pagar.
Abonos de CXP |	Permite acceder a la ventana para registrar abonos a los proveedores. Esta ventana registra un abono a un solo documento.
Cargos de CXP |	Permite acceder a la ventana para registrar cargos a los proveedores.
Conceptos de CXP |	Permite acceder a la ventana para definir los diferentes conceptos de cargo y abono en cuentas por pagar.
Estado de cuenta |	Permite consultar el estado de cuenta de un proveedor. En esta ventana se muestran todos los cargos y abonos registrados a un proveedoir, así como el saldo actual. La consulta puede enviarse a Excel.
Borrar movimientos del estado de cuenta |	Habilita el botón para eliminar movimientos al consultar el estado de cuenta de un proveedor.
Reporte de CXP  |	Permite acceder a los reportes de cuentas por pagar.
Actualizar vencimientos |	Permite acceder a la ventana para actualizar la fecha de vencimiento de las cuentas por pagar. La fecha de vencimiento se graba al procesar compras o gastos de acuerdo a la fecha de la factura y los días de crédito que otorga el proveedor. La ventana de "Actualizar Vencimientos" permite modificar el vencimiento una vez que haya procesado el movimiento.
Elaborar cheque |	Permite acceder a la ventana para elaborar cheques a proveedores. Desde esta opción se afecta a 3 diferentes módulos: Cuentas por Pagar, Bancos y Contabilidad
Pagar Compras |	Permite acceder a las ventanas de "Registrar Pagos" la cual permite registrar pagos a varias facturas.
Verificar saldos de proveedores |	Permite acceder a la ventana para detectar inconsistencias en el estado de cuenta de los proveedores. En caso de detectar alguna, permite corregirlas.

Permiso | Explicación de Permiso
---|--- 
Menú de Gastos |
Agregar gastos |	Permite acceder a la ventanas agregar facturas de gastos.
Eliminar gastos |	Habilita el botón para borrar gastos dentro de la "Consulta General de Gastos".
Consultas gastos |	Permite acceder a la ventana para realizar una consulta general de gastos por proveedor y rango de fechas.
Reporte de gastos |	Permite acceder a la ventana para la consulta de reportes de gastos.
Definir tipos de gastos |	Permite acceder a la ventana para definir los diferentes gastos de la empresa. Una vez que se hayan definido, se podrán capturar en la ventana de "Agregar gastos". 


## BANCOS

![IMG](10.PNG)

Permiso | Explicación de Permiso 
---|---
Beneficiaros | 	Permite tener acceso al catalogo de Beneficiarios, con el objetivo de permitir Agregar un nuevo beneficiario, Modificar o Eliminar un beneficiario ya existente.
Cuentas bancarias | 	Permite al usuario contar con el acceso al Catalogo de Cuentas Bancarias, el cual permite Agregar una Nueva Cuenta Bancaria, Modificar o Eliminar una ya existente.
Definir cheques frecuentes | 	Permite al usuario contar con el acceso para generar/elaborar cheques frecuentes de operaciones repetitivas para la empresa, ejemplo de estos pueden ser: Cheque de Pago de Nomina, Pago de luz, Pago de Agua, Pago de Teléfono, entre otros.
Conceptos bancarios | 	Permite al usuario accesar al catalogo de Conceptos Bancarios, con el objetivo de Agregar un concepto nuevo, Modificar o Eliminar alguno ya existente. Ejemplo de estos pueden ser: Transferencia, Depósito, Retiro, Etc. Cabe de Mencionar que esta opción aparecerá en SAIT siempre y cuando no se maneje el enlace de Bancos y Contabilidad.
Agregar cheques | 	Permite al usuario generar o elaborar cheques en el modulo de bancos.
Modificar cheques | 	Permite al usuario modificar cheques que ya se encuentran almacenados en el modulo de Bancos.
Cancelar cheques | 	Permite al usuario cancelar cheques que ya se encuentran almacenados en el modulo de Bancos, con el objetivo que se guarde la referencia de cancelación.
Borrar cheques |  	Permite al usuario borrar cheques que ya se encuentran almacenados en el modulo de Bancos.
Registrar depositos | 	Permite al usuario la facultad para poder registrar depósitos dentro del modulo de Bancos. Esta opción solo aparecerá cuando no hay enlace entre el modulo de Bancos y Contabilidad.
Registrar otros movimientos | 	Permite al usuario registrar otros movimientos de conceptos bancarios definidos po el usuario. Esta opción solo aparecerá cuando el sistema detecte que no hay enlace entre el modulo de Bancos y Contabilidad, o bien no se cuente con ningún sistema contable enlazado.
Estado de cuenta | 	Permite al usuario contar con el acceso al estado de cuenta de cuentas bancarias.
Consultas movs | 	Permite al usuario contar con el acceso a realizar consulta de movimientos bancarios, por cada una de las cuentas definidas.
Grafica de saldo | 	Facultad que permite al usuario contar con el acceso a generar, mostrar e imprimir la grafica de saldos actual de las cuentas bancarias.
Conciliación bancaria | 	Permiso que está relacionado para permitir al usuario realizar le proceso de conciliación bancaria entre cada una de sus cuentas bancarias. Permitiendo las siguientes actividades: Conciliar Depósitos, Conciliar Cheques, Conciliar Retiros, Modificar Fechas de Cobros e Imprimir la conciliación.
Menu contabilidad | 	Permite al usuario contar con el acceso a visualizar la pestaña de CONTABILIDAD dentro del menú principal de SAIT.
Cuentas contables | 	Permite al usuario contar con el acceso al catalogo de Cuentas Contables, el cual permite Agregar una nueva Cuenta, Modificar o Eliminar una cuenta contable ya existente.
Tipos de póliza | 	Permite al usuario definir las distintas pólizas a generar en el modulo de contabilidad.
Agregar pólizas | 	Permite al usuario registrar o agregar pólizas contables.
Modificar pólizas | 	Permite al usuario modificar pólizas contables que actualmente se encuentran almacenadas.
Borrar pólizas |	Permite al usuario Borrar/Eliminar pólizas contables.
Generar póliza de operaciones | 	Permite al usuario generara de manera automática la(s) pólizas de operaciones. En base a la definición previa establecida en la configuración de integración contable. 

## UTILERIAS

![IMG](11.PNG)

Permiso | Explicación de Permiso 
---|---
Barra de Herramientas | 	Permite contar con acceso a utilerías tales como: Cambiar Fondo o Imagen del Sistema, Mostrar Mosaico como imagen, Habilitar o deshabilitar la barra de herramientas principal de SAIT y Ocultar/Mostrar la barra de estado inferior.
Catálogo de usuarios del sistema 	|  Permite tener acceso al catalogo de usuarios SAIT, con la funcionalidad de Agregar, Modificar y Eliminar Usuarios ya existentes.
Catálogo de grupos de usuarios | 	Permite tener acceso al catalogo de Grupos o Niveles, con el objetivo de permitir Agregar un nuevo nivel, Modificar los Permisos de un nivel o Eliminar un nivel ya existente.
Catálogo de sucursales/cambiarse de almacen 	| Permite tener acceso al catalogo de Sucursales y realizar actividades de: Agregar una nueva sucursal/almacén, Modificar los datos de una Sucursal/Almacén o Eliminar una Sucursal/Almacén ya existente. También con la funcionalidad de permitir cambiarse de almacén en cualquier momento.
Definir períodos de trabajo |	Permite al usuario definir un rango de fechas, en los que los usuarios podrán generar movimientos. Los módulos operativos a afectar son: Documentos de Ventas, Documentos de Compras, Movimientos a Cobranza, Movimientos a Inventario y Cheques/Pólizas.
Recalcular Estadistícas anuales |	Permite al usuario contar con el acceso al procedimiento de cargar nuevamente las estadísticas anuales de los catálogos de: Clientes, Artículos y Proveedores, tomando en cuenta los movimientos registrados en el año indicado por el usuario.
Actualizar TC oficial |	Permite al usuario actualizar el TC oficial según la página de la SHCP. Con la funcionalidad de contar con un enlace directo para consultar el dato del TC Oficial en la página de internet.
Calcular claves de autorización |	Permite al usuario tener acceso a calcular una clave de autorización para la cancelación de documentos de ventas, tales como son: Notas de Ventas, Remisiones, Cotizaciones, Facturas y Notas de Crédito.
Ejecutar módulos especiales |	Permite al usuario tener acceso a ejecutar módulos o ventanas especiales desarrolladas por el departamento de desarrollo de SAIT Software Administrativo, el cual son ventanas adecuadas a los requerimientos de los clientes.
Importar información de otros sistemas |	Permite al usuario poder integrar a SAIT información proveniente de otros sistemas, ejemplos de estos son: Wincont, Contpaq, Compunegocio y desde Excel.
Configuración del sistema |	Permite al usuario contar con el acceso a modificar y re-definir las opciones de configuración general del sistema, la cual está separada por cada uno de los módulos del sistema. REGULARMENTE solo el nivel de SUPERVISOR GENERAL DEL SISTEMA deberá tener acceso a dicha opción mencionada.
Modificar formato de documentos |	Permite al usuario contar con el acceso a la configuración general de reportes, de los documentos más comunes, entre ellos encontramos: Facturas, Remisiones, Cotizaciones, Devoluciones, Compras, Orden de Compra, Cortes de Caja. Además de modificar los formatos ya antes mencionados y adecuarlos al formato deseado por el usuario.
Respaldar información |	Permite al usuario generar respaldos de manera manual de la información de la empresa. Al generar el respaldo, se generara un archivo con terminación .Zip.
Recuperar información |	Permite al usuario contar con acceso a recuperar la información de cierto respaldo generado a cierta fecha, con el objetivo de restaurarlo hasta la fecha de generación del mismo.
Generar indíces |	Permite al usuario contar con el acceso a ejecutar el proceso de generación de índices. La generación de índices consiste en la reconstrucción de todos los registros contenidos en las bases de datos del sistema.
Grabar consultas al modificarse |	Permite al usuario que al modificar una consulta con ciertos campos definidos por el usuario, cuando se vuelva a generar dicha consulta, se conserven los campos definidos.
Barra de diseño |	Permite al usuario contar la barra de diseño de consultas, con el objetivo de construir y generar consultas personalizadas que el mismo usuario puede definir.
Actualizar bases de datos |	Permite al usuario actualizar las bases de datos, en función a la versión que actualmente tiene instalada de SAIT.
Depurar información |	Permite al usuario contar con el acceso a depurar/eliminar información de la empresa, con el objetivo de aligerar la carga de información y dejar solo información hasta una fecha en especifica. Antes de ejecutar el proceso de depuración se deberá realizar un respaldo de la información.
Modificar claves |	Permite al usuario tener acceso a la utilería para cambiar claves. Esta utilería es muy útil ya que permite al usuario unificar claves de Artículos, Clientes o Proveedores repetidos.
Utilerías de SAIT distribuido |	Permite al usuario contar con el acceso a utilerías relacionadas con el servicio de enlace de sucursales. Dichas utilerías permiten reenviar la existencia actual de la sucursal al resto de las sucursales, reenviar el catalogo de clientes, visualizar la bitácora de eventos generados y enviados por la sucursal, entre otras. 