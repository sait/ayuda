﻿+++
title = "Conciliación"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=DOXsGPFJxDE&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&index=9&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. H Conciliacion.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT es posible imprimir la conciliación bancaria en cuestión de minutos, siempre y cuando se registren todos los depósitos y retiros realizados a cada cuenta bancaria.

Al finalizar cada mes, es posible realizar la consulta de los movimientos procesados por el Banco ya sea mediante la consulta del estado de cuenta a través de Internet o en el momento en que llegan los estados de cuenta impresos en la empresa.

Para poder generar e [imprimir la conciliación](#imprimir-conciliacion)  bancaria es necesario conciliar los depositos y retiros registrados en el sistema: 

#### Existen 2 diferentes formas de conciliar depósitos:

* [Conciliar Depósitos](#conciliar-depositos)
        
* [Conciliar Depósitos Mejorada](#conciliar-depositos-version-mejorada) 

#### Existen 4 diferentes formas de conciliar retiros:

* [Conciliar Retiros](#conciliar-retiros)
        
* [Conciliar Retiros Version Mejorada](#conciliar-retiros-version-mejorada)

* [Conciliar Cheques](#conciliar-cheques)

* [Modificar Fecha de Cobro ](#modificar-fecha-de-cobro)

<!--- <h2 class="text-primary">Conciliar Depósitos </h2> -->

## Conciliar Depositos

El siguiente proceso describe los pasos para conciliar los depósitos realizados a cada cuenta bancaria.

1.Ingrese al menú de Bancos / Conciliación / Conciliar depósitos. 

![IMG](1.png)

2.Seleccione la cuenta bancaria.

3.Especifique el periodo movimientos que se van a conciliar.

4.Haga clic en [Cargar Mov].

5.Se muestra la información de cada depósito realizado: Fecha e Importe.

6.Haga clic en la casilla de la columna "Conc" para conciliar los movimientos.

7.Haga clic en [?] para buscar un depósito en especifico.

8.Al activar la casilla [ * ] Incluir conciliados permite Des-conciliar algún movimiento.

9.Cierre la ventana. 

<!--- <h2 class="text-primary">Conciliar Depósitos (Versión Mejorada)</h2> -->

## Conciliar Depositos Version Mejorada

El siguiente proceso describe los pasos para conciliar los depósitos realizados a cada cuenta bancaria con una versión mejorada del proceso anterior del sistema.

Con la versión mejorada se puede pegar la información de los depósitos contenida en el portapapeles. Dicha información puede estar en excel, archivo texto, etc. solo deberán copiar los datos y pegarlos en la ventana de conciliaciones.

Antes de entrar a esta opción deberá copiar la información que se va a utilizar, el orden de las columnas se selecciona hasta que se pegan los datos.

1.Ingrese al menú de Bancos / Conciliación / Conciliar Depósitos (Mejorada) 

![IMG](2.jpg)


2.Seleccione la cuenta bancaria

3.Haga clic en **[Pegar]**

4.La información se muestra por columnas en el orden en que fueron copiados. 

![IMG](3.jpg)

5.Haga clic sobre cada columna y seleccione el titulo que le corresponde según el tipo de dato. 

![IMG](4.jpg)

6.Seleccione el formato de fecha usado.

7.Haga clic en **[Continuar]**

8.Se muestran los datos del banco y se cargan los movimientos a conciliar en el sistema. 

![IMG](5.jpg)

9.Si se detectan movimientos del banco y del sistema con la misma fecha e importe, automáticamente se marcan listos para conciliar.

10.Puede hacer clic en la casilla correspondiente a cada movimiento para conciliarlo o descartarlo de la conciliación.

11.Por último, haga clic en **[Conciliar]** para grabar. 


## Conciliar Retiros


Es una forma sencilla de conciliar retiros, es similar a la ventana de "Conciliar Depósitos".

1.Ingrese al menú de Bancos / Conciliación / Conciliar retiros. 

![IMG](6.png)

2.Especifique la fecha en que fue cobrado el cheque según el estado de cuenta del banco. Esta fecha es muy importante ya que es la base para generar la DIOT.

3.Seleccione la cuenta bancaria.

4.Especifique el periodo de movimientos que se van a conciliar.

5.Haga clic en **[Cargar Mov]**

6.Se muestra la información de cada retiro realizado: Fecha, Folio, Beneficiario, Retiro.

7.Haga clic en la casilla de la columna "Conc" para conciliar los movimientos.

8.Se pueden realizar búsquedas de un retiro en especifico.

9.Se pueden ordenar los datos en forma ascendente o descendente.

10.Al activar la casilla [ * ] Incluir conciliados permite Des-conciliar algún movimiento.

11.Cierre la ventana. 

## Conciliar Retiros Version Mejorada


El siguiente proceso describe los pasos para conciliar los retiros realizados a cada cuenta bancaria con una versión mejorada del proceso anterior del sistema.

Con la versión mejorada se puede pegar la información de los retiros contenida en el portapapeles. Dicha información puede estar en excel, archivo texto, etc. sólo deberán copiar los datos y pegarlos en la ventana de conciliaciones.

Antes de entrar a esta opción deberá copiar la información que se va a utilizar, el orden de las columnas se selecciona hasta que se pegan los datos.

1.Ingrese al menú de Bancos / Conciliación / Conciliar Retiros (Mejorada)

![IMG](7.jpg)

2.Seleccione la cuenta bancaria

3.Haga clic en **[Pegar]**

4.La información se muestra por columnas en el orden en que fueron copiados.

![IMG](8.png)

5.Haga clic sobre cada columna y seleccione el título que le corresponde según el tipo de dato.

![IMG](9.jpg)

6.Seleccione el formato de fecha usado.

7.Haga clic en **[Continuar]**

8.Se muestran los datos del banco y se cargan los movimientos a conciliar en el sistema.

![IMG](10.jpg)

9.Si se detectan movimientos del banco y del sistema con el mismo folio e importe o fecha e importe, automáticamente se marcan listos para conciliar.

10.Si hay algún movimiento que no aparece en la información del banco y será conciliado, deberá especificar la fecha de cobro de esos movimientos conciliados manualmente.

11.Puede hacer clic en la casilla correspondiente a cada movimiento para conciliarlo o descartarlo de la conciliación.

12.Por último, haga clic en **[Conciliar]** para grabar.

## Conciliar Cheques

Permite realizar de una forma más sencilla la conciliación de cheques (retiros), ya que ahí se pueden agregar desde Excel los retiros en caso de contar con el estado de cuenta del banco en Excel o capturar directamente el folio del cheque.

1.Ingrese al menú de Bancos / Conciliación / Conciliar Cheques.

![IMG](11.png)

2.Especifique la clave de la cuenta bancaria. Puede dar clic en **[?]** para buscar.
3.Capture la fecha en que fue cobrado el cheque.

4.Haga clic en **[Pegar desde Excel]** en caso de tener los movimientos en una hoja de Excel.

Únicamente se debe tener el folio de los cheques que se van a conciliar.

5.También se puede capturar directamente el folio de cada cheque

6.Por último, haga clic en **[Conciliar]** para grabar los cambios

## Modificar Fecha de Cobro

Permite modificar la fecha en que fueron conciliados los movimientos de una o más cuentas bancarias. Este proceso es de mucha utilidad para corregir errores al momento de especificar la fecha de cobro al conciliar los movimientos.
1.	Ingrese al menú de Bancos / Conciliación / Modificar Fecha de Cobro.

![IMG](12.png)

2.Especifique los cheques a incluir: Todos, movimientos en tránsito o únicamente los que ya que conciliaron.

3.Capture la clave de la cuenta bancaria. Puede dar clic en [?] para buscar.

4.Especifique el rango de fechas de los cheques.

5.Capture el rango de fechas en que fueron cobrados.

6.Especifique el rango de folio de los movimientos.

7.Nota:En caso de omitir estos datos, la consulta va a mostrar todos los movimientos de Bancos de todas las cuentas

8.Haga clic en **[Consultar]**

9.Se muestran los movimientos que cumplen con las restricciones de la consulta: Banco, No. de Cuenta, Folio, Fecha, Beneficiario, Retiro, Fecha de Cobro.

10.En la columna “Cobrado” se puede especificar la nueva fecha de cobro. Al dejar en blanco esta fecha, el movimiento ya no estará conciliado.

11.Cerrar la ventana.

## Imprimir Conciliacion

Permite imprimir la conciliación de depósitos y retiros de un determinado periodo. 

1.Ingrese al menú de Bancos / Conciliación / Imprimir Conciliación. 

![IMG](13.png)

2.Especifique la fecha de la conciliación. 

3.Seleccione la cuenta bancaria. 

4.Se muestra el saldo según libros, el total de depósitos y cheques en tránsito. 

5.Especifique el saldo de la cuenta bancaria según el banco. 

6.En base al saldo según el banco y el saldo según libros, se muestra el total. 

7.En caso de haber diferencias se muestra el importe total de la diferencia, la cual se puede deber a algún movimiento que no se concilió o no se registro en el sistema 

8.Se puede imprimir el Resumen o el Detalle de la conciliación. 
Al imprimir el **[Resumen]** solo muestra totales: 

![IMG](14.png)


Al imprimir el **[Detalle]**, además de los totales, se muestra la relación de depósitos y cheques que quedaron en tránsito 
 
![IMG](15.png)