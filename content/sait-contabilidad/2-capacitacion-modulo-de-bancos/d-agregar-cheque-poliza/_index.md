﻿+++
title = "Agregar Cheque – Póliza"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=ikE0W2one_I&index=8&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. D Agregar Cheque-Poliza.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Este proceso  a diferencia de generar Cheque desde el Modulo de Cuentas por Pagar se hace de manera manual es decir capturando cargos y abonos, así mismo al hacer este proceso desde Bancos no afectamos Cuentas por Pagar solo afecta Bancos y Contabilidad.

Para realizar este proceso diríjase a:

1.Bancos / Agregar Cheque – Póliza.


3.El número de cheque el sistema lo asigna a partir del último que se agregó. Si lo desea, puede modificarlo y a partir de ahí se llevará el consecutivo.

4.Capture la fecha.

5.Se puede capturar la fecha de cobro, la cual puede ser impresa en el cheque.

6.Especifique el nombre del beneficiario. Haga clic en [?] o presione la tecla [F2] para realizar búsquedas.

7.Escriba el importe.
8.	Active la casilla [ * ] Para abono en cuenta en caso de desee que salga impresa esa leyenda en el cheque.

9.Escriba el concepto general del cheque

10.Especifique el Tipo de Cambio en caso de que sea en dólares.

11.Capture los asientos contables correspondientes a la póliza del cheque.

12.Presione la tecla [F8] para procesar e imprimir el Cheque-Póliza.


![IMG](1.png)

**Nota: Se puede imprimir únicamente el Cheque, la Póliza o ambos al hacer clic en el botón [►] que esta enseguida de [Imprimir]**