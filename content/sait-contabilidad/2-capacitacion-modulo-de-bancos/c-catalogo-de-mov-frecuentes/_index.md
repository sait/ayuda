﻿+++
title = "Catálogo de Movimientos Frecuentes"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=wYK_TC6067g&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&index=6&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. C Catálogo de Movimientos Frecuentes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT es posible definir cheques y pólizas que se realizan con frecuencia, lo cual facilita su elaboración y disminuye considerablemente el tiempo que se tarda en contabilizar cada movimiento en forma manual. 

El catálogo de movimientos frecuentes permite agregar, modificar y eliminar cheques y pólizas frecuentes. También es posible definir movimientos al consultar un cheque o una póliza. 

<h2 class="text-primary">Agregar Movimiento Frecuente</h2>

1.Diríjase a la sección de Bancos/ Catálogo de Movimientos Frecuentes

2.De clic en Agregar

![IMG](1.png)
 
3.Escriba la descripción del movimiento. Esta será la descripción que aparecerá en el menú de movimientos frecuentes. Se recomienda que sea breve y haga referencia al movimiento de que se trata. **Ejemplo: Pago de Luz.** 

4.En caso de que el movimiento se trate de un cheque: 

* Active la casilla [ * ] Cheque. 

* Capture la clave del beneficiario. Haga clic en [?] o presione la tecla [F2] para realizar búsquedas. 

* Especifique el importe del cheque. 

* Capture el concepto general de la póliza. 

* De clic en Grabar


![IMG](2.png)


<h2 class="text-primary">Modificar Movimiento Frecuente</h2>

1.Ingrese al menú de Bancos / Catálogo de Movimientos Frecuentes. 

2.Localice el movimiento que desea modificar y haga clic en [Modificar]. 

3.Realice las modificaciones necesarias. 

4.Haga clic en [Grabar] 

<h2 class="text-primary">Eliminar Movimiento Frecuente</h2>

1.Ingrese al menú de Bancos / Catálogo de Movimientos Frecuentes.

2.Localice el movimiento que desea eliminar y haga clic en [Eliminar].

3.Se muestra su información. 

4.Verifique que sea el movimiento que se desea eliminar. 

5.Haga clic en [Eliminar] 
