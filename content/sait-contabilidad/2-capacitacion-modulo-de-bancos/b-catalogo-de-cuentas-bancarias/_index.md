﻿+++
title = "Catálogo de Cuentas Bancarias"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=xHubmTg6-iQ&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&index=3&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. B Catálogo de cuentas bancarias.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT se puede llevar el control de varias cuentas bancarias tanto en pesos como en dólares, lo cual le permite conocer en todo momento el saldo actual de cada cuenta siempre y cuando registre todos los depósitos y retiros.

El catálogo de cuentas bancarias le permite agregar, modificar y eliminar las diferentes cuentas bancarias que se van a manejar en el sistema. También le permite exportar el catálogo a Excel.

<h2 class="text-primary">Agregar Nueva Cuenta Bancaria</h2>

1.Diríjase a la sección de Bancos/ Catálogo de cuentas bancarias

2.Clic en el botón [Agregar]


![IMG](1.png)

3.Llenar los datos requeridos:

* La clave de la cuenta es un número consecutivo el cual es proporcionado a partir de la última cuenta que se agregó. Si lo desea, puede modificarla y a partir de ahí se llevará el consecutivo de claves. 

* Escriba el número de cuenta del banco. 

* Especifique el nombre del banco. 

* Cada cuenta bancaria puede contar con un formato distinto de acuerdo con las especificaciones físicas de cada cheque. El sistema asigna automáticamente el nombre del formato. 

* Seleccione con la barra espaciadora la divisa del documento: **Pesos, Dólares.** 

* Escriba el siguiente número de cheque. A partir de ahí el sistema llevará el consecutivo, el cual se mostrará al agregar un cheque. 

* Capture la cuenta contable 

* En caso de que la divisa de cuenta sea “Dólares”, escriba la cuenta contable complementaria. 

* Escriba el número del tipo de póliza a Generar. Ejemplo: **D (Diario)** 

El tipo de póliza se configura en su sistema contable. En SAIT el tipo de póliza se define entro del menú de Contabilidad / Tipos de Póliza 


![IMG](2.png)

•	Seleccione las opciones al imprimir póliza: 

[ * ] Ordenar Asientos (Cargos, Abonos) 

[ * ] Imprimir Cuentas de Mayor 

•	Haga clic en [Agregar].

![IMG](3.png)

<h2 class="text-primary">Modificar una Cuenta Bancaria</h2>


1.Ingrese al menú de Bancos / Catálogo de Cuentas Bancarias y seleccione la cuenta que desea modificar. 

2.Una vez seleccionado dar clic en la opción [Modificar]. 

3.Realizar las modificaciones necesarias en la ventana. 

![IMG](4.png)

4.Deberá dar clic en [Modificar] para guardar los cambios. 

5.Listo

<h2 class="text-primary">Eliminar una Cuenta Bancaria</h2>


1.Ingrese al menú de Bancos / Catálogo de Cuentas Bancarias y seleccione la cuenta que desea eliminar. 

2.Deberá dar clic en [Eliminar]. 

3.Verificar que la cuenta bancaria a eliminar sea la correcta.

4.Si la cuenta bancaria que desea eliminar es la correcta deberá dar clic en [Eliminar].

![IMG](5.png)

5.Aparecerá el siguiente mensaje de confirmación y daremos clic en Si
 
![IMG](6.png)

**NOTA: Al borrar la cuenta bancaria solo se elimina del catálogo, deberá realizar los cambios necesarios en contabilidad en caso de haber procesado cheques pólizas relacionados con esa cuenta bancaria.**