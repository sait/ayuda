﻿+++
title = "Importar Cuentas Contables"
description = ""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=GtTeHp79S9o&list=PLhfBtfV09Ai4JH5ywKBuuqQ8KU0fDpdaW&index=7&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. L Importar cuentas contables.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Si usted está iniciando con el proceso de implementación de contabilidad electrónica, SAIT cuenta con una herramienta que te permite capturar de forma masiva el catálogo de cuentas contables.

Para esto, deberá dirigirse al menú de Contabilidad / Utilerías / Importar Cuentas Contables.

En esta pantalla se especifica claramente los pasos a seguir para importar de forma masiva el catálogo de cuentas contables.

![IMG](1.png)

1.El archivo en Excel debe tener 5 columnas ordenadas por: 

### Cuenta | Descripción | Grupo BR | Tipo BR | Tipo DA

* **Número de Cuenta contable**

* **Grupo BR (En la parte inferior de la ventana se especifican las opciones para esta columna)**

* **Tipo BR (Balance o Resultados)**

* **Tipo DA (Deudora o Acreedora)**

2.Una vez que tenga toda la información capturada en el orden correcto en el archivo en Excel, solo deberá seleccionar en el documento en Excel las celdas que contienen la información que se va a importar. Es decir, **NO debe seleccionar los títulos.**

![IMG](2.png)

3.Ya seleccionada la información entre al menú de Edición y de clic en [Copiar]. O bien, dar clic derecho y seleccionar la opción de [Copiar].

![IMG](3.png)


4.Dentro de la ventana de SAIT, solamente deberá presionar el icono    de [Pegar].

5.Y listo, la información se mostrará en pantalla. Como puede observar, no es necesario capturar las columnas de grupo BR, tipo BR y tipo DA para las subcuentas. Basta con que las coloque a las cuentas de mayor, y en automático éstas se las añadirá a sus subcuentas.

![IMG](4.png)


6.Verifique que la información haya sido capturada correctamente, de estar todo en orden, solo deberá presiona el botón de [Agregar Cuentas].

7.Deberá confirmar que efectivamente desea agregar las cuentas, a su catálogo.

![IMG](5.png)

8.Y listo, el sistema le confirmará que las cuentas han sido agregadas correctamente.


![IMG](6.png)