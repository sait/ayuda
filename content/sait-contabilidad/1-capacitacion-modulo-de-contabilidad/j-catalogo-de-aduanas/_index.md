﻿+++
title = "Catálogo de Aduanas"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=LhQfBeKjxNw&list=PLhfBtfV09Ai4JH5ywKBuuqQ8KU0fDpdaW&index=9&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. J SHCP - SAT - Aduanas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El catálogo de aduanas le permite definir las aduanas usadas en sus importaciones. 

Para acceder el catálogo vaya al menú de: Contabilidad / SHCP-SAT / Aduanas. 

Se presentará la siguiente ventana: 


![IMG](1.png)

<h2 class="text-primary">Agregar Aduana</h2>

Antes de agregar una aduana nueva, es **MUY IMPORTANTE** que investigue cual es la clave que el SAT asignó a su aduana, normalmente está clave aparece en el pedimento de importación.
 Para agregar una aduana realice lo siguientes pasos: 

1.En el menú seleccionar la opción de: Contabilidad / SHCP-SAT / Aduanas. 

2.Revise que la aduana no aparece en el catálogo. 

3.Haga clic en el botón de [Agregar]

4.Escriba la clave asignada por el SAT a la aduana. 

5.Escriba el nombre de la aduana. 

6.Haga clic en el botón de [Agregar]

7.El sistema agregará la aduana a la lista de aduanas y regresará a la ventana de "Catálogo de Aduanas" 

![IMG](2.png)

<h2 class="text-primary">Modificar Aduana</h2>

Para modificar una aduana realice lo siguientes pasos: 

1.En el menú seleccionar la opción de: Contabilidad / SHCP-SAT / Aduanas. 

2.Seleccione el registro que desea modificar y de clic en [Modificar]

3.Ingrese los cambios (solo puede modificar el nombre de la Aduana)

4.Haga clic en el botón de [Modificar]

5.Listo, los cambios fueron guardados.

![IMG](3.png)

<h2 class="text-primary">Eliminar Aduana</h2>


Para eliminar una aduana realice lo siguientes pasos:

1.En el menú seleccionar la opción de: Contabilidad / SHCP-SAT / Aduanas. 

2.Seleccione el registro que desea eliminar y de clic en [Eliminar]

3.Confirme es el registro correcto y de clic en [Eliminar]

![IMG](4.png)

4.Confirme la acción dando clic en Si 

![IMG](5.png)

5.Listo, la aduana ha sido eliminada.


<h3 class="text-danger">NOTA</h3>

Como cualquier registro en SAIT no se recomienda eliminarlo si ya hemos realizado movimientos con ese registro.