﻿+++
title = "Tipos de Pólizas"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=coYXAAm0Nac&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. B Tipos de Pólizas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite definir los tipos de pólizas que usted desee utilizar. Es muy importante definirlas antes de iniciar con las operaciones de registros de pólizas, esto con la intención de que toda la información quede correctamente registrada en el sistema.

El SAT solamente establecer 3 tipos de pólizas:

* **DIARIO**

* **INGRESOS**

* **EGRESOS**

Estos tipos de pólizas, usted los puede utilizar para los distintos conceptos que desee implementar en las pólizas, mismos que debemos clasificar dentro de nuestro catálogo de tipos de pólizas de SAIT / Contabilidad, por cada tipo de póliza que lleguemos a manejar

![IMG](1.png)

<h2 class="text-primary">Agregar / Modificar Tipos de Pólizas</h2>

Para clasificar estos tipos de pólizas que el SAT establece, dentro de SAIT:

1.Ingrese al menú de Contabilidad / Tipos de Pólizas.

2.El sistema por default tendrá definido los 3 tipos de pólizas que establece el SAT.

![IMG](1.png)

3.Si usted desea ingresar un nuevo tipo de póliza deberá definir en la cuadricula:

**a.	La clave (máximo 2 caracteres**)

**b.	La descripción**

**c.	Y el tipo de póliza (presione la barra de espacio para seleccionar el tipo SAT como Diario, Egresos o Ingresos).**

![IMG](2.png)

4.Una vez que haya finalizado con la captura de los nuevos tipos de póliza deberá presionar el botón de [Guardar].

<h2 class="text-primary">Eliminar Tipos de Pólizas</h2>

Para eliminar un tipo de póliza en el sistema, deberá:

1.Ingresar al menú de Contabilidad / Tipos de Pólizas.

2.Posicionarse sobre el tipo de póliza que desea eliminar.

![IMG](3.png)

3.Una vez posicionado sobre el registro deseado, haga clic en el botón de [Borrar].

4.Deberá confirmar que se eliminó el registro correcto y en caso de estar todo en orden, haga clic en el botón de [Guardar].
