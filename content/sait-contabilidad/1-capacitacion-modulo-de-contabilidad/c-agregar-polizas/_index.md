﻿+++
title = "Agregar Pólizas"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=uSZRpQJG4SI&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. C Agregar Pólizas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite agregar pólizas de forma manual, según lo requiera. Por ejemplo, para capturar la póliza de saldos iniciales en el sistema.

Para realizar este proceso deberá seguir las siguientes indicaciones:

1.Ingrese al menú de Contabilidad / Agregar Pólizas.


![IMG](1.png)

2.Especifique la **Fecha** en la que desea registrar la póliza.

3.Indique el **Tipo** de póliza a generar.

4.Coloque el **Folio del movimiento.**

5.Especificar una **Descripción** relacionada a la póliza. 

![IMG](2.png)

6.En el campo de **CFDIs** puede presionar F2 para adjuntar comprobantes a la póliza. 

7.Posteriormente deberá capturar las cuentas contables en donde se harán los cargos y abonos pertenecientes a la póliza.

![IMG](3.png)

8.De manera opcional puede relacionar los comprobantes por renglón.

9.Una vez que haya terminado de capturar todos los asientos contables dentro de la póliza, deberá presionar el botón de [Guardar = F8] y confirmar que la póliza esta correcta.

10.Y listo, la póliza ha sido generada correctamente.


