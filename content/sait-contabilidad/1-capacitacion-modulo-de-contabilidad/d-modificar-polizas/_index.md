﻿+++
title = "Modificar Pólizas"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=oTnEWAAdD50&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. D Modificar Polizas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para modificar pólizas ya registradas previamente en SAIT deberá realizar estas indicaciones: 

1.Ingrese al menú de Contabilidad / Modificar Pólizas.

![IMG](1.png)

2.Debe indicar el **Año** en el que se encuentra registrada la póliza que desea modificar.

3.Seleccione el **Mes** en el que realizó la póliza.

4.Especifique el **Tipo de póliza** con el que fue registrada la póliza.

5.Indique el **Folio** con el que se grabó dicha póliza y presione ENTER.

6.Automáticamente se mostrará el detallado de la póliza.

![IMG](2.png)

7.En este momento usted podrá modificar:

* Fecha de registro

* El concepto

* Comprobantes fiscales 

* Asientos contables

* Cargos y Abonos 

8.Una vez que haya realizado las modificaciones necesarias, haga clic en [Guardar = F8].

![IMG](3.png)

9.Y listo, la póliza ha sido modificada correctamente.

<h2 class="text-primary">Borrar Pólizas</h2>

Para borrar una póliza deberá consultarla previamente:

1.Ingresar al menú de Contabilidad / Modificar Pólizas.

2.Indicar el período en el que se encuentra registrada la póliza (Año y Mes).

3.Indicar el tipo de póliza, el folio correspondiente de dicha póliza y presionar ENTER.

4.En la parte inferior de esta ventana, deberá presionar el botón de [Borrar].

![IMG](4.png)

5.Confirmar que desea eliminar la póliza.

![IMG](5.png)

6.Y listo, la póliza ha sido eliminada correctamente.

<h2 class="text-primary">Pólizas Frecuentes</h2>

Una póliza frecuente es una plantilla que estará disponible en el sistema para que la pueda utilizar en cualquier movimiento, sin tener que volver a capturar los asientos contables.

Para hacer una póliza frecuente en el sistema deberá:

1.Ingresar al menú de Contabilidad / Modificar Pólizas.

2.Indicar el período en el que se encuentra registrada la póliza (Año y Mes).

3.Indicar el tipo de póliza, el folio correspondiente de dicha póliza y presionar ENTER.

4.En la parte inferior de esta ventana, deberá presionar el botón de [Frecuente].

![IMG](6.png)

5.Deberá confirmar que está seguro de generar una póliza frecuente.

![IMG](7.png)

6.Deberá establecer la descripción que desea colocarle a dicha póliza y presionar el botón de [Continuar].

![IMG](8.png)

7.Y listo, la póliza frecuente ha sido generada correctamente. 

8.Estas pólizas estarán disponibles en el menú de Contabilidad / Pólizas Frecuentes.
