﻿+++
title = "Registro Manual de Gastos"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=qXr_-bfBJCM&index=6&t=0s&list=PLhfBtfV09Ai6evDYeDMhYLYgGgMc_iBq7" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="13. A Registro de Gastos Manualmente.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


Dentro de SAIT usted puede realizar el registro de gastos de una forma fácil y rápida.

Para registrar un gasto directamente en el sistema SAIT diríjase a:

<h4 class="text-primary">Gastos / Registro de Gastos</h4>

![IMG](1.png)

1. Debe indicar la clave del proveedor. También puede presionar **[F2]** para buscar por nombre de proveedor.

2. Debe seleccionar el tipo de gasto a registrar.

3. Capture el folio de factura del gasto.

4. Capture la fecha en que se emitió el gasto 

5. Defina la fecha de vencimiento del documento.

6. Indique la divisa en que se procesó el gasto. Puede cambiar dicho valor presionando la  barra espaciadora de su teclado.

7. Debe capturar los importes del documento.

8. En caso de ser necesario, puede capturar alguna observación para el Gasto.

![IMG](2.png)

9. Finalmente haga clic en **[Guardar]**.

10. Listo, el gasto ha sido grabado correctamente en su sistema.

<h3 class="text-danger">NOTA</h3>

#### En caso de que registre si gasto bajo la divisa de dólares, debe especificar el tipo de cambio oficial correspondiente  para la fecha del registro del gasto. Esto en el menú de Utilerías  / Tipo de Cambio Oficial.
