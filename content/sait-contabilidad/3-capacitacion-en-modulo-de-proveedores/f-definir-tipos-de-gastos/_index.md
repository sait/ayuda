﻿+++
title = "Tipos de Gastos"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=_CGXQdAfW5Y&index=2&t=0s&list=PLhfBtfV09Ai6evDYeDMhYLYgGgMc_iBq7" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="13. E Tipos de Gastos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


SAIT le permite definir los tipos de gastos que según tu empresa requiera. 

Para poder agregar otro tipo de gastos dentro de SAIT diríjase a:

<h4 class="text-primary">Gastos / Tipos de gastos</h4>


![IMG](1.png)

1.El sistema por default ya trae precargado los gastos más comunes como gastos de teléfono, gasolina, papelería o de asesoría, los cuales puedes modificar según tus necesidades o bien, agregar nuevos gastos.

2.Para agregar un nuevo gasto solo debe presionar el botón de **[Agregar]**.

![IMG](2.png)


A continuación, se mostrará la siguiente ventana en donde deberá seleccionar un tipo de gasto a agregar, éste puede ser de tipo factura, factura con descuento, recibo de honorarios, recibo de arrendamiento u otro.  (Para efectos del ejemplo se seleccionó factura) 

![IMG](3.png)

3.Debe especificar la descripción del gasto: para el ejemplo será Sistema Contable, el cual aplicará para facturas de gastos.

4.Al haber seleccionado el tipo como Factura, en automático se cargan los datos a capturar, considerando los datos típicos de cualquier factura como lo es Subtotal e Impuesto. En caso de ser necesario usted puede agregar otros datos a capturar:

* Indicando el titulo del dato

* Seleccionando el tipo de dato como descuentos, datos informativos o Retenciones.  (puede cambiar de dato con la barra espaciadora )

* En la tercera columna se refiere a la fórmula del CFDI, es decir, de que nodo se va a tomar o leer el valor dentro de la estructura del archivo XML, en caso de agregar un gasto a partir del CFDI. (puede cambiar de dato con la barra espaciadora )

![IMG](4.png)

{{%alert info%}} Hasta aquí usted ya puede agregar el gasto a su sistema para hacer el cargo al proveedor, pero si desea llevar la contabilidad en SAIT es importe que defina los asientos contables. {{%/alert%}} 

5.En la parte de asientos, es donde se le debe indicar al sistema de qué manera se va a contabilizar el gasto que vamos a dar de alta. Es decir, si se va a contabilizar en cheques, si se va a provisionar el gasto y si se va a pagar la provisión.

![IMG](5.png)

{{%alert info%}} Debe indicar las cuentas contable y las variables que según su contabilización requieran. En este punto le recomendamos que esta configuración la realice con su asesor contable. {{%/alert%}} 

6.En la primera columna usted puede indicar una cuenta contable o una variable. Puede teclear directamente o puede presionar la tecla **[F2]** para buscar una cuenta contable.

7.En la tercera columna debe indicar con la barra espaciadora del teclado si este asiento representa un **[C]** cargo o un **[A]** abono.

8.En la columna de importe se deberá colocar el dato al que se le atribuirá a dicho asiento, por ejemplo, para la cuenta del proveedor será el subtotal anteponiendo siempre la letra "n" indicando que es un valor numérico. Estos datos los tomaremos de la ventana superior de Datos a Capturar.

9.Posteriormente en concepto puede colocar algún dato informativo variable o fijo. En este caso se tomó la variable de provedorFactura. Es decir la clave del proveedor + el folio del documento de gasto.

10.Usted decide si desea agrupar los asientos contables por el tipo de asiento o por una definición especial.

11.Y finalmente, seleccionar la forma de contabilizar, contabilizar en cheques, provisionar o pagar provisionar.

12.En la parte inferior de la ventana usted puede presionar el botón de **[Variables]** y podrá observar que el sistema previamente trae pre cargadas las variables de provedorFactura y CuentaDelProvedor. 

![IMG](6.png)

Aquí es donde usted puede definir todas las variables que requiera. 

13.Una vez que termine de capturar todos los asientos contable de su gasto de clic en **[Guardar]**.
