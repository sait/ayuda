﻿+++
title = "Reportes de Proveedores"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=TqKEG14LnjQ&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI&index=12&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. M Reporte de Proveedores.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT se pueden obtener reportes del catálogo de los proveedores registrados en el sistema de una manera muy sencilla y rápida.

Esto sirve para obtener una relación de los proveedores con los que cuenta la empresa. 

Para acceder a esta opción, deberá ingresar al menú de:

**Compras / Reportes de Proveedores / Catálogos de Proveedores**

![IMG](1.png)


Para obtener el reporte puede: 

1.Filtrar por rango de proveedores. 

2.Seleccionar el orden a presentar la información (por: clave, nombre, R.F.C, clasificación, dirección, entre otros). 

3.Presionar el botón de **[Imprimir]** u **[Hoja]** para mostrar la información de acuerdo con los datos que sugeriste en la búsqueda. 

4.Si se selecciona la opción de **[Imprimir]** se mostrará el siguiente menú de impresión, donde deberá elegir la opción deseada. 


![IMG](2.png)

Si elige la opción **[Pantalla]** se mostrará de la siguiente manera:

![IMG](3.png)


5.Si selecciona la opción de **[Hoja]** la información se mostrará en columnas, que te permitirá exportar los datos a Excel dando clic en ícono de la aplicación.

![IMG](4.png)