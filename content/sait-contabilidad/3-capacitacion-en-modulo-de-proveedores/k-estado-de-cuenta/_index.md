﻿+++
title = "Estado de Cuenta de Proveedor"
description = ""
weight = 11
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=oWAuABsMoHU&t=0s&index=8&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. J Estado de Cuenta CXP.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En el estado de cuenta se permiten consultar todos los movimientos que se le han realizado a un proveedor en específico. Cada factura de compra o gasto y cada pago realizado pueden ser consultados desde esta ventana.

Para ingresar a esta opción debe dirigirse al menú de Cuentas por Pagar / Estado de Cuenta.

1.Escriba la clave del proveedor, o presione la tecla [F2] para buscar por nombre.
 
![IMG](1.png)

2.Se mostrarán todos los movimientos del proveedor.

3.La información se puede ordenarse por Fecha y Factura si así lo desea.

4.Al hacer doble clic sobre el folio del documento de tipo Compra, se muestra la consulta individual para su revisión.

![IMG](2.png)

5.De manera opcional, se puede enviar la consulta a [Excel] presionando el icono.

<h3 class="text-danger">NOTA</h3>

* Los cargos pendientes de pago se muestran en color rojo.

* Los abonos o cargos pagados se muestran en color negro.

* Los anticipos o notas de crédito (saldo a favor al proveedor) pendientes de aplicar se muestran en color azul.


<h2 class="text-primary">MODIFICAR FOLIO</h2>

Se puede modificar el folio de algún movimiento en caso de ser necesario.

1.Para esto debe solo debe localizar el movimiento con folio incorrecto.

2.Deberá dar clic en el botón de [Modificar Folio] en la parte inferior de esta ventana.
 
![IMG](3.png)

3.Especifique el nuevo folio.
 
![IMG](4.png)


<h2 class="text-primary">ELIMINAR MOVIMIENTOS</h2>

Además de consultar movimientos, desde esta ventana es posible eliminar movimientos en caso de haberse registrado de manera incorrecta o que no corresponda al proveedor.

1.Antes que nada deberá localizar el movimiento que se desea eliminar.

2.Puede ordenar la información por Fecha o Factura para que sea más fácil de localizar el movimiento.

![IMG](5.png)

3.Una vez localizado, se posiciona sobre el registro deseado y haga clic en [Eliminar Movimiento] en la parte inferior de esta ventana.

4.El sistema preguntará si desea borrarlo. En caso de estar completamente seguro haga clic en [Si].

![IMG](6.png)

<h3 class="text-danger">NOTA</h3>

**En caso de que el tipo de movimiento que se va a eliminar sea un Cargo el sistema les indicará que es necesario que primero se eliminen los abonos recibidos al documento.**

![IMG](7.png)