﻿+++
title = "Pagar CFDI"
description = ""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=3h2H-D_7Pwk&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. F Pagar CFDI.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Esta opción te permite registrar el gasto y el pago del gasto en un solo movimiento.

Para realizar este proceso deberá seguir las siguientes indicaciones:

1.Ingresar al menú de Cuentas por Pagar / Pagar CFDI.

![IMG](1.png)

2.Debe indicar el **archivo XML** del cual se jalará el gasto. Presione F2 para buscar el comprobante en un directorio local.

![IMG](2.png)

3.Debe especificar el tipo de gasto que desea registrar.

4.En pantalla se mostrarán los datos del proveedor y la información contenida en el archivo XML como el folio, fecha de factura, fecha de vencimiento, divisa y los importes totales del documento.

![IMG](3.png)

5.Deberá presionar el botón de [F8 Pagar] y confirmar que realmente desear el CFDI.

![IMG](4.png)

6.Y listo, el gasto se ha registrado correctamente.

7.Posteriormente se mostrará la ventana de Elaborar Cheque para registrar el pago al proveedor.
 
![IMG](5.png)

<h3 class="text-danger">NOTA</h3>

**Es importante aclarar que este proceso solo aplica si su configuración de gastos es Contabilizar en Cheque. No aplica si su configuración es para provisionar Gastos.**

8.En automático, en esta ventana ya viene seleccionado el proveedor, ya viene asociado el archivo XML, ya está indicado el documento a pagar y la póliza que se va a generar. Solamente deberá:

* Seleccionar el banco.

* Especificar el folio de la póliza.

* Indicar la fecha del movimiento.

* Especificar si es Transferencia o Cheque.

* En el caso del Cheque, podrá seleccionar si es para abono en cuenta y definir la fecha límite para cobrar dicho cheque, con el fin de que salga impreso.

* Especificar el concepto del movimiento. 

9.Finalmente deberá presionar el botón de [F8 Procesar] para grabar el pago a proveedor.

10.Y listo, se ha realizado el registro del gasto y el pago a proveedor en un solo proceso.



