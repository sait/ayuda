+++
title = "Actualización de tablas 2023 y Salario Mínimo"
description = ""
weight = 10
+++

<h2 class="text-primary">Salarios 2023</h2>  

Para realizar la actualización de nuestros salarios hay que entrar al concepto 101 y 102 y actualizar, OJO es importante que identifiques tu zona para realizar dichos cambios.

![1](2023.png)

{{%alert success%}}Los pasos para modificar los conceptos números 101 y 102 son los siguientes:{{%/alert%}}

1. Entrar a SAIT Nomina.

2. Colocarse en un período de nómina que aún no se ha pagado.

3. Dirigirse al menú: **Catálogos / Conceptos**.

4. En el campo: **CLAVE** capturar 101 y presionar la tecla **[Enter]**.

5. Posicionarse en el campo: **FORMULA** y capturar el salario mínimo diario según la zona que corresponda

6. Haga clic en el botón: **[Grabar]**. 

7. Realizar los mismos pasos 4 y 5, para el **Concepto 102**. 

8. Listo, nuestro sistema de nómina 2023, ya está actualizado con el salario mínimo diario de la zona.

{{%alert success%}}Si ya generaste tu nómina de la primer semana de enero no te preocupes, recuerda que ahora el cálculo de los impuestos es con base al valor del UMA.{{%/alert%}}.

<h2 class="text-primary">TABLA 1 ISR 2023</h2> 

Como ustedes saben, año con año en el diario oficial de la federación se publican los nuevos parámetros a incluir en las tablas mensuales para el cálculo de ISPT de nómina, con el objetivo de integrarlos a las aplicaciones de nómina.

**Las tablas ISR 2023 este año sufrieron un cambio sólo en la tabla 1 del ISR, mientras que la del subsidio quedó exactamente igual**

En SAIT NÓMINA es necesario capturar las tablas cuando existan modificaciones, las tablas no se cambian automáticamente al actualizar la versión.

#### TABLA 1

La tabla 1 correspondiente al ISPT o ISR Mensual es la siguiente:

**No de Tabla:** 	1

**Nombre:**	ISPTM

**No Columnas:**	4	

**Conc Entrada:**	149		

LimInf|LimSup|CuotaFija|PjeSExced
---|---|---|---
0.01 | 746.04  | 0.00 | 0.0192
746.05 | 6332.05 |	14.32 |	0.064
6332.06 | 11128.01 |	371.83 | 0.1088
11128.02 |	12935.82 |	893.63 | 0.16
12935.83 |	15487.71 |	1182.88 |	0.1792
15487.72 |	31236.49 |	1640.18 |	0.2136
31236.50 |	49233.00 |	5004.12 |	0.2352
49233.01 |	93993.90 |	9236.89 |	0.30
93993.91 |	125325.20   | 22665.17  | 0.32
125325.21 | 375975.61  | 32691.18  | 0.34
375975.62 | En adelante | 117912.32 | 0.35

<h2 class="text-primary">TABLA 3 SUBSIDIO AL EMPLEO 2023</h2> 

#### TABLA 3 

La tabla 3 correspondiente al subsidio al empleo y SIGUE SIENDO LA MISMA no tuvo ningún cambio al 2023, pero de igual manera se anexa en caso de que desee validar los datos.

**No de Tabla:** 	3

**Nombre:**	SUB2023

**No Columnas:**	3

**Conc Entrada:**	149

LimInf|LimSup|SubEmpl 
---|---|--- 
0.01 | 1768.96 | 407.02
1768.97 | 2653.38 | 406.83
2653.39 | 3472.84 | 406.62
3472.85 | 3537.87 | 392.77
3537.88 | 4446.15 | 382.46
4446.16 | 4717.18 | 354.23
4717.19 | 5335.42 | 324.87
5335.43 | 6224.67 | 294.63
6224.68 | 7113.90 | 253.54
7113.91 | 7382.33 | 217.61
7382.34 | 999999999 | 0

<h2 class="text-primary">Como Modificar las tablas en SAIT Nómina</h2> 

<h4 class="text-danger">Este proceso siempre es el mismo y lo único que cambia son los valores.</h4>

Antes de modificar las tablas 1 ISPT y 3 Subsidio al Empleo, deberás asegurarte de estar posicionado en un periodo de nómina que aun no se ha pagado.

1.Vaya al menú de Catálogos / Tablas, se visualiza de la siguiente ventana:

![1](1.jpg)

2.Capturar el número de tabla 1 y dar un [Enter]. Se visualiza la siguiente ventana con los valores anteriores, los cuales es necesario modificar:

![1](valores-anteriores.jpg)

3.Deberá cambiar el nombre de la tabla a: **ISPTM2023**.

4.Deberá seleccionar únicamente los valores de la tabla 1 (es decir sin los encabezados) y posteriormente copiarlos.

LimInf|LimSup|CuotaFija|PjeSExced
---|---|---|---
0.01 | 746.04  | 0.00 | 0.0192
746.05 | 6332.05 |	14.32 |	0.064
6332.06 | 11128.01 |	371.83 | 0.1088
11128.02 |	12935.82 |	893.63 | 0.16
12935.83 |	15487.71 |	1182.88 |	0.1792
15487.72 |	31236.49 |	1640.18 |	0.2136
31236.50 |	49233.00 |	5004.12 |	0.2352
49233.01 |	93993.90 |	9236.89 |	0.30
93993.91 |	125325.20   | 22665.17  | 0.32
125325.21 | 375975.61  | 32691.18  | 0.34
375975.62 | En adelante | 117912.32 | 0.35


5.Posicionarse nuevamente en SAIT Nómina y hacer un clic en la primera columna con valor de la tabla y posteriormente deberá de dar clic en el botón de PEGAR y notará que se pegarán los valores copiados.

![1](actualizada.jpg)

6.Para grabar los nuevos valores, deberá hacer clic en el botón [Grabar].

7.Listo, usted ha actualizado los valores de la tabla.


