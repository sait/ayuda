+++
title = "Nuevos Factores de Integración para vacaciones 2023"
description = ""
weight = 12
+++

Debido  a los cambios de los artículos 76 y 78 de la LFT, en los cuales se aumentó  los días de vacaciones a partir de  2023, estos tendrán un efecto en  materia seguro social ya que esto modifica el cálculo del Salario Base de Cotización ante el IMSS para el año 2023 debido a que se pagará más prima vacacional a los trabajadores.

En el sistema de nómina  SAIT, para los factores de integración existe una tabla en el menú de Catálogos, la cual tendrá que ser modificada.

El factor de integración mínimo será de 1.0493 a partir de 2023, al modificar a 12 los días de vacaciones en el primer año. Este factor de integración reemplaza al que anteriormente consideraba 6 días de vacaciones en el primer año y que era del 1.0452.

Nueva tabla

![img](base.jpg)

<h2 class="text-primary">Configuración para una empresa con movimientos</h2>

1) Si es una empresa que ya está trabajando, es decir tiene movimientos, se recomienda agregar una nueva tabla con los nuevos valores, para esto deberá entrar al menú de Catálogos / Factores de Integración

Capture la clave 2 y de **Enter** en su teclado, de esta manera el sistema habilitará tabla como una nueva, capturamos la descripción a TABLA DE LEY 2023.

![img](2.1.jpg)

Los valores a capturar son: Años, Días Aguin, Dias Vac y %Prima Vac, el campo de Factor Int no es modificable ya que este es función de los días de aguinaldo, días de vacaciones y prima vacacional.

Para terminar de clic en **Guardar** y listo.

![img](2.jpg)

Para ver los valores completos en excel de clic en el siguiente enlace de [descarga](tabla-completa.xlsx)

2) Finalmente deberá de actualizar en el catálogo de trabajadores la tabla a la clave 

Clic en **Guardar** y listo.

![img](trabajador.jpg)

<h2 class="text-primary">Configuración para una empresa nueva de reciente instalación</h2>


Si es una empresa nueva, es decir sin periodos creados o bien ya con periodos o que no se hayan pagado vacaciones en el sistema, se recomienda modificar la tabla que viene por default en el sistema.

Para esto deberá entrar al menú de Catálogos / Factores de Integración

Capture la clave 1 y de **Enter** en su teclado, se deberá colocar en la columna de días de vacaciones y sobre escribe los días de vacaciones, de clic en **Grabar** le da grabar y listo modificamos la TABLA DE LEY 2022 por la del 2023.

Para ver los valores completos en excel de clic en el siguiente enlace de [descarga](tabla-completa.xlsx)

![img](1.jpg)

Los valores a modificar son: Años, Días Aguin, Dias Vac y %Prima Vac, el campo de Factor Int no es modificable ya que este es función de los días de aguinaldo, días de vacaciones y prima vacacional.




