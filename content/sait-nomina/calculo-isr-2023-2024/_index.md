+++
title = "Cálculo Anual de ISR Ejercicio 2023"
description = ""
weight = 7
+++

Cada año debe de realizarse el cálculo anual de ISR sobre los ingresos de sueldos y salarios pagados, el cual se puede generar desde SAIT Nómina, para lo cual será necesario ejecutar el siguiente proceso.

<h2 class="text-primary">Pasos para Actualizar tabla 6 para el Cálculo Anual de Impuestos 2023</h2> 


* Entrar a SAIT Nómina.

* Colocarse en el último periodo del año 2023, utilice el botón [->] de la barra de herramientas, ya que puede haber periodos de PTU, Aguinaldos, etc. que por clave se convierten en los últimos.

* Dirigirse al menú Catálogos / Tablas.

* Capturar el No. de Tabla 6 y cambiar el nombre a ISR2023, de igual manera para el campo “Concepto de Entrada” deberá de asignar 249 y en el campo No. de Columnas capturar el valor de: 4. Posteriomente deberás de ingresar los valores descritos:


LimInf | LimSup | CuotaFija| PjeImp 
---|---|---|---
0.01 | 8952.49 |  	0.00  | 1.92
8952.50 | 75984.55 | 171.88  | 6.40
75984.56 | 133536.07 | 4461.94  | 10.88
133536.08 | 155229.80 | 10723.55 | 16.00
155229.81 | 185852.57 | 14194.54 | 17.92
185852.58 | 374837.88 | 19682.13 | 21.36
374837.89 | 590795.99 | 60049.40 | 23.52
590796.00 | 1127926.84 | 110842.74 | 30.00
1127926.85 | 1503902.46 | 271981.99 | 32.00
1503902.47 | 4511707.37 | 392294.17 | 34.00
4511707.38 | En adelante | 1414947.85 | 35.00

* Una vez capturada o Copiada la tabla, en caso de ser necesario de moverse entre los registros de la tabla utilice la tecla [Enter]. Ya finalizada la revisión de la misma, haga clic en el botón [Grabar] y para asegurarse que se haya grabado la tabla de manera correcta, deberá de asegurarse en visualizar el siguiente mensaje que aparece en la parte superior derecha de SAIT Nomina, tal como se muestra “DATOS DE LA TABLA HAN SIDO GUARDADOS”.

* El resultado nos mostrará algo como lo siguiente: 

![IMG](IMG1.JPG)

<h2 class="text-primary">Revisión de Conceptos Anuales para el Calculo de ISPT 2023</h2> 

Una vez actualizada la tabla 6 para el calculo anual de ISPT 2023, es necesario revisar si en nuestro catalogo de conceptos contamos con los siguientes CONCEPTOS dados de alta en nuestro ultimo periodo del 2023, que son los siguientes: 

No. de Concepto |  Descripción |Comentarios Sobre la Fórmula 
---|---|---
245 | ISPT retenido a lo largo del año   | AAConc(51) 
248 | Ingreso Exento Anual   | AAConc(507) 
249 | Ingreso Gravable Anual    | AAConc(506) 
250 | SIN USO    | Ninguna
251 | Impuesto causado anual    | (Con(249)-Tabla(6,1))* Tabla(6,4)/100 + Tabla(6,3) 
252 | SIN USO   | Ninguna
253 | Suma de Subsidio al Empleo    | AcumExpr("(iif(Con(1)>0,Con(153),0)/30.4*((Periodos.FFINAL-Periodos.FINICIO)+1))") 
254 | 	ISPT Anual    | Max( Con(251)-Con(253), 0) 
255 | SIN USO    | Ninguna
256 | SIN USO    | Ninguna
257 | Se hizo calculo anual Si=1 No=2     | 2
258 | ISPT a cargo     | Max ( Con(254) - Con(245), 0) 
259 | ISPT a favor    | Max ( Con(245) - Con(254), 0) 


En caso de que haga falta alguno de estos conceptos que se mencionan en la tabla anterior, los deberá de definir siguiendo el siguiente proceso:

1.Colocarse en el último periodo del año usando el botón [->] de la barra de herramientas, ya que puede haber periodos de PTU, Aguinaldos, etc. que por clave se convierten en los últimos.
    

2.Dirigirse al menú de Catálogos / Conceptos.
    
3.Ingresar el No. de Conceptos.

4.Capturar la descripción y la formula según las especificaciones de la tabla anterior.
    
5.Hacer clic en el botón [Grabar]. 

<h2 class="text-primary"> REPORTES DEL CÁLCULO DE ISR TRABAJADORES EN FORMA INDIVIDUAL  </h2> 


Ya revisada la información de la nómina, es decir el acumulado de sueldos, puede optar por sacar el reporte sin necesidad de generar los procesos anuales, pero debe estar seguro que la información es correcta tanto de sueldos pagados como de retenciones efectuadas. 

Los reportes son los siguientes y para instarlos deberá realizar las siguientes instrucciones:

* **Reporte calculo anual de ispt.rpt:** para instalarlo al sistema deberá hacerlo desde el menú de Utilerías / Recibir Reportes. Para descargar el reporte actualizado de clic [Aquí](calculo-anual-de-ispt-2021.rpt) una vez instalado el reporte para consultarlo deberá ir a Reportes / Otros reportes/ Cálculo anual isr

* **ConcAnio.FXP y ConcAnio.PRG:** estos archivos los deberá colocar dentro del directorio de la empresa.  Para descargarlo de clic [Aquí](ConcAnio.zip)

Dentro de dicho archivo comprimido encontrará los siguientes 2 archivos y son los que colocará dentro del directorio de la empresa:

![IMG](zip.JPG)

<h2 class="text-primary">Pasos para Generar la Declaración Informativa de Sueldos y Salarios 2022 </h2> 

1.Colocarse en el último periodo del año usando el botón [->] de la barra de herramientas, ya que puede haber periodos de PTU, Aguinaldos, etc. que por clave se convierten en los últimos.

3.Para configurar los datos es importante tener a la mano los conceptos de nómina que se tienen definidos. Entrar al menú de Catálogos / Conceptos e imprimirlos.

4.Entrar al menú de Operación / Procesos Anuales / Declaración Informativa de Sueldos:

![IMG](3.jpg)

5.Seleccionar la pestaña “Configurar Datos”.

6.Entre los datos que tenemos que tener especial cuidado encontramos los siguientes:


No. de Concepto |  Descripción |Comentarios Sobre la Fórmula 
---|---|---
1 | Mes Inicial   | iif( Year(Emp.ALTA)==2023,SubStr(Dtos(Emp.ALTA),5,2) , '01') 
2 | Mes Final   | iif( Year(Emp.BAJA)==2023,SubStr(Dtos(Emp.BAJA),5,2) , '12') 
8 | Área geográfica del salario mínimo 1=A 2=B 3=C    | '01' 
9 | Indique si el patrón realizó cálculo anual (1=Si 2=No)   | iif (not Empty(Emp.BAJA) and Emp.BAJA < {^2023-12-01} , 2, iif(Emp.ALTA > {^2023-01-01} , 2, iif(Con(257) >= 2, 1, 2)) )  
15 | Clave de la entidad federativa dónde prestó sus servicios    | 02 = Baja California / 19 = Nuevo León 26 = Sonora 
30 | Declara pagos por separación (1=Si 2=No   | 2
31 | Declara pagos asimilados a salarios (1=Si 2=No)    | 2 
32 | Declara pagos del patron a trabajador (1=Si 2=No)  | 1
**ANEXO DE PAGOS DEL PATRÓN A LOS TRABAJADORES (58-119)** | **Importes Gravados y Exentos de las Percepciones, separadas por grupos** | Usar las fórmulas de: AAGrav(n) AAExcen(n) AAConc(n) **También en caso de ser necesario, puede Sumar varios conceptos a considerar dentro de un mismo rubro, es decir varios conceptos en un mismo renglón. Ejemplos, Conceptos 58 y 59** 
58 | Sueldos, salarios, rayas y Jornales (Gravado)    | **AAGrav(1)+AAGrav(9)** En Donde: Concepto 1 = Salario Normal, Concepto 9 = Vacaciones **IMPORTANTE:** Para asegurarse tomar los conceptos adecuados, puede imprimir el listado completo de **CONCEPTOS**, dirigiéndose al menú: **Catálogos / Conceptos,** y hacer clic en el botón **[Imprimir].** 
59 | Sueldos, salarios, rayas y Jornales (Exento)  | **AAExen(1)+AAExen(9)** En Donde: Concepto 1 = Salario Normal, Concepto 9 = Vacaciones **IMPORTANTE:** Para asegurarse tomar los conceptos adecuados, puede imprimir el listado completo de **CONCEPTOS**, dirigiéndose al menú: Catálogos / Conceptos, y hacer clic en el botón **[Imprimir].** 
116 | Impuesto retenido durante el ejercicio que declara    | 	AAConc(51) 
118 | Saldo a favor determinado en el ejercicio que declara, que el patrón compensará durante el siguiente ejercicio o solicitará su devolución    | Con(259) 
122 | Monto total de ingresos obtenidos por concepto de prestaciones de previsión socia   | Incluir todos los conceptos que se agrupan dentro del plan de previsión social: Por ejemplo lo mas común para el plan de previsión social es: **11:**Bono de Asistencia **12:**Bono de Puntualidad 13: Vale de despensa, Entonces la fórmula es: **AAConc(11)+AAConc(12)+AAConc(13)**
123 | Suma de ingresos exentos por concepto de prestaciones de previsión social |Lo mas común es incluir el vale de despensa: Entonces la fórmula es: AAConc(13) 
126 | Monto del subsidio para el empleo entregado en efectivo al trabajador durante el ejercicio que declara    | AAConc(51) 
128 | ISR conforme a la tarifa anual    |-AAConc(52) 
131 | 	Impuesto sobre ingresos acumulables    | Con(254) 
134 | Monto del subsidio para el empleo que le correspondió al trabajador durante el ejercicio    | -AAConc(52) 

1.Seleccionar la pestaña “Obtener Información”.

2.Haga clic en el botón [Obtener Información].

3.El sistema empezara con la obtención de información.

4.Se muestra el resultado por cada trabajador el cual podrá ser copiado al portapapeles o exportado a Excel.

5.Revise la información exportada a Excel y efectúa la conciliación con contabilidad de sueldos y retención de  ISR

6.Revisar el reporte  realizado en el “Calculo Anual de ISPT”, operación/procesos anuales/calculo anual y enviarlo a Excel. En ese reporte tendrá en forma comprimida la información que se genera en el punto 3 y 4; es decir el saldo a favor o en contra del trabajador.


**NOTA IMPORTANTE: En caso de hacer alguna modificación en la fórmula de los datos a formar, es necesario volver a GENERAR EL ARCHIVO DE LA DECLARATIVA**



