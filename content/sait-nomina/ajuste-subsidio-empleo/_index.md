+++
title = "Ajuste del Subsidio para el Empleo"
description = ""
weight = 3
+++

## CAMBIOS EN LA NÓMINA PUBLICADO EL 14 DE OCTUBRE DE 2019

#### [SECCIÓN I. Ajuste al Subsidio Causado](#ajuste-al-subsidio-causado)
#### [SECCIÓN II. Ajuste al ISR mensual](#ajuste-al-isr-mensual)
______________________________________

<h2 class="text-primary">Sección I</h2> 

## AJUSTE AL SUBSIDIO CAUSADO

A partir de la versión 2020.0.0 de SAIT Nómina se incluye una ventana para el cálculo del subsidio mensualizado, de acuerdo a los lineamientos de la Guía de llenado del complemento de nómina, establecidos en el Apéndice 7, (página 97). Para descargar la guía de clic [aquí](GuiaNomina11102019.pdf)

![Imagen1](1.JPG)


## MENÚ

* [1.Actualizar Version](#actualizar-version)
* [2.Habilitar Permiso](#habilitar-permiso)
* [3.Proceso para realizar ajuste](#proceso-para-realizar-ajuste)
* [4.Capturar cuenta contable de conceptos](#capturar-cuenta-contable-de-conceptos)

______________________________________

## Actualizar version 

<h4 class="text-danger">NOTA: Es muy importante ANTES de colocar la última versión verificar el Concepto 155 ya que una vez actualizado este concepto queda bloqueado para su modificación, el concepto debe estar de la siguiente manera: </h4>

<h5 class="text-primary">IIF(EmpPer.REGIMEN=='02',Con(153)/30.4*Con(131),(0)) </h5>  

![155](155.png)

Para actualizar la versión del sistema siga las siguientes instrucciones:

1.Descargar el archivo desde la siguiente liga:

Sait Nómina: https://sait.mx/download/2023/saitnom.exe

2.Es necesario Guardar el archivo dentro de la carpeta de SAIT. Para esto debe de estar cerrado el Sistema.

3.Si el archivo se guardó automáticamente en Descargas, hay que seleccionar el archivo, dar clic derecho y **[Copiar]**, o CTRL C.

![Imagen1](2.png)

Seguir con el proceso de actualización descritos en el siguiente link:

https://ayuda.sait.mx/sait-basico/1-instalacion-y-activacion-licencias/b-actualizar-version-de-sistema/


## Habilitar Permiso

 Al colocar la nueva versión de nómina en el menú de Operación aparecerá esta nueva opción la cual viene deshabilitada por default, por lo cual es importante habilitar a los niveles de usuarios que tendrán acceso. 

![Imagen1](3.png)

 Para habilitarlo deberá entrar a: 

**Utilerías / Grupos de Usuarios**

Y palomear la opción de Ajuste del Subsidio para el Empleado

Clic en **[Grabar]**

![Imagen1](4.png)

## Proceso para realizar ajuste

De acuerdo a la Guía de llenado, cuando se realicen pagos por periodos menores a un mes, es decir semanal, decenal, catorcenal o quincenal, deberá en la último periodo del mes determinar el subsidio causado  de acuerdo a las percepciones mensuales y en caso en que se haya otorgado subsidio en exceso, deberá realizarse un ajuste.

Los cambios de  SAIT  Nómina para cumplir con lo establecido por el SAT son los siguientes:

a) Nuevos conceptos del 901-903 (deducciones), del 911-913 (otros pagos) [clic aquí](#nuevos-conceptos).

**Es importante tener en cuenta que si usted ya utilizaba estos conceptos, deberá crearlos nuevamente con una clave distinta**.

b) Conceptos que ya no podrás modificar es decir fijos [clic aquí](#concepto-155).

c) Nueva ventana que muestra los Acumulados para determinar el Ingreso  Mensual

d) Revisión del cálculo antes de procesar

Dirigirse a Operación / Ajuste del Subsidio al Empleo, dar clic en **[Consultar]** 

Aparecerá la siguiente ventana en donde mostrará la siguiente información en donde:

## Pasos a verificar antes del ajuste:

1.Verificar que los periodos incluidos (parte superior derecha) sean los que fueron efectivamente pagados en el mes.

![Imagen1](ventana.JPG)

Para verificar la información diríjase a Catálogos / Periodos y corrobe la columna de fechas de pago:

![Imagen1](periodos.JPG)

**NOTA: Para realizar el ajuste sólo se consideran nóminas de periodos ordinarios.**

2.Revisar los días pagados:

![Imagen1](diaspagados.png)

<h4 class="text-danger">Es muy importante mencionar que el concepto 131 días pagados al elaborar cada nómina debe de coincidir con los efectivamente laborados, ya que es un concepto que determina el subsidio causado o bien el ISR a retener.</h4>

3.Revisar pre-cálculo antes de Procesar

Para verificar la cantidad que se ajustará deberá dar clic en el botón de **[Ver Cálculo]** en donde se generará un archivo de excel con el desglose de las cantidades.

![Imagen1](calculo.png)

El archivo de excel se generá de la siguiente manera:

![Imagen1](excel.JPG)

4.Procesar el Ajuste 

Al dar clic en el botón de **[Procesar]** las columnas de los Ajustes al Subsidio serán los importes que se incluirán en el recibo de la nómina del periodo en que se encuentra.

Es importante mencionar que al Procesar las cantidades que se reflajarán en el recibo de cada trabajador será:

* El ISR del periodo activo menos el subsidio que se muestra en la columna de ajuste AUMENTAR.

* Y en el caso de los que tengan ajuste a DISMINUIR el importe que se reflejará en el recibo será el ISR del periodo activo y un concepto por separado de Ajuste a ISR del importe que se encuentra por disminuar.

<h3 class="text-danger">NOTA: Sí usted no descuenta ISR por ser Salarios Mínimos NO DEBERÁ PROCESAR LOS AJUSTES.</h3> 

![Imagen1](ajuste.JPG)

<h3 class="text-danger">Es importante comentar que si usted no desea procesar estos ajustes o requiera hacerlos solo a ciertos empleados deberá de hacerlo de manera manual, agregando los siguientes conceptos:</h3> 


![conceptos](conceptos.png)


Al procesar el ajuste, el sistema creará automáticamente los conceptos necesarios [dar clic aquí](#nuevos-conceptos) y el recibo de nómina quedará de la siguiente forma:


![Imagen1](reciboajustado.JPG)

<h4 class="text-danger">Estos conceptos se estarán creando cada vez se procese el ajuste y esto es por periodo.</h4>

Para verificar la cantidad en el ISR retenido (solo en casos que no haya Subsidio Causado en el periodo activo) diríjase a:

Reportes / Otros Reportes

![Imagen1](isr.png)

![Imagen1](reporteisr.png)

En caso de no contar con el reporte de clic  [aquí](calculo-de-isr.rpt)

Para verificar la cantidad en el ISR retenido (en caso de que sí haya Subsidio Causado en el periodo activo) solo será visible después de ajustar

![subdidio](subsidio.JPG)

![subsidiorep](subsidiorep.JPG)

## CAPTURAR CUENTA CONTABLE DE CONCEPTOS

Para efectos del timbrado usted deberá capturar la cuenta contable de los siguientes conceptos:

52 - Subsidio al Empleo (este concepto siempre se declarará en 0 por lo tanto siempre debe de llevar cuenta contable).       

155 - Subsidio Causado (este concepto siempre se declarará en 0 por lo tanto siempre debe de llevar cuenta contable).       

Las cuentas de los siguientes conceptos 901,902,903,911,912 y 913 afectan directamente la cuenta contable del ISR retenido.

![conceptos](cuentas-conceptos.jpg)

1.Ingrese a Utilerías / Configurar Cuentas Contables / Agregar

2.Aparecerá la ventana en dónde deberá ingresar la información solicitada:

* **Clave** del concepto a clasificar (presione [?] para realizar una búsqueda por palabras).

* **Aplicación por**: presione barra espaciadora para cambiar de opción (si es una opción diferente a Empresa el sistema pedirá clave de trabajador o clave del departamento según sea el caso). **NOTA: Si usted no maneja SAIT ERP, la opción que elegirá será por Empresa.**

* **Generar:** Seleccione Cargo (percepciones) o Abono (deducciones) usando barra espaciadora.

* **Cuenta Contable:** Ingresaremos la cuenta contable, si usted utiliza SAIT ERP deberá utilizar la cuenta con todos sus niveles ejemplo: 5001-001-184, en caso de que usted no use SAIT ERP deberá ingresar la cuenta con el primer nivel solamente, ejemplo: 600-001

3.Clic en Agregar.

![cuentasc](cuentac.JPG)

4.Listo la cuenta ha sido agregada y clasificada correctamente.

## NUEVOS CONCEPTOS

Se crearán 6 nuevos conceptos, es importante tener en cuenta que si usted ya utilizaba estos conceptos al colocar la nueva versión y ajustar el subsidio éstos se van a sobrescribir para crear los nuevos conceptos.

<h4 class="text-primary">NOTA: Si usted ya cuenta con el periodo creado, estos conceptos se crearán DESPUÉS de realizar el ajuste. No es necesario crearlos manualmente. </h4>

* 901 – 903 Deducciones

* 911 – 913 Otros Pagos

![conceptos](conceptos.png)

## CONCEPTO 155 

Concepto 155 quedará bloqueado para efectos del cálculo, debido a que los importes generados en cada nómina son validados en cada timbrado; por lo tanto la fórmula no puede ser modificada, ya que el subsidio causado va en función de los días pagados.

Adicionalmente de que el ajuste final depende de las cantidades consideradas en cada nómina.

Fórmula del concepto 155: 

<h5 class="text-primary">IIF(EmpPer.REGIMEN=='02',Con(153)/30.4*Con(131),(0)) </h5>  

![155](155.png)

## MOVIMIENTOS CAPTURADOS

Al momennto de procesar el Ajuste el sistema captura automáticamente en los movimientos de cada trabajador los nuevos conceptos.

Por lo que si por error procesó los Ajustes, deberá entrar a cada trabajador y borrar TODOS los movimientos (51,52,155,901-903,911-913).

![Imagen1](movimientos.JPG)

<h2 class="text-primary">Sección II</h2> 

## AJUSTE AL ISR MENSUAL

A partir de esta nueva versión se podrá ajustar el importe del ISR retenido, efectuando un recálculo mensual de las percepciones, este procedimiento es de suma importancia cuando los ingresos varían considerablemente cada semana.

![ajustarisr](ajustarISR.JPG)

a)	Ajuste de ISR (mensual) automático o por selección (usted podrá palomear los empleados a los que se desea ajustar).



![seleccionar](seleccionar.JPG)

b)	Revisión del procedimiento del  cálculo de ISR antes de procesar

Para verificar la cantidad de ISR deberá dar clic en el botón de **[Ver Cálculo]** en donde se generará un archivo de excel con el desglose de las cantidades.  

![ajustarisr](excelISRAJUSTE.JPG)

c)	Creación automática de los conceptos necesarios para hacer el Ajuste del ISR.

Al procesar el ajuste, el recibo quedará de la siguiente manera:

![seleccionar](reciboajustadoISR.JPG)

