+++
title = "Actualización de tablas 2025 y Salario Mínimo"
description = ""
weight = 12
+++

Subsidio al empleo y tarifas de retención ISR para 2025 y Actualización del Salario Mínimo en el sistema de nómina SAIT 

Modificaciones sobre el tema publicadas en el DOF del 31 diciembre de 2024, https://bit.ly/3PiERA7

1.**Aumento del Porcentaje de Subsidio:** El porcentaje del subsidio para el empleo se modifica del 11.82% al 13.8% de la Unidad de Medida y Actualización.

2.**Nuevo Límite de Ingresos:** Se aumenta el límite de ingresos para beneficiarse del subsidio, que pasa de $9,081.00 en 2024 a $10,171.00 en 2025.

3.**Porcentajede Subsidio para enero de 2025:** Durante enero de 2025, se aplicará un porcentaje temporal de 14.39 % para el cálculo del subsidio, ya que el valor de la UMA actualizado comenzará a regir en febrero.  **El decreto especifica que el monto mensual de subsidio será de $475.00,  equivalente al 14.39% del valor de la UMA vigente en 2024: $108.57 * 14.39% * 30.4 = $474.95**

<h2 class="text-primary">Salarios 2025</h2>  

Para realizar la actualización de nuestros salarios hay que entrar al concepto 101 y 102 y actualizar, OJO es importante que identifiques tu zona para realizar dichos cambios.

![1](salarios.png)

{{%alert success%}}Los pasos para modificar los conceptos números 101 y 102 son los siguientes:{{%/alert%}}

1. Entrar a SAIT Nomina.

2. Colocarse en un período de nómina que aún no se ha pagado.

3. Dirigirse al menú: **Catálogos / Conceptos**.

4. En el campo: **CLAVE** capturar 101 y presionar la tecla **[Enter]**.

5. Posicionarse en el campo: **FORMULA** y capturar el salario mínimo diario según la zona que corresponda

6. Haga clic en el botón: **[Grabar]**. 

7. Realizar los mismos pasos 4 y 5, para el **Concepto 102**. 

8. Listo, nuestro sistema de nómina 2025, ya está actualizado con el salario mínimo diario de la zona.

{{%alert success%}}Si ya generaste tu nómina de la primer semana de enero no te preocupes, recuerda que ahora el cálculo de los impuestos es con base al valor del UMA.{{%/alert%}}

<h2 class="text-primary">TABLA 1 ISR 2025</h2> 

Como ustedes saben, año con año en el diario oficial de la federación se publican los nuevos parámetros a incluir en las tablas mensuales para el cálculo de ISPT de nómina, con el objetivo de integrarlos a las aplicaciones de nómina.

{{%alert success%}}La tabla ISR 2025 no sufrió ningún cambio en relación a la del año 2024, sin embargo la compartimos de nueva cuenta para que las verifique en su sistema de SAIT Nómina{{%/alert%}}

En SAIT NÓMINA es necesario capturar las tablas cuando existan modificaciones, las tablas no se cambian automáticamente al actualizar la versión.

#### TABLA 1

La tabla 1 correspondiente al ISPT o ISR Mensual es la siguiente:

**No de Tabla:** 	1

**Nombre:**	ISPTM

**No Columnas:**	4	

**Conc Entrada:**	149		

LimInf|LimSup|CuotaFija|PjeSExced
---|---|---|---
0.01 | 746.04  | 0.00 | 0.0192
746.05 | 6332.05 |	14.32 |	0.064
6332.06 | 11128.01 |	371.83 | 0.1088
11128.02 |	12935.82 |	893.63 | 0.16
12935.83 |	15487.71 |	1182.88 |	0.1792
15487.72 |	31236.49 |	1640.18 |	0.2136
31236.50 |	49233.00 |	5004.12 |	0.2352
49233.01 |	93993.90 |	9236.89 |	0.30
93993.91 |	125325.20   | 22665.17  | 0.32
125325.21 | 375975.61  | 32691.18  | 0.34
375975.62 | En adelante | 117912.32 | 0.35


<h2 class="text-primary">TABLA 3 SUBSIDIO AL EMPLEO 2025</h2> 

#### TABLA 3 

La tabla 3 correspondiente al subsidio al empleo, sufrió un cambio respecto a los cambios publicados en mayo del 2024, quedando de la siguiente manera

**No de Tabla:** 	3

**Nombre:** SubEmpMens

**No Columnas:**	3

**Conc Entrada:**	149

LimInf|LimSup|SubEmpl|
---|---|---|
0.01 | 10171.00 | 475 |
10171.00 | 9999999999 | 0.00 | 

<h4 class="text-danger">De acuerdo a la publicación del DOF en febrero de 2025 habrá un cambio en el tope cuando se publique el nuevo valor de la UMA</h4>

![1](subsidio.png)



<h2 class="text-primary">Como Modificar las tablas en SAIT Nómina</h2> 

<h4 class="text-danger">Este proceso siempre es el mismo y lo único que cambia son los valores.</h4>

Antes de modificar las tablas 1 ISPT y 3 Subsidio al Empleo, deberás asegurarte de estar posicionado en un periodo de nómina que aún no se ha pagado.

1.Vaya al menú de Catálogos / Tablas, se visualiza de la siguiente ventana:

![1](1.jpg)

2.Capturar el número de tabla 1 y dar un [Enter]. Se visualiza la siguiente ventana con los valores actuales:

![1](valores-anteriores.jpg)

3.Deberá seleccionar únicamente los valores de la tabla 1 (es decir sin los encabezados) y posteriormente copiarlos.

LimInf|LimSup|CuotaFija|PjeSExced
---|---|---|---
0.01 | 746.04  | 0.00 | 0.0192
746.05 | 6332.05 |	14.32 |	0.064
6332.06 | 11128.01 |	371.83 | 0.1088
11128.02 |	12935.82 |	893.63 | 0.16
12935.83 |	15487.71 |	1182.88 |	0.1792
15487.72 |	31236.49 |	1640.18 |	0.2136
31236.50 |	49233.00 |	5004.12 |	0.2352
49233.01 |	93993.90 |	9236.89 |	0.30
93993.91 |	125325.20   | 22665.17  | 0.32
125325.21 | 375975.61  | 32691.18  | 0.34
375975.62 | En adelante | 117912.32 | 0.35

4.Posicionarse nuevamente en SAIT Nómina y hacer un clic en la primera columna con valor de la tabla y posteriormente deberá de dar clic en el botón de PEGAR y notará que se pegarán los valores copiados.

![1](actualizada.jpg)

5.Para grabar los nuevos valores, deberá hacer clic en el botón [Grabar].

6.Listo, usted ha actualizado los valores de la tabla.

**El proceso es el mismo para la tabla 3 del Subsidio al Empleo**


