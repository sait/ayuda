+++
title = "Actualizar Versión de Sistema Nómina"
description = ""
weight = 2
+++

### **Indicaciones**:

Antes de hacer otro proceso, lo primero al actualizar es realizar un respaldo de la información, para ello deberá digirse a Utilerías / Respaldar Información

![IMG](respaldar.PNG)

1.Para verificar su vérsión actual ingrese en el Menú a la sección de:

Ayuda / Acerca de Sait /

Donde aparecerá la siguiente ventana con la información.

![IMG](versionom.PNG)

2.Posteiormente deberá dar clic en el siguiente enlace para bajar la última versión de SAIT Nómina

**Sait Nómina:** [https://sait.mx/download/2025/saitnom.exe](https://sait.mx/download/2025/saitnom.exe)

Es necesario guardar el archivo ejecutable en descargas o cualquier otra carpeta que usted desee. 

Una vez guardado es necesario seleccionar dicho archivo y dar clic derecho y **[Copiar]**, o CTRL C.

![IMG](copiar.png)

3.Identificar el directorio del sistema y abrir la carpeta para pegar nuevo ejecutable

Para identificar la carpeta en donde está la instalación de SAIT Nómina, para ello deberá dar clic derecho en el ejecutable y seleccionar la opción Abrir la ubicación del archivo:

![IMG](ubicacion.png)

Y presionará CTRL V **[Pegar]** o Clic derecho dentro de la carpeta y dar clic en **[Pegar]**.

Como este archivo ya existe, hay que reemplazarlo.

![IMG](reemplazar.png)


4.Deberá dirijirse al escritorio abrir el sistema.

Se mostrará la siguiente ventana y se deberá inhabilitar la opción de **[Preguntar siempre antes de abrir este archivo]**y después dará clic en el botón de **[Ejecutar]**.

![IMG](preguntar.PNG)


4.Seleccionar la empresa y presionar el botón de **[Accesar Empresa]**.

{{%alert info%}} En algunos cambios de versiones el sistema pide una nueva activación de licencia, por ello es necesario cuente con su contrato y contraseña para estos casos. {{%/alert%}}

![IMG](seleccionar.PNG)

5.Dar clic en **[Continuar]**.

![IMG](continuar.PNG)

6.Para continuar es necesario Actualizar la base de datos del sistema.

Dar clic en **[Aceptar]**.

![IMG](actualizar.PNG)

7.Presionar el botón de **[Actualizar base de datos]**.

![IMG](actualizar2.PNG)

8.Una vez que termine el proceso, le aparecerá el siguiente mensaje y dará clic en **[Aceptar]**.

![IMG](actualizar3.PNG)

9.Presionar el botón de **[Cerrar]** una vez que se haya finalizado el proceso de actualización.

10.Listo, su versión de nómina ha sido actualizada.