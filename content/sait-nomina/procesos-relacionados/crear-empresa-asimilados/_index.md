+++
title = "Creacion de Empresa Asimilados"
description = ""
weight = 2
+++

En caso de que su empresa se vea en la necesidad de crear una empresa para sus empleados asimilados deberá seguir las siguientes instrucciones:

## I. Crear una nueva empresa especial para ASIMILADOS

Dará clic en su ícono de SAIT Nómina y posteriormente dará clic en **Catálogo de Empresas**

![IMG](1.png)

Después dará clic en **Crear Nueva Empresa**

![IMG](2.png)

Posteriormente deberá agregar el nombre con la que vamos a identificar la empresa y dará clic en **Crear Archivos**

![IMG](3.png)

Dará clic en **Sí** para confirmar la creación de la empresa

![IMG](4.png)

Y podrá ver que a su catálogo de empresa se agregó la recién creada empresa de Asimilados, dará clic en **Acceder Empresa**

![IMG](5.png)

El sistema le pedirá actualizar la base de datos y dará clic en **Aceptar**

![IMG](6.png)

Damos clic en el botón de **Actualizar bases de Datos**

![IMG](7.png)

Esperamos a que se complete el proceso y cuando haya terminado el sistema le mostrará el siguiente mensaje y daremos clic en **Aceptar**

![IMG](8.png)

Daremos clic en **Cerrar** para finalizar

![IMG](9.png)

Y ahora deberemos de completar los datos fiscales de la empresa, una vez haya llenado los campos dará clic en **Aceptar**

![IMG](10.png)

## II. Establecer el periodo de nómina que vaya a manejar la empresa (Semanal, Quincenal y Mensual)

Establecemos el periodo según el manejo de la empresa.

![IMG](crear.PNG)

Damos clic en **Si** para confirmar

![IMG](12.PNG)

Clic en **Si** para finalizar

![IMG](13.PNG)

## III. Una vez creado el directorio, borrar la tabla 3 (Subsidio al Empleo)

Puesto que para este tipo de nómina el subsidio al empledo no aplica, deberá de borrarla, para hacerlo debe dirigirse a Catálogos / Tablas / Ingresará el número 3 para que cargue la tabla y dará clic en **Borrar**

![IMG](borrar-tabla.png)

Dará clic en **Sí** para confirmar y listo.

![IMG](confirmar-borrar-tabla.PNG)

## IV. Inactivar los conceptos IMSS y SUBSIDIO AL EMPLEO en nuestro catalogo de conceptos

Deberá inactivar los conceptos 52 y 53, para hacer este proceso deberá dirigirse a Catálogos / Conceptos
/ Ingresará el número 52 y dará clic en **Modificar** se posicionará en el campo de Status y con la barra espaciadora de su teclado cambiará su status a INACTIVO como se muestra en la siguiente imagen:

![IMG](52.png)

Posteriormente dará clic en **Grabar** y listo.

Repita este proceso para el concepto 53.

## V. Generar la Nómina y en catálogo de trabajadores colocar en el tipo de régimen de contratación ASIMILADOS A SALARIOS

Para modificar el tipo de régimen de contratación del empleado deberá dirigirse a Catálogos / Trabajadores / Ubicar el empleado a modificar y dar clic en el botón de **Modificar** posteriormente deberá elegir la opción deseada y dará clic en **Grabar**

![IMG](contrato.png)

## VI. Una vez realizado todos los cálculos de percepción y deducciones, hacer el timbrado de la nómina


