﻿+++
title = "Retención del ISR sobre finiquito"
description = ""
weight = 5
+++

Cuando se obtengan ingresos por concepto de primas de antigüedad, retiro e indemnizaciones u otros pagos, por separación, se calculará el impuesto anual de acuerdo al art 95 y 96 de la LSR (Artículo 95 y Artículo 96).

Tras reducir la parte exenta del finiquito o indemnización laboral, se deberá efectuar una retención al trabajador, la cual tendrá el carácter de pago provisional.

En el caso de la indemnización laboral (prima de antigüedad e indemnizaciones), al ingreso gravado se le aplicará la siguiente mecánica (artículo 96, sexto párrafo, de la LISR):

**Art. 95 y 96 ISR**

{{%alert success%}}Ingreso total gravado por prima de antigüedad, indemnización u otros pagos por separación{{%/alert%}}

* (×) Tasa.

* (=) ISR a retener por prima de antigüedad, indemnización u otros pagos por separación.

{{%alert success%}}Donde la tasa a aplicar es:<br>ISR del último sueldo mensual ordinario (artículo 96 de la LISR){{%/alert%}}

* (÷) Último sueldo mensual ordinario

* (=) Proporción.

* (×) Constante 100.

* (=) tasa.

### Instalar el reporte ISR Art 96

* Descargar el archivo: Reporte_art96.rpt [clic aquí para descargar](Reporte-art96.rpt)

* En SAIT Nómina, ir al menú de: **Utilerías / Recibir Reportes**.

* Seleccionar el archivo que se acaba de descargar.

![IMG](imagen2.PNG)

* Dar clic en: **[Recibir]**.

![IMG](imagen7.PNG)

* Y listo, el reporte ha sido agregado a su sistema.

### Consultar el reporte ISR Art 96

Para hacer uso de este reporte, deberá realizar lo siguiente:

* En SAIT Nómina, ir al menú de:  **Reportes / Otros Reportes**.

* Seleccionar el reporte ISR Art96.

* Dar clic en **[Imprimir]**.

![IMG](imagen4.PNG)

* Dar clic en **[Pantalla]**.

![IMG](imagen6.PNG)

* Se deberá mostrar el siguiente formato con la información del ISR del trabajador.

![IMG](imagen5.png)


