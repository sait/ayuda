﻿+++
title = "Cómo realizar nómina de pago de aguinaldos"
description = ""
weight = 11
+++

El Aguinaldo es una gratificación obligatoria que las empresas dan a sus trabajadores año con año de acuerdo con el articulo 87 la Ley Federal del Trabajo, la cual se debe recibir antes del 20 de diciembre y su monto mínimo es de 15 días de salario o en su caso, la parte proporcional.

En SAIT Nómina usted puede cumplir con esta obligación, para lo cual existen dos formas de realizar la nómina del aguinaldo:

• **Realizar el cálculo automático:** en esta opción solo ingresamos los días a pagar al trabajador y el sistema calculará el importe que se dará al trabajador.

• **Realizar el cálculo manual:** esta opción se aplica cuando ya se tiene el importe a pagar al trabajador y solo lo capturamos en el recibo.

El proceso del aguinaldo es el siguiente:

* [Verificar conceptos](#verificar-conceptos)
* [Crear periodo especial para pago del aguinaldo](#crear-periodo-especial-para-pago-del-aguinaldo)
* [Desactivar conceptos](#desactivar-conceptos)
* [Proceso automático](#proceso-automatico)
* [Proceso manual](#proceso-manual)

## verificar conceptos

Antes de realizar el pago del aguinaldo revise que estén definidos los siguientes conceptos en el catalogo:

**Concepto de Aguinaldo**

1. Ir a Catálogo / Conceptos
2. Seleccionar el concepto **4** y dar clic en Modificar
3. Deberá aparecer la percepción Aguinaldo
4. Verifique la información esté como en la imagen y en caso de ser distinta, modificarlo y dar clic en Grabar

![IMG](1.jpg)

**Concepto Días de Aguinaldo**

1. Ir a Catálogo / Conceptos
2. Seleccionar el concepto **133** y dar clic en Modificar
3. Deberá aparecer la percepción Días de Aguinaldo
4. Verifique la información esté como en la imagen y en caso de ser distinta, modificarlo y dar clic en Grabar

![IMG](2.jpg)

**Concepto Días a Pagar**

1. Ir a Catálogo / Conceptos
2. Seleccionar el concepto **131** y dar clic en Modificar
3. Deberá aparecer la percepción Días de Aguinaldo
4. Verifique la información esté como en la imagen y en caso de ser distinta, modificarlo y dar clic en Grabar

![IMG](3.jpg)

## crear periodo especial para pago del aguinaldo

Por lo general para realizar el pago del aguinaldo se genera un periodo espacial, ya que no se
desea que en el mismo recibo de nómina se junte con el pago semanal, quincenal, mensual.

Para generar el periodo especial siga las siguientes instrucciones:

1. Ir a Utilerías / Crear nuevo periodo
2. En la clave, se acostumbra colocar el número de periodo seguido de una letra A para indentificar rápidamente ese periodo entre los demás
3. En la fecha definir 1-dic al 31-dic
4. En la descripción poner: Aguinaldos 2023
5. Seleccionar en tipo de nómina: Extraordinaria
6. Seleccionar en periodicidad del pago: Otra periodicidad
7. Dar clic en Crear período

![IMG](4.jpg)

1. Ir al menú de Catalogo / Conceptos
2. Escribir la clave del concepto de días a pagar: 130
3. Se presentan los datos de la percepción
4. En status, con la barra espaciadora, cambiar a Inactivo

## desactivar conceptos

1. Ir al menú de Catalogo / Conceptos
2. Escribir la clave del concepto a inactivar
3. Seleccionar el concepto y dar clic en Modificar
4. En el campo de Status, con la barra espaciadora, cambie a Inactivo
5. Clic en Grabar

![IMG](5.jpg)

Conceptos a inactivar:

* Concepto 1: Salario normal

* Concepto 53: IMSS

*  Concepto 52: Subsidio al Empleo                      

*  Concepto 155: Subsidio causado         

Si en el recibo de nómina aparecen otros conceptos como bonos de puntualidad, asistencia, infonavit, etc, también deberá inactivarlos.


## proceso automático

Esta opción podrá utilizarla para capturarle a los trabajadores los días de aguinaldo correspondientes y de esta manera, nos realizará el cálculo automático del importe a otorgar al trabajador

1. Ir al menú Operación / Procesos anuales / Aguinaldos

![IMG](6.jpg)

2. De manera opcional, si desea pagar más de los 15 días mínimos que marca la ley puede editar la columna Días

![IMG](7.jpg)

3. Se recomienda dejar el concepto 133 (Días de aguinaldo) donde se capturarán los días de aguinaldos ya que es el concepto que viene por default, si usted utiliza otro, deberá capturarlo.

4. Haga clic en el botón [Generar].

5. Vaya a utilerías / Generar CFDI

6. La nómina ya está lista para timbrarse.

![IMG](10.jpg)

## proceso manual

Esta opción podrá utilizarla cuando el cálculo lo realiza manualmente y solo desea indicarle al sistema el importe a pagar por cada trabajador.

1. Ir al menú Operación / Captura Masiva

2. En concepto poner el concepto: 4 (Aguinaldo)

4. En tipo seleccione con la barra espaciadora: solo esta nomina

5. En la columna de Trabajador [F2] escriba el número de trabajador

6. En la columna de Cantidad, escriba el importe a pagar de aguinaldo al trabajador

7. Haga clic en el botón <Grabar>

![IMG](11.jpg)

8. Vaya a utilerías / Generar CFDI

9. La nómina ya está lista para timbrarse.

![IMG](12.jpg)



