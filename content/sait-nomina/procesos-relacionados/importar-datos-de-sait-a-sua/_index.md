﻿+++
title = "Cómo importar datos de SAIT Nómina a SUA"
description = ""
weight = 14
+++

El proceso de traspasar información desde el sistema SAIT Nómina a SUA (Sistema Único de Autodeterminación) ofrece importar ciertos tipos de información:

* Trabajadores (aseg.txt)

* Movimientos (movt.txt)

Para que dicho proceso de importación/exportación se realice correctamente, es importante considerar los datos de la empresa bajo que se está trabajando, que a continuación se presentan: 

## **Puntos a considerar dentro de Sait Nómina**

•	Dentro de Utilerías / Configuración General Del sistema / Datos de la Empresa, agregar todos los campos correspondientes a la empresa bajo que se estará trabajando, principalmente el campo de Reg. IMSS.

![IMG](1.png)

•	Ingresar todos los datos de los trabajadores  (Catálogos / Trabajadores), dichos datos tendrán que concordar a los que se presentaran en SUA, o si esta es una modificando, tendrán que concordar los demás datos que no han sido modificados.

![IMG](2.png)

•	Al terminar de agregar los trabajadores en el paso anterior dentro de SAIT, en la ventana de  IMSS / Altas será necesario ingresar los datos referentes a su fecha de ingreso, salario normal e integrado

![IMG](3.png)

## **Puntos a considerar dentro de SUA**

•	Dentro de Actualizar / Patrones debe de estar registrado el patrón en el que se va a trabajar. Es importante considerar la prima de riesgos de trabajo.

![IMG](4.png)

•	En caso de que ya exista dicho patrón, solamente seleccionarlo de Actualizar / Patrones / clic en **[Seleccionar]**

![IMG](5.png)

•	Se deben tener capturados  los salarios mínimos desde la fecha de alta del patrón a la fecha en la que se está trabajando,  dentro de Actualizar / Salarios Mínimos

![IMG](6.png)

## **Funcionamiento**

#### En SAIT Nómina

1.	Clic en IMSS / Transferencia al Sistema SUA

2.	Seleccionar el mes y el año que se desea exportar

3.	Si se desea solo traspasar los trabajadores, clic en  Trabajadores Nuevos **[Generar archivo]**

4.	Seleccionar la ruta donde se guardara el archivo aseg.txt

5.	Si se desea traspasar todos los movimientos, marcar las casillas de los datos que solo se quiere compartir

6.	Clic en Movimientos **[Generar archivo]**

7.	Seleccionar la ruta donde se guardara el archivo movt.txt

#### Dentro de SUA

1.	Clic en Actualizar / Trabajadores 

2.	Clic en la pestaña de **[Importar datos]**

3.	Seleccione el tipo de Importación que desee

o	Si se importara los trabajadores, clic en **[Trabajadores (aseg.txt)]**

o	Si se importara los movimientos, clic en **[Movimientos (movt.txt)]**

4.	Clic en **[Examinar> para buscar la ruta del archivo generado por SAIT Nómina]**

5.	Clic en **[Importar]**

## **Errores comunes (dentro de SUA)**

![IMG](7.png)