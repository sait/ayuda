﻿+++
title = "Emisión de 2 recibos en finiquitos"
description = ""
weight = 99
+++

En las nóminas por finiquitos deberán separar los ingresos por tipo, es decir:

a)	Deberá elaborar una nómina por las percepciones ordinarias (Sueldos, compensaciones, bonos, etc.)

b)	Y otra nómina extraordinaria por las percepciones  
extraordinarias (Aguinaldo, Indemnizaciones, Prima de Antigüedad, etc).

Anteriormente se podría emitir y timbrar un recibo de finiquito de la siguiente forma:

![seleccionar](aguinaldo.JPG)

Ahora deberá usted hacer 2 recibos:

Crear un nuevo periodo Tipo Extraordinaria para timbrar las percepciones extraordinarias.

![seleccionar](crearperiodo.JPG)

En donde quedará solo la percepción extraordinaria

![seleccionar](aguinaldosolo.JPG)