+++
title = "Configurar Cuentas Contables en Nómina"
description = ""
weight = 4
+++

<h2 class="text-primary"> I.Introducción </h2> 

A partir de la versión 2019.1, SAIT Nómina cuenta con la opción de que en el XML se informe la cuenta contable y para esto se agregó la opción de “Configurar cuentas contables”.

El agregar las cuentas contables no significa que se hará contabilización de los conceptos, solo es para efectos de cumplir con lo obligación marcada en la Guía y en el Estándar técnico de nómina:


![IMG](1.png)

En esta sección usted deberá clasificar todos los conceptos que usted utiliza al momento de timbrar. 

Ejemplo: salario normal, séptimo día, horas extras, ISR, IMSS, descuentos Infonavit, etc.

Para saber cómo se clasificarán dichos conceptos es necesario que usted tenga bien identificado cómo las tiene en contabilidad, es decir, por empresa o por empleado, esto es porque al momento de clasificar un concepto usted tendrá la opción de clasificar por Trabajador, Departamento o por Empresa.

Para acceder a dicha función dentro de SAIT Nómina diríjase a:

Utilerías / Configurar Cuentas Contables

Aparecerá la siguiente ventana en donde usted podrá, Agregar, Modificar o Eliminar una Cuenta Contable, y en caso de ser necesario podrá exportar a Excel dicho catálogo.


![IMG](2.png)

<h2 class="text-primary"> II.Agregar Cuenta Contable </h2> 

1.Ingrese a Utilerías / Configurar Cuentas Contables / Agregar

2.Aparecerá la ventana en dónde deberá ingresar la información solicitada:

* Clave del concepto a clasificar (presione [?] para realizar una búsqueda por palabras).

* Aplicación por: presione barra espaciadora para cambiar de opción (si es una opción diferente a Empresa el sistema pedirá clave de trabajador o clave del departamento según sea el caso)

* <h4 class="text-danger">NOTA: Si usted no maneja SAIT ERP, la opción que elegirá será por Empresa</h4> 
* Generar: Seleccione Cargo (percepciones) o Abono (deducciones) usando barra espaciadora.

* Cuenta Contable: Ingresaremos la cuenta contable, si usted utiliza SAIT ERP deberá utilizar la cuenta con todos sus niveles ejemplo: 5001-001-184, en caso de que usted no use SAIT ERP deberá ingresar la cuenta con el primer nivel solamente, ejemplo: 600-001

3.Clic en Agregar.

![IMG](3.png)

4.Listo la cuenta ha sido agregada y clasificada correctamente.
 


<h2 class="text-primary"> III.Modificar Cuenta Contable </h2> 

1.Ingrese a Utilerías / Configurar Cuentas Contables 

2.Seleccione la cuenta y de clic en Modificar

3.Realice los cambios deseados

![IMG](4.png)
 
5.Clic en Modificar.

6.Listo la cuenta ha sido Modificada de manera correcta.


<h2 class="text-primary"> IV.Eliminar Cuenta Contable </h2> 


1.Ingrese a Utilerías / Configurar Cuentas Contables 
2.Seleccione la cuenta y de clic en Eliminar
3.Confirme sea la cuenta correcta a eliminar
4.De clic en Eliminar

![IMG](5.png)
 
5.Confirme la acción dando clic en Sí

![IMG](6.png)
 
6.Listo la cuenta ha sido eliminada.

<h2 class="text-primary"> VI.Asignar Cuenta Contable (52-Subsido al Empleo) </h2> 

Es importante que asigne una cuenta contable al concepto 52 (subsidio al empleo) en el catálogo mencionado anteriormente. Aunque no a sus empleados no les pague subsidio en efectivo, ya que es un concepto obligatorio a informar en los casos en que tengan subsidio causado.

En caso de que no lo realice esta asignación de cuenta contable a este concepto, se mostrará alguno de los siguientes mensajes de error, al intentar timbrar un recibo de nómina de algún trabajador.


![IMG](7.png)

![IMG](8.png)


