+++
title="Configuración de Reloj Checador"
description=""
weight = 3
+++

Para realizar la configuración del reloj Checador con huella digital en SAIT Nómina el usuario tiene que realizar los siguientes pasos:

{{%alert success%}}Reloj checador Biometrico ZKsoftware modelo: A10{{%/alert%}}


* [Instalación de Reloj Checador](#instalación-de-reloj-checador)
* [Configuración en SAIT NOMINA](#configuración-en-sait-nómina)
* [Configuración de Reloj.exe para realizar el traspaso de checadas a SAIT NOMINA](#configuración-de-reloj-para-traspaso-a-sait-nómina)
* [Funcionamiento de Reloj.exe](#funcionamiento-de-aplicación-reloj)
* [Como Registrar un Nuevo Trabajador](#como-registrar-un-nuevo-trabajador)
* [Como Eliminar las checadas en el Reloj checador](#como-eliminar-checadas-del-reloj)
* [Como Eliminar todos los datos en el Reloj checador](#como-eliminar-todos-los-datos-del-reloj)

### Liga de descarga de la app reloj.exe. [Descargar](reloj628.exe)

### Instalación de Reloj Checador

{{%alert success%}}Se tiene que instalar en un área segura para garantizar su durabilidad y funcionalidad.{{%/alert%}}

* El reloj Checador mandara la información a SAIT Nomina por medio de un cable de Eterneth (cable de red).
* Se tiene que asignar una IP Fija al reloj Checador para realizar la conexión al equipo que cuenta con SAIT Nomina.

**Como definir la IP en el reloj checador**

* Oprimes el boton de Menú \ Opciones \ Comm. Opc. \ Dir. IP "Ejemplo: 192.168.1.201" (Hacer ping al reloj para probar la configuración de red).
* Para grabar se presiona la tecla OK.
* Presiona la tecla ESC.
* Va a preguntar si desea grabar. Presionar la tecla OK.

**Instalar Librerías en su equipo**

* Después de definir la IP en el reloj, se tiene que agregar y ejecutar las librerías en su equipo (las librerías dependen del tipo de Windows que maneje).
* Como Ingresar las librerías del reloj Checador a Windows"

{{%alert success%}}Windows XP{{%/alert%}}

* Descargar el siguiente archivo para Windows XP de 32 Bits. [Descargar](Librerias_reloj_Win_XP.zip)
* Una vez descargado el archivo .zip tiene que descomprimirlo dentro de C:\ Windows\ System32
* Para ejecutar las librerias que se instalaron se tiene que ingresar a CMD (Simbolo del sistema).
* Para ingresar a CMD tienes que dar click en Inicio\ ejecutar\ingresar la palabra CMD.
* Oprimes la tecla enter o seleccionas con un click el boton aceptar.
![IMG](imagen1.PNG)
* Una vez que se ingreso al programa se tiene que agregar la siguiente instruccion.
* **"Regsvr32 “C:\windows\ System32\zkemkeeper.dll”**
![IMG](imagen2.PNG)
* Después de ingresar la instrucción se oprime la tecla ENTER para ejecutar las librerias y si la instrucción es correcta, te mostrara una leyenda de que su ejecución fue satisfactoria.
* Listo!!! Usted ya cuenta con las librerías ejecutadas para el funcionamiento del reloj checador en su equipo.


{{%alert success%}}Windows 7 32 Bits{{%/alert%}}

* Descargar el siguiente archivo para Windows 7 de 32 Bits. [Descargar](Librerias_reloj_Win_7.zip)
* Una vez descargado el archivo .zip tiene que descomprimirlo dentro de C:\ Windows\ System32
* Para ejecutar las librerias que se instalaron se tiene que ingresar a CMD (Simbolo del sistema) en modo ADMINISTRADOR.
* Para ingresar a CMD tienes que dar click en Inicio\Todos los programas\Accesorios\ Simbolo del sistema.
* Seleccionas el acceso con click derecho para que te despligue las opciones del menu.
* Seleccionas la opcion de ejecutar como administrador.
![IMG](imagen3.PNG)
* Una vez que lo ejecutes en modo administrador te preguntara si deseas que este programa realice cambios en su equipo, lo cual, seleccionaras que SI.
* Una vez que se ingreso al programa se tiene que agregar la siguiente instruccion.
* Regsvr32 “C:\windows\ System32\zkemkeeper.dll”
* Después de ingresar la instrucción se oprime la tecla ENTER para ejecutar las librerias y si la instrucción es correcta, te mostrara una leyenda de que su ejecución fue satisfactoria.
* Listo!!! Usted ya cuenta con las librerías ejecutadas para el funcionamiento del reloj checador en su equipo.

{{%alert success%}}Windows 7 64 Bits{{%/alert%}}

* Descargar el siguiente archivo para Windows 7 de 64 Bits. [Descargar](Librerias_reloj_Win_7_64bits.zip)
* Una vez descargado el archivo .zip tiene que descomprimirlo dentro de **C:\ Windows\ SysWOW64**
* Para ejecutar las librerias que se instalaron se tiene que ingresar a CMD (Simbolo del sistema) en modo ADMINISTRADOR.
* Para ingresar a CMD tienes que dar click en Inicio\Todos los programas\Accesorios\ Simbolo del sistema.
* Seleccionas el acceso con click derecho para que te despligue las opciones del menu.
* Seleccionas la opcion de ejecutar como administrador.
![IMG](imagen3.PNG)
* Una vez que lo ejecutes en modo administrador te preguntara si deseas que este programa realice cambios en su equipo, lo cual, seleccionaras que SI.
* Una vez que se ingreso al programa se tiene que agregar la siguiente instruccion.
* **Regsvr32 “C:\windows\SysWOW64\zkemkeeper.dll”**
* Después de ingresar la instrucción se oprime la tecla **ENTER** para ejecutar las librerias y si la instrucción es correcta, te mostrara una leyenda de que su ejecución fue satisfactoria.
* Listo!!! Usted ya cuenta con las librerías ejecutadas para el funcionamiento del reloj checador en su equipo.

### Configuración en SAIT Nómina

Una vez que el técnico ingreso las librerías que le corresponden al Windows, tiene que realizar la configuración en SAIT NOMINA para realizar el registro de las checadas de sus trabajadores.

El usuario tiene que ingresar a SAIT NOMINA y realizar los siguientes pasos:

* Ingresar al menú de CHECADOR.
* Selecciona la opción de: **Definir reloj checador.**
![IMG](imagen4.PNG)
* Selecciona la opción de agregar.
* El usuario define el nombre de los relojes checadores que utilizará.
* Posteriormente ingresara la dirección IP que le definió directamente en cada reloj checador.
* El puerto a definir siempre será el **4370**.
![IMG](imagen5.PNG)
* Oprime el botón agregar con un click.
* Listo!!!! el reloj checador está configurado.

### Configuración de reloj para Traspaso a Sait Nómina

Una vez que ya se cuenta con la configuración del reloj checador en nomina se tiene que realizar la instalación del archivo Reloj.exe para poder realizar la trasferencia del registro de checadas a SAIT NOMINA, lo cual se realiza de la siguiente forma:

Antes que nada el usuario tiene que verificar en que directorio guarda los registro de su nomina para poder ingresar el achivo Reloj.exe a la carpeta de la empresa.

* Tiene que ingresar a utilerías \Configuración General del Sistema.
![IMG](imagen9.PNG)
* En la parte inferior izquierda te muestra el directorio de la empresa ejemplo: **C:\SAITNOM\CIA001**
* Una vez identificado el directorio el usuario tiene que ingresar a la carpeta de la empresa.
* Descargar el archivo Reloj.exe
* Una vez que descarga el archivo el usuario tiene que ingresar a la carpeta de la empresa para descomprimir el archivo.
* Despues tiene que realizar la creación de un acceso directo hacia el escritorio.
* Listo!!! de esa manera ya estará listo para realizar las trasferencias de información.


### Funcionamiento de Aplicación Reloj

Una vez que ya cuentas con toda la instalación de manera correcta y está trabajando con el reloj checador de manera normal con el registro de checadas de entradas y salidas del trabajador, podrá realizar el traspaso de la información de la siguiente manera:

* El usuario tiene que abrir el programa de trasferir checadas a **SAIT NOMINA**.
* Ejecuta el programa dando doble click en el icono de Reloj.exe.
![IMG](imagen8.PNG)
* Posteriormente te mostrara la siguiente ventana:
![IMG](imagen6.PNG)
* Una vez que ingreso al modulo de trasferir checadas, Selecciona el nombre del reloj a trasferir las checadas.
* Una vez que tiene identificado el reloj, tiene que oprimir el botón de trasferir checadas.
* Selecciona el Periodo de fechas a trasferir.
![IMG](imagen7.PNG)
* Nuevamente oprime el botón de trasferir, el sistema le preguntara si realmente desea trasferir las checadas.
* Listo!!! el sistema agrega automáticamente todas las checadas que se generaron en determinado período.


### Como Registrar un Nuevo Trabajador

En SAIT Nomina se cuenta con un catálogo de trabajadores los cuales tienen que ser registrados en el Reloj checador para poder identificar el registro de sus checadas. 

{{%alert success%}}Como Poder dar de alta un trabajador en el reloj checador:{{%/alert%}}

* Primero tienes que darlo de alta en SAIT Nomina, ya que la clave que se le asigne es la misma que se tiene que definir en el reloj checador.
* En el reloj checador tienes que ingresar ha Menú \ Gestion Usr. \ Grabar Usr. \ Grabar Hu, en este proceso se puede especificar el número de empleado manualmente.
* Para grabar se presiona la tecla OK.
* Presiona la tecla ESC.
* Va a preguntar si desea grabar. Presionar la tecla OK.
* Para salir del menú presiona ESC hasta estar en la pantalla principal.
* Listo!!!!


### Como Eliminar Checadas del Reloj

Para la eliminación de las checadas: Se debe de tener cuidado con estos procedimientos ya que este movimiento elimina el registro de todas las checadas que se generaron en el trascurso de los días.

* Como eliminar las checadas: **Menú\ Opciones \ Opc Sist \ Opc Avanz \ Borrar Fich**.
* Te preguntara si deseas Borrar los Fich.
* Presionas la tecla Ok.
* El Reloj borra la información.

### Como Eliminar Todos los Datos del Reloj

Para Eliminar toda la información: En este proceso se debe de tener un mayor cuidado, ya que te elimina toda la información con que cuenta el reloj chocador (datos del trabajador y sus registros de checadas).

* Como eliminar todos los datos: **Menú\ Opciones \ Opc Sist \ Opc Avanz \ Borrar Datos**.
* Te preguntara si deseas Borrar los Datos.
* Presionas la tecla Ok.
* El Reloj borra la información.



























































