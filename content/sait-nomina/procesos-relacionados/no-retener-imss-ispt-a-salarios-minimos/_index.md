﻿+++
title = "Cómo configurar fórmulas para no retener ISPT ni IMSS"
description = ""
weight = 13
+++

El sistema de SAIT Nómina es tan configurable como usted lo desee, el cómo se manejeen ciertos conceptos varía de cliente a cliente, en caso de que en su empresa desea que NO se le retenga ISTP e IMSS si es salario mínimo, solo es cuestión de agregar una condición a las fórmulas de cada concepto.

Instrucciones:

1.Acceder a un periodo que no haya pagado aún

2.Acceder al menú de Catálogos / Conceptos y seleccionar el concepto 51 ISR

3.Clic en **[Modificar]** y en la parte de CONDICIÓN colocar esta expresión: 

con(501)>con(101) dejándolo de esta manera y clic en **[Grabar]**

![IMG](51.png)

4.Acceder al menú de Catálogos / Conceptos y seleccionar el concepto 53 IMSS 

5.Clic en **[Modificar]** y en la parte de CONDICIÓN colocar esta expresión: 

con(501)>con(101) dejándolo de esta manera y clic en **[Grabar]**  

![IMG](53.png)

Y listo de esta manera, SAIT NÓMINA no hará la retención de IMSS ni ISR si el empleado tiene el salario mínimo


