﻿+++
title = "Cómo calcular PTU (Proceso Automático)"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=FdWbgLu_njs" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="PTU.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

A partir de la versión 2018.4 se incluyó en SAIT Nómina un proceso especial para generar el cálculo de PTU de forma automática.

Este proceso simplifica el tener que obtener el reporte "Auxiliar para PTU", tener que hacer el cálculo manualmente y tener además que capturar el importe a pagar a los trabajadores de forma individual o de forma masiva.

Hay que verificar que su nivel de usuario tenga permitido acceder a esta opción en el menú de utilerías / grupos de usuarios y en el nivel que está utilizando debe habilitar la casilla de **[Calcular PTU]**.
 

![IMG](1.-Permiso.png)

Para tener acceso a esta opción debe dirigirse al menú de:

<h4 class="text-primary">Operación / Procesos Anuales / Calcular PTU</h4>

![IMG](2.-CalcularPTU.png)

En esta ventana:

* Debe establecer el año del que desea obtener los datos acumulados de días trabajados y total de percepciones de los trabajadores.

* Si desea realizar el cálculo para un trabajador en específico, debe colocar la clave del trabajador. Si se deja en blanco el cálculo se realizará para todos los trabajadores.

* Debe indicar el importe a repartir (el monto que va a colocar en este apartado es el monto que le haya resultado como ganancia a la empresa en la declaración anual del ejercicio anterior)
Al presionar el botón de **[Generar]**, se indica el importe a repartir por días laborados, y el importe a repartir por sueldo.

![IMG](3.-CalculoPTU.png)

Se mostrarán en la consulta los datos de los trabajadores, el total de días trabajados y total de percepciones (neto y porcentaje). 

Automáticamente el sistema indicará el importe a pagar por día y por sueldos, indicando el neto total a pagar. Lo único que deberá realizar, es verificar que el concepto en donde se va a ingresa el neto de PTU a pagar sea el correcto. 

* Y finalmente deberá indicar el botón de **[Procesar]**

* A manera de confirmación, deberá indicar en SI para procesar el cálculo de PTU

* Listo, el cálculo de PTU ha sido procesado
Para verificar esta información puede dirigirse a consultar los recibos de los trabajadores para realizar la confirmación de ISR y pueda timbrar los recibos.

![IMG](4.-RECIBO.png)
 