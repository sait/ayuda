+++
title = "Cómo aplicar saldo a favor o cargo en recibo"
description = ""
weight = 7
+++
<h2 class="text-primary">Aplicación en recibo de saldo a favor o cargo</h2> 

Cuando se realiza el ajuste anual a sus trabajadores en términos del artículo 97 de la LISR, este indica que los resultados que se obtengan se procederá como sigue:

-El ISR a cargo se enteren a más tardar el mes de febrero del siguiente año y el saldo a favor deberá compensarse contra la retención del mes de diciembre y las retenciones sucesivas, a más tardar dentro del año posterior.

Los conceptos a utilizar en el sitema SAIT Nómina son los siguientes: 

**NOTA: El concepto 701 es un concepto fijo y el 730 lo puede tener en cualquier concepto (no es fijo)**.

**SALDO A FAVOR**

De acuerdo con el artículo 6 de la Ley de la LISR el acreditamento, devolución o compensación de saldos a favor: únicamente podrá acreditarlo contra el impuesto a su cargo que le corresponda en los meses siguientes hasta agotarlo o solicitar su devolución. 

![IMG](701.PNG)


**ISR A CARGO**

En el caso de ISR a cargo se deberá registrar como una deducción en el concepto usted desee designar siempre cuidando que la clave SAT sea “101” (ISR Retenido de ejercicio anterior).

En estos casos solo es necesario recordar la cantidad no puede ser mayor a lo que sale de ISR en esa nómina donde están poniendo el saldo a favor, **para eso es necesario usar el concepto de monto limite** ya que son datos que se necesarios para el timbrado correcto del recibo.

![IMG](730.PNG)

![IMG](movimientos.PNG)

**A considerar**

Si el cálculo anual de los trabajadores lo tiene listo antes de que termine el año y éste saliera a favor y aún no tiene certificada su última nómina podrá incluir en su recibo un concepto de OTRO PAGO con que la clave SAT “001 Reintegro de ISR pagado en exceso (siempre que no haya sido reportado al SAT).”

**Como su descripción lo indica  sólo puede usarse  siempre  que no se haya realizado la declaración de pago provisional de ISR, por lo anterior, te recomendamos consultar con tu contador cuándo podrías hacer uso de esta clave**.