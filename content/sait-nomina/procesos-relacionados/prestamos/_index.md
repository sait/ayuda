﻿+++
title = "Cómo realizar préstamos personales"
description = ""
weight = 12
+++

En SAIT nómina usted puede otorgar a sus trabajadores préstamos personales, en el cual podrá definir el prestamos que se facilitará y también podrá indicar cómo se irá descontando de nóminas posteriores

1.Primera deberá crear concepto para dar el préstamo en el menú Catálogos / Conceptos

Dicho concepto deberá ser con clave del 1 al 50, la que tenga disponible o con clave del 700 en adelante, en caso de que no exista alguna disponible entre el 1 y 50 (en SAIT nómina, los conceptos del 1 al 50 son percepciones y del 50 en adelante deducciones, hasta el 700 en adelante se consideran como percepciones nuevamente)

2.Para agregar el monto del préstamo al recibo, siga las siguientes instrucciones:

* Entrar a menú de Operación / Consultar Recibo 

* Ingrese el número de trabajador 

* De  clic en **[Movimientos]** 

* Ingrese el concepto que definió como Prestamo Personal, en este caso fue el 720 e ingrese la Cantidad y de clic en **[Grabar]**

![IMG](1.jpg)

De esta manera se verá reflejado el importe del préstamo en el recibo 

![IMG](2.jpg)

2.Ahora para descontar el préstamo, siga las siguientes instrucciones: 

* Entrar a menú de Operación / Consultar Recibo 

* Ingrese el número de trabajador 

* Clic en **[Movimientos]**

* Capture en Concepto la clave 55 

* En **[Tipo Mov]** presione barra espaciadora en su teclado y deje seleccionado la opción de **Monto Límite**

* En cantidad establecer el importe que se le descontará cada semana

* En **Monto limite** debe ser el Total de lo prestado

* En **Monto Acum** se deja en ceros el sistema acumulará lo descontado de cada semana

![IMG](3.jpg)

3.Al generar el siguiente periodo realizar el proceso generación automática de movimientos para poder transferir los movimiento repetitivos del periodo anterior al nuevo.

Para eso deberá ir al menú de Operación y eleccionar la opción de Generación automática de movimientos y dar clic en **[Generar movimientos]**

![IMG](4.jpg)

De esta manera notará se cargó automáticamente la deducción de 500 pesos en le periodo actual

![IMG](5.jpg)

Como comentario adicional, normalmente los préstamos se otorgan junto a una semana de salario normal, pero si usted decidió hacer una nómina en donde solo pagará el monto del préstamo, deberá capturar en el concepto 1 SALARIO NORMAL y en el  concepto 131 DÍAS A PAGAR INCLUYENDO DESCANSO el valor de 0.01


