﻿+++
title = "Calcular Finiquito"
description = ""
weight = 15
+++

Uno de los procesos más importantes esta relacionado con el proceso de cálculo de finiquito, el cual se puede generar por las siguientes casos: Renuncia o Despido. El sistema de manera automática realiza el cálculo en base a los conceptos establecidos en la ley.

En caso de **renuncia**, por ley se afectan los siguientes conceptos:

- Aguinaldo
- Vacaciones
- Prima Vacacional 25%

En caso de **despido**, por ley se afectan los siguientes conceptos:

- Aguinaldo
- Vacaciones
- Prima Vacacional 25%
- Indemnización
- Prima de Antigüedad

<h3 class="text-danger">Importante</h3>

Al realizar un finiquito, deberá separar los ingresos por tipo, es decir:

a) Deberá elaborar una nómina por las percepciones ordinarias (Sueldos, Compensaciones, Bonos, etc.)

b) Y otra nómina extraordinaria por las percepciones
extraordinarias (Aguinaldo, Indemnizaciones, Prima de Antigüedad, etc).

Ver documentación [aquí](/sait-nomina/procesos-relacionados/emision-de-2-recibos-en-finiquitos/)

<h3 class="text-primary">Cálculo de Finiquito</h3>

1.Dirijase a Operación / Calcular Finiquito.
  
2.Aparecerá la siguiente ventana: 

![IMG](1.png)

3.Capture el numero de trabajador. En caso de ser necesario puede hacer una busqueda por nombre, hacer clic en el botón [?], se mostrarán los datos del trabajador como: Fecha de Ingreso, Antiguedad y Salario Diario.

4.Seleccione la causa de la salida: Despido o Renuncia, para nuestro ejemplo vamos a tomarlo como Renuncia.

5.Capturar la fecha de salida de Renuncia o Despido y de manera automática se mostrarán en pantalla la Antiguedad en base a la fecha de ingreso capturada en el catálogo de trabajadores. Al igual que se mostrarán todos los conceptos relacionados, como se muestra en la siguiente ventana: 

![IMG](2.png)

- **En antigüedad considerar años proporcionales** : En caso de ser necesario de considerar en la antigüedad años proporciones, es decir la antigüedad de manera fraccionada deberá habilitar esta opción.
- **En indemnización considerar salario integrado**: En caso de ser necesario de considerar en el cálculo de indemnización por el salario integrado en lugar del salario diario, deberá habilitar esta opción.
- **Días de vacaciones pendientes del año anterior**: En caso de haber quedado pendientes días de vacaciones del año anterior, deberá de capturarlos en el campo.

6.Una vez verificados los datos del cálculo de los conceptos anteriormente mencionados en la tabla, para guardar los conceptos en el periodo actual de nómina deberá hacer clic en el botón [Guardar], el sistema le preguntará si está seguro que desea alimentar los importes capturados en los conceptos indicados en el cálculo, a lo cual deberá seleccionar en "Si". 

