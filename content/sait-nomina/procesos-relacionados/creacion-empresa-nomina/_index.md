+++
title = "Creación de Empresa de Nómina"
description = ""
weight = 1
+++

En caso de que necesite agregar otra razón social a su sistema de SAIT NÓMINA deberá de seguir los siguientes pasos para activarla de manera correcta.

<h3 class="text-primary">1. Crear empresa</h3>

El primer paso es crear la empresa en su programa de SAIT nómina, para visualizar todos los pasos de este proceso deberá entrar al siguiente link:

https://ayuda.sait.mx/sait-basico/1-instalacion-y-activacion-licencias/c-crear-empresa/

<h3 class="text-primary">2. Activación de clave de empresa</h3>

El segundo paso consiste en activar la clave de empresa, es importante no se confunda con la clave de activación de licencia la cual se activa por paquete SAIT la clave de empresa se activa por RFC o Razón Social:

https://ayuda.sait.mx/sait-basico/1-instalacion-y-activacion-licencias/e-activar-clave-por-razon-social/

<h3 class="text-primary">3. Agregar CSD (Sellos Digitales)</h3>

Para poder timbrar sus recibos de nómina deberá agregar los Certificados de Sellos Digitales correspondientes a la razón social que estamos creando

https://ayuda.sait.mx/sait-basico/4-configuracion-de-empresa/c-ingresar-sellos-digitales/

<h3 class="text-primary">4. Firmar manifiesto</h3>

Es importante firmar el manifiesto, en el cual nos autoriza ser su proveedor de timbrado para que en un futuro no tenga problema en su generación de timbrado de nóminas, las insutrcciones para firmar el manifiesto las encuentra en el siguiente enlace:

https://ayuda.sait.mx/sait-basico/1-instalacion-y-activacion-licencias/f-firma-manifiesto/

<h3 class="text-primary">5. Activar clave de facturación electrónica</h3>

Finalmente es necesario realizar la clave de activación de factura electrónica, este proceso se hace por razón social o RFC, para generarla deberá entrar al siguiente enlace: http://msl.com.mx/claves/activarfe/index.htm

Se mostrará el formulario de llenado el formulario, complete todos los datos CORRECTAMENTE, verifique la información ingresada y de clic en **[Activar]** y copie la clave que se le proporcione.

![IMG](FACTURACION2012.JPG)

Esta clave la ingresará en su sistema de SAIT Nómina en el menú de Utilerías / Configuración General del Sistema / CFDI 

Los datos se llenan tal y como aparece en la imagen con su número de contrato, versión del CFDI 4.0, versión del complemento 1.2 y la clave de facturación electrónica que recién generó clic en **[Aceptar]** y listo.

![IMG](CFDI.png)
 
