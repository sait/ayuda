﻿+++
title = "Cómo realizar nómina de pago de vacaciones"
description = ""
weight = 10
+++

En SAIT nómina usted puede pagar los días de vacaciones a sus empleados de dos formas, junto al salario o en una nómina aparte, a continuación se explicarán ambas maneras:

* Nómina de pago para todos los empleados

* Periodo especial para vacaciones

<h2 class="text-primary">Pago de vacaciones junto con semana normal</h2>

1.Crear periodo:

![IMG](periodo.jpg)

2.Conceptos a verificar que estén correctamente definidos

![IMG](148.jpg)

![IMG](9.jpg)

![IMG](10.jpg)

3.Para efectos de la prueba simularemos un empleado, tomó 4 dias de vacaciones y trabajó 3 días.

Para esta situación en el 132 deberá capturar los días de vacaciones a pagar, los cuales son 4 días 

![IMG](dias-descansar.jpg)

En el 130 deberá capturar los días trabajados a pagar, que este caso serán 3

![IMG](dias-trabajados.jpg)

Verificar que en el 131 de un total de 7 días o bien si pagará más días de vacaciones, revisar que el total de los días coincida con el pago, ya que el concepto 131 define el ISR  retener.

Como se puede observar al capturar sólo los días de vacaciones, se calcula de forma automática el importre de las vacaciones y prima vacacional.
![IMG](total-dias.jpg)

<h2 class="text-primary">Pago de vacaciones separada</h2>

1.Al crear el periodo especial para el pago de vacaciones por separado y para efectos de control, se recomienda se modifique la descripción para ubicar más rápido entre los periodos la nómina en que se pagaron vacaciones, puede agregarla una V al número de periodo para identificarlo más fácilmente, la nómina debe ser ordinaria y otro tipo de periodicidad.

![IMG](periodo.jpg)

2.Conceptos a verificar que estén correctamente definidos

![IMG](9.jpg)

![IMG](10.jpg)

3.Los conceptos que deberá inactivar son: 

* 1 (Salario Normal) 

* 53 (IMSS)

Como comentario adicional, sí usted no paga el 7mo día de igual forma deberá inactivar el concepto 7 para que no aparezca en el recibo.

Para inactivar los conceptos deberá ir al menú de Catálogo / Conceptos, seleccione el concepto y con barra espaciadora cambie a INACTIVO y clic en Grabar

![IMG](1-inactivar.jpg)

![IMG](53-inactivar.jpg)

5.Después deberá ir a ir a Operación / Consultar recibo / Movimientos y capturaremos en el Concepto 132 los días a descansar **para este ejemplo serán 6 días**

![IMG](nomina-sola.jpg)

Y finalmente en el Concepto 130 deberá capturarlo con valor 0

![IMG](nomina-sola2.png)

6.Quedando de esta manera nuestro recibo

![IMG](nomina-sola-recibo.jpg)

<h4 class="text-primary">Muy importante mencionar que cuando genere el siguiente periodo se encuentre usted posicionado en un periodo normal, ya que si está posicionado en este periodo especial, va a repetir los movimientos que se hizo en este periodo.</h4>
