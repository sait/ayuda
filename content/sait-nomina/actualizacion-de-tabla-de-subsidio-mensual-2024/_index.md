+++
title = "Actualización de tabla de Subsidio Mensual 2024"
description = ""
weight = 4
+++

<h2 class="text-primary">Actualización de tabla de Subsidio Mensual 2024</h2>  


Como ustedes saben, el día 01 de Mayo del 2024, se publicó en el diario oficial de la federación (DOF), el decreto de que si el trabajador que tenga un salario diario entre 248.93 a 298.71 tendrán un beneficio, ya que obtendrán un subsidio al empleo de 390.12 (Mensual) lo que los ayudará a reducir el pago del ISR.

Es importante mencionar que el decreto NO Beneficia a ninguna persona de la Frontera Norte ya que ninguna recibirá Subsidio al Empleo, debido a que el salario mínimo de esa zona es 374.89


<h2 class="text-primary">Nuevo Cálculo</h2>  

Dentro de SAIT Nómina, es importante considerar la sustitución de la tabla utilizada para el otorgamiento del subsidio al empleo. Este decreto elimina la tabla emitida en 2013 y la sustituye por un cálculo mediante cuota mensual, equivalente al 11.82% de la unidad de medida y actualización (UMA) para los trabajadores cuyos ingresos mensuales base del ISR no excedan de MXN **9,081.00.**

Los ingresos que son base gravable no incluyen conceptos como primas de antigüedad, retiro e indemnizaciones.

En los casos donde el ISR del mes respectivo a cargo del trabajador sea menor al subsidio al empleo a otorgar, no dará lugar a que su remanente se aplique contra el impuesto generado posteriormente, ni se entregará cantidad alguna por concepto de este subsidio.

<h2 class="text-primary">Actualizar tabla 3 de Subsidio al Empleo</h2>  

1.Vaya al menú de Catálogos / Tablas

2.Modifique  la tabla número 3 titulada: "Subsidio al Empleo Mensual", elimine todos los renglones y capture solamente los siguientes 2 renglones:

LimInf|LimSup|SubEmpl|
---|---|---|
0.01 | 9081.00 | 390.12 |
9081.01 | 9999999999 | 0.00 | 


3.Se deberá ver de esta manera y para terminar deberá hacer clic en el botón [Grabar].

![1](SUB.png)

4.Listo, usted ha actualizado los valores de la tabla.

<h2 class="text-primary">Actualizar valor de la UMA en SAIT Nómina</h2>  

1.Vaya al menú de: catálogos / Conceptos. Se visualiza de la siguiente ventana:

![1](CONC.png)

2.Dar clic en el botón [Buscar F2] y escribir UMA o unidad de medida de actualización, regularmente es en concepto 164, pero hay nóminas a la medida que lo consideran en otro concepto y asegurarse tenga el valor de **108.57** que es la UMA del 2024, dato que se actualiza cada febrero.

![1](CONC2.png)

3.Clic en el botón [Grabar], para guardar los cambios.

<h2 class="text-primary">Consideraciones Importantes</h2>  

* La aplicación de este decreto deberá considerar anualmente su actualización con la UMA que se emita en febrero de cada año, debido a que el otorga del subsidio al empleo mediante una cuota mensual ocupa como base la UMA vigente, no con el límite de ingresos gravables que serán base para ser sujeto a obtener subsidio al empleo, ya que la cantidad permanecerá 9,081.00 M.N.

* Para determinar la cuota mensual, aplicable en pagos que se realicen por periodos menores a un mes, se podrá dividir la cuota mensual que resulta del 11.82% de la UMA entre 30.4 y multiplicar este resultado por el número de días del periodo de pago.

* Asimismo, si se realizan pagos por dos o más meses, se deberá calcular el monto mensual de la UMA multiplicado por 11.82%, y a su vez, por el número de meses que corresponda el pago.

* En ambos casos, el importe de subsidio al empleado determinado no podrá exceder del importe mensual máximo que resulte de multiplicar el valor mensual de la UMA por 11.82%.

