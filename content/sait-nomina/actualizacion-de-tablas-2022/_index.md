+++
title = "Actualización de tablas 2022 y Salario Mínimo"
description = ""
weight = 9
+++

<h2 class="text-primary">Salarios 2022</h2>  

Para realizar la actualización de nuestros salarios hay que entrar al concepto 101 y 102 y actualizar, OJO es importante que identifiques tu zona para realizar dichos cambios.

![1](2022.png)

{{%alert success%}}Los pasos para modificar los conceptos números 101 y 102 son los siguientes:{{%/alert%}}

1. Entrar a SAIT Nomina.

2. Colocarse en un período de nómina que aún no se ha pagado.

3. Dirigirse al menú: **Catálogos / Conceptos**.

4. En el campo: **CLAVE** capturar 101 y presionar la tecla **[Enter]**.

5. Posicionarse en el campo: **FORMULA** y capturar el salario mínimo diario según la zona que corresponda

6. Haga clic en el botón: **[Grabar]**. 

7. Realizar los mismos pasos 4 y 5, para el **Concepto 102**. 

8. Listo!!!, nuestro sistema de nómina 2022, ya esta actualizado con el salario mínimo diario de la zona.

{{%alert success%}}Si ya generaste tu nómina de la primer semana de enero no te preocupes, recuerda que ahora el cálculo de los impuestos es con base al valor del UMA.{{%/alert%}}

<h2 class="text-primary">TABLA 1 ISR 2022</h2> 

Como ustedes saben, año con año en el diario oficial de la federación se publican los nuevos parámetros a incluir en las tablas mensuales para el cálculo de ISPT de nómina, con el objetivo de integrarlos a las aplicaciones de nómina.

{{%alert success%}}Las tablas ISR 2022 y Subsidio al Empleo, este año NO sufrieron ningún cambio en relación a la del año 2021, sin embargo las compartimos de nueva cuenta para que las verifique en su sistema de SAIT Nómina{{%/alert%}}


**Recuerda:** **En SAIT NÓMINA** es necesario capturar las tablas cuando existan modificaciones, las tablas no se cambian automáticamente al actualizar la versión

#### TABLA 1

La tabla 1 correspondiente al ISPT o ISR Mensual es la siguiente:

**No de Tabla:** 	1

**Nombre:**	ISPT2022

**No Columnas:**	4	

**Conc Entrada:**	149		
			
LimInf|LimSup|CuotaFija|PjeSExced
---|---|---|---
0.01 | 644.58 | 0 | 0.0192
644.59 | 5470.92 | 12.38 | 0.064
5470.93 | 9614.66 | 321.26 | 0.1088
9614.67 | 11176.62 | 772.1 | 0.16
11176.63 | 13381.47 | 1022.01 | 0.1792
13381.48 | 26988.50 | 1417.12 | 0.2136
26988.51 | 42537.58 | 4323.58 | 0.2352
42537.59 | 81211.25 | 7980.73 | 0.30
81211.26 | 108281.67 | 19582.83 | 0.32
108281.68 | 324845.01 | 28245.36 | 0.34
324845.02 | 9999999999 | 101876.9 | 0.35

<h2 class="text-primary">TABLA 2 SUBSIDIO AL EMPLEO 2022</h2> 

#### TABLA 3 

La tabla 3 correspondiente al subsidio al empleo **SIGUE SIENDO LA MISMA** no tuvo ningún cambio al 2021, pero de igual manera se anexa en caso de que desee validar los datos.

**No de Tabla:** 	3

**Nombre:**	SUB2022

**No Columnas:**	3

**Conc Entrada:**	149

LimInf|LimSup|SubEmpl 
---|---|--- 
0.01 | 1768.96 | 407.02
1768.97 | 2653.38 | 406.83
2653.39 | 3472.84 | 406.62
3472.85 | 3537.87 | 392.77
3537.88 | 4446.15 | 382.46
4446.16 | 4717.18 | 354.23
4717.19 | 5335.42 | 324.87
5335.43 | 6224.67 | 294.63
6224.68 | 7113.90 | 253.54
7113.91 | 7382.33 | 217.61
7382.34 | 999999999 | 0

<h2 class="text-primary">Como Modificar las tablas 1 y 3 en SAIT Nómina</h2> 

<h4 class="text-danger">Antes de modificar las tablas 1 ISPT y 3 Subsidio al Empleo 2022, deberás asegurarte de estar posicionado en un periodo de nómina que aun no se ha pagado.</h4>

1.	Dirigirse al menú de Catálogos / Tablas, Se visualiza de la siguiente ventana:

![1](tablas.JPG)

2.Capturar el número de tabla 1 y dar un [Enter]. Se visualiza la siguiente ventana:

![1](tabla1.JPG)

3. Deberá cambiar el nombre de la tabla a: **ISPTM2022**.

4. Deberá seleccionar únicamente los valores de la tabla 1 (es decir sin los encabezados) y posteriormente copiarlos.

LimInf|LimSup|CuotaFija|PjeSExced
---|---|---|---
0.01 | 644.58 | 0 | 0.0192
644.59 | 5470.92 | 12.38 | 0.064
5470.93 | 9614.66 | 321.26 | 0.1088
9614.67 | 11176.62 | 772.1 | 0.16
11176.63 | 13381.47 | 1022.01 | 0.1792
13381.48 | 26988.50 | 1417.12 | 0.2136
26988.51 | 42537.58 | 4323.58 | 0.2352
42537.59 | 81211.25 | 7980.73 | 0.30
81211.26 | 108281.67 | 19582.83 | 0.32
108281.68 | 324845.01 | 28245.36 | 0.34
324845.02 | 9999999999 | 101876.9 | 0.35

5. Posicionarse nuevamente en SAIT Nomina y hacer un clic en la primera columna con valor de la tabla, tal como se visualiza en el paso 1, posteriormente deberá de dar clic en el botón de PEGAR y notará que se pegarán los valores copiados.

![1](copiar.png)

6. Para grabar los nuevos valores, deberá hacer clic en el botón [Grabar].

7. Listo!!!, usted ha actualizado los valores de la tabla.

