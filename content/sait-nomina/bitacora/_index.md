﻿+++
title = "Bitácora de Cambios SAIT-NÓMINA"
weight = 1
+++

{{%alert info%}} Para descargar la última versión del sistema SAIT Software Administrativo de clic [Aquí ](../../otros-temas/descargar-version/){{%/alert%}} 

Bitácora de mejoras y correcciones para SAIT Nómina 2023

Versión | Fecha | Descripción
---|---|---
2025.1.0 | 03.Ene | Corrección:  Solo inicializar Cia.CFDIVER cuando se crea el campo (#131964)
2023.3.0 | 24.Jul | Mejora: Se agregó Régimen Fiscal 626 RESICO. (#120803)
2023.3.0 | 24.Jul | Corrección: Ahora se crea IncapSAT anualmente al actualizar base de datos. (#121117)
2023.3.0 | 24.Jul | Corrección: Antes de emitir CFDI se vuelve a confirmar que esté seleccionado el trabajador correcto. (#122367)
2023.2.0 | 09.Jun | Mejora: Nuevos motivos de baja para el IMSS. (#122092)
2023.2.0 | 09.Jun | Corrección: Finiquito usaba renglón anterior en la tabla. (#122177)
2023.1.0 | 30.May | Mejora: Timbrado Mensual para CFDI 4.0
2023.1.0 | 30.May | Corrección: Ventana de Finiquito ahora toma los días de vacaciones y aguinaldo desde la tabla de factor de integración asignada al trabajador.

Bitácora de mejoras y correcciones para SAIT Nómina 2022

Versión | Fecha | Descripción
---|---|---
2022.3.0 | 14.Jul | Mejora: Se agregó Régimen Fiscal 625 Plataformas Digitales. 
2022.3.0 | 14.Jul | Corrección: al validar de Clave de Activacion de Timbrado para CFDI 4.0 para SA y SRL. 
2022.3.0 | 14.Jul | Corrección: falla ortográfica 'Indemnización' al realiar finiquito.
2022.2.0 | 30.Jun | Corrección: Mostrar Regimen Fiscal de Cliente en myCfdi40.frx. 
2022.2.0 | 30.Jun |Corrección: Expresión Régimen: cveDescCatCfdi('RegimenFiscal',oCFDI.Receptor.RegimenFiscalReceptor)
2022.1.0 | 27.Jun | Mejora: Versión para Emitir CFDI 4.0. 
2022.1.0 | 27.Jun | Mejora: Se permite agregar Régimen Fiscal en Catálogo de Cliente. 
2022.1.0 | 27.Jun | Mejora: Se preparó formato de Recibo para 4.0 se crea automático: mycfdi40.frx

Bitácora de mejoras y correcciones para SAIT Nómina 2021

Versión | Fecha | Descripción
---|---|---
2021.0.5 |02.Jun | Mejora. Se permite timbrar CFDIs de prueba con nuevo CSD. 
2021.0.4 |04.May | Corrección. Al ajustar neto en consulta del recibo, fallaba al realizarlo varias veces (sólo con el último trabajador registrado) (#855)
2021.0.4 |04.May | Mejora. Se incluye columna de número de periodo en ventana de consultar cfdis (#852)
2021.0.4 |04.May | Corrección. Al imprimir recibos de diferentes periodos desde consultar cfdis, se muestraban las fechas del periodo activo (#850)
2021.0.3 |27.Mar | Corrección. Al agregar nodo otrospagos en el Xml cuando el trabajador no tuvo subsidio al empleo y si tuvo otros pagos
2021.2.0 |25.Mar | Mejora. Se permite capturar la información de incapacidades, en percepciones/deducciones con conceptos del 700 al 999 (#843)
2021.2.0 |25.Mar | Mejora. Se agregó rango de departamentos al generar layout para dispersión de nomina en ventana de interfase con bancos (#841)
2021.2.0 |25.Mar | Mejora. Al enviar recibos a trabajadores por correo, se muestra barra de porcentaje de avance (#840)
2021.1.0 |26.Feb | Mejora. En Ajuste del Subsidio para el Empleo, la opción 'Calcular reintegro de ISR' se separo en dos opciones diferentes
2021.0.0 |20.Ene | Mejora. Se hicieron cambios en la forma de almacenar las incapacidades.
2021.0.0 |20.Ene | Mejora. Cambios al agregar nodo otrospagos en el Xml cuando el trabajador no tuvo subsidio al empleo


Ésta es la bitácora de las mejoras y correcciones realizadas a SAIT Nómina durante 2020

Versión | Fecha | Descripción
---|---|---
2020.7.0 |11.Dic | Mejora. En cálculo de ajuste de subsidio para el empleo
2020.6.0 |25.Nov | Mejora. Envío masivo de recibos por correo a los trabajadores desde la Consultar CFDI (#780)
2020.6.0 |25.Nov | Mejora. Captura masiva de cuentas contables por trajador (#773)
2020.6.0 |25.Nov | Corrección. Al consultar CFDI, al imprimir primer recibo mostraba un CFDI distinto al timbrado (#757)
2020.5.0 |30.Abr | Mejora. Cambios en ajuste al subsidio para el empleo en el importe del concepto 912-ISR ajustado por subsidio
2020.5.0 |30.Abr | Mejora. Se agregaron nuevos tipos de deducción y otros pagos para asignar clasificación SAT a los conceptos
2020.5.0 |30.Abr | Corrección. En percepciones, deducciones y otros pagos no se permite incluir conceptos que los totalizan (#709)
2020.5.0 |30.Abr | Corrección. En formato de CFDI mensual mostrar la fecha inicial menor, fecha de final y fecha de pago mayor de todos los períodos timbrados (#708)
2020.4.0 |28.Feb | Mejora. En timbrados mensuales, no validar conceptos ordinarios y extraordinarios.
2020.3.0 |26.Feb | Corrección. Ajustes en remanente de saldo a favor por compensación anual
2020.2.0 |17.Feb | Mejora. Se agrega el atributo TotalOtrosPagos aun cuando tenga valor cero
2020.1.0 |06.Feb | Mejora. Se obliga a actualizar la tabla tipoded.dbf la cual contiene los tipos de deducciones del SAT
2020.0.0 |04.Feb | Mejora. Proceso para el Ajuste del Subsidio para el Empleo. Documentación [Aquí](/sait-nomina/ajuste-subsidio-empleo/)
2020.0.0 |04.Feb | Mejora. Al generar archivo para IDSE, preguntar ruta para grabar el archivo (#652)
2020.0.0 |04.Feb | Corrección. Al generar archivo para IDSE, incluir correctamente acentos y Ñ (#651)
2020.0.0 |04.Feb | Mejora. Al abrir el catálogo de conceptos, se posiciona en el primer concepto en lugar del último se que agregó (#391)


Esta es la bitácora de las mejoras y correcciones realizadas a SAIT Nómina durante 2019

Versión | Fecha | Descripción
---|---|---
2019.10.0 |03.Ago | Mejora. En consultas de acumulados e históricos, se permite agregar conceptos arriba del 508 (#577)
2019.9.0 |10.Jul | Mejora. Si el CFDI no se pudo cancelar, se deja activo (Ya no se asigna el status Por Cancelar) (#571).
2019.8.0 |21.Jun | Mejora. En cancelación masiva de Cfdis, al final del proceso se muestra el detalle si por algún motivo no se pudieron cancelar todos.
2019.7.0 |31.May | Mejora. Se permite capturar importes en millones en los conceptos.
2019.7.0 |31.May | Corrección. En cálculo de PTU no se incluyen trabajadores que laboraron menos de 60 días (#544)
2019.6.0 |15.May | Mejora. Se permite cancelar masivamente CFDIs desde la ventana de "Consultar CFDI"
2019.6.0 |15.May | Mejora. Ya se permite generar CFDIs semanales, después de haber generado CFDIs mensuales y fueron cancelados (#508)
2019.6.0 |15.May | Mejora. Se permite elegir entre el formato de recibo anterior o apartir de XML, en configuración general del sistema (#450)
2019.5.0 |05.Abr | Mejora. Si el contrato del trabajador es 09, 10 o 99 al timbrar el recibo no se incluye en el XML el registro patronal, num. seguridad social, fecha inicio rel. laboral, antigüedad y salario diario integrado (#495)
2019.5.0 |05.Abr | Corrección. Al consultar un cfdi y ver el XML, se deshabilitaba la barra de herramientas.
2019.4.0 |22.Mar | Corrección. Cuando CFDI solo tenía una percepción, no se generaba PDF
2019.3.0 |10.Ene | Mejora. Optimización en generación de CFDI
2019.2.0 |10.Ene | Mejora. Ajustes internos
2019.1.0 |09.Ene | Corrección. Se realizaron ajustes al enviar CFDI por correo

Esta es la bitácora de las mejoras y correcciones realizadas a SAIT Nómina durante 2018

Versión | Fecha | Descripción
---|---|---
2019.0.0 |31.Dic | Mejora. Declaración de subsidio causado en casos donde se aplique.
2019.0.0 |31.Dic | Mejora. Se agregaron nuevos tipos de clasificación en percepciones, deducciones, régimen de contratación.
2019.0.0 |31.Dic | Corrección. Cálculo de dias a pagar en aguinaldo.
2019.0.0 |31.Dic | Mejora. Actualización a formato de recibo de nómina al enviar por correo.
2019.0.0 |31.Dic | Mejora. Se incluye opción de imprimir recibo a partir del XML desde la ventana de "Consultar CFDI"
2018.4.0 |31.Ago | Mejora. Incluir cuenta contable de la percepcion, deducción y otros pagos en el XML. Anteriormente se incluía la clave del concepto.
2018.3.0 |31.Jul | Mejora. Nuevo proceso para Calcular PTU. 
2018.3.0 |31.Jul | Mejora. Se actualizaron las fórmulas al crear una nueva empresa.
2018.3.0 |31.Jul | Mejora. Se permite enviar a Excel el cálculo del salario diario integrado.
2018.3.0 |31.Jul | Mejora. En consulta de recibos, solo se permite capturar movimientos o ajustar neto si el usuario tiene acceso (#309) 
2018.3.0 |31.Jul | Corrección. Al obtener el total de dias trabajados en ventana de "Actualizar Salario Diario Integrado" 
2018.3.0 |31.Jul | Corrección. Al cancelar recibo de nómina con status "Por Cancelar", no los marcaba como cancelados (#302) 
2018.3.0 |31.Jul | Corrección. Mostraba error al abrir al mismo tiempo las ventanas de Checador\Modificar transacciones y Checador\Enlace con reloj (#268)
2018.2.0 |03.Abr | Corrección. La ventana de reporte de movimientos del IMSS y consulta de recibos mostraban un error si estaban abiertas al cambiar de período. 
2018.2.0 |03.Abr | Mejora. Si el año de la fecha de pago es distinto a la ultima fecha del periodo se pide confirmar.