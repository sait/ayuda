﻿+++
title = "Modificar Cheque – Póliza"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=hGuYFTzlJOg&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&index=4&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. E Modificar Cheque-Poliza.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Dicha opción le permite re-imprimir, modificar, cancelar, borrar y definir un Cheque- Póliza frecuente que ya se procesó.

<h2 class="text-primary">Modificar Cheque – Póliza</h2>

Para realizar este proceso diríjase a 

Bancos / Modificar Cheque Póliza

1.Seleccione la cuenta bancaria del cheque.

2.Capture el número de Cheque y presione la tecla [Enter]

3.Se muestran los datos.

4.Realice las modificaciones necesarias.

5.Presione la tecla [F8] para guardar e imprimir el Cheque-Póliza.

**Nota: Se puede imprimir únicamente el Cheque, la Póliza o ambos al hacer clic en el botón [►] que esta enseguida de [Imprimir]**


![IMG](1.png)

<h2 class="text-primary">Eliminar Cheque – Póliza</h2>

Al borrar o eliminar un cheque la idea es volver a capturar la información, pero de manera correcta, ya que el folio queda disponible para su recaptura, a diferencia que cuando se cancela un cheque.

1.Ingrese al menú de Bancos / Modificar Cheque – Póliza.

2.Seleccione la cuenta bancaria del cheque.

3.Capture el número de Cheque. Presione la tecla [Enter]

4.Se muestran los datos.

5.Verifique que sea el cheque que se desea borrar.

6.Haga clic en [Borrar]

<h2 class="text-primary">Cancelar Cheque – Póliza</h2>

Al borrar un cheque la idea es volver a capturar la información, pero de manera correcta, ya que el folio queda disponible para su recaptura, a diferencia que cuando se cancela un cheque.

Para realizar este proceso diríjase a:

1.Seleccione la cuenta bancaria del cheque.

2.Capture el número de Cheque. Presione la tecla [Enter]

3.Se muestran los datos.

4.Verifique que sea el cheque que se desea cancelar.

5.Haga clic en [Cancelar]

<h2 class="text-primary">Definir un Cheque – Póliza como Frecuente</h2>

Cuando un cheque es cancelado dentro del sistema puede ser porque físicamente el cheque se perdió, se manchó, se rompió, etc y ya no será utilizado, al cancelarlo dentro del sistema aparecerá la leyenda de cancelado al consultarlo y dicho folio no podrá ser utilizado mas a diferencia de cuando borramos un cheque en donde si podemos rehusar el folio en cuestión.

Para realizar este proceso diríjase a:

1.Ingrese al menú de Bancos / Modificar Cheque – Póliza.

2.Seleccione la cuenta bancaria del cheque.

3.Capture el número de Cheque. Presione la tecla [Enter]

4.Se muestran los datos.

5.Haga clic en [Frecuente]

6.Especifique el título del cheque.

7.Haga clic en [Continuar]
