﻿+++
title = "Estado de Cuenta"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=Vt2VmupufQ0&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&index=7&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. F Estado de Cuenta.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Permite consultar todos los movimientos registrados de cada cuenta bancaria, lo cual permite saber a determinada fecha el saldo que había en cada cuenta, de acuerdo a los depósitos y retiros realizados.

Para acceder a dicha función ingrese a:

1.Ingrese al menú de Bancos / Estado de Cuenta.



![IMG](1.png)

2.Seleccione la cuenta bancaria.

3.Se muestran los movimientos: **Fecha, Tipo de póliza, Folio, Concepto, y los importes de los Depósitos, Retiros y el Saldo hasta ese momento.**

4.En caso de que el movimiento se haya **conciliado**, se muestra activada la casilla que se encuentra enseguida del depósito.

5.Se pueden realizar búsquedas por: **Importes, Folio, Concepto, Fecha.**

6.La consulta puede ser enviada a Excel.

7.Haga clic en [Imprimir] para imprimir la relación de movimientos.
