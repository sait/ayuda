﻿+++
title = "Catálogo de Beneficiarios"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=h-oS_xkxzUg&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&index=2&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. A Catálogo de Beneficiarios.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Los beneficiarios son las personas o empresas a las cuales se emiten cheques. El catálogo de beneficiarios le permite agregar, modificar y eliminar beneficiarios.

Los nuevos beneficiarios se pueden agregar desde este catálogo o bien, la primera vez que se elabora un cheque al beneficiario el sistema lo busca dentro del catálogo y de no existir, lo agrega automáticamente al procesar el cheque.

El siguiente proceso describe los pasos para agregar, modificar o borrar beneficiarios desde el catálogo. También le permite exportar el catálogo a Excel.

<h2 class="text-primary">Agregar Nuevo Beneficiario</h2>

1.Para esto debe dirigirse al menú de Bancos/ Catálogo de Beneficiarios.

![IMG](1.png)

2.Clic en el botón [Agregar].

![IMG](2.png)

3.La clave del beneficiario es proporcionado a partir del último beneficiario que se agregó. Si lo desea, puede modificarla, a partir de ahí se llevará el consecutivo de claves.

4.Capture el nombre del beneficiario. (Este dato es de suma importancia, ya que a este nombre saldrá impreso en el cheque).

5.Capturar el RFC del beneficiario.

6.Por último, clic en [Agregar]

![IMG](3.png)

<h2 class="text-primary">Eliminar Beneficiarios</h2>

1.Para esto debe dirigirse al menú de Bancos/ Catálogo de Beneficiarios.

2.Ubicar el cursor en la clave del beneficiario a eliminar o bien buscar desde la opción [F2] en caso de no encontrar rápidamente el beneficiario a eliminar.

![IMG](4.png)

3.Por último deberá dar clic en el botón Eliminar

![IMG](5.png)

4.Clic nuevamente en "Eliminar" y listo

![IMG](6.png)

<h2 class="text-primary">Eliminar Beneficiarios</h2>

1.Ingrese al menú de Bancos / Catálogo de Beneficiarios y seleccione el beneficiario que desea modificar.

2.Una vez seleccionado dar clic en la opción [Modificar].

![IMG](7.png)
 
3.Realizar las modificaciones necesarias en la ventana y dar clic en [Modificar]

![IMG](8.png)




