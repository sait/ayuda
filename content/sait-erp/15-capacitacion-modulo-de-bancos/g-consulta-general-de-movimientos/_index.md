﻿+++
title = "Consulta General de Movimientos"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=aIs8OKrzXBw&list=PLhfBtfV09Ai61H9LMmlDQ9JyPG8icTMua&index=8&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="15. G Consulta General de Movimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Esta opción permite realizar consultas generales de los movimientos registrados a una o varias cuentas bancarias siempre y cuando cumplan con las restricciones establecidas en la consulta.

1.Ingrese al menú de Bancos / Consulta General de Movimientos.

2.Capture la clave de la cuenta bancaria. Haga clic en [?] o presione la tecla [F2] para buscar.

3.Especifique el tipo de movimiento bancario. Este dato se utiliza si en su empresa no se maneja bancos enlazado a contabilidad.

4.Capture la clave del beneficiario. Haga clic en [?] o presione la tecla [F2] para buscar.

5.Especifique el rango de fechas de los movimientos.

6.Especifique el rango de fechas programadas para pago de los movimientos. Este dato se captura al elaborar un cheque-póliza.

7.Haga clic en [Consultar]

8.Se muestran los movimientos que cumplen con las restricciones. La información que se muestra es: **Fecha, Tipo, Folio, Concepto, Beneficiario y el importe del Depósito o Retiro.**

9.El resultado de la consulta puede enviarse a Excel.


![IMG](1.png)
