+++
title = "Crear Empresa"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=dFG2UkuMi48&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="crear-empresa.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para crear una nueva empresa siga las siguientes instrucciones.

1.Al iniciar el sistema se muestra la siguiente ventana con la empresa DEMO disponible.


2.Para crear una nueva empresa hay que seleccionar la opción **[Catálogo de Empresas]**.

![IMG](catempresa.png)

3.Seleccionar la opción de **[Crear Nueva Empresa]**.

![IMG](crearnuevaempresa.png)

4.En esta ventana se debe definir el nombre de la empresa y su directorio.

Dar clic en el botón **[Crear archivos]**.

![IMG](creararchivos.png)

5.Dar clic en **[SI]** para crear la empresa.

![IMG](confirmarcreacion.png)

6.Seleccionar la empresa que se acaba de crear y dar clic en el botón **[Accesar Empresa]** y **[Continuar]**

En caso de que le pida actualizar base de datos, deberá de dar clic en **[Actulizar Base de Datos]** para continuar

![IMG](accederempresa.png)

7.Para terminar de crear la empresa, se deberán ingresar los datos generales de la empresa en la siguiente ventana.

Dar clic en **[Continuar]**.

![IMG](datosempresa.png)

8.Dar clic en **[Empezar a usar SAIT]**.

![IMG](empezar.png)

