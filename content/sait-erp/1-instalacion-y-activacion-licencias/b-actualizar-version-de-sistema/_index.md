+++
title = "Actualizar Versión de Sistema"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=PxD36cy2oIQ&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="1. B Actualizacion de versión de sistema.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

**En algunos cambios de versiones el sistema pide una nueva activación de licencia, por ello es necesario cuente con su contrato y contraseña a la mano**

Para actualizar la versión del sistema siga las siguientes instrucciones:

1.Descargar el archivo ejecutable según el paquete SAIT que tenga instalado:

Para verificar su paquete de SAIT, así como la versión ingrese en el Menú a la sección de:

Ayuda / Acerca de Sait /

Donde aparecerá la siguiente ventana con la información.

![IMG](versionSaitERP.png)

Una vez verificado el paquete que tenemos tendrá que descargar el paquete correspondiente:

**Sait ERP:** [https://sait.mx/download/2025/sait.exe](https://sait.mx/download/2025/sait.exe)

Otros paquetes:

**Sait Básico:** [https://sait.mx/download/2025/saitbasico.exe](https://sait.mx/download/2025/saitbasico.exe)

**Sait Nómina:** [https://sait.mx/download/2025/saitnom.exe](https://sait.mx/download/2025/saitnom.exe)

**Sait Contabilidad:** [https://sait.mx/download/2024/saitcont.exe](https://sait.mx/download/2024/saitcont.exe)

Es necesario Guardar el archivo dentro de la carpeta de SAIT. Para esto debe de estar cerrado el Sistema.

Si el archivo se guardó automáticamente en Descargas, hay que seleccionar el archivo, dar clic derecho y **[Copiar]**, o CTRL C.

![IMG](descargarexe.png)

2.Identificar el directorio del sistema y abrir la carpeta.

C:\SAIT

Presionar CTRL V **[Pegar]** o Clic derecho dentro de la carpeta y dar clic en **[Pegar]**.

Como este archivo ya existe, hay que reemplazarlo.

![IMG](pegarexe.png)

3.Dar doble clic en el icono de SAIT para abrir el sistema.

Se mostrará la siguiente ventana y seguiremos las siguientes instrucciones: 

![IMG](1.jpg)

![IMG](2.jpg)

4.Seleccionar la empresa y presionar el botón de **[Accesar Empresa]**.

![IMG](seleccionarempresa.png)

5.Dar clic en **[Continuar]**.

![IMG](cotinuar.png)

6.Para continuar es necesario Actualizar la base de datos del sistema.

Dar clic en **[Aceptar]**.

![IMG](actualizarBD.png)

7.Cuando este proceso se realiza se recomienda **[Respaldar Información]**.

Presionar el botón de **[Actualizar base de datos]**.

![IMG](respaldarinfo.png)

8.Dar clic en **[SI]** para confirmar la actualización de las bases de datos.

![IMG](confirmar1.png)

9.Dar clic en **[SI]** para continuar.

![IMG](confirmar2.png)

10.Dar clic en **[SI]** para confirmar que ya se realizó el respaldo.

![IMG](confirmar3.png)

11.Una vez que se haya terminado el proceso de actualización se muestra el siguiente mensaje.

Dar clic en **[Aceptar]**.

![IMG](procesoterminado.png)

12.Presionar el botón de **[Cerrar]** una vez que se haya finalizado el proceso de actualización.

Después de esto último ya puede entrar al sistema sin ningún problema.

![IMG](cerrar.png)

<h2 class="text-primary">Instalación Manual de librerias</h2>

En ciertas versiones el sistema le pedirá descargar nuevas librerías, las cuales deberían de descargarse de manera automática, pero en caso de no ser así y se muestre alguno de los siguientes mensajes deberá seguir las instrucciones que se indicarán a continuación dependiendo el mensaje de error.

## Librerias libxslt

Si se muestra el siguiente mensaje deberá seguir estos pasos:

![IMG](LIBXSLT.jpg)

1.Descargar las librerías entrando al link indicado en el mensaje de error

2.Diríjase a sus descargas y abra el archivo comprimido, seleccione todos los elementos y péguelos en una nueva carpeta que llamará 'libxslt'

![IMG](extraer.PNG)

3.Vaya a la carpeta de instalación del sistema, si tiene dudas cuál es, de clic derecho sobre el acceso directo de SAIT y seleccione "Abrir Ubicación del Archivo"

![IMG](UBICACION.jpg)

Observaremos que hay una carpeta llamada libxslt

![IMG](SAITLIBXSLT.PNG)

Vaya a descargas y copie la carpeta libxslt con los archivos que acaba de descomprimir  y péguela en esta ubicación

Si el sistema le indica que ya hay una carpeta con dicho nombre y le pregunta si desea reemplazar, dará clic en Sí 

![IMG](reemplazar.png)

## Librerias SATPROD

Si se muestra el siguiente mensaje deberá seguir estos pasos:

![IMG](SATCAT.jpg)

1.Descargar las librerías entrando al link indicado en el mensaje de error

2.Diríjase a sus descargas y descomprima su contenido en la carpeta donde se encuentra la información de la empresa.

2.1	Para conocer la ruta en donde están los archivos de su empresa deberá entrar a SAIT y dar clic en **[Catálogo de Empresas]**.

![IMG](catalogoempresas.png)

2.3 Copiar la ruta de la empresa, por ejemplo: C:\SISTEMASAIT\SAIT\DEMO\       

![IMG](copiarruta.png)

2.4 Vaya al inicio de la computadora en el ícono de Windows, pegue la ruta y presione **[Enter]** en su teclado.

![IMG](iniciowindows.png)

2.5 De tal manera que se abra la carpeta con los archivos de la empresa, aquí deberá arrastrar los archivos descomprimidos de la carpeta satprod.zip 

El sistema le va a preguntar si desea reemplazar los archivos y deber dar clic en **[Si]**

![IMG](reemplazarsatprod.png)
