﻿+++
title = "Variables y Asientos Contables en Contabilidad Básica"
description = ""
weight = 20
+++

### **Indicaciones**:

En esta sección explicaremos como instalar el conjunto de Variables y Asientos contables que regularmente se manejan en todas las integraciones de contabilidad electrónica.

{{%alert success%}}Esto con el objetivo de facilitar las operaciones y configuraciones de sus pólizas contables y con esto agilizar todas las operaciones para la generación automática y semiautomática de pólizas.{{%/alert%}}


* [Instalación de Archivos de Variables y Asientos](#instalación-de-archivos-de-variables-y-asientos)
* [Pólizas a Generar en Contabilidad Básica](#pólizas-a-generar-en-contabilidad-básica)
* [Visualización de Variables y Asientos de: VENTAS](#visualización-de-variables-y-asientos-de-ventas)
* [Visualización de Variables y Asientos de: COMPRAS](#visualización-de-variables-y-asientos-de-compras)
* [Visualización de Variables y Asientos de: GASTOS](#visualización-de-variables-y-asientos-de-gastos)


### **Instalación de Archivos de Variables y Asientos**

Con el objetivo de no capturar y definir las variables y asientos contables de manera manual, SAIT Software Administrativo coloca a tu disposición ciertos archivos y tablas del sistema listas solo para instalar en tu directorio de datos dichos archivos.

{{%alert success%}}La descarga de la Contabilidad Básica la puedes realizar a través del siguiente link: [Clic Aquí](ContabSAIT.zip){{%/alert%}}

Una vez descargado dicho archivo adjunto, deberás de **DESCOMPRIMIRLO**. Ya descomprimido el archivo, deberás de colocar el contenido de dicha carpeta en el directorio en donde se encuentra la información de tu empresa en SAIT.

Si tienes duda en que parte se ubica la información de tu empresa, puedes seguir los siguientes pasos:

* Accesar a SAIT ERP.
* Dirigirse al menú: **Utilerías / Configuración General del Sistema**.
* ![1](IMAGEN1.PNG)
* Dirigirse a la pestaña de **EMPRESA** y localizar el campo: **"Directorio de la Empresa"**.
* Tomar nota de la ruta y hacer clic en el botón **[Cerrar]**.

### **Pólizas a Generar en Contabilidad Básica**

Las pólizas que se contemplan en la generación automática son las siguientes:

{{%alert success%}}Póliza de Diario (Facturas y Notas de Crédito)<br>Póliza de Compras.(Provisión)<br>Póliza de Gastos.(Provisión){{%/alert%}}

**Mismas que ya vienen por default en los archivos que previamente instalo dentro de la aplicación de SAIT ERP.**

### **Visualización de Variables y Asientos de Ventas**

Pasos para Visualizar las Variables y Asientos Contables de Ventas:

* Dirigirse al menú de: **Contabilidad / Configurar Enlace Contable / Ventas**. Se visualizará la siguiente ventana:
* ![1](IMAGEN2.PNG)
* Hacer clic sobre el Renglón de Ventas. Se visualiza la siguiente ventana:
* ![1](IMAGEN3.PNG)
* Como se puede observar en la imagen, se encuentra la configuración típica de la afectación de la póliza de DIARIO de nuestras ventas.
* Para visualizar el conjunto de variables a utilizar en nuestros asientos contables, deberá hacer clic sobre el botón [Variables]. Se muestra la siguiente ventana:
* ![1](IMAGEN4.PNG)
* En donde se puede visualizar claramente, el conjunto de variables definidas que podemos utilizar en cada uno de nuestros Asientos Contables. Mismas que en caso de ser necesario puedes utilizar, si deseas hacer algún ajuste a la contabilidad básica.
* Hacer clic en el botón **[Grabar]**.
* En la ventana, usted puede hacer el ajuste y cambiar el número de cuenta contable en relación a su catálogo de Cuentas Contables que usted maneje.
* Una vez hecho los cambios, hacer clic en el botón **[Grabar]**.

### **Visualización de Variables y Asientos de Compras**

Pasos para Visualizar las Variables y Asientos Contables de Compras:

* Dirigirse al menú de: **Contabilidad / Configurar Enlace Contable / Compras**. Se visualizará la siguiente ventana:
* ![1](IMAGEN5.PNG)
* Hacer clic sobre el Renglón de Compras. Se visualiza la siguiente ventana:
* ![1](IMAGEN6.PNG)
* Como se puede observar en la imagen, se encuentra la configuración típica de la afectación de la póliza de DIARIO de nuestras compras.
* Para visualizar el conjunto de variables a utilizar en nuestros asientos contables, deberá hacer clic sobre el botón [Variables]. Se muestra la siguiente ventana:
* ![1](IMAGEN7.PNG)
* En donde se puede visualizar claramente, el conjunto de variables definidas que podemos utilizar en cada uno de nuestros Asientos Contables. **Mismas que en caso de ser necesario puedes utilizar, si deseas hacer algún ajuste a la contabilidad básica**.
* Hacer clic en el botón **[Grabar]**.
* En la ventana, usted puede hacer el ajuste y cambiar el número de cuenta contable en relación a su catálogo de Cuentas Contables.
* Una vez hecho los cambios, hacer clic en el botón **[Grabar]**.



### **Visualización de Variables y Asientos de Gastos**

Pasos para Visualizar las Variables y Asientos Contables de Gastos:

* Dirigirse al menú de: **Contabilidad / Configurar Enlace Contable / Gastos**. Se visualizará la siguiente ventana:
* ![1](IMAGEN8.PNG)
* Hacer clic sobre el Renglón de alguno de nuestros Gastos. Para efectos de esta explicación tomaremos el tipo de gasto "PAPELERIA" Se visualiza la siguiente ventana:
* ![1](IMAGEN9.PNG)
* Como se puede observar en la imagen, se encuentra la configuración típica de la afectación de la póliza de DIARIO de nuestro gasto **PAPELERIA**.
* Para visualizar el conjunto de variables a utilizar en nuestros asientos contables, deberá hacer clic sobre el botón [Variables]. Se muestra la siguiente ventana:
* ![1](IMAGEN10.PNG)
* En donde se puede visualizar claramente, el conjunto de variables definidas que podemos utilizar en cada uno de nuestros Asientos Contables. Mismas que en caso de ser necesario puedes utilizar, si deseas hacer algún ajuste a la contabilidad básica.
* Hacer clic en el botón **[Grabar]**.
* En la ventana, usted puede hacer el ajuste y cambiar el número de cuenta contable en relación a su catálogo de Cuentas Contables.
* Una vez hecho los cambios, hacer clic en el botón **[Grabar]**.
