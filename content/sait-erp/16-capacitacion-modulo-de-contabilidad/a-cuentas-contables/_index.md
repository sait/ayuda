﻿+++
title = "Cuentas Contables"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=yQR3v-srFEQ&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. A Cuentas Contables.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El catálogo principal del módulo de la contabilidad, es el catálogo de cuentas contables, el cual permite almacenar nuevas cuentas contables, modificar las cuentas ya existentes y eliminar todas aquellas cuentas contables que no sean necesarias, siempre y cuando no cuenten con ningún movimiento en algún póliza. 

Es importante mencionar que previamente en el menú de:

**Utilerías / Configuración General del Sistema / Pestaña de Contabilidad**

Debe establecer la estructura o máscara de cuenta contable que usted va a utilizar a utilizar en su empresa, ya que en base a este tipo de estructura el sistema le va a permitir agregar subniveles a las cuentas de mayor.


![IMG](1.png)

<h2 class="text-primary">Agregar Cuenta de Mayor</h2>

Para agregar una nueva cuenta contable debe realizar lo siguiente:

1.Entre al menú de Contabilidad / Cuentas Contables.

2.Se mostrará la siguiente ventana:

![IMG](2.png)

3.Presione el botón de agregar.

![IMG](3.png)

4.Indique la cuenta contable y presione [Enter].

5.En este momento aparecerá el asistente para crear una cuenta de mayor.

6.Debe seleccionar el tipo de cuenta a crear y presionar [Enter].

![IMG](4.png)

7.Automáticamente el sistema colocará esta descripción en la ventana.

![IMG](5.png)

8.Debe establecer el tipo de cuenta al que pertenece la cuenta contable.

9.Debe seleccionar la naturaleza de la cuenta **(Deudora o Acreedora).**

10.Especificar la clasificación SAT. Presione F2 para buscar.

![IMG](6.png)

11.Y por último presione el botón de [Agregar].

<h2 class="text-primary">Agregar Subcuenta</h2>

Para agregar una subcuenta, presione nuevamente botón de [Agregar], en esta ventana:

![IMG](7.png)

1.Indique la cuenta de mayor, guión medio (-) y el número consecutivo que desea agregar. Ejemplo: **1001-0001**

![IMG](8.png)

2.Automáticamente el sistema detectará el tipo de cuenta que desea crear y su naturaleza. 

3.Deberá capturar la descripción de la subcuenta.

4.Y especificar su clasificación SAT correspondiente. Presione el icono [?] de signo de interrogación o presione [F2] para buscar.

![IMG](9.png)

5.Finalmente, presione el botón de [Agregar].

<h2 class="text-primary">Modificar una Cuenta Contable</h2>

Para modificar una cuenta contable debe realizar lo siguiente:

1.Entre al menú de Contabilidad / Cuentas Contables.

2.Debe estar posicionado sobre el registro que desee modificar.

![IMG](10.png)

3.Presione el botón de [Modificar].
 
4.Sobre esta ventana, realice las modificaciones necesarias.

![IMG](11.png)

5.Y de clic en el botón de [Modificar].

<h2 class="text-primary">Eliminar una Cuenta Contable</h2>

Para eliminar una cuenta contable debe realizar lo siguiente:

1.Entre al menú de Contabilidad / Cuentas Contables.

2.Debe estar posicionado sobre el registro que desee eliminar.

![IMG](12.png)

3.Presione el botón de [Eliminar].

4.A manera de confirmación, el sistema mostrará los datos de la cuenta contable que desea eliminar. 

<h3 class="text-danger">NOTA</h3>

5.Se recomienda eliminar solamente cuenta contables que NO tengan ningún movimiento en el sistema.

![IMG](13.png)

6.Si está totalmente seguro de que desea eliminar la cuenta contable, haga clic en el botón de [Eliminar].