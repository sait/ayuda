﻿+++
title = "Póliza de Cierre de Ejercicio"
description = ""
weight = 10
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=rMlkdFa-7T0&list=PLhfBtfV09Ai4JH5ywKBuuqQ8KU0fDpdaW&index=9" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. L Importar cuentas contables.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

La generación de la póliza de cierre en SAIT, básicamente consiste en saldar las cuentas de resultados y determinar la pérdida o ganancia contable del ejercicio que se está cerrando, con opción de pasar el saldo o diferencia a la respectiva cuenta de Resultado del Ejercicio. 

<h2 class="text-primary">Puntos Previos a Considerar</h2>

Es muy importante que antes de realizar la póliza de cierre, cree la cuenta contable que almacenará el Resultado de Ejercicio Anterior. 

Para definir una cuenta se puede guiar de la siguiente liga: https://ayuda.sait.mx/sait-erp/16-capacitacion-modulo-de-contabilidad/a-cuentas-contables/

<h2 class="text-primary">Proceso de Generación de Póliza de Cierre </h2>

1.Dirigirse al menú de Contabilidad / Utilerías / Póliza de Cierre del Ejercicio. 

2.Aparecerá la siguiente ventana: 

![IMG](1.png)

3.Indicar el Año del ejercicio a cerrar y hacer clic en el botón [Mostrar Póliza] 

4.Aparecerá la siguiente ventana:

![IMG](2.png)

5.Indicar la **Fecha del Registro** de la póliza. 

6.Seleccionar el **Tipo de Póliza** a generar. (Aquí se recomienda ampliamente que sea del tipo: Diario). 

7.Capturar el **Folio** de la póliza de cierre. 

8.Posteriormente usted visualizará un monto debajo de la columna de: CONCEPTO, ese monto total representa la diferencia entre el total de CARGOS y ABONOS y es el que debe de dirigir hacia la cuenta contable de resultado de ejercicios anteriores. 

9.Una vez capturado el monto de la diferencia, usted notará que la póliza se cuadra. 

10.Para procesar la póliza de cierre, hacer clic en el botón [Guardar=F8] 

11.Listo, nuestra póliza de cierre fue generada exitosamente. 
