﻿+++
title = "Organizador de Comprobantes Fiscales (OCF)"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=Ifx3rC9d9uY&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. F OCF.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT ERP cuenta con la herramienta de Organizador de Comprobantes Fiscales, en donde su funcionalidad es organizar todos los comprobantes **RECIBIDOS y EMITIDOS** para todas las razones sociales que esté manejando.

Es importante poder identificar en todo momento con facilidad, los comprobantes que se utilizarán para ser contabilizados o incluidos dentro de alguna de las pólizas, registro de gastos o compras.

Es por eso que SAIT te ofrece la posibilidad de consultar y visualizar cada uno de tus comprobantes fiscales digitales, es decir, visualizar la estructura del XML y PDF original de nuestro proveedor.

### NOTA

Para hacer uso de esta herramienta, previamente deberá haber seguido las instrucciones de instalación del Organizador de Comprobantes Fiscales en su servidor, instrucciones en el siguiente enlace: 

https://ayuda.sait.mx/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/

Para acceder al visor del OCF, deberá:

1.Ingresar al menú de Contabilidad / Organizador de Comprobantes Fiscales.  

![IMG](1.png)

2.En esta ventana puede especificar si desea consultar los comprobantes EMITIDOS o RECIBIDOS.

![IMG](2.png)

3.Puede filtrar la información por EMISOR o RECEPTOR (según sea el caso), presionando la tecla de F2 para buscar por nombre.

![IMG](3.png)

4.Puede consultar por rango de fechas.

5.Presione el botón de [Consultar] para que el sistema arroje información según los filtros colocados.

![IMG](4.png)

6.En esta ventana usted podrá presionar el botón de [Ver XML] para ver la estructura XML del registro en el que se encuentre posicionado.

![IMG](5.png)

7.También puede presionar el botón de [Ver PDF] para ver el archivo PDF original de comprobante.

![IMG](6.png)

8.O bien, puede presionar el botón de [Excel] para enviar toda la consulta a Excel.

![IMG](7.png)

9.La información que se mostrará en esta ventana de consulta es:

* a.	RFC del emisor o receptor

* b.	Nombre del emisor o receptor

* c.	Fecha de registro del comprobante

* d.	Folio interno del comprobante fiscal

* e.	Tipo de comprobante: Ingreso, Egreso, Pago, Nómina, Traslado…

* f.	El importe total del comprobante

* g.	Folio fiscal UUID del comprobante

* h.	Y las pólizas en las que se ha asociado dicho comprobante


Esta ventana de consulta es muy útil cuando se tiene la opción de descargar automáticamente los comprobantes recibidos por parte de los proveedores, para poder visualizar su ya fueron descargados de forma automática y si ya se encuentran disponibles para poderlos utilizarlos en los registros de gastos, de compras o de pólizas.