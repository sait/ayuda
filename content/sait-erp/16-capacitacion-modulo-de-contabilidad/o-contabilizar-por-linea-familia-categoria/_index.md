﻿+++
title = "Contabilizar por línea, familia o categoría"
description = ""
weight = 18
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>

<div class="card">
<a href="contabilizar-por-linea-familia-categoria.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT se cuenta con diferentes formas de llevar la contabilidad de cada empresa de tal modo que el proceso quede adaptado a las necesidades de cada una.

Una las formas de contabilizar las ventas es por línea, familia o categoría lo cual permite contabilizar compras o ventas agrupadas por la clasificación de los artículos.

{{%alert success%}}**Consideraciones especiales:**<br><br> * La clave de la línea, familia o categoría debe ser numérica.<br> * La sub-cuenta contable para las ventas debe corresponder a la clave de la línea, familia o categoría. Ejemplo: La cuenta 4000 corresponde a las ventas.{{%/alert%}}

| **Línea** | **Nombre** | **Cuenta** |
| --- | --- | --- |
| 1 | QUIMICOS | 4000-0001 |
| 2 | PAPEL | 4000-0002 |
| 3 | EQUIPOS | 4000-0003 |

<h2 class="text-primary">Definición de Variables</h2>

**Se debe agregar la definición de las variables que se van a incluir en la definición de los asientos de las pólizas.**

* Ingrese al menú de: **Contabilidad / Configurar Enlace Contable / Ventas por Partidas**.
* Se mostrará la siguiente ventana. Deberá dar clic en: **[Agregar]**.
* ![IMG](imagen1.PNG)
* En la siguiente ventana deberá dar clic en el botón de: **[Variables]**
* ![IMG](imagen2.PNG)
* Deberá dar clic en: **[Insertar]** para agregar las variables que usted requiera.
* ![IMG](imagen3.PNG)
* La definición de variables que se propone es la siguiente:

| **Nombre** | **Fórmula** |
| --- | --- |
| IEsFactura | Allt(Docum.TIPODOC) == &#39;F&#39; |
| IEsCompra | Allt(Docum.TIPODOC) == &#39;C&#39; |
| IEsNota | Allt(Docum.TIPODOC) == &#39;N&#39; |
| nImporte | Movim.CANT \* Movim.PRECIO \* (1-Movim.PJEDESC/100) |
| nIVA | nImporte \* (Movim.IMPUESTO1/100) |
| nTotal | nImporte + nIVA |
| cCtaVtaLinea | &#39;4000-&#39; + Allt(Arts.LINEA) |
| cConcVtaLinea | &#39;Ventas del día de &#39; + Allt(Lineas.NOMLIN) |


<h2 class="text-primary">Definición de Asientos Contables</h2>

**Una vez definidas las variables, se deben configurar los asientos para contabilizar la póliza.**

* Entre al menú de: **Contabilidad / Configurar Enlace Contable / Ventas por Partidas.**
* Haga clic en: **[Agregar]**.
* ![IMG](imagen4.PNG)
* La definición de asientos contables que se propone es la siguiente:

| **Cuenta** | **Descripción** | **CA** | **Importe** | **Concepto** | **Agrupar Asientos** | **Agrupa Por** |
| --- | --- | --- | --- | --- | --- | --- |
| 1002-0002 | BANAMEX 8292091 | A | nTotal | Ventas del Día | X |
 |
| 2003-0001 | IVA TRASLADADO (IVA Trasladado) | C | nIVA | IVA del Día | X |
 |
| cCtaVtaLinea | | C | nImporte | cConcVtaLinea | X | cCtaVtaLinea |


<h2 class="text-primary">Definir Póliza a Generar</h2>

Una vez definidas las variables y asientos, solo resta definir el tipo de póliza a generar.

* Entre al menú de: **Contabilidad / Configurar Enlace Contable / Pólizas a Generar.**
* Haga clic en **[Agregar]**.
* Defina la póliza de la siguiente manera:
* ![IMG](imagen5.PNG)
* Haga clic en **[Agregar]** nuevamente para grabar la configuración.


<h2 class="text-primary">Generar Póliza</h2>

* Ingrese al menú de: **Contabilidad / Generar Póliza de Operaciones.**
* ![IMG](imagen6.PNG)
* Seleccione el tipo de póliza: **Ventas por Líneas.**
* Especifique el día de las ventas que se van a incluir en la póliza.
* Haga clic en: **[Ver Póliza]**
* La póliza se genera de la siguiente forma:
* ![IMG](imagen7.PNG)
