﻿+++
title = "Función Cargardocumento para contabilizar"
description = ""
weight = 12
+++


A partir de versiones recientes, se facilitó la obtención de variables para la contabilización de documentos, mediante el uso de la función **CargarDocumento**.


{{%alert success%}}Le recordamos que para configurar las variables de contabilización, debe ir al menú de: **Contabilidad / Configurar Enlace Contable / Variables**.{{%/alert%}}

* [Definición y Función CargarDocumento](#función-cargardocumento)
* [Lista de Variables Disponibles](#lista-de-variables-disponibles)
* [Uso de Variables de Contabilización en Reportes](#uso-de-variables-de-contabilización-en-reportes)
* [Uso de Variables de Contabilización en Formatos](#uso-de-variables-de-contabilización-en-formatos)


### Función CargarDocumento

La función **CargarDocumento**, permite crear una variable tipo objeto, la cual contiene muchas variables relacionadas con el documento a contabilizar, ya sea una compra o una venta.

{{%alert success%}}Ejemplo de algunas variables que puede definir:{{%/alert%}}

| Nombre | Fórmula |
| --- | --- |
| Factura | Docum.NUMDOC |
| Proveedor | Provedor.NOMPROV |
| ProvedorFactura | Allt(Left(Provedor.NOMPROV,20))+&#39; &#39;+Allt(Docum.REFER) |
| CuentaDelProvedor | Provedor.NUMCTA |
| oDoc | CargarDocumento( Docum.TIPODOC+Docum.NUMDOC, 1) |
| nIva | oDoc.Impuestos.Iva10 + oDoc.Impuestos.Iva11 + oDoc.Impuestos.Iva15 + oDoc.Impuestos.Iva16 |
| nSubTotal | oDoc.Impuestos.GravadosIva0 + oDoc.Impuestos.GravadosIva10 + oDoc.Impuestos.GravadosIva11 + oDoc.Impuestos.GravadosIva15 + oDoc.Impuestos.GravadosIva16 + oDoc.Impuestos.ExentosIva |
| nTotal | oDoc.Impuestos.Total |

La función CargarDocumento puede mandarse en 2 modalidades:

* **oDoc = CargarDocumento(cKeyDoc)**

Cargará todo el documento completo,inclusive un arreglo de partidas con los nodos de artículos, cliente, movimiento al inventario.

* **oDoc = CargarDocumento(cKeyDoc, 1)**

Solamente cargará la información del nodo de impuestos (Es decir aquellas que empiezan con oDoc.Impuestos.) Esto hace que la función sea más rápida porque no carga los demás nodos. Está opción es buena usarla en caso de que sean muchos los documentos a contabilizar y el proceso se esté mostrando que toma tiempo.


### Lista de Variables Disponibles

En caso de cargar todo el documento, se tienen las siguientes variables:

oDoc = CargarDocumento(cKeyDoc)

#### Campos de la tabla DOCUM

* oDoc.NumCli
* oDoc.Importe
* oDoc.Impuesto1
* oDoc.Impuesto2

#### Datos adicionales del documento, configurados para la empresa:

* oDoc.OtrosDatos.Get('Color')
* oDoc.OtrosDatos.Get('Fecha_Entrega')
* oDoc.OtrosDatos.Get('Depto_Liverpool')

	
#### Campos de la tabla CLIENTES

* oDoc.Cliente.NumCli
* oDoc.Cliente.NomCli
* oDoc.Cliente.Calle
* oDoc.Cliente.Sucursal.NumCliSuc
* oDoc.Cliente.Sucursal.Nombre
* oDoc.Cliente.Sucursal.Direccion

#### Las siguientes variables están expresadas en la divisa del documento:

* oDoc.ExentosIva
* oDoc.GravadosIva0
* oDoc.GravadosIva10
* oDoc.GravadosIva11
* oDoc.GravadosIva15
* oDoc.GravadosIva16
* oDoc.GravadosIva8
* oDoc.Iva16
* oDoc.Iva8
* oDoc.Total

#### Nodos de Impuestos

Las siguientes variables está expresadas en pesos al tipo de cambio del diario oficial de la federación:

* oDoc.Impuestos.ExentosIva
* oDoc.Impuestos.GravadosIva0
* oDoc.Impuestos.GravadosIva10
* oDoc.Impuestos.GravadosIva11
* oDoc.Impuestos.GravadosIva15
* oDoc.Impuestos.GravadosIva16
* oDoc.Impuestos.GravadosIva8
* oDoc.Impuestos.Iva10
* oDoc.Impuestos.Iva11
* oDoc.Impuestos.Iva11
* oDoc.Impuestos.Iva15
* oDoc.Impuestos.Iva16
* oDoc.Impuestos.Iva8
* oDoc.Impuestos.Total

#### Las partidas del documento

* oDoc.Partidas[n].CamposDeMovim
* oDoc.Partidas[n].Articulo.CamposDeArts
* oDoc.Partidas[n].Minv.CamposDeMinv

**Por Ejemplo:**

* oDoc.Partidas[1].TipoDoc
* oDoc.Partidas[1].NumDoc
* oDoc.Partidas[1].NumPar
* oDoc.Partidas[1].Articulo.NumArt
* oDoc.Partidas[1].Articulo.Desc
* oDoc.Partidas[1].Articulo.PrecioPub
* oDoc.Partidas[1].Minv.Costo


### Uso de Variables de Contabilización en Reportes

Para poder usar estas variables en reportes de documentos, es importante tomar en cuenta lo siguiente:

* Entrar al formato del reporte que desea modificar.
* Estando dentro del reporteador seleccionar la opción del menú: Report / Variables
* Crear las siguientes 2 variables en el reporte:

| Variable | Valor Inicial (Inital Value) | Valor a Almacenar (Value to store) | Cálculo (Calculate) |
| --- | --- | --- | --- |
| oDoc | 0 | 1 | Nothing |
| dumy | 0 | cargardocumento(Docum.TIPODOC+Docum.NUMDOC,1,@oDoc) | Nothing |

Una vez definidas esas 2 variables usted podrá agregar a su banda de detalle, cualquiera de las expresiones mencionadas en la lista de variables de la función CargarDocumento.

{{%alert success%}}**CUIDADO** Estas indicaciones solo aplican para reportes en donde el detalle o renglones son facturas. <BR> **NO USAR** estas indicaciones cuando el detalle o renglones son partidas de la factura.{{%/alert%}}

### Uso de Variables de Contabilización en Formatos

Para poder usar las variables de contabilización en formatos de facturas, compras, etc, haga lo siguiente:

* Entrar al formato que desea modificar.
* Estando dentro del reporteador seleccionar la opción del menú: Report / Variables
* Crear las siguiente variables en el reporte:

| Variable | Valor Inicial (Inital Value) | Valor a Almacenar (Value to store) | Cálculo (Calculate) |
| --- | --- | --- | --- |
| dumy | 	DoCmd('Public oDoc')+DoCmd('oDoc = CargarDocumento(Docum.TIPODOC+Docum.NUMDOC)') | 0 | Nothing |

{{%alert success%}}Una vez definida esa variable, al iniciar la impresión del formato se creará la variable oDoc, y usted podrá usar las expresiones indicadas arriba, en cualquier parte del formato.{{%/alert%}}


