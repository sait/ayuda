﻿+++
title = "Cambiar Estructura de Cuentas Contables"
description = ""
weight = 19
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>

<div class="card">
<a href="cambiar-estructura-ctas-contables.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

La reconfiguración de la estructura de Cuentas Contables consiste básicamente, en cambiar la estructura elemental de la máscara de la cuenta.

{{%alert success%}}Por mencionar algunos ejemplos mas comunes de estructura se muestran las siguientes:<br><br>**9999-9999-9999** En donde la estructura de mascara sería: 4-4-4.<br>**9999-999-999-999** En donde la estructura de mascara sería: 4-3-3-3{{%/alert%}}

El proceso consiste en cambiar la estructura de las cuentas contables actuales que se cuenten almacenadas dentro de SAIT. Siguiendo la temática, se explica un ejemplo de cambio utilizando dicha utilería.

### **Ejemplo de Reconfiguración de Estructura**

Se cuenta con una estructura actual de: **9999-999-999** y ejemplo de una cuenta sería: 1008-001-001 Banamex MN. 

{{%alert success%}}Se requiere reconfigurar la estructura a: **9999-9999-9999** y ejemplo de esa conversión sería: **1008-0001-0001** Banamex MN.{{%/alert%}}

### **Proceso de Reconfiguración de Estructura de Cuentas Contables**

* Dirigirse al menú de: **Contabilidad / Utilerías / Reconfigurar Estructura de Cuentas Contables**.
* Aparecerá la siguiente ventana:
* ![IMG](imagen1.PNG)
* Deberá de leer las indicaciones mostradas en la ventana, entre las cuales y muy importantes son: Generar un respaldo de la información, Imprimir reportes contables para verificar saldos antes del proceso, entre otros.
* Capturar la estructura nueva máscara de la cuenta contable.
* Para iniciar con el proceso de conversión de la nueva estructura, hacer clic en el botón **[Modificar]**
* A manera de confirmación, aparece el siguiente mensaje:
* ![IMG](imagen2.PNG)
* Hacer clic en el botón **[SI]**.
* En esos momentos, al finalizar el proceso deberá de indicar el siguiente mensaje:
* ![IMG](imagen3.PNG)
* Listo!!!, La estructura del catálogo de cuentas contables cambio a la nueva especificada.





