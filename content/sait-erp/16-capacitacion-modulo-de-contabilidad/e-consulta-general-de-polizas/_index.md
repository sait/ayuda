﻿+++
title = "Consulta General de Pólizas"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=xiqo-SdbpJY&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. E Consulta General de Póliza.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Esta ventana te permite realizar una consulta general de las pólizas registradas en el sistema en un período determinado. 

1.Ingrese al menú de Contabilidad / Consulta General de Pólizas.

![IMG](1.png)

2.La consulta la puede realizar haciendo los siguientes filtros:

* Tipo de Póliza en especifico

* Por CFDIs asociados: Todos, Sin CFDIs o Con CFDIs.

* Rango de folios

* Rango de fechas

* Cuenta bancaria en especifico 

* Conceptos incluidos en las pólizas

3.Si se dejan en blanco todos los filtros, la consulta se realizará para todas las pólizas registradas en el sistema. 

4.Presione el botón de [Consultar].

5.En esta ventana se mostrará la información según los filtros colocados en la consulta.

![IMG](2.png)

6.Puede presionar el botón de [Imprimir] para imprimir todas las pólizas incluidas en la consulta.

7.Puede presionar el botón de [Excel] para enviar toda la consulta a Excel.

8.También puede presionar [Enter] o dar doble clic sobre el registro para abrir el detallado de la póliza.

![IMG](3.png)

9.En la consulta individual de la póliza usted podrá modificarla, eliminarla o registrarla como una póliza frecuente.


