﻿+++
title = "Capturar Póliza de Saldos Iniciales"
description = ""
weight = 17
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>

<div class="card">
<a href="poliza-de-saldos-iniciales.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El primer pasó que se debe de realizar al estar generando la integración contable dentro de SAIT, es capturar la póliza de Saldos Iniciales. Regularmente las empresas que inician con este proceso imprimen o generan una Balanza de Comprobación en su anterior sistema contable, el cual sirve como base para capturar los saldos dentro de SAIT.

<h2 class="text-primary">Proceso para Capturar la Póliza de Saldos Iniciales</h2>

**El proceso para capturar la póliza de Saldos Iniciales es muy sencillo y se detalla a continuación:**

* Contar con la impresión de la Balanza de Comprobación del anterior sistema contable.
* Dirigirse al menú de: **Contabilidad / Agregar Pólizas**.
* Aparecerá la siguiente ventana:
* ![IMG](imagen1.PNG)
* Capturar la fecha de registro de póliza.
* Seleccionar el Tipo de Póliza a generar (Ampliamente se recomienda que sea del tipo **DIARIO**).
* Indicar el folio de número de póliza.
* En el campo de Concepto, capturar como referencia el título: **"Póliza de Saldos Iniciales"**, seguido del año.
* Capturar cada uno de los asientos que le corresponden como saldos iniciales en la póliza, tal como se mostró en la imagen anterior.
* Una vez capturados los saldos iniciales y que nuestra póliza cuadre, hacer clic en el botón **[Guardar=F8]**.
* **Listo!!!**, nuestra póliza de Saldos Iniciales se encuentra registrada en nuestro sistema.

<h2 class="text-primary">Recomendaciones Especiales</h2>

Una vez capturada nuestra póliza de Saldos Iniciales, es primordial comparar los saldos capturados en SAIT, en contra del anterior sistema contable. Para esto, usted deberá de imprimir los siguientes reportes (Tanto en el anterior sistema contable, como en SAIT):

{{%alert success%}}1.- Balanza General.<br>2.- Balanza de Comprobación.{{%/alert%}}

Se comparan ambos reportes, si los saldos coinciden, significa que se capturaron de manera correcta dentro de SAIT y se puede proseguir con la integración contable. **"En Caso de que no coincidan, se recomienda revisar cada uno de los asientos ingresados, para determinar si se encuentran abonados o cargados correctamente en la naturaleza de la cuenta."**

