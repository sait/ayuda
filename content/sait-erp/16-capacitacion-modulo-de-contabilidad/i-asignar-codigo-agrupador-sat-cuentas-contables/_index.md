﻿+++
title = "Asignar Código Agrupador SAT a Cuentas Contables"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=eCRegng631c&list=PLhfBtfV09Ai4JH5ywKBuuqQ8KU0fDpdaW&index=8&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="16. I CLave SAT de cuentas contables.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

A partir de Julio del 2014 el SAT publicó una nueva disposición fiscal vigente, la cual consiste en enviar de manera mensual la información de tu catálogo de cuentas contables, balanza de comprobación y en ciertos casos en que el contribuyente se encuentre en auditoria o revisión por parte del SAT, éste puede solicitarnos los XML de nuestras pólizas.


Para cumplir con este objetivo, el SAT publicó un catálogo de cuentas contables universal, que va a servir como base para relacionar el catálogo de cuentas que se maneja en SAIT Contabilidad Electrónica y el catálogo publicado por el SAT.

A continuación puede visualizar la relación de ese catálogo universal: 

![IMG](cuentas-universales.JPG)

**Para descargar el catálogo completo en excel de clic en el siguiente enlace** [descargar](cuentas.xlsx)

Si usted está iniciando con el proceso de implementación de Contabilidad electrónica o bien desea clasificar una cuenta nueva, SAIT te proporciona la siguiente herramienta que te permite asignar el código agrupador SAT a todas las cuentas contables de manera masiva o de manera individual según sea su caso.

<h2 class="text-primary">Asignar código agrupador de forma individual</h2>

Para realizar este proceso de forma masiva debe:

1.Dirigirse al menú de Contabilidad / SHCP-SAT / Asignar Código Agrupador SAT a Cuentas. 

![IMG](1.png)

2.Podrá asignar la clave SAT de manera individual, posicionándose sobre la columna #SAT, y presionando la tecla [F2] para buscar la clave.

![IMG](2.png)

3.Una vez hecha la captura individualmente, deberá presionar el botón de [Guardar].

![IMG](3.png)

4.Confirme que los datos están correctos, presionando el botón de [SI].

![IMG](4.png)

5.Y en ese momento, el sistema le indicará que las cuentas han sido clasificadas correctamente.

![IMG](5.png)


<h2 class="text-primary">Asignar código agrupador de forma masiva</h2>

También puede realizar este proceso de forma masiva:

1.Debe posicionarse sobre el campo "Cuentas que inician con" e ingresar el numero de la cuenta de mayor a seleccionar y presionar la tecla ENTER.

![IMG](7.png)


2.En el campo "Clasificación" deberá de colocar el código agrupador del SAT. En caso de ser necesario, puede realizar una búsqueda por nombre haciendo clic cobre el botón [?] o presionando la tecla [F2].

![IMG](8.png)

3.Una vez seleccionado la cuenta deseada según el SAT, deberá hacer clic en el botón [Aplicar]. 


![IMG](9.png)

4.Deberá confirmar que dese aplicar la clasificación seleccionada a todas las cuentas que se muestran en pantalla.

![IMG](10.png)

5.Confirme que desea aplicar la clasificación:

![IMG](11.png)

6.Y finalmente, deberá presionar el botón de [Guardar].

![IMG](12.png)

7.Confirme que los datos están correctos, presionando el botón de [SI].

![IMG](13.png)

8.Y en ese momento, el sistema le indicará que las cuentas han sido clasificadas correctamente.

![IMG](14.png)





