﻿+++
title = "Configurar una empresa sin movimientos"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=kiGpoCktEH4&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>


<div class="card">
<a href="3. C. Configurar una empresa sin movimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


Hacer la configuración para una empresa sin movimientos es sumamente sencillo, ya que desde el inicio del trabajo de la empresa definimos el número de almacenes, la serie a utilizar y todos los documentos se elaboran y se guardan con esta información, de manera que no será necesario reubicarlas después.

Debemos cuidar los siguientes puntos:

1.Crear Almacenes  

Ingresar al módulo de Utilerías/Catalogo de Sucursales/Almacenes.

![IMG](1.jpg)

En caso de no tener ninguno, hay que crearlos.

Para crear un almacén nuevo vamos a dar clic en [Agregar]

![IMG](2.jpg)


* Capturar la clave de la sucursal. Se recomienda que se asigne un valor numérico que no exceda de dos caracteres.
* Capturar el nombre de la sucursal, y demás datos de domicilio fiscal (el código postal es un campo obligatorio).
* Indicar la clave de los grupos los cuales tendrán acceso a la Sucursal. En caso de que todos los niveles requieran el acceso en determinada, escribir la palabra TODOS.
* Capturar el folio de la siguiente salida a sucursal, en donde se especifica el folio de la letra que le corresponde al almacén.
* Para grabar los datos, hacer clic en el botón [Agregar]. Como se muestra en la siguiente imagen:

![IMG](3.jpg)

2.Marcar Usar Varios Almacenes

Utilerías/Configuración General del Sistema/Inventario

![IMG](4.jpg)

3.Marcar con qué número de almacén iniciará el sistema.

Utilerías/Configuración General del Sistema/Inventario

![IMG](5.jpg)

4.Colocar número de almacén que se usará para trabajar.

Regularmente se ingresa el mismo que en Inventario.

Utilerías/Configuración General del Sistema/SAIT Distribuido.

![IMG](6.jpg)

5.Colocar series de documentos

Ventas / Siguientes Folios

![IMG](7.jpg)

6.Dar series a tipos de movimiento en inventario

Inventario / Tipos de movimientos

Seleccionar cada tipo de movimiento de inventario e ingresar el folio siguiente.

![IMG](8.jpg)