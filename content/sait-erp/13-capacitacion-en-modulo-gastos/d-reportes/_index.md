﻿+++
title = "Reportes de Gastos"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=fze7z5_McvA&index=5&t=0s&list=PLhfBtfV09Ai6evDYeDMhYLYgGgMc_iBq7" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="13. D Reportes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


Este proceso te permite emitir reportes de gastos según las especificaciones que la empresa requiera.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Gastos / Reportes</h4>


![IMG](1.png)

1.Seleccione el tipo de reporte que requiere emitir.

2.De manera opcional usted puede filtrar la información por:

* Tipo de gasto en específico o Todos los gastos
* Sucursal en específico
* Proveedor en específico.
* Rango de folios de factura de gasto
* Rango de fechas de la factura
* Rango de fechas de captura de gasto
* Rango de fechas de vencimiento del gasto

3.Puede presionar el botón de **[Imprimir]** para seleccionar una destino de impresión:

![IMG](2.png)

4.Seleccione la opción de **[Pantalla]** para ver un previo del resultado.

![IMG](3.png)

5.También puede presionar el botón de **[Hoja]** para ver la información de la siguiente manera.

![IMG](4.png)

6.Si lo desea puede enviar la consulta a Excel presionando el icono   que se encuentra en la parte superior del reporte.