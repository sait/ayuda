﻿+++
title = "Registro de Gastos a partir de un XML"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=h-5b9tGderE&index=3&t=0s&list=PLhfBtfV09Ai6evDYeDMhYLYgGgMc_iBq7" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="13. B Registros de Gastos a partir del XML.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


Dentro de SAIT usted puede realizar el registro de gastos a través  de un XML de una forma fácil y rápida.

Para registrar un gasto a partir de un XML diríjase a:

<h4 class="text-primary">Gastos / Registro de Gastos</h4>

![IMG](1.png)

1. Posicionado sobre el campo de Folio SAIT UUID, presione la tecla **[F2]** de su teclado para buscar un XML. 

2. Presione nuevamente **[F2]** para cambiar de Emisor.

3. Deberá buscar el comprobante por nombre de proveedor.

![IMG](2.png)

4. Seleccionar el comprobante deseado y dar doble clic o presionar **[Enter]**.

![IMG](3.png)


5. El folio UUID se llenará con el folio fiscal del comprobante. A continuación, deberá presionar **[Enter]** para que se cargue la información en la ventana automáticamente.

![IMG](4.png)


6. En caso de ser necesario, puede capturar alguna observación para el Gasto.

7. Finalmente haga clic en **[Guardar]**.

