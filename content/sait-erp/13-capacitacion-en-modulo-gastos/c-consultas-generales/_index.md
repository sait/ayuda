﻿+++
title = "Consultas Generales de Gastos"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=YmIYWE6CRx8&index=4&t=0s&list=PLhfBtfV09Ai6evDYeDMhYLYgGgMc_iBq7" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="13. C Consultas Generales.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


SAIT nos permite consultar de manera rápida y flexible la relación de Gastos registrados en el sistema. 

Para generar una consulta general de gastos ediríjase a:

<h4 class="text-primary">Gastos / Consultas Generales</h4>

![IMG](1.png)

1.Puede hacer la consulta por los siguientes filtros: 

* Por un tipo de gasto en especifico o todos los gastos.

* Por un proveedor en específico.

* Por rango de folio de facturas del gasto.

* Por  rango de fechas.

2.Haga Clic en el botón **[Consultar]**, automáticamente se muestra la relación de gastos.

3.En caso de ser necesario puede exportar la consulta a Excel, presionando el icono de Excel

![IMG](2.png)

4.Si lo desea, puede eliminar un gasto desde esta ventana presionando el botón de **[Borrar]**.


![IMG](3.png)

5.Se mostrará el detalle del gasto y si está seguro de eliminar el gasto, presione el botón de **[Eliminar]**.

6.Para consultar solamente el detalle de un gasto, haga doble clic o **[Enter]** sobre el gasto deseado

![IMG](4.png)


7.Una vez finalizada la consulta, haga clic en el botón **[Cerrar]**.

