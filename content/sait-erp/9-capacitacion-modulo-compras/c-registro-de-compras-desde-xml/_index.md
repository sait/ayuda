﻿+++
title = "Registro de Compras a partir de un XML o CFDI"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=HXJDYxxw7ic&index=3&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. C Registro de compras a partir del XML.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


SAIT le permite registrar compras a partir de un XML el cuál le facilitará el proceso de ingreso de mercancía al inventario, dicha funcionalidad trae consigo muchos beneficios, algunos de ellos son:

* **Rapidez al momento de ingresar una compra:** Ya que solo hay que elegir el XML con los artículos y procesarla.

* **Evita errores de captura:** Al no tener que capturar partida por partida los artículos evitamos errores de captura en costos, cantidad, etc.

* **Unificación de Claves o Códigos de Artículos en relación SAIT - Proveedores**: Es decir, solo con relacionar la primera vez las claves de su proveedor vs. SAIT, en la siguiente compra los asocia de manera automática.

* **Evite captura desde CERO Costos de Artículos**: Es decir, extrae desde el XML el costo por partida o renglón de los artículos, lo que evitará errores en la captura de su información.

Es muy importante asegurarse de instalar el organizador de comprobantes fiscales (OCF), ya que sin esta herramienta es imposible registrar nuestra compra desde el XML.

Para instalar y configurar el Organizador de Comprobantes Fiscales ingrese al siguiente enlace: 

https://ayuda.sait.mx/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/

<h2 class="text-primary">Ingresar compra a partir de un XML</h2>

Diríjase a al menú de:

**Compras / Registro de Compras**

1.Ingresar la clave del proveedor. En caso de ser necesario, puede hacer búsquedas por nombre haciendo clic en el botón [?].

2.Seleccionar el Tipo de Documento "Compra" utilizando la Barra Espaciadora.

3.Capturar la fecha de la compra.


![IMG](1.jpg)

4.Hacer clic en el botón **[Cargar CFDI]** Se visualizará la siguiente ventana:

![IMG](2.png)


5.Si tiene el folio fiscal lo ingresa, si no presione **[F2]**

6.Ingrese la razón social o RFC para localizar el proveedor.

![IMG](3.png)

7.Una vez localizado el Proveedor, seleccionarlo y presionando la tecla **[Enter]** o haciendo doble clic sobre el registro del proveedor.

En este momento se visualizarla la siguiente ventana, misma que mostrará el o los comprobantes CFDIS que se encuentran en el organizador:

![IMG](4.jpg)

Deberá de seleccionar el comprobante que se va a tomar como base para el registro de la compra. Una vez seleccionado, hacer clic en el botón **[Continuar]**. Presionar la tecla **[Enter]** y se visualizará la siguiente ventana:

![IMG](5.png)

En ese momento deberá asociar la clave de su proveedor y colocar en el renglón de abajo la clave similar en SAIT.

**El proceso de asociar hace referencia a empatar o relacionar el código del proveedor con nuestra clave SAIT**

Si usted lo desea puede ir capturando estos datos previamente es decir, ir capturando cuál es el código de proveedor para cada uno de mis artículos con la finalidad de que al ingresar una compra a partir de un XML ya no realice este paso, para realizar este proceso previo ingrese al siguiente enlace:

https://ayuda.sait.mx/sait-erp/9-capacitacion-modulo-compras/d-productor-asociados-a-proveedores/

En el segundo renglón, sobre el campo ASOCIAR, deberá de presionar la tecla **[F2]** para realizar búsqueda por nombre en caso de ser necesario, se mostrará la siguiente ventana:

![IMG](6.jpg)

Una vez que escriba y localice el articulo en su catálogo de SAIT, presionar la tecla **[Enter]** o hacer doble clic sobre el registro.

<h3 class="text-danger">NOTA</h3>

**El sistema solo le pedirá asociar una vez, en la siguiente ocasión el sistema detectará automáticamente el código que hemos designado para el artículo.**

![IMG](7.png)

Una vez finalizada la ASOCIACIÓN de las claves en SAIT, hacer clic en el botón **[F8 Procesar]**. Se visualizará la siguiente ventana, la cual carga los artículos en la compra:


![IMG](8.jpg)

De esta manera y como se podrá dar cuenta, se carga también la información adicional de la compra y la asociación del folio fiscal, para que en dado caso usted pueda manejar contabilidad electrónica.

Una vez finalizada con la captura y para procesar la compra deberá hacer clic sobre el botón **[F8 Procesar]**.

Listo, de esta manera ha agregado la compra a partir del CFDI del proveedor.
