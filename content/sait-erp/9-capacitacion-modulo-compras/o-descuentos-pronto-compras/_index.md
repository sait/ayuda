+++
title = "Descuento por Pronto Pago en Registro de Compras"
description = ""
weight = 13
+++

La versión de SAIT 2023.22 incluye una mejor importante la cual integra el uso de  Descuento por pronto pago (DPP) en el Costo de los artículos.

Si la empresa tiene acuerdo de DPP con algunos proveedores y desea utilizar esta nueva opcion debera:

- Activar la opción en Utilerías / Configuración General en la pestaña de Compras, la nueva opción de llama "Capturar Descuento por Pronto Pago".

![IMG](1.png)

- Aparecerá un mensaje que dice, dar clic en SÍ

![IMG](2.png)

- Al registrar una compra nueva, aparecerá a la derecha un campo nuevo con titulo: "%DescProntPago:" ahi deberá escribir el % de descuento por pronto pago que el proveedor otorgará a la empresa.

![IMG](3.png)

- Capturamos los datos de la compra de manera habitual:

![IMG](4.png)

- Si observamos el Kárdex del Artículo veremos que el % de descuento por pronto pago fue descontado del costo del artículo.

![IMG](5.png)

- Y la compra queda de esta manera

![IMG](6.png)


