﻿+++
title = "Pedimentos de Importación en Compras"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=PG385vKTFq4&index=5&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. F Pedimentos de Importaciones.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


El catálogo de Pedimentos de Importación permite tener almacenados los datos de todos los pedimentos de importación realizados por su empresa ante la SHCP, mediante los cuales realizó el ingreso de mercancía comprada en el extranjero hacia México. 

Antes de alimentar una compra realizada en el extranjero deberá capturar los datos del pedimento de importación realizado. 

<h2 class="text-primary">A. Agregar un Pedimento de Importación </h2>

Para capturar un nuevo pedimento de importación, realice los siguientes pasos: 

•	En el menú seleccionar la opción de: Compras / Pedimentos de Importaciones. 

•	Aparece la ventana de "Pedimentos de Importación". 


![IMG](1.png)

•	Haga clic en el botón de **[Agregar]**.

•	Se presenta la ventana "Agregar Pedimento" 


![IMG](2.png)

•	Escriba los siguientes datos: 

-	Número de Agente Aduanal que realizó el pedimento de importación. 

-	Fecha del pedimento de importación. 

-	Número de operación en la aduana en formato Xnnnnnn donde: 

* X Es el último digito del año de fecha de importación. 

* nnnnnn Es el número de operación secuencial en la aduana en cuestión. 

-	Clave de la aduana por donde se ingresó la mercancía al país. Puede presionar F2 o dar clic en [?] para buscar la aduana. 

•	Haga clic en el botón de **[Agregar]**.


Es importante mencionar que el primer dato que es la clave de pedimento, se forma AUTOMATICAMENTE siguiendo la estructura aabbccccXnnnnnn en donde: 

•	aa Son los últimos 2 dígitos del año de fecha de importación. 

•	bb Son los primeros 2 dígitos de la clave de la aduana. 

•	cccc Es el número de Agente Aduanal. 

•	X Es el último digito del año de fecha de importación. 

•	nnnnnn Es el número de operación secuencial en la aduana en cuestión. 

Esta clave de pedimento es la usted deberá capturar cuando alimente las compras relacionadas con este pedimento. 

<h2 class="text-primary">B. Habilitar Opción para Capturar de Pedimentos en Compras</h2>


Diríjase al Menú de Utilerías / Configuración General del Sistema / Pestaña Compras

Palomee la opción de Capturar Pedimento de Importación

![IMG](3.png)


De esta manera a partir de este momento al momento de capturar una compra aparecerá disponible el campo para capturar el pedimento.

![IMG](5.PNG)
