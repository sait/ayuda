﻿+++
title = "Consultas Generales e Individual"
description = ""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=OPorrN5RGmo&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI&index=10&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. H Consultas Generales e Individuales.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


<h2 class="text-primary">Consultas Generales</h2>

La consulta general es una forma práctica de ver las compras u órdenes de compra realizadas, en la cual podemos establecer filtros para realizar la consulta. Es una forma rápida de localizar un documento cuando no sabemos el folio.

Para realizar la consulta de compras debe realizar lo siguiente: 

1.En el menú ir a Compras / Consultas generales 

2.Aparecerá la siguiente ventana: 

![IMG](1.png)


3.Establezca los filtros de la consulta: 

* En el campo **Tipo**, seleccione con la barra espaciadora los documentos que desee, compras u orden de compras. También puede teclear la primera letra del documento 'O' para Órdenes de Compra y 'C' para compras 

* En el campo **Divisa**, puede seleccionar con la barra espaciadora Pesos, Dólares o Ambos. también puede teclear la primera letra de la divisa 

* En el campo **Folios**, puede establecer rango de folios de los documentos 

* En el campo **Fecha**, puede establecer un periodo determinado a consultar 

* En el campo **Proveedor**, puede establecer a un proveedor en especifico 

* En el campo **Sucursal**, puede establecer que solo muestre los movimientos de una sucursal en especifico 
 
* Haga clic en el botón de **[Consultar]**

4.Aparecerán los documentos como se muestra en la siguiente ventana: 

![IMG](2.png)

5.Si desea ver de forma detallada el documento posicionar el cursor en él, presione la tecla Enter o doble clic. 

**Puede enviar a Excel la consulta realizando clic en su ícono.**

<h2 class="text-primary">Consulta Individual</h2>

La consulta individual de documentos es la forma rápida de consultar un documento solo se debe tener a la mano el folio del documento. 

Para realizar este proceso debe realizar lo siguiente:

1.Entre al menú de Compras / Consulta Individual 

2.Aparecerá la siguiente ventana: 

![IMG](3.png)

3.Seleccione el tipo de documento que desea consultar 

4.En el campo **Folio**, teclee el folio del documento 

5.También puede presionar el botón **[F2]** donde aparecerá la lista de los documentos recientes como se muestra en la siguiente imagen: 

![IMG](4.png)

6.Seleccione el documento que desea consultar presionando **[Enter]** 

7.Presione el botón **[Consultar]** 

8.Aparecerá el documento. 


![IMG](5.png)