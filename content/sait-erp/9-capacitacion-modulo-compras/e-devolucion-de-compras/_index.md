﻿+++
title = "Devolución de Compra"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=1oqLU1u6bP0&index=4&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. E Devolución de compras.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


El registro de devoluciones es muy fácil y sencillo. Al registrar la devolución de la compra en el sistema, estamos afectando las cuentas por pagar y el inventario.

Para realizar una devolución de compra diríjase al menú de:

**Compras / Devoluciones**

Aquí es donde deberá escribir el folio del documento a devolver, la fecha de la devolución y presione la tecla **[Enter]**.

En la columna de "Devolver", escriba la cantidad a devolver del artículo.
 

![IMG](1.png)

Clic en el botón **[Procesar= F8]** 

<h3 class="text-danger">NOTA</h3>

**Es importante mencionar que los artículos contenidos en la compra que se desea devolver, deben tener existencia suficiente para realizar la devolución. En caso contrario puede que afecte su inventario de manera negativa.**

