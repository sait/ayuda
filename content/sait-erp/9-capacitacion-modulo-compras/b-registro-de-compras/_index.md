﻿+++
title = "Registro de Compras"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=OUaSLn-XKzc&index=2&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. B Registro de Compras.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


SAIT le permite registrar diferentes documentos de compras, tales como lo son: órdenes de compra y facturas de compras.

Para ingresar a esta opción, dirigirse al menú de:

**Compras/Registro de Compras**


![IMG](1.png)

Para registrar una compra deberá seguir el siguiente proceso:

1. Ingresar el **número correspondiente al proveedor**. En caso de no saber rápidamente cuál es, dar clic en el botón [?] o presionar la tecla F2 para realizar la búsqueda.

2. Seleccionar el **tipo de documento** utilizando la barra espaciadora para seleccionarlo.

3. Escribir la **fecha del documento**.

4. Agregar algún **Comentario** relacionado con el Documento de Compra.

5. Seleccionar con la barra espaciadora si será la **Divisa** en Pesos o en Dólares.

6. Especificar el **Tipo de Cambio**, en caso de que la compra sea registrada en Dólares.

7. Escribir numero de **Factura del proveedor**.

8. Especificar la **Fecha del pago**.

9. Escribir la cantidad de **Gastos indirectos** en caso de ser necesario.

10. Capturar los **Artículos** a incluir en el Documento de Compra.

11. Por último, haga clic en el botón **[F8 Procesar]**.
 