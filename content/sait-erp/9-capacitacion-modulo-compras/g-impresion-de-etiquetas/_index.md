﻿+++
title = "Impresión de Etiquetas"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=lbkE_g6TjvM&index=6&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. J Impresion de etiquetas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

<h2 class="text-primary">A. Impresión de Etiquetas</h2>

En SAIT se pueden imprimir etiquetas de códigos de barra para artículos que físicamente no lo traen. 

Se pueden definir diferentes tipos de formatos para imprimir la etiqueta de acuerdo con la medida exacta. 

1.Ir al menú de Compras / Imprimir Etiquetas 

![IMG](1.png)

3.Seleccione el formato de la etiqueta que se va a imprimir 

4.Seleccione ingrese el código del artículo que desea imprimir las etiquetas.

5.Se pueden saltar "n" cantidad de etiquetas al inicio: Esta opción es de mucha utilidad cuando ya se han impreso etiquetas en una hoja.

6.Capture algún mensaje que desee incluir en las etiquetas. Ejemplo: ARTICULO DE PROMOCIÓN, NUEVO PRODUCTO, ARTÍCULO DE IMPORTACIÓN, etc.

7.Por último, haga clic en **[Imprimir]** para imprimir las etiquetas 

8.Las etiquetas se imprimirán según el diseño de formato elegido: 

Por ejemplo:

![IMG](2.png)

<h2 class="text-primary">B. Cargar artículos usando una lectora portátil </h2>


Si en su empresa cuenta con alguna lectora portátil de códigos de barra, puede realizar capturar los códigos de barra de los productos a través de la lectora y posteriormente usar el asistente para descargarlos desde esa opción. 

1.En el menú de Compras / Imprimir Etiquetas, hacer clic en el botón indicado:

![IMG](3.png)

2.Se muestra el asistente para cargar artículos desde una lectora.

![IMG](4.png)

3.Finalice el proceso para transferir los códigos de barra.

4.Listo, ya puede imprimir las etiquetas para los artículos transferidos.

<h2 class="text-primary">C. Cargar artículos de un documento</h2>


Esta opción permite cargar todos los artículos capturados en un documento de Venta o Compra. 

Los documentos que se tienen disponible son: Compra, Orden, Factura, Remisión, Pedido, Cotización. 

Este proceso es de mucha utilidad cuando se desean imprimir etiquetas de artículos recién comprados o cuando se van a surtir de almacén. 

1.Deberá colocar el tipo de documento cuyos artículos se van a cargar. 

2.Deberá especificar el folio. Presionar la tecla **[Enter]**.

3.Se cargan los artículos relacionados al documento. 

![IMG](5.png)

<h2 class="text-primary">D. Cargar Existencias</h2>


Permite cargar todas las existencias de los artículos que cumplan con las restricciones de la consulta. 

Este proceso es de mucha utilidad cuando se desea etiquetar a todos los artículos. 

1.En la ventana dar clic en el botón **[Cargar Existencias]** 

![IMG](6.png)

2.Deberá mostrarse la siguiente ventana de consulta: 

![IMG](7.png)

3.Podrá realizar los filtros que usted requiera: 

* Rango de clave de artículos 

* Línea 

* Familia 

* Categoría 

* Departamento 

* Proveedor 

* Ubicación 


4.Haga clic en el botón **[Cargar]**

5.Los artículos con existencia que cumplan las restricciones serán cargados 
