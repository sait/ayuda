﻿+++
title = "Catálogo de Proveedores"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=9sJ3AQf0WG0&index=6&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. A Catálogo de Proveedores.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


SAIT le permite registrar nuevos proveedores, modificar y borrar los ya existentes.
Para acceder a dicha ventana deberá diríjase a:

**Compras / Proveedores**

![IMG](1.png)

<h2 class="text-primary">Dar de alta un nuevo proveedor</h2>

Por default le asignará un número de proveedor que se genera por consecutivo.

* Capturar el nombre del proveedor.

* Ingresar el domicilio fiscal

* Ingresar el nombre, correo y número telefónico del contacto de ese proveedor.

* Ingresar el RFC del proveedor.

* Clic en **[Guardar]** y Listo.

![IMG](2.png)

<h2 class="text-primary">Modificar proveedores</h2>

•	Escribir la clave del proveedor. Hacer clic en [?] para realizar búsquedas.

•	A continuación, se mostrará su información. Modificar los datos que sean necesarios.

•	Hacer clic en el botón **[Grabar]**.

![IMG](3.png)

<h2 class="text-primary">Borrar Proveedores</h2>


•	Escribir la clave del proveedor. Hacer clic en [?] para realizar búsquedas.

•	Se mostrará su información. Verificar que sea el proveedor que se desea borrar.

•	Hacer clic en el botón **[Borrar]**.

![IMG](4.png)

<h3 class="text-danger">NOTA</h3>

No se recomienda borrar un proveedor si ya se han registrado movimientos con el ya que posteriormente estos movimientos no se podrán consultar.
