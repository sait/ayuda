+++
title = "Comisión Cobro con Tarjeta"
description = ""
weight = 15
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>

<div class="card">
<a href="comision-cobro-tarjeta.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Cuando dentro una empresa se reciben varios tipos de pagos es común que en ocasiones se considere la posibilidad de cobrar un porcentaje extra por dichas transacciones. 

Es por ello que el sistema cuenta con la funcionalidad de establecer un cobro de porcentaje por comisión al realizar ciertos tipos de movimientos dentro del sistema. Dicho porcentaje se puede cambiar o de igual manera inhabilitar, según sea la necesidad del usuario. 

Para realizar esta configuración deberá entrar al maneú de: 

<h4 class="text-primary">Caja / Tipos de Movimientos </h4>

1.A continuación se le presentará la ventana del catálogo de conceptos de caja

![IMG](1.png) 

2.Deberá seleccionar el concepto al que le desea agregar el % de Comisión y dará clic en Modificar, en la sección de % de Comisión por aceptar este tipo agregará el porcentaje que deseemos manejar, una vez agregado damos clic en Modificar y listo.

![IMG](2.png) 

3.También deberá crear el siguiente artículo para terminar con la configuración de cobro de comisión con tarjeta, para esto deberá entrar a Inventario / Catálogo de Artículos y Servicios, lo deberá dar de alta justo como se muestra en la imagen:

![IMG](3.png) 

4.Para corroborar el funcionamiento deberá de entrar a Caja / Registrar Ventas o Ventas / Registrar Ventas y agregar algún artículo y al dar clic en F8 procesar el sistema.

Seleccione en la forma de pago TARJ DE CREDITO y el sistema automáticamente le abrirá la ventana de Aplicar Comisión al Pago.

De clic en Aplicar Pago y listo.

![IMG](4.png) 

5.Finalmente de clic en F8 Procesar y listo la venta fue procesada de manera correcta

![IMG](5.png) 

6.La nota se mostrará de la siguiente manera:

![IMG](6.png) 