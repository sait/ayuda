+++
title = "Apartados/Pedidos con CFDI 4.0"
description = ""
weight = 13
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>

<div class="card">
<a href="Proceso Apartados.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite llevar un control de los documentos de tipo pedidos o apartados de una forma muy sencilla. Esta modalidad te permite registrar el documento con el fin de tener el listado de productos o servicios que se deben de entregar a una determinada fecha. 

El cliente puede ir realizando abonos y al final entregar el pedido para que se haga la afectación en el inventario dentro del sistema.

Para utilizar esta opción dentro de SAIT se requiere configurar previamente el sistema para ello. A continuación se enlista lo que debe considerar:

## Configuración Previa

* En el menú de Inventario / Catálogo de Artículos y Servicios, agregar los conceptos ABONO y *ABONO con las siguientes especificaciones: 

Clave | Descripción | Unidad | Clave SAT | Servicio | Precio Míinmo
---|---|---|---|---|---
ABONO |	Abono a Apartados |	ACT |	84111506 |	SI	|
*ABONO |	Abonos Anteriores |	ACT |	84111506 |	SI	| -9,999,999.99 


* Descargar el formato de nota de venta especial para apartados [dando clic aquí](FtoNota_Apartados.rpt)

La nota se vería de la siguiente manera

![IMG](nota.png)

* Descargar el formato de pedido especial para apartados [dando clic aquí](FtoPedido_Apartados.rpt)

El pedido en tamaño ticket se vería de la siguiente manera

![IMG](pedido.png)

* Ir al menú de Utilerías / Recibir reportes seleccionar uno a uno los formatos recién descargados (Formato de nota de venta y formato de pedidos) y presionar el botón de **[Recibir]**.
 
![IMG](recibir.png)

* Identificar el directorio de la empresa. Puede dirigirse al menú de utilerías / configuración general del sistema:

![IMG](config.png)
 
* Descargar la función “Histpagos” [dando clic aquí](HistPagos.zip)

* Descomprimir este archivo dentro del directorio de la empresa. 

![IMG](extraer.png)
  
## Elaborar un apartado en CAJA

Para poder realizar este proceso, ingrese a: 

**Caja  / Registrar Ventas**

1.Presione la tecla [F5], para buscar el cliente, o posicione el cursor en el campo de los datos del cliente para buscar por nombre.

2.Presione la tecla [F6] o haga clic en el botón [F6 Otras] y seleccione la opción de Elaborar Apartado.

![IMG](1.png)
 
3.Agregue los artículos a la lista de apartado.

![IMG](2.png)
 
4.En la sección de pago, puede especificar si el cliente dará un abono o se puede dejar en blanco.

5.En caso de dejar un abono el sistema le pedirá confirmación de que los datos se encuentran correctos. 

![IMG](3.png)
 
6.En caso de dejar en blanco el sistema pedirá confirmación como se muestra en la siguiente ventana:
 
![IMG](4.png)

7.Haga clic en **[SI]** y listo, se ha procesado correctamente el apartado.
 
## Registrar un abono a apartado en CAJA a un pedido ya existente

Para realizar este proceso, ingrese a: 

**Caja  / Registrar Caja**

1.Seleccione el tipo de documento como Nota o Factura, para emitir el comprobante del
abono.
2.Presione la tecla **[F5]**, para buscar el cliente, o posicione el cursor en el campo de los datos del cliente para buscar por nombre.
3.Presione la tecla **[F6]** o haga clic en el botón **[F6 Otras]** y seleccione la opción de **[Abonar o Entregar un Apartado]**. 

![IMG](5.png)
 
4.Deberá seleccionar el folio del apartado al que desea abonar. Si es un solo pedido pendiente, éste se cargará en automático.
 
![IMG](6.png)

5.Seleccione el botón de **[Abonar]**.

![IMG](7.png)
 
6.Indique la cantidad que desea abonar.

7.Seleccione la divisa como pesos o dólares.

8.Presione el botón de **[Abonar]**.

![IMG](8.png) 

9.Se mostrará la ventana de documento de venta con el concepto de ABONO A PEDIDOS.

10.Deberá indicar la forma de pago que recibió por parte del cliente por el importe total del abono.

![IMG](9.png)
 
11.Presione el botón de **[F8 procesar]** y listo, se imprimirá el comprobante del abono. 
 
### Entregar un apartado en CAJA

Para entregar un pedido o apartado ya existente debe dirigirse al menú de Caja / Registrar Ventas. (Este proceso realiza la salida de inventario de los artículos incluidos en el pedido)

1.Seleccione el tipo de documento como nota de venta. NO se permite entregar un pedido como Factura.

2.Presione la tecla **[F5]**, para buscar el cliente al que está asociado el pedido, o posicione el cursor en el campo de los datos del cliente para buscar por nombre.

3.Presione la tecla **[F6]** o haga clic en el botón **[F6 Otras]** y seleccione la opción de **[Abonar o Entregar un Apartado]**. 

![IMG](10.png) 

4.Deberá seleccionar el folio del apartado al que desea abonar. Si es un solo pedido pendiente, éste se cargará en automático.

![IMG](11.png)
 
5.Presione el botón de **[Entregar]**.

![IMG](12.png)
 
6.Se cargará en la pantalla de caja los productos que serán entregados.

7.Si el pedido tiene algún saldo pendiente, deberá indicar la forma de pago en la que se liquidará el pedido, de lo contrario se permitirá procesar el documento en 0.00

![IMG](13.png) 

8.Presione el botón de **[F8 procesar]** y listo, se imprimirá la nota de venta. 
 
### Consideraciones Especiales

1.	Si el pedido se enlaza directamente a un documento de venta (factura, nota de venta o remisión), éste ya no estará disponible para abonar porque se entiende que la mercancía ya fue entregada.
2.	Se permite terminar de abonar en caja, para que en Ventas / Registro de Ventas, se elabore una factura enlazando el pedido (sin recibir pago) para facturar los productos entregados. 
3.	Se permite abonar un apartado con documento de nota de venta o factura. 
4.	Se permite abonar un apartado como factura a crédito solamente en caja. 
5.	Siempre se debe colocar primero el cliente antes de seleccionar la opción de abonar o entregar un apartado para excluir los pedidos cancelados. 
6.	Pedidos entregados en nota de venta con importe en 0, en factura global saldrán con un importe de 0.01 para emitir correctamente el timbrado según lo requerido por el SAT.
7.	No se puede entregar un apartado en tipo de documento Factura. Solamente como Nota de Venta. 
8.	No se puede facturar Notas de Venta cuando se incluye el concepto de abono a pedidos. 

