﻿+++
title = "Factura del Día"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=VPVzacTI9ac&t=0s&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&index=7" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. F Elaborar Factura del dia.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El proceso de la factura del día permite facturar todas aquellas notas de venta que no fueron facturadas de manera individual.

Como dato informativo la factura del día sale a nombre del cliente PÚBLICO GENERAL.

Para realizar la factura del día siga las siguientes instrucciones:

1.Entre al menú de Caja / Elaborar Factura del Día 

Aparecerá siguiente pantalla en donde le solicitará seleccionar la periodicidad, las opciones disponibles son:

* **Facturar un Día**

* **Facturar un Semana**

* **Facturar un Mes** 

![IMG](1.png)

También deberá indicar cuál es el día a factura y la fecha que aparecerá en la emisión del comprobante fiscal, la fecha de emisión se puede atrasar hasta 3 días.

2.Una vez verificados los datos damos clic en **[Facturar]**

Aparecerá la siguiente ventana en dónde daremos clic en **[Si]** para confirmar la acción.
![IMG](2.png)

3.Una vez procesada la factura se mostrará de la siguiente manera:

![IMG](3.png)