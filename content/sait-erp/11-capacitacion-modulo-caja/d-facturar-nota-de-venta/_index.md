﻿+++
title = "Facturar Nota de Venta"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=iibba7-qjmE&t=0s&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&index=8" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. D Facturar Nota de Venta.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Este proceso permite facturar una o varias notas de venta registradas previamente. Sólo se pueden facturas notas que NO hayan sido incluidas en la factura del día.

Para realizar este proceso siga las siguientes instrucciones:

1.Entre al menú de Caja / Facturar Nota de Venta.


![IMG](1.png)

2.Si se desea que solo se realice factura de una nota a la vez el supervisor puede habilitar la opción: Permitir facturar SOLAMENTE una nota.

3.Capture el folio de la(s) nota(s) de venta que se va(n) a facturar separadas con coma. Ejemplo: 22,23,24 etc.

4.Clic en **[Facturar]**

5.Se mostrará la ventana de "Facturar" con el detalle de artículos incluidos en la(s) nota(s).


![IMG](2.png)

6.Especifique el cliente al que se va a facturar.

7.Clic en **[F8 Procesar]**.

8.Listo.

<h3 class="text-danger">NOTA</h3>

#### Las notas de venta facturadas individualmente no serán incluidas en la factura del día.
