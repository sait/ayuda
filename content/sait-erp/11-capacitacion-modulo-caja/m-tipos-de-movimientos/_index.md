+++
title = "Tipos de Movimientos en Caja"
description = ""
weight = 12
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=AQGSX2G6zbI&index=13&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. M Tipos de Movimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Los tipos de movimientos en Caja permiten definir los conceptos de entrada o salida de efectivo en el corte de caja. SAIT cuenta con los conceptos que comúnmente se utilizan al realizar, pero también permite definir conceptos personalizados de acuerdo con las necesidades de cada empresa.

Para acceder al catálogo de tipos de movimientos ingrese a:

**Caja / Tipos de Movimientos**

![IMG](1.png)

El siguiente proceso describe los pasos para agregar, modificar o eliminar un tipo de movimiento en Caja.

<h2 class="text-primary">Agregar</h2>


1.Haga clic en el botón **[Agregar]**



2.Capture la clave del movimiento, la cual debe tener como máximo de 5 caracteres. 

Ejemplo: **TRANS**

3.Especifique la descripción del concepto. 

Ejemplo: **Transferencia Bancaria**.

4.Seleccione el tipo:

* **Pago:** Se utiliza para realizar pagos a documentos contado o crédito de los clientes.

* **Entrada por otro movimiento:** Se utiliza para registrar una entrada de efectivo distinta al pago de un cliente. Ejemplo: Fondo de caja recibido.

* **Salida por otro movimiento:** Se utiliza para registrar una salida de efectivo. Ejemplo: Retiro a Bóveda.

5.Especifique la clave de los niveles de usuario que tendrán acceso a registrar este tipo de movimiento separados con coma. Ejemplo: **SUPER,CAJ,VEND**

6.Si el movimiento está relacionado con un movimiento de cobranza captura la clave del concepto de cobranza. Puede realizar búsquedas al presionar la tecla **[F2]** o al hacer clic en el botón **[?]**.

7.Active la casilla **[ * ]** Es movimiento de efectivo en caso de que se ocupe dar feria.

* **Si el tipo de movimiento es Pago:** Active la casilla **[ * ]** Capturar Referencia en caso de ser necesario. Generalmente se necesita capturar referencia en pagos con cheque, transferencias bancarias, pago con tarjeta, etc. Esta referencia permite identificar el pago.

* **Si el tipo de movimiento es Salida por otro movimiento:** Active la casilla **[ * ]** Capturar Beneficiario en caso de necesite identificar la persona a la cual se realiza la salida.


8.Active la casilla **[ * ]** Mostrar total al entregar corte en caso de necesite que se totalicen los pagos realizados con este movimiento al momento de entregar el corte de caja. Generalmente se activan con esta casilla los movimientos en los cuales se captura referencia tales como cheque, transferencias bancarias, pago con tarjeta, etc.

9.Se puede configurar un formato para imprimir algún comprobante al registrar este tipo de movimiento. Consulte al final de esta documentación las instrucciones.

10.Haga clic en **[Agregar]**.

![IMG](2.PNG)

<h2 class="text-primary">Modificar</h2>


1.Seleccione del listado de movimientos el que se desea modificar y haga clic en **[Modificar]**.

2.Se muestra la información del movimiento.

![IMG](3.png)

3.Modifique la información que sea necesaria.

4.Para guardar cambios de clic en **[Modificar]**

<h2 class="text-primary">Eliminar</h2>

1.Seleccione del listado de movimientos el que se desea eliminar y haga clic en **[Eliminar]**.

2.Se mostrará la información del movimiento.

![IMG](4.png)

3.Verifique que sea el movimiento correcto.

4.El sistema le preguntará si desea eliminar el concepto. Haga clic en **[Si]**
<h2 class="text-primary">Agregar Formato a Tipo de Movimiento</h2>

1.Seleccione el movimiento al que desea se imprima un formato de clic en **[Modificar]**

2.Se abrirá la siguiente pantalla en donde dará clic en **[Formato]**

![IMG](5.png)

3.En la pantalla que se muestra a continuación en el campo Formato deberá ingresar lo siguiente: **CJFTOMOV**   

En la descripción ingrese la descripción del nombre del movimiento.

Si desea se imprima automáticamente al procesar este movimiento deberá ingresar la ruta de donde se encuentra la impresora de tickets.


![IMG](6.png)

4.Ahora cuando procese un movimiento aparecerá el formato de la siguiente manera:

![IMG](7.png)
