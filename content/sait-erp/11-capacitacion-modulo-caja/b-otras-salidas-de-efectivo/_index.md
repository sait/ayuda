﻿+++
title = "Otras Salidas de Efectivo"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=kshqQXQuq_Q&t=0s&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&index=3" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. B Otras Salidas de Efectivo.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Este proceso permite registrar salidas de efectivo al corte de caja activo.

Puede haber varias razones por las que se requiere realizar una salida de efectivo:

*	Manejo de Retiros de Efectivo en Caja.

*	Pago a proveedores.

*	Devolución de efectivo a clientes

*	Fondo de caja para el siguiente turno, etc.

Los conceptos para la salida de efectivo deberán ser configurados previamente dentro de los Tipos de Movimientos de caja.

1.Entre al menú de Caja / Otras Salidas de Efectivo.


![IMG](1.png)

2.Seleccione el tipo de Salida. Ejemplo: Pago a Proveedor.

3.En caso de ser necesario, capture el beneficiario al que se realiza la salida.

4.Seleccione la divisa: Pesos o Dólares.

5.Capture el importe del efectivo al que se dará salida. Puede presionar la tecla [F2] para capturar las denominaciones de billetes y monedas.

6.Clic en **[Procesar]**



