﻿+++
title = "Cancelar Movimientos"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=deqJPsFlaPI&t=0s&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&index=6" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. E Cancelar Movimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Desde la cancelación de movimientos desde caja se pueden cancelar documentos de venta y al mismo tiempo cancelar el movimiento en caja relacionado a éste.

También permite cancelar movimientos de caja que no están relacionados a ningún documento de venta. Ejemplo: fondo de caja recibido o entregado al sig. cajero, retiros de efectivo a bóveda, etc.

Se pueden cancelar los siguientes movimientos: Nota, Factura, Apartado, Pago, Salida de Efectivo, Entrada de Efectivo.


<h3 class="text-danger">NOTA</h3>

**Es importante mencionar que en esta opción únicamente se pueden cancelar documentos de venta de contado. La cancelación de documentos de venta de crédito se realiza desde el módulo de Ventas.**

<h2 class="text-primary">Cancelar Documento de Ventas</h2>


Para cancelar una nota de venta desde el módulo de caja siga las siguientes instrucciones:

1.Entre al menú de Caja / Ventas / Cancelar Movimientos o desde la ventana de Registrar Ventas seleccione la siguiente opción:


![IMG](1.png)

2.Aparecerá la siguiente ventana en donde deberá seleccionar el tipo de documento que cancelará y también deberá ingresar el folio del documento a cancelar.

![IMG](2.png)


3.De clic en [Cancelar] y posteriormente se le mostrará la ventana de consulta individual del documento.

![IMG](3.png)

4.Ingrese los comentarios o motivos de la cancelación, seleccione el motivo en la opción del ComboBox e ingrese el folio del documento a cancelar.


![IMG](4.png)

5.Una vez llenados los datos de clic en **[Si]**. Aparecerá la siguiente pantalla en donde seleccionará la opción Si para confirmar la acción.

![IMG](7.png)

6.Finalmente le aparecerá el nuevo estatus el cual es CANCELADO.

![IMG](6.png)

<h2 class="text-primary">Cancelar Movimiento de Caja</h2>

1.Entre al menú de Caja / Ventas / Cancelar Movimientos o desde la ventana de Registrar

2.Aparecerá la siguiente ventana en donde deberá seleccionar el tipo de documento/movimiento que cancelará y también deberá ingresar el folio de dicho movimiento, para efectos de la prueba seleccionaremos Entrada de Efectivo.

Ingresamos el folio del movimiento:

![IMG](10.png)


3.Verifique que se trate del movimiento que se desea cancelar.

4.Capture el motivo de cancelación.


![IMG](11.png)


5.Confirme la cancelación del movimiento.

![IMG](12.png)

6.Listo, el movimiento ha sido cancelado.

![IMG](13.png)


