﻿+++
title = "Recibir Pagos de Contado"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=qflNBGQPWVY&t=0s&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&index=8" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. G Modalidad Ventas - Caja - Recibir pagos de contado.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Mediante esta opción en SAIT usted puede registrar los de ventas de contado que han sido procesadas desde Ventas / Registro de Ventas (F4).

Esta modalidad es muy útil para giros como ferreterías, refaccionarías, madererías o cualquier negocio en donde hay vendedores en mostrador haciendo los pedidos de los clientes y para pagar se dirigen a un equipo que funge como caja.

**Si desea conocer el proceso completo para realizar esta configuración en su sistema SAIT de clic:** [aquí](/temas-soporte/configurar-ventas-mostrador/)  

Para usar esa modalidad en SAIT siga las siguientes instrucciones.

1.Entre al menú de Ventas / Registrar Ventas F4

Desde los equipos que estén en mostradores registre la venta como de costumbre.

![IMG](2.png)

2.En el equipo que funge como Caja entre al menú de Caja / Registrar Pagos Contado F12 o presione la tecla **[F12]**.

Le aparecerá la siguiente pantalla en donde aparecerá la venta por $ 232 pesos que acabamos de realizar desde Ventas / Registrar Ventas F4


![IMG](3.png)

Para pagarla deberá dar **[Enter]** sobre el registro o dar doble clic para que se habilite la pantalla en donde se podrá registrar el pago:

* Seleccione la forma de pago con la barra espaciadora.

* Especifique el total del pago en pesos y/o dólares.

* En caso de que la forma de pago lo requiera, especifique el número de referencia.

* Presione la tecla **[F8]** para procesar el pago.


![IMG](4.png)

3.Listo, la nota ya no aparecerá en los documentos pendientes de la ventana F12.

![IMG](5.png)










