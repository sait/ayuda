﻿+++
title = "Catálogo de Beneficiarios"
description = ""
weight = 11
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=lTKzPQOhTxc&index=12&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&t=3s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. L Catalogo de Benificiarios.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Los beneficiarios son las personas o empresas a las cuales se les genera un movimiento de entrada o salida de efectivo en el corte de caja. El catálogo de beneficiarios le permite agregar, modificar y eliminar beneficiarios.

Para registrar un movimiento al beneficiario es necesario que se agregue previamente desde el catálogo.

El siguiente proceso describe los pasos para agregar, modificar o borrar beneficiarios. También le permite imprimir el catálogo.

1.Entre al menú de Caja / Catálogo de Beneficiarios.
Aparecerá la siguiente ventana en donde podrá AGREGAR, MODIFICAR, IMPRIMIR y ELIMINAR.


![IMG](1.jpg)

<h2 class="text-primary">Agregar Nuevo Beneficiario</h2>


* La clave del beneficiario es proporcionada a partir del último que se agregó. Si lo desea, puede modificarla y a partir de ahí se llevará el consecutivo de claves.

* Capture el nombre. Este dato es muy importante ya que con ese nombre será identificado el movimiento en Caja.

* Haga clic en **[Grabar]**

<h2 class="text-primary">Modificar Beneficiario</h2>

* Capture la clave. Haga clic en **[?]** o presione la tecla **[F2]** para realizar búsquedas.

* Se cargará la información en donde podrá hacer la modificación del nombre.

![IMG](2.png)

* Haga clic en **[Grabar]**

<h2 class="text-primary">Borrar Beneficiario</h2>

* Capture la clave. Haga clic en **[?]** o presione la tecla **[F2]** para realizar búsquedas.

* Se muestra su información.

![IMG](3.png)

* Verifique que sea el beneficiario que se desea borrar.

* Haga clic en **[Borrar]**

**Al borrar el beneficiario solo se elimina del catálogo, los movimientos en Caja que se hayan elaborado no se eliminan.**

<h2 class="text-primary">Imprimir Catálogo</h2>

Para imprimir el catálogo de beneficiarios solo debe entrar a la ventana y hacer clic en **[Imprimir]** para imprimirlo en impresora, pantalla, generar PDF o enviarlo por correo.

![IMG](imprimir.png)

![IMG](4.png)

