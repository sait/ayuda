﻿+++
title = "Otras Entradas de Efectivo"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=liks-Tqel0A&t=0s&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&index=4" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. C Otras Entradas de Efectivo.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Este proceso permite registrar entradas de efectivo al corte de caja activo. Generalmente las entradas de efectivo se realizan a través de los pagos realizados, pero se cuenta con esta opción para registrar otro tipo de entradas.

Los conceptos para la entrada de efectivo deberán ser configurados previamente dentro de Tipos de Movimientos de caja.

1.Entre al menú de Caja / Otras Entradas de Efectivo.



![IMG](1.png)

1.Seleccione el tipo de Entrada.

2.En caso de ser necesario, capture el beneficiario que procesa la entrada.

3.Seleccione la divisa: Pesos o Dólares.

4.Capture el importe del efectivo que está entrando al corte. Puede presionar la tecla **[F2]** para capturar las denominaciones de billetes y monedas.

5.Clic en **[Procesar]**
