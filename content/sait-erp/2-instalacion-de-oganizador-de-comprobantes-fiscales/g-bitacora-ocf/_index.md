+++
title = "Bitácora de Cambios OCF"
weight = 6
+++

Bitácora de mejoras y correcciones para Organizador de Comprobantes Fiscales (OCF)


Versión |  Fecha de Salida | Descripción
---|---|---
4.28.0.0 | 02-Dic-2024 | Se corrigió la descarga de cfdis desde email de archivos que en el nombre tenían caracteres inválidos.
4.27.0.0 | 19-Dic-2023 | Se corrigió carga de PDF con extensión de archivo incorrecta, ejemplo si la extensión venía como .Pdf o .Xml no lo leía el Organizador
4.26.0.0 | 3-Feb-2022 | Permite recibir CFDIs 4.0
4.25.6.0 | 23-Jul-2021 | Se corrige un problema que no desglosaba correctamente el iva cuando tenía varios. https://gitlab.com/sait/cfdi33/-/issues/890
4.25.5.0 | 16-Ene-2021 | Añadir soporte para codificaciones iso.
4.25.4.0 | 19-Oct-2020 | Se corrige un problema que no movía los archivos zip corruptos a la carpeta de error. El procesamiento de la carpeta del buzón de hace cada 10 segundos.
4.25.3.0 | 09-Sep-2020 | Se descargan todos los archivos del correo excepto los exe. se corrige la consulta de los contribuyentes, si tenían campos null no los consideraba.
4.25.2.0 | 03-Jul-2020 | Se descargan los archivos zip del correo.
4.25.1.0 | 05-Jun-2020 | Se corrige un problema que le quitaba los espacios a los nombres de los contribuyentes en cfdi 3.2
4.25.0.0 | 19-Dic-2019 | Se cambian los logs de errores a la carpeta de logs y se graban por dia, esto porque se dificultaba revisar el archivo de logs en los casos de soporte, en ocaciones llegaba a pesar hasta mas de 100 MB
4.24.0.0 | 01-Nov-2019 | Se corrige un error que impedia descargar los comprobantes de boveda.
4.23.0.0 | 04-Oct-2019 | Se regresa correctamente la version por API ya que SAIT ERP lo necesita
4.22.0.0 | 25-Sep-2019 | Se espera a crear los campos nuevos para iniciar el servicio, el reprocesamiento se hace en segundo plano
4.21.0.0 | 25-Sep-2019 | La actualizacion de base de datos y reprocesamiento se hacen en segundo plano al arrancar para no interrumpir el trabajo
4.19.3.0 | 01-Ago-2019 | Se corrige la consulta por rfc en la ruta de emitidos
4.19.2.0 | 04-Jun-2019 | Se crean indices si no existen: rfce en cfdisrecibidos, rfcr en cfdisemitidos, serie, folio, tipo en cfdisrecibidos y cfdisemitidos.
4.19.1.0 | 08-May-2019 | Se integra con la nueva bóveda.
4.19.0.0 | 25-Sep-2018 | Se creo rutina de reprocesamiendo cuando se actualizen las tablas.
4.18.0.0 | 10-Ago-2018 | Se agregaron y se insertan los campos de método de pago y versión.
4.17.0.0 | 04-Abr-2018 | Salos de Linea. Al momento de parsear un xml, eliminar saltos de linea en el nombre, y rfc del emisor y receptor, ocasionaba que la vista de comprobantes fiscales en SAIT no mostrara los datos correctamente
4.17.0.0 | 04-Abr-2018 | Marca BOM Mejora, Limpiar los caracteres raros y marca BOM al leer y procesar los xml's 
4.17.0.0 | 04-Abr-2018 | servidor Mail Sync. Solucion, Se corrigio el detalle al descargar correos, se saturaba la memoria y se paraba la rutina mailsync 
4.11.0.0 | 14-Nov-2017 | servidor SMTP Solucion al error certificate signed by unknown authority, se omitio la verificación de la cadena de certificados del servidor y el nombre de host
4.10.0.0 | 10-Nov-2017 | Tipo de Comprobante en Cfdi 3.3 Corrección al guardar tipo de comprobante "N" nomina en cfdi 3.3
4.9.0.0 | 09-Nov-2017 | Total con Espacios. Se Corrigió el detalle al cargar en buzonSait XML's con espacios en los atributos
4.8.0.0 | 31-Oct-2017 |Impuestos. Se Corrigió el detalle de impuestos y retenciones al revés
4.7.0.2 | 18-Oct-2017 | Ampersand. Se Corrigió problema en OFC al leer RFC con ampersand
4.7.0.0 | 9-Sep-2017 | CFDI v3.3 Se agrego soporte para CFDI v3.3 
4.7.0.0 | 9-Sep-2017 | Corrección: Problemas con rutina al sincronizar. Se solucionó el problema al cargar demasiados comprobantes en Buzón SAIT
4.7.0.0 | 9-Sep-2017 | Creación de PDF Mejora, al meter XML en Buzón SAIT sin PDF, el sistema creará un reporte PDF genérico de este.


Versiones 3.x

Versión | Fecha | Descripción
---|---|---
3.9.1.1 | 23-Dic-2016 | Corrección: Problemas con rutina para sincronizar bóveda. Ajustes para tener en constante sincronización los servicios de bóveda sin que este se interrumpa
3.9.1.1 | 23-Dic-2016 | Corrección: Error de estructura al intentar organizar comprobantes nómina 1.2 	Se hicieron ajustes para que permitiera manejar comprobantes de nómina con complemento 1.2
3.9.1.1 | 23-Dic-2016 | Corrección: Se incrementa pool de conexiones. Solución al error: 500 Internal Server ErrorError 1040: Too many connections.
3.6.0.1 | 13-Oct-2016 | Corrección: Cantidades decimales a string Las cantidades que son decimales las regresa en formato string 
3.5.11.2 | 27-Ago-2015 | Se agrego consulta por folio, serie, uuid. Se agrego consulta por folio, serie, uuid y método para consultar por uuid 
3.5.8.1 | 12-Ago-2015 | Soluciona problemas para bajar correos y subir facturas a bóveda en servidores des-actualizados. Se quito verificación de certificados ssl al hacer prueba de internet
3.5.7.0 | 09-Abr-2015 | Se removió soporte para descargar archivos ZIP ya que ocasionaba problemas en maquinas con pocos recursos. Solución al error sql: Scan error on column index 2: unsupported driver -> Scan pair: <nil> -> *string
3.5.6.2 | 24-Mar-2015 | Mejoras en control de dependencias y manejo de errores. Corrección en lanzador corrige el problema en maquinas lentas OCF no se iniciaba correctamente, Mejoras en el control de errores y capacidades de recuperación
3.5.6.0 | 20-Mar-2015 | Corrección error. Al guardar un comprobante emitido en buzon sait se borraba la carpeta buzon sait en windows server 2003 
3.5.5.6 | 18-Mar-2015 | Mejoras en manejo de procesos, descargar zip desde correo electrónico, mejor manejo de CPUs, Buzon SAIT mejo de CFDIS emitidos. Se realizaron cambios para manejar CFDIS Emitidos en buzón SAIT, Rutina de correo descarga archivos ZIP, Corrección al error Can't not create data base "rfc", Procesar rfc con espacios, Programa aprovecha mejor los hilos del procesador
3.5.4.0 | 12-Mar-2015 | Se agregaron funciones para consultar CFDIS Emitidos. Se realizaron cambios para consultar información de CFDIS Emitidos. 
3.5.3.0 | 11-Mar-2015 | Corrección al descargar correo no se tomaba en cuenta archivos con terminación XML, PDF. Se preparo para que soporte la descarga de archivos xml, pdf escritos en mayúscula XML, PDF desde correos electrónicos
3.5.2.0 | 09-Mar-2015 | Mejora para sistemas Win2003 al bajar correos IMAP. En sistemas win2003 no actualizados corrige el siguiente error x509: certificate signed by unknown authority
3.5.1.0 | 04-Mar-2015 | Modifica Sistema de errores. Se modifico sistema de reporte de errores de SAIT el archivo de errores se renombrar de sait.log a error.log y la estructura de errores es mas clara ademas lo errores estarán todos en buzonsait/error
3.5.0.0 | 19-Feb-2015 | Organiza comprobantes y funciona con SAIT. Se agrego inicio en carga de comprobantes de manera que pueda recibirlos en formato zip 
