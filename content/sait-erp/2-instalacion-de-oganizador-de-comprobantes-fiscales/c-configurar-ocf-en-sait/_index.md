+++
title = "Configurar OCF en SAIT"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=IzCpd5yOCuM&feature=youtu.be target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="configurar-cf-en-sait.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>


### **Indicaciones**:

Para configurar el Organizador de Comprobantes Fiscales dentro de SAIT, es decir que las facturas que nos envían los proveedores a nuestro correo estén disponibles para consulta o para ingresar una compra a partir de un archivo XML siga las siguientes instrucciones.

1.Entrar a SAIT e ir a Utilerías / Configuración General 


2.Seleccionar pestaña de otros.

Dar clic en **[Configurar Organizador de Comprobantes Fiscales OCF]**


![IMG](OCF.png)


3.**Configurar IP**.

Algo importante a considerar es configurar la dirección IP del servidor como estática.
El OCF se enlaza por medio de la IP del servidor, por lo tanto se recomienda usar una IP estática ya que al cambiar la IP del servidor la conexión se perderá hasta que ingresemos la dirección IP que tenga en ese momento nuestro servidor.


* Esta configuración será necesario realizarla en servidor y en cada una de las estaciones.

* En caso de contar con más de una empresa, es necesario entrar a cada empresa y definir la dirección IP del servidor.

<h3 class="text-primary">Si no conocemos cuál es la IP del servidor siga las siguientes instrucciones:</h3>

* En el servidor, ir al Buscador de Windows y teclear la palabra CMD
<br />
![IMG](CMD.png)

* Escribir ipconfig y dar **[Enter]** en la ventana de comandos.
<br />
![IMG](ipconfig.png)
 
La dirección IPv4 es el dato que necesitamos tomar y definir en el OCF dentro de SAIT.
 
![IMG](ipv4.png)


4.Definir dirección IP del servidor y definir el correo electrónico que usaremos para recibir las facturas.

Ir a Utilerías / Configuración General / Pestaña otros / Configurar Organizador de Comprobantes Fiscales (OCF).

{{%alert success%}} Por cuestiones de compatibilidad con el sistema se recomienda utilizar Gmail {{%/alert%}}
Configuración del correo de recepción

* Ingresamos nuestro correo electrónico
<br />
* Ingresa la contraseña del correo 
<br />
* Definimos el servidor de entrada: imap.gmail.com
<br />
* Definimos el puerto de entrada: 993 
<br />

![IMG](guardarconfig.png)
Dar clic en **[Comprobar Acceso]**

Si todos los datos están correctos nos mostrará el siguiente mensaje.

![IMG](comprobar.png)

<br />

Dar clic en **[Guardar Configuración]**

![IMG](guardarconfig.png)



5.**Folder Receptor de Comprobantes**

El sistema crea un folder receptor de comprobantes en donde usted y su personal pueden colocar los CFDIs que descarguen manualmente.

El folder es c:\buzonsait si usted cuenta con una red, deberá compartir el folder c:\buzonsait con su personal y será necesario crear un acceso directo de buzonsait en las estaciones. 

![IMG](carpetabuzon.png)



<h3 class="text-primary">Para definir una IP fija en el servidor siga las siguientes instrucciones: </h3>


Cuando no tenemos una IP fija en el servidor, ésta puede estar cambiando cada vez que apaguemos el equipo, por lo que SAIT nos estará mostrando el mensaje de falta de conectividad con el server, para evitar dicho inconveniente es necesario definir una dirección IP fija.

Nos dijimos a: 

Panel de control / Redes e Internet / Centro de redes y recursos compartidos 

Seleccionar la opción de Cambiar configuración del adaptador.

![IMG](CentroDeRedes.JPG)

Click derecho sobre la conexión, y seleccionamos la opción de propiedades.

![IMG](Propiedades.png)

  Seleccionamos la opción Protocolo de inversión 4(TCP/IPv4) y damos Click en Propiedades. 

![IMG](IPV42.png)

 Y definimos Usar la siguiente dirección IP.

Abriremos el CMD para tomar los datos necesarios.

Ingreasamos los datos como se muestra en la siguiente imagen damos Click en **[Aceptar]** para guardar y listo.

![IMG](Fija.png)