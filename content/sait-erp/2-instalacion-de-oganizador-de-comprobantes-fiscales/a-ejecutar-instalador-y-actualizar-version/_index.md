+++
title = "Ejecutar Instalador de OCF y Actualizar Versión"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=9AXemRR7oWE&feature=youtu.be target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="ejecutar-instalador-de-OCFy-actualizador.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>


### **Indicaciones**:

A partir de la versión de SAIT 2015, el sistema cuenta con un Organizador de Comprobantes Fiscales (abreviado OCF) que le ayuda a mantener resguardados y en orden todos los CFDIs que le envían sus proveedores así como los emitidos. 

1.Descargar instalador desde la siguiente liga:

* Instalador: **http://www.contabilidad.digital/descarga/sait-ocf-instalador.exe**

* Instalador que pregunta ruta: **http://www.contabilidad.digital/descarga/sait-ocfx-instalador.exe**

* Última versión del servicio: **http://www.contabilidad.digital/descarga/saitbovedawin32.exe**

2.Ejecutar instalador.

Dar clic en **[Ejecutar]**


![IMG](ejecutarinstalador.png)


3.Aceptar el acuerdo de uso.
Dar clic en **[Acepto]**

![IMG](aceptaracuerdoocf.png)


4.Instalar
Dar clic en **[Instalar]** y esperamos a que se termine de completar el proceso.

![IMG](instalar1.png)

![IMG](instalar2.png)

5.Actualizar OCF.

Ir a la siguiente ruta:

C:\Program Files (x86)\SAIT\Boveda3


Localizar el ejecutable llamado: **sait-ocf-actualizador.exe**


Dar clic derecho sobre el ejecutable y seleccionar opción Ejecutar como Administrador.
 
![IMG](actualizar.png)

Se nos abrirá una ventana de comandos. Escribimos “SI”.

![IMG](cmd.png)
 
Esperamos a que se complete la descarga de los archivos necesarios.

Al finalizar la actualización presionamos le tecla “Enter” y listo nuestro OCF ha sido actualizado.

![IMG](enter.png)


 

