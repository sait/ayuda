﻿+++
title = "Nota de crédito por concepto y aplicación para restar saldo de factura"
description = ""
weight = 13
+++

Esta opción es de utilidad cuando se requiere realizar una nota de crédito a una factura ya sea por, pronto pago, por descuento o por diferencia de precio.

Para realizar este proceso primeramente deberá dar de alta un artículo que utilizaremos para la nota de crédito.

<h2 class="text-primary">1) Crear artículo</h2>

1.Ir al Inventario / Catálogo de Artículos y Servicios

2.Registramos un articulo por ejemplo:

* Clave ‘DPP’, Descripción ‘DESCUENTO POR PRONTO PAGO’

* En la pestaña de Clasificación deberá de indicar la clave SAT ‘84111506’ que corresponde a servicios de facturación y marcaremos la casilla de "Servicio".

* El artículo debe tener la clave de unidad ‘ACT’ de actividad, si no existe registrarla en Inventario / Catálogo de Unidades / Agregar y lo damos de alta de esta manera ![IMG](2.png)

* En la pestaña de Precios el precio queda en cero y el impuesto en %16.

* Clic en [Grabar]

![IMG](1.png)

<h2 class="text-primary">2) Realizar la Nota de Crédito</h2>

3.Ir a Ventas / Devoluciones y Notas de Crédito

4.Se le presentara la ventana ‘Devolución de Venta’

5.Hacer clic en el boton ‘Capturar Artículos’.

![IMG](3.png)

6.Capturar los datos de la devolución como habitualmente se realiza, cliente, comentarios, etc.

Es importante mencionar las diferencias de En Efectivo y En Crédito al seleccionar la Forma al hacer una nota de crédito

* Si seleccionamos "En Efectivo" significa que le dimos en efectivo al cliente el importe.

* Si seleccionamos "En Crédito" quiere decir que se generará un saldo a favor en el estado de cuenta del cliente, el cual se podrá aplicar a una factura

![IMG](4.png)

7.En los artículos capturará el concepto que recién dimos de alta. Es decir la clave DEVOLUCION. Una vez elegido el articulo, en cantidad colocamos 1, y en P.Unitario colocaremos el monto a devolver antes de impuestos y verificamos que el total de la derecha superior coincida con el importe por el que deseamos se haga la nota de crédito.

Para efectos de la prueba, haremos la nota de crédito por un total $500.MN ya con impuestos.

8.Clic en [Procesar] o bien, presionar F8

- Se abrira el nuevo formulario ‘Información del CFDI’

	- Capturar la Forma de Pago en que se pagara la nota de crédito.

	- Si no se conoce la forma de pago, podremos seleccionar ‘99 - Por definir’

	- En el campo ‘Método de Pago’ le sugerira ‘PUE – Pago en una sola exhibición’.

	- Sin embargo, quedara habilitado para la selección que usted desee

	- En el campo ‘Uso del CFDI’ aparecera fijo ‘G02 - Devoluciones, descuentos o bonificaciones’. Debido a que es el correspondiente para Notas de Crédito

	- En el campo ‘Tipo de Relación’ seleccionar el tipó de relación que tendra esta Nota de Crédito en referencia a las facturas que acontinuación se relacionaran.

	- Clic en el signo de ? para agregar un CFDI Relacionado 

![IMG](5.png)

- Se le presentara el formulario ‘Búsqueda de CFDI Relacionados’

	- Ingresar los filtros de busqueda para mostrar las FACTURAS DEL CLIENTE SIN CANCELAR

	- Clic en [Buscar]

    - Marcar la casilla de las facturas que deseemos relacionar

	- Clic en [Relacionar]

![IMG](6.png)

Antes de procesar la ventana se deberá de ver de la siguiente manera, si todo está correcto, damos clic en F8=Procesar

![IMG](7.png)

Confirmamos la acción dando clic en [Sí]

![IMG](8.png)

Se geneará la nota de crédito y tomaremos nota del folio de la misma, en este caso es la AA49

![IMG](9.png)

Si consultamos el estado de cuenta del cliente notaremos se agregó un movimiento de nota de crédito con saldo a favor

![IMG](14.png)

<h2 class="text-primary">3) Aplicar Nota de Crédito desde Cobranza</h2>

9.Ir a Cobranza / Registrar Pagos de los Clientes 

10.Seleccionar el cliente, fecha de aplcación, en Forma de Pago deberá seleccionar **"APLIC. NOTA DE CRÉDITO"**, en el folio deberá ingresar el folio de la nota de crédito, damos [Enter] para que el sistema cargue automáticamente en el importe el saldo a favor

![IMG](10.png)

11.Seleccionamos la factura a la cual deseamos aplicar la nota de crédito y damos clic en F8=Procesar

![IMG](11.png)

12.Damos clic en Sí 

![IMG](12.png)

13.Y listo, de esta manera si revisamos el estado de cuenta del cliente la factura de 2,500 pesos ahora tiene un saldo de 2,000 pues se le restaron los 500 de la nota de crédito.

De igual forma la nota de crédito ya no aparece en azul pues ya fue aplicada

![IMG](13.png)
