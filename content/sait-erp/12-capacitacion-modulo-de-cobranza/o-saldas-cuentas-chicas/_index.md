﻿+++
title = "Saldar cuentas pequeñas"
description = ""
weight = 14
+++

A partir de la versión 2023.25 se agregó esta funcionalidad que permite saldar cuentas pequeñas en el estado de cuenta de los clientes.

A continuación se detallan las instrucciones:

En Cobranza / Verificar Saldo de los Clientes

1.Ir a la pestaña saldar deudas pequeñas

![IMG](VerificarSaldos-SaldarDeudasPequeñas1.jpg)

2.En el campo: Deudas menor a, establecer el importe máximo para considerar como deudas pequeñas. Todos los saldos menores a este importe nos aparecerán en la consulta.

3.Puede cambiar la divisa del importe: pesos o dólares. con la barra espaciadora del teclado.

4.Puede especificar Cliente, si deja en blanco nos incluirá a todos

5.Dar clic en el botón [Consultar]

![IMG](VerificarSaldos-SaldarDeudasPequeñas2.jpg)

6.Si desea saldar todas las facturas puede dar clic en la opción Todos, como se muestra en la siguiente imagen:

![IMG](VerificarSaldos-SaldarDeudasPequeñas3.jpg)

7.Si no se desea saldar todas las facturas puede seleccionar los documentos que desee en la columna Saldar, ver siguiente imagen

![IMG](VerificarSaldos-SaldarDeudasPequeñas4.jpg)

8.Una vez seleccionados los documentos deseados, debe establecer el concepto de abono a usar para saldar las cuentas. Puede utilizar cualquiera del los ya definidos en cobranza o puede dar de alta uno nuevo para este proceso, puede seguir la guía [Conceptos de cuentas por Cobrar](/sait-erp/12-capacitacion-modulo-de-cobranza/k-conceptos-de-cxc/)


9.Damos clic en el botón [Saldar deudas marcadas]

![IMG](VerificarSaldos-SaldarDeudasPequeñas5.jpg)

10.Dar clic en el botón [Si], nos aparecerá el siguiente mensaje

![IMG](VerificarSaldos-SaldarDeudasPequeñas6.jpg)

Es importante mencionar que la consulta se puede enviar a Excel solo debemos dar clic en el icono con el logo de Excel como se muestra en la siguiente imagen

![IMG](VerificarSaldos-SaldarDeudasPequeñas7.jpg)

Nos generara un archivo como el siguiente:

![IMG](VerificarSaldos-SaldarDeudasPequeñas8.jpg)
