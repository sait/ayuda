﻿+++
title = "Captura Masiva de Pagos"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=37yakMs3t1s&index=4&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. C Captura Masiva de Pagos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El siguiente proceso describe los pasos para registrar pagos masivos a diferentes facturas de diferentes clientes.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Captura Masiva de Pagos</h4>

1.Escriba la **fecha** del pago.

2.Escriba el **tipo de cambio** al que se van a tomar los dólares.

3.Escriba el **folio de la factura**.

4.Se muestran los datos del documento: **Cliente, Fecha, Total, Divisa, Saldo**.

5.Capture el **importe del pago**.

6.Seleccione con la barra espaciadora la **divisa**.

7.Especifique la forma de pago: **EF** para Efectivo, **CH** para Cheque, **TC** para Tarjeta de Crédito, etc.

8.Especifique el **folio del pago** en caso de ser necesario.

9.Presione la tecla **[F8]** para procesar los pagos.

10.Listo


![IMG](1.png)

<h2 class="text-danger">Importante</h2>

#### Es importante tener cuidado de no repetir pagos a una misma factura.