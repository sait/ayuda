﻿+++
title = "Emitir Recibo de Pago a Terceros (Clientes con mismo RFC, pero distinta clave en SAIT)"
description = ""
weight = 13
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=a5lxo9Igl7Y&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="emitir-pagos-a-tercero-mismo-rfc-diferente-clave-sait.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

A partir de la versión 2018.53 SAIT cuenta con la opción de emitir REP a terceros, el cual puede ser de gran utilidad para las empresas que por distintos motivos tengan dados de alta un mismo cliente, es decir mismo RFC, pero con diferente clave SAIT.

Supongamos tenemos el siguiente cliente: EQUIPOS ELECTRICOS GARCIA, S.A DE C.V el cual se dio de alta dos veces para diferenciar sus sucursales, como podemos notar cuentan con el mismo RFC y razón social, pero con distinta clave de SAIT.

![IMG](1.png)

El detalle que puede presentarse es que al momento de emitir el complemento de pago el sistema nos lo detectará como clientes diferentes por contar con distinta clave de cliente.

Para poder timbrar los pagos a un mismo RFC, deberá utilizar la opción de Emitir REP a terceros, para utilizar esta opción dentro de SAIT siga las siguientes instrucciones:

1.Cobranza / Emitir CFDI de Pago / Emitir REP a tercero


![IMG](2.png)

2.Aparecerá la siguiente pantalla:

En donde ingresaremos el RFC y Razón Social del cliente al que deseamos se emita el recibo de pago:

(El cliente al que le emitiremos el recibo de pago puede o no estar dado de alta en el sistema, si es el caso, donde es la primera vez que le emitiremos un recibo de pago a ese cliente en la siguiente ocasión el sistema ya lo reconocerá como existente).

Damos clic en Agregar Pago.


![IMG](3.png)


3.Hacemos la búsqueda con los filtros de cliente o rango de fecha deseados.

Damos clic Consultar. 

Como podemos notar aparecen todos los pagos de diferentes clientes y el sistema nos permite seleccionar los diferentes pagos aunque sean de distintos clientes.

Seleccionamos los pagos a timbrar y damos clic en Agregar Pagos.


![IMG](4.png)

4.Si ya estamos listos para emitir el recibo de pago, daremos clic en F8 Generar REP.

![IMG](5.png)

Damos Clic en Confirmar

![IMG](6.png)

Y listo, lo recibos de pago fueron emitidos.

5.La consulta se hace de la misma manera es decir desde el menú de:

Cobranza / Consultar CFDIS de pago 

Damos clic en Consultar y aparecerá el complemento recién timbrado.


![IMG](7.png)


Si consultamos el XML, podemos visualizar el RFC receptor es el que nosotros indicamos, en este caso y para efecto de la demostración se puede notar dicho RFC es un tercero, es decir un RFC receptor que no tiene relación con las facturas que pagamos anteriormente.

![IMG](8.png)


