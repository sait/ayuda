+++
title="Agregar, modificar y eliminar Cuentas Origen"
description=""
weight = 3
+++

Para agregar las cuentas origen de un cliente puede usar esta guía.  
El uso de este campo en el CFDI es opcional y se recomienda usarlos solo si el cliente lo solicita. 

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=TL7m6DdceM4" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

#### Agregar
- Ir a Cobranza / Abonos
- En el campo 'Forma de Pago' ingresar una forma de pago que requiera la captura de 'Cuenta Origen' y 'Depositado en'
- En el campo 'Cliente' ingresar el cliente que realizaremos la asignación
- En el campo 'Cuenta Origen', clic en [+] o [Agregar]
- Nos presentara la ventana 'Agregar Cuenta Origen del Cliente'
- Seleccionar la Forma de Pago a que asignaremos la cuenta origen, así como el Emisor o Banco
- Según la forma de pago, será la referencia a ingresar en el campo final del formulario, ejemplo:
  - Cheque Nominativo -> Número de Cuenta (11 dígitos) Cuando una cuenta de cheques no tiene los once digitos debera rellenar los campos con ceros al inicio.
  - Transferencia electrónica de fondos -> Clabe interbancaria (18 dígitos)
  - Tarjeta de Crédito -> No. Tarjeta (16 dígitos)
- Clic en [Agregar]

![IMG](imageAddCta.png)

#### Modificar
- Ir a Cobranza / Abonos
- En el campo 'Forma de Pago' ingresar una forma de pago que requiera la captura de 'Cuenta Origen' y 'Depositado en'
- En el campo 'Cliente' ingresar el cliente que realizaremos la asignación
- En el campo 'Cuenta Origen', clic en [Modificar]
- Nos presentara la ventana 'Modificar Cuenta Origen del Cliente'
- Seleccionar el Emisor o Banco que se modificara la cuenta origen
- Según la forma de pago, será la referencia a ingresar en el campo final del formulario, ejemplo:
  - Cheque Nominativo -> Número de Cuenta (11 dígitos) Cuando una cuenta de cheques no tiene los once digitos debera rellenar los campos con ceros al inicio.
  - Transferencia electrónica de fondos -> Clabe interbancaria (18 dígitos)
  - Tarjeta de Crédito -> No. Tarjeta (16 dígitos)
- Clic en [Modificar]

#### Eliminar
- Ir a Cobranza / Abonos
- En el campo 'Forma de Pago' ingresar una forma de pago que requiera la captura de 'Cuenta Origen' y 'Depositado en'
- En el campo 'Cliente' ingresar el cliente que realizaremos la asignación
- En el campo 'Cuenta Origen', clic en [Modificar]
- Nos presentara la ventana 'Modificar Cuenta Origen del Cliente'
- Clic en [Eliminar]

![IMG](imageEditCta.png)


## Agregar un nuevo emisor o banco de cuenta origen.


#### Agregar 
- Ir a Cobranza / Abonos
- En el campo 'Forma de Pago' ingresar una forma de pago que requiera la captura de 'Cuenta Origen' y 'Depositado en'
- En el campo 'Cuenta Origen', clic en [+] o [Agregar]
- Nos presentara la ventana 'Agregar Cuenta Origen del Cliente'
  - En el campo 'Emisor o Banco' clic en [+] o [Agregar]
  - Nos presentara la ventana 'Agregar Emisor Cuenta Origen'
      - Ingresar el RFC de dicho emisor o banco. En caso de ser extranjero puede usar 'XEXX010101000'
      - Ingresar el Nombre de emisor o banco
      - Clic en [Agregar]

![IMG](imageAddEmisor.png)

#### Modificar
- Ir a Cobranza / Abonos
- En el campo 'Forma de Pago' ingresar una forma de pago que requiera la captura de 'Cuenta Origen' y 'Depositado en'
- En el campo 'Cuenta Origen', clic en '+' o 'Agregar'
- Nos presentara la ventana 'Agregar Cuenta Origen del Cliente'
  - En el campo 'Emisor o Banco' seleccionar el Emisor que deseamos modificar
  - Clic en [Modificar]
  - Nos presentara la ventana 'Modificar Emisor Cuenta Origen'
      - Actualizar los datos RFC y Nombre con los correctos
      - Clic en [Modificar]

#### Eliminar
- Ir a Cobranza / Abonos
- En el campo 'Forma de Pago' ingresar una forma de pago que requiera la captura de 'Cuenta Origen' y 'Depositado en'
- En el campo 'Cuenta Origen', clic en '+' o 'Agregar'
- Nos presentara la ventana 'Agregar Cuenta Origen del Cliente'
  - En el campo 'Emisor o Banco' seleccionar el Emisor que deseamos eliminar
  - Clic en [Modificar]
  - Nos presentara la ventana 'Modificar Emisor Cuenta Origen'
      - Clic en [Eliminar]

![IMG](imageEditEmisor.png)


Estos procesos también se pueden realizar desde la ventana Cobranza / Registrar Pagos de los Clientes

