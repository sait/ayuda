﻿+++
title = "Emitir Recibo de Pago a Terceros (Factoraje)"
description = ""
weight = 12
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=jNWrIccg29w&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="emitir-pagos-a-terceros-factoraje.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


A partir de la versión 2018.53, el sistema  cuenta con la opción de emitir REP a terceros, el cual es de gran utilidad en los siguientes casos:

1.	Para emitir el Recibo de pago a empresas financieras,  cuando la  empresa vende sus cuentas por cobrar, o facturas, para financiar su operación (Factoraje)

2.	Para emitir el recibo del pago en los casos de cobranza delegada, es decir los que emite la empresa financiera o la empresa de cobranza.

En el siguiente diagrama se pueden observar los distintos procesos que se llevan a cabo al utilizar el factoraje.
![IMG](factoraje.png)

Es importante mencionar que para poder utilizar este proceso en SAIT debe contar con el Organizador de Comprobantes Fiscales instalado y actualizado en su máquina servidor. Para instalar y configurar el OCF deberá acceder al siguiente enlace, clic [aquí](/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/)


Las instrucciones para poder realizar un Recibo de Pago Electrónico por Factoraje son las siguientes:


### PASO 1

Crear el concepto de Comisión Factoraje en el menú de:

Cobranza / Conceptos de Cuentas por Cobrar

De clic en [Agregar] y llene los campos de la siguiente manera:

* Concepto: CF

* Descripción: COMISIÓN FACTORAJE

* Cargo/Abono: Seleccione Abono

* Palomee opción Capturar Referencia

* Formato: (opcional)

* Siguiente Folio: (opcional)

* Obligar a capturar folio: (opcional)

* Permitir folios repetidos: (opcional)

* Clave SAT: Compensación – 17

Clic en [Agregar]


![IMG](1.png)


### PASO 2

Capturar los abonos a la factura de la cual cedimos los derechos a la empresa de factoraje (Banco) en este caso es la factura con folio 25523 por un total de $ 10,000.00 pesos.

NOTA: Es importante mencionar esta factura tendrá dos partidas de pago, los cuales sumarán el total de la operación:

* El importe de lo que nos depositó el banco y

* Otro por el importe de la comisión del banco


Para registrar los pagos diríjase a Cobranza / Registrar Pagos de los Clientes


* Seleccione el cliente

* La fecha del pago

* Los datos Cuenta Origen y Cuenta Destino son opcionales

* Forma de pago en este caso es Transferencia

* Indique el importe: 9,000 pesos

* Clic en procesar o [F8]

* De clic en [Si] para confirmar la acción

![IMG](2.png)


Refresque la pantalla de pagos

* Seleccione el cliente nuevamente

* Indique la fecha del pago

* Forma de pago en este caso ahora deberá seleccionar el concepto: Comisión Factoraje (o comisión del banco)

* Indique el importe: 1,000 pesos

* Clic en procesar o [F8]

* De clic en [Si] para confirmar la acción

* Listo

![IMG](3.png)


### PASO 3

El siguiente paso es Emitir el Recibo de Pago Electrónico

Diríjase a Cobranza / Emitir CFDI de Pago

Seleccione Emitir REP a Tercero

![IMG](4.png)


Aparecerá la siguiente pantalla en donde usted puede seleccionar el cliente en caso de haya registrado como cliente a la empresa de Factoraje es decir ya está dado de alta en el sistema, sin embargo, usted puede emitir el recibo de pago al RFC que usted guste sin que esté dado de alta como cliente en el sistema.

En este caso haremos la demostración con un cliente que no existe en mi base de datos:

•	Ingresamos RFC receptor

•	Ingresamos Razón Social de receptor

Damos clic en [Agregar Pago]


![IMG](5.png)

Se nos mostrará la siguiente pantalla en donde realizaremos los pagos que recién registramos

Podemos buscar por cliente, por rango de fecha o por factura

Damos clic en [Buscar]

![IMG](6.png)

Seleccionamos ambos pagos.

Damos clic en [Agregar Pagos]

![IMG](7.png)

Finalmente damos clic en [F8 Generar REP]

![IMG](8.png)

Damos clic en [Si]

![IMG](9.png)

Nos aparecerá el siguente mensaje en donde confirma el recibo se emitió de manera correcta

Damos clic en [Aceptar]

![IMG](10.png)



### PASO 4

Como paso final, puede consultar el recibo de pago, el cual se hace de la misma manera que con cualquier REP

Diríjase a Cobranza / Consultar CFDIs de Pago

Damos clic en [Consultar]

Y nos aparecerá el recibo de pago, como podemos observar el campo de cliente no está lleno, esto es porque se emitió el recibo a un receptor que no existe en el sistema.

![IMG](11.png)

Y por este motivo al dar clic en PDF aparece el siguiente mensaje.

![IMG](12.png)

Sin embargo, si damos clic en XML podemos observar el receptor está correctamente indicado.

![IMG](13.png)











