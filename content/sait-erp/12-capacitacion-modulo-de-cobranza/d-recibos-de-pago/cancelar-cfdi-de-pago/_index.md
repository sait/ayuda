+++
title="Cancelar CFDI de Pago"
description=""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=iFxuqftdF6U" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

* Ir a Cobranza / Consultar CFDI de pagos
* Se nos presentará la ventana de consultas, seleccionamos el cliente del cual vamos a cancelar el CFDI.
* En  la consulta se debe seleccionar el rango de fechas donde entra el pago,
* Podemos especificar el folio o el UUID si lo conocemos
* Una vez especificados los datos de consulta hacemos clic en <Consultar>
* Si existen CFDIs de pagos en la consulta aparecerán en el catálogo.
* Debemos seleccionar el pago a Cancelar y hacemos clic en < Cancelar  CFDI>
* Se nos presentará la ventana de confirmación de cancelación, aquí colocamos algún comentario sobre la cancelación, seleccionamos el motivo y debemos escribir el folio del documento donde se indica, hacemos clic en < SI >
* Una vez cancelado el Status del documento cambiará a "Cancelado" y se mostrará en texto rojo, el acuse de cancelación aparece en el recuadro de la derecha.


{{%alert warning%}}Para que se habilite el boton < Cancelar CFDI> el usuario debe tener configurado el permiso para cancelar CFDI de pagos, puede verlo [Aquí](https://ayuda.sait.mx/sait-erp/12-capacitacion-modulo-de-cobranza/d-recibos-de-pago/habilitar-cfdi-pagos/){{%/alert%}}


![](1.PNG)
