+++
title="Dar acceso a CFDI de pagos"
description=""
weight = 1
+++

El acceso a emitir CFDI de pagos se da desde el menú de cobranza pero si el usuario no tiene acceso le aparecerán las opciones en gris, por lo que será necesario que se le den los permisos.

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=JhK5RDMduUE" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

- Nos dirigimos a Utilerías/Grupos de usuarios/Catálogo de niveles.
- Se nos mostrará una ventana donde tenemos que seleccionar el nivel de usuario que modificaremos.
- Nos movemos a la pestaña Cxc
- En la parte baja encontraremos los atributos que debemos marcar.
![IMG](ACCESO.png)
- El permitir emitir y el Cancelar CFDI de pagos son permisos por separado.
- Damos clic en grabar y listo.
