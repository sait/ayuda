﻿+++
title = "Conceptos de Cuentas por Cobrar"
description = ""
weight = 11
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=23xMPDDoSrM&t=0s&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&index=10" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. K Conceptos de Cuentas por Cobrar.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El catálogo de Conceptos de Cuentas por Cobrar permite definir los conceptos para realizar pagos o cargos al estado de cuenta de los clientes.

SAIT cuenta con los conceptos que comúnmente se utilizan al realizar cargos o abonos, pero también permite definir conceptos personalizados de acuerdo con las necesidades de cada empresa.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Conceptos de Cuentas por Cobrar</h4>

![IMG](1.png)

El siguiente proceso describe los pasos para agregar, modificar o borrar un concepto de cobranza.

<h2 class="text-primary">Agregar un Nuevo Concepto</h2> 

1.Ingrese al menú de Cobranza / Conceptos de Cuentas por Cobrar.

2.Haga clic en el botón **[Agregar]**

![IMG](2.png)

3.Capture la clave del nuevo concepto, la cual debe tener como máximo de 2 caracteres. Ejemplo: **TR**

4.Especifique la descripción del concepto. Ejemplo: Transferencia

5.Seleccione con la barra espaciadora el tipo: **Cargo o Abono**

6.Habilite la casilla si es necesario: Capturar Referencia. Es importante considerar que al capturar referencia se DEBE conocer a que documento pertenece el movimiento.

7.Especifique el nombre del formato que se utilizará como comprobante de impresión al registrar este tipo de movimiento.

8.Capture el siguiente folio con el que se va a iniciar el consecutivo del concepto. La próxima vez que registre un movimiento con este concepto, el sistema le mostrará el siguiente folio a partir del último movimiento capturado.

9.Habilite la casilla si es necesario: Obligar a Capturar el Folio del movimiento.

10.Habilite la casilla si es desea: Permitir Folios Repetidos.

11.Especifique la clave SAT a la que corresponde el concepto.

12.Por último, haga clic en el botón **[Agregar]**

![IMG](3.png)


<h2 class="text-primary">Modificar un Concepto</h2> 

La opción de modificar nos permite corregir errores en la definición de los conceptos de inventario.

Para realizar este proceso siga las instrucciones descritas a continuación:

1.Ingrese al menú de Cobranza / Conceptos de Cuentas por Cobrar.

2.Haga clic en el botón **[Modificar]**

![IMG](4.png)
 
3.Seleccione el concepto que va a modificar

4.Haga clic en el botón **[Modificar]**

5.Modifique la información que sea necesaria.

6.Haga clic en **[Modificar]**

![IMG](5.png)


<h2 class="text-primary">Borrar un Concepto</h2> 

En SAIT puede eliminar conceptos de cobranza, sin embargo, **lo más recomendable es no eliminarlos debido a que si ya se han realizado movimientos con ese concepto, al consultar en el estado de cuenta de algún cliente no sabrá a que movimiento se refiere**

Para realizar este proceso realice lo siguiente:

1.Ingrese al menú de Cobranza / Conceptos de Cuentas por Cobrar.

2.Haga clic en el botón **[Eliminar]**

![IMG](6.png)
 
3.Seleccione el concepto que va a Eliminar

4.Verifique que sea el concepto correcto.

5.Haga clic en **[Eliminar]**

6.El sistema le pregunta si desea borrar el concepto. Haga clic en **[Si]**

![IMG](7.png)

<h2 class="text-primary">Agregar un Formato de Impresión</h2> 

Cuando se agrega un nuevo concepto también puede colocar un formato para que se envíe a imprimir.

Para agregar el formato realice los siguientes pasos:

1.Ir al menú de Cobranza / Conceptos de Cuentas por Cobrar

2.Seleccione el concepto en donde desea colocar el formato

3.Haga clic en el botón **[Modificar]**

4.En el campo Formato colocar el nombre del formato a utilizar.

5.Puede dar clic en el botón **[...]** para buscar algún formato en específico.

6.Deberá dar clic en **[Modificar]** para grabar los cambios realizados.

![IMG](8.png)