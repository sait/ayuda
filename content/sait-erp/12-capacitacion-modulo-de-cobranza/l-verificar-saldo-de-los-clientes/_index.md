﻿+++
title = "Verificar Saldo de los Clientes"
description = ""
weight = 12
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=fW5s4DpUzbw&t=0s&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&index=11" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. L Verificar Saldos de los Clientes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

<h2 class="text-primary">Detectar clientes con problemas</h2>

La utilería para verificar saldos de los clientes permite corregir errores al momento de registrar movimientos al estado de cuenta del cliente.

Estos errores pueden tener diversas causas, la mayoría relacionados a problemas con el equipo lo cual ocasiona que no se registren correctamente todos los movimientos y quede registrado un saldo mayor o menor al correcto.

El sistema detecta errores en los saldos, al consultar el estado de cuenta del cliente se muestra una leyenda indicando que el saldo no concuerda con movimientos.

En ese momento, se puede verificar el problema y corregirlo mediante esta utilería.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Verificar Saldos de Clientes</h4>

1. Haga clic en la primer pestaña "Detectar clientes con problemas" para detectar inconsistencias en todos los clientes.

2. Especifique las condiciones para detectar los errores:

3. **Detectar diferencias en saldo de catálogo de clientes:** Detecta errores entre el saldo global del cliente vs movimientos.

4. **Detectar diferencias en saldo de documentos:** Detecta errores entre el saldo total del documento vs abonos.

5. No considerar diferencias menores a: Especifique la cantidad mínima para detectar errores, ya que en ocasiones puede haber diferencias por centavos.

8. Haga clic en **[Detectar Problemas]**

9. En caso de que el sistema detecte diferencias o errores se mostrará la siguiente información:


![IMG](1.png)

NOTA: La consulta puede copiarse y pegarse en word para que pueda ser impresa para su revisión.


<h2 class="text-primary">Corregir Saldos</h2>

En caso de detectar errores en los saldos de los clientes o documentos, se debe hacer una revisión para detectar que ocasionó el error para posteriormente corregirlo.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Verificar Saldos de Clientes</h4>

1. Haga clic en la segunda pestaña "Corregir Saldos".

2. Capture la clave del cliente o presione el botón **[?]** para buscar.

3. Se muestra el saldo en documentos y en catálogo. En caso de haber diferencias, hacer clic en **[Corregir Saldo en Catálogo]**.

4. En caso de haber inconsistencias en el saldo de documentos, se muestra la leyenda: "Saldo de algunos documentos es incorrecto" y se muestra la siguiente información:

![IMG](2.png)

5. Seleccione el documento y haga clic en **[Corregir Saldo de Documento]**.
