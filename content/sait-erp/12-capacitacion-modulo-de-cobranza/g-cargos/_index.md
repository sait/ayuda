﻿+++
title = "Cargos"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=db3uyH1RfQ4&index=6&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. G Cargos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Generalmente los cargos se graban directamente al estado de cuenta del cliente a partir de una factura a crédito.

Pero este proceso permite registrar cargos directamente al estado de cuenta del cliente tales como cargos por Cheques Devueltos, Saldos Anteriores, Facturas Eliminadas por Error del estado de cuenta, etc.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Cargos</h4>

1.Seleccione el concepto del cargo.

2.Escriba la clave del cliente. Hacer clic en **[?]** para buscar.

3.Escriba el folio del documento.

4.Capture la fecha del cargo.

5.Especifique el importe del cargo.

6.Seleccione con la barra espaciadora la divisa.

7.En caso de que el cargo sea en dólares, especifique el Tipo de Cambio.

8.Presione la tecla **[F8]** para procesar el cargo.
 

![IMG](1.png)

<h2 class="text-danger">Importante</h2>

#### En caso de que el concepto del cargo no se encuentre definido, consulte la siguiente dirección en la cual se especifican los pasos para configurar conceptos de cobranza:

#### https://ayuda.sait.mx/sait-erp/12-capacitacion-modulo-de-cobranza/k-conceptos-de-cxc/