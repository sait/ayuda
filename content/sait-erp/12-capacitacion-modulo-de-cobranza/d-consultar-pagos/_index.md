﻿+++
title = "Consultar Pagos"
description = ""
weight = 4
+++

La siguiente ventana le servirá de apoyo para verificar que al registrar los pagos de los clientes se inique en qué cuenta se recibió ese importe y también verificar si los pagos ya tienen su REP correspondiente.

![IMG](cuenta.png)

Esta ventana está disponible en Cobranza / Consultar Pagos, paara verificar los pagos SIN cuenta destino asignada, deberá habiltar la opción * Mostrar Pagos sin asignar Cuenta Bancaria, indicar el periodo y dar clic en Consultar, la ventana arrojará los abonos que no fueron asignados a una cuenta destino al momento de realizarse

![IMG](1.png)

Si desea consultar los abonos que sí se asigno una cuenta destino, deberá desmarcar la opción * Mostrar Pagos sin asignar Cuenta Bancaria y dar clic en Consultar

![IMG](2.png)
