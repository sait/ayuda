+++
title = "Configuración esquema de Factura Electrónica CFDI"
description = ""
weight = 5
+++


En esta ventana se define la versión de factura que se desee utilizar y se configura él  envió por correo electrónico y la impresión automática  de los comprobantes emitidos

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>

<div class="card">
<a href="https://www.youtube.com/watch?v=zSZ7CJAEo2M" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>
### **Indicaciones**:
  * Ir a utilerías/ Factura Electrónica/Configurar CFDI  
  * En esta ventana se tienen las siguientes opciones:
    * **Versión de CFDI**: Aquí se indica la versión de CFDI 4.0
    * **Formato PDF a utilizar**: Seleccionar el formato de impresión para CFDI que desea utilizar.
      * **Editar**: Se tiene la posibilidad de personalizar los formatos internos ya establecidos en el sistema
    * **Imprimir en el siguiente destino**: Se marca esta casilla si desea que los CFDI se impriman automáticamente después de emitirse.
      * **Todas las PCs**: La dirección de la impresora a la que las PC envían las impresiones.
      * **Esta computadora**:  La dirección de la impresora a la cual la computadora que se está usando va a enviar las impresiones.
    * **Enviar al cliente Correo Electrónico:** Marcar esta casilla si desea que se envié un correo electrónico con el PDF y el XML al cliente después de emitir la factura.
      * **Servidor SMTP**: Dirección de su proveedor de servicio de correo electrónico (ej: smtp.gmail.com)
      * **Puerto**: Puerto por el que se envían los correos
      * **Usuario**: Cuenta con la cual se envía el correo electrónico.
      * **Contraseña**: Contraseña de la cuenta.
      * **Usar Conexión SSL**: Marcar si el servidor usa SSL
      * **Usar Conexión TLS**: Usar si el Servidor usa TLS.
	  * **Anexar estado de cuenta**: Si se marca se enviara el estado de cuenta del cliente en el cuerpo del correo.
    El cual llegará de la siguiente manera:

![IMG](correo.png)

  * Si lo desea puede utilizar el Botón **[Probar Envío]** Con el que se puede enviar un Correo electrónico de prueba a una dirección especificada para revisar que los datos estén correctos.
  * **Generar CFDI de Traslado:** Si se marca esta casilla se generara un CFDI de traslado automáticamente al registrar salidas por traspaso o registrar una salida a sucursal.
  * **Generar REP:** Si se marca esta casilla se generara el REP de manera automática al registrar pago en cobranza, adicionalmente se pueden agregar esta opción también a las facturas de contado.
  * **Usar complemento IEDU:** Si se marca esta casilla podrá generar sus facturar con el complemento IEDU para escuelas, aquí toda la información: 
  https://ayuda.sait.mx/otros-temas/complemento-iedu/
  * Si todo esta correcto solo resta dar clic en **[Guardar Configuración]**;  

![IMG](cfdi.PNG)