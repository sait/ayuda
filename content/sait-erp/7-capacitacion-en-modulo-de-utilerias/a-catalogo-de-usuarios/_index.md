+++
title = "Catálogo de Usuarios"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=Yb9wXWLWdtU&index=1&list=PLhfBtfV09Ai6MVckTTAICCQ0E1u1n3N7_" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="7. A Catalogo de usuarios.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Dentro del catálogo de usuarios se lleva el control de las personas que tendrán acceso al sistema, se define su contraseña, nivel de acceso; así como la configuración para el envío de correos desde SAIT. 

Para agregar un nuevo usuario diríjase a:

<h4 class="text-primary">Utilerías / Catálogo de Usuarios</h4>

![IMG](1.png)

1.Haga clic en **[Agregar]**

![IMG](2.png)

 * Capture la clave del usuario, es un dato alfanumérico el cual consta de 5 caracteres. Esta clave es la que se usará para ingresar al sistema. 

 * Especifique el nombre completo del usuario. 

 * Capture la contraseña para acceder al sistema. 

 * Confirme la contraseña. 

 * Seleccione el Grupos de Usuarios /Catalogo de Niveles al que pertenece. 

 * De ser necesario, capture la información para Enviar correos. 
 
 * Haga clic en **[Agregar]**

 ![IMG](agregar.png)

2.Cómo Modificar un Usuario

![IMG](3.png)

* Seleccione de la lista de usuarios el que se desea modificar. Se pueden realizar búsquedas al presionar la tecla [F2] 

* Haga clic en **[Modificar]** 

* Se muestra la información. La clave se muestra deshabilitada ya que no puede ser modificada. 

* Realice la modificación necesaria. 

* Haga clic en **[Modificar]**

3.Cómo Eliminar un Usuario

![IMG](4.png)

* Seleccione de la lista de usuarios el que se desea eliminar. Se pueden realizar búsquedas al presionar la tecla [F2] 

* Haga clic en **[Eliminar]** 

* Se muestra la información. Verificar que sea el registro correcto que se va a eliminar. 

* Haga clic en **[Eliminar]**