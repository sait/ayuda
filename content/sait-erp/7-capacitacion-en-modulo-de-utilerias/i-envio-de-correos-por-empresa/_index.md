+++
title = "Envío de Correos por Empresa"
description = ""
weight = 8
+++

A partir de la versión 2024.12 SAIT le permite configurar una cuenta de correo corporativa y permitir a los usuarios enviar correos usando esa cuenta.


Para configurar este envío deberá dirigirse a 'Utilerías / Configuración general del  sistema / Otros' en [Credenciales Correo de la Empresa]

![IMG](1.png)

Aquí se deberá capturar la información del correo con el que se enviarán los documentos desde SAIT.

![IMG](2.png)

Si lo desea puede hacer una prueba de conexión dando clic en [Probar Envío]

![IMG](3.png)

De ser exitosa la prueba, le llegará un correo como este

![IMG](4.png)

De igual manera, desde Utilerías / Catálogo de Usuarios deberá seleccionar el método de envío de correo 'Usar Correo de la Empresa' para que considere las credenciales anteriormente comentadas.

![IMG](5.png)