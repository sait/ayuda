+++
title="Condicionar formatos"
description=""
weight = 2
+++

Ahora en SAIT, puedes manejar tus formatos de Facturas (tanto de contado y crédito) y devoluciones de venta (tanto en efectivo y crédito) en un mismo lugar, en donde se podra agregar condiciones para que al momento de registrar o consultar uno de estos documentos CFDI 4.0, solo nos de a elegir entre los formatos que queremos.

### Indicaciones:
* Ir a Utilerías/Modificar Formatos/Facturas y Notas de Crédito CFDI33
* Se nos presentara la ventara 'Configuración de formatos facturas y notas de crédito cfdi33'
* En el 'Cond Rep' ingresar la condición que tendra que cumplir el documento para usar ese formato. A continuación se anexan unos ejemplos que puede colocar en este campo:

| Ejemplo                        | Condición                                            |
|--------------------------------|------------------------------------------------------|
| Facturas                       | Allt(Docum.TIPODOC)=='F'                             |
| Devoluciones Venta             | Allt(Docum.TIPODOC)=='DV'                            |
| Facturas de Contado            | Allt(Docum.TIPODOC)=='F' and Docum.FORMAPAGO=='1'    |
| Facturas de Crédito            | Allt(Docum.TIPODOC)=='F' and Docum.FORMAPAGO=='2'    |
| Devoluciones Venta en Efectivo | Allt(Docum.TIPODOC)=='DV' and Docum.FORMAPAGO=='1'   |
| Devoluciones Ventas en Crédito | Allt(Docum.TIPODOC)=='DV' and Docum.FORMAPAGO=='2'   |
| Documentos cancelados          | Docum.STATUS==1                                      |
| Cliente en específico          | Allt(Docum.NUMCLI)=='1'                              |
| Almacén en específico          | Allt(Docum.NUMALM)=='1'                              |
| Documento en pesos             | Docum.DIVISA=='P'                                    |
| Documento en dólares           | Docum.DIVISA='D'                                     |

* En dado caso que no se ingrese un valor en este campo, cada vez que imprima un CFDI 4.0 (ya sea devol. de venta o factura) le sugerirá imprimir dicho formato.

![IMG](imageCatFtos.PNG)