+++
title="Catálogo de formatos adicionales"
description=""
weight = 2
+++

Aparte de contar con tu formato principal, se cuenta con la opción de agregar mas formatos de impresión de tus facturas y devoluciones de facturas con CFDI 4.0

Cabe destacar, que estos formatos solo se podran visualizar, imprimir o mandar por correo cuando el documento en cuestion se registre o consulte. Si se hace desde 'Ventas / Consultar CFDIs de Ventas', se nos presentara unicamente el formato principal.

### Indicaciones:
* Ir a Utilerías/Modificar Formatos/Facturas y Notas de Crédito CFDI33
* Se nos presentara la ventara 'Configuración de formatos facturas y notas de crédito cfdi33'

#### Agregar
* Clic en [Nuevo]
* En 'Descripción' ingresar el nombre con el cual identificaremos el formato
* En 'Grupos' ingresar la lista (separados por coma) de los grupos de usuarios que tendran permiso a seleccionar este formato
* En 'Cond Rep' ingresar la condición que tendra que cumplir el documento para usar ese formato
* En la sección de 'Formato' clic en [...] para buscar el archivo del formato. Puede dar clic en [Editar] para verificar que selecciono el formato correcto

#### Modificar
* Dentro de la lista 'Reportes Disponibles', seleccionar el formato que modificaremos
* Clic en [Editar]
* Se nos presentara el diseñador de reportes, en donde podremos realizar los ajustes necesarios al reporte
* Si desea guardar los cambios, ir a File/Save, o bien, teclear Ctrl+S
* Si desea salir, ir a File/Close, o bien, teclear ESC

#### Eliminar
* Dentro de la lista 'Reportes Disponibles', seleccionar el formato que modificaremos
* Clic en [Borrar]
* Cabe destacar que solamente se borrara el formato para imprimir, sin embargo, el archivo fisico se conservara por si se desea posteriormente reutilizar.

![IMG](imageCatFtos.PNG)

### Formatos disponibles

A continuación se anexan ejemplos de formatos que puede usar dentro de este catálogo (dichos formatos ya están actualizados para 4.0):

* [Formato normal tamaño carta](formato-normal-tamano-carta.rpt) 

	* [Vista Previa](sait-formato-normal-tamano-carta.pdf)

* [Formato normal tamaño miniprinter 80mm](formato-normal-miniprinter-80mm.rpt)

	* [Vista Previa](sait-formato-normal-miniprinter-80-mm.pdf)

* [Formato normal tamaño miniprinter 58mm](formato-normal-miniprinter-58-mm.rpt)

	* [Vista Previa](sait-formato-normal-miniprinter-58-mm.pdf)

* [Formato en escalas grises tamaño carta](formato-en-escalas-grises-tamano-carta.rpt)

	* [Vista Previa](sait-formato-en-escalas-grises-tamano-carta.pdf)

* [Formato con impuestos por partidas](formato-con-impuestos-por-partidas.rpt)

	* [Vista Previa](sait-formato-con-impuestos-por-partidas.pdf)

* [Formato factura no timbrada](factura-no-timbrada.rpt)

	* [Vista Previa](sait-factura-no-timbrada.pdf)