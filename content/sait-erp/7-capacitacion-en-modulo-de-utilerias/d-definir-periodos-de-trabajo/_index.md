+++
title = "Definir periodos de trabajo"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=RbTd75FgnxQ&index=3&list=PLhfBtfV09Ai6MVckTTAICCQ0E1u1n3N7_" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="7. D Definir periodos de trabajo.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El sistema cuenta con la opción de definir periodos de trabajos, es decir permitir en un periodo de tiempo movimientos en caja, inventarios, etc.

En ocasiones si no está bien definidas las fechas puede ser que al momento de realizar algún proceso dentro del sistema no nos permita avanzar y nos muestre el siguiente mensaje.

![IMG](2.PNG)

Para definir los periodos y corregir estos mensajes siga las siguientes instrucciones:

* Nos dirigimos al módulo de Utilerías/ Definir Periodos de Trabajo.

* Capturamos las fechas tal y como se muestra en la siguiente imagen siendo la fecha final 31/12/2030.

![IMG](1.PNG)

Nota: se recomienda llenar todos los campos con una fecha determinada, previniendo problemas que se puedan presentar en un futuro. 

* Clic en **[Aceptar]** y LISTO.