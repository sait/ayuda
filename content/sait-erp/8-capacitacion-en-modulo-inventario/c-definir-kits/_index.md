﻿+++
title = "Definir Kits o Paquetes"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=BRSAtA5FmKw&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=4&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. C. Definir Kits.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT permite la definición de Kits o Paquetes, el cual se define como un concepto que está conformado por un conjunto de artículos, mismos que se descuentan del inventario una vez vendido o remisionado dicho Kit.
La regla principal para manejar Kits o Paquetes dentro de SAIT es que todos los artículos que lo conforman cuenten con existencia en el inventario.

El proceso para definir un Kit o un Paquete dentro del sistema es muy sencillo y se explica a continuación:

<h2 class="text-primary">Definir El Kit o Paquete</h2>

1.Dirigirse al menú de: Inventarios / Catalogo de Artículos y Servicios. 

2.Ingresar la clave del articulo (Kit) a Definir y capturar el nombre del paquete. 

3.Capturar los datos relacionados con la clasificación del paquete. 

4.En la pestaña de precios, es muy importante especificar los datos de Impuesto1 y asignar el precio normal del paquete. 

5.Hacer clic en el botón **[Grabar]**. 



![IMG](1.png)

<h2 class="text-primary">Dar de alta los productos que componen el KIT o Paquete</h2>

1.Dirigirse al menú de Inventarios / Definir Kits. 

2.Capturar la clave del Kit o Paquete que se generó en el catálogo de artículos y servicios. En caso de ser necesario, dar clic en el botón **[?]** para buscar. 

3.Deberá de ingresar en la cuadricula todos los artículos que conforman el Kit. 

4.Conforme se vayan capturando los artículos del paquete, la ventana muestra una sumativa en precios, con el objetivo de que usted pueda visualizar el precio del paquete si se adquieren los artículos por separado, es de suma importancia ya que le permite identificar también sumativas por costos de los artículos. 

5.Una vez finalizada la captura de los artículos y agregar el kit, hacer clic en el botón **[Grabar]**. 

6.Listo, de esta manera se ha definido el Kit o paquete para usarse en los módulos de: Ventas y Caja. 


![IMG](2.png)