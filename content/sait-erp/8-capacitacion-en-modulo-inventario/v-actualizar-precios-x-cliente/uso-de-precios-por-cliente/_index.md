+++
title="Uso de precios por cliente en Ventas"
description=""
weight = 5
+++


Para el uso de los Precios por Cliente deberá dirigirse a Ventas / Registrar Ventas

Como podemos observar el cliente con clave 2 no tiene un precio especial por lo que se carga el precio default de 600, caso contrario con el cliente con clave 1 el cual sí tiene un precio especial por la unidad adicional de CAJA y es por eso el precio que se carga es el de 500.


![IMG](ventas.png)