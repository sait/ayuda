﻿+++
title = "Actualizar Precios por Cliente"
description = ""
weight = 21
+++

SAIT le permite manejar precios especiales por cliente, esto incluye también poder hacer incrementos por porcentajes a los precios que tiene definidos.

También a partir de la versión 2021.21 se agregó una nueva funcionalidad para el manejo de precios especiales por unidades adicionales.

Para hacer uso de estas opciones en SAIT deberá digirise a Inventario / Actualizar Precios y Costos / Precios por Cliente

{{% children  %}}


