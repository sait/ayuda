﻿+++
title = "Recibir correo con los artículos que llegaron a su stock mínimo"
description = ""
weight = 20
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=bhzqXK3MA68&t=1s target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="recibir-reporte-con-articulos-con-existencia-menor-al-minimo.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT cuenta con la opción de calcular máximos y mínimos para tus artículos y de esta manera tener inventarios sanos y al día y por supuesto, no tener desabastos de mercancía, ahora para complementar esta función de SAIT te traemos esta nueva herramienta la cual te permitirá recibir un correo electrónico diario por sucursal en donde podrás visualizar todos los artículos que tengas existencia igual o menor de su stock mínimo.

Para ello, previamente si sus artículos no tienen definido un stock mínimo deberá llenar este dato para poder hacer uso de esta funcionalidad.

Después deberá dirigirse al menú de Utilerías / Configuración General del Sistema / pestaña de Inventario y activar esta opción, así como indicar el o los correos a los que estará llegar el reporte, si es más de un correo deberá separarlo por comas.

![IMG](existencias-minimas.png)

El reporte llegará de la siguiente manera a nuestro correo electrónico:

![IMG](html.PNG)

### Los artículos que no tengan definido un mínimo el sistema no los considerará en su reporte diario
