﻿+++
title = "Registrar Entradas y Salidas en Inventario"
description = ""
weight = 10
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=gI35tGrJgDE&index=9&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. J. Registrar Entradas y Salidas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Las opciones de registrar salidas y entadas te permiten realizar ajustes de inventario por motivo de mermas, salidas a sucursales, inventario inicial, etc.

<h4 class="text-danger">Es IMPORTANTE aclarar que los movimientos relacionados con las ventas y las compras deben realizarse en los módulos correspondientes, NO desde Inventario</h4>

Es decir:


![IMG](1.PNG)

<h2 class="text-primary">Salidas al Inventario</h2>

Para realizar salidas de artículos siga los pasos descritos a continuación:

1.Ir al menú de Inventarios / Registrar Salidas

2.Seleccionar el tipo de movimiento que desea realizar

3.Escriba la clave del artículo o presione **[F2]** para buscar

4.Escriba la cantidad de los artículos que van a dar salida

5.Si desea poner observaciones puede escribirlas en el campo descripción debajo del artículo, puede escribir en varios renglones.

![IMG](2.png)

6.Al terminar haga clic en el botón **[Procesar = F8]**

<h2 class="text-primary">Entradas al Inventario</h2>


Para realizar salidas de artículos siga los pasos descritos a continuación:

1.Ir al menú de Inventarios / Registrar Entradas

2.Seleccionar el tipo de movimiento que desea realizar

3.Escriba la clave del articulo o presione **[F2]** para buscar

4.Escriba la cantidad de los artículos que se van a ingresar y el costo de entrada

5.Si desea poner observaciones puede escribirlas en el campo descripción debajo del artículo, puede escribir en varios renglones.

![IMG](3.png)


6.Al terminar haga clic en el botón **[Procesar = F8]**
