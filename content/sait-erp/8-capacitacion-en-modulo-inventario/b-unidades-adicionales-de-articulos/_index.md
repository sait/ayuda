﻿+++
title = "Unidades Adicionales de Artículos"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=S55WZzwoHTo&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=3&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. B. Unidades Adicionales de Articulos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

La definición de unidades es una opción muy práctica que le permitirá vender con unidad diferente a la normal, solamente debemos establecer la equivalencia, por ejemplo el articulo X tiene la unidad normal piezas y también se desea vender por cajas debemos establecer cuantas piezas componen una caja.

A continuación te ilustramos un claro ejemplo de manejo de distintas Unidades de Venta para el artículo Agua embotellada:

![IMG](agua.PNG)


Como podemos observar, contamos con nuestra unidad principal del artículo que son PIEZAS. Y adicionalmente podemos vender ese mismo artículo en: CAJA y PAQUETES, con distinto precio. Con esto evitamos dar de alta un artículo distinto en nuestro catálogo. 


<h3 class="text-danger">Las unidades de Medidas definidas en el sistema solo sirven en el proceso de VENTAS.
</h3>


<h2 class="text-primary">Objetivos del Manejo de Unidades de Medida</h2>


1.Evitar dar de alta un mismo artículo, con presentaciones diferentes de unidades.

2.Manejo de distintos precios, dependiendo de la unidad de medida seleccionada en el proceso de ventas.

3.Descontar de manera CORRECTA el inventario, acorde a la equivalencia definida en la unidad de medida.

<h2 class="text-primary">Proceso para Definir Unidades de Medida</h2>

1.Dirigirse al menú de: Inventarios / Unidades

2.Aparecerá la siguiente ventana:

![IMG](2.jpg)


3.Escriba la clave del articulo o presione F2 para buscar

4.Aparecerán los datos del articulo y su unidad normal

5.Escriba la nueva unidad en el campo de Nueva Unidad

6.Escriba la equivalencia y defina los precios para la nueva unidad

7.Si desea que la nueva unidad sea la unidad predeterminada al vender marque la casilla Utilizar como predeterminada.

8.Ver siguiente imagen:


![IMG](3.png)


9.Haga clic en el botón **[Grabar]**

10.Listo, puede usar la nueva unidad en Ventas

Ahora el momento de vender ya sea desde Ventas o Caja, nos aparecerán las distintas unidades que hemos dado de alta.

<h2 class="text-primary">USO DE UNIDADES ADICIONALES EN MÓDULO VENTAS</h2>

En Ventas al ingresar la clave Botella es necesario posicionarnos en la columna unidad y presionar la barra espaciadora para visualizar las diferentes unidades.


![IMG](4.png)

<h2 class="text-primary">USO DE UNIDADES ADICIONALES EN MÓDULO CAJA</h2>

Al seleccionar la clave Botella se muestra de la siguiente manera en donde seleccionaremos la unidad indicada.

![IMG](5.png)

<h2 class="text-primary">AFECTACIÓN DEL KÁRDEX</h2>

En esta factura se se vendió una caja la cual corresponde a 12 piezas

![IMG](FACTURA.png)

Y de esta manera, en el kárdex se han descontado 12 piezas

![IMG](kardex.png)
