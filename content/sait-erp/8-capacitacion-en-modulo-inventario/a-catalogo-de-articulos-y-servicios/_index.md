﻿+++
title = "Catálogo de Artículos y Servicios"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=YFOqYCMmIz0&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=2&t=37s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. A. Catálogo de artículos y servicios.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT se cuenta con el Catálogo de Artículos en el cual podemos dar de alta todos los artículos que maneja nuestra empresa, así como la modificación y eliminación del articulo.

A continuación se explicarán todos los movimientos que se pueden realizar en el catálogo del artículo.

* [Agregar Artículo](#agregar-articulo)
* [Clasificaciones](#clasificaciones)
* [Precios](#precios)
* [Manejo de Precios con % de Margen de Ganancia](#precios-con-margen-de-ganancia)
* [Manejo de Precios con Factor de Ganancia](#precios-con-factor-de-ganancia)
* [Últimas Compras](#ultimas-compras)
* [Estadísticas](#estadisticas)
* [Observaciones](#observaciones)
* [Modificar Artículo](#modificar-articulo)
* [Eliminar Artículo](#eliminar-articulo)
* [Retención ISR y Retención IVA por Artículo](#retencion-por-articulo)


## Agregar Articulo

Ir al menú de Inventario / Catálogo de artículos y servicios. 
Se mostrará la siguiente ventana:

![IMG](1.png)

* Tienes que proporcionar una clave del artículo o servicio, con la cual pudas identificarlo fácilmente.

* Agregar la descripción del artículo o servicio.

* Puedes agregar también la marca, modelo, unidad de medida, y muy importante definir la clave SAT del artículo.

* Una vez que ingreses toda la información del artículo, deberás seleccionar el botón **[Grabar]** y Listo.

<h4 class="text-danger">NOTA</h4>

Los campos indicados en rojo son campos obligatorios para poder grabar información.

## Clasificaciones


En esta área usted puede otorgarle al artículo una clasificación por medio Marca, Línea, Familia, Categoría, Departamento, o  Proveedor;

Todo esto para un mejor control de inventario y un buen uso del mismo.

Al seleccionar la pestaña de **[Clasificación]** en el catálogo de artículos te proporcionará la siguiente ventana:

![IMG](2.png)

PPuedes definirle clasificaciones al artículo, como familia, línea, etc, **se recomienda utilizar al menos una clasificación** para facilitar la consulta de reportes o en su momento, inventarios parciales. 

Puedes marcar tu artículo también como si es un servicio, insumo, o usa series.

Una vez que tengas bien definida la clasificación de tu artículo seleccionas la opción de **[Grabar]**.

Listo.

Para habilitar / inhabilitar las opciones de clasificaciones deberán dirigirse al menú de Utilerías / Configuración general del sistema, pestaña de Inventarios

![IMG](Capturar.jpg)

## Precios

Al seleccionar la pestaña de **[Precios]** en el catalogo de artículos te proporcionará la siguiente ventana:

<h4 class="text-primary">Es súmamente importante mencionar que si utilizará la lista de precios, los precios deben de ordernarse del más alto al más bajo como se muestra en la siguiente imagen</h4>

![IMG](3.png)

## Precios con Margen de Ganancia

Si SAIT está configurado para utilizar % margen de ganancia entonces en la columna de margen % puedes configurar el % de ganancia que tendrás, desde el margen más alto de ganancia hasta el más bajo.

El precio se calculará de manera automática, en base al último costo, costo de sig. capa disponible o máximo costo.

![IMG](4.png)

Si tu configuración de Sait es **[Usar % margen de ganancia]** y quieres utilizar también precios fijos, únicamente tienes que habilitar la opción de **[Precio fijo]**, presionas la opción de **[Grabar]** y listo, ese artículo utilizará precios fijos.

## Precios con Factor de Ganancia

Si SAIT está configurado para utilizar precios con factor de ganancia, entonces en la columna de Factor, puedes configurar el Factor que tendrás, desde el factor más alto de ganancia hasta el más bajo.

El precio se calculará de manera automática, en base al factor que le hayas definido y sobre el último costo, costo de sig. capa disponible o máximo costo.

![IMG](5.png)


Una vez que ya tengas bien definido las opciones de precios de tu artículo, presionas el botón **[Grabar]**.

Listo.

<h4 class="text-danger">NOTA</h4>

Para configurar cualquiera de estas opciones hay que ingresar al menú de Utilerías / Configuración general del sistema
Seleccionar la pestaña de Inventarios / sección manejo de precios


## Ultimas Compras

Esta Opción del catálogo de artículos únicamente es de calidad informativa, en el cual tu puedes revisar cuales fueron las últimas compras que se realizaron del articulo en relación a las fechas de compras, costos del artículo, así como al proveedor que se le realizaron.

![IMG](6.png)


## Estadisticas

De igual manera en la pestaña de estadística nos muestra la información de las compras y ventas que ha tenido este artículo en el periodo de un año, lo cual nos informa el monto de las compras, la cantidad y la última compra o venta que se realizó en el año.


![IMG](7.png)


## Observaciones

Esta opción se utiliza para agregar información adicional sobre el artículo que desee, además se puede configurar para que siempre aparezca impresa al realizar una venta del articulo.

![IMG](8.png)

## Modificar Articulo

La modificación de la información del artículo desde el catálogo se realiza de la siguiente manera:

Ir al menú de Inventario / Catálogo de artículos y servicios.

Ingresar la clave del articulo o realizar una búsqueda oprimiendo la tecla **[F2]** o dando un clic en **[?]**.

Te aparece una ventana donde ingresas la descripción del artículo, una vez que encuentras el articulo lo seleccionas con doble clic o **[Enter]**.

Te aparece toda la información del artículo en el catálogo.

Realizas todas las modificaciones deseadas al artículo.

Una vez que ya realizaste las modificaciones, seleccionas la opción de **[Grabar]**.

Listo

![IMG](9.png)

## Eliminar Articulo

La eliminación de un artículo o servicio, desde el catálogo se realiza de la siguiente manera:

Ir al menú de Inventario / Catálogo de artículos y servicios.
Ingresar la clave del articulo o realizar una búsqueda oprimiendo la tecla **[F2]** o dando un clic en **[?]**.

Te aparece una ventana donde ingresas la descripción del artículo, una vez que lo encuentras, seleccionas con doble clic el artículo o **[Enter]**.
Te aparece toda la información del artículo en el catalogo.

Si el artículo que seleccionaste es el correcto, oprimes el botón de **[Borrar]**.

<h4 class="text-danger">Es importante mencionar que no se recomienda eliminar una clave que ya haya tenido movimientos en el kárdex, ya que no se podrán consultar después
</h4>

![IMG](10.png)

Listo.

Tu artículo fue borrado.


