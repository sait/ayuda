﻿+++
title = "Actualización Rápida/Masiva"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=XXC5ed4_5kA&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=7&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. F. Actualización Masiva.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT cuenta con la opción de actualización rápida de información que te permite realizar cambios en los datos de los productos de una forma muy sencilla y rápida.

Para realizar la actualización rápida diríjase a:

<h4 class="text-primary">Inventario / Actualización Rápida</h4>

1.Aparecerá una ventana como se muestra en la siguiente imagen:

![IMG](3.PNG)

2.En los campos de buscar puede escribir hasta tres palabras distintas para realizar la búsqueda de los artículos que desea actualizar **(si dejamos los 3 espacios en blanco me traerá todo el catálogo de artículos)**

3.Aparecerá la lista de los artículos buscados y usted podrá realizar las modificaciones necesarias en cualquiera de las columnas, excepto en la clave del artículo.

4.Al terminar haga clic en el botón **[F8=Procesar]**, para guardar los cambios.

![IMG](busqueda.PNG)

<h2 class="text-primary">CONFIGURACIÓN</h2>

Es importante comentar que las columnas de la ventana se pueden cambiar de acuerdo con las necesidades de la empresa.

Para configurar las columnas o datos a modificar diríjase a:

<h4 class="text-primary">Inventario / Actualización Rápida</h4>

1.Haga clic en el botón **[Configurar]**

![IMG](configurar.png)

2.Aparecerá la siguiente ventana:


![IMG](2.png)


3.Seleccione las casillas de los datos que desea modificar, puede desactivar los que no necesite.

4.Al terminar haga clic en el botón **[Continuar]**.

