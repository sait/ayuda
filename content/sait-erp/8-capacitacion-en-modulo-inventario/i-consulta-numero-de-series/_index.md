﻿+++
title = "Consulta de Números de Series"
description = ""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=_vw_ZRkUawQ&index=9&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. I. Consulta de Número de Series.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite llevar un control de los artículos que manejan número de series.

Para esto es necesario que al momento de dar de alta el artículo en el menú de 

Inventario / Catálogo de Artículos y Servicios se especifique si un artículo va a [ * ] Usar Series.

**Si un artículo ya había sido dado de alta y se quiere empezar a usar series, lo más correcto es dar de alta una nueva clave.**

![IMG](1.png)

### Captura de Número de Series


Para ingresar el registro de número de serie del artículo es necesario registrar la entrada del artículo por compra.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Compras / Registro de Compras</h4>

Por cada artículo ingresado en la cuadrícula de productos deberá presiona la tecla **[F3]** si el artículo usa SERIES.

En la ventana deberá especificar el número de serie del artículo como se muestra en la siguiente imagen:

![IMG](2.png)


Para consultar los números de serie debe asegurarse de contar con el reporte actualizado: [Consulta de Número de Series](consulta-de-número-de-series.rpt)  

Para instalarlo dentro del sistema diríjase a Utilerías / Recibir Reportes / y seleccione el formato descargado.

Una vez con el formato ya instalado o verificado que ya lo tiene continúe con las siguientes instrucciones, diríjase a:

<h4 class="text-primary">Inventario / Consulta de Números de Serie</h4>

![IMG](3.png)

1.Puede restringir la consulta por:

* Clave de un Artículo o Número de Serie en específico.

* Series pertenecientes a un Almacén en especifico

* Estatus de las series, cambiando con la barra espaciadora: Todas, Disponible, Vendidas

* Cliente o Proveedor en especifico

* Establecer por periodo de Fecha de Entrada y/o Fecha de Salida

2.Puede ordenar la información por clave, número de serie, cliente o proveedor.

3.Puede hacer clic en el botón **[Imprimir]**, para enviarlo a impresora.

4.O puede hacer clic en el botón **[Hoja]**, para visualizarlo en pantalla y enviarlo a Excel en donde se mostrará el siguiente reporte.

![IMG](4.png)

En el campo status las opciones son: **Todas, Disponible, Vendido**

<h2 class="text-primary">Recomendaciones SAIT para manejo de series</h2>

* Si un artículo no manejaba series y después desea manejar series (o viceversa) no se recomienda usar la misma CLAVE, sino crear una nueva y esta nueva clave marcarla o desmarcarla para el uso de series.


* En el catálogo de grupo de usuarios todos los niveles deben de tener habilitado este permiso

![IMG](1.jpg)

* En Utilerías / Configuración General del Sistema debe estar habilitada esta opción, la cual nos asegurará que no se de salida a series que no existen (esto se hace por cada servidor, no se transfiere por enlace de sucursales)

![IMG](2.jpg)

* Si utilizan Enlaces de Sucursales, se debe habilitar la opción de ENVIAR Y RECIBIR del evento **MOVSERIES** en el archivo SDCONDIG.exe que se encuentra en la carpeta del Enlace, esto es necesario para actualizar los estatus correctamente de las series en todas las sucursales.

![IMG](4.jpg)

* No se recomienda habilitar el siguiente permiso, ya que puede ocasionar que el usuario ingrese incorrectamente el folio consecutivo y después no se pueda aceptar o consultar un movimiento tipo traspaso u otro.

![IMG](3.jpg)

