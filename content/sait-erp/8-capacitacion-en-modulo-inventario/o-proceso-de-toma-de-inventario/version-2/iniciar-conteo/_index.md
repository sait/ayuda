+++
title = "Iniciar conteo"
description = ""
weight = 3
+++

Para iniciar un nuevo conteo, deberá dirigirse al menú de Inventarios / Toma de Inventario / Conteos de Inventario.

![Conteos2](conteos2.png)

* Haga clic en [Agregar Conteo]
* Se muestra la ventana para iniciar un nuevo conteo:

![Conteos3](conteos3.png)

Existen varios criterios al momento de consultar los articulos que se van a contar:

* Todos los artículos.
* Aleatorio: Se puede realizar un conteo aleatorio, indicando la cantidad de artículos que deseamos contar.
* Línea.
* Familia.
* Categoría.
* Departamento.
* Artículos asignados a algún proveedor.
* Los que tengan ciertas palabras en la descripción del artículo.
* Específicamente un artículo.

Una vez seleccionada la opción deseada, deberá hacer clic en [Consultar] y se muestran automáticamente los artículos que cumplen con las restricciones de la consulta.

![Conteos4](conteos4.png)

Se muestran activados todos los artículos para incluirlos en el conteo, si lo desea, puede desmarcar los que no se van a contar.

Por último, puede presionar la tecla [F8] o hacer clic en [F8 Iniciar] para abrir el conteo.

Nota: En caso de seleccionar un artículo que ya está incluido en un conteo abierto, se mostrará una alerta informando en que conteo se encuentra y no podrá ser incluido en el conteo que se desea abrir.

La consulta se puede imprimir o enviar a Excel.