+++
title = "Contar artículos"
description = ""
weight = 4
+++

Para contar artículos, debera ir al menú de Inventario / Toma de Inventario / Contar Artículos.

![Conteos5](conteos5.png)

A partir del conteo del artículo, se llevará el monitoreo de las entradas y salidas que tuvo posterior al conteo para que sean consideradas al final del proceso y se obtengan las diferencias considerando los movimientos posteriores al conteo.

{{%alert success%}} Para agilizar el proceso del conteo, se recomienda utilzar la aplicación para Android Sait INVENTARIO, esta aplicacion se enlaza con SAIT y nos proporciona la funcionalidad de la ventana de conteo, puede consultar las instrucciones de uso e instalación  en el siguiente enlace. [App Inventario](../../aplicacion-inventario/).  {{%/alert%}}

![App](app.png)

### Conteo individual


Una vez dentro de la ventana del conteo, se podrá capturar la clave del artículo o escanear con algún lector de códigos de barras. Se muestra la información del artículo y la descripción del tipo de conteo seleccionado (Aleatorio, Linea, Familia, Categoria, etc).

![Conteos6](conteos6.png)

En caso de que el artículo tenga capturada su ubicación en el almacén, se mostrará en la parte superior. Así mismo, se podrá capturar la ubicación donde se está realizando el conteo, ésta ubicación se podrá utilizar para actualizar la ubicación de los artículos contados.

Por último, deberá hacer clic en [Grabar] para almacenar la cantidad contada.


### Captura masiva
En caso de que desee capturar masivamente los artículos contados, debera ir al menú de Inventario / Toma de Inventario / Contar Artículos.

![Conteos7](conteos7.png)

Dentro de la ventana, deberá hacer clic en [Tabla] para que se muestre la captura masiva:

![Conteos8](conteos8.png)

Podrá capturar la ubicación donde se encuentran los articulos contados. La captura de los artículos puede ser manera individual, o bien, al descargarlos desde la lectora de códigos.

![Conteos9](conteos9.png)

Para finalizar, presione la tecla [F8] o haga clic en [F8 Procesar] para grabar el conteo.

Nota: Si desea regresar al conteo individual, haga clic en el botón [Forma]
