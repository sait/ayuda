+++
title = "Consulta general de conteos"
description = "Permite consultar los conteos de inventario realizados en la empresa"
weight = 2
+++

Permite consultar los conteos de inventario realizados en la empresa. Deberá dirigirse al menú de Inventarios / Toma de Inventario / Conteos de Inventario.

Desde esta ventana se puede:

* Consultar los conteos.
* Iniciar un nuevo conteo.
* Ver el detalle del conteo.

Al entrar a esta opción nos mostrará todas las tomas de inventario abiertas, con la finalidad de poder ver el detalle de los conteos o iniciar uno nuevo.

![Conteos1](conteos1.png)

La consulta se puede realizar aplicando las siguientes restricciones:

* Sucursal.
* Rango de fecha de inicio del conteo.
* Usuario que lo inició.
* Status:

1. Abierto: Conteo que esta pendiente cerrar. Los artículos que se encuentren en un conteo abierto, no podrán ser incluidos en uno nuevo.
1. Cerrado: Conteo finalizado, las diferencias no han sido ajustadas. Los artículos del conteo podrán ser incluidos en uno nuevo. 
1. Ajustado: Se registraron las salidas y entradas por ajustes según las diferencias entre la cantidad contada y la existencia actual.
1. Todos: Permite consultar todos los conteos independientemente del status en que se encuentre.

