+++
title = "Configuración de Niveles"
description = "Configurar accesos para cada nivel a las diferentes opciones del conteo"
weight = 1
+++


<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://youtu.be/8w74TthsoCY" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="configuraciondeniveles.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


Se deberán dar los permisos necesarios a cada nivel para acceder a cada opción del proceso: 

* Ir al menú Utilerías / Catálogo de Niveles.
* Seleccionar la pestaña de "Inventario2".
* Activar el acceso a "Toma de Inventario 2.0"
* Activar los accesos permitidos para el nivel a las siguientes opciones: Eliminar conteo, Ver diferencias, Ver valuación, Actualizar ubicación y Realizar ajustes.
* Hacer clic en [Grabar]

![ConteosNiv](conteosniv.png)

