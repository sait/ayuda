﻿+++
title = "Consulta de Caducidades"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=4vu6sJ7noEY&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=8&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. H. Consulta de Caducidades.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT se cuenta con una herramienta para poder consultar nuestros artículos que utilizan lotes y caducidades de una manera rápida y sencilla.

Para realizar dicha consulta dentro de SAIT diríjase a:

<h4 class="text-primary">Inventario / Consulta de Caducidades</h4>

La ventana muestra distintos filtros:

* Por clave del artículo

* Por línea

* Por familia

* Por tipo (compra, cancelación de salida por ajuste, cancelación de traspaso, etc)

* Por folio Incluir (todos, sin caducidad, vencidos, por caducar)


![IMG](1.png)

Si deseamos consultar el detalle de los movimientos basta con dar **[Enter]** sobre un renglón y nos aparecerá la siguiente pantalla en donde podemos consultar más a detalle la información de dicho artículo.


![IMG](2.png)
