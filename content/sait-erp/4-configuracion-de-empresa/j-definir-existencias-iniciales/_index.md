﻿+++
title = "Definir Existencias Iniciales"
description = ""
weight = 11
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=XFOHbsQJxLU&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h&index=12&t=3s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. J Definir Existencias Iniciales.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Uno de los procesos más importantes antes de empezar a utilizar el sistema y si queremos llevar correctamente nuestro inventario es el ingreso de existencias iniciales, para esto se tuvo que haber realizado un conteo previo de todos los artículos con los que contamos.

Para dar entrada al inventario inicial siga las siguientes instrucciones.

<h2 class="text-primary">OPCIÓN 1</h2>

1.Entrar a SAIT e ir a Inventario / Registrar Entradas / Seleccionar tipo Inventario Inicial

* En la columna Clave: Ingresamos el código del artículo (si no conocemos el código presionamos **[F2]** para abrir la pantalla de búsqueda de artículos).

* En la columna Cant: Ingresamos la cantidad física contada.

Para procesar presionar **[F8]** o clic en **[Procesar]** 


![IMG](1.png)

<h2 class="text-primary">OPCIÓN 2</h2>

Si contamos con lectora portátil en lugar de ingresar uno a uno todos los artículos contados podemos realizar el proceso aún más sencillo y de una forma masiva, para llevar a cabo este proceso siga las siguientes instrucciones.

2.Nos dirijimos al botón indicado en la sección final de la pantalla y damos clic sobre el.

![IMG](2.png)

2.Clic en **[Siguiente]**.

![IMG](3.png)

3.Descargamos los datos.

Clic en **[Siguiente]**.

![IMG](4.png)

4.Verificamos que el formato en el que se presentan los datos sea correcto, en este caso es código y cantidad.

Clic en **[Siguiente]**.

![IMG](5.png)

5.Validamos la información.

Clic en **[Finalizar]**.

![IMG](6.png)

6.Finalizar

Clic en **[Procesar]**.


![IMG](7.png)



