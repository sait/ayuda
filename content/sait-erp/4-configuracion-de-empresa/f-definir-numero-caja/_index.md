﻿+++
title = "Definir Número de Caja para Cortes"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=UDXGWBj-6SQ&index=8&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. F Definir Número de Caja para Cortes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para poder empezar a utilizar caja, debemos definir un número de caja, para realizar esta configuración diríjase a:

<h4 class="text-primary">Utilerías / Configuración General del Sistema / Pestaña Caja-Punto de Venta</h4>

Para configurar este dato solo escribimos en el campo (Número de Caja) el número que le corresponda a dicha estación y damos clic en  **[Cerrar]**.


![IMG](1.png)

Si al momento de Abrir Corte nos arroja el siguiente mensaje, significa que falta configurar este dato.


![IMG](2.png)