﻿+++
title = "Ingresar y Verificar datos de Empresa: Razón Social, RFC y Régimen Fiscal"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=Cw8Bjt-yNt4&index=2&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. A Ingresar y Verificar datos de empresa Razon Social, RFC y RF.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Si estaremos generando facturas en el sistema es muy importante que los datos fiscales estén correctamente capturados.

Para poder ingresar estos datos en SAIT diríjase a:

<h4 class="text-primary">Utilerías / Configuración General del Sistema</h4>


No es necesario dar clic en ningún botón para guardar los cambios, con solo ingresarlos quedan guardados.


![IMG](datos.png)

