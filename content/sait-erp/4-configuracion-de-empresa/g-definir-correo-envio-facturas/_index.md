﻿+++
title = "Definir Correo para envío de facturas "
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=xfj3rsof4S0&index=9&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. G Definir Correo para envío de facturas (de preferencia en GMail).pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT permite enviar facturas (archivo pdf y xml) a nuestros clientes de una manera muy sencilla, para habilitar esta funcionalidad solo es necesario configurar un correo en los usuarios.

Para realizar esta configuración diríjase a:

<h4 class="text-primary">Utilerías / Catálogo de Usuarios / Modificar</h4>

El sistema nos mostrará la siguiente ventana, damos clic en **[Configurar Correo]**.

![IMG](1.png)


2.Llenar los datos requeridos.

{{%alert info%}} Por cuestiones de compatibilidad con el sistema se recomienda utilizar Gmail {{%/alert%}}


* Tipo de envío: Seleccionar Usar Servidor SMTP.

* Email del usuario: Ingresamos nuestro correo.
<br />
* Firma para incluir al final de mensajes: para adjuntar una firma en los correos enviados, este dato es opcional.
<br />
* Servidor: con Gmail el servidor de salida es: smtp.gmail.com
<br />
* Usuario: Ingresamos el correo nuevamente.
<br />
* Contraseña: Ingresamos la contraseña de dicho correo.
<br />
* Puerto: 465
<br />
* Tiempo de Espera: 20
<br />
* Palomear ambas opciones (Usar Autenticación y Usar conexiones seguras SSL)
<br />
* Clic en **[Continuar]**.


![IMG](2.png)
