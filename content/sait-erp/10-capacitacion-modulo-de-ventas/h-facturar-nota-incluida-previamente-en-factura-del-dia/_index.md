+++
title = "Facturar una nota de venta incluida previamente en la Factura del Día"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

En ocasiones cuando realizamos nuestra factura del día, puede suceder que un cliente venta el día después a solicitar la facturación de su nota de venta, sin embargo si ya elaboramos la factura global esa nota ya se facturó en la global diaria, si llega a tener esta situación en su empresa hay dos alternativas que puede realizar para solucionar esta situacion.

<h2 class="text-primary">Opción 1: Cancelar Global y Facturar Nota</h2>

Esta opción es la más sencilla ya que se realizan menos pasos, esta opción se recomienda utilizarse mientras estemos dentro del mismo mes ya que se cancelará el CFDI de la factura global.

Supongamos tenemos la factura global del día 20 de junio en donde se incluyeron las siguientes notas de venta y el cliente viene al día siguiente a solicitar la factura de la nota con folio AA521 

![IMG](cancelar-global.png)

Para liberar la nota AA521 lo que haremos es cancelar la factura global con el motivo 04 - Operación nominativa relacionada en una factura global.

![IMG](cancelar-global-motivo.png)

Una vez cancelada, nos dirijiremos a Caja / Facturar Nota de Venta y la facturamos directo al cliente que desea su CFDI

![IMG](cancelar-global-emitir.png)

Ahora solo faltaría emitir de nuevo la factura global del día 20 de junio, nos dirijmos a Caja / Elaborar Factura del día, una vez timbrada, observaremos el total es menos ya que esta vez la global ya no consideró la nota AA521 de $200 de importe ya que se había facturado a otro cliente de manera directa

![IMG](cancelar-global-final.png)

<h2 class="text-primary">Opción 2: Devolver nota, hacer nota de crédito y hacer factura</h2>

Pasos a realizar:

- [Realizar Devolución de Nota](#realizar-devolución-de-nota)   
- [Realizar Nota de Crédito](#realizar-nota-de-crédito)  
- [Realizar Factura](#realizar-factura)

### Realizar Devolución de Nota

- Ir a Ventas / Devoluciones y Notas de Crédito

- Se nos presentara la ventana 'Devolución de Venta'

- En 'Devolver' seleccionar 'Nota'

- En 'Folio' capturar el folio de la nota de venta

- El sistema nos alertara que la nota de venta ya esta facturada, damos clic en [Sí]

- En el desglose de los artículos, en la columna 'Devolver' ingresar la cantidad que aparece en la columna de 'Vendidos'

![IMG](FactNota1.PNG)

- Clic en [Procesar]

- El sistema nuevamente nos alertara que la nota de venta ya esta facturada, aceptamos dichos avisos

### Realizar Nota de Crédito

Se requiere elaborar una nota de crédito para cancelar el ingreso ya reconocido en la factura global del día. 

Para realizar la Nota de Crédito deberá de incluir como CFDI relacionado la factura del día, siga las siguientes instrucciones: 

Para realizar este proceso primeramente deberá dar de alta un artículo que utilizaremos para la nota de crédito.

1.Ir al Inventario / Catálogo de Artículos y Servicios

2.Registramos un articulo por ejemplo:

Clave ‘DEVOLUCION’, Descripción ‘DEVOLUCION DE MERCANCIA’

El artículo debe tener la clave de unidad ‘ACT’ de actividad, si no existe registrarla en la ventana catalogo de unidades.

El artículo debe contar con la clave ‘84111506’ que corresponde a servicios de facturación.

El precio queda en cero, y el impuesto en %16.

![IMG](devolver-global-articulo.png)


3.Ir a Ventas / Devoluciones y Notas de Crédito

4.Se le presentara la ventana ‘Devolución de Venta’

5.Hacer clic en el boton ‘Capturar Artículos’, se habilitaran los campos de captura de articulos.

6.Capturar los datos de la devolución como habitualmente se realiza, cliente, Comentarios, etc.

7.En los artículos capturará el concepto que recién dimos de alta. Una vez elegido el articulo, en cantidad colocamos 1, y en P.Unitario colocaremos el monto a devolver.

8.Clic en [Procesar] o bien, presionar F8

Se abrira el nuevo formulario ‘Información del CFDI’

* Capturar la Forma de Pago en que se pagara la nota de crédito.

* Si no se conoce la forma de pago, podremos seleccionar ‘99 - Por definir’

* En el campo ‘Método de Pago’ le sugerira ‘PUE – Pago en una sola exhibición’ (Sin embargo, quedara habilitado para la selección que usted desee)

* En el campo ‘Uso del CFDI’ aparecera fijo ‘G02 - Devoluciones, descuentos o bonificaciones’ (Debido a que es el correspondiente para Notas de Crédito)

* En el campo ‘Tipo de Relación’ seleccionar el tipó de relación que tendra esta Nota de Crédito en referencia a las facturas que acontinuación se relacionaran.

* Clic en el signo de ? para agregar un CFDI Relacionado

![IMG](devolver-global-informacioncfdi.png)

Se le presentara el formulario ‘Búsqueda de CFDI Relacionados’

Ingresar los filtros de busqueda para mostrar las FACTURAS DEL CLIENTE SIN CANCELAR

Clic en [Buscar]

Marcar la casilla de las facturas que deseemos relacionar

Clic en [Relacionar]

![IMG](devolver-global-seleccionar.png)

Confirmar que los datos de la devolución estan correctos y listo, damos clic en [F8 Continuar]

![IMG](fin.png)

9.Listo la nota de crédito se ha generado de manera correcta.


### Realizar Factura a cliente

- Ir a Ventas / Registrar Ventas F4

- Se nos presentara la ventana 'Documentos de Venta'

- En 'Tipo' seleccionar 'Factura'

- En Forma de Pago y Divisa, capturar según lo acorde a la factura que se entregara al cliente

- En 'Cliente' capturar el Receptor de la factura

- En 'Comentarios' podemos ingresar una observación referente a la nota de venta que se esta facturando, ejemplo 'Notas Facturadas: BB1,BB2...'

- Clic en 'Copiar'

- Se nos presentara la ventana 'Copiar Documento'

- En 'Tipo' seleccionar 'Nota'

- En 'Folio' ingresar el folio de la nota de venta que deseamos facturar

![IMG](FactNota2.PNG)

- Clic en 'Cargar Artículos'

- Verificamos que los artículos e importes se hayan cargado correctamente

- Clic en [Procesar]

- Capturar los datos adicionales referentes a la Información del CFDI

- Clic en [F8 Continuar]

![IMG](FactNota3.PNG)

- Confirmar que los datos de la factura estén correctos y listo, puede entregar a su cliente el CFDI recién emitido