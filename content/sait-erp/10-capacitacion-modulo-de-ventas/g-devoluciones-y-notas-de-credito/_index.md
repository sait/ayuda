﻿+++
title = "Devoluciones y Notas de Crédito"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=okqrB4CVWYs&index=8&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. G Devoluciones y Notas de Credito.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


SAIT te permite generar Devoluciones de Ventas y Notas de Crédito, según como la empresa lo requiera. Esto le da oportunidad al usuario de tener un registro de las Devoluciones y Notas de Crédito que se generen en el sistema.

Este proceso, de manera automática te realizan los ajustes necesarios al inventario. Todo esto de una manera muy práctica y sencilla para el usuario.

Para acceder a esta opción deberá ingresar al menú de Ventas / Devoluciones y Notas de Crédito.

## Nota de Credito devolviendo mercancia

**Esta opción se utiliza cuando el cliente si regresó mercancía y a su vez, se hará una nota de crédito a la factura original.**

1.Ir a Ventas / Devoluciones y Notas de Crédito

2.Se le presentara la ventana 'Devolución de Venta'

3.En el campo de 'Folio' ingresar el folio del documento a generar la Nota de Crédito

4.Capture las observaciones y en la columna de devolver capture las piezas que devolverá si será una devolución parcial, en caso de devolver todos los artículos, deberá seleccionar 'Devolver Todos'

La forma **Efectivo** significa que se le regresó dinero al cliente, si selecciona **Crédito** significa que no se le devolvió dinero al cliente sino que se generá un saldo a favor en el estado de cuenta del cliente.

  ![IMG](1.png)

5.Clic en **[Procesar]** o bien, presionar F8

6.Se abrira el nuevo formulario 'Información del CFDI', ahí capturar la Forma de Pago en que se pagara la nota de crédito. 

  - Si no se conoce la forma de pago, podremos seleccionar '99 - Por definir'
  - En el campo 'Método de Pago' le sugerira 'PUE – Pago en una sola exhibición'.
      - Sin embargo, quedara habilitado para la selección que usted desee
  - En el campo 'Uso del CFDI' aparecera fijo 'G02 - Devoluciones, descuentos o bonificaciones'
      - Debido a que es el correspondiente para Notas de Crédito
  - En el campo 'Tipo de Relación' seleccionar el tipó de relación que tendrá esta Nota de Crédito en referencia a la factura que a continuación se relacionará.

  - Confirmar que los datos de la devolución estan correctos y listo, damos clic en **[F8 Continuar]**

7.Listo, la nota de crédito se ha realizado de manera correcta.

## Nota de Credito por descuento o por devolucion manual:

**Esta opción es de utilidad cuando se requiere realizar una nota de crédito a una factura ya sea por, pronto pago, por descuento, por diferencia de precio, o incluso para realizar una nota de crédito a alguna factura del día o global en caso de que un cliente nos devuelva una nota de venta que ya incluimos en la factura global.**

Para realizar este proceso primeramente deberá dar de alta un artículo que utilizaremos para la nota de crédito.

1.Ir al Inventario / Catálogo de Artículos y Servicios 

2.Registramos un articulo, usaremos el siguiente para el ejemplo: 

- Clave 'DEVOLUCION', Descripción 'DEVOLUCION'

- El artículo debe tener la clave de unidad 'ACT' de actividad, si no existe registrarla en la ventana catalogo de unidades.

- El artículo debe contar con la clave '84111506' que corresponde a servicios de facturación.

- El precio queda en cero y el impuesto en %16.

![IMG](devo.png)

3.Ir a Ventas / Devoluciones y Notas de Crédito

4.Se le presentara la ventana 'Devolución de Venta'

5.Hacer clic en el boton 'Capturar Artículos', se habilitaran los campos de captura de articulos.

6.Capturar los datos de la devolución como habitualmente se realiza, cliente, Comentarios, etc.

7.En los artículos capturará el concepto que recién dimos de alta. Una vez elegido el articulo, en cantidad colocamos 1, y en P.Unitario colocaremos el monto a devolver.

8.Clic en **[Procesar]** o bien, presionar F8

![IMG](MANUAL.png)

Se abrirá el nuevo formulario 'Información del CFDI'

Capturar la Forma de Pago en que se pagara la nota de crédito. 

  - Si no se conoce la forma de pago, podremos seleccionar '99 - Por definir'
  - En el campo 'Método de Pago' le sugerira 'PUE – Pago en una sola exhibición'.
      - Sin embargo, quedara habilitado para la selección que usted desee
  - En el campo 'Uso del CFDI' aparecera fijo 'G02 - Devoluciones, descuentos o bonificaciones'
      - Debido a que es el correspondiente para Notas de Crédito
  - En el campo 'Tipo de Relación' seleccionar el tipó de relación que tendra esta Nota de Crédito en referencia a las facturas que acontinuación se relacionaran.

![IMG](RELACIONAR.png)

  - Clic en el signo de **?** para agregar un CFDI Relacionado


      - Se le presentara el formulario 'Búsqueda de CFDI Relacionados'
      - Ingresar los filtros de busqueda para mostrar las FACTURAS DEL CLIENTE SIN CANCELAR (si conoce el folio interno o el uuid, puede ingresarlo en el campo Folio o UUID)
      - Clic en **[Buscar]**
      - Marcar la casilla de las facturas que deseemos relacionar
      - Clic en **[Relacionar]**

  ![IMG](RELACIONAR2.png)

- Confirmar que los datos de la devolución estan correctos y listo, damos clic en **[F8 Continuar]**

  ![IMG](RELACIONAR3.png)

9.Listo la nota de crédito se ha generado de manera correcta.

## Nota de Crédito sin relacionar UUID

Esta opción funciona de la misma manera que cuando hacemos una devolución manual, por pronto pago, descuento, etc, pero sin la necesidad de relacionar un UUID en específico. 

Para realizar este proceso deberá seguir las siguientes instrucciones:

1.Primeramente deberá tener creado el artículo como se especifíca en la parte superior:

2.Ir a Ventas / Devoluciones y Notas de Crédito

3.Se le presentara la ventana 'Devolución de Venta'

4.Hacer clic en el boton 'Capturar Artículos', se habilitaran los campos de captura de articulos.

5.Capturar los datos de la devolución como habitualmente se realiza, cliente, comentarios, etc.

6.En los artículos capturará el concepto que recién dimos de alta. Una vez elegido el articulo, en cantidad colocamos 1 (siempre), y en P.Unitario colocaremos el monto a devolver.

7.Clic en **[Procesar]** o bien, presionar F8

8.Aparecerá la ventana de la Información del CFI 

![IMG](NC.PNG)

9.Deberá colocar el cursor sobre el campo de Tipo de Relación como aparece en la siguiente imagen:

![IMG](NC2.png)

10.En su teclado presione la tecla **[SUPR]** la ventana deberá aparecer de esta manera.

![IMG](NC3.PNG)

11.Confirmamos el proceso dando clic en Sí y listo de esta manera generamos una nota de crédito sin relacionar un UUID.

![IMG](NC4.PNG)
