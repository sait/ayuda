﻿+++
title = "Sucursales de los Clientes"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=c3JPqXeX-rg&t=0s&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&index=3" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. C Sucursales de los clientes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite llevar el registro de las sucursales de tus clientes de una manera muy sencilla.

La información que se permite capturar en este catálogo, hace referencia al domicilio de la sucursal enlazado al cliente principal que previamente se registró en el catálogo de clientes.

Esta opción, tiene la ventaja de contar con el registro de las sucursales con las que cuenta el cliente y al momento de procesarle una venta te habilite la opción de elegir a que sucursal pertenece esa venta.

**Dicha opción es de mucha utilidad para los clientes que tienen varias sucursales en distintas ubicaciones**, la cuales todas pertenecen a una misma razón social, de esta manera podemos dar de alta las diferentes sucursales del cliente con sus mismos datos fiscales sin necesidad de duplicar el cliente con otra clave, pero mismo RFC y razón social.

<h2 class="text-primary">A.	Agregar una Sucursal de un Cliente</h2>

Ir al menú de Ventas / Sucursales de los Clientes.

1.Seleccionar la opción de **[Agregar]**.

2.Agregar la clave o identificación de la sucursal.

3.Elegir el cliente al que pertenece la sucursal ingresando la clave o haciendo la consulta presionando la tecla **[F2]**.

4.Ingresar los datos generales de la sucursal.

5.Para finalizar, presionar la opción de **[Agregar]**.


![IMG](1.PNG)

<h2 class="text-primary">B.	Modificar una Sucursal de un Cliente</h2>


Ingrese al menú de Ventas / Sucursales de lo Clientes, seleccionar el registro que desea modificar.

1.Una vez seleccionado dar clic en la opción **[Modificar]**.

2.Realizar las modificaciones necesarias en el formulario y dar clic en **[Modificar]**.

![IMG](2.png)

<h2 class="text-primary">C.	Eliminar una Sucursal de un Clientee</h2>


Ingrese al menú de Ventas / Sucursales de los clientes, seleccionar el registro que desea eliminar.

1.Una vez seleccionado dar clic en la opción **[Eliminar]**.

2.De manera informativa se muestra la información del cliente.

3.Si el cliente que desea eliminar es el correcto dar clic en **[Eliminar]**.

![IMG](3.png)

<h2 class="text-primary">D.	Realizar Búsquedas de Sucursales de Clientes</h2>

Si desea buscar a un cliente en particular, dar clic en **[Buscar]** o presionar la tecla **[F2]**.

![IMG](4.png)
 
1.Se mostrará la siguiente pantalla en donde deberá ingresar el nombre de la sucursal.
 
![IMG](5.png)

2.Dar doble clic o presionar la tecla **[Enter]** sobre el registro deseado para mostrar la información.

<h2 class="text-primary">E.	Exportar el Catalogo de Sucursales de los Clientes a Excels</h2>

Este proceso es muy sencillo lo único que se tiene que hacer es:

1.En el menú de Ventas / Sucursales de los Clientes, seleccionar la opción de**[Excel]** y listo.

![IMG](6.png)