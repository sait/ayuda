﻿+++
title = "Tipo de Cambio"
description = ""
weight = 10
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=8wxdfojiALY&index=9&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. K Tipo de Cambio.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT les permite definir el tipo de cambio que desean manejar al momento de realizar sus documentos de venta.

Para configurar o modificar el tipo de cambio, hay que seguir las siguientes indicaciones:

1.	Ir al menú de Ventas / Tipo de Cambio

![IMG](1.png)


3.	Modificar el valor del tipo de cambio a utilizar en los diferentes escenarios:

4.	Facturas, Cotizaciones y notas de Contado

5.	Facturas y Cotizaciones de Crédito

6.	Al recibir dólares en punto de venta y caja.

7.	Para guardar los cambios dar click en **[Aceptar]**.

8.	Listo

En Utilerías / Grupos de Usuarios y Catálogos de Niveles, se agregó el siguiente permiso el cual le permite dar permisos por cada una de las opciones

![IMG](2.png)

<h3 class="text-danger">NOTA</h3>

#### Al realizar una factura en dólares el Tipo de Cambio que aparecerá en el XML será el capturado en el Tipo de Cambio Oficial el cuál se encuentra en el módulo de Utilerías.



