﻿+++
title = "Registrar Ventas"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=-6kWziWsmb8&index=7&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. F Registrar Ventas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

<h2 class="text-primary">Registrar Ventas</h2>


SAIT te permite realizar varios tipos de documentos de venta como lo son: Facturas, Remisiones, Notas de Venta, Pedidos y Cotizaciones. Además, facilita la operación de tu negocio al permitirte enlazar diferentes documentos de venta para mejorar la atención al cliente.

Para acceder a esta opción deberá ingresar al menú de Ventas / Registro de Ventas, o bien, presionando la tecla **[F4]**, la cual funciona como acceso directo a esta opción.


Para registrar una venta se deberá ingresar la información necesaria:

1.	Definir el vendedor que realiza la venta.

2.	Seleccionar el tipo de documento a procesar: 

* Nota de Venta 
* Factura
* Remisión
* Pedido
* Cotización

3.	Definir la forma de pago del documento: contado o crédito, modificando este valor con la tecla de barra espaciadora.

4.	Definir la divisa del documento: pesos o dólares.

5.	Ingresar la clave del cliente al que se va a realizar el documento de venta o presionar tecla **[F2]** para realizar una búsqueda del cliente

6.	Agregar los artículos que se van a incluir en la venta, si no conocen su clave presionar tecla **[F2]** para realizar la búsqueda de los artículos.

7.	Una vez agregado todos los artículos presionar el botón **[Procesar]** o la tecla **[F8]**.


![IMG](1.png)

<h2 class="text-primary">Enlazar Documentos</h2>


Esta opción les permite agilizar el proceso de venta, enlazando documentos de venta para dar salida al inventario o bien, facturando algún documento.

Los documentos que se pueden enlazar son los siguientes:


* **Facturas** puede enlazar: remisiones, pedidos, cotizaciones.
* **Notas de venta** puede enlazar: remisiones, pedidos, cotizaciones.
* **Remisiones** puede enlazar: pedidos y cotizaciones.
* **Pedidos** puede enlazar: cotizaciones
* **Cotizaciones** puede enlazar: ninguno.
* **Prefacturas** puede enlazar: remisiones, pedidos, cotizaciones.



Para enlazar un documento a otro, deberá realizar lo siguiente:

1.	Ir al menú de ventas / Registro de Ventas.

2.	Seleccionar el tipo de documento a realizar.

3.	Es muy importante ingresar el cliente al que se va a realizar el documento (Debe ser el mismo cliente que el documento a enlazar).

4.	Seleccionar la forma de pago del documento: Crédito o Contado.

5.	Seleccionar la divisa del documento: Pesos o Dólares.

6.	Seleccionar el Botón **[Enlazar]** que se encuentra en la parte inferior de la ventana.

7.	Definir la forma en que se enlazará el documento:

1.	Todos los artículos de un documento.

2.	Un artículo de un documento.

3.	Un artículo de varios documentos.

4.	Los artículos pendientes de un período.

8.	Seleccionar el tipo de documento a enlazar.

9.	Agregar el folio del documento o presionar el botón de **[?]** para hacer la búsqueda del documento.

10.	Seleccionar la opción de **[Enlazar]**.


![IMG](2.png)

11.	Listo. Tu documento está enlazado.

12.	Para finalizar dar clic en **[Procesar]** para terminar el documento.

<h2 class="text-primary">Copiar un Documento de Venta ya Realizado
</h2>

Esta opción se refiere a realizar una copia de un documento de venta ya procesado anteriormente.

Si deseas realizar una venta con esa misma información únicamente tienes que realizar lo siguiente:

1. Ir al menú de Ventas / Registro de Ventas.

2. Presionar el botón **[Copiar]** que se encuentra en la parte inferior de la ventana:


![IMG](5.png)

3. Seleccionar el tipo de documento a copiar.

4. Ingresar el folio del documento o presionar la tecla **[?]** para buscarlo.

5. Presionar el botón de **[Cargar Artículos]**.

![IMG](3.png)

6. Listo. Los artículos han sido copiados.

<h4 class="text-danger">Importante: al hacer COPIAR un documento se le da nuevamente salida a la mercancía del inventario, a diferencia de cuando enlazamos un documento en donde solo le cambiamos el status al documento original es decir se cambia a facturado, facturado parcial etc. </h4> 

<h2 class="text-primary">Tipos de Documentos y su afectación al Inventario</h2>

* **Facturas**: Si descuenta del inventario al momento de procesar.

* **Notas de venta**: Si descuenta del inventario al momento de procesar.

* **Remisiones**: Si descuenta del inventario al momento de procesar.

* **Pedidos puede**: No descuenta del inventario al momento de procesar el documento sino hasta que se entrega el pedido. 

* **Cotizaciones**: No descuenta del inventario al momento de procesar.

* **Prefacturas**: No descuenta del inventario al momento de procesar el documento.

El funcionamiento de una prefactura es poder emitir una factura normal y con validez fiscal, la única diferencia es que solamente se puede utilizar esta opción siempre y cuando todos los productos agregados en la prefactura NO cuenten con existencia.

Y después cuando se tenga la mercancía y se desee descontar del inventario, se consulta la prefactura de manera individual y das clic en el botón de FACTURAR para dar salida al inventario solamente.

Si lo que quieren es un documento previo para que autorice el cliente tendría que ser otro tipo de documento una cotización por ejemplo y no la prefactura.

1) Como podemos observar este artículo tiene existencia cero

![IMG](prefactura1.jpg)

2) Nos dirigiremos a Ventas / Registrar Ventas / Seleccionaremos el tipo de documento: prefactura

Agregaremos la clave SAE 40 y daremos clic en **[F8=Procesar]**.

![IMG](prefactura2.jpg)

3) Después que hayamos ingresamo la compra, aceptado el traspaso, hecho el ajuste o cualquier movimiento al inventario que signifique una entrada, ya podremos darle salida a la mercancía, nos dirigiremos a Ventas / Consulta Individual / Ingresamos el folio y damos clic en **[Consultar]**.

![IMG](prefactura3.jpg)

4) Indicar la fecha en la que deseamos afecta la salida al kárdex

![IMG](prefactura4.jpg)

5) Listo

![IMG](prefactura5.jpg)
