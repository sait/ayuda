﻿
+++
title = "Año 2018"
weight = 14
+++

Esta es la bitácora de las mejoras y correcciones realizadas a SAIT ERP, Básico y Contabilidad durante 2018

Versión | Fecha | Descripción
---|---|---
2018.54.0 |10.Dic | Mejora. Poder consultar por rango de folios desde 'Consultar CFDIs de Pagos' (#401)
2018.54.0 |10.Dic | Mejora. Poder reenviar por enlace de sucursales los CFDIs de Pagos desde 'Consultar CFDIs de Pagos' (#377)
2018.54.0 |10.Dic | Mejora. En 'Auxiliar de Pedido a Proveedor' poder calcular cantidad a comprar en base al máximo o mínimo del artículo (#369)
2018.54.0 |10.Dic | Mejora. Emitir CFDI de Traslados sobre Salidas a Sucursal (#280)
2018.54.0 |10.Dic | Corrección. Al registrar Compra enlazada a Orden de Compra validar que no quede nada pendiente (#372)
2018.54.0 |10.Dic | Corrección. Al Cargar CFDI desde 'Registro de Compras' tomar T.C. correctamente
2018.53.0 |26.Nov | Mejora. Se permite emitir un REP a un tercero, se puede elegir el receptor del CFDI y seleccionar los pagos que se van a incluir.
2018.53.0 |26.Nov | Mejora. Si no se tienen decimales configuradoras para entradas y salidas de inventario, tomar los de compras (#415)
2018.53.0 |26.Nov | Corrección. Marcaba error al seleccionar día en calendario en 'Emitir CFDI de Pago' (#408)
2018.53.0 |26.Nov | Corrección. Aumentar los decimales en T.C. inverso.
2018.53.0 |26.Nov | Mejora. Al relacionar el UUID de la factura en el REP, se carga tal y como viene en el XML de la factura.
2018.53.0 |26.Nov | Mejora. Grabar los UUID de facturas, notas de credito y REP en mayúsculas.
2018.52.0 |14.Nov | Corrección. Al emitir REP en dólares que pagan facturas en dólares, tomar tipo de cambio oficial del día anterior al pago
2018.52.0 |14.Nov | Corrección. Se corrige detalles al modificar detalle de REP y enviar por enlace
2018.51.0 |09.Nov | Mejora. Poder emitir REP desde cualquier sucursal sin estar recibiendo facturas (#403). Se debe agregar la siguiente linea al config.msl: eypEnviarCfdis=SI
2018.51.0 |09.Nov | Mejora. Al agregar factura manualmente como cargo desde el menú de cobranza, se enlaza con el CFDI de factura (#402)
2018.51.0 |09.Nov | Corrección. Al cancelar un REP, si el RFC del emisor tiene & marcaba error (#397)
2018.51.0 |09.Nov | Corrección. Al emitir REP de distintos clientes se enviaba el correo de todos los REP al primer cliente.
2018.51.0 |09.Nov | Mejora. Se incluyen los archivos XSLT en el directorio libxslt de los siguientes complementos: Comercio Exterior, Divisas, Notarios Públicos, Serv. Parc. de Const. e INE (#386)
2018.50.0 |03.Nov | Mejora. Nuevo proceso de cancelación de CFDI
2018.50.0 |03.Nov | Mejora. Nuevo proceso para aplicar pagos con notas de crédito de proveedores en Cuentas por Pagar (#323)
2018.50.0 |03.Nov | Mejora. Al enviar REP por correo si tiene correo de contacto a cobranza solo envia a ese contacto.
2018.50.0 |03.Nov | Correción. Al enviar REP por correo cerraba tablas.
2018.50.0 |03.Nov | Mejora. Al seleccionar capa de salida, ordenar por fecha de caducidad (#388)
2018.50.0 |03.Nov | Mejora. Al emitir CFDI de Pago, poder anexar recibos SPID
2018.50.0 |03.Nov | Correcion. Al modificar una cotización, si artículo tenia unidades de medida, marcaba error con Temp.PREUNI
2018.36.0 |13.Oct | Corrección. Se realizaron ajustes en la generación de CFDIs de Pago al relacionar uno cancelado
2018.36.0 |13.Oct | Mejora. Se actualizo catálogo de Conceptos de Cuentas por Pagar
2018.36.0 |13.Oct | Corrección. Se realizaron ajustes en cálculo de Tipo de Cambio en generación de CFDIs de Pagos con facturas en dólares y pagos en pesos
2018.36.0 |13.Oct | Mejora. Envio automatico de correos al emitir REP
2018.36.0 |13.Oct | Mejora. Se envia evento al definir unidad por defecto en unidades adicionales
2018.36.0 |13.Oct | Mejora. Se envia evento al actualizar vencimientos
2018.36.0 |13.Oct | Corrección. Importador, corregido error al obtener metodo de pago de cfdi importado.
2018.36.0 |13.Oct | Corrección. Importador, se corrige la necesidad de tener campos opcionales para 3.2 al importar CFDIs, se agrego boton para crear los campos si se quiere importar CFDIs 3.2.
2018.35.0 |06.Oct | Mejora. Actualización en uso de Tipos de Cambio en generación de CFDIs de Pagos
2018.35.0 |06.Oct | Corrección. Al generar CFDIs de Pagos, no se consideraban las Notas de Crédito aplicadas anteriormente
2018.34.0 |02.Oct | Mejora. Se permite configurar los decimales a utilizar en cantidad para entradas y salidas de inventario (#304)
2018.34.0 |02.Oct | Mejora. Al emitir Nota de Crédito, solicitar Tipo de Cambio del día anterior
2018.34.0 |02.Oct | Mejora. Catalogo de cuentas bancarias en SAIT BASICO (en menu de cobranza)  (#351)
2018.34.0 |02.Oct | Corrección. En Consulta de CFDIs de Pagos, no mostrar Divisa si fue un CFDI que se realizó por varios pagos
2018.34.0 |02.Oct | Corrección. Cuando se tenían unidades adicionales en Arts. no se respetaba el precio asignado al cliente (#322)
2018.34.0 |02.Oct | Corrección. Al generar CFDI de Pagos por abono que se realizo a facturas de distintas divisas marcaba Property TIPOCAMBIODR is not found (#366)
2018.34.0 |02.Oct | Corrección. Al convertir Prefactura a Factura se da salida de inventario a componentes de Kits (#226)
2018.34.0 |02.Oct | Mejora. En conteo individual del artículo, mostrar el último artículo contado: clave/descripción/cantidad (#353)
2018.34.0 |02.Oct | Corrección. Al importar cfdis registraba mal los caracteres especiales, obligaba tener metodopago (#353,#364)
2018.34.0 |02.Oct | Corrección. Facturar abono de apartado ya carga correctamente la forma de pago seleccionada (#343)
2018.33.0 |17.Sep | Mejora. No anexar información aduanera (números de pedimento) al realizar facturas con Complemento de Comercio Exterior (#346)
2018.33.0 |17.Sep | Corrección. Marcaba error al realizar un documento de venta desde 'Registrar Ventas F4' con un artículo con IVA de 15% (#354)
2018.33.0 |17.Sep | Corrección. Al realizar un cargo por factura a crédito con impuestos locales, en el estado de cuenta del cliente no aparecía correctamente el saldo (#350)
2018.33.0 |17.Sep | Corrección. Al enviar CFDIs de Pagos por enlace no se enviaba la divisa (#347)
2018.33.0 |17.Sep | Corrección. En nota de crédito del anticipo, relacionar factura de la venta del bien. Anteriormente se relacionaba la factura del anticipo (#340)
2018.33.0 |17.Sep | Corrección. Al definir promociones, si el articulo tiene IEPS calcula mal el precio de la promocion (#298)
2018.32.0 |06.Sep | Mejora. En conteos de inventario abiertos, se permite enviar las diferencias a excel (#331)
2018.32.0 |06.Sep | Mejora. Se amplió el margen para definir consultas más grandes con SQL al definir reportes (#337)
2018.32.0 |06.Sep | Mejora. Detectar inconsistencias en contabilidad: se agregó verificación para que exista el encabezado de pólizas (#338)
2018.32.0 |06.Sep | Corrección. Al facturar desde caja, en ocasiones fallaba al validar la unidadsat de los artículos (#321)
2018.32.0 |06.Sep | Mejora. En grupos de usuarios, se agregó el permiso 'Permitir modificar datos de CFDI' (#315)
2018.32.0 |06.Sep | Mejora. Al registrar pagos de los clientes, sugerir tipo de cambio del día anterior (#334)
2018.32.0 |06.Sep | Mejora. Consultar CFDIs de Pagos por número de sucursal (#319)
2018.32.0 |06.Sep | Mejora. Eliminar archivos de C:\SAIT\TEMP con fecha mayor a 10 días (#336)
2018.32.0 |06.Sep | Mejora. Al realizar nota de crédito por aplicación de anticipo tomar tipo de cambio del día anterior
2018.32.0 |06.Sep | Mejora. Al emitir CFDI de Pagos por Cliente, validar que los pagos correspondan al mismo mes
2018.32.0 |06.Sep | Mejora. Al calcular impuestos en función CargarDocumento()
2018.32.0 |06.Sep | Corrección. Al entregar corte de caja, al abrir desglose de monedas marcaba error si no te tenia la divisa capturada (#339)
2018.32.0 |06.Sep | Corrección. No se liberaran notas de ventas al cancelar factura del día no timbrada (#317)
2018.31.0 |24.Ago | Mejora. Toma de Inventario 2.0 Se agrego un nuevo tipo de conteo: Algunos Articulos los cuales permite capturar las claves de los artículos que se van a contar y permite descargarlos tambien desde la lectora.
2018.31.0 |24.Ago | Mejora. Función CargarDocumento() ahora toma Tipo de Cambio del día anterior (#329)
2018.30.0 |13.Ago | Mejora. En registro de compras se aplica configuración 'Validar que compras en dolares usen TC Oficial'
2018.28.0 |07.Ago | Mejora. Se optimizo proceso de cerrado de conteo en toma de Inventario a Puertas Abiertas
2018.27.0 |01.Ago | Corrección. Se toma Tipo de Cambio del día anterior en 'Pagos Parciales a Proveedores' (#314)
2018.26.0 |31.Jul | Corrección. En salidas al inventario, se borraba la capa de lote seleccionada al presionar Enter (#313)
2018.26.0 |31.Jul | Corrección. En salidas al inventario, al cambiar la sucursal origen volver a cargar el siguiente folio del movimiento que corresponde a la sucursal (#305)
2018.26.0 |31.Jul | Mejora. En conteos de inventario, al obtener las entradas y salidas se consideran únicamente movimientos del almacén activo.
2018.26.0 |31.Jul | Mejora. En conteos de inventario, ocultar la sucursal si la empresa no maneja almacenes.
2018.26.0 |31.Jul | Mejora. En conteos de inventario, se permite buscar artículos por múltiples códigos de barra al capturar el conteo múltiple.
2018.26.0 |31.Jul | Corrección. Al abonar pedidos, fallaba al validar articulos inactivos en las observaciones del abono.
2018.25.0 |20.Jul | Corrección. Al terminar de actualizar base de datos, generar índices.
2018.25.0 |20.Jul | Mejora. Al consultar compras, se hicieron cambios al cargar los datos de columna adicional.
2018.24.0 |17.Jul | Mejora. Se agregó nuevo proceso para [Toma de Inventario a Puertas Abiertas](/otros-temas/inventario-puertas-abiertas)
2018.24.0 |17.Jul | Mejora. En registro de compras, al cambiar divisa se recalculan los importes y totales de acuerdo a la divisa seleccionada.
2018.24.0 |17.Jul | Mejora. Al elaborar Cheque para Pago a Proveedor de un CFDI en dólares, tomar Tipo de Cambio de día anterior.
2018.24.0 |17.Jul | Mejora. En reporte de Balanza de Comprobación, se incluyen totales separados por su naturaleza Deudora o Acreedora.
2018.24.0 |17.Jul | Corrección. Al registrar compra marcaba 'Ya se cargo este CFDI para un gasto' (#307)
2018.24.0 |17.Jul | Corrección. Al abonar/entregar pedido en caja, no cargaba el tipo de documento seleccionado para procesar el abono/entrega (Factura Contado, Factura Crédito o Nota) (#308)
2018.24.0 |17.Jul | Mejora. Se permite editar reportes contables internos (#306)
2018.24.0 |17.Jul | Mejora. Nuevos íconos.
2018.24.0 |17.Jul | Corrección. Al mostrar columna con datos adicionales de compras, fallaba al cargar datos de una función o prg.
2018.23.0 |10.Jul | Mejora. Si la empresa no maneja enlace de sucursales, se permite definir máximos y mínimos de otro almacén (#301)
2018.23.0 |10.Jul | Mejora. Obligar a capturar el folio del concepto de pago en cobranza SOLO si tiene activada la opción para obligar a capturar folio (#299)
2018.22.0 |05.Jul | Mejora. Al enviar contabilidad al SAT, al generar la Balanza de Cierre se valida que solo sea de diciembre
2018.22.0 |05.Jul | Mejora. Al enviar contabilidad al SAT, al generar la Balanza de Cierre se tiene la opción de incluir póliza de cierre (#285)
2018.22.0 |05.Jul | Mejora. Al consultar reporte de Balanza de Comprobación se tiene la opción de incluir póliza de cierre
2018.22.0 |05.Jul | Mejora. Se integran reportes contables a ejecutable de SAIT ERP y SAIT Contabilidad
2018.22.0 |05.Jul | Corrección. Al importar cuentas contables desde Excel se importa correctamente la naturaleza
2018.22.0 |05.Jul | Corrección. En algunos casos al consultar pagos por pendientes de timbrar eran tomados como cancelados(#300)
2018.21.0 |26.Jun | Mejora. Actualización de uso correcto de Tipo de Cambio oficial en módulos Compras, Gastos y Cuentas por Pagar. Ahora toma el del día anterior según la fecha del documento.
2018.21.0 |26.Jun | Mejora. Actualización de uso de Tipo de Cambio para los días sábado y domingo
2018.21.0 |26.Jun | Mejora. Al registrar compra a partir de CFDI, usar divisa de XML
2018.21.0 |26.Jun | Mejora. Al registrar compra a partir de CFDI, validar que no se haya registrado anteriormente como un gasto (#276)
2018.21.0 |26.Jun | Mejora. Se incluye buscador de CFDIs para relacionar UUIDs a facturas o notas de crédito
2018.20.0 |05.Jun | Mejora. Se envía por el enlace la información de las cuentas de banco del catálogo de proveedores (#284)
2018.20.0 |05.Jun | Corrección. Al otorgar 100% de descuento, no permitía timbrar por que la base para el IVA queda en cero.
2018.20.0 |05.Jun | Mejora. Al aplicar un anticipo a una factura, ahora se envía nota de crédito por correo automáticamente
2018.20.0 |05.Jun | Mejora. Se aplica permiso para usuarios 'Ventas / Reimprimir documentos' en ventana 'Consultar CFDIs de Ventas'
2018.20.0 |05.Jun | Corrección. Ajustes en aplicación de condiciones en la generación de póliza de ventas por partida
2018.20.0 |05.Jun | Corrección. Al enviar reporte por correo, si el nombre contiene comas no se enviaba archivos anexos (#278)
2018.20.0 |05.Jun | Mejora. Al registrar salidas por fabricación, se permite capturar lote y caducidad (#277)
2018.20.0 |05.Jun | Corrección. Al registrar ventas en caja, validar que el precio no sea menor al mínimo al vender unidades adicionales (#250)
2018.20.0 |05.Jun | Mejora. Al registrar ventas en caja, si el artículo esta inactivo se muestra en rojo (#187)
2018.19.0 |24.May | Mejora. Al realizar nota de crédito sin capturar factura relacionada, no sugerir UUID de alguna factura
2018.19.0 |24.May | Corrección. Al cancelar factura global no timbrada, devolvía el status a las notas que se incluyen en una factura global si timbrada (#281)
2018.19.0 |24.May | Mejora. En calcular saldos de pagos parciales para CFDI de Pagos (#279)
2018.18.0 |17.May | Corrección. Al cancelar o devolver compra, no validar existencia si no se está usando la opción 'Afectar inventario al registrar Compras' (#267)
2018.17.0 |15.May | Mejora. Al enviar CFDI por correo automático se puede utilizar un formato personalizado
2018.17.0 |15.May | Corrección. Marcaba error al facturar impuestos locales con ciertos caracteres
2018.17.0 |15.May | Corrección. Marcaba error al reenviar documentos CFDI 3.2
2018.17.0 |15.May | Mejora. Al definir máximos y mínimos, sólo se permite editar la información de la sucursal activa.
2018.16.0 |02.May | Corrección. Al realizar pagos de documentos de contado en caja, se estaba tardando al realizar validaciones.
2018.15.0 |27.Abr | Mejora. Complemento IEDU. Documentación [Aquí](/otros-temas/complemento-iedu/)
2018.14.0 |25.Abr | Mejora. Se permite definir los decimales a usar en el atributo ValorUnitario en el xml. Se debe crear el campo Clientes.CFDIDECPRE
2018.13.0 |23.Abr | Mejora. En factura del día no permitir editar fecha de emisión: Sólamente se podrá editar si el usuario tiene acceso a modificar la fecha en registro de ventas.
2018.13.0 |23.Abr | Mejora. No se permite capturar porcentajes de descuento negativos en ventas y caja.
2018.13.0 |23.Abr | Mejora. Se valida que el porcentaje de IVA sea 0% o 16% en ventas.
2018.12.0 |16.Abr | Mejora. En factura global para RIF no se desglosan los impuestos.
2018.12.0 |16.Abr | Mejora. Se permite predefinir las condiciones de pago del CFDI desde el catálogo de clientes. Al procesar el comprobante, se permite editar las condiciones de pago (#255)
2018.12.0 |16.Abr | Corrección. La emisión de CFDI de traslado automáticamente desde inventario/salidas no mostraba la dirección destino en el PDF.  
2018.12.0 |16.Abr | Mejora. Se agregaron nuevas formulas de CFDI para reglas de pólizas.
2018.11.0 |11.Abr | Corrección. El estado de cuenta automático se enviaba con cualquier factura, ahora sólo se enviará con facturas a crédito.
2018.10.0 |06.Abr | Corrección. Tasa de IEPS con 6 decimales (#270)
2018.10.0 |06.Abr | Corrección. Consulta para envio de estado de cuenta con SQLSERVER marcaba error (#271)
2018.9.0 |03.Abr | Corrección. Al cobrar comisión por forma de pago en caja aparecía el mensaje: "FALLA temp.RECNOART no se cargó correctamente para renglón" (#269)
2018.9.0 |03.Abr | Corrección. Al registrar el primer abono de un apartado en caja aparecía el mensaje: "FALLA temp.RECNOART no se cargó correctamente para renglón" (#266)
2018.9.0 |03.Abr | Corrección. Visualización incorrecta de propiedades de Gastos, cambio en valprop (#264)
2018.9.0 |03.Abr | Mejora. Formato CFDI de Pagos incluye datos de parcialidades (#229)
2018.9.0 |03.Abr | Mejora. Actualización de nombre de XML y PDF en correos enviados automáticamente
2018.9.0 |03.Abr | Mejora. Al enviar CFDIs automáticamente por correo, se muestra el tipo de CFDI en el asunto (#147)
2018.8.0 |24.Mar | Mejora. Catálogo de Formatos Internos para CFDI 3.3
2018.8.0 |24.Mar | Mejora. Se permite generar cfdi de pagos electrónicos por cliente
2018.8.0 |24.Mar | Mejora. Al enviar facturas por correo, se incluye el estado de cuenta del cliente.
2018.8.0 |24.Mar | Mejora. Guardar en un zip los cfdis de pagos (xml y pdf) (#252)
2018.8.0 |24.Mar | Corrección. Al imprimir facturas en donde la serie del folio tenga diagonal, marcaba error (#259)
2018.7.0 |16.Mar | Corrección. Marcaba "Variable cPedimentos is not found" al momento de realizar facturas con pedimentos de importación
2018.6.0 |15.Mar | Mejora. Complemento Otros Derechos e Impuestos (Impuestos Locales)
2018.6.0 |15.Mar | Mejora. Mostrar 'RfcProvCertif' en Cadena Original del SAT en PDF de Pagos
2018.6.0 |15.Mar | Mejora. Al aplicar anticipos, evaluar si el cliente tiene asignado el uso del CFDI para usarlo (#257)
2018.6.0 |15.Mar | Mejora. En notas de crédito permitir seleccionar el uso del CFDI (#256)
2018.6.0 |15.Mar | Mejora. No se obliga a capturar cuenta de origen al seleccionar un banco extranjero en pagos.(#225)
2018.6.0 |15.Mar | Mejora. La búsqueda de proveedores ahora puede hacerse con varias palabras.(#211)
2018.6.0 |15.Mar | Corrección. Error al convertir Usuarios en nueva instalación .(#258)
2018.6.0 |15.Mar | Mejora. Nuevo ícono de mayor resolución para el sistema.(#221)
2018.5.0 |12.Mar | Corrección. Usar TC del día anterior al generar notas de crédito (#242)
2018.5.0 |12.Mar | Corrección. Al cancelar CFDI enviar el RFC del cliente sin transformar los parámetros (#236)
2018.4.0 |08.Mar | Mejora. Generar índices automáticos de cfditr.dbf
2018.4.0 |08.Mar | Corrección. Al imprimir factura desde Caja / Registrar Ventas, no salia correcto el nombre del cliente (#253)
2018.4.0 |08.Mar | Mejora. Descarga automática de Catálogo de Unidades del SAT actualizados (#235)
2018.4.0 |08.Mar | Mejora. Timbrar CFDI después de desbloquear tablas para liberar los DBFs (#231)
2018.4.0 |08.Mar | Mejora. Al agregar factura, en el evento AddFact y AddNC se incluyen las tablas de CFDIS.dbf y CFDIAAMM.dbf antes se enviaban con el evento *CFDI (#231) Se debe actualizar a la versión del enlace 2.0.178
2018.4.0 |08.Mar | Mejora. Limpiar ventana si no se timbro CFDI (#183)
2018.3.0 |07.Mar | Corrección. Error en ventana Emitir CFDI de pagos tablas cerradas y consulta de Metodo de Pago de un CFDI 3.2 (#249)
2018.3.0 |07.Mar | Corrección. El saldo de timbres solo se guardará cuando la consulta sea exitosa, soluciona errores getsaldotimbres[] y seleccionacia[] (#247,#245)
2018.3.0 |07.Mar | Corrección. Botones Encimados al Consultar de Forma Individual una Orden de Compra (#248)
2018.3.0 |07.Mar | Corrección. En compras/Registro de Compras/Cargar CFDI, al hacer clic en icono de Artículos no abría la ventana (#240)
2018.3.0 |07.Mar | Corrección. Cuando la empresa no a configurado el manejo de precios saltaba un error en la actualización masiva de precios (#214)
2018.3.0 |07.Mar | Mejora. Al calcular valor unitario en factura global
2018.2.0 |02.Mar | Corrección. Al volver a capturar folio en ventana de detalle de pedido se empalmaban los botones (#243)
2018.2.0 |02.Mar | Corrección. Al capturar clave incorrecta del articulo en caja aparecía el mensaje: "FALLA temp.RECNOART no se cargó correctamente para renglón" (#246)
2018.1.0 |28.Feb | Mejora. Se activó la facturación Cfdi 3.3 en Caja\Registrar Ventas (#179)
2018.1.0 |28.Feb | Corrección. Diferencias al elaborar nota y facturarla desde caja o al incluirla en la factura global (#192)
2018.1.0 |28.Feb | Mejora. Al emitir Cfdi de pagos, se muestra el método de pago de las facturas (#234) 
2018.1.0 |28.Feb | Mejora. Al depurar información, se incluyen nuevas tablas de cfdis (#232)
2018.1.0 |28.Feb | Corrección. Se corrige error al abonar pedidos: "No puede abonar más del saldo actual" (#220)
2018.1.0 |28.Feb | Mejora. En catálogo de clientes, sólo se obliga a capturar el RFC (#219)
2018.1.0 |28.Feb | Mejora. Integrar No. de Series a la descripción del concepto en XML (#215)
2018.1.0 |28.Feb | Corrección. Se duplicaba el pago al abrir 2 veces la ventana de pagos de contado en caja (#153)
2018.1.0 |28.Feb | Corrección. Consulta de saldo de timbres marcaba error si el cliente no tiene historial de compras (#148)
2018.1.0 |28.Feb | Mejora. Mostrar desglose de impuestos por concepto en formato interno al agregar la expresión oCfdi.getImpuestos()
2018.1.0 |28.Feb | Corrección. Error al timbrar artículos con varios Pedimentos de Importación anteriores a 10 años
2018.1.0 |28.Feb | Corrección. En 'Pago de Facturas y Notas' marcaba error al registrar pago en efectivo
2018.0.0 |12.Feb | Corrección. En Devoluciones y Notas de Crédito no mostraba total si eran millones
2018.0.0 |12.Feb | Corrección. Al entregar pedido marcaba 'Unidad no se encuentra definida'
2018.0.0 |12.Feb | Corrección. Comprobante:Descuento no es igual a la suma de los atributos Descuento (#192, #85)
2018.0.0 |12.Feb | Corrección. En factura global no considera descuentos en cascada (#224)
2018.0.0 |12.Feb | Corrección. Al generar póliza de ventas, no se incluía la factura global (#145)
2018.0.0 |12.Feb | Corrección. Error al consultar compras de timbres la primera vez (#148)
2018.0.0 |12.Feb | Mejora. Se integró proceso para generar [CFDI de Traslados de Mercancía](/otros-temas/cfdi-de-traslado) (#96)

{{% children  %}}
