﻿+++
title = "Año 2017"
weight = 15
+++

Esta es la bitácora de las mejoras y correcciones realizadas a SAIT ERP, Básico y Contabilidad durante 2017

Versión | Fecha | Descripción
---|---|---
2017.49.0 |31.Ene | Corrección. Al imprimir PDFs cerrar tablas correctamente para prevenir perdida de información
2017.49.0 |31.Ene | Corrección. Error al timbrar con Informacion Aduanera de Pedimentos anteriores a 10 años (#185)
2017.49.0 |31.Ene | Mejora. Validacion en salidas en inventario: si articulo esta inactivo sólo permitir procesar si tiene existiencia (#216)
2017.49.0 |31.Ene | Corrección. En factura global, convertir a pesos las notas de dólares (#209)
2017.48.0 |25.Ene | Corrección. Al imprimir oCfdi.aImp[] no alcanzaba a guardar todos los importes (#205)
2017.48.0 |25.Ene | Corrección. Al realizar búsqueda de fechas en ventana de emitir cfdi de pagos mostraba error (#204)
2017.48.0 |25.Ene | Mejora. Mostrar 2 decimales en atributos TotalImpuestosTraladados y TotalImpuestosRetenidos (#201)
2017.48.0 |25.Ene | Corrección. Al aplicar anticipos en dolares marcaba error por que no se estaba incluyendo el atributo TipoCambio
2017.48.0 |25.Ene | Corrección. Al mandar correos automaticamente marcaba error "Alias CFDIS is not found"
2017.47.0 |23.Ene | Corrección. Al abonar pedido marcaba 'Unidad no se encuentra definida' (#203)
2017.47.0 |23.Ene | Mejora. Exportar de manera masiva XML y PDF en archivo comprimido, con serie y folio (#200)
2017.47.0 |23.Ene | Mejora. Si la Factura del Día no se timbró regresar el status disponible a Notas (#194)
2017.47.0 |23.Ene | Corrección. Cambio de secuencia en datos de código QR y Cadena Original en PDF
2017.46.0 |17.Ene | Mejora. Mostrar 'RfcProvCertif' y 'SelloSAT' en Cadena Original del SAT en PDF (#195)
2017.46.0 |17.Ene | Mejora. Al relacionar CFDIs se puede ingresar el UUID manualmente y no cancelados (#146)
2017.46.0 |17.Ene | Mejora. Poder usar datos de Vendedores en formato CFDI 3.3 (#144)
2017.46.0 |17.Ene | Corrección. Al grabar ieps en factura global del día
2017.45.0 |12.Ene | Mejora. Se cambio de lugar el mensaje que aparece al usar WinXP para no bloquear las tablas  (#188)
2017.45.0 |12.Ene | Mejora. Se amplió la captura de decimales en la retencion de iva del catálogo de clientes  (#182)
2017.45.0 |12.Ene | Mejora. Generar factura global del día por corte de caja (#181)
2017.45.0 |12.Ene | Mejora. Permitir cambiar valores en conceptos del xml. Se requiere tener el archivo cfdifixconcepto.fxp (#180)
2017.45.0 |12.Ene | Mejora. Bloquear al vender por debajo de precio mínimo en caja (#167)
2017.45.0 |12.Ene | Corrección. Error en Tasa de Impuesto (16.01%) en abono inicial al hacer un apartado en caja (#158)
2017.44.0 |06.Ene | Corrección. No muestra CBB (código QR) en algunas Facturas (#160)
2017.43.0 |05.Ene | Corrección. Múltiples Series de Notas de Crédito marcaba "Variable CNUMNUSER is not found" (#174)
2017.43.0 |05.Ene | Mejora. No incluir Ieps de combustibles en la base al calcular IVA (#176)
2017.42.0 |03.Ene | Corrección. Diferencia de centavos al elaborar factura del día (#166)
2017.42.0 |03.Ene | Corrección. Al cobrar comisión por tipo de pago en caja, no guardaba la unidad de medida de la clave COMXTC y no permitía facturar la nota (#173)
2017.42.0 |03.Ene | Corrección. Al momento de facturar una nota de venta, guardar los costos de la nota (#171)
2017.42.0 |03.Ene | Mejora. Enviar Uso de CFDI y Forma de Pago de clientes por enlace de sucursales (#170)
2017.42.0 |03.Ene | Mejora. Permitir cancelar CFDI de Pagos en Windows XP (#169)
2017.41.0 |28.Dic | Corrección. Formatos personalizados de Factura CFDI 3.3 no funcionan en ventana de pagos de caja (#168)
2017.40.0 |26.Dic | Corrección. No incluir pedimentos de importación si no se encuentra en el catálogo de pedimentos (#150)
2017.40.0 |26.Dic | Corrección. Error al imprimir factura global muy grande "insufficient stack space" (#149)
2017.40.0 |26.Dic | Corrección. Error data type mismatch al facturar con addenda Chedraui y Comercial Mexicana (#133)
2017.40.0 |26.Dic | Mejora. Incluir observaciones de artículos en Xml (#94). Se debe agregar la siguiente linea al config.msl: VentasInclObsArt=SI
2017.39.0 |22.Dic | Mejora. En factura del día, si el total de la nota es cero se considera 1 centavo para poder facturarla
2017.39.0 |22.Dic | Mejora. No permitir facturar individualmente una nota con ABONO y *ABONO debido a que no pueden venir precios en cero o negativos (#164)
2017.39.0 |22.Dic | Corrección. Al registrar una Prefactura no reconocía la clave de la unidad SAT aún cuando la unidad si tiene clave sat (#162)
2017.39.0 |22.Dic | Mejora. Al registrar gasto en dólares y no tiene capturado el TC, el mensaje que mostraba estaba incorrecto (#156)
2017.39.0 |22.Dic | Corrección. Al tratar de imprimir factura NO TIMBRADA y ya cancelada, marcaba "Cannot update the cursor" (#154)
2017.39.0 |22.Dic | Corrección. Problema con SQL "Error en Select File 'tempMov.dbf' does not exist" (#152)
2017.39.0 |22.Dic | Corrección. En factura del día no consideraba las devoluciones de notas (#143)
2017.39.0 |22.Dic | Corrección. En factura del día no consideraba los descuentos en las notas (#142)
2017.39.0 |22.Dic | Mejora. Al generar póliza de ingresos de cobranza, no incluir el concepto AN - Anticipo (#136)
2017.38.0 | 9.Dic | Mejora. Se optimizo la factura del dia, tardaba cuando se facturaban mas de 3 mil notas
2017.37.0 | 5.Dic | Mejora. En formato de Factura, solo imprimir observaciones en primer hoja (#107)
2017.37.0 | 5.Dic | Corrección. Validar periodos de trabajo al elaborar la factura del día (#140)
2017.37.0 | 5.Dic | Mejora. Agregar proveedor en las observaciones del producto (F11) dentro de la ventana de registro de ventas (#139)
2017.37.0 | 5.Dic | Corrección. Enviar por el enlace de suc. los períodos de trabajo de movimientos al inventario (#135)
2017.37.0 | 5.Dic | Corrección. Al facturar a cliente eventual, la factura (XML) sale con receptor C O N T A D O XAXX010101000 (#132)
2017.37.0 | 5.Dic | Corrección. No se puede hacer timbrar CFDI de pago en Windows XP (#131)
2017.37.0 | 5.Dic | Mejora. Poder seleccionar forma de pago en el catálogo de clientes (#124)
2017.37.0 | 5.Dic | Corrección. Factura CFDI 3.3 no se imprime desde Recibir Pagos (Contado) (#121)
2017.37.0 | 5.Dic | Corrección. No puede imprimir factura 3.3 que no se logro timbrar (#24)
2017.36.0 |29.Nov | Corrección. Al intentar realizar una factura queda el mensaje "Esperando al archivo: TMPXXX.TMP" (#128)
2017.36.0 |29.Nov | Corrección. En factura del día, si la nota esta en cero considerar 1 centavo en los totales(#130)
2017.36.0 |29.Nov | Corrección. Ya no se valida si emisor cuenta con timbres antes de emitir CFDI
2017.35.0 |27.Nov | Mejora. En consulta de CFDIs de ventas se agregó opción para generar archivo .zip con los Xmls consultados (#108)
2017.35.0 |27.Nov | Mejora. En Catalogo de Artículos validar que el porcentaje de Iva sólo sea 0% o 16% (#122)
2017.35.0 |27.Nov | Corrección. En catalogo de artículos con fotografía en el campo margen graba *** cuando el costo es cero (#123)
2017.35.0 |27.Nov | Mejora. Anexar xml de recibo de pago electrónico al generar póliza de ingresos desde cobranza (#119)
2017.35.0 |27.Nov | Corrección. Factura del día tomar la forma de pago mas alta de las notas facturadas (#113)
2017.35.0 |27.Nov | Mejora. SAIT Contabilidad actualizar versión de OCF recomendada (#111)
2017.35.0 |27.Nov | Corrección. No muestra total de saldo en ventana de registro de pagos de clientes en cobranza (#110)
2017.35.0 |27.Nov | Corrección. Factura del dia genera muchas hojas (#107)
2017.35.0 |27.Nov | Corrección. Al elaborar cheques de reposicion de caja, se pierde el foco y NO permite entrar nuevamente a la cuadricula para capturar facturas (#104)
2017.35.0 |27.Nov | Mejora. Antes de generar CFDI, verificar si emisor cuenta con timbres (#82)
2017.34.0 |21.Nov | Mejora. En datos del receptor, incluir residencia fiscal y NumId Fiscal en facturas para comercio exterior (#109)
2017.34.0 |21.Nov | Corrección. Factura global del día duplicaba el importe del IEPS (#106)
2017.34.0 |21.Nov | Corrección. Factura global del día guardaba los costos totales de las notas facturadas (#105)
2017.34.0 |21.Nov | Mejora. Poder editar el formato CFDI de recibo de pago electrónico (#93)
2.0.173   |21.Nov | Corrección. SaitDist - Al emitir CFDI 3.3 en sucursales, en concentradora marca error el enlace: FILE ...CFDI1711.DBF does not exist (#34)
2017.33.1 |17.Nov | Corrección. Al facturar nota: Illegal redefinition of variable 'I'
2017.33.0 |17.Nov | Mejora. En consulta individual de ventas y compras, al abrir la busqueda de documentos mostrar el status (#101)
2017.33.0 |17.Nov | Mejora. Al facturar una nota, usar la forma de pago de mayor importe recibido en caja (#92)
2017.33.0 |17.Nov | Mejora. En cobranza validar que divisa del pago y divisa de la cuenta bancaria a depositar sean iguales (#91)
2017.32.0 |11.Nov | Corrección. Al generar Nota de Crédito en Dólares, marca el mensaje "Property TIPOCAMBIO is not found" (#88)
2017.32.0 |11.Nov | Corrección. En Caja/Facturar Nota de Venta: marca el mensaje "El valor del campo Importe no se encuentra entre el limite inferior y superior permitido" (#86)
2017.32.0 |11.Nov | Corrección. Error al Facturar CFDI33110: atributo descuento no coincide con la suma de importes (#85)
2017.32.0 |11.Nov | Corrección. Problema al generar factura del dia y alguna nota tiene partidas sin clave de art. como observaciones: Traslado debe ser mayor que cero (#84)
2017.32.0 |11.Nov | Corrección. Al generar 2 notas de crédito de la misma factura en 2 PCs diferentes (#8)
2017.31.0 | 8.Nov | Corrección. Al obtener los timbres disponibles. (#80)
2017.31.0 | 8.Nov | Corrección. Al procesar una nota de venta muestra la ventana de facturación de CFDI 3.2 y marca "Falla de Timbrado. Folio Fiscal Vacío, documento debe cancelarse" (#78)
2017.31.0 | 8.Nov | Corrección. Fue Imposible verificar que exista concepto:EF porque tabla de ConcCxc no se encuentra abierta. (#77)
2017.31.0 | 8.Nov | Corrección. Falla en Temp.RECNOART no se cargo correctamente para renglón 1 en Abonos a Pedidos (#75)
2017.31.0 | 8.Nov | Corrección. Al modificar un documento de venta: Record is out of range (#74)
2017.31.0 | 8.Nov | Corrección. Al anexar SPEI 'File vfpmd5.prg does not exist' (#16)
2017.30.0 | 7.Nov | Mejora. En Caja/Recibir Pagos (Creditos) Se permite ordenar registros haciendo doble clic en la columna (#71)
2017.30.0 | 7.Nov | Mejora. En facturas, se agregó el tipo de relacion 02-Notas de débito y se permite relacionar un CFDI de una devolución de factura (#70)
2017.30.0 | 7.Nov | Mejora. En devol. de factura NO seobligaa a relacionar un CFDI (#69)
2017.30.0 | 7.Nov | Corrección. En devol. de factura al borrar el tipo de relacion de un CFDI no podía volver a relacionar un CFDI (#68)
2017.30.0 | 7.Nov | Corrección. Al registrar pagos de clientes en cobranza tardaba en mostrar los documentos por cobrar y tardaba al procesar el pago (#67)(#38)
2017.30.0 | 7.Nov | Mejora. Se agregó método de usuario ArticulosGrabar en ventana de "Catálogo de articulos con fotografía" (#51)
2017.30.0 | 7.Nov | Corrección. Validar si descripción de articulo lleva el caracter pipe no permitir grabar factura (#45)
2017.30.0 | 7.Nov | Corrección. En Cobranza/Abonos al buscar los documentos pendientes de pago tardaba en mostrarlos
2017.30.0 | 7.Nov | Corrección. Al imprimir todos desde Cobranza / Consultar CFDIs de Pagos solo imprime primer registro (#58)
2017.30.0 | 7.Nov | Corrección. Al facturar nota de venta, no imprime formatos adicionales (#61)
2017.30.0 | 7.Nov | Mejora. No se permite realizar facturas desde Caja / Registrar Ventas (#73)
2017.29.0 | 2.Nov | Corrección. Enviar por enlace la factura del día (#59)
2017.29.0 | 2.Nov | Corrección. Al tratar de facturar en negativo, mostraba el mensaje "Property SETGOCUS is not found" (#54)
2017.29.0 | 2.Nov | Mejora. Permitir devolver notas facturadas.
2017.29.0 | 2.Nov | Mejora. En devol. de factura mostrar por default la forma de pago 01-Efectivo, metodo de pago PUE y relacionar el uuid de la factura (#56)
2017.29.0 | 2.Nov | Mejora. En facturas de contado mostrar por default la forma de pago 01-Efectivo y en crédito 99-Por definir (#55)
2017.29.0 | 2.Nov | Mejora. Tasa de IVA con 6 decimales (#47)
2017.29.0 | 2.Nov | Corrección. Al facturar una nota desde caja usando versión 3.3 solicitaba la ventana de método de pago de versión 3.2 (#33)
2017.28.0 | 31.Oct | Corrección. Al entrar a SAIT aparecia un mensaje que indicaba que debe instalar el OCF (#53)
2017.27.0 | 31.Oct | Corrección. Poder elaborar facturas sin timbrar como estaba antes, cambiando la configuracion de factura electronica de version 3.3 a blanco. (#50 y #52)
2017.27.0 | 31.Oct | Corrección. Fallaba al imprimir CFDI 3.3 con Addendas marcaba Fatal Error en nodeToObj (#40)
2017.26.0 | 27.Oct | Corrección. Al registrar un nuevo artículo o exportar desde Excel, no mandaba Clave SAT por enlace 
2017.26.0 | 27.Oct | Corrección. Al emitir factura global usaba método de pago como PPD, debe ser PUE 
2017.25.0 | 26.Oct | Mejora. Manejo de Anticipos segun la nueva regla definida por el SAT, ==vertema==
2017.25.0 | 26.Oct | Corrección. En buscador de Unidades de Medida mostraba '.NULL.' si no tiene Clave SAT
2017.25.0 | 26.Oct | Corrección. En Formatos personalizados tipo CFDI33, no se muestran las unidades de medida del SAT
2017.25.0 | 26.Oct | Corrección. Al modificar cotización, marcaba el error: FALLA en Temp.RECNOART no se cargo correctamente para renglón 1 
2017.24.2 | 18.Oct | Mejora. Version de SAIT Basico 3.3
2017.24.2 | 18.Oct | Corrección. En ocasiones al registrar sistema marcaba error: operator type mismatch
2017.24.2 | 18.Oct | Corrección. Al devolver una remisión, marcaba el error: "Revise bien los datos, ya que NO es posible CANCELAR DEVOLUCIONES DE REMISIONES"
2017.24.1 | 17.Oct | Corrección. Al facturar una remisión no agregaba los pedimento en XML
2017.24.1 | 17.Oct | Corrección. Al mandar 'Consultas de Ventas' a excel marcaba error con 'uuidConGuion.prg'
2017.22.1 | 4.Oct | Mejora. En CFDI para Walmart, permitir llenar noIdentificacion con el SKU ==vertema==
2017.22.1 | 4.Oct | Corrección. Fallaba al timbrar con Windows XP o con Windows 2003 debido al certificado https, se regreso a http con un retraso, se recomienda migrar a Windows 7
2017.22.0 | 3.Oct | Mejora. Envío de Contabilidad Electrónica Versión 1.3
 

{{% children  %}}
