+++
title = "Bitácora de Cambios SAIT-ERP"
description = ""
weight = 1
+++

**NUEVO ENLACE DE DESCARGA 2025:** Para descargar la última versión del sistema SAIT Software Administrativo de clic [aquí ](/otros-temas/descargar-version/)

## Bitácora de versiones

## 2025.10 28-Feb-2025

### Correcciones

- Algunas veces se generaban eventos con caracteres raros, como char(0) o chr(2) o chr(7) esto impedía el procesamiento en SAITSync o SAITNube o SAITDistribuido. Ahora antes de generar el evento se sanitiza el string para evitar esos caracteres raros.

## 2025.7 24-Feb-2025

### Correcciones

- Al generar REP se realizó ajuste al cálculo de impuestos cuando las facturas incluyan IEPS. (OsTicket-132974)

## 2025.6 21-Feb-2025

### Mejoras

- Generar DIOT "Declaracion Informativa de Operaciones con Terceros" con en el nuevo formato 2025

## 2025.5 18-Feb-2025

### Mejoras

- En catálogo de niveles, se agregaron permisos para Ventas 'No permitir copiar documentos' y 'No permitir enlazar documentos'. (Issue-414)

- En evento ActCosto se agregaron fecha de último costo, fecha de costo reposición y fecha de costo máximo. (OsTicket-132799)

## 2025.4 12-Feb-2025

### Correcciones

- En registro de ventas desde Caja, respetar correctamente promociones por Unidad de Artículo. (OsTicket-132754)

- En Tipos de Póliza se agregó validación para no duplicar clave. (OsTicket-132523)

- En reporte de Auxiliares, se restablecieron filtros originales. (OsTicket-132212)

- Proceso 'Verificar diariamente existencias vs movimientos' se igualó consulta a 'Inventario/Verificar existencias/Detectar artículos con problemas'. (OsTicket-132316)

## 2025.3 5-Feb-2025

### Mejoras

- En ventana 'Calcular Máximos y Mínimos' se agregó opción 'Consulta de Artículos sin ventas' para borrar máximo, mínimo y reorden a artículos sin ventas.  (Issue-403)

- Antes de iniciar o ajustar un conteo de toma de inventario V2 se verificarán existencias de los artículos. (OsTicket-132316)

### Correcciones

- Al enviar kardex por enlace, se filtrará por almacén activo. (OsTicket-132435)

## 2025.2 23-Ene-2025

### Mejoras

- Al agregar promociones, ahora se permite especificar Unidad de Articulo y Lista de Precio de Articulo / Unidad, además de filtros por Clasificación de Cliente y Departamento de Articulo. (Issue-412). [Documentación ](/sait-erp/10-capacitacion-modulo-de-ventas/e-descuentos-y-promociones/) 

- En Configuración General de Sistemas / Ventas2, se agregó opción 'Incluir impuestos en columna de Importe'. (OsTicket-132238)

### Correcciones

- Se realizó ajuste al borrar factores de artículo y dar clic en guardar, no se guardaba la información. (OsTicket-132359)

## 2025.1 17-Ene-2025

### Mejoras

- Al aplicar descuento por uso de forma de pago en registro de ventas, solo aplicar cuando el monto a pagar sea mayor a las demás formas de pago. (Issue-372)

- Se independizó permiso para consultar 'Contabilidad / Organizador de Comprobantes Fiscales'. (OsTicket-132117)

- Al acceder a Ayuda o presionar F1, se abrirá navegador con página https://ayuda.sait.mx/. (Issue-969)

### Correcciones

- Al abonar a apartado, validaba erróneamente el último costo de las partidas sin artículo. (OsTicket-132186)

- Al modificar cheque-póliza con REPs relacionados, ahora no se perderá el número de pago correspondiente. (OsTicket-131767)

- Al registrar ventas e incluir descuento en precio, realizar solamente cuando exista el porcentaje de descuento. (OsTicket-131892)

- Al generar REP, se realizó ajuste al cálculo de impuestos cuando las facturas incluyan retención. (OsTicket-132147)

- Al registrar ventas, marcaba error al usar 'Inicializar precio cada vez que se pasa por él'. (OsTicket-132237)

- Al registrar devoluciones de venta, considerar retención por cliente para dar prioridad a retención por articulo. 

## 2024.38 23-Dic-2024

### Mejoras

- Al aplicar descuento por uso de forma de pago en Caja / Registrar Ventas, se realizó ajuste al cargar automáticamente importe cuando es Tarjeta de Crédito/Debito. (Issue-372)

## 2024.37 19-Dic-2024

### Mejoras

- Se permite aplicar descuento por uso de forma de pago en registro de ventas desde Caja. (Issue-372)

- En consultas de ventas se mostrará divisa de documento. (EurosMikai)

- Ahora se permite registrar devoluciones de venta en otras divisas. (EurosMikai)

- En registro de ventas, ahora no se permite editar TC y se mostrará TC de USD al generar documento en Pesos. (EurosMikai)

- En registro de ventas, se agregó soporte para usar TCDOF de otras divisas. (EurosMikai)

### Correcciones

- Al registrar clientes eventuales, no se generaba evento correctamente. (OsTicket-131642)

## 2024.36 4-Dic-2024

- Al generar REPs, considerar devoluciones de venta para calcular impuestos pagados. (OsTicket-130597)

## 2024.34 14-Nov-2024

### Mejoras

- Al emitir factura desde Registrar Ventas F4, en atributo Unidad se permite anexar descripción según el catálogo de unidades del SAT. (Issue-400)

### Correcciones

- Se realizó ajuste a opciones de menú de Cobranza

## 2024.33 13-Nov-2024

### Mejoras

- Ahora al configurar correo por usuario, se permite usar servicio de envío de correos (Postal)

## 2024.32 12-Nov-2024

### Mejoras

- Se agregó nueva ventana Cobranza / Consultar Pagos, la ventana permite visualizar los abonos y sus reps y verificar se hayan registrado a una cuenta destino. [Documentación ](/sait-erp/12-capacitacion-modulo-de-cobranza/d-consultar-pagos/) 

- Se agregó captura de TCDOF a Euros en modo demo (EurosMikai)

### Correcciones

- En actualización rápida de artículos, al modificar los campos Ubica, Máximo, Mínimo o Reorden se afectará MultiAlm si se manejan almacenes. (OsTicket-130384)

- Después de enviar reporte diario de ventas, al generar índices manualmente marcaba error "No se puede abrir: CliEvent". (OsTicket-130919)

- En catálogo de artículos, ahora se permite guardar factores aunque no se capture último costo. (OsTicket-130945)

- En catálogo de artículos, ahora al calcular precios usando máximo costo se guardarán factores correctamente. (OsTicket-131035)

## 2024.31 30-Oct-2024

### Mejoras

- Se agregó captura de TC a Euros en modo demo

### Correcciones

- Al modificar cotización o pedido, no permitía cambiar moneda. (OsTicket-130899)

- Al registrar ventas, después de capturar descuento se borraba. (OsTicket-130868)

- Al registrar ventas, no hacía bien el cambio de precios al modificar la divisa. (OsTicket-130862)

- Ajuste al inicializar Otros Datos cuando se está consultando documento de Compras

## 2024.30 24-Oct-2024

### Mejoras

- En buscador de almacenes, al entrar a la ventana ahora se mostrarán todos los almacenes. (OsTicket-130816)

### Correcciones

- En catálogo de clientes, se realizó ajuste para inicializar siguiente folio

- En devoluciones de compra directas, al ser en dólares no se inicializaba correctamente el tipo de cambio de usar. (OsTicket-130780)

- En función CargarDocumento(), ahora si Docum.DATOSCFDI se encuentra dañado, no marcara error e inicializará las variables en blanco. (OsTicket-130700)

## 2024.29 16-Oct-2024

### Mejoras

- En  Devoluciones de Compra, se agregó procedimiento de usuario al inicializar ventana.

### Correcciones

- En correo de verificación existencias vs movimientos, ahora no se consideraran artículos eliminados (OsTicket-130595)

- En catálogo de clientes, si usuario no tiene permiso "Permitir modificar datos de CFDI" no permitirá modificar Régimen Fiscal.

## 2024.28 10-Oct-2024

### Correcciones

- Al cambiar de almacen, se agregó buscador por texto. (OsTicket-11676)

- No Considerar Kits para lista de faltantes. (OsTicket-130198)

- En Catálogo de Clientes, definir ancho máximo del campo Email Ecommerce.

- Al entrar por primera vez al sistema no enviaba el correo de verificación existencias vs movimientos. (OsTicket-130521)

## 2024.27 9-Oct-2024

- Se agregó soporte para Euros en modo demo al catálogo de articulos y creación de documentos de venta.

- Se actualizó buscador de almacenes, ahora es por texto. (OsTicket-111676)

## 2024.26 7-Oct-2024

### Mejoras

- Se agregó buscador en catálogo de promociones. (OsTicket-130387)

- Ahora se permite depurar devoluciones de notas de venta. (OsTicket-130386)

### Correcciones

- Al generar Cheque-Póliza con pago parcial, se agregó corrección al cálculo de totales. (OsTicket-128310)

- Al generar CFDI de Traslado, ahora si marca error se limpiará la ventana. (OsTicket-129969)

## 2024.25 25-Sep-2024

### Mejoras

- Se agregó la generación de reportes desde SAITNube

### Correcciones

- Al registrar compras y modificar artículos, se agrego validación para guardar Factores correctamente. (Issue-998)

- Al entrar al sistema, se optimizo definición de recursos para evitar que tarde en iniciar el sistema. (OsTicket-129686)

## 2024.24 17-Sep-2024

### Mejoras

- Se agregó captura de otros datos en documentos de Compras, se puede habilitar desde 'Utilerías / Configuración general del sistema / Compras'

- Ahora la columna adicional de Compras también se visualizará en Devoluciones

### Correcciones

- Al generar pólizas, ahora no se agregará ganancia/perdida si no están definidas las cuentas desde configuración. (OsTicket-129916)

- Al realizar ajustes rápidos de existencia se valida que artículo no use lotes. (OsTicket-129805)

## 2024.23 21-Ago-2024

### Mejoras

- Se agregó historial de cambio de precios, se puede consultar desde 'Inventario / Actualizar precios y Costos / Historial de Precios'. [Documentación ](/sait-erp/8-capacitacion-en-modulo-inventario/e-actualizar-precios-y-costos/) 

## 2024.22 29-Jul-2024

### Correcciones

- En catálogo de artículos, campo 'Permitir movimientos con cant. fracc.' ahora se enviará por enlace

- Al actualizar base de datos, solo se van a generar índices si es requerido

## 2024.21 15-Jul-2024

### Correcciones

- Al timbrar CFDI se aumentó a 15 segundos de tolerancia de espera para timbrar documento. (OsTicket-129038)

- Al exportar consulta de documentos a Excel, si un dato llevaba doble comillas marcaba error. (OsTicket-128862)

## 2024.20 8-Jul-2024

### Mejoras

- Se implementó Complemento Carta Porte 3.1, se puede habilitar desde 'Configuración de Factura Electrónica'. [Documentación ](/otros-temas/carta-porte-3.1/)

- Se agregó configuración 'Impedir movimientos con cantidad fraccionada', se puede habilitar desde 'Configuración General del Sistema / Inventario'.

	- Para excluir artículos de ésta validación puede habilitar la opción de  **[ * ] Permitir movimientos con cant. fracc.** en la ventana de Inventario / Catálogo de Artículos y Servicios e Inventario / Unidades Adicionales en Artículos

### Correcciones

- Al agregar cliente se agregó bloqueo para no usar siguiente folio de cliente si ya se usó por otro usuario. (Issue-1143)

## 2024.19 1-Jul-2024

### Mejoras

- En catálogo de niveles se agregaron permisos para modificar Método de Pago y Condiciones de Pago al emitir CFDI. (Issue-324)

### Correcciones

- Al emitir CFDI, ahora cuando el Método de Pago sea PUE la Forma de Pago debe ser distinta a 99-Por Definir. (Issue-324)

## 2024.18 25-Jun-2024

### Mejoras

- Se agregó la funcionalidad de emitir reportes de las facturas emitidas por los clientes usando el módulo del autofacturador en línea. (OsTicket-126457)

## 2024.17 21-Jun-2024

### Mejoras

- Se agregó la funcionalidad de poder incluir el IEPS en el precio del producto para que los CFDIS de Factura de ciertos clientes no se desglose el IEPS. Para identificar los clientes que NO desea desglosar IEPS, debera agregar el campo de clientes.InclIepsPr tipo Lógico, en la pestaña de otros datos del catálogo de clientes. 

## 2024.15 19-Jun-2024

### Mejoras

- Se agregó nueva ventana para "Unificar Articulos" que permite unir 2 artículos. Se accede en Utilerias / Modificar Claves / Artículos

	**NO PRUEBE LA UNIFICACIÓN DE ARTÍCULOS HASTA HABER ACTUALIZADO**

 	Actualizar las versiones de los siguientes programas: 

	- SAIT.exe(2024.15) 

	- SAITDist.exe(2.0.195) 

	Si usa SAITSync como Enlace de Sucursales necesita las versiones: 

    - SAITx.dll(1.17) 
		
	- SAITSyncLinux(3.5)

- En la ventana de "Detectar Inconsistencias" se agregaron consultas DocumSinMovim y MovimSinMinv. (OsTicket-125169)

- Al asociar CFDI a Compra lo normal es que un CFDI aparezca en una sola compra y si intentas asociar el CFDI a otra compra te aparezca el mensaje "CFDI enlazado a Compra ###", pero en algunas empresas se requiere poder dividir la compra en varios documentos. Para pasar por alto esa validación se agregó un nuevo permiso por grupo de usuario que puede activa en Utilerias / Grupos de Usuarios / Compras en [ * ] Permitir enlazar un CFDI a varias compras. (OsTicket-128041)

### Correcciones

- En Facturar Nota y Devoluciones, considerar el campo Clientes.CFDIUSASKU que sirve para que al emitir CFDI el nodo Concepto en el atributo NoIdentificacion contenga en Codigo de Barra del producto en lugar de la clave. (OsTicket-126457)

- Al facturar nota de venta, se detectó que en algunas ocasiones se podia generar un evento incorrectos que al recibirse en otra sucursal creaban un cliente sin clave. (Issue-1139)

- Al crear una compra remota (que se va a recibir en otra sucursal) se detectó que al agregar una partida como observacion, generaba un evento incorrecto, que al recibirse en otra sucursal creaban un articulo sin clave. (Issue-1137)

- En Cobranza - Consultar CFDIs de Pago, al reenviar el evento del CFDI, se detectó que generaba un evento incorrecto. (Issue-1138)

- Al generar índices para Kits de artículos, se estableció el índice 1 como la llave primaria de la tabla. (Issue-1140)

- Al emitir un CFDI, ahora en la ventana de "Datos del CFDI" se puede omitir el atributo de CondicionesDePago. (OsTicket-128318)

- En Inventario al Registrar Salidas, si la empresa maneja números de serie en sus productos al capturar las series si por equivocación se dejaba un renglón en blanco se generaban mal la cantidad de series que salieron. (OsTicket-128280)

- Al emitir un CFDI de Traslado con Complemento Carta Porte, sin cerrar la ventana marcaba error en el segundo CFDI, el error era: El valor del atributo "Comprobante:TipoDeComprobante" es diferente de "T" o el valor registrado en el atributo "Comprobante:Moneda" es diferente de "XXX". (OsTicket-128198)

- Al asociar el UUID del CFDI a Compra no se enviaba por el enlace. (OsTicket-127537)

- Al Agregar Gastos se detectó que el bloqueo de tablas era en orden incorrecto, esto podría provocar algunos bloqueos en empresas con mucho movimiento. (OsTicket-127476)

## 2024.13 14-May-2024

### Mejoras

- En ventana 'Salidas al inventario' ahora se permite usar Salidas a Sucursal usando SAIT Distribuido

### Correcciones

- En ventana 'Asociar REPs de Proveedores a Pólizas' el número de pago de CFDIs 4.0 no se mostraba correctamente. (OsTicket-127649)

- En Ventana 'Consultas de Ventas', al generar Excel no incluía todos los dígitos del UUID de las facturas. (OsTicket-127174)

- En ventana 'Registrar Ventas F4' al usar la opción de 'Copiar' ahora carga los impuestos del documento raíz. (OsTicket-127506)

- En ventana 'Modificación de Números de Serie' ahora se genera evento para SAIT Distribuido. (OsTicket-127885)

- En ventana 'Abonos a Cuentas por Cobrar' ahora se inicializa fecha correctamente después de grabar un abono. (OsTicket-125502)

- En ventana 'Facturar Nota de Venta' ahora el nombre de receptor siempre se mandará en mayúsculas. (OsTicket-128016)

- En Ventana 'Emitir CFDI de Traslado' se realizó ajuste al cargar partidas de compras y movimientos de inventario. (OsTicket-127960)

- En ventana 'Buscar CFDIs de Proveedores' se realizó ajuste para agilizar consulta de folios de compras relacionadas. (OsTicket-127690)

## 2024.12 10-Abr-2024

### Mejoras

- Ahora se permite configurar una cuenta de correo para enviar todos los correos de la empresa, desde [Credenciales Correo de la Empresa] en Configuración general del Sistema. También se debe configurar los usuarios para que tome este método de envío. (Issue-308) Documentación [aquí](/sait-erp/7-capacitacion-en-modulo-de-utilerias/i-envio-de-correos-por-empresa/)

### Correcciones

- Al consultar reportes de ventas se modificó filtro de rango de folios. (OsTicket-127454)

## 2024.11 05-Abr-2024

### Mejoras

- Al consultar CFDI de Proveedores se mostrará la consulta anterior para agilizar búsqueda de CFDIs. (OsTicket-127376)

### Correcciones

- Al registrar Gastos y consultar CFDI de Proveedores ahora no aplicará bloqueo para agregar UUID relacionado a compra. (OsTicket-127406)

## 2024.10 26-Mar-2024

### Correcciones

- Al enviar contabilidad al SAT se amplió campo Num. de Trámite a 14 dígitos. (OsTicket-127207)

## 2024.9 21-Mar-2024

### Correcciones

- Al consultar CFDI de Proveedores, ahora se inicializan fechas y filtros para realizar búsqueda mas rápidamente. (OsTicket-124917)

## 2024.8 21-Mar-2024

### Correcciones

- Al consultar CFDI de Proveedores se optimizó proceso para obtener Folios de Compras relacionados a UUID. (OsTicket-124917)

## 2024.7 19-Mar-2024

### Mejoras

- Al consultar CFDI de Proveedores, se agregaron mensajes para detectar tiempos de respuesta de procesos. (OsTicket-124917)

## 2024.6 06-Mar-2024

### Mejoras

- Se incluye Complemento Venta de Vehículos para Facturas. (OsTicket-126541) Documentación [aquí](/modulos-especiales/complemento-venta-vehiculos/)

- Se agregó configuración para excluir impuestos en Facturas, se puede habilitar desde 'Configuración de Factura Electrónica'. (OsTicket-126600) Documentación [aquí](/temas-soporte/facturar-usando-objetoimp01/)

- En catálogo de clientes se agregó buscador presionando F2.

## 2024.5 20-Feb-2024

### Mejoras

- Se agregó permiso 'Permitir incluir facturas con método de pago PUE' para timbrado de REP. (OsTicket-126000)

- Se agregó permiso 'Permitir Modificar Fecha' para realizar Entradas  y Salidas de Inventario. (OsTicket-116521)

### Correcciones

- Al generar Compl. Carta Porte de Importación, se realizaron ajustes al usar RFC Extranjero. (OsTicket-126553)

- En ventana 'Existencia en Sucursales' se retiraron columnas 'Apartados' y 'Por Llegar'. (OsTicket-126450)

## 2024.4 13-Feb-2024

### Mejoras

- En catálogo de clientes, se realizaron ajustes para mostrar registros activos.

### Correcciones

- Al modificar clientes no se cargaba correctamente lista de Uso de CFDI y Forma de Pago. (OsTicket-126316)

- Antes de emitir factura, ahora se permite editar Método de Pago. (OsTicket-126297)

## 2024.3 30-Ene-2024

### Mejoras

- Se agregó nuevo Catálogo de Clientes. (Issue-1131) 

- Se agregó captura de Ret. ISR en Devoluciones de Compra. (OsTicket-125746)

### Correcciones

- Al cargar CFDI a Compra, se realizo ajuste a cálculo de importes. (OsTicket-124607)

## 2024.2.0 - 08-Ene-2024

### Mejoras

- Emitir Carta Porte con múltiples destinos, dicha opción podrá adquirirla por medio de una licencia de uso. Documentación [aquí](/otros-temas/cfdi-de-traslado/multiples-destinos/) 

- Se agregaron archivos para generar Complemento de Comercio Exterior 2.0 a Libxslt

- En Inventario / CFDIs de Traslado de Mercancía / Camiones / Se agregó campo para capturar el peso de camión y se sumariza con los pesos capturados por artículo

## 2023.41.0 - 6-Dic-2023

### Mejoras

- Evento 1.2 se agrego el nodo Keys para ser usado en SDSync

- Ahora en En Compras - Modificar Órdenes de Compra, solo lo puede hacer el usuario que registró la Orden de Compra. Si desea que cualquier usuario modifique la OC, debe tener activo el nuevo permiso de "Modificar OC de Otros Usuarios". (OsTicket-125240)

### Correcciones

- Falla en GetMsl() imposible abrir archivo config.msl, se amplió el número de intentos antes de marcar error. (OsTicket-125149)

- Al agregar gasto validar si existe cargo en Cxp. (OsTicket-123459)

- Si se maneja precios basados en Margen y tienes un artículo marcado como precio fijo, al recibir compras se cambiaba el precio aún y cuando estaba marcado como precio fijo, ahora se respeta y no cambia el precio. (OsTicket-125187)

- En Ventas / Consultar CFDIs de Ventas, si el usuarios tiene desactivada el permiso de "Consultar Documentos - Ver Totales" ahora se ocultan los totales.(OsTicket-124999)

-  En Inventario / Catálogo de Artículos, si el usuario tiene desactivado el permiso de "Editar Últimas Compras" se inhabilitan también la captura de los últimos proveedores. (OsTicket-124848)

- En ventana de "Asociar REPs de Proveedores a Pólizas" se permite relacionar varios REPs en una póliza, ya que ahora CFE y Telmex generan 2 REPs (Recibo Electrónico de Pago) o CFDI con Complemento de Pago cuando el cliente hace un solo pago. (OsTicket-124232)

## 2023.40.0 - 17-Nov-2023

### Mejoras

- Ahora se permite realizar Complemento Carta Porte 3.0 para Importación y Exportación.

- Se optimizó generación de índices para catálogos del SAT. (Issue-1129)

## 2023.38.0 - 9-Nov-2023

### Mejoras

- Se implementó Complemento Carta Porte 3.0, se puede habilitar desde 'Configuración de Factura Electrónica'.

	Los cambios CCP 3.0 incluídos fueron:

	- Captura de Peso Bruto Vehicular
	- Obligatorio nombre de Operador
	- En formato se incluye un UUID adicional del complemento y un código QR adicional

## 2023.37.0 - 9-Nov-2023

### Correcciones

- En Catálogo de Clientes, después de crear cliente se mostrará un aviso con la clave asignada. (OsTicket-124693)

## 2023.36.0 - 7-Nov-2023

### Correcciones

- Al intentar crear un nuevo cliente con número distintinto al siguiente consecutivo tomaba el consecutivo. (OsTicket-124733)

- Si 2 usuarios procesaban al mismo tiempo una factura y una compra, se tardaba mucho. (OsTicket-123665)

## 2023.33.0 - 31-Oct-2023

### Mejoras

- Al emitir CFDI, validar que existan los programas de sellado openssl.exe y xsltproc.exe, en caso de falla con alguno de ellos mostrar mensaje de falla y guardar mensaje en errores.txt (OsTicket-124654)

- Al emitir CFDI, se bloqueó la selección de PUE o PPD porque los usuarios se confunden y el SAT está tomando esa falla para mandar atenteas invitaciones a pagar las diferencias, de igual forma si una nota de venta se hace a crédito al facturala en Caja se realizará como PPD y si es de contado PUE. (OsTicket-124622)

### Correcciones

- Al crear una Salida al Inventario, si el sistema está configurado para seleccionar capa, considerar el costo de la capa seleccionada. (OsTicket-122911)

- En el menú de Ayuda, se restringió acceso a comandos de VFP, solo podrán accederlo quienes tengan acceso a Configuración General del Sistema. (OsTicket-124520)

- En Ventas-Consulta Individual a veces marcaba el error: Variable RESP is not found

## 2023.31.0 - 24-Oct-2023

### Correcciones

- En Compras-Registro de Compras, al activar botón de limpiar marcaba error unkown member PJEDDP.

- En Inventario-Entradas, bloquear tabla minv antes de procesar entrada al inventario para evitar fallas si otro usuario graba al mismo tiempo.

- En el archivo de rastreo bloquea.txt, se muestra la versión de sait.exe que use la estación.

## 2023.30.0 - 23-Oct-2023

### Correcciones

- Deadlock al estar grabando factura y modificar cliente en diferentes estaciones

## 2023.29.0 - 23-Oct-2023

### Correcciones

- Falla al grabar facturas o notas. File / Record is in use by another user. (OsTicket-123665)

## 2023.28.0 - 17-Oct-2023

### Correcciones

- Falla al grabar facturas o notas, file is in use by another user. (OsTicket-123665)

## 2023.26.0 - 4-Oct-2023

### Mejoras

- Ahora se pueden actualizar Precios por Volumen con % de margen de ganancia en base a último costo. (Issue-248)

### Correcciones

- Al registrar salida a sucursal, no tomaba los mismos decimales al realizar la entrada. (OsTicket-123286)

- Al agregar cargo manualmente en Cobranza, en ocasiones no cargaba correctamente la clave del concepto. (OsTicket-124170)

## 2023.25.0 - 25-Sep-2023

### Mejoras

- En “Utilerías / Configuración general del Sistema / Otros” se agregó la consulta de Inconsistencias en Base de Datos. (Issue-1118). Documentación [aquí](/temas-soporte/inconsistencias-en-bd/)  

- En “Cobranza / Verificar Saldos de Clientes” se agregó la pestaña "Saldar deudas pequeñas". (OsTicket-123310). Documentación [aquí](/sait-erp/12-capacitacion-modulo-de-cobranza/o-saldas-cuentas-chicas/)  


### Correcciones

- Al finalizar de agregar Abonos de Cobranza, ahora se vuelve a cargar correctamente la fecha. (OsTicket-123915)

- Al agregar Clientes, ahora se agregó bloqueo para no duplicar claves. (Issue-1079)

- Al cargar XML de CFDI recibido con comentarios marcaba error. (OsTicket-123714)

- En consulta de CFDIs recibidos ahora solo se consideran Folios de Compra no cancelados. (Issue-203)

## 2023.24.0 - 10-Ago-2023

### Mejoras

- Se aumentó a 15 columnas a mostrar en búsqueda de artículos. (OsTicket-122531)

### Correcciones

- Se agregó validación para no permitir capturar cantidades en negativo al registrar ventas. (OsTicket-123350)

- En consulta de CFDIs recibidos ahora se refrescan los Folios de Compra cada vez que se ingresa a la ventana. (Issue-203)

## 2023.23.0 - 4-Ago-2023

### Mejoras

- Se crearon permisos individuales para actualizar Tipos de Cambio de venta. (Issue-227)

- En Compras / Registro de Compras en consulta de CFDIs recibidos ahora se muestra Folio de Compra enlazada y se muestran en rojo los XML que ya fueron registrados. (Issue-203)

### Correcciones

- Para capturar Descuento por Pronto Pago en Registro de Compras ahora no es necesario habilitar permiso 'Descuentos'.

## 2023.22.0 - 14-Jul-2023

### Mejoras

- Ahora se permite capturar Descuento por Pronto Pago en Registro de Compras. (Issue-158). Documentación [aquí](/sait-erp/9-capacitacion-modulo-compras/o-descuentos-pronto-compras/)

### Correcciones

- En "Ventas / Registrar Documentos" corrección  de versión 2023.18 marcaba "record is use by another user" al grabar notas. (OsTicket-122739)

## 2023.20.0 - 5-Jul-2023

### Correcciones

- Se dejó configurable el desglosar IEPS por Cuota para Combustibles en CFDI. (Lugasa)

## 2023.19.0 - 30-Jun-2023

### Correcciones

-  Falla al emitir CFDI de Traslado con artículos que tienen IEPS por Comb. ValorUnitario se llenaba cuando debe ser siempre 0. (OsTicket-122699)

## 2023.18.0 - 27-Jun-2023

### Correcciones

-  Mejora al procesar venta desde Caja, ahora se bloquean archivos. (OsTicket-122304)

-  En Registro de Ventas F4, al registrar varios documentos seguidos empezaba a marcar 'Alias is not found'. (OsTicket-122560)

- En Registro de Ventas F4, al registrar varios documentos seguidos empezaba a marcar 'File is in use'.

## 2023.17.0 - 22-Jun-2023

### Correcciones

- Falla al reabrir DBFs que no tenían registros. (OsTicket-122557)

## 2023.15.0 - 20-Jun-2023

### Correcciones

- Falla Ventas-Registrar Docs. No se actualiza correctamente el índice en los movimientos de caja, faltan algunos movimientos en la consulta, sí están en el dbf. (OsTicket-122304)

- Falla al emitir CFDI a cliente con "+" marca error prevalidación en el nombre. 

## 2023.14.0 - 14-Jun-2023

### Correcciones

- Se amplió caracteres de Descripción de Arts al enviar a excel en ventana Auxiliar de Pedido en Compras. (OsTicket-121733)

- Botón de 'Facturar' no se alcanzaba a mostrar cuando se utilizaba la prefactura. (OsTicket-121899)

## 2023.13.0 - 23-May-2023

### Mejoras

- Se agregó permiso No mostrar Datos de Crédito de clientes. (Issue #202)

### Correcciones

- Mandar nombre de receptor siempre en mayúsculas. (OsTicket-121918)

- Considerar solo descuento por partida en devoluciones de compra. (OsTicket-121733)

- Error CRP20211 al generar REP, ahora vamos acumular sin redondear. (OsTicket-121623)

- Fallaba al obtener XML de uuid en minúsculas (directo de XML), se mejoró forma de obtener XML de factura. (OsTicket-121456)

- No se mostraba completo el número de almacén al usar 3 dígitos, ahora se define el ancho de clave de sucursal antes de inicializar. (OsTicket-121725)

## 2023.12.0 - 8-May-2023

### Mejoras

- Poder emitir CFDI con clave de Exportación 03 o 04. (Issue #194)

## 2023.10.0 - 2-May-2023

### Correcciones

- En Ventas / Consultas Generales se captura la clave de cliente en mayúsculas. (OsTicket-121282)

- En Caja / Facturar Nota de Venta / No se aplicará redondeo, si se usan todos los decimales los totales son más exactos. (OsTicket-105500)

- Se preparó ventana Cuentas por Pagar / Reponer Gastos Pagados en Efectivo para considerar CFDIs 4.0 (OsTicket-121341)

- Correcciones al realizar devoluciones utilizando cliente eventual. (OsTicket-121282)

## 2023.9.0 - 19-Abr-2023

### Mejoras

- Al emitir CFDI al indicar los CFDI Relacionados, ahora es posible emitir CFDI con varios tipos de relación. (Issue #190)

## 2023.8.0 - 17-Abr-2023

### Correcciones

- En Devoluciones de Compras se dejó cálculo de Impuestos igual que Registro de Compras porque había diferencias. (OsTicket-121083)

- Al imprimir REP con exentos se borraba la lista de Importes. (OsTicket-121132)

- Validar salida de series cuando se enlacen Ped. o Cot. (OsTicket-120933)

- Marcaba error al enviar correo CFDI de Traslado. (OsTicket-120871)

- Abrir Pedim.DBF para que no marque errores al generar Factura con pedimento. (OsTicket-119883)


## 2023.7.0 - 27-Mar-2023

### Correcciones

- Al emitir CFDI 4.0, no se incluirá atributo 'TotalImpuestosTrasladados' si solo se incluyen artículos exentos. (OsTicket-120765)

- En "Ventas-Registro de Ventas", columna de Importe ahora siempre se mostrará con impuestos de artículo. (OsTicket-120306)

## 2023.6.0 - 23-Mar-2023

### Mejoras

- En "Caja - Facturar Notas de Venta", se agregó permiso "Caja/Refacturar notas de venta" para poder volver a realizar factura de una nota de venta ya facturada.

### Correcciones

- En "Cobranza - Emitir REP a Terceros", ahora siempre se permite editar el nombre del receptor antes de emitir el CFDI. (OsTicket-120381)

- En "Inventario - Catálogo de Artículos, Kárdex y Catálogo de Unidades", ahora se ocultaran costos de artículos si usuario no tiene permiso "Inventario / Mostrar costos". (OsTicket-120352)

- En "Inventario - Registrar Entradas/Salidas", al capturar mas de 15 partidas las últimas partidas se borraban. (OsTicket-120442)

- En "Caja - Elaborar Factura del Día", se habilitó generación de Factura Global CFDI 4.0 para emisores RIF. (OsTicket-120554)

## 2023.5.0 - 10-Mar-2023

### Mejoras

- "Caja-Otras Salidas de Efectivo" ahora solicita autorización, en caso que el tipo de movimiento tenga activada la opción de "Solicitar Autorizacion". 

- "Ventas-Registro de Ventas" Al modificar Cotización o Pedido permitir usar lectora y pegado de información. (OsTicket-120110)

- "Utilerias-Enlace por Internet-Reenviar Informacion" ahora en "Enviar Documentos", se permite ReEnviar los kardex de la sucursal a la Concentradora, para facilitar la Depuracion de Movimientos al Inventario en Concentrador. (Issue-1116)

### Correcciones

- "Ventas-Registro de Ventas", al pegar partidas con unidad NO default no pegaba correctamente. (OsTicket-119945)

- "Contabilidad-Organizador de CFDIs" al descargar XML sin folio marcaba error. (OsTicket-119845) 

- "Cobranza-Consultar CFDI", al imprimir reps 3.3 desde 'imprimir todos' marcaba error. (OsTicket-119978)

- "Venta-Registro de Ventas" al facturar a "Publico en General" y  tener activado la prevalidacion de datos fiscales del cliente, marcaba error "Debe existir Nodo Informacion Global". (OsTicket-119990)

- "Caja-Facturar Nota de Venta" estaba llenando el dato "Forma de Pago" con el dato del catálogo de clientes, en vez de usar el dato de como fue pagada realmente la Nota de Venta. (OsTicket-119621)

- "Bancos-Estado de Cuenta" en caso de que la empresa sola tenga una cuenta bancaria, fallaba al mostrarlo. (OsTicket-118910)

- "Ventas-Registro de Ventas" al actualizar (manualmente) precio de artículo, redondear a los decimales configurados. (OsTicket-119836)

- "Ventas-Clientes Eventuales" se agregó el campo de Nombre Fiscal, porque al emitir CFDI 4.0 a Clientes Eventuales, a veces fallaba cuando el SAT tiene registrado al contribuyente con Apellido-Nombre. (OsTicket-118382)

- "Caja-Elaborar Factura del Dia", se cambió forma de validar que la nota de venta tenga partidas, ya que muchas veces los usuarios acostumbran saltar un renglón al inicio de la cuadrícula. (OsTicket-120143)

## 2023.4.0 - 14-Feb-2023

### Mejoras

- Se agregó permiso 312 "Prohibir Cerrar SAIT.exe" al definir un Grupo de Usuarios en la pestaña de Utilerías, al activarla los usuarios pertenecientes a ese grupo, no podrán cerrar el sistema, ni usar ALT-F4, ni Utilerías-Salir, ni el botón de salir en la barra de herramientas.

## 2023.3.0 - 31-Ene-2023

### Mejoras

- Se agregó la opción de validar datos fiscales del cliente, antes de guardar la factura, documentación [aquí](/cfdi4/validar-datos-del-cliente-cfdi40/)

### Correcciones

- Falla al emitir CFDI de Pago en dólares de facturas en pesos, se aumentó a 10 decimales el atributo de EquivalenciaDR (OsTicket-119542)

- La ventana de "Inventario - Actualización Rápida" ahora incluye el código de barra en los campos usados para localizar los artículos a modificar.

- En "Caja - Elaborar Factura del Día" en caso de encontrar inconsistencia en las notas de venta, se avisa e impide la emisión de la factura. Las inconsistencias que se detectan son (1) una nota de venta sin partidas y (2) dos notas de venta con el mismo folio.

## 2023.1.1 - 10-Ene-2023

### Correcciones

- En "Utilerias - Importar Informacion - CFDI Emitidos" al importar un CFDI de Factura Global, marcaba el mensaje "No se pudo cargar XML", se debía al nuevo atributo Año en el nodo de FacturaGlobal. (OsTicket-119125)

- En "Contabilidad - Generar Póliza de Operaciones - Ventas" si la empresa usa IVA al 8%, los asientos contables de la Factura Global aparecen sin cuenta. La causa era una falla al calcular las variables nGravado8 y nIvaTasa8. (OsTicket-119002)

- En "Ventas - Registro de Ventas" al emitir CFDI 4.0 con IEPS de Vinos, con diferente tasa de IEPS, aparecia la falla de timbrado CFDI40119 por mal cálculo del IEPS. (OsTicket-116409)

- Al emitir CFDI 4.0 a RFC Genérico XAXX010101000, el campo de DomicilioFiscalReceptor debe ser el CP del Emisor (LugarExpedicion). (OsTicket-118862)

### Mejoras

- Antes de emitir un CFDI 4.0 en la ventana de "Información de CFDI" si metodo de pago es PPD (Pago en Parcialidades o Diferido) automáticamente la forma de pago debe será 99:Por Definir. (OsTicket-117250)

- Al emitir CFDI 4.0 de Traslado, los atributos de Subtotal y Total se mandan sin decimales.

- Al imprimir CFDI de Pago o REP, se muestran las tasas de impuesto en cada Pago.

## [2022.23.10] - 7-Diciembre-2022

### Correcciones

- En Compras / Cargar Compra desde CFDI, si se trataba de un CFDI 4.0 con descuento, no consideraba el descuento. (OsTicket-118519 - Guadalajara Jal)
- En Ventas / Registro de Ventas, al aplicar un Anticipo, el SAT cambió la instrucción y ahora la Nota de Credito que se genera automáticamente, SOLO DEBE INCLUIR en el CFDI Relacionado la factura del bien o servicio, anteriormente la indicación del SAT fue incluir tanto la factura del anticipo como la factura del bien. (OsTicket-117813)

### Mejoras

- En Inventario / Precios / Precios por Cliente, se mejoró la ventana de catálogo, se agregaron filtros de artículo y cliente para localizar más fácilmente los registros.

- Al emitir CFDI de Pago (REP), se agregó un filtro de Sucursal, para emitir los Recibos de los pagos hechos en una Sucursal. (OsTicket-118537 - Estado de México)

## [2022.23.9] - 1-Diciembre-2022

### Correcciones

- Al tener activado el uso de IEPS para Combustibles, a veces emitir CFDI 4.0 marcaba Error 40108 Suma de valores Importe de Conceptos  ES DIFERENTE de Comprobante.Subtotal. (OsTicket-118284 - Guadalajara Jal)

- En Ventas/Consultar CFDIs al generar ZIP con XML y PDF marcaba Error "Nothing to do !" esto pasaba cuando el folder donde se instaló SAIT.exe contiene espacios, ejemplo en una instalación bajo terminal server, C:\users\juan perez\documents\SAIT (OsTicket-113854 - Hermosillo Sonora)

## [2022.23.8] - 17-Noviembre-2022

### Correcciones

- Al emitir un CFDI 4.0 con Addenda, el nodo Addenda aparece 2 veces en el XML, ahora solo aparece una vez. (OsTicket-118036 - MD3)

- En "Inventario - Consulta de Movimiento a Inventario" en caso de que la empresa manejar más de 99 sucursales (numalm de 3 dígitos) la consulta del movimiento, mostraba un almacén incorrecto. Lo mismo se hizo en "Inventario - Salidas de Inventario". (OsTicket-117775 - Tuxtepec Oaxaca)

- En "Compras - Registrar Compras", en caso de cargar una Orden de Compra, la fecha de pago mostrada no era correcta, ahora después de cargar la orden de compra, se carga la fecha de pago según la fecha de compra más los días de crédito que se tienen con el proveedor. (Issue #1084 - Rodeson Sonora)

- Al salir de SAIT, marca mensaje "Too many parameters", la falla era ocasionada porque había muchos archivos en la carpeta de c:\sait\temp que fueron generados en "Ventas - Consultar CFDIs de Ventas" con el botón de "Guardar Todos en ZIP", ahora después de usar ese botón, se eliminan esos archivos temporales generados. (Issue #1099 - SAIT BC)

- En "Inventario - Catálogo de Articulos Web" al definir el nombre del archivo de la foto, permitir hasta 60 caracteres, en el nombre del archivo. (Issue #1097 -MD3)

- En "Inventario - Modificar Precios - Precios x Cliente", en caso de tratar de agregar un registro que ya existe, en lugar de marcar el mensaje: "Ya existe ese precio para Cliente-Articulo-Unidad" ahora pregunta si desea modificarlo.

- En "Caja - Pagar" se aumentó la máscara del pago, para permitir aceptar pagos de hasta 999,999.99 antes estaba a 99,999.99. (OsTicket-117467 - San Quintin BC)

- En "Ventas-Consulta Individual" solo mostrar el botón de "Seguimiento a Cancelacion" en facturas y notas de crédito timbradas. (OsTicket-117527 - Hermosillo Son)

- En "Caja - Registrar Ventas" validar que precio no sea menor que el costo. (OsTicket-117368 - SLRC Son)

### Mejoras

- Ahora al emitir una factura a crédito en CFDI 4.0, automaticamente se usara la Forma de Pago 99: Por Definir, ya que el SAT asi lo obliga. (OsTicket-117250)

## [2022.23.7] - 10-Oct-2022

### Correcciones

- Falla al emitir cfdi 4.0 Objeto6.Set. (OsTicket-117381)

- Se corrigió falla generada en versión 2022.23.6

## [2022.23.6] - 7-Oct-2022

### Mejoras

- Ahora en "Caja - Elaborar Factura del Dia" en CFDI 4.0 permite elaborar factura por: Mes, Semana o Diaria. Se agregó la opción Semanal. (OsTicket-114790 - Gualajara Jal)

- Ahora en las variables contabilizacion se agregaron la base segun la tasa de iva y el descuento segun tasa de iva. [Ver documentación](/sait-erp/16-capacitacion-modulo-de-contabilidad/q-variables-asientos-contabasica/)

Las variables agregadas fueron:

* nBaseIvaE, nBaseIva0, nBaseIva8, nBaseIva16

* nDescIvaE, nDescIva0, nDescIva8, nDescIva16

Tanto en el objeto oDoc (usando las moneda del documento) como en el objeto oDoc.Impuestos (en pesos al TC).


## [2022.23.5] - 5-Oct-2022

- En "Cobranza - Emitir REPs" en caso de ser CFDI 4.0, pago en MXN y facturas en USD, aparecía el error de timbrado: CRP20268 BaseP en Traslado no es igual a la suma de los importes de las bases. (OsTicket-117278 - Mexicali BC)

## [2022.23.4] - 5-Oct-2022

### Correcciones

- Al usar "Caja - Registrar Ventas" para facturar, a veces se timbraba con la unidad SAT incorrecta. Se debía a que por default se selcciona nota y al cambiar el tipo de documento a generar no se recargaban las partidas y se usaba la unidad incorrecta. (OsTicket-116643 - Salina Cruz Oax)

- Al usar "Cobranzar - Emitir REP a Terceros" fallaba al tratar de usar un nombre muy largo como: "ARRENDADORA Y FACTOR BANORTE, S.A. DE C.V., SOCIEDAD FINANCIERA DEOBJETO MULTIPLE, ENTIDAD REGULADA, GRUPO FINANCIERO BANORTE". (OsTicket-116910 - Monterrey NL)

- En "Contabilidad - Generar Polizas ", en caso que la empresa tuviera desactivada la opción de: Configuración Genral - Contabilidad - Ordenar Asientos, los UUIDs relacionados de la poliza, aparecian en blanco, ahora ya aparecen correctamente. (OsTicket-117158 - Mexicali BC)

- En "Cobranza - Emitir REPs" en caso de ser CFDI 4.0, pago en MXN y facturas en USD, aparecia el error de timbrado: CRP20217 Suma de Valores de ImpPagado incorrecta. (OsTicket-117278 - Mexicali BC)

### Mejoras

- En "Caja - Registrar Corte de Caja" si la empresa este configurarda para NO aceptar Dólares como pago, no se mostrará la opción de capturar dólares. (OsTicket-116730 - Tuxtepec Oax)

- En "Bancos - Conciliacion Bancaria", se mejoraron las ventanas:

* "Conciliar Depósitos"

* "Conciliar Depósitos Mejorada"

* "Conciliar Retiros Mejorada"

Se agregó un botón de búsqueda, que permite buscar por monto, texto, concepto en los movimientos.

## [2022.23.2] - 9-Sep-2022

### Correcciones

  - En Inventario/Salidas, al tener habilita opción 'Generar CFDI de Traslado al registrar salidas por traspaso', ahora genera CFDI 4.0 y 3.3. (Issue #1084)

## [2022.23.1]  - 2-Sep-2022

Esta versión requiere actualización de base de datos

### Correcciones

  - Ahora en Cobranza / Registrar Abono, permite emitir CFDI de Pago 4.0. Debe tener habilitada la opción de: Generar REP al Registrar Abonos (Osticket-116380 - Transmexicana)

  - Falla al generar REP de cliente que lleva comillas en el nombre (Osticket-115669 - Operadora Nava de Mexico)

  - Corrección en ventana de Cxp - Agregar Cargo, fallaba al validar que folio ya se hubiera registrado, cuando el folio inicia igual un folio anterior, del mismo proveedor. (OsTicket-116315 - Tomason)

  - Emisión de CFDI para reventa de combustible con IEPS integrado en precio. (OsTicket-115912 - Guillermo Perez)

  - Al agregar clientes en diferente estacion, se creaban 2 clientes con el mismo número. (OsTicket-116213 - Ferreteria El Cardenal)

  - Al emitir REP marca el mensaje "The 'ImpSaldoInsoluto' attribute is invalid", esto sucede solamente después de haber modificado el folio interno de factura, en la ventana de consulta de facturas. (OsTicket-115138: Norma Deila Maytorena Minjares)

  - En Ventas / Devoluciones al dar enter, hacer salto al siguiente renglón. (OsTicket-114794 - Tomason)

  - En Inventario / Emitir CFDI de Traslado si se enlaza una orden de compra no carga nada. (OsTicket-115549 - SAIT BC)

  - Cuando utilizas la búsqueda de artículos por clave, la ventana de Búsqueda de Artículos no mostraba en rojo los inactivos. (sTicket-114829 - Emagas)

  - Agregar automáticamente el uso de CFDI S01 - Sin Efectos Fiscales (Osticket-114713 - Carlos Flores)

  - En Ventas - Registrar Devoluciones, validar que número de cliente no sea vacío, ya que existía un registro en la tabla de clientes con folio vacío. (Osticket-114794 - Tomason) 

  - Al emitir Factura Global con CFDI Relacionado marcaba error porque la estructura era incorrecta, nodo InformacionGlobal debe ir antes que nodo de CfdiRelacionados. (OsTicket-115774 - Farmacias Angeles)

  - Al utilizar la ventana de Inventario - Actualizar Precios - Precios por Volumen, NO considera correctamente el IEPS. (Issue-1073 - Super Dulce)

  - En impresión de CFDI de Pago, se agregó el Regimen del Cliente y la version CFDI 4.0. (Issue-1056)

  - Al enviar Orden de Compra por correo, incluir el 3er contacto del proveedor. (Issue-1070)

  - No permitir enlazar OC de diferente almacén. (Issue-1078)

  - Al recalcular estadísticas anuales, actualizar MultiAlm.VENTANOQTY. (Issue-1075)

## [2022.22.15] - 8-Ago-2022

### Correcciones

  - Correcciones al importar CFDIS emitidos. (OsTicket-115904 - Fuerza telefónica del norte)

## [2022.22.14] - 8-Ago-2022

### Correcciones

  - Falla al cancelar CFDI de Cliente Eventual. (OsTicket-115840 - Tomason)

  - Al importar CFDIs emitidos, no se leía correctamente Ñ y acentos. (OsTicket-115904 - Fuerza telefónica del norte) 

## [2022.22.12] - 12-Jul-2022

### Correcciones

  - No se detectaba correctamente el cambio de versión. (OsTicket-114977)

  - Fallaba al cancelar CFDI de Traslado: Imposible obtener status del CFDI en el SAT. (OsTicket-115237 - Tomason)

  - Falla al cancelar facturas: Imposible obtener status del CFDI en el SAT (OsTicket-115186)

## [2022.22.11] - 7-Jul-2022

### Correcciones

  - Al emitir REP a terceros, nombre de receptor no cabía en campo de texto. Se aumentó tamaño. (OsTicket-115039 - Aislantes y recubrimientos técnicos)

## [2022.22.10] - 29-Jun-2022

### Correcciones

  - Factura Global del Día, se agregó una opción especial para EXCLUIR las devoluciones de la factura del dia. Para configurarlo ir a Utilerias/Configuración General pestaña de Caja activar: En Factura del Día, excluir Devoluciones. (OsTicket-14665 - Ahued)

  - Importar CFDIs, si RFC es genérico, buscar Cliente por nombre, consideraba solo mayúsculas

## [2022.22.9] - 27-Jun-2022

### Correcciones

  - Factura Global del día, salen con diferencias de 2 pesos. (OsTicket-114666 - Ahued)

### Mejoras

  - Al importar XML, si es RFC generico buscar Cliente por nombre

## [2022.22.7] - 22-Jun-2022 

### Correcciones

  - Al cancelar CFDI si emisor o receptor contiene &, marcaba: Imposible obtener status de CFDI en SAT. (OsTicket-114614 - Promotora Produce)

  - En "Compras-Registro de compras" despues de enlazar una "Orden de compra" al activar el boton de insertar renglon, eliminaba el último renglón de la cuadricula. (OsTicket-111908 - SAITBC y NPO)

## [2022.22.6] - 21-Jun-2022 

### Mejoras

  - En Proceso de Inventario Mejorado 2.0, en la ventana de "Conteo de Inventario" se agregó que se muestre la observacion o comentario del conteo. (OsTicket-112775 -Tomason)

### Correcciones

  - En "Utilerias-Config General-Inventario" al tener activada la opción de "Verificar diariamente existencias vs movimientos" enviar reporte por correo, fallaba al enviar correo, ahora se usa el correo definido en "Envío de Correo" en "Configuración de Factura Electronica". (OsTicket-112125 - MD3-SICSA)

  - En "Caja - Facturar Nota" al emitir factura de una nota, permitia al usuario modificar la forma de pago, ahora toma la forma de pago usada en la nota. (OsTicket-114551 - Ahued)

  - En "Ventas-Registro de Ventas" al tener definido precios en dólares para artículos, a veces mostraba el mensaje "Los siguientes articulos fueron rebajados por el vendedor" por fallas de redondeo. (OsTicket-112803 - Patricia Martinez Arce)

  - Ventana de "Modificar Cheque Póliza" cargaba mal la cuenta bancaria al accesarla asi: Bancos - Consulta General de Movimientos - Doble clic en el cheque a modificar. (Issue-1033 - Fetasa)

  - En "Inventario-Catálogo de artículos", si la empresa utiliza precios en base a costos, si marcabas la casilla de "Precio Fijo" para manejar ese artículo con precio fijo, no se enviaba por enlace de sucursales. (OsTicket-114280 - Hayakawa)

  - En "Ventas-Registro de Ventas" al modificar cotización y un artículo ya no existe (porque fue borrado), marcaba error:  record is out of range form1.actttotales33. (OsTicket-113863 - Cercos Vertical)

  - En "Inventario-Actualizar Precis-Actualizacion Masiva" al utilizar la opción #3 "Modificar Maximo Costo", no generaba eventos para enlace de sucursales. (OsTicket-114280 - Kayakawa)

  - En "Caja-Facturar Nota de Venta", al activar la casilla de "Enviar a Cobranza" no validaba los días de crédito ni el límito de crédito. (Issue: 1012 - NPO)

  - "Inventario-Catálogo de Articulos", se corrigió mensaje incorrecto al agregar código de barras, que decia: "Está modificando la unidad del articulo..." (OsTicket-112714 - SAITBC)

## [2022.22.5] - 14-Jun-2022

### Mejoras

  - En Inventario - Actualización Rápida de Artículos, se agregó campo de "Peso". (OsTicket-113502)

### Correcciones

  - Falla al cancelar CFDI de Pago (REP) imposible obtener status de CFDI en SAT. (OsTicket-114243)

  - En Ventas/Registro de Ventas falla al aplicar promociones cuando se trata de una promo relacionada con UNIDADES ADICIONALES al artículo. (OsTicket-111988 )

## [2022.22.4] - 9-Jun-2022

### Correcciones

  - Despues de actualizar tablas, regenerar índices

## [2022.22.3] - 7-Jun-2022

### Correcciones

  -  Falla al emitir factura de exportación.
  -  Falla al emitir CFDI con artículos exentos. (OsTicket-113794)
  -  Falla al emitir factura de anticipo. (OsTicket-113324)

## [2022.22.2] - 6-Jun-2022

### Correcciones

 - Al emitir factura global en una sucursal, se debe usar el mismo CP en Emisor y Receptor

## [2022.22.1] - 3-Jun-2022

### Correcciones

  - Se agregó regimen 626 RESICO - Régimen Simplificado de Confianza al catálogo de Regimen Fiscales.

## [2022.22.0] - 3-Jun-2022

### Correcciones

- Al Cancelar factura marcaba: Data type mismatch CancCfdi1.crear() cuando el CFDI aún no llegaba al SAT. (OsTicket-113952)

- Al eliminar un código de barra en la ventana de Multiples códigos de barra, cuando usas SAITSYnc, no se trasmitía la eliminación. 

- Al generar CFDI de Traslado, en CFDI 4.0 Receptor.DomicilioFiscalReceptor se tomaba el CP del Almacén y no de la empresa.

- El Formato PDF de CFDI de Pago mostraba CFDI 3.3 se cambio a mostrar ambas versiones. 

## [2022.21.0] - 2-Jun-2022

### Correcciones

- Corrección al emitir REP marcaba error CFDI40145 si el nombre del cliente excede 60 posiciones. (OsTicket-113900)

## [2022.20.0] - 31-May-2022

### Correcciones

- Al emitir REP marcaba error CFDI40145 si el nombre del cliente excede 60 posiciones. (OsTicket-113900)

## [2022.19.0] - 31-May-2022

### Correcciones

- A emitir marcaba error: CRP20261 El valor del campo ImporteDR no se encuentra entre el rango permitido. (Issue-1052)

## [2022.17.0] - 30-May-2022

### Mejoras

- En Cobranza - Emitir REP CFDI de Pago, emite CFDI 4.0 o 3.3

- En Inventario - Emitir CFDI de Traslado, emite CFDI 4.0 o 3.3

- Se agregó una nueva opción para actualizar el sistema SAIT fácilmente, accediendo al Menú de "Utilerías - Actualizar Sistema SAIT" que le permite actualizar rapidamente su sistema a la última versión disponible.

- En Cobranza - Emitir REP CFDI de Pago, ahora permite usar Alt-T para activar casilla "Todos"

- En Ventas - Consultar CFDIs, ahora permite usar Alt-A para acceder al seguimiento de cancelaciones

## [2022.16.0] - 24-May-2022

### Correcciones

-  En Ventas - Registro de Ventas, cuando empresa es americana y no usa CFDI, marcaba error: unkonw member oFe - (Issue-1046)

## [2022.15.0] - 20-May-2022

### Correcciones

-  En Catálogo de Articulo, falla al grabar artículos con factor de ganancia mayor a 9.99. (Issue-998)

## [2022.14.0] - 19-May-2022

### Mejoras

- Caja - Facturar Nota emite CFDI 4.0

- Caja - Factura del Dia emite CFDI 4.0

## [2022.13.0] - 12-May-2022

### Correciones

- Utilería - Importar Información - Importar CFDIs
  - Ahora al mandar a Excel el previo puedes ver el folio de la factura y # de cliente
  - Ahora agrega las facturas en el orden segun fecha de elaboración.
  - Corrección al validar la existencia del cargo en cobranza, siempre lo agregaba.
  - Corrección al importar acentos, se importaban con como '?'

- Correción al timbrar CFDI 4.0 con SW, error: CC3001 schemaLocation no válido

## [2022.11.0] - 5-May-2022

### Mejoras

- Caja - Registrar Ventas, emite Facturas CFDI 4.0 o 3.3

### Correciones

- Inventario - Emitir CFDI de Traslado, al mostrar motivo de Cancelacion ...

- Formato interno de CFDI, abajo mostraba CFDI 3.3 ahora muestra 4.0 o 3.3

## [2022.10.0] - 3-May-2022

### Mejoras

- Ventana de Inventario - CFDI de Traslado - Emitir CFDI
  - Ahora permite enlazar Compra, Orden de Compra, Cotización
  - También permite quitar articulos a un documento enlazado
  - Se mejoró la interfase
- Timbrado CFDI 4.0 [Documentación](/cfdi4/)
  - Catálogo de Clientes, permite capturar el Regimen Fiscal 
  - Catálogo de Clientes Eventuales, permite capturar Regimen Fiscal 
  - Configuracion de Factura Electronica, permite definir timbrado 4.0
  - Ventas - Registro de Ventas, permite timbrar con 4.0
  - Ventas - Devoluciones, permite timbrar con 4.0
  - Aun pendiente en 4.0: REP, Factura Global y Facturar desde Caja

### Correcciones

  - En Ventana de Emitir CFDI de Traslado
    - Considera correctamente el peso de la unidad adicional (957)
    - Columna de peso se bloqueaba despues de emitir un CFDI (1015)

### Notas

[Documentación para timbrado 4.0 de pruebas](/cfdi4/consideraciones-cfdi-4)

[Ajustes de formato para 4.0](/cfdi4/ajuste-de-formatos/)

## [2022.9.0] - 5/Abr/2022 Happy 22th MSL SAIT Anniversary

### Mejoras

- Nueva funcionalidad para Seguimiento a Cancelación de CFDIs [Documentación](https://ayuda.sait.mx//cfdi4/seguimiento-a-cancelacion-de-cfdis/)
  - En Ventas / Consultar CFDI, se agregó el botón de "Cancelaciones" que abre la ventana de "Cancelaciones de CFDI"
  - En la ventana de "Cancelaciones de CFDI" puede ver los CFDIs que fueron mandados cancelar al SAT y aun se encuentran en proceso o fueron rechazados.
  - Al cancelar una factura con status "Cancelabe con Aceptación" se manda la solicitud al SAT, pero la  factura queda vigente en SAIT, hasta que el SAT reporta el CFDI como cancelado. Es decir que el cliente aceptó la cancelacion.
  - Al consultar una factura que fue mandada cancelar en SAT, aparece un boton "Seguimiento a Cancelación"
  - Nueva ventana "Seguimiento a Cancelación de CFDI" muestra como fue cambiando el status de cancelación para ese CFDI en el SAT

### Correcciones

- En Catálogo de Artículos, evitar grabar precios mayores a 9 millones porque generaban error en enlace. (Issue-998)
- Mejor búsqueda de almacenes. (Issue-995)
- En Caja - Registrar Ventas, ya se cosidera correctamente la retención del ISR, para que las peresonas en RESICO, puedan emitir facturas con retención de ISR. (#985)
- Al cancelar facturas que requieren Aceptación del cliente, ahora nos esperamos hasta que efectivamente se muestren canceladas en SAT (#488,#1003)

## [2022.8.0] - 12/Mar/2022

### Mejoras
- Desde consulta de CFDIs de Traslado, se pueden enviar emails masivamente (Issue-982)

## [2022.7.0] - 3/Mar/2022

### Mejoras
- Al emitir CFDI de Traslado, ahora se puede capturar observaciones por partida (Issue-972)

### Correcciones
- Ahora se puede emitir REP con RFC 'XIA190128J61' (Issue-993)
- Al usar SAITSync, al cancelar CFDI de Traslado no se replicaba evento (#988)

## [2022.6.0] - 21/Feb/2022

### Mejoras

- En compras, ahora se puede capturar Retención ISR (Issue-987) [Documentación](/cfdi4/retenciones-en-compras)

## [2022.5.0] - 16/Feb/2022

### Correcciones
- Marcaba error al registrar Gastos con CFDI 4.0 (Issue-979)
- Marcaba error al registrar Compras con CFDI 4.0 (Issue-978)
- Marcaba error al asociar REPs 4.0 a pólizas (Issue-977)

## [2022.4.0] - 15/Feb/2022

### Mejoras
- Al Cancelar CFDI con status 'Cancelable con Aceptación', ahora se iniciara solicitud desde ventana de confirmación (Issue-983)

## [2022.3.0] - 12/Feb/2022

### Mejoras

- Se implemento Cancelaciones de CFDI 2.0 (#973) [Documentación](/cfdi4/cancelaciones2.0/)

### Correcciones
- Al emitir CFDI de Traslado, sí se capturada información de Carta Porte pero no se marcaba casilla 'Incluir Complemento', se guardaba información en base de datos


## [2022.2.0] - 11/Feb/2022

### Correcciones

- Se amplió el tiempo de espera al obtener respuesta en cancelaciones al SAT

- Al emitir CFDI de Traslado, marcaba error ‘Alias CFDITR is not found’ después de varios documentos (Issue-970)

### Mejoras

- Ajustes en permisos al consultar y capturar documentos de compra

- Al emitir CFDI de Traslado, ahora se envía correo automáticamente si esta habilita la configuración ‘Enviar al cliente correo electrónico’ (Issue-971)


## [2022.1.0] - 25/Ene/2022

### Mejoras

- Se habilito captura de un 2do operador para Compl. Carta Porte (Issue-964)

- Al emitir CFDI de Traslado, si existe variable en Config.MSL 'CfdiTrExcluirImpuestos=NO' se incluirán impuestos por concepto en CFDI (Issue-960)

- Al emitir CFDI de Traslado con Compl. Carta Porte, ahora se valida que camión tenga datos de Aseguradora de Medio Ambiente en caso que se requiera
- Nuevos niveles de acceso en compras para consultar y cancelar por tipo de documento (Issue-963)

### Correcciones

- Se realizaron ajustes a formato CFDI de Traslado con Compl. Carta Porte

- Al emitir CFDI de Traslado directo, no borraba correctamente partida de temporal si no existía artículo

- Se vuelve agregar el atributo ViaEntradaSalida a Compl. Carta Porte

- Se realizo corrección al mandar Estado y Ciudad cuando país es diferente a México en Compl. Carta Porte

- Al cancelar salidas por traspaso se vuelve a abrir la tabla Trasp.dbf (#967)

## [2022.0.0] - 17/Ene/2022

### Mejoras

- Al importar artículos desde Excel, enviar por enlace Arts.PJEDESC (Issue-948)

- Se agregó permiso ‘Catálogos de Compl. Carta Porte’ (Issue-954)

- Al emitir CFDI de Traslado, ahora se puede relacionar CFDI cancelado (Issue-956)

- En catálogo de Camiones se puede capturar información de arrendador para Compl. Carta Porte (Issue-947)

- Al emitir CFDI de Traslado con Compl. Carta Porte, ahora se toma dirección de sucursal de cliente como destino (Issue-945)

- Se actualizo catálogo de Regímenes Fiscales (Issue-955)

### Correcciones

- En unidades adicionales de artículos, al capturar margen o factor calcular precio público (Issue-959)

- No permitía emitir CFDI de Traslado a partir de N.V. facturada

- Al consultar reporte de Gastos con comando SQL avanzado, marcaba error

Versiones anteriores

{{% children  %}}

