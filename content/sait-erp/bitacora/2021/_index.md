﻿+++
title = "Año 2021"
weight = 11
+++

Bitácora de mejoras y correcciones para SAIT ERP, Básico y Contabilidad 2021

Versión | Fecha | Descripción
---|---|---
2021.31.0|04.Ene | Mejora. Se agrego permiso 'Cancelar CFDI de Traslado' (#949)
2021.31.0|04.Ene | Mejora. Al emitir CFDI de Traslado con Compl. Carta Porte, ahora no obliga a capturar Remolque
2021.31.0|04.Ene | Mejora. Al emitir CFDI de Traslado con Compl. Carta Porte, ahora no obliga a capturar Clave de Material Peligroso
2021.31.0|04.Ene | Corrección. Ajustes internos a generación a Compl. Carta Porte 2.0
2021.30.0|23.Dic | Mejora. Se permite realizar CFDI de Traslados manualmente (#946)
2021.30.0|23.Dic | Corrección. Al registrar ventas desde Caja, ahora se considera correctamente PjeImp1Sucursal
2021.30.0|23.Dic | Corrección. Se actualizo formato Compl. Carta Porte para mostrar Origen y Destino correctamente
2021.30.0|23.Dic | Corrección. Al validar retiros de efectivo en caja, no tomar en cuenta la venta que se va a procesar en todo el módulo de caja (#940)
2021.29.0|16.Dic | Corrección. Al validar retiros de efectivo en caja, no tomar en cuenta la venta que se va a procesar (#940)
2021.29.0|16.Dic | Mejora. Se actualizaron archivos CartaPorte20 en libxslt
2021.28.0|13.Dic | Mejora. Emitir CFDI de Traslados sobre Nota de Venta (#939)
2021.28.0|13.Dic | Mejora. Al capturar Origen y Destino de Compl. Carta Porte, se obliga a capturar Dirección (#938)
2021.28.0|13.Dic | Mejora. Al emitir Compl. Carta Porte, se valida sí artículo aplica para ser Material Peligroso. 
2021.28.0|13.Dic | Corrección. Ahora se carga Arts.CLAVESAT y Unidades.CLAVESAT antes de emitir CFDI de Traslado y no al cargar folio de documento
2021.28.0|13.Dic | Mejora. Se amplió máscara de totales a millones al recibir pagos en caja (#943)
2021.28.0|13.Dic | Corrección. Número de hoja al imprimir libro mayor de contabilidad (#918)
2021.27.0|03.Dic | Mejora. Se actualizo formato Compl. Carta Porte para mostrar Origen y Destino correctamente
2021.27.0|03.Dic | Mejora. Se agrego generación de eventos para catálogos de Camiones, Operadores y Remolques. Agregar eventos 'CPCamion, CPOperador, CPRemolque' a SAIT Distribuido.
2021.27.0|03.Dic | Mejora. Actualización en reglas de cancelación de CFDI (#919)
2021.27.0|03.Dic | Mejora. Nueva ventana para consultar kardex general de todas las sucursales
2021.27.0|03.Dic | Mejora. Se amplió la descripción del artículo al registrar compras (#931)
2021.27.0|03.Dic | Corrección. Al emitir CFDI de Traslado a sobre Salida por Traspaso o Salida por Sucursal no cargaba Peso de artículo
2021.27.0|03.Dic | Corrección. Al emitir CFDI de Traslado a sobre Factura o Remisión con partidas con observaciones, marcaba error (#936)
2021.27.0|03.Dic | Corrección. Corrección al actualizar base de datos PrecXCli.DBF
2021.26.0|26.Nov | Mejora. Emitir CFDI de Traslados sobre Remisión (#886)
2021.26.0|26.Nov | Corrección. En Carta Porte, se omite atributo Mercancia.Moneda y se corrige detalle con Receptor.RFC
2021.25.0|24.Nov | Mejora. Se incluye Complemento Carta Porte 2.0 en CFDI de Traslados
2021.24.0|22.Nov | Mejora. En Devoluciones de Venta ahora se considera impuesto por sucursal de variable PjeImp1Sucursal  (#927)
2021.24.0|22.Nov | Mejora. Ajustes internos a Complemento Carta Porte 1.0
2021.24.0|22.Nov | Corrección. Al registrar entradas y salidas de inventario, se podía grabar un concepto distinto (#920)
2021.23.0|04.Nov | Mejora. Se incluye Complemento Carta Porte 1.0 en CFDI de Traslados (#802)
2021.22.0|28.Oct | Mejora. En gastos, calcular la fecha de vencimiento en base a los dias de crédito del proveedor (#915)
2021.22.0|28.Oct | Mejora. Grabar la fecha y hora de captura del gasto en Gastos.FCAPT  (#914)
2021.22.0|28.Oct | Mejora. Envío diario de reporte de ventas, grabar la fecha de la ultima consulta de ventas y a partir de ésta se genera la consulta (#908)
2021.22.0|28.Oct | Corrección. Al depurar información de CxC, no se eliminaban correctamente Devoluciones (#909)
2021.22.0|28.Oct | Corrección. Se actualizaron leyendas de medidas en Catálogo de Artículos Web (#916)
2021.21.0|12.Oct | Corrección. Al recibir evento de inventario remoto saitdist se quedaba ciclado (#913)
2021.21.0|12.Oct | Mejora. Aplicación de notas de credito en cuentas por pagar (#494, #678) Tomason
2021.21.0|12.Oct | Mejora. Al definir Precios por Cliente, ahora se permite especificar Unidad, documentación [Aquí ](https://ayuda.sait.mx/sait-basico/8-capacitacion-en-modulo-inventario/v-actualizar-precios-x-cliente/#)
2021.20.0|06.Oct | Mejora. Se amplió máscara de totales en valuación de conteos de inventario (#912)
2021.20.0|06.Oct | Corrección. Al consultar conteos de inventario sin filtro de numalm no mostraba resultado (#910)
2021.20.0|06.Oct | Corrección. En factura del día, convertir a pesos los pagos en dólares al acumular las formas de pago (#904)
2021.20.0|06.Oct | Mejora. No se permite cancelar una salida por traspaso si ya fué aceptada (#891)
2021.20.0|06.Oct | Correccion. Al generar eventos para saitsync de la clasificacion de departamentos no se guardaba bien el campo llave (#33)
2021.19.0|18.Sep | Mejora. Ajustes internos de configuración
2021.18.0|06.Sep | Corrección. En ocasiones, al cancelar REP mostraba el mensaje: Record is out of range (#906)
2021.18.0|06.Sep | Mejora. En cobro de comisión por pago en caja, enviar por enlace el porcentaje de impuesto de COMX (#905)
2021.18.0|06.Sep | Mejora. Al registrar salidas de efectivo en caja, validar que el importe no sea mayor al efectivo disponible (#900)
2021.18.0|06.Sep | Corrección. En registro de ventas, antes de procesar pago se vuelve a validar pago completo (#899)
2021.18.0|06.Sep | Mejora. En búsqueda de series se amplió columna se #Serie para mostrarla completa (#897)
2021.18.0|06.Sep | Mejora. Se agrego siguiente folio a Conteos de Inventario 2.0
2021.18.0|06.Sep | Mejora. Se agrego botón para eliminar Conteos de Inventario 2.0, se debe habilitar permisos 'Eliminar Conteo' y 'Eliminar Conteos Ajustados'
2021.18.0|06.Sep | Corrección. En 'Salidas al inventario' no seleccionaba correctamente # de sucursal si se maneja NumAlm a 3 caracteres
2021.17.0|21.Ago | Mejora. Al enviar reportes a excel, los temporales se crean en c:\sait\temp\ para evitar que se generen en la carpeta de la empresa (#884)
2021.17.0|21.Ago | Corrección. En registro de ventas se realizo ajuste a redondeo para validar cuando usuario cambie de precio (#888)
2021.17.0|21.Ago | Corrección. Ahora solo se permite modificar cotización o pedido si no se ha enlazado o cancelado (#895)
2021.16.0|28.Jul | Corrección. En Visor de OCF no se podían descargar los XML de Nómina (#894)
2021.16.0|28.Jul | Mejora. En ventana de actualizar costos, generar evento ActPrecio (#889)
2021.16.0|28.Jul | Mejora. Al imprimir etiquetas, cargar existencia del almacén activo en lugar de la existencia global (#887)
2021.15.0|05.Jul | Mejora. Al validar escanear código de barras del artículo en cajam se aumentó tiempo de espera a 1 seg si código mide 7 dígitos o más
2021.14.0|02.Jul | Corrección. Al enlazar documentos de venta por periodo, no cargaba correctamente la unidad del articulo en el documento enlazado (#877)
2021.14.0|02.Jul | Corrección. Al generar nota de crédito en aplicación de anticipos, usar impuesto especial para la sucursal (#872)
2021.14.0|02.Jul | Mejora. Permitir visualizar capas disponibles en cotización (#857)
2021.13.0|14.Jun | Mejora. Actualización de ventanas de modificar claves (artículos, clientes, proveedores y artículos masiva) para funcionamiento con SaitSync
2021.13.0|14.Jun | Mejora. Verificar conexion con SaitSync cada 15 segundos, para evitar que se generen eventos si se presentan fallas con el servicio
2021.12.0|12.Jun | Mejora. Mostrar sólo documentos con saldo en estado de cuenta del cliente (#880)
2021.11.0|02.Jun | Mejora. Se permite timbrar CFDIs de prueba con nuevo CSD. Descargarlo [Aquí ](https://sait.mx/download/satcsd/csd_aaa010101aaa.zip) (#873)
2021.11.0|02.Jun | Mejora. Al agregar un cliente, no se permite editar los datos del CFDI si nivel no tiene acceso (#871)
2021.11.0|02.Jun | Mejora. Se acomodó el orden de los controles en ventana de Ventas\Siguientes Folios (#868)
2021.11.0|02.Jun | Corrección. Validaciones al generar sello de Cfdis que contengan caracteres raros (#774)
2021.11.0|02.Jun | Corrección. Al cancelar factura que facturó N.V., no se generaba correctamente evento para devolver N.V. a status disponible
2021.10.0|13.May | Mejora. En 'Compras\Consultas Generales' y 'Compras\Consulta Individual' sólo se permite consultar el tipo de documento al que el usuario tiene acceso
2021.9.0 |28.Abr | Corrección. Validar que no esté vacía la hora de entrega al modificar pedidos (#856)
2021.9.0 |28.Abr | Mejora. Se permite modificar suc. del cliente al modificar pedidos y cotizaciones (#853)
2021.9.0 |28.Abr | Mejora. Se agregó procedimiento de usuario 'VentasModDoc' al modificar pedido o cotización
2021.8.0 |06.Abr | Mejora. Al emitir REP, se ampliaron los decimales al generar TipoCambioDR (#805)
2021.7.0 |31.Mar | Corrección. En Catálogo de Artículos y Servicios, al posicionarse en ubicación y dar clic en grabar se enviaba el cursor a clave del artículo y no guardaba máximo, reorden y mínimo (#844)
2021.7.0 |31.Mar | Corrección. Al enviar estado de cuenta del banco a Excel, no se incluía el tipo y folio de la póliza capturados desde Contabilidad \ Agregar Pólizas (#842)
2021.7.0 |31.Mar | Mejora. Se permite copiar las observaciones de la partida de un documento de venta a un documento de compra (#839)
2021.7.0 |31.Mar | Corrección. Se muestra la ruta correcta para configurar precios por volumen al entrar a la ventana de Precios por Volumen (#838)
2021.7.0 |31.Mar | Corrección. Al cargar CFDI a Compra, no te actualizada la divisa automáticamente (#497)
2021.7.0 |31.Mar | Corrección. Cambios al generar índices de Entradas.DBF (#845)
2021.7.0 |31.Mar | Corrección. Al generar en REP con SPEI marcaba error
2021.6.0 |02.Mar | Mejora. Inicializar cliente después de procesar un pedido en registro de ventas y no desea agregar anticipo (#837)
2021.6.0 |02.Mar | Mejora. Al registrar ventas en Caja considerar impuesto definido para el cliente (#836)
2021.6.0 |02.Mar | Mejora. En Catálogo de Artículos, opción 'Mostrar en Tienda Web' se envía por enlace (#821)
2021.6.0 |02.Mar | Mejora. Al crear un conteo a puertas abiertas ahora se puede crear por ubicación (#778)
2021.6.0 |02.Mar | Corrección. Al manejar comisión por X tipo de pago en caja, al abonar pedidos reflejaba mal el saldo (#816)
2021.6.0 |02.Mar | Corrección. Al facturar desde caja marcaba error "Alias is not found"
2021.6.0 |02.Mar | Corrección. Al emitir REP, marcaba error 'No se encontro el archivo en la tabla SPEIS'
2021.5.0 |10.Feb | Mejora. Al reprocesar movimientos de los artículos, volver a calcular el costo promedio del almacén (#825)
2021.4.0 |09.Feb | Corrección. Al calcular costo promedio del almacén se agregó validación para que costo no sea negativo o mayor a 99,999,999 (#824)
2021.3.0 |05.Feb | Mejora. Se cambió leyenda: Fecha fuera de período. Se agregó la ruta para cambiar la fecha (#828)
2021.3.0 |05.Feb | Mejora. Al imprimir etiquetas en compras, se permite capturar más de 20 registros de forma manual (#827)
2021.3.0 |05.Feb | Mejora. Se amplió la columna de FOLIO en la ventana de pagos de contado en caja F12 (#822)
2021.3.0 |05.Feb | Mejora. Al registrar ventas en caja, se modificó la forma de mostrar mensaje cuando el artículo no existe (#817)
2021.3.0 |05.Feb | Mejora. En catálogo de articulos actualizar el nombre del almacén activo en captura de máximos y mínimos
2021.2.0 |22.Ene | Corrección. Al calcular IVA de articulos con IEPS de combustibles
2021.2.0 |22.Ene | Mejora. Cargar artículos desde la lectora en registro de ventas F4
2021.1.0 |06.Ene | Mejora. En catálogo de articulos se permite capturar máximos y mínimos de la sucursal activa
2021.1.0 |06.Ene | Mejora. Al ver año completo en captura de TC Oficial sólo se permite consultar, no editar los TC
2021.0.0 |05.Ene | Corrección. En capturar TC Oficial se quitó validación que año estuviera entre 1999 y 2020 (#813)
2021.0.0 |05.Ene | Mejora. Al aceptar traspasos, en validación de no duplicar series se considera el número de almacén (#812)
2021.0.0 |05.Ene | Mejora. Se modificó la forma de mostrar mensaje de vencimiento de CSD para no bloquear dbfs (#809)
2021.0.0 |05.Ene | Corrección. Al devolver factura de cliente eventual, no se timbraba con RFC correcto (#808)

{{% children  %}}
