﻿+++
title = "Año 2020"
weight = 12
+++

Esta es la bitácora de las mejoras y correcciones realizadas a SAIT ERP, Básico y Contabilidad durante 2020

Versión | Fecha | Descripción
---|---|---
2020.30.0 |23.Dic | Mejora. Cambios en consulta de status de cancelacion de cfdis
2020.29.0 |15.Dic | Mejora. Cambios internos al generar eventos para SaitSyc
2020.28.0 |01.Dic | Mejora. Al registrar entradas de inventario, se valida que no se repitan los numeros de serie en el mismo documento (#798)
2020.28.0 |01.Dic | Mejora. En consulta general de ventas y compras, se agregó columna de tipo de cambio al enviar a excel la consulta (#797)
2020.28.0 |01.Dic | Corrección. Al verificar saldos de proveedores, verificaba de forma incorrecta la aplicación de notas de crédito a otros documentos (#796)
2020.28.0 |01.Dic | Mejora. Se permite configurar los decimales a utilizar en porcentaje de descuento para compras
2020.27.0 |24.Nov | Corrección. Se corrige error al timbrar con 2 o más porcentajes de retención distintos por artículo (#799)
2020.26.0 |03.Nov | Corrección. En consulta general de gastos no se mostraban gastos con folio el minúscula (#795)
2020.26.0 |03.Nov | Mejora. En registro de ventas al presionar F11 se muestra la existencia del almacén activo (#792)
2020.26.0 |03.Nov | Mejora. En catálogo de artículos se muestra una alerta al modificar la unidad de medida informando que puede haber problemas al cancelar mov. (#790)
2020.26.0 |03.Nov | Corrección. Se actualizó enlace para obtener T.C. por internet (#786)
2020.26.0 |03.Nov | Mejora. Al registrar Ordenes de Compra y Compras se puede capturar comentarios con F5 (#754)
2020.25.0 |16.Oct | Corrección. RIF la factura global no convertía a pesos las notas de venta en dólares (#788)
2020.25.0 |16.Oct | Mejora. Al cancelar ventas, compras y mov. al inv. se valida que realice en la sucursal y/o almacenes adicionales (#787)
2020.25.0 |16.Oct | Corrección. Al registras salidas de inventario, antes de procesar se vuelve actualizar el siguiente folio en caso de que ya exista (#785)
2020.25.0 |16.Oct | Mejora. En consulta general de ventas y compras se muestra la clave del cliente o proveedor (#784)
2020.25.0 |16.Oct | Corrección. Al cancelar una devolución, ahora se generan eventos para actualizar status de documento devuelto (#765)
2020.25.0 |16.Oct | Corrección. En consulta de CFDIs, al seleccionar 'Imprimir todos' marcaba error con facturas globales
2020.25.0 |16.Oct | Corrección. En consulta de Series, se corrige detalle al filtrar por almacén
2020.24.0 |02.Oct | Corrección. Fallas de timbrado por diferencia de centavo con IEPS (#675 y #781)
2020.23.0 |29.Sep | Mejora. Al agregar gastos, se valida que el CFDI sea Ingreso o Egreso (#779)
2020.23.0 |29.Sep | Mejora. En registrar pagos a proveedores, cargar la referencia del folio de factura desde el estado de cuenta (#777)
2020.23.0 |29.Sep | Mejora. Permitir venta por debajo del precio mínimo cuando el artículo tiene promociones o descuentos (#742)
2020.23.0 |29.Sep | Mejora. Al cancelar una devolución de compra, ahora se genera evento por enlace de sucursales. Agregar evento 'CancDC' a SAIT Distribuido (#392)
2020.23.0 |29.Sep | Mejora. En Inventarios a puertas abiertas, antes de cerrar un conteo ahora se actualizara ventana antes de procesar (#783)
2020.22.0 |05.Sep | Mejora. Se permite emitir la factura global por mes
2020.21.0 |04.Sep | Mejora. No se permite seleccionar numeros de serie de artículos en cotizaciones/pedidos (#764)
2020.21.0 |04.Sep | Mejora. Al hacer devoluciones de venta directas (sin devolver documento) se permite capturar los números de serie de los artículos (#761)
2020.21.0 |04.Sep | Mejora. En registro de ventas.  validar que el precio no sea negativo antes de procesar (#758)
2020.21.0 |04.Sep | Mejora. Ventana registrar ventas en caja no se permite cambiar de tamaño (#756)
2020.21.0 |04.Sep | Mejora. Mostrar límite de crédito en estado de cuenta del cliente (#751)
2020.21.0 |04.Sep | Mejora. En consulta individual de movimientos, mostrar columna referencia capturada al registrar entradas de inventario.
2020.21.0 |04.Sep | Corrección. En Visor OCF, folios mayores a 10 dígitos se mostraban incorrectamente (#752)
2020.21.0 |04.Sep | Corrección. Al Asociar REPs de Proveedores a Pólizas marcaba error si no existía el atributo 'ImpPagado'
2020.20.0 |28.Jul | Mejora. Al facturar desde caja, ahora se anexa a CFDI las observaciones por partida (#753)
2020.20.0 |28.Jul | Mejora. Al comprar/vender se valida que los números de serie capturados no sean menor que la cantidad capturada (#748)
2020.20.0 |28.Jul | Corrección. Alinear correctamente la clave del departamento al importar artículos desde Excel (#746)
2020.20.0 |28.Jul | Mejora. Se optimizó envío asíncrono del Reporte de Artículos con Existencia Mínima
2020.20.0 |28.Jul | Mejora. Se optimizó envío asíncrono del Reporte Diario de Ventas
2020.20.0 |28.Jul | Mejora. Se optimizó envío asíncrono de Alertas por Baja disponibilidad de Timbres Fiscales
2020.19.0 |21.Jul | Mejora. Envío de Reporte de Artículos con Existencia Mínima, documentación [Aquí ]( https://ayuda.sait.mx/sait-erp/8-capacitacion-en-modulo-inventario/u-recibir-reporte-articulos-con-stock-menor-al-minimo/#)
2020.19.0 |21.Jul | Mejora. Envío de Reporte Diario de Ventas, documentación [Aquí ](https://ayuda.sait.mx/sait-erp/10-capacitacion-modulo-de-ventas/m-envio-automatico-de-ventas-al-correo/)
2020.19.0 |21.Jul | Mejora. Envío de Alertas por Baja disponibilidad de Timbres Fiscales, documentación [Aquí ](https://ayuda.sait.mx/sait-erp/4-configuracion-de-empresa/d-consultar-timbres-disponibles/#)
2020.19.0 |21.Jul | Mejora. Al registrar cargos en cobranza, se valida el límite de crédito y saldo disponible del cliente (#740)
2020.19.0 |21.Jul | Mejora. En consulta general de movimientos de banco, se permite consultar por tipo de póliza (#734)
2020.18.0 |04.Jul | Mejora. Al generar póliza de ingresos de cobranza, validar que la fecha capturada sea igual a los pagos (#741)
2020.18.0 |04.Jul | Corrección. En caja no se actualizan los precios al cambiar de cliente (#726)
2020.18.0 |04.Jul | Mejora. Al devolver factura enlazada a una remision con kits se devuelven los articulos del kit (#291)
2020.18.0 |04.Jul | Mejora. Se actualizaron archivos libxslt para optimizar opción 'Enviar Contabilidad al SAT'
2020.18.0 |04.Jul | Corrección. Ahora al generar indices se incluye SolCanc.dbf
2020.18.0 |04.Jul | Corrección. Al imprimir una factura global sin timbrar marcaba error 'Unknown member oCfdi33print' (#727)
2020.17.0 |16.Jun | Corrección. En registrar pagos en cobranza, si no existe el concepto CH en cobranza se cargaba en blanco el tipo de movimiento (#725)
2020.17.0 |16.Jun | Corrección. En registro de ventas, al cambiar el tipo de documento mostraba XXXXX en divisa (#288)
2020.17.0 |16.Jun | Mejora. Nuevo método de usuario en catálogo de clientes: ClientesPreValida. Utiliza la propiedad _Screen.lValidaCliente para determinar si pasó la validación
2020.17.0 |16.Jun | Corrección. Al generar una consulta desde 'Reportes de Inventario' no filtraba correctamente por Sucursal
2020.16.0 |10.Jun | Corrección. Al facturar N.V. o generar factura global marcaba error "Alias is not found"
2020.15.0 |02.Jun | Mejora. Al elaborar cheque, mostrar clave del beneficiario, cargar el beneficiario seleccionado (#582)
2020.15.0 |02.Jun | Corrección. En reportes detallados de ventas, al habilitar el campo de Suc. Cliente en el Sql marcaba error (#574)
2020.14.0 |27.May | Mejora. En consulta general de CFDIs de pagos se agregaron los totales por divisa, al enviar a excel se agregó columna de divisa (#721)
2020.14.0 |27.May | Mejora. Al enviar a Excel la consulta general de ventas, mostrar en cero los documentos devueltos (#716)
2020.14.0 |27.May | Mejora. En registro de compras, mostrar existencia del almacén activo. Si no tiene existencia, mostrar cero. (#715)
2020.14.0 |27.May | Mejora. Al eliminar pagos en cobranza, si hay otros pagos con el mismo concepto y folio muestra aviso para verificar si deben eliminarse (#714)
2020.14.0 |27.May | Correción. Al modificar clientes ahora se aplica validación para no repetir RFC (#722)
2020.13.0 |18.May | Correción. Se corrige envio de eventos de catalogo de niveles por el Sync
2020.13.0 |18.May | Mejora. Se permite enviar Catálogo de Beneficiarios por enlace de sucursales. Agregar eventos 'ModBenef' y 'DelBenef' a SAIT Distribuido (#558)
2020.13.0 |18.May | Mejora. Ahora se permite aplicar anticipos y N.C. desde Cobranza / Abonos (#712)
2020.13.0 |18.May | Correción. Búsqueda de emisores en OCF entraba en conflicto al obtener T.C. por internet (#717)
2020.12.0 |11.May | Mejora. Se puede configurar IVA 0% por Proveedor, colocando 0.01 en el porcentaje desde el catalogo (#713)
2020.12.0 |11.May | Mejora. Al enviar correo a cliente eventual, ahora se carga automáticamente su correo (#626)
2020.11.0 |05.May | Mejora. Al cancelar un movimiento de inventario, ahora se valida que pertenezca al inventario activo (#711)
2020.10.0 |04.May | Mejora. Se incluye consulta 'Solicitudes de Cancelación' (#488)
2020.9.0 |04.May | Mejora. En iconos de acceso rápido, preguntar si desea cambiar de empresa, almacén, usuario y salir del sistema (#710)
2020.9.0 |04.May | Mejora. No se permite cotizar articulos inactivos (#707)
2020.9.0 |04.May | Corrección. Al verificar saldos de clientes, se verifica correctamente la aplicación de notas de crédito directas (sin refer. de doc. devuelto) (#706)
2020.9.0 |04.May | Mejora. Validar lista de precios mínimos permitidos de vendedores en caja (#704)
2020.9.0 |04.May | Mejora. Al entregar apartado, se permitir seleccionar lote y serie de los artículos (#643)
2020.9.0 |04.May | Corrección. En registro de ventas, al pedir pago en notas y facturas se quedaba activada la ventana de selecció de destino y no se podía cerrar (#529)
2020.9.0 |04.May | Mejora. En proceso de toma de inventario, al ajustar existencias se muestra la divisa del artículo y totales en pesos y dólares (#472)
2020.8.0 |03.Abr | Mejora. Al relacionar CFDIs a facturas, se valida que corresponda al receptor (#703)
2020.8.0 |03.Abr | Mejora. En caja, se permite devolver cambio de todas las formas de pago marcando la casilla 'Es Movimiento en Efectivo' (#693)
2020.8.0 |03.Abr | Corrección. Al utilizar Precios X Volumen, ahora se realiza el calculo de precio unitario correctamente (#705)
2020.8.0 |03.Abr | Corrección. En emisión de REP, no tomar Aplicación de N.C. como número de parcialidad (#698)
2020.8.0 |03.Abr | Corrección. No te permitía agregar el primer pago a un apartado cuando se utiliza cobro de comisión por forma de pago (#687)
2020.8.0 |03.Abr | Corrección. Fallaba al cancelar devoluciones de facturas a crédito (#682)
2020.8.0 |03.Abr | Mejora. Al aplicar anticipos, se usa el porcentaje de Iva del concepto APLANTICIPO (#702)
2020.8.0 |03.Abr | Corrección. Se permite seleccionar cuenta bancaria del proveedor al elaborar cheque parcial de reposición de caja (#700)
2020.8.0 |03.Abr | Corrección. Al cambiar claves de artículos y unir movimientos, se duplicaba la información en artículos sustitutos (#696)
2020.8.0 |03.Abr | Corrección. Totales incorrectos al entregar apartados en registro de ventas (#695)
2020.8.0 |03.Abr | Mejora. Al aplicar un anticipo se muestra la ventana "Información del CFDI" para poder seleccionar el método y forma de pago (#639)
2020.8.0 |03.Abr | Mejora. Se creó nuevo nivel de acceso para eliminar proveedores (#481)
2020.8.0 |03.Abr | Mejora. Se creó nuevo nivel de acceso para eliminar articulos (#465)
2020.8.0 |03.Abr | Mejora. Al registrar abonos / pagos de los clientes, actualizar el siguiente folio automáticamente (#591)
2020.8.0 |03.Abr | Mejora. Agregue método de usuario ComprasModOrden para desarrollos especiales al modificar órdenes de compra (#535)
2020.7.0 |12.Mar | Mejora. Se agrego opción en menú Contabilidad \ Asociar REPs de Proveedores a Pólizas
2020.6.0 |11.Mar | Mejora. Al modificar capa desde kardex del artículo, se envía la información por el enlace de sucursales (#691)
2020.5.0 |10.Mar | Mejora. En menu de artículos sustitutos en registro de ventas se muestra la existencia global y la del almacén activo (#690)
2020.5.0 |10.Mar | Mejora. Se agrego asociación masiva de REPs de proveedores a cheques póliza.
2020.4.0 |06.Mar | Mejora. A generar REP a Terceros, se puede agrupar por Forma de Pagos.
2020.4.0 |06.Mar | Mejora. A generar REP a Terceros, se puede relacionar un REP cancelado (#686)
2020.3.0 |02.Mar | Mejora. Al generar formato 29 para DIOT, en períodos anteriores a 2019 se envía en blanco la columna de Gravado 8% y la columna IVA No Acreditable 8%
2020.3.0 |02.Mar | Mejora. Al editar costos de los artículos desde Inventario \ Actualizar Precios y Costos \ Actualizar Costos se envía la información por el enlace de suc. (#688)
2020.3.0 |02.Mar | Mejora. En Ventas F4, se optimizo proceso de cancelación de documentos (#682)
2020.3.0 |02.Mar | Mejora. En Caja, se aplica validación 'Detener si precio es menor al costo + % de manejo' (#600)
2020.3.0 |02.Mar | Mejora. En Compras, al cargar CFDI y relacionar orden de compra ahora se cargan descuentos por partidas (#594)
2020.3.0 |02.Mar | Corrección. Al facturar nota de venta, antes de procesar se vuelve a validar que esté disponible y no se haya facturado previamente (#680)
2020.3.0 |02.Mar | Corrección. En Ventas F4, al enlazar documentos se valida correctamente la cantidad disponible (#683)
2020.3.0 |02.Mar | Corrección. En Ventas F4, al mostrar saldos de clientes en billones, se mostraban asteriscos (#679) 
2020.3.0 |02.Mar | Corrección. Al modificar clave de clientes, ahora se actualiza catálogo de Sucursales de Clientes (#677)
2020.3.0 |02.Mar | Corrección. En Ventas F4, al mostrar lista de precios de artículos ahora se considera porcentaje de IEPS (#674)
2020.3.0 |02.Mar | Corrección. Al procesar una devolución de ventas, la clave de autorización se generaba incorrectamente (#671)
2020.3.0 |02.Mar | Corrección. En Ventas F4, al copiar documento marcaba error 'obtenerDescuentoPorCliente.prg does not exist.' (#665)
2020.3.0 |02.Mar | Corrección. En Ventas F4, al usar la función de F2 para calcular precio público ahora se considera porcentaje de IEPS (#570) 
2020.3.0 |02.Mar | Corrección. Al modificar asientos contables de gasto por proveedor, no se visualizaba la sección de 'Datos a Capturar' (#548)
2020.2.0 |20.Feb | Mejora. Se permite enviar MInv.DISP y Entradas.DISP por enlace de sucursales. Agregar evento 'ActDisp' a SAIT Distribuido
2020.2.0 |20.Feb | Mejora. Se permite enviar registros de Entradas.dbf por enlace de sucursales. Agregar evento 'Entradas' a SAIT Distribuido
2020.2.0 |20.Feb | Mejora. Ahora las compras remotas afectan inventario correctamente
2020.2.0 |20.Feb | Mejora. En catálogo de artículos, los costos se muestran con hasta 5 decimales según configuración de precios (#673)
2020.2.0 |20.Feb | Mejora. En precios por: zona, cliente y sucursal del cliente se muestran hasta 5 decimales según configuración de precios (#667)
2020.1.0 |27.Ene | Mejora. Nuevo proceso para modificar masivamente las claves de los artículos desde Utilerías \ Modificar Claves \ Artículos
2020.1.0 |27.Ene | Mejora. Agregada compatibilidad con nuevos modelos de lector de huella digital, Modelo U.are.U 5300, documentación.
2020.1.0 |27.Ene | Mejora. Nueva versión de utileria Freader.
2020.0.0 |21.Ene | Mejora. Antes de emitir REP se valida que al menos seleccionen un pago (#661)
2020.0.0 |21.Ene | Mejora. Al imprimir factura global, se tomara formato interno (#627)
2020.0.0 |21.Ene | Mejora. Se agregó nuevo permiso en catálogo de niveles, para obligar a capturar RFC en catálogo de clientes
2020.0.0 |21.Ene | Corrección. Al imprimir o mandar por correo CFDI, mandaba dañado PDF dependiendo del nombre del archivo (#657)
2020.0.0 |21.Ene | Corrección. En devoluciones de venta en efectivo, grabar el número de cliente en el movimiento de devolución en caja (#641)
2020.0.0 |21.Ene | Corrección. En registro de venta y salidas al inventario, si el artículo usa series la cantidad debe ser mayor a 1 (#638)
2020.0.0 |21.Ene | Corrección. En devoluciones de venta, en caso de traer una remision enlazada usar el costo de salida de la remisión (#615)
2020.0.0 |21.Ene | Corrección. Al enlazar un documento en registro de ventas, antes de procesar se valida que las partidas sigan disponibles (#583)
2020.0.0 |21.Ene | Corrección. Al devolver facturas, no se incluyen en el Xml artículos sin precio (Marcaba error de timbrado) (#230)

{{% children  %}}
