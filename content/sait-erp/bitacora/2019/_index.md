﻿+++
title = "Año 2019"
weight = 13
+++

Esta es la bitácora de las mejoras y correcciones realizadas a SAIT ERP, Básico y Contabilidad durante 2019

Versión | Fecha | Descripción
---|---|---
2019.32.0 |26.Dic | Corrección. Al entregar apartados no se calculaba correctamente importe de IVA  (#663)
2019.31.0 |20.Dic | Mejora. Se agrego buscador de REPs al asociar CFDIs a cheques póliza
2019.30.0 |04.Dic | Mejora. Al configurar enviar remisiones a cobranza, grabar fecha pago de remisiones a crédito (#656)
2019.30.0 |04.Dic | Mejora. Al cancelar devoluciones de compra, pedir motivo de cancelación y grabarlo (#655)
2019.30.0 |04.Dic | Corrección. En devoluciones de compra, usar la configuración de decimales en precio de compras (#654)
2019.30.0 |04.Dic | Mejora. Al emitir REP y enviar a Excel la consulta, se incluye la columna MP (#642)
2019.30.0 |04.Dic | Corrección. Fallas de timbrado cuando hay descuentos del 100% (#637)
2019.30.0 |04.Dic | Mejora. Al abrir ventana de elaborar cheque, verificar si el folio ya existe y validar que sea del mismo proveedor (#634)
2019.29.0 |26.Nov | Mejora. Se optimizo cálculo de totales en Ventas F4 y en Caja para que concuerden cuando se usa Precio Publico (#636)
2019.29.0 |26.Nov | Corrección. Al timbrar REP que incluía pago en pesos con facturas en dólares, marcaba error con decimales (#653)
2019.29.0 |26.Nov | Corrección. En Ventas F4, al enlazar una cotización o remisión con vendedor, marcaba error 'obtenerDescuentoPorCliente.prg does not exist.' (#650)
2019.29.0 |26.Nov | Corrección. Ajustes al cancelar documentos de venta para evitar duplicidad en la cancelación (#635)
2019.29.0 |26.Nov | Corrección. En conteos de inventario, al ver el detalle del conteo se desagrupaban las ubicaciones (#633)
2019.29.0 |26.Nov | Corrección. Al facturar N.V. se presentaban diferencias en decimales contra la factura emitida (#632)
2019.28.0 |04.Nov | Corrección. Al capturar Impuestos Locales desde Ventas F4, no te dajaba capturar solamente retenciones
2019.28.0 |04.Nov | Corrección. Al buscar RFC en OCF desde 'Buscar Emisor', fallaba al buscar vacios (#625)
2019.28.0 |04.Nov | Corrección. Al cargar una compra a partir de un CFDI 3.2, marcaba error con variable 'oDescuento' (#620)
2019.28.0 |04.Nov | Corrección. En ventana de configuración de SAIT SYNC corregido error al registrar IP local.
2019.27.0 |07.Oct | Corrección. Fallas de timbrado por diferencia de centavo en total de impuestos trasladados y total de retenciones (#580 y #547)
2019.27.0 |07.Oct | Mejora. Avisar retiros de efectivo en caja sólo cuando se hayan recibido pagos en el corte (#603)
2019.27.0 |07.Oct | Mejora. Se optimizó la generación de índices de CFDIAAMM.dbf
2019.27.0 |07.Oct | Corrección. Al imprimir REP, se imprimía incorrectamente número de paginas (#614)
2019.26.0 |25.Sep | Mejora. Ahora se permite actualizar Precios por Cliente por Porcentaje
2019.26.0 |25.Sep | Mejora. Se amplió el campo para la captura de la llave de acceso a bóveda para el envío de cfdis emitidos (#618)
2019.26.0 |25.Sep | Mejora. Se permite emitir REP por cliente aún cuando haya pagos con cfdis relacionados (#617)
2019.26.0 |25.Sep | Mejora. Mostrar la existencia del almacén activo en artículos sustitutos (#616)
2019.26.0 |25.Sep | Mejora. En consulta general de ventas y compras mostrar en rojo documentos cancelados (#613)
2019.26.0 |25.Sep | Mejora. Nuevos métodos de usuario en consulta individual de movs. al inventario para desarrollos especiales: InventarioConsMinvInit, InventarioConsMinvInitDoc, InventarioConsMinvNumDocValid
2019.26.0 |25.Sep | Corrección. Al modificar Pedido o Cotización, no guardaba Forma de Pago nueva (#604) 
2019.26.0 |25.Sep | Corrección. En ventana de comandos de VFP, se mostraba erroneamente tabla UserLogOout.DBF (#602)
2019.26.0 |25.Sep | Corrección. Desde Ventas F4, no permitía cancelar documentos a crédito si estaban incluidos en un corte de caja
2019.25.0 |20.Sep | Corrección. En devoluciones directas de venta, usar el costo del artículo según el tipo de costeo a usar (último costo/costo promedio) (#612)
2019.25.0 |20.Sep | Mejora. En catálogo de artículos y kardex se muestra la existencia total con decimales (#611)
2019.25.0 |20.Sep | Mejora. En entradas al inventario, se muestra el costo del artículo según tipo de costeo a usar (último costo/costo promedio) (#610)
2019.25.0 |20.Sep | Mejora. Al aceptar transferencias de inventario: Se muestran las observaciones de la partida, valida que los artículos existan en la sucursal (#609)
2019.25.0 |20.Sep | Mejora. En consulta general de movimientos de bancos, se muestra el cheque-póliza al hacer doble clic o dar enter (#608)
2019.25.0 |20.Sep | Mejora. Cambios en consulta general de pólizas: se agregó botón para buscar cfdis, mostrar el cheque-póliza al hacer doble click o dar enter (#607)
2019.25.0 |20.Sep | Mejora. En consulta general de gastos, se muestra el uuid del comprobante y permite consultar por cfdis asociados o sin asociar (#606)
2019.25.0 |20.Sep | Mejora. Poder asociar Cfdi en gastos en caso de estar vacío (#605)
2019.24.0 |06.Sep | Mejora. Se optimizó la forma de generar el PDF de facturas y notas de crédito.
2019.24.0 |06.Sep | Mejora. Nuevo proceso para asociar Cfdis a polizas y cheques desde la consulta general de pólizas (#470)
2019.24.0 |06.Sep | Mejora. Cambios en ventana de buscar Cfdis: se agregaron nuevos filtros a la consulta para optimizar las búsquedas.
2019.24.0 |06.Sep | Mejora. Cambios en consulta general de pólizas: se muestran los tipos de cfdis asociados a la poliza (Ingreso, Egreso, Pago, Nómina, Traslado)
2019.24.0 |06.Sep | Corrección. Ya no se permite abonar a un pedido cancelado (#599)
2019.24.0 |06.Sep | Corrección. Al realizar devolución de compra, no devolvía el lote de la compra en cuestión (#597)
2019.24.0 |06.Sep | Mejora. Se optimizo los redondeos realizados en Elaboración de Cheques (#592)
2019.24.0 |06.Sep | Mejora. En visor OCF, se agrego filtro por UUID, Tipo de CFDI y Método de Pago
2019.24.0 |06.Sep | Mejora. Se optimizo redondeo de T.C. inverso al mezclar divisas en REPs
2019.24.0 |06.Sep | Mejora. Se actualizo la descarga automática de las Cuentas Contables versión Agosto 2019
2019.24.0 |06.Sep | Corrección. Al utilizar descuentos por clientes con versión de promociones básica marcaba error (#588)
2019.24.0 |06.Sep | Corrección. Al eliminar partidas desde Caja marcaba error 'Subscript is outside defined range' (#586)
2019.24.0 |06.Sep | Corrección. Al exportar reportes marcaba error 'File getCursorPos does not exist' (#585)
2019.23.0 |06.Sep | Mejora. Se permite configurar cierre de sesión a usuarios por exceso de inactividad
2019.23.0 |24.Jul | Corrección. Al importar desde Excel los campos Clientes.NUMZONA y Clientes.NUMVEND no se alineaban correctamente (#579)
2019.23.0 |24.Jul | Corrección. No se actualizaba el campo Docum.MOSTRADOR al modificar cotizaciones y pedidos (#576)
2019.23.0 |24.Jul | Mejora. En consultas generales de ventas, se muestra ZZZZZZZZZZ en rango final de folios para incluir todos los folios alfanuméricos (#573)
2019.23.0 |24.Jul | Mejora. En Caja / Registrar Ventas, se muestra en azul los artículos con promoción (#569)
2019.23.0 |24.Jul | Mejora. No considerar importe de devoluciones en el total de la consulta general de ventas (#568)
2019.23.0 |24.Jul | Mejora. Se aplica validación de pedir autorización si vendedor bajó precio en caja (#485)
2019.23.0 |24.Jul | Correccion. Al aplicar descuentos globales y descuentos por promoción en Caja / Registrar ventas.
2019.22.0 |05.Jul | Mejora. Ajustes en autorizaciones al bajar precios/descuentos desde Ventas / Registro de Ventas.
2019.21.0 |02.Jul | Mejora. Cambios en catalogo de clientes para tienda web.
2019.20.0 |25.Jun | Mejora. Se actualizó el catálogo de clasificación de cuentas contables y Bancos del SAT (#545)
2019.20.0 |25.Jun | Mejora. Al aceptar transferencias, se abria varias veces la ventana del detalle de la transferencia (#566)
2019.20.0 |25.Jun | Mejora. Se agregó filtro de sucursal en reportes de compras (#561)
2019.20.0 |25.Jun | Corrección. Al definir Mínimo, Reorden y Máximo de otras sucursales, no se iva por el enlace (#560)
2019.20.0 |25.Jun | Corrección. Al importar articulos desde Excel, los campos Mínimo, Máximo y Reorden no se afectaban correctamente en el almacen activo (#557)
2019.20.0 |25.Jun | Mejora. En Cobranza\Actualizar vencimientos mostraba asteriscos con importes mayores a 999,999.99 (#550)
2019.20.0 |25.Jun | Corrección. Error Alias 'OTRASVALOR' is not found al registrar un gasto con articulos exentos a partir del XML (#543)
2019.20.0 |25.Jun | Mejora. En el concepto de la póliza se permite capturar hasta 100 caracteres (Siempre y cuando el campo esté ampliado) (#460)
2019.20.0 |25.Jun | Mejora. En grupos de usuarios, se agregó el permiso 'Modificar ordenes de compra' (#522)
2019.20.0 |25.Jun | Corrección. Error "Property LTRASAUTSS is not found" al seleccionar opción registrar salidas de inventario (#555)
2019.20.0 |25.Jun | Corrección. Al capturar descuento global a venta desde caja ahora se aplica correctamente
2019.19.0 |17.Jun | Mejora. Al registrar ventas desde caja, se permite capturar descuento global
2019.19.0 |17.Jun | Mejora. Al cargar CFDIs en Compras se  cargan los descuentos del XML. 
2019.18.0 |30.May | Mejora. Al importar artículos desde Excel y al reenviar información de artículos ahora se actualizan Precios por Volumen en enlace de sucursales (#549)
2019.18.0 |30.May | Corrección. Ajustes en factura del día por diferencias de centavos (#547)
2019.17.0 |29.May | Corrección. Diferencia de centavos al elaborar factura del día (#547)
2019.17.0 |29.May | Corrección. Al generar CFDI de Traslados de artículos con pedimentos de importación marcaba error (#540)
2019.17.0 |29.May | Corrección. Ventana de búsqueda de artículos por clave/descripción en ocasiones perdía el focus y no se podía cerrar (#539)
2019.16.0 |24.May | Mejora. Se optimizó la búsqueda de proveedores, se permite configurar la búsqueda.
2019.16.0 |24.May | Corrección. En registro de ventas, no cargaba el porcentaje de IVA definido para la sucursal
2019.16.0 |24.May | Corrección. Al cancelar salidas de inventario no se regresaba el pedimento a la capa disponible (#526)
2019.15.0 |21.May | Mejora. Se optimizo cálculo de totales al incluir descuentos (#192)
2019.15.0 |21.May | Mejora. Al definir promociones, se permite capturar el proveedor relacionado a los artículos (#500)
2019.15.0 |21.May | Mejora. En registro de ventas, al consultar F6-Distribución en Almacenes se permite configurar los almacenes a mostrar.
2019.15.0 |21.May | Mejora. En reportes de artículos, si la empresa maneja almacenes la ubicación que se usa es la del almacén activo (#527)
2019.15.0 |21.May | Mejora.	Al cargar CFDIs en Compras se  cargan los impuestos del XML. 
2019.15.0 |21.May | Mejora. En Registrar entradas al inventario al cambiar divisa se recalculan costos (#265)
2019.15.0 |21.May | Corrección. Al actualizar precio por zona, cliente o sucursal de cliente, no borraba precios anteriores (#538)
2019.14.0 |14.May | Mejora. Cargar la forma de pago del CFDI que tiene asignado el cliente sólo en facturas de contado (#531)
2019.14.0 |14.May | Corrección. En registro de ventas, al cambiar de cliente y éste tiene capturado el porcentaje de IVA no volvía a cargar el IVA en los artículos capturados (#480)
2019.14.0 |14.May | Mejora. En nota de crédito del anticipo, además de relacionar la factura de la venta del bien, relacionar también la factura del anticipo (#342)
2019.14.0 |14.May | Mejora. Al emitir REP a tercero con Documento Relacionados se tomara solo UUID del D.R. al primer pago de la lista.
2019.14.0 |14.May | Mejora. En cuentas bancarias de dólares, poder agregar claves interbancarias mayores a 18 dígitos (#489)
2019.14.0 |14.May | Mejora. Dentro de 'Configurar CFDI', se incluye opción 'Generar CFDI de Traslado al registrar salidas a sucursal' (#280)
2019.14.0 |14.May | Mejora. Se agregaron nuevas opciones a la ventana de configuración de boveda.
2019.14.0 |14.May | Corrección. Al realizar cancelación de Salida a Sucursal, ahora se registra correctamente el movimiento de cancelación en Kardex (#518)
2019.14.0 |14.May | Corrección. Dentro de 'Registrar Ventas F4', en 'Precio Neto' se mostraban asteriscos en cantidades mayores a 9,999,999.99 (#517)
2019.14.0 |14.May | Corrección. Al modificar formato de 'Libro Diario' marcaba error (#515)
2019.14.0 |14.May | Corrección. En ventana Ventas / Consulta CFDI emitidos, al generar zip los pares de archivos tendran el mismo nombre en minuscula.
2019.14.0 |14.May | Corrección. En ventana Cobranza / Abonos, marcaba error al emitir varios REP seguidos de forma automatica.
2019.14.0 |14.May | Corrección. Al importar desde excel artículos con columnas adicionales marcaba error (#533)
2019.14.0 |14.May | Corrección. Al exportar a excel 'Balanza de Comprobación' se escondían cuentas contables (#525)
2019.14.0 |14.May | Corrección. En catalogo de articulos y servicios al marcar para tienda web y guardar, registraba el articulo de forma incompleta en tienda y se tenia que volver a guardar, se corrigio esto.
2019.14.0 |14.May | Mejora. Procedimientos de usuario en datos del cfdi: CfdiOtrosDatosInit y CfdiOtrosDatosPreValida(Utiliza la propiedad _Screen.lValidaCfdiOtrosDatos para determinar si pasó la validación y procesar el documento)
2019.13.0 |29.Abr | Mejora. Se incluye configuración para obtener Tipo de Cambio oficial de internet automáticamente (#398)
2019.13.0 |29.Abr | Mejora. Poder definir IVA al 0% por cliente (#519)
2019.13.0 |29.Abr | Mejora. Al imprimir factura sin timbrar, mostrar formatos personalizados (#499)
2019.13.0 |29.Abr | Mejora. En emisión de REP, no tomar N.C. como número de parcialidad (#487)
2019.13.0 |29.Abr | Mejora. Antes de borrar un artículo, alertar si artículo tiene movimientos en Kardex (#476)
2019.13.0 |29.Abr | Corrección. Al importar consulta general de compras a Excel, marcaba error (#524)
2019.13.0 |29.Abr | Corrección. Al cargar XML en dolarés al registrar compra, no se cargaba correctamente la divisa en la ventana (#497)
2019.13.0 |29.Abr | Mejora. Se cambió la forma de controlar los retiros de efectivo en caja (#473)
2019.13.0 |29.Abr | Mejora. Al registrar salidas de inventario, se valida si el nivel de usuario tiene acceso a cambiar de sucursal (#514)
2019.12.0 |17.Abr | Mejora. Se permite emitir REP a tercero con documento relacionado.
2019.12.0 |17.Abr | Mejora. En ventana de catálogo de artículos y servicios, se agregó pestaña para datos de la tienda Web.
2019.12.0 |17.Abr | Mejora. Nueva opción en la ventana de configur CFDI para timbrar documentos de contado al abonar.
2019.11.0 |05.Abr | Mejora. Se optimizo proceso de generación de PDF con Addenda (#505)
2019.11.0 |05.Abr | Corrección. Al emitir CFDI de Traslado, mandar atributo 'LugarExpedicion' sin espacios (#498)
2019.11.0 |05.Abr | Corrección. En abonos y pagos de clientes se corrige el la secuencia en el movimiento entre campos de la ventana. (#502)
2019.11.0 |05.Abr | Corrección. En Emitir REPs, no se tomaba la fecha de emision editada en la ventana (ocurria desde versión 2019.4) (#501)
2019.11.0 |05.Abr | Corrección. En REP a terceros el formato salia con la dirección del cliente que pago, queda en blanco como debe ser. (no afecta timbrado, ocurria desde versión 2019.4)
2019.11.0 |05.Abr | Corrección. En registro de entradas y salidas de inventario, al buscar un artículo con clave de 20 caracteres no se cargaba correctamente (#504)
2019.11.0 |05.Abr | Mejora. Al registrar ventas desde caja, si el total de la venta es cero preguntar si desea procesar en cero el documento (#483)
2019.10.0 |29.Mar | Mejora. Cambios para sincronizador con tienda en línea.
2019.9.0 |27.Mar | Corrección. Emitir REP terceros, se emitia al cliente que hizo del pago, no al receptor seleccionado en la ventana (ocurria desde versión 2019.4).
2019.9.0 |27.Mar | Corrección. Al borrar partida en caja se eliminaban las observaciones de todos los artículos.
2019.9.0 |27.Mar | Corrección. Divisa y total del REP se guardaba solo en pesos, afectaba consultas no al REP en si.
2019.8.0 |22.Mar | Mejora. Se permite definir impuesto IVA por proveedor (#452)
2019.8.0 |22.Mar | Mejora. Se permite capturar 'Fecha de Entrega' en ordenes de compra (#443)
2019.8.0 |22.Mar | Mejora. Se permite copiar y pegar notas de venta desde 'Registrar Ventas F4' (#461)
2019.8.0 |22.Mar | Mejora. Se agregó impuesto IVA al 8% a 'Catálogo de Artículos Web' (#478)
2019.8.0 |22.Mar | Mejora. En 'Gráfica de Ventas' se incluye reporte 'Ventas con impuestos' (#468)
2019.8.0 |22.Mar | Mejora. Se amplía rango de decimales en precio a tomar en CFDIs de compra (#474)
2019.8.0 |22.Mar | Corrección. Al entregar pedidos desde caja, si artículo tenía observaciones no se permitía procesar entrega (#463)
2019.8.0 |22.Mar | Corrección. Al registrar salida de inventario, no solicitar capa de salida para servicios (#482)
2019.8.0 |22.Mar | Corrección. Al vender artículos con precio 0 desde caja, se guardaba mal en base de datos (#477)
2019.8.0 |22.Mar | Corrección. Al tener 'Dólares' como divisa por default al registrar compras, solicitaba erróneamente el tipo de cambio al iniciar (#462)
2019.8.0 |22.Mar | Corrección. Ahora se generan índices automáticamente de bases de datos Conteos1, Conteos2, y Conteos3 (#484)
2019.8.0 |22.Mar | Corrección. En 'Registrar Pagos de los Clientes' se mostraban asteriscos en cantidades mayores a 9,999,999.99 (#486)
2019.8.0 |22.Mar | Corrección. No se permitira cambiar hora de pago en REPs.
2019.7.0 |14.Mar | Corrección. En ventana Cobranza/Emitir REP, al editar la hora en detalle del pago actualizaba la base de datos, pero no se reflejaba el cambio al emitir REP. 
2019.6.0 |13.Mar | Corrección. Al incluir cheques de pagos parciales en DIOT, se hicieron correcciones en los cálculos de importes.
2019.6.0 |13.Mar | Mejora. En Cobranza/Abonos se puede cargar factura al ingresar su folio cuando cliente esta vacío. 
2019.5.0 |12.Mar | Mejora. En Cobranza/Registro de pagos de clientes, se puede cargar un documento al ingresar su folio (Habilitar opciónn en botón engranes).
2019.4.0 |11.Mar | Mejora. En detalles de pagos al emitir REPs, se puede modificar la hora del pago.
2019.4.0 |11.Mar | Mejora. En Cobranza/Abonos y Registrar pago clientes, se puede emitir REP automáticamente al procesar si se activa en la configuración de CFDI.
2019.4.0 |11.Mar | Mejora. Cambios internos para emicion de REPs.
2019.3.0 |05.Mar | Mejora. En registro de ventas, al registrar el pago del documento en caja se permite recibir pagos de documentos en dólares
2019.3.0 |05.Mar | Mejora. En registro de ventas, al registrar el pago del documento en caja se agrega la partida del cobro de comisión por la forma de pago (#456)
2019.2.0 |28.Feb | Mejora. Cambios para la versión de DIOT 2019 1.2.1 
2019.2.0 |28.Feb | Mejora. Después de cargar un CFDI a compra, se permite enlazar las ordenes asociadas a la compra. Documentación [Aquí](/sait-erp/9-capacitacion-modulo-compras/n-enlazar-ordenes-asociadas-a-comopra-con-xml/)
2019.2.0 |28.Feb | Mejora. Se realizaron ajustes al cálculo de pólizas a generar en 'Pagos Parciales a Proveedores'
2019.1.0 |22.Feb | Mejora. Cambios para nueva versión de DIOT 2019 1.2.0 (#457)
2019.1.0 |22.Feb | Mejora. En "Registro de gastos" y "Reponer gastos pagados en efectivo", se permite descargar desde el XML los importes Exentos, Gravados0, Gravados8, Gravados16, Iva8, Iva16. Documentación [Aquí](/otros-temas/gastos-separar-importes-por-tasas-de-impuesto)
2019.1.0 |22.Feb | Mejora. En tipos de gastos, se permite definir variables para separar los importes Exentos, Gravados0, Gravados8, Gravados16, Iva8, Iva16 (#453)
2019.1.0 |22.Feb | Mejora. Al capturar Cuenta Origen de Cliente al generar REP se permite capturar folio de cheque con 18 dígitos.
2019.1.0 |22.Feb | Corrección. Al Importar xml de CFDI guardar folio en mayuscula.
2019.0.0 |01.Feb | Mejora. En Inventarios a puertas abiertas solo se permite cerrar o ajustar conteos del almacen activo.
2019.0.0 |01.Feb | Mejora. Se agregaron variables de IVA 8% a CargarDocumento() (#446)
2019.0.0 |01.Feb | Mejora. Se agrego pestaña 'Otros Datos' en Catálogo de Proveedores (#424)
2019.0.0 |01.Feb | Mejora. Poder enviar CFDIs 3.3 emitidos a OCF (#49)
2019.0.0 |01.Feb | Mejora. Se amplió los caracteres a capturar en 'Almacenes adicionales a controlar en esta sucursal'
2019.0.0 |01.Feb | Corrección. Al hacer un conteo a puertas abiertas de un mismo articulo en varios almacenes locales solo se tomaba en cuenta el primer almacen. 
2019.0.0 |01.Feb | Corrección. Al realizar compra con importes mayores a 999,999.00 mostraba asteriscos (#445)
2019.0.0 |01.Feb | Corrección. Al generar REP a Tercero con SQLSERVER marcaba error (#442)
2019.0.0 |01.Feb | Corrección. Al generar PDF a Tercero marcaba 'Problemas para cargar datos de cliente' (#439)
2019.0.0 |01.Feb | Corrección. Al factura nota de venta y seleccionar 'Enviar a cobranza', guardar factura como 'Crédito'
2019.0.0 |01.Feb | Corrección. Error Alias 'BUSCADO' Is Not Found al cerrar ventana de búsqueda de proveedor (#441)
2019.0.0 |01.Feb | Mejora. Nuevo método de usuario ComprasInitDoc para desarrollos especiales
2018.57.0 |19.Ene | Corrección. Al facturar notas en dos equipos, no se actualizaba el siguiente folio y aparecía mensaje: Folio ya existe
2018.57.0 |19.Ene | Corrección. Al cobrar comisión por pago en caja, no se agregaba la unidad al articulo COMX y fallaba al facturar (#437)
2018.57.0 |19.Ene | Corrección. Al facturar, usar los primeros 1000 caracteres de la descripción de la partida (#435)
2018.57.0 |18.Ene | Mejora. Al facturar nota de venta, se agrego opción 'Enviar a cobranza' para generar cargo en el estado de cuenta
2018.57.0 |18.Ene | Mejora. Se amplió rango de totales a mostrar en PDF (#436)
2018.57.0 |18.Ene | Mejora. Al realizar Factura global, se puede relacionar una factura global anteriormente cancelada (#214)
2018.57.0 |18.Ene | Mejora. Al agregar factura manualmente como cargo desde el menú de cobranza, considerar retenciones e impuestos locales (#434)
2018.56.0 |10.Ene | Mejora. En grupos de usuarios, se agregó el permiso 'Definir máximos y mínimos  de otras sucursales' (#430)
2018.56.0 |10.Ene | Mejora. Enviar registro de Gastos por enlace de sucursales. Agregar evento 'Gastos' a SAIT Distribuido (#390)
2018.55.0 |02.Ene | Mejora. Se eliminaron restricciones para solo manejar IVA al 0% y 16%. Se puede capturar porcentaje de IVA libremente.
2018.55.0 |02.Ene | Mejora. Se restringe el eliminar artículos, solo permite cuando tiene existencia 0.
2018.55.0 |02.Ene | Mejora. Se permite iniciar conteo con mismo artículo en distintos almacenes al mismo tiempo.
2018.55.0 |02.Ene | Mejora. Al cambiar clave de cliente aplica a documentos de CFDI 3.3 y REP. 


{{% children  %}}
