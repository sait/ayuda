+++
title = "Respaldos Diarios por medio de SAIT"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=JdDUa4zYNi0&list=PLhfBtfV09Ai6KSJCorE3HpReaVqtMkqkr&index=3&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="5. A. Respaldos Automáticos Diarios por Sait.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para realizar la configuración de respaldos automáticos en Sait diríjase a:

<h4 class="text-primary">Utilerías / Configuración General del Sistema</h4>

* Aparecerá la ventana de Configuración General del Sistema, ir a la pestaña de "OTROS"
 

![IMG](1.png)

* En la sección de Respaldo Automático habilite la opción de "Respaldar diariamente".

* Escriba el directorio donde se almacenará el respaldo o haga clic en el botón con el signo de [?] para seleccionar el directorio. En nuestro caso es:  C:\RESPALDOS\

* En la opción de "Mantener los Últimos" establezca cuantos respaldos desea tener de su empresa. Para este ejemplo son 30
.
* Haga clic en **[Cerrar]**.

* Salir y entrar nuevamente a SAIT para que se genere el respaldo del día de hoy.

* El sistema mostrará el avance en la parte superior derecha. Como se presenta en la siguiente imagen:

![IMG](2.png)

* ¡Listo! Ha terminado de configurar los respaldos diarios en su computadora. A partir de este momento, se realizarán respaldos automáticos en la ruta que haya indicado.
