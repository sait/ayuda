+++
title = "Respaldos Automáticos por medio de SAITBKUP"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=etcLNldGfe8&list=PLhfBtfV09Ai6KSJCorE3HpReaVqtMkqkr&index=2&t=2s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="5. B. Respaldos Automáticos Diarios por SAITBKUP.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para realizar la configuración de respaldos automáticos por SAITBKUP, favor de seguir las siguientes indicaciones:

**1.Instalar el programa de respaldos SAITBKUP.**

a.	Descargar desde la siguiente dirección el instalador del programa para respaldar: 

https://www.sait.com.mx/download/instsaitbkup.exe 

b.	Ejecutar el instalador dando doble clic sobre el ícono del instalador.

![IMG](1.png)

c. Se nos abrirá la siguiente pantalla. Damos clic en **[Ejecutar]**


![IMG](2.png)

d. Descomprimir archivos.
El sistema por default nos muestra la carpeta temp en la raíz del disco C:\ para descomprimir los archivos, pero esta carpeta y ruta son editables.

Damos clic en **[Unzip]**

![IMG](3.png)

e. Ejecutamos el setup.exe ubicado dentro de la carpeta C:\temp

![IMG](4.png)

f. Comenzamos la instalación de SAITBKUP.

Clic en **[Coninuar]**.

![IMG](5.png)

g. Ingresamos los datos solicitados

Clic en **[Aceptar]**

![IMG](6.png)

h. Confirmamos la acción.

Damos clic de nuevo en **[Aceptar]**

![IMG](7.png)

i. Esperamos a que se realice el proceso.

![IMG](8.png)

j. Definir carpeta en donde se estarán guardando los respaldos asi como el ejecutable de la aplicación.

De igual forma el sistema nos proporndrá una carpta default, pero nosotros podemos cambiarla.

Clic en **[Aceptar]**

![IMG](9.png)

k. Damos clic en el botón indicado en la siguiente imagen para inciar la instalación.


![IMG](10.png)

l. Finalizar la instalación.

Damos clic en **[Aceptar]**

![IMG](11.png)

**2.Actualizar el ejecutable de respaldos SAITBKUP.**

a. Descargar desde la siguiente dirección la nueva versión del archivo SAITBKUP.EXE:    

https://www.sait.com.mx/download/saitbkup.exe
            
b. Nos dirigimos a nuestra carpeta de descargas y damos clic derecho sobre el ícono y seleccionamos Copiar.

![IMG](12.png)

c. Posteriormente nos dirigimos a la carpeta C:\SAITBKUP, damos clic derecho y seleccionamos Pegar o presionamos CTRL+V y damos clic en Copiar y Reemplazar.

![IMG](13.png)

3.Definir las carpetas que se estarán respaldando.

a.	Creamos el archivo llamado SAITBKUP.CFG

Para crearlo nos dirigimos a la carpeta C:\SAITBKUP 

Damos clic derecho y nos dirigimos a la sección de Nuevo y seleccionamos Documento de texto.

![IMG](14.png)

Nombramos el archivo tal y como se muestra, al momento de cambiar la extensión .TXT a .CFG se nos mostrará la siguiente ventana de alerta, damos clic en Sí.

![IMG](15.png)

b.	 Una vez creado el archivo SAITBKUP.CFG  hay que editarlo definiendo los directorios que queremos respaldar así como los días que queremos mantener dichos respaldos.

Damos clic derecho sobre el archivo SAITBKUP.CFG y seleccionar la opción de Editar.
![IMG](16.png)

Primeramente definimos los días que se estarán manteniendo los respaldos, para efectos de esta demostración se dejará en 15.
Dejamos el renglón siguiente tal y como está.

Y en la tercera sección indicamos las rutas de los directorios ( indicamos que deseamos respaldar todo lo que contiene dicha carpeta con la siguiente instrucción * . * )

Guardamos los cambios.

![IMG](17.png)

Crear un acceso directo al archivo C:\SAITBKUP\SAITBKUP.EXE 
De ahora en adelante al ejecutar la aplicación SAITBKUP se realizará el respaldo de todas las carpetas que hemos indicado previamente.


<h4 class="text-primary">Para que esta utilería se ejecute automáticamente y en una hora en específico se deberá de crear una tarea programada.</h4> 

Para descargar la documentación de tareas programadas de clic [aquí](crear-y-ejecutar-tareas-programadas.pdf)


### RECOMENDACIONES:

* Puede habilitar la opción para que al grupo de Inicio de Windows este se ejecute al encender el equipo.

* De manera opcional se puede crear dicho acceso directo en el grupo de inicio de Windows para que se ejecute al entrar, y no se tengan que hacer de forma manual los respaldos. 

* Funciona muy bien cuando se desean hacer respaldos de varios directorios y los desean almacenar en un solo archivo. 
Es fácil de descomprimir y se crean diferentes carpetas según los archivos que se respaldaron, de ese modo la información no se mezcla. 
