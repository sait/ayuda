﻿+++
title = "Actualizar Vencimientos"
description = ""
weight = 11
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=y6BIjHr3orA&t=0s&index=9&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. K Actualizar Vencimientos CxP.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El vencimiento de un documento a crédito se determina en el momento en que se procesa, en base a la fecha del documento y los días de crédito que se le haya otorgado el proveedor.

En ocasiones cuando no se puede cubrir el pago del documento a la fecha de vencimiento, se negocia el pago con el proveedor y se define otra nueva fecha de vencimiento.

**Es importante mantener los vencimientos actualizados con los proveedores en SAIT para que al consultar reportes de saldos, el documento aparezca con la fecha correcta.**

<h2 class="text-primary">Actualizar Fechas</h2>

1.Si lo que desea es actualizar el saldo de un documento, debe dirigirse al menú de Cuentas por Pagar / Actualizar Vencimiento.

2.Escriba la clave del proveedor, o presione la tecla [F2] para buscar por nombre.

3.Colóquese en la columna “Vencimiento” y actualice la fecha de vencimiento del documento deseado (de forma individual).

![IMG](1.png)

4.Si lo que desea es actualizar el vencimiento de todos aquellos documentos que se vencen en la misma fecha que el documento seleccionado, haga clic en [Modificar Fechas].

![IMG](2.png)

5.Debe escribir la nueva fecha para los documentos que se vencen dicho día.

6.Para grabar los cambios de clic en el botón de [Procesar = F8].


<h2 class="text-primary">Borrar Fechas</h2>

De manera opcional puede hacer clic en [Borrar Fechas] para borrar todas las fechas de vencimiento de todos los documentos a partir del registro en que se encuentra posicionado, y de esa manera pueda colocar manualmente las nuevas fechas de vencimiento.

![IMG](5.png)

* Deberá confirmar que desea borrar todas las fechas a partir de ese documento. Presione el botón [SI].


* Presione la tecla [F8] para procesar la actualización de los vencimientos.
