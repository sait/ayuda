+++
title = "Aplicar pagos con Notas de Crédito de proveedores en Cuentas por Pagar"
description = ""
weight = 15
+++

Para utilizar esta opción en SAIT deberá realizar una configuración previa:

Deberá contar con los siguientes conceptos, en caso de no tenerlos deberá crearlos en el menú de:

Cuentas por Pagar / Conceptos de Cuentas por Pagar (puede revisar la guía donde se explica cómo agregar un nuevo concepto https://ayuda.sait.mx/sait-erp/14-capacitacion-modulo-cuentas-por-pagar/c-conceptos-de-cuentas-por-pagar/ )

Concepto | Descripción | Tipo
---|---|---
AC | APLICAR NOTA DE CRÉDITO | ABONO
CC | CANCELACIÓN DE NOTA DE CRÉDITO | CARGO
DC | DEVOLUCIÓN DE COMPRA | ABONO
NC | NOTA DE CRÉDITO | ABONO (Capturar referencia)


Las devoluciones de compras pueden ser a compras pagadas parcialmente o pagadas en su totalidad, con esta forma para aplicar pagos con notas de crédito se debe tomar en cuenta que si la devolución es mayor al saldo, al proveedor le quedará un saldo a favor.

Las devoluciones de compras se realizarán de la manera cotidiana en SAIT es decir desde Compras / Devoluciones.

Para efectos de la prueba, utilizaremos este proveedor, el cual se le hizo una compra por 11,600 pesos y fue pagada en su totalidad

![IMG](1.png)

Posteiormente realizaremos una devolución total de la compra AA5122

![IMG](2.png)

En el estado de cuenta del proveedor se nos generará una Nota de Crédito con saldo a favor de 11,600 pesos y esta Nota de Crédito la podrá aplicar las veces necesarias mientras tenga aún saldo pendiente.

![IMG](3.png)

Para tener saldo pendiente y hacer la demostración, se ingresó otra compra por un total de 1,000 pesos y la abonaremos con la NC AA32

![IMG](4.png)

El concepto a utilizar para hacer pagos aplicando la nota de crédito (Cuentas por Pagar / Registrar Pagos) será el siguiente:

* Seleccione la Forma de Pago "Aplicar Nota de Crédito"

* El número de Proveedor

* El Folio de la devolución AA32 (al poner el folio, cargará en automático el saldo disponible)

* Captgure el importe a aplicar

![IMG](5.png)

El estado de cuenta se actualizará de la siguiente manera, en donde podemos notar aún tenemos saldo a favor por 11,100 pesos.

![IMG](6.png)

Otro punto importante a comentar es que si se da el caso de que se haya abonado una compra de manera parcial con nota de crédito y se saldará la compra desde Elaborar Cheque el sistema de manera automática detectará la nota de crédito para efectos del pago.

![IMG](7.png)


