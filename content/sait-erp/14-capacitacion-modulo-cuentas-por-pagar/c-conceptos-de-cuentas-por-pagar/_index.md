﻿+++
title = "Conceptos de Cuentas por Pagar"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=Gg3KkFx0HbA&index=2&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh&t=10s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. C CONCEPTOS DE CXP.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El catálogo de conceptos de cuentas por pagar permite definir nuevos conceptos para que los pueda utilizar según las necesidades de la empresa, ya sean como cargos o abonos.

Para poder definir nuevos conceptos, modificar o eliminarlos deberá ingresar al menú de

Cuentas por Pagar / Conceptos de Cuentas por Pagar.
 


![IMG](1.png)


<h2 class="text-primary">Agregar nuevo concepto</h2>

Para agregar un nuevo concepto debe hacer clic en el botón de [Agregar].

1.Capture la **Clave del nuevo Concepto** a generar.  (La clave puede estar compuesta por un máximo de 2 caracteres).

2.Capture la **Descripción** o nombre del Concepto.

3.Seleccionar el **Tipo de Concepto** a ingresar, con la barra de espacio puede elegir: **CARGO ó ABONO.**

4.Indicar si es necesario o no, capturar alguna **Referencia** en el nuevo concepto.

5.Indicar el **Nombre del Reporte** que se utilizará como comprobante de impresión.

6.Capturar el siguiente **Folio Consecutivo** del concepto de cuentas por pagar.

![IMG](2.png)

7.Para grabar el nuevo concepto debe hacer clic en el botón [Grabar].

<h2 class="text-primary">Modificar un concepto</h2>

Para modificar un concepto debe hacer clic en el botón [Modificar], estando posicionado sobre el concepto que desea modificar.

1.En pantalla, se mostraran los datos relacionados con el concepto.

2.Deberá realizar las modificaciones a los datos que sean necesarios.

![IMG](3.png)


3.Para grabar los cambios realizados al concepto, hacer clic en el botón [Grabar].

<h2 class="text-primary">Eliminar un concepto</h2>

Para eliminar un concepto debe hacer clic en el botón de [Eliminar], estando posicionado sobre el concepto que desea modificar.

<h3 class="text-danger">NOTA</h3>

**Se recomienda eliminar solamente los conceptos que no hayan sido utilizados aún.** 

1.En pantalla, se mostraran los datos relacionados con el concepto.

![IMG](4.png)
 
2.Debe verificar que el concepto a borrar sea el correcto.

3.Una vez confirmado el dato, y se haya asegurado que el concepto a eliminar es correcto, debe hacer clic en el botón [Eliminar].
