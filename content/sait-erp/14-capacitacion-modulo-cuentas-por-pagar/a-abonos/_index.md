﻿+++
title = "Abonos"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=jvOYfklX_JY&index=6&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. A Abonos a Cuentas por Pagar.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

La opción de Abonos a cuentas por pagar, permite registrar pagos totales o abonos parciales a un solo documento que pertenezca a un proveedor en específico. 

Es importante aclarar, que este proceso solamente afecta o salda el estado de cuenta del proveedor. En ningún momento se afecta bancos o contabilidad.

Para realizar este proceso debe:

1.Dirigirse al menú de Cuentas por Pagar / Abonos.

2.Debe indicar el **Tipo de Concepto** de abono a utilizar.

3.Indique la **Clave del Proveedor** o haga clic en el signo de interrogación para buscar.

4.Posteriormente, debe especificar la **Referencia** o el **Folio de Compra** al que se le aplicará el abono. En caso de ser necesario puede dar clic en el botón [?] para buscar.

5.Debe capturar la **Referencia del Abono** y la **Fecha** en que está aplicando el pago.

6.Ingrese el **Monto** equivalente al abono e indique la divisa del movimiento. Con la barra de espacio puede cambiar de pesos a dólares. 

7.En pantalla se mostraran los datos de saldo anterior, abono y nuevo saldo. 

8.De manera opcional, puede capturar algún comentario relacionado con el abono.



![IMG](1.png)

9.Para procesar el abono, haga clic en el botón [Procesar=F8].

10.Listo, el abono ha sido procesado con éxito.
