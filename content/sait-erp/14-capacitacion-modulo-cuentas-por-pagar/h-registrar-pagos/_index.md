﻿+++
title = "Registrar Pagos"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=It2ya2I0ByE&t=0s&index=7&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. H Registrar Pagos.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El siguiente proceso describe los pasos para registrar con un solo movimiento, el pago a varias facturas de un mismo proveedor. 

**Es importante aclarar, que este proceso solamente afecta o salda el estado de cuenta del proveedor. En ningún momento se afectan bancos o contabilidad.**

Para realizar este proceso deberá:

1.Ingresar al menú de Cuentas por Pagar / Registrar Pagos.

2.Debe especificar la forma del pago.

3.Debe indicar la clave del proveedor. En caso de que no conozca la clave, puede presionar el signo de interrogación para buscar por nombre.

4.Escriba la referencia de movimiento y la fecha del pago.

5.Capture el importe total del pago.

6.Seleccione con la barra espaciadora la divisa. En caso de que el pago sea en dólares, especifique el tipo de cambio.

7.En la cuadricula deberá seleccionar todas aquellos documentos a los que desea aplicar el pago. 

8.De manera opcional, puede modificar los abonos correspondientes a las facturas, posicionándose sobre la columna de Abono, y capturando directamente el importe que usted desee aplicar a esa factura.
 
![IMG](1.png)

9.Finalmente, una vez que haya aplicado todos los pagos a las facturas correspondientes, deberá dar clic en el botón [Procesar = F8].

10.Confirme que desea aplicar los pagos y listo, el pago ha sido procesado correctamente
