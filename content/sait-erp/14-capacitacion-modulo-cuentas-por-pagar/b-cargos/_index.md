﻿+++
title = "Cargos"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=zd4YT86Hvp4&index=5&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. B Cargos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Esta opción permite registrar un cargo o monto adicional que va estar pendiente de pago en el estado de cuenta de un proveedor en específico. Esta opción es muy útil cuando se desea importar los cargos pendientes de los proveedores de otro sistema o bien, por error se eliminó algún cargo del estado de cuenta del proveedor.

El proceso para registrar un cargo en el sistema es muy sencillo y se describe a continuación:

1.Debe dirigirse al menú de Cuentas por Pagar / Cargos.


![IMG](1.png)


2.Debe seleccionar el **Tipo de Concepto** de cargo a utilizar (Gasto o Compra) por ejemplo.

3.Indique la **Clave del Proveedor**. En caso de ser necesario dar clic en el botón de signo de interrogación para buscar por nombre.

4.Capture el **Número de Documento.**

5.Capture la **Fecha** de emisión del documento.

6.Capture la **Fecha de vencimiento.**

7.Ingrese el **Monto** equivalente al cargo

8.Y la **Divisa** del documento. Con la barra de espacio puede modificar este valor de Pesos a Dólares.

9.De manera opcional, puede capturar algún comentario relacionado al cargo.
