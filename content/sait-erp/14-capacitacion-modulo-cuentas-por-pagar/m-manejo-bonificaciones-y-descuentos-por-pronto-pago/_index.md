﻿+++
title = "Manejo de Bonificaciones y Descuentos por pronto pago de Proveedores"
description = ""
weight = 14
+++

<h2 class="text-primary">1.Puntos previos a considerar</h2>	

a. Permitir devoluciones sin compra

i.	Dirigirse al menú de Utilerías / Grupos de Usuarios

ii.	Especificar Grupo o Nivel

iii.	En la pestaña de compras, habilitar la opción: permitir devol. Directas (sin tener compras)

![IMG](1.jpg)

b.	Verificar que los conceptos de cuentas por pagar se encuentren definidos o de lo contrario agregarlos (puede revisar la guía donde se explica cómo agregar un nuevo concepto https://ayuda.sait.mx/sait-erp/14-capacitacion-modulo-cuentas-por-pagar/c-conceptos-de-cuentas-por-pagar/ )


CONCEPTO | DESCRIPCIÓN | CARGO O ABONO | REPORTE | SIG.NUM.DOC 
---|---|---|---|---
AC | APLIC. NOTA DE CRÉDITO |ABONO | VACÍO | 1 
CC | CANC. NOTA DE CRÉDITO | CARGO | VACÍO | 1  
DC | DEVOLUCIÓN DE COMPRA | ABONO | VACÍO | 1  


<h2 class="text-primary">2.Definir concepto de descuento por pronto pago</h2>	


a.	Dirigirse al menú de Inventario / Catálogo de Artículos y Servicios.

b.	En clave escriba: DPP

c.	En descripción escriba: DESCUENTO POR PRONTO PAGO

d.	En unidad escriba: ACT de Actividad

e.	En clave SAT: 84111506 que corresponde a servicios de facturación

f.	Marque la casilla: Es un servicio

g.	El precio queda en cero

h.	El porcentaje de impuesto en %16

i.	Haga clic en **[Grabar]**

![IMG](2.jpg)


<h2 class="text-primary">3.Registrar Devolución </h2>	


a.	Dirigirse al menú Compras / Devoluciones

b.	Dar clic en el botón: Capturar Artículos

![IMG](3.jpg)

d.	Especificar fecha y divisa

e.	Especificar el proveedor

f.	En el detalle de los productos, establezca:

i.	Clave: DPP

ii.	Devolver: 1

iii. Precio: establecer el importe del descuento sin el impuesto.

g.	Verificar que el total de la devolución coincida con el total del descuento

h.	Dar clic en: Procesar o F8

i.	Ver siguiente Imagen:

![IMG](4.jpg)

k.	Si revisamos el estado de cuenta del proveedor:

![IMG](5.jpg)

m.	Veremos que la devolución por descuento de pronto pago aparece como saldo a favor del proveedor.


<h2 class="text-primary">4.Aplicar devolución por descuento a Compra</h2>	


a.	Dirigirse al menú de Cuentas por Pagar / Registrar Pagos

b.	Especificar forma de pago: Aplic. Nota de Crédito

c.	Especificar el proveedor

d.	En el campo de folio indique el folio de la devolución

e.	Seleccione la compra a la que se aplicara el descuento, ver siguiente imagen:

![IMG](6.jpg)

g.	Si revisamos el estado de cuenta veremos que ya se aplicó el descuento a la compra en cuestión, ver siguiente imagen:

![IMG](7.jpg)


<h2 class="text-primary">5.Saldar el documento</h2>	


a.	Si no utiliza contabilidad en SAIT realice los siguientes pasos

i.	Para saldar el documento dirigirse a Cuentas por pagar / Registrar Pagos

ii.	Especificar forma de pago

iii.	Especificar el proveedor

iv.	En caso de ser necesario indique el folio de la forma de pago

v.	Especifique la fecha de pago

vi.	Especifique el importe y divisa

vii.	Seleccione la compra a la que se aplicara el pago


![IMG](8.jpg)


b.	Si utiliza la contabilidad de SAIT realice los siguientes pasos

i.	Para saldar el documento dirigirse a Cuentas por pagar / Elaborar Cheque con Pago Parcial

ii.	Especificar forma de pago (Cheque o Transferencia)

iii.	Verificar folio y fecha

iv.	Indicar el proveedor

v.	Indicar el concepto

vi.	Seleccionar la factura a pagar

 
![IMG](9.jpg)

c.	Si revisamos el estado de cuenta la compra nos aparecerá ya con saldo en ceros

