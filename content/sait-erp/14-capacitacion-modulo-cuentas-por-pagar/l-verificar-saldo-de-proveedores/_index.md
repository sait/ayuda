﻿+++
title = "Verificar Saldo de Proveedores"
description = ""
weight = 12
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=rKinzHbuXdM&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh&index=13&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. L Verificar Saldos de Proveedores.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

La utilería para verificar saldos de los proveedores permite corregir errores al momento de registrar movimientos al estado de cuenta de un proveedor. 

Estos errores pueden tener diversas causas, la mayoría relacionados a problemas con el equipo, fallas con energía eléctrica o errores de captura, lo cual ocasiona que no se registren correctamente todos los movimientos y quede registrado un saldo mayor o menor al correcto.


<h2 class="text-primary">Detectar problemas con Saldos</h2>

1.Ingrese al menú de Cuentas por Pagar / Verificar Saldos de Proveedores.

![IMG](1.png)

2.En esta ventana, usted podrá detectar inconsistencias en todos los proveedores, especificando las siguientes condiciones:

* Detectar diferencias en saldos de proveedor vs sus movimientos: Este proceso detecta errores entre el saldo global del proveedor contra los movimientos que tiene registrado en su estado de cuenta como compras, gastos y abonos.

* Detectar diferencias en saldo de documentos vs sus movimientos: Este proceso detecta errores entre el saldo total del documento contra los abonos que se realizaron a dicho documento.

* No considerar diferencias menores a cierta cantidad: Debe especifique la cantidad mínima para detectar errores, ya que en ocasiones puede haber diferencias por centavos. 

3.Haga clic en [Detectar Problemas].

![IMG](2.png)

4.Una vez que usted ya haya realizado el proceso de detectar errores, podrá copiar a un archivo de texto la consulta que se muestra en esta ventana presionando el icono    de copiar datos al portapapeles. 

<h2 class="text-primary">Corregir Saldos</h2>

En caso de detectar errores en los saldos de los proveedores o documentos, se debe hacer una revisión para detectar que ocasionó el error para posteriormente corregirlo.

1.Ingrese a la segunda pestaña [Corregir Saldos] que se encuentra en esta misma ventana.

2.Debe indicar la clave del proveedor o presionar F2 para buscar por nombre.

3.En caso de haber diferencias en el saldo de un proveedor se mostrará la siguiente leyenda “Saldo de catálogo no concuerda con movimientos”.

![IMG](3.png)


4.Lo único que deberá hacer es verificar el saldo en movimientos y el saldo en catálogos para posteriormente presionar el botón de [Corregir Saldo en Catálogo].

5.Confirmar que desea corregir el saldo.

![IMG](4.png)

6.Y el sistema en ese momento le confirmará que el saldo ha sido corregido correctamente.

![IMG](5.png)

7.En caso de haber inconsistencias en el saldo de documento, se muestra la leyenda: **"Saldo de algunos documentos es incorrecto"** y se mostrarán todos aquellos documentos que cuenten con un saldo incorrecto.

![IMG](6.png)

8.En este caso, deberá seleccionar  el documento y presionar en la parte inferior de esta ventana la opción de [Corregir saldo de documento].

9.Deberá confirmar que desea corregir el saldo de ese documento en específico. 

![IMG](7.png)

10.Y listo, con este proceso ha verificado el saldo de los proveedores y saldos de documentos y los ha corregido correctamente.