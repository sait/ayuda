﻿+++
title = "Cancelar Anticipos y Notas de Crédito"
description = ""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=XzqLS08ja98&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. I Cancelar anticipos y notas de credito.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:



SAIT te permite llevar un control interno para los anticipos y notas de crédito que se tengan con un proveedor en específico.

Para agregar un SALDO A FAVOR en el estado de cuenta de un proveedor por concepto de ANTICIPO o NOTA DE CRÉDITO, deberá ir al menú de Cuentas por Pagar / Abonos. 

![IMG](1.png)

En el estado de cuenta del proveedor, se verá reflejado este documento en color azul, como recordatorio de que se tiene un SALDO A FAVOR.

![IMG](2.png)

Para CANCELAR un ANTICIPO o NOTA DE CRÉDITO de un proveedor deberá:

1.Ingresar al menú de Cuentas por Pagar / Cancelar Anticipos y Notas de Crédito.

![IMG](3.png)

2.Deberá indicar la clave del proveedor.

3.Seleccionar con la barra de espacio el tipo de movimiento que desea cancelar: Anticipo o Nota de Crédito.

4.Indicar el folio del documento y presionar ENTER.

5.Indicar la fecha de cancelación.

6.Y colocar algún comentario relacionado al motivo de cancelación del documento.

![IMG](4.png)

7.Presione el botón de [Cancelar].

8.Confirme que realmente desea cancelar el saldo del documento.

![IMG](5.png)

9.Y listo, el saldo del documento a favor ha sido cancelado correctamente.

10.En el estado de cuenta del proveedor se seguirá reflejando el movimiento con su respectivo concepto de cancelación, sin embargo ya no contarán con ningún saldo a favor.
 
![IMG](6.png)


