﻿+++
title = "Reponer Gastos Pagados en Efectivo"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=42Arz71G1oQ&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. G Reponer Gastos Pagados en Efectivo.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite definir un proveedor genérico con clave 0 en el cual solamente debe especificar el nombre de dicho proveedor y su RFC.

![IMG](1.png)

Esta opción de reponer gastos pagados en efectivo, permite en un solo pago registrar las facturas de distintos proveedores.

 Para realizar este proceso deberá seguir las siguientes indicaciones:

1.Ingresar al menú de Cuentas por Pagar / Registrar Gastos Pagados en Efectivo.

![IMG](2.png)

2.Debe especificar el concepto al que hará referencia el movimiento.

3.Posteriormente debe establecer la relación de comprobantes que desea pagar.

4.Presione el botón de [Agregar].

![IMG](3.png)

 5.Busque al emisor por nombre y presione ENTER para seleccionarlo.

![IMG](4.png)

6.Deberá especificar de qué forma desea contabilizar el archivo XML, es decir, especifique el tipo de gasto que desea agregar.

7.En la cuadricula debe seleccionar los comprobantes que desea adjuntar.

![IMG](5.png)

8.Presione [F8 Continuar] y confirme que realmente desea contabilizar los comprobantes con el tipo de gasto seleccionado.

![IMG](6.png)

9.En la ventana de reposición de gastos usted podrá agregar todos los comprobantes que desee, presionando nuevamente en el botón de [Agregar].
 
![IMG](7.png)

10.Una vez que haya terminado de agregar todos los comprobantes que necesite, presione el botón de [F8 Pagar].

11.Confirme que desea pagar los comprobantes seleccionados.

![IMG](8.png)

12.Y listo, los gastos fueron registrados correctamente en el sistema.

13.Posteriormente se mostrará la ventana de elaborar cheque para pago a proveedor.

![IMG](9.png)

<h3 class="text-danger">NOTA</h3>

**Es importante aclarar que este proceso solo aplica si su configuración de gastos es Contabilizar en Cheque. No aplica si su configuración es para provisionar Gastos.**

14.En automático, en esta ventana ya viene seleccionado el proveedor genérico, ya viene asociado el archivo XML, ya está indicado el documento a pagar y la póliza que se va a generar. Solamente deberá:

* Seleccionar el banco.

* Especificar el folio de la póliza.

* Indicar la fecha del movimiento.


* Especificar si es Transferencia o Cheque.

* En el caso del Cheque, podrá seleccionar si es para abono en cuenta y definir la fecha límite para cobrar dicho cheque, con el fin de que salga impreso.

* Especificar el concepto del movimiento. 

15.Finalmente deberá presionar el botón de [F8 Procesar] para grabar el pago a proveedor.
16. Y listo, se ha realizado el registro 
