﻿+++
title = "Elaborar Cheque"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=6ifHqtbEJm4&index=8&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. D Elaborar Cheque.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El siguiente proceso describe los pasos para registrar con un solo movimiento, el pago a varias facturas de un mismo proveedor. 

Esta forma de registrar pagos es la más recomendada si se desea registrar el pago total a varias facturas del mismo proveedor, **afectar Bancos y Contabilidad al generar la póliza**, en caso de que el sistema este configurado con enlaces contables.

El siguiente proceso describe los pasos para registrar con un solo movimiento, el pago a proveedores, generar movimiento en bancos y grabar la póliza en contabilidad.

1.Ingrese al menú de Cuentas por Pagar / Elaborar Cheque.
 

![IMG](1.png)


2.Seleccione la forma de contabilizar: Contabilizar en Cheque, Pagar Facturas Provisionadas, Reposición de Caja.

3.Seleccione la cuenta del banco del cual se hará el cheque o transferencia.

4.En caso de ser necesario puede modificar el folio de cheque o transferencia.

5.Debe indicar la fecha de emisión en la que se está aplicando el pago.

6.De manera opcional, se puede capturar la fecha de cobro la cual puede ser impresa en el cheque. Así como seleccionar la opción [ * ] Para abono en cuenta.

7.En el caso de transferencia el punto anterior no aplica.

8.Escriba la clave del proveedor. En caso de no recordar la clave, puede dar clic en el signo de interrogación para buscar por nombre o presionar la tecla [F2].

9.En el caso de la transferencia es necesario que indique la cuenta destino del proveedor.

10.Capture el concepto del cheque o transferencia

11.En el campo de comprobantes se reflejaran los folios correspondientes a las facturas a las que se esté aplicando el pago.

12.En la cuadricula, debe seleccionar los documentos que se van a pagar.

![IMG](2.png)

13.Si cuenta con enlaces contables configurados previamente en su sistema, al lado derecho de la pantalla se podrá observar la póliza a generar. Se pueden agrupar las cuentas para disminuir la cantidad de asientos contables.

14.Por último, presione la tecla [Procesar = F8] para grabar el pago e imprimir el Cheque-Póliza.
