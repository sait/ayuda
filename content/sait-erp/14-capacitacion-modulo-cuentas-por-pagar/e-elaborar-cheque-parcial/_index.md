﻿+++
title = "Elaborar Cheque Parcial"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=4R8BYipEhFo&t=0s&index=6&list=PLhfBtfV09Ai5Zpe9FGGpcB8l4fzleRpjh" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="14. E Elaborar Cheque Parcial.pdf" target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Esta forma de registrar pagos es la más recomendada ya que desde esta opción se pueden registrar pagos totales o parciales a varias facturas del mismo proveedor, **afectar Bancos y Contabilidad al generar la póliza** en caso de que el sistema este configurado con enlaces contables.

El siguiente proceso describe los pasos para registrar con un solo movimiento, el pago total o parcial a proveedores, generar movimiento en bancos y grabar la póliza en contabilidad.

1.Ingrese al menú de Cuentas por Pagar / Elaborar Cheque.


![IMG](1.png)

2.Seleccione la forma de contabilización: Contabilizar en Cheque, Pagar Facturas Provisionadas o Reposición de Caja.

3.Seleccione la cuenta del banco del cual se hará el cheque.

4.Seleccione la forma de pago como Cheque o Transferencia.

5.En caso de ser necesario puede modificar el folio del pago.

6.Especifique la fecha de emisión en la que se realizará el pago.

7.Indique la clave del proveedor. En caso de no recordar la clave, puede presionar el signo de interrogación para buscar por nombre, o presionar la tecla [F2].

8.En caso de que el proveedor tenga cuentas por pagar bajo la divisa en Dólares, el sistema le solicitará capturar el tipo de cambio al que se tomará sus pesos al pagar facturas en dólares.
 
![IMG](2.png)

9.En el caso del Cheque, de manera opcional puede activar la casilla [*] para abono en cuenta y capturar la fecha de cobro, con la intención de que salgan impresas en el cheque póliza.

10.Debe especificar un concepto de referencia.

11.En la cuadricula deberá seleccionar las facturas que desea pagar o abonar.

12.En caso de que no esté saldando completamente la factura, puede colocar manualmente el importe a abonar posicionándose sobre la columna de [Abonar].

![IMG](3.png)

13.En caso de haber facturas con divisa distinta a la cuenta bancaria, en la parte inferior se hace mención de dichos folios indicando su divisa y la divisa con la que se muestra el saldo.

* El total y el saldo se muestra en la divisa de la cuenta bancaria: Si la divisa de la cuenta bancaria es pesos y la factura es dólares, el total y saldo se muestran en pesos.

14.Si cuenta con enlaces contables, al lado derecho de la pantalla se puede observar la póliza a generar. Se pueden agrupar las cuentas para disminuir la cantidad de asientos contables.

15.Por último, presione la tecla [F8 = Procesar] para procesar el pago e imprimir el comprobante del cheque o transferencia.

<h3 class="text-danger">Importante</h3>

Es importante mencionar que si su cuenta de bancos es en Dólares, debe especificar el tipo de cambio oficial correspondiente al día anterior, en el menú de utilerías / tipo de cambio oficial.

![IMG](4.png)

Las cuentas destinos de los proveedores se debe especificar en el menú **Compras / Proveedores, pestaña Otros.**

![IMG](5.png)