+++
title = "Modo conteos de inventario"
description = ""
weight = 2
+++

Con la versión 2.0 de inventarios [ver documentación](/sait-erp/8-capacitacion-en-modulo-inventario/o-proceso-de-toma-de-inventario/version-2/) es indispensable se utilice un dispositivo móvil para capturar en SAIT en en tiempo real la existencia física conforme vaya contando

La nueva versión de SAIT Móvil tiene integrada la opción de Conteos de Inventarios, a continuación se explica su funcionamiento:

<h3 class="text-primary"> Requerimientos técnicos </h3>

- Ejecutar en el servidor el instalador de SAIT Móvil Server [ver documentación](/sait-movil/intalacion-sait-movil-server/)
- Dispositivo Android (teléfono o tablet)
- Scanner Bluetooth obligatorio para agilizar la lectura de códigos de barras e identificar el artículo 
- Red wifi con buena estabilidad 

<h3 class="text-primary"> Pre-requisitos </h3>

- Para acceder a la aplicación es necesario que el número de vendedor sea igual al numero de usuario

- Habilitar versión 2.0 de Toma de Inventario [ver documentación](/sait-erp/8-capacitacion-en-modulo-inventario/o-proceso-de-toma-de-inventario/version-2/)

<h3 class="text-primary"> Funcionamiento </h3>

1)Ejecutar la app y seleccionar Toma de Inventario

![IMG](conteos.png)

2)En la sección de CLAVE O CÓDIGO DE ARTÍCULOS podrá agregar el artículo que contarará

![IMG](1.jpeg)

3)Una vez agregado el artículo deberá ingresar la CANTIDAD FÍSICA de dicho artículo, puede agregar la cantidad 

* Con el teclado ingresando directamente la cantidad

* Con el botón de + para ir sumarizando de 1 en 1

* Con la opción de * Escaneo Consecutivo el cual le permite sumarizar la cantidad 1 en 1 al escanear el código de barras, dicha opción puede habilitarla en la sección de Configuración / Inventarios

![IMG](2.jpeg)

Una vez contado se agregará a la cuadricula inferior

![IMG](3.jpeg)

También puede actualizar la ubicación de los artículos contados, ingrese el nombre de la ubicación ejemplo: PAS1, ESTANTE1, PARED10, etc, de clic en GUARDAR

![IMG](4.jpeg)

La ubicación aparecerá en la parte superior y cuando en SAIT realice los ajustes tendrá la opción de actualizar la ubicación de este conteo

![IMG](5.jpeg)





