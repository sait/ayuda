+++
title = "Modo cambios de precios"
description = ""
weight = 3
+++

<h3 class="text-primary"> Requerimientos técnicos </h3>

- Ejecutar en el servidor el instalador de SAIT Móvil Server [ver documentación](/sait-movil/intalacion-sait-movil-server/)

- Miniprinter bluetooth de **ancho de 80mm**

- Dispositivo Android (teléfono o tablet)

- Scanner Bluetooth opcional 

- Red wifi con buena estabilidad 

<h3 class="text-primary"> Pre-requisitos </h3>

- Para acceder a la aplicación es necesario que el número de vendedor sea igual al numero de usuario

- Contar con la versión 2024.23 o posterior en donde se agregó la funcionalidad en 'Inventario / Actualizar precios y Costos / Historial de Precios'. [Documentación ](/sait-erp/8-capacitacion-en-modulo-inventario/e-actualizar-precios-y-costos/) 

<h3 class="text-primary"> Funcionamiento </h3>

1)Ejecutar la app y seleccionar Cambios de Precios

![IMG](cambios-de-precio.png)

2)Al acceder a esta ventana aparecerán todos los cambios de precios que el sistema detectó 

![IMG](1.jpg)

3)Al realizar un cambio de precio en SAIT, la aplicación reconocerá el cambio y lo mostrará en la aplicación

![IMG](2.png)

![IMG](3.jpg)

4)Al tener su impresora Bluetooth configurada aquí mismo puede decidir cuantas etiquetas imprimir para actualizar precios en sus estantes, para esto solo debe seleccionar el ícono verde de la impresora 

![IMG](4.jpg)

Se abrirá la siguiente ventana en donde puede seleccionar la cantidad a imprimir

![IMG](5.jpg)

5)Si desea quitar algún artículo de la lista de cambios de precios, solo tiene que arrastrar a la izquierda 

![IMG](6.jpg)

![IMG](7.jpg)