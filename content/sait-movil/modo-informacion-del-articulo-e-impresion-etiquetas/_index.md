+++
title = "Modo información del artículo e impresión de etiquetas"
description = ""
weight = 4
+++

<h3 class="text-primary"> Requerimientos técnicos </h3>

- Ejecutar en el servidor el instalador de SAIT Móvil Server [ver documentación](/sait-movil/intalacion-sait-movil-server/)

- Miniprinter bluetooth de **ancho de 80mm**

- Dispositivo Android (teléfono o tablet)

- Scanner Bluetooth opcional

- Red wifi con buena estabilidad 

<h3 class="text-primary"> Pre-requisitos </h3>

- Para acceder a la aplicación es necesario que el número de vendedor sea igual al numero de usuario

<h3 class="text-primary"> Funcionamiento </h3>

1)Ejecutar la app y seleccionar Información del Artículo

![IMG](1.png)

2)En la parte inferior de la pantalla notará que aparece el botón de CONFIGURAR IMPRESORA

![IMG](1.1.jpg)

3)Deberá refrescar la búsqueda 

![IMG](2.jpeg)

4)Seleccione la impresora e imprima una hoja de prueba que compruebe está funcionando correctamente, también puede elegir entre el tipo de etiqueta 

![IMG](3.jpeg)

5)Al consultar un artículo ingresando la clave del artículo o usando un scanner, se mostrará la siguiente información de

- Clave del artículo

- Descripción

- SKU (Código de Barras)

- Precio público

- Unidad de venta

- Existencia 

- Último costo  (esta opción se puede configurar para mostrarlo o no en la sección de Configuración, activando la opción Modo Checador)

![IMG](4.jpg)

![IMG](ejemplo.jpg)

