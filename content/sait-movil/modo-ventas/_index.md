+++
title = "Modo ventas"
description = ""
weight = 1
+++

La funcionalidad de ventas con SAIT Móvil es ideal para aquellas empresas se enfocan en la atención de clientes, con SAIT Móvil tus vendedores que se encuentran en piso podrán generar notas de venta a sus clientes dando seguimiento a cada uno de los productos que el cliente desea comprar ademas podrás otorgar descuentos en ese mismo instante todo mientras el vendedor es en contacto con el cliente en cualquier parte de la empresa. 

Una vez agregados todos los artículos a la nota de venta, se procesa y el cliente pasa a caja a pagar.

<h3 class="text-primary"> Requerimientos técnicos </h3>

- Ejecutar en el servidor el instalador de SAIT Móvil Server [ver documentación](/sait-movil/intalacion-sait-movil-server/)
- Dispositivo Android (teléfono o tablet)
- Scanner Bluetooth opcional
- Red wifi con buena estabilidad 

<h3 class="text-primary"> Pre-requisitos </h3>

- Para acceder a la aplicación es necesario que el número de vendedor sea igual al numero de usuario
- El cliente cero debe de existir como "Público en General"
- Todos los clientes deben de tener asignado un precio de lista 
- Ejecutar el siguiente comando en la empresa: alter table cajarem add numest c(2)

<h3 class="text-primary"> Funcionamiento </h3>

1)Ejecutar la app y seleccionar Notas de Ventas

![IMG](ventas.png)

2)Para agregar artículos a la venta puede usar el scanner de códidos de barras, puede teclear directo la clave del artículo o puede seleccionar el ícono de la lupa para buscar por descripción

![IMG](ventas1.png)

3.Al ir agreando artículos se verá de la siguiente manera, al seleccionar el ícono del lápiz, podrá realizar distintos procesos

![IMG](ventas2.png)

4.Podrá modificar la cantidad a vender, puede modificar el precio o puede otorgar descuentos, seleccione GUARDAR al terminar de hacer los ajustes

![IMG](ventas3.png)

5.Al otorgar un descuento se verá de la siguiente manera

![IMG](ventas4.png)

6.Para procesar la nota deberá seleccionar el ícono del carrito de compra en la parte inferior, si está todo correcto, seleccione PAGAR para que la nota sea procesada

![IMG](ventas5.png)

7.Posteriormente deberá pasar a caja y ir al menú de Caja / Recibis Pagos de Contado (F12) en donde aparecerá la nota realizada con el dispositivo móvil

![IMG](caja1.png)

8.Deberá cubrir el total de la nota y listo

![IMG](caja2.png)