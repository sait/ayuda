+++
title = "Instalación de SAIT Móvil Server"
description = ""
weight = 6
+++

Para utilizar la app en cualquiera de sus modalidades (ventas, conteos de inventarios, etc) es necesario correr un instalador en el servidor, a continuación las intrucciones para este paso

<h3 class="text-primary"> Instalación de SAIT Móvil Server </h3>

Descargar SAIT Movil Server https://sait.mx/download/sait-server-v2-instalador-2.2.2.exe

![IMG](1.png)

![IMG](2.png)

El instalador le abriará la ruta en donde normalmente está la carpeta de instalación de SAIT, si no es la ruta correcta, deberá dar clic en Examinar y seleccionar la carpeta correcta

![IMG](3.png)

![IMG](4.png)

![IMG](5.png)

![IMG](6.png)

![IMG](7.png)

<h3 class="text-primary"> Obtener dirección IP de servidor SAIT </h3>

En el servidor, ir al Buscador de Windows y teclear la palabra CMD 

![IMG](CMD.png)

Escribir ipconfig y dar [Enter] en la ventana de comandos, el dirección IPv4 es el dato que necesitamos anotar

![IMG](IP.png)

Este dato lo utilizará al abrir la aplicación la primera vez, posteriormente ya no será requerido

![IMG](ip-cel.png)


<h3 class="text-primary"> Abrir puerto 48804 en Firewall  </h3>

1.Ir a Panel de Control / Sistema y seguridad / Firewall de Windows / Configuración Avanzada

![IMG](configavanzada.png)
 
2.Ir a Reglas de Entrada 

![IMG](reglasentrada.png)

3.Ir a Nueva Regla

![IMG](nuevaregla.png)
 
4.En tipo de reglas, seleccionar Puerto, clic en **[Siguiente]**.

![IMG](puertoocf.png)

5.Seleccionar Regla TCP e ingresar el puerto 48804

Clic en **[Siguiente]**.

![IMG](48804.png)

6.En la siguiente ventana seleccionar Permitir la Conexión, clic en **[Siguiente]**.

![IMG](permitirconexion.png)

7.Seleccionar las 3 opciones que aparecen en pantalla, clic en **[Siguiente]**.

![IMG](opcionespalomear.png)
 
8.En la ventana de Nombre, ingresar el nombre de: SAITWS. 

El campo descripción es opcional, clic en **[Finalizar]**.

![IMG](saitws.png)

<h3 class="text-primary"> Conocer IP del servidor </h3>

1.En el servidor, ir al Buscador de Windows y teclear la palabra CMD 

2.Escribir ipconfig y dar [Enter] en la ventana de comandos y escribir ipconfig y dar enter

![IMG](ipconfig1.png)

El dato que necesitamos es Dirección IPv4

![IMG](ipconfig2.png)

3.Abrimos la aplicación de SAIT Móvil e ingresamos la dirección ip, después daremos clic en Revisar Conexión

![IMG](ip1.jpg)

Si la conexión es correcta, nos aparecerá de esta manera

![IMG](ip2.jpg)

Finalmente nos aparecerá el listado de empresas, seleccionaremos a la que deseamos ingresar y presionamos Continuar 

![IMG](ip3.jpg)

