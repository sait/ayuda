+++
title = "Bienvenido"
description = ""
date = "2017-04-24T18:36:24+02:00"

+++

# Documentación SAIT

<p  style="text-align: center;">Bienvenido a nuestro manual en línea, aquí encontrará toda la información que necesita para aprender a usar SAIT Software Administrativo.</p> 
 
<div style="display: flex; justify-content: space-around; flex-wrap: wrap;">
<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">CFDI 4.0</h4>
	<img src="cfdi4.0.png" style="max-height: 250px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/cfdi4/">Ver más..</a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Solución de Problemas</h4>
	<img src="soporte.gif" style="max-height: 250px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/temas-soporte/">Ver más..</a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Mejoras de Últimas Versiones</h4>
	<img src="update.jpg" style="max-height: 250px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/sait-erp/bitacora/">Ver más..</a>

</div>
</div>


</div>
