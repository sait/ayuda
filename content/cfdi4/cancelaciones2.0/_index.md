+++
title="Cancelaciones 2.0"
description=""
weight = 2
+++

A partir del 1 de enero de 2022, todas las solicitudes de cancelación deberán contener el motivo de cancelación del CFDI. 

Los motivos de cancelación permitidos son los siguientes:

Clave | Descripción
---|---
01 | Comprobante emitido con errores con relación
02 | Comprobante emitido con errores sin relación
03 | No se llevó a cabo la operación
04 | Operación nominativa relacionada en una factura global

Dicho cambio está disponible a partir de la versión 2022.3.0.

<h3 class="text-primary">01 - Comprobante emitido con errores con relación</h3>

Este caso aplica cuando la factura generada contiene un error en la clave del producto, valor unitario, descuento o cualquier otro dato, por lo que se debe reexpedir. En este caso, primero se sustituye la factura y cuando se solicita la cancelación, se incorpora el folio de la factura que sustituye a la cancelada.


![IMG](1.png)


### PASO 1:

Primeramente, deberá hacer la nueva factura que sustituye la anterior, procesamos el documento y daremos clic en el botón de Agregar CFDI Relacionados. 

![IMG](2.png)

### PASO 2:

Seleccionamos el tipo de relación, agregamos el folio de la factura a cancelar (se puede agregar directo el folio de la factura en el campo Folio o UUID y dando clic en el botón de + para agregarlo, en caso de que no se conozca el Folio puede dar clic en el botón de interrogación para abrir la ventana de búsqueda) después daremos clic en F8 Continuar 

![IMG](3.png)

### PASO 3:

Listo, ya hemos emitidos la nueva factura corregida, ahora tendrá que cancelar la factura anterior, nos dirigiremos a la consulta de la factura y daremos clic en Cancelar donde veremos que automáticamente la ventana nos arroja el UUID de la nueva factura

![IMG](4.png)

### PASO 4:

Llenamos los datos de Folio del Documento a Cancelar, los Comentarios o Motivos de Cancelación, dejamos seleccionado el motivo para el SAT 01- Comprobante con errores con Relación y damos clic en Sí para cancelar el documento y listo. 

![IMG](5.png)

<h3 class="text-primary">02 - Comprobante emitido con errores sin relación</h3>


Este supuesto aplica cuando la factura generada contiene un error en la clave del producto, valor unitario, descuento o cualquier otro dato y no se requiera relacionar con otra factura generada.

![IMG](caso2.png)

<h3 class="text-primary">03 - No se llevó a cabo la operación</h3>

Este supuesto aplica cuando se facturó una operación que no se concretó.

![IMG](caso3.png)

<h3 class="text-primary">04 - Operación nominativa relacionada en una factura global</h3>

Este supuesto aplica cuando se incluye una venta en la factura global de operaciones con el público en general y posterior a ello, el cliente solicita su factura nominativa, lo que conlleva a cancelar la factura global y reexpedirla

![IMG](caso4.png)