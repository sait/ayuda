+++
title="Consideraciones timbrado 4.0"
description=""
weight = 3
+++


- Timbrado 4.0 en Modo Pruebas

 - Cambiar el RFC AAA010101AAA por XIA190128J61
 - Nombre de Empresa debe ser:XENON INDUSTRIAL ARTICLES
 - Clave de Activacion: 3NsQN9VKPQ8fLB1VqYDw1hvqafCp21NOkad825zII1YY0A==
 - Contrato Activacion: MSL
 - Agregar CSD: 30001000000500003442 [clic aquí para descargar sello](CSD_XIA190128J61.zip)
 - Contrasena: 12345678a
