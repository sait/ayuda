+++
title="Seguimiento a Cancelación de CFDIs"
description=""
weight = 5
+++

A partir de la versión 2022.9.0 SAIT cuenta con una nueva funcionalidad para dar un seguimiento a la cancelación de CFDIs.

Con esta nueva opción podrá:

* Ver los CFDIs que fueron mandados cancelar al SAT y aún se encuentran en proceso o fueron rechazados.

* Al cancelar una factura con status "Cancelable con Aceptación" se manda la solicitud al SAT, pero la factura queda vigente en SAIT, hasta que el SAT reporte el CFDI como cancelado, es decir cuando el cliente aceptó la cancelacion.

* Al consultar una factura que fue mandada cancelar en SAT, aparece un botón "Seguimiento a Cancelación".

* La nueva ventana "Seguimiento a Cancelación de CFDI" muestra cómo fue cambiando el status de cancelación para ese CFDI en el SAT.

Dicha opción la podrá encontrar en el menú de Ventas / Consultar CFDIs de Ventas donde se agregó el botón de **[Cancelaciones]** que abre la ventana de "Cancelaciones de CFDI".

![IMG](1-boton-cancelaciones.png)

La ventana se mostrará una barra da progreso ya que se hará una verificación de status actual de las facturas Pendientes por Cancelar.

![IMG](2-verificando-en-sat.png)

<h3 class="text-primary">Procesos</h3>

* [Intento de cancelación](#intento-de-cancelacion)

* [Ir a ventana de proceso de cancelaciónn](#ir-a-ventana-de-proceso-de-cancelacion)

* [Ver seguimiento](#ver-seguimiento)

### Intento de cancelacion

Al cancelar una factura en SAIT con una versión igual o posterior a la 2022.9.0. 

![IMG](3-confirmar-cancelacion.png)

La cual entró en el proceso de cancelación se mostrará el siguiente mensaje:

![IMG](4-sol-iniciada.png)

### Ir a ventana de proceso de cancelacion

Para dar seguimiento a al proceso de cancelación puede hacerlo desde la consulta individual de la factura en Ventas / Consulta Individual. 

![IMG](5-boton-seguimiento.png)


### Ver seguimiento

Al dar clic en el botón **[Seguimiento a Cancelación]** le mostrará la siguiente ventana en donde le aparece el historial del proceso de cancelación de la factura.

![IMG](6-detallado-seguimiento.png)

Si su cliente rechazó la cancelación, nos aparecerá en la ventana de seguimiento y si se requiere, puede volver a mandar la solicitud de cancelación.

![IMG](7-rechazada.png)

Si la factura aparece con Status Cancelada quiere decir ya está cancelada en el SAT por lo que deberá cancelarla en SAIT para que aparezca como cancelada.

![IMG](8-status.png)