+++
title="Ajuste de Formatos CFDI 4.0"
description=""
weight = 3
+++

La última versión de SAIT incluye los siguientes formatos internos ya actualizados:

- CFDI Básico
- CFDI Basico Alternativo
- Oficial del SAT
- CFDI de Pago o Recibo Electrónico de Pago REP
- CFDI de Traslado


Puede seleccionarlos en el menú de Utilerias - Factura Electronica - Config CFDI.


Si usted utiliza formatos personalizados tome nota de las expresiones que debera agregar o modificar:

### Agregar campo Régimen Fiscal del Cliente

```
    - Expresión: cveDescCatCfdi('RegimenFiscal',eval2('oCfdi.Comprobante.Receptor.RegimenFiscalReceptor'))
    - PrintWhen: oCfdi.Comprobante.Version=="4.0"
```

![IMG](regimen.png)



### Agregar campo Versión del CFDI (Ahora es 4.0)
```
    - Etiqueta: CFDI 4.0
    - PrintWhen: oCfdi.Comprobante.Version=="4.0"
```

![IMG](etiqueta.png)


Recuerde que el sistema toma las siguientes prioridades para decidir el formato a usar:

- Formato antiguos
- Formato interno modificado
- Formato interno

- **SE RECOMIENDA SIEMPRE USAR LOS FORMATOS INTERNOS** ya que de esta manera usted tendrá siempre el PDF del CFDI siempre actualizado, en caso de hace una personalizacion cada actualizacion del SAT deberá modificar el formato de factura, para incluir los nuevos campos que el SAT agrega.

- ¿Cómo saber qué formato estoy usando?


 - 1) Ir a Utilerias - Modificar Formatos - Facturas y NC

 - 2) Si existen formatos ahi, usted esta usando los "Formatos Antiguos" NO RECOMENDADOS

 - 3) Revisar el folder "Formatos" en la emmpresa, normalmente es: C:\SAIT\CIA001\Formatos en caso que tenga varias empresas debera revisar cual es el folder de datos de la empresa.

 - 4) Si hay archivos en el folder "Formatos" entonces usted está usando "Formato interno modificado" **NO RECOMENDADO**

 - 5) Si no se cumplen las condiciones anterior, entonces usted está usando los Formatos Internos **RECOMENDADOS**

 - 6) Para seleccionar el formato interno a usar, vaya a: Utilerías - Factura Electronica Configurar CFDI

- Expresion a agregar en los formatos antiguos y modificados



 
