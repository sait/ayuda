+++
title="Falla de Timbrado # 40157: Este atributo, debe contener un valor del catálogo c_RegimenFiscal"
description=""
weight = 1
+++


#### Causa

El cliente no tiene registrado Régimen Fiscal.

#### Solución

Ir a Ventas - Catálogo de Cliente y registrar el regimen fiscal  tal y como aparece en la constancia de situacion fiscal del cliente.


