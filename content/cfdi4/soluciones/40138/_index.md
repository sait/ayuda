+++
title="Falla de Timbrado # 40138: Este atributo, debe encontrarse en la lista de RFC inscritos no cancelados en el SAT"
description=""
weight = 1
+++

#### Causa

El nombre del emisor debe ser exactamente el registrado en la constancia del SAT, debe aparecer en mayúsculas y no incluir régimen de capital.

El campo nombre de emisor, no corresponde con el nombre del titular del certificado de sello digital del emisor.

#### Solución

Ir a Utilerías - Configuración General y quitar el régimen capital del nombre de la empresa.

Deberá quitar del Nombre de la Empresa, los sufijos del régimen de capital
- SA de CV
- S DE RL
- SRL
- SA

El nombre debera ponerlo en mayusculas y sin el régimen de capital.

Revise que NO tenga doble espacio entre el nombre y el apellido.