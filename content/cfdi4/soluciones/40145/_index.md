+++
title="Falla de Timbrado # 40145: Este atributo, debe pertenecer al nombre asociado al RFC registrado en el atributo Rfc del Nodo Receptor"
description=""
weight = 1
+++

#### Causa

Nombre del Cliente, debe ser exactamente el mostrado en constancia del SAT, aparecer en mayúsculas,  NO incluir régimen de capital.

#### Solución

Ir a Ventas - Catálogo de Cliente y modificar el nombre del cliente. Escribirlo tal y como aparece en la constancia del SAT, sin el régimen de capital.

Deberá quitar del Nombre, los sufijos del régimen de capital:
- SA de CV
- S DE RL
- SRL
- SA

El nombre debera ponerlo en mayusculas y sin el régimen de capital.

