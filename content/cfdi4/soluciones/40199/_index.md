+++
title="Falla de Timbrado # 40199: Este atributo, debe cumplir con las reglas de validación definidas"
description=""
weight = 1
+++


#### Causa

El cliente no tiene registrado el Código Postal

#### Solución

Ir a Ventas - Catálogo de Cliente y registrar el codigo postal tal y como aparece en la constancia de situación fiscal del cliente.

