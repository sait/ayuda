+++
title="Falla de Timbrado # 40158: El régimen fiscal que se registre en este atributo debe corresponder con el tipo de persona del receptor"
description=""
weight = 1
+++


#### Causa

El Régimen Fiscal indicado para el Cliente, NO es el registrado en el SAT o no corresponde al tipo de personal moral o física.


#### Solución

Ir a Ventas - Catálogo de Cliente y registrar el regimen fiscal  tal y como aparece en la constancia de situacion fiscal del cliente.


