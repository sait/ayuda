+++
title="Falla de Timbrado # 40148: Este atributo, debe pertenecer al nombre asociado al RFC registrado en el atributo Rfc del Nodo Receptor"
description=""
weight = 1
+++


#### Causa

El Código Postal del cliente, debe ser exactamente el registrado en la constancia del SAT.

#### Solución

Ir a Ventas - Catálogo de Cliente y modificar el código postal tal y como aparece en la constancia de situacion fiscal del cliente.


