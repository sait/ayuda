+++
title="Retenciones en Compras"
description=""
weight = 4
+++

De acuerdo con el Servicio de Administración Tributaria (SAT) las Personas Físicas que tributen en el nuevo Régimen Simplificado de Confianza estarán sujetas a la retención del 1.25% cuando estas tengan operaciones con Personas Morales.

Dado este escenario en SAIT se habilito la opción de capturar retención de ISR al registrar comparas

1.Para esto se deberá dirigirse al menú de Compras  / Registro de Compras

2.Capturar los datos encabezado de la compra:

* a.	Proveedor

* b.	Fecha
	
* c.	Divisa 

* d.	Folio de factura proveedor

3.Después de capturar  los datos de encabezado de la compra indicar el importe de retención de ISR en la factura

![IMG](1.JPG)

4.Continuar con la captura de la compra

![IMG](2.JPG)

### **IMPORTANTE**

En el formato de compra deberá agregar el importe referente a las retenciones, deberá seguir las siguientes instrucciones:

1.Consulte la factura en Compras / Consultas Individual / Imprimir y de clic en [Formato]

![IMG](3.jpg)

2.Deberá de localizar la barra de herramientas “Report Controls”. En caso de no tenerla visible, para habilitarla deberá de dirigirse al menú de: 

View / Report Controls Toolbar y hacer un clic sobre la opción de [Field]:

![IMG](4.jpg)

3.Posteriormente deberá dar clic sobre la parte del documento que desee que aparezca este campo, en este caso será al final en la sección de totales, al final del formato, aparecerá la siguiente ventana y llenaremos la información de la siguiente manera: DOCUM.RETENCION2

![IMG](5.jpg)

4.Una vez que haya colocado los datos, deberá dar clic en [OK]

5.Adicionalmente podemos agregar la leyenda que deseemos con la siguiente opción de texto, como se muestra en la siguiente imagen:

![IMG](6.jpg)

6.Y daremos clic en la parte en donde queramos agregar la leyenda, por ejemplo:

![IMG](7.jpg)

7.Agregar al total el importe de la retención, dar doble clic en el campo del total de documento

![IMG](8.jpg)

8.Sustituir el contenido del campo Expression  por la sentencia: 

**Docum.IMPORTE-Docum.DESCUENTO+Docum.IMPUESTO1+Docum.IMPUESTO2-Docum.RETENCION2** 

9.Una vez que haya colocado los datos, deberá dar clic en [OK]

10.Para finalizar deberá ir a al menú de File y dar clic en Save. Y listo, de esta manera su formato personalizado ya mostrará las cantidades correspondientes a retención de ISR

![IMG](9.jpg)


