+++
title="Validar Datos Fiscales del Cliente al emitir CFDI 4.0"
description=""
weight = 6
+++

SAIT Software cuenta con una nueva funcionalidad que evitará fallas al momento de timbrar CFDI de versión 4.0, ya que ahora usted podrá validar los datos fiscales del cliente en las listas del SAT, antes de mandar timbrar.

{{%alert success%}} AVISO: Deberá de asegurarse en contar con una Versión Igual o Superior a 2023.3 {{%/alert%}}

Con esta nueva opción, se validan los siguientes datos del Cliente:

* RFC, Debe encontrarse inscrito en la lista de RFC inscritos y no cancelados del SAT.
* Nombre, Debe corresponder al nombre de la razón social plasmado en la constancia de situación fiscal.
* Código Postal, Debe corresponder al domicilio fiscal plasmado en la constancia de situación fiscal.
* Régimen Fiscal, La clave de régimen fiscal debe corresponder al tipo de persona (física o moral) y debe corresponder al plasmado en la constancia de situación fiscal.
* Uso del CFDI, La clave de Uso de CFDI seleccionado debe ser aplicable según el régimen fiscal que ejerza el receptor.

{{%alert success%}} Al capturar erróneamente alguno de estos datos, es motivo para que no se genere el timbre fiscal de su documento. {{%/alert%}}


Te compartimos a continuación los pasos para poderlo activar dentro de: **SAIT ERP o SAIT Básico**:


<div style="display: flex; justify-content: space-around; flex-wrap: wrap;">

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Configuración SAIT</h4>
	<img src="configuracion.png" style="max-height: 200px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/cfdi4/validar-datos-del-cliente-cfdi40/configuracion-sait">Ver mas..</a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Funcionamiento</h4>
	<img src="uso.png" style="max-height: 200px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/cfdi4/validar-datos-del-cliente-cfdi40/funcionamiento-prevalidacion">Ver mas..</a>
</div>
</div>


</div>

{{% children  %}}
