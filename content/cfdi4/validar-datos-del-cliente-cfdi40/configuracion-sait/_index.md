+++
title="Configuración Previa en SAIT"
description=""
weight = 1
+++

Antes de empezar de validar los  datos del cliente para CFDI 4.0, es necesario realizar el siguiente paso en las plataformas de SAIT Básico y SAIT ERP respectivamente.

{{%alert success%}} AVISO: Deberá de asegurarse en contar con una Versión Superior 2023.3, de SAIT Básico o SAIT ERP {{%/alert%}}


## Configurar SAIT para validar datos del Cliente.

Los pasos para activarlo, son los siguientes:

* Ir al menú de: **Utilerías / Factura Electrónica / Configurar CFDI**
* En la ventana de: **Configuración de Factura Electrónica** habilitar la casilla "Prevalidar datos del cliente en servidores del SAT" tal como se muestra a continuación:

![IMG](imagen1.png)

* Para guardar ls cambio, clic en el botón **[Guardar Configuración]**.





