+++
title="Funcionamiento de la Validación de Datos Fiscales del Cliente"
description=""
weight = 2
+++


{{%alert success%}} La Validación de los datos fiscales del cliente, se encuentra disponible en las ventanas de: {{%/alert%}}

* Generar una Factura desde opción de: "Ventas / Registrar Ventas F4"
* Generar Nota de Crédito desde opción: "Ventas / Devoluciones y Notas de Crédito" 
* Generar Factura desde opción: "Caja / Registrar Ventas"
* Generar Factura desde opción: "Caja / Facturar Nota de Venta"

## Ejemplo de Uso

* Dirigirse al menú de: **Ventas / Registrar Ventas F4.**
* En Tipo, seleccionamos "Factura".
* En el campo **CLIENTE**, vamos a seleccionar alguno. (Nuestro cliente tiene mal el dato del Código Postal).
* Continuamos con la captura de la demás información del documento.
* Hacemos clic en el botón **[Procesar F8]**.
* Se indican y selccionan las demás opciones del CFDI, como se muestra en la siguiente imagen:
* ![IMG](imagen2.png)
* Clic en el botón **"F8 Continuar"**.
* En estos momentos se tomarán los datos del Receptor/Cliente y se validaron en los servidores del SAT. En caso de encontrarse una inconsistencia, se nos mostrará un mensaje como el siguiente:
* ![IMG](imagen3.png)
* En caso de encontrar fallas en los datos fiscales del cliente, se presentan tres valores:

{{%alert success%}} Código SAT: Codigo de error según la matriz de errores del SAT para CFDI 4.0 <br> Error: Error corto donde se explica en pocas palabras cual es el error encontrado en los datos del receptor<br>Descripcion completa del error según la matriz de errores del SAT para CFDI 4.0{{%/alert%}}


* Al dar clic en [Aceptar], se nos devolverá a la ventana ‘Información de CFDI’, dándonos la oportunidad de ir al Catálogo de Clientes y capturar el Código Postal correspondiente al Receptor/Cliente.
* Una vez corregido esto, en caso de no encontrarse alguna otra inconsistencia, la factura en cuestión procederá a timbrarse correctamente en el SAT. 







