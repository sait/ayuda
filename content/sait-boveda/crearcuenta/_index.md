+++
title = "Crear Cuenta"
description = ""
weight = 1
+++

<div class="text-center">Comience a usar SAIT Bóveda</div> 

#### Lo primero que tiene que hacer es dirigirse a   http://boveda.sait.mx/app/#/login

#### De clic en **Crear Cuenta**

![IMG](inises.png) 

#### Se le presentara la pantalla de registro

![IMG](crearcuenta2.png) 

#### Se le dará la notificación de que revise su correo electrónico para confirmar cuenta.
![IMG](confirmar.png) 
#### El coreo electrónico pedirá confirmar su dirección de correo, clic en confirmar cuenta
![IMG](confirmaremail.png) 

#### Se le redirigirá a una pagina en la que debe ingresar una nueva contraseña para terminar de confirmar su cuenta.
![IMG](confirmar2.png) 

#### Ya confirmada su cuenta podrá iniciar sesión.
![IMG](confirmar3.png) 

#### Al iniciar sesión lo primero que tiene que hacer es seleccionar la empresa a la cual ingresara (Una cuenta de correo puede tener acceso a varias empresas diferentes)
![IMG](seleccionaempresa.png) 

#### Después de seleccionar se le presentara la pantalla principal de bóveda con los datos de la empresa seleccionada.
![IMG](principal.png) 