+++
title = "Registrar Certificados"
description = ""
weight = 6
+++

El registro de los certificados en necesario para el funcionamiento de algunas características de SAIT Bóveda, estos se usaran para comprobar la identidad del usuario ante el SAT.

**Certificados**:

*	**CSD**: Se usara para el funcionamiento del buzón inteligente, con este sello recibiremos las solicitudes de cancelación.
*	**e.Firma (FIEL)**: Se usara para hacer las solicitudes de descarga de comprobantes.

##### Registrar certificados.

*	Dirigirse a la sección **Certificados** y haga clic en **agregar**
![IMG](certificados1.png) 

*	En la ventana agregue los archivos que se solicitan y la contraseña del certificado.
![IMG](certificados2.png)

*	Listo! el certificado ha sido registrado.
![IMG](certificados3.png)