+++
title = "Sincronizar Correo"
description = ""
weight = 5
+++

##### El registro de correo electronico es un paso opcional para descargar los CFDIs que lleguen a su cuenta de correo electronico.


*	Dirígase a la seccion de correos y de clic en el boton agregar 
![IMG](correo1.png) 

*	Aparecera una ventana donde debe colocar la informacion de su cuenta de correo 
	*	Es necesario que coloque tambien la URL del servidor IMAP y el puerto IMAP, estos varian dependiendo de su proveedor de correo electronico 
	* En caso de usar Gmail se usan estos datos:
		*	**Servidor IMAP**: imap.gmail.com	
		*	**Puerto IMAP**: 993
![IMG](correo2.png)

*	Despues de comprobar las credenciales el correo aparecera registrado en la tabla de correos y se empezaran a recibir los CFDIs en el Buzon.
![IMG](correo3.png) 

