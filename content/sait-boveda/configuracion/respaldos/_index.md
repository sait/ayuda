+++
title = "Respaldos en Bóveda SAIT"
description = ""
weight = 7
+++

<h2 class="text-primary">Configuración de Respaldos</h2>

* [Descargar e instalar saitbkup.exe](#descargar-e-instalar-saitbkup)
* [Establecer contraseña de acceso a la configuración de respaldos automáticos](#establecer-contraseña-de-acceso-a-la-configuracion-de-respaldos-automaticos)
* [Agregar nueva cuenta SAIT Bóveda](#agregar-nueva-cuenta-sait-boveda)
* [Programar nuevo respaldo](#programar-nuevo-respaldo)
* [Modificar configuración del respaldo programado](#modificar-configuracion-del-respaldo-programado)

<h2 class="text-primary">Consulta de Respaldos</h2>

* [Información resguardada](#informacion-resguardada)
* [Detalles del respaldo](#detalles-de-respaldo)
* [Descargar respaldo](#descargar-respaldo)
* [Información de respaldos programados](#informacion-de-respaldos-programados)

### Descargar e instalar saitbkup

Para realizar la configuración de respaldos en SAIT Bóveda, favor de seguir las siguientes indicaciones:

Descargar e instalar el programa de respaldos saitbkup

1.Haga clic en el siguiente enlace para descargar el programa de respaldos:

Para descargar el ejecutable de clic [aquí](saitbkup.exe)

2.Ejecute el instalador dando clic sobre el icono saitbkup.exe

![img](Screenshot_74.png)

3.Seleccione la opción [Acepto el acuerdo] y haga clic en [Siguiente]:

![img](Screenshot_75.png)

4.Ahora el programa está listo para instalar. Haga clic en [Instalar]:

![img](Screenshot_76.png)

5.Esperar a que se realice la instalación del programa.

![img](Screenshot_77.png)

6.Y por último haga clic en [Finalizar].

![img](Screenshot_78.png)


### Establecer contraseña de acceso a la configuracion de respaldos automaticos

Posterior a la instalación, automáticamente se abrirá una pestaña en su navegador de internet.

1.Se solicitará establecer una contraseña para poder acceder. Haga clic en [Guardar].

![img](Screenshot_80.png)

2.Capture de nuevo la contraseña y haga clic en [Iniciar Sesión].

![img](Screenshot_81.png)

3.Y listo, se mostrará la pestaña de configuración de respaldos programados.

![img](Screenshot_82.png)


### Agregar nueva cuenta SAIT Boveda

Para programar un nuevo respaldo, debe de contar con una cuenta vigente de Bóveda SAIT.

1.Para registrar su cuenta, vaya al apartado de Cuentas y haga clic en [Agregar Cuenta].

![img](Screenshot_83.png)

2.Capture la llave (ApiKey) de su cuenta Bóveda SAIT y haga clic en [Guardar].

![img](Screenshot_84.png)

**La llave de la cuenta (Apikey) es usada para hacer una conexión segura con el servicio de SAIT Bóveda.**

2.1 El ApiKey se encuentra dentro de su cuenta Bóveda SAIT en la sección de configuración

![img](Screenshot_85.png)

3.Listo, la cuenta se ha registrado correctamente.

![img](Screenshot_86.png)

### Programar nuevo respaldo

Para programar un nuevo respaldo, diríjase al apartado de Respaldos Programados.

1.Haga clic en [Programar nuevo respaldo].

![img](Screenshot_87.png)

2.Indique la siguiente información para programar el respaldo:

a. **Cuenta de SAIT Bóveda**, seleccionar la cuenta en la que se guardaran los respaldos.

b. **Nombre del respaldo**, capture el nombre con el que se va a guardar el respaldo.

c. **Directorio a respaldar**, capture el directorio a respaldar. Puede copiar el directorio de la barra de direcciones de la ventana de Windows.

![img](Screenshot_102.png)

d. Realizar respaldos a las: indique la hora exacta en la que se va a iniciar a generar el respaldo de forma automática.

NOTA. Procure programar el respaldo a una hora en la que ya no se esté utilizando el sistema, ya que se puede ralentizar el sistema debido al proceso del respaldo.

3.Haga clic en [Guardar].

![img](Screenshot_90.png)

El límite de archivos a subir es de 15 directorios, si lo desea ampliar nos puede contactar al correo de soporte@sait.com.mx

4.Una vez configurado el respaldo se encontrará en el apartado de Respaldos programados.

El respaldo tendrá un estatus pendiente ya que el respaldo se realizará hasta que se cumpla con la hora asignada. Para realizar el respaldo de manera inmediata, puede dar clic al botón de Realizar ahora, de
esa manera el respaldo comenzará inmediatamente.

![img](Screenshot_91.png)

### Modificar configuracion del respaldo programado

Si desea modificar los parámetros definidos en un respaldo programado, solo debe dar clic en el nombre del respaldo para ver información detallada.

Los datos que se presentarán son:

* Fecha último respaldo, se indica fecha y hora en la que se realizó el último respaldo.

* Fecha siguiente respaldo, se indica fecha y hora en la que se realizará el respaldo.

* Cuenta, se indica la cuenta de SAIT Bóveda en donde guardó el respaldo.

Si desea modificar la configuración del respaldo, haga clic en [Modificar Respaldo Programado].

![img](Screenshot_103.png)

Realice las modificaciones deseadas y haga clic en [Guardar].

![img](Screenshot_104.png)

____________________________________________________

### Informacion resguardada

Ingrese a su cuenta de SAIT Bóveda y diríjase a la pestaña de Respaldos.
En esta sección usted podrá consultar y ver los detalles de todos los respaldos realizados.

![img](Screenshot_95.png)

### Detalles de respaldo

Si desea ver los detalles de un respaldo, haga clic en el nombre del respaldo o al enlace ver detalle.

![img](Screenshot_96.png)

Se presentarán los siguientes datos:

**UUID:** Es el identificador único que se le tiene asignado al respaldo.

**Checksum SHA256:** Es una manera de proteger su información resguardada de cualquier cambio accidental o cualquier cambio de terceros hacia su información, de tal manera que si malguna persona descarga su información que se tiene resguardada en SAIT Bóveda y la modifica a su beneficio gracias al Checksum se dará cuenta que información fue alterada.

**Contraseña Zip:** Es la contraseña que se le asigna a su archivo.zip para poder descomprimido. De tal manera que no cualquier persona pueda descomprimir la información que se encuentra en los respaldos descargados

### Descargar respaldo

Para descargar un respaldo, haga clic en al enlace de descargar.

![img](Screenshot_97.png)

Al completarse la descarga, localice el archivo comprimido en el área de descargas.

Haga clic derecho sobre el archivo y haga clic en [Extraer todo].

**Nota. Este paso puede variar de acuerdo a su software de compresión disponible.**

![img](extraer.png)

Verifique el destino en donde se extraerán los archivos y haga clic en [Extraer].

![img](Screenshot_105.png)


Al descomprimir el archivo se solicitará la contraseña del respaldo, deberá ingresarla y dar clic en [Aceptar].

![img](Screenshot_99.png)

La contraseña requerida se encuentra en SAIT Bóveda, en el detalle del respaldo.

![img](Screenshot_107.png)

Listo, el archivo se podrá descomprimir y la información de su respaldo estará disponible para su uso.

![img](Screenshot_108.png)

### Informacion de respaldos programados

Diríjase a la pestaña de [Respaldos Programados].

En esta sección visualizará todos los respaldos que usted tiene programados en SAIT Bóveda.

Los datos que se presentarán en esta consulta son:

* **ID:** es el identificador único que se le asignó al respaldo programado.

* **Nombre:** es el nombre que se le definió al respaldo.

* **No. de archivos subidos:** indica el número de archivos realizados y subidos a SAIT bóveda.

* **Activo:** indica si el respaldo programado está activo o no para continuar realizándose.

![img](Screenshot_109.png)

Si desea inactivar el respaldo programado haga clic en [Avanzado].

Se mostrará una ventana en donde se podrá activar o desactivar el estado del respaldo.

Para activar el respaldo solo activar la casilla, de lo contrario desactivarla.

![img](Screenshot_110.png)