+++
title = "Accesos"
description = ""
weight = 2
+++


#### En esta pestaña puede conceder acceso a otros usuarios y conceder permisos.
#####	Esta acción registra el acceso de un usuario para ver o modificar la información de una empresa, en caso de tener varias empresas debe registrar el usuario en cada empresa que quiera que tenga acceso. 

![IMG](1.png) 
#### Ingrese el correo del usuario y el nivel de privilegios que tendrá.
*	**Administrador**: Acceso a TODO.
*	**Auxiliar**: 
	* No descarga ZIP.
	* No descarga SAT.
	* No agregar-modificar Reglas de cancelación.
	* No Modificar configuración.
	* No Agregar-modificar accesos.
	* No agregar-modificar correos sync.

![IMG](2.png) 
#### Quedara registrado con un estatus de **PENDIENTE**.
![IMG](3.png) 
#### El usuario recibirá un correo como ese en tu bandeja de entrada para confirmar su cuenta.
![IMG](configuracion23.png) 
#### Al confirmar su cuenta el estatus de la cuenta del usuario cambia a **Confirmada**.
![IMG](5.png) 
