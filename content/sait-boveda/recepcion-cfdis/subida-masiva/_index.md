+++
title = "Subida Masiva"
description = ""
weight = 2
+++

Para subir comprobantes de forma masiva que tengas en tu equipo local hemos diseñado una aplicación de escritorio que te permite hacerlo en pocos pasos.

[https://sait.mx/download/saituploader.exe](https://sait.mx/download/saituploader.exe)


![IMG](uploader.png)