+++
title = "Por Correo"
description = ""
weight = 2
+++

Al configurar tu cuenta de correo en Bóveda se estará revisando constantemente tu bandeja de entrada en búsqueda de comprobantes fiscales.

*	Diríjase a la sección de correos y de clic en el botón agregar 
![IMG](correo1.png) 

*	Aparecerá una ventana donde debe colocar la información de su cuenta de correo 
	*	Es necesario que coloque tambien la URL del servidor IMAP y el puerto IMAP, estos varian dependiendo de su proveedor de correo electronico 
	* En caso de usar Gmail se usan estos datos:
		*	**Servidor IMAP**: imap.gmail.com	
		*	**Puerto IMAP**: 993
![IMG](correo2.png)

*	Después de comprobar las credenciales el correo aparecerá registrado en la tabla de correos y se empezaran a recibir los CFDIs en el Buzón.
![IMG](correo3.png) 