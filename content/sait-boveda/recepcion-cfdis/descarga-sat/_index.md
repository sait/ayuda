+++
title = "Descarga del SAT"
description = ""
weight = 3
+++


El servicio de descarga masiva de comprobantes del SAT esta integrado en SAIT Bóveda sin tener que entrar al portal del SAT, puedes dejar tus solicitudes de descarga y una vez procesadas tus comprobantes estarán resguardados en SAIT Bóveda.

**Es un requisito tener registrados sus certificados en SAIT Bóveda**

*	Diríjase a la sección de Descargas donde podrá ver el listado de solicitudes de descarga pendientes

![IMG](masiva.png) 

* Para hacer una solicitud de descarga de clic en el botón agregar 

![IMG](masiva2.png)

*	Aparecerá una ventana donde debe seleccionar los parámetros para descarga.
	*	Seleccione el intervalo de fechas para la descarga y si serán emitidos o recibidos.

*	El estado de la solicitud de descarga cambiara conforme a lo que responda el SAT, cuando se procese de manera automática se agregaran los comprobantes a la Bóveda.
![IMG](masiva3.png) 