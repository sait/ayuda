+++
title = "Recepción de CFDIs"
description = ""
weight = 3
+++

{{% children  %}}


**Por cualquier medio que se haga la recepción de cfdis en bóveda,  ya sea bajar del SAT, subir archivo ZIP, jalar de correos electrónicos.**

*	Se validará que los archivos sean tipo cfdi y que corresponda el rfc de la cuenta al emisor o receptor
*	Se guardará y se marcara con status “Pendiente”, el archivo pasa a una cola para posterior validación y resguardo.
*	Se validará integridad por medio del sello y validez ante el SAT.


**Estatus CFDI**

1. **Pendiente**: Aún está pendiente validar CFDI (sello & sat) y resguardar.
1. **Vigente**: El comprobante se encuentra en el SAT y no está alterado.
1. **Cancelado**: El comprobante se encuentra cancelado en el SAT y no está alterado  
1. **Invalido**: El comprobante aun no llega al SAT tiene 72hrs a partir de la fecha de certificación, si no manda alerta a contacto de bóveda.
1. **Alterado**: El comprobante está corrupto en su estructura, se manda alerta a contacto de bóveda.

*	CFDIs con status Vigente y Cancelado. Se muestran en Catálogo Emitidos o Recibidos.
*	CFDIs con status Pendiente, Invalido o Alterado. Se muestran en Catálogo de Inhabilitados 