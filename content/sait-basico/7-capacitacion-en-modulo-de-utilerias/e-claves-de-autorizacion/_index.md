+++
title = "Calcular Claves de Autorización"
description = ""
weight = 4
+++


### **Indicaciones**:

Este proceso le permitirá tener mayor control en la cancelación o devolución de documentos en el sistema ya que el usuario tendrá que ingresar una clave de autorización que le será proporcionada por el personal autorizado. Sin la cual no se podrá realizar el proceso.

Para activar este proceso en SAIT deberá dirigirse a:

1.Utilerías / Grupos de Usuarios-Catalogo de niveles en donde aparecerá la siguiente ventana: 

2.Ingrese el nivel que debe pedir autorización para cancelar documentos

3.Habilite la opción Requiere autorización para cancelar documentos o hacer devoluciones

4.Haga clic en el botón **[Grabar]**

![IMG](autorizacion.png)

5.A partir del momento que se activa dicha opción el sistema al cancelar un documento le pedirá ingresar la clave de autorización:

![IMG](cancelacion.JPG)

6.Para poder calcular la clave de autorización un usuario con acceso a la opción de Calcular Claves de Autorización deberá ingresar el el folio del documento que se desea cancelar:

7.Inice sesión con su clave de Administrador y vaya a utilerías / Calcular Claves de Autorización, aparecerá una ventana como la siguiente

8.Escriba el folio del documento

**NOTA:** Es importante anteponer el tipo de documento a cancelar despúés del folio, por ejemplo: si deseo cancelar la factura con folio ARR25545 se deberá anteponer una F antes del folio: **FARR25545**

![IMG](ingresar-documento.JPG)


9.Haga clic en el botón **[Continuar]**

10.Aparecerá una ventana con la clave de autorización, en una ventana como la siguiente:

![IMG](clave-de-autorizacion.JPG)

11.Tomo nota de la clave y ahora sí al momento de realizar la cancelación o devolución ingresamos la clave  y de clic en el botón **[Continuar]** 

![IMG](cancelacion-con-clave.JPG)

12.Finalmente ingresamos los datos de la confirmación de cancelación y listo

![IMG](confirmacion.JPG)