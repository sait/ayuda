+++
title="Nuevas expresiones"
description=""
weight = 3
+++

A continuación se anexa las posibles expresiones que usted puede utilizar ahora en sus formatos CFDI 4.0, tanto en Facturas y en Notas de Crédito. Cabe destacar que para que dichas expresiones funcionen, tiene que existir el nodo o atributo en el XML.

### Datos generales

| Nombre                     | Expresión                                                                      |
|----------------------------|--------------------------------------------------------------------------------|
| Tipo de Comprobante        | IIF(oCfdi.Comprobante.TipoDeComprobante=='I','Factura','Nota de Crédito')      |
| Serie                      |  eval2('oCfdi.Comprobante.Serie')                                              |
| Folio                      | oCfdi.Comprobante.Folio                                                        |
| Folio Fiscal (UUID)        | oCfdi.Comprobante.Complemento.TimbreFiscalDigital.UUID                         |
| Fecha y Hora Emisión       | oCfdi.Comprobante.Fecha                                                        |
| Certificado del Emisor     | oCfdi.Comprobante.NoCertificado                                                |
| Tipo de Comprobante        | cveDescCatCfdi('TipoDeComprobante',oCfdi.Comprobante.TipoDeComprobante)        |
| Moneda                     | oCfdi.Comprobante.Moneda                                                       |
| Tipo de Cambio             | oCfdi.Comprobante.TipoCambio                                                   |
| Fecha y Hora Certificación | oCfdi.Comprobante.Complemento.TimbreFiscalDigital.FechaTimbrado                |
| Certificado SAT            | oCfdi.Comprobante.Complemento.TimbreFiscalDigital.NoCertificadoSAT             |
| Uso de CFDI                | cveDescCatCfdi('UsoCFDI',oCfdi.Comprobante.Receptor.UsoCFDI)                   |
| Forma de Pago              | cveDescCatCfdi('FormaPago',oCfdi.Comprobante.FormaPago)                        |
| Condiciones de Pago        | oCfdi.Comprobante.CondicionesDePago                                            |
| Método de Pago             | cveDescCatCfdi('MetodoPago',oCfdi.Comprobante.MetodoPago)                      |
| Lugar Exped. (C.P.)        | oCfdi.Comprobante.LugarExpedicion                                              |
| Sello CFD                  | oCfdi.Comprobante.Complemento.TimbreFiscalDigital.SelloCFD                     |
| Sello SAT                  | oCfdi.Comprobante.Complemento.TimbreFiscalDigital.SelloSAT                     |
| Cadena Original            | oCfdi.cadenaOriginalTFD()                                                      |
| Logotipo                   | logotipo()                                                                     |
| Codigo QR (CBB)            | oCfdi.getQRFileName()                                                          |
| Tipo de CFDI relacionados  | cveDescCatCfdi('TipoRelacion',oCfdi.Comprobante.CfdiRelacionados.TipoRelacion) |
| CFDIs relacionados         | oCfdi.getCFDIsRelacionados()                                                   |

### Emisor
| Nombre              | Expresión                              |
|---------------------|----------------------------------------|
| RFC                 | oCfdi.Comprobante.Emisor.Rfc           |
| Nombre              | oCfdi.Comprobante.Emisor.Nombre        |
| Regimen Fiscal      | oCfdi.Comprobante.Emisor.RegimenFiscal |

### Receptor
| Nombre              | Expresión                              |
|---------------------|----------------------------------------|
| RFC                 | oCfdi.Comprobante.Receptor.Rfc         |
| Nombre              | oCfdi.Comprobante.Receptor.Nombre      |

### Conceptos (formato principal)

{{%alert warning%}}Estas expresiones solo se pueden utilizar en el formato CFDI 3.3 principal, no en los formatos adicionales{{%/alert%}}

| Nombre                   | Expresión                                                                                                           |
|--------------------------|---------------------------------------------------------------------------------------------------------------------|
| Clave Artículo           | oCfdi.oConcepto.NoIdentificacion                                                                                    |             
| Clave SAT (ClaveProdServ)| oCfdi.oConcepto.ClaveProdServ                                                                                       |             
| Cantidad                 | transf(val(oCfdi.oConcepto.Cantidad), oCfdi.cMascaraCant)                                                           |             
| Unidad de Medida         | oCfdi.oConcepto.Unidad                                                                                              |             
| Clave SAT de Unidad      | oCfdi.oConcepto.ClaveUnidad                                                                                         |             
| Descripción              | oCfdi.oConcepto.Descripcion                                                                                         |             
| Precio                   | transf(val(oCfdi.oConcepto.ValorUnitario),oCfdi.cMascaraPrec)                                                       |             
| Importe                  | val(oCfdi.oConcepto.Importe)                                                                                        |       
| % Descuento              | round(val(oCfdi.oConcepto.get('Descuento'))/val(oCfdi.oConcepto.Importe)*100,2)                                     |
| Descuento neto           | -1*val(oCfdi.oConcepto.Descuento)                                                                                   |
| Num. Pedimento           | oCfdi.getPedimentos()                                                                                               | 
| Cuenta Predial           | iif(type('oCfdi.oConcepto.CuentaPredial.Numero')=='C','Cuenta Predial: '+(oCfdi.oConcepto.CuentaPredial.Numero),'') | 

#### Condiciones (Print When...) necesarias en formato principal

{{%alert info%}}Estas condiciones se ocupan capturar en 'Print only when expression is true'{{%/alert%}}

| Nombre                    | Print When...                                     |
|---------------------------|---------------------------------------------------|
| % Descuento               | val(oCfdi.oConcepto.get('Descuento')) > 0         |
| Descuento neto            | val(oCfdi.oConcepto.get('Descuento')) > 0         |
| Num. Pedimento            | Not Empty(oCfdi.getPedimentos())                  |
| Cuenta Predial            | type('oCfdi.oConcepto.CuentaPredial.Numero')=='C' |


### Conceptos (formatos adicionales)

{{%alert warning%}}Estas expresiones solo se pueden utilizar en formatos CFDI 3.3 adicionales, no en el formato principal{{%/alert%}}

| Nombre                   | Expresión                                                                                                           |
|--------------------------|---------------------------------------------------------------------------------------------------------------------|
| Clave Artículo           | Allt(Movim.NUMART)                                                                                                  |
| Clave SAT (ClaveProdServ)| Allt(Arts.CLAVESAT)                                                                                                 |
| Cantidad                 | Movim.CANT                                                                                                          |
| Unidad de Medida         | Allt(Movim.UNIDAD)                                                                                                  |
| Clave SAT de Unidad      | Allt(UnidadesCat.CLAVESAT)                                                                                          |             
| Descripción              | Allt(Arts.DESC)                                                                                                     |
| Precio                   | Movim.PRECIO*(1-Movim.PJEDESC/100)                                                                                  |
| Importe                  | Movim.CANT * Movim.PRECIO * (1-Movim.PJEDESC/100)                                                                   |
| % Descuento              | Round(Movim.PJEDESC,2)                                                                                              |
| Descuento neto           | round(Movim.CANT * Movim.PRECIO * (Movim.PJEDESC/100),2)                                                            |
| Num. Pedimento           | Allt(Minv.PEDIMENTO)                                                                                                |
| Cuenta Predial           | iif(not Empty(Arts.CTAPREDIAL),'Cuenta Predial: '+(Arts.CTAPREDIAL),'')                                             |

#### Condiciones (Print When...) necesarias en formatos adicionales

{{%alert info%}}Estas condiciones se ocupan capturar en 'Print only when expression is true'{{%/alert%}}

| Nombre                    | Print When...                                     |
|---------------------------|---------------------------------------------------|
| % Descuento               | Movim.PJEDESC > 0                                 |
| Descuento neto            | Movim.PJEDESC > 0                                 |
| Num. Pedimento            | Not Empty(Minv.PEDIMENTO)                         |
| Cuenta Predial            | type('Arts.CTAPREDIAL')<>'U'                      |

### Totales
| Título          | Importe         |
|-----------------|-----------------|
| oCfdi.aImp[1,1] | oCfdi.aImp[1,2] |
| oCfdi.aImp[2,1] | oCfdi.aImp[2,2] |
| oCfdi.aImp[3,1] | oCfdi.aImp[3,2] |
| oCfdi.aImp[4,1] | oCfdi.aImp[4,2] |
| oCfdi.aImp[5,1] | oCfdi.aImp[5,2] |
| oCfdi.aImp[6,1] | oCfdi.aImp[6,2] |
| oCfdi.aImp[7,1] | oCfdi.aImp[7,2] |