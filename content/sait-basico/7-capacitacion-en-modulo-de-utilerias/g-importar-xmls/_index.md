+++
title = "Importar Información desde / CFDIs Emitidos"
description = ""
weight = 6
+++

Este proceso nos puede servir para distintos casos, migración de otro sistema a SAIT, si se quiere recuperar un XML que no está en SAIT, también si se empieza desde 0 una empresa si se requiere después timbrar un recibo de pago se necesita la factura esté en el sistema para poder aplicar el pago y posteriormente timbrarlo.

Cual sea su caso, a continuación mostaremos las instrucciones detalladas para llevar a cabo este proceso.


<h2 class="text-primary">Validaciones</h2>

- El RFC emisor del xml debe ser el mismo que tiene en Utilerías / Configuración General del Sistema

- Tanto como la SERIE y el FOLIO en conjunto no pueden medir más de 10 caracteres, ya que este es el límite del campo NUMDOC en SAIT

- Es importante confirmar que el XML a importar contiene el nodo 
cfdi:Concepto NoIdentificacion="CLAVE" ya que de esta manera el sistema identifica el artículo de la factura

- Se recomienda haceru una carpeta en el disco C:\ sin espacios. Ejemplo: C:\XML

- Es MUY IMPORTANTE que después de importar los XML, se borre el contenido de la carpeta C:\XML ya que de no hacerlo y volver a importarlos la información se duplicará en la base de datos.

### Instrucciones:

1.Descarga el o los xml que desea importar al sistema SAIT

2.Cree una carpeta en el disco C:\XML y coloque ahí los XML a importar

3.Diríjase a Utilerías / Importar Información desde / CFDIs Emitidos

4.Se nos presentará la ventana de Importación desde CDFIs Emitidos

![IMG](2.png)

Las opciones a utilizar son las siguientes:

**Directorio donde se encuentran los XLM:** ruta en la que están los XML

**Importar CFDIs creados del:** rango de fecha que considerará el SAIT a importar

### CATÁLOGOS

✓ **Crear Clientes:** esta opción se utiliza cuando ese Cliente/RFC no existe en el catálogo de clientes.

✓ **Validar nombre del Receptor:** esta opción se utiliza únicamente cuando tenemos a los clientes dados de alta en SAIT con el RFC XAXX-010101-000      

Ejemplo práctivo: Tengo el cliente con la clave 1 y el nombre SALINAS DEL REY, S.A. DE C.V., si no selecciono la opción de "Validar nombre del Receptor" el sistema importará la factura al cliente 0 ya que es el que por defaulr tiene el RFC genérico XAXX-010101-000.

![IMG](3.png)

Para efectos de la prueba importaremos la factura AA480 que como podemos ver hace falta su consecutivo en la consulta general de facturas

![IMG](1.png)

Agreamos la ruta en donde colocamos el XML y seleccionamos la opción de "Validar nombre del Receptor", crear Facturas  

Clic en [Cargar CFDIs]

![IMG](4.png)

La ventana nos dará una vista previa, en caso de que haya algún problema con el XML en la columna de Importa aparecera la palabra "No" y seguido de la causa del porqué no se puede importar, en este caso todo está corrcto por lo que solo daremos clic en [Importar], y clic en [Sí] para confirmar. 

Nos aparecerá la ventana donde nos confirma la información de importó de manera correcta.

![IMG](6.png)

Si hacemos una consulta general observaremos que la factura AA480 ya aparece y está agregada al cliente número 1.

![IMG](7.png)

✓ **Crear artículos:** esta opción se utiliza cuando la clave de artículo del XML no existe en el catálogo de artículos y servicios.

### FACTURAS

✓ **Crear Facturas:** esta opción siempre se debe de seleccionar ya que es lo que hará que se importe la factura.

✓ **Crear Cargo en Cobranza:** esta opción se selecciona en conjunto con Crear Facturas y sirve para poder agregar el cargo al estado de cuenta del cliente en caso de las facturas a crédito o PPD.

### NOTAS DE CRÉDITO

✓ **Notas de Crédito:** esta opción se utiliza cuando el CFDI a importar es una nota de crédito, una factura de tipo EGRESO.





