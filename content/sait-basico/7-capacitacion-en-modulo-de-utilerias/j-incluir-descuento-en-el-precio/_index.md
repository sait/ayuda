+++
title = "Incluir descuento en el precio"
description = ""
weight = 9
+++

Si usted otorga descuento a sus clientes, SAIT cuenta con la siguiente configuración la cual le permite no desglosar el descuento e incluirlo en el precio.

Para activar esta opción, deberá ir a Utilerías / Configuración General del Sistema / Ventas2 / * Incluir descuento en el precio

**Para que el sistema tome los cambios deberá cerrar por completo su sistema SAIT y entrar nuevamente**

![IMG](1.png)

Posteriormente deberá activar un descuento ya sea por cliente, por clasificación (línea, familia, categoría, proveedor) o por artículo, para esto deberá ir al menú de Ventas / Descuentos y Promociones / Promociones / Agregar

![IMG](2.png)

Visualización de factura

![IMG](3.png)

Visualización de XML

![IMG](4.png)


