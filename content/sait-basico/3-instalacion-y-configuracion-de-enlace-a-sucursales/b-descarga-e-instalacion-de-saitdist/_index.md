﻿+++
title = "Descarga e Instalación de SaitDist"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=KNF4NUInAMY&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>


<div class="card">
<a href="3.A2.Descarga-instalacion-SAITDist.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En esta sección de la instalación descargaremos los ejecutables y librerías del enlace de sucursales.

Primeramente descargaremos los archivos de SAIT DISTRIBUIDO [clic aquí para descargar](SAITDIST.zip)

Recomendamos guardar el archivo descargado en disco C:\SAITDIST de la siguiente manera: 

Abrir en carpeta el archivo descargado

![IMG](1.jpg)

Doble clic al archivo descargado y dar clic en la opción de WinRar **[Extraer en]**

![IMG](2.jpg)

Seleccionar el disco C:\ , clic en nuevo folder y creamos una carpeta llamada "SaitDist"

![IMG](3.jpg)

Clic en **[Aceptar]**

![IMG](4.jpg)

Una vez descomprimidos los archivos, podemos ubicarlos en la carpeta que acabamos de crear. Nos dirigimos a dicha carpeta.

![IMG](5.jpg)

_En algunas ocasiones encontrarán que la carpeta no se llamará SaitDist, podrá llamarse "Conc", haciendo referencia a una Concentradora, "Distmat", haciendo referencia a Sait Distribuido de alguna matriz o bien el nombre de alguna sucursal._

**ES IMPORTANTE COMENTAR que el nombre no tiene ninguna repercusión en el funcionamiento del mismo.**