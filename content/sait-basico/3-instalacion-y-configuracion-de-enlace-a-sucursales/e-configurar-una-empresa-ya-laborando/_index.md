﻿+++
title = "Configurar una empresa ya laborando"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=LGLTYvHNWeE&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>


<div class="card">
<a href="3. D. Configurar una empresa ya laborando.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

<h4 class="text-danger">Muy importante aclarar que este proceso solo se puede realizar cuando el cliente tiene una sola sucursal y ésta tiene tiempo funcionando, si el cliente tiene dos o más sucursales no se recomienda realizar de esta manera el proceso ya que los catálogos princicipales (artículos, proveedores, artículo) muy probablemente tengan claves distintas y el enlace no funcionará de manera correcta. Si tiene más de una sucursal laborando, se recomienda empezar con una empresa desde cero.</h4>

1.Crear Almacenes

Eso quiere decir aun no tenemos varios almacenes para guardar nuestros documentos, por lo que hay que convertir nuestro unialmacén en un multialmacén.

Ingresar al módulo de Utilerías / Catálogo de Sucursales/Almacenes.

![IMG](1.jpg)

En caso de no tener ninguno, deberemos crearlos.

Para crear un almacén nuevo vamos a dar clic en **[Agregar]**

![IMG](2.jpg)

* Capturar la clave de la sucursal. Se recomienda que se asigne un valor numérico que no exceda de dos caracteres.
* Capturar el nombre de la sucursal, y demás datos de domicilio fiscal **(el código postal es un campo obligatorio).**
* Indicar la clave de los grupos los cuales tendrán acceso a la Sucursal. En caso de que todos los niveles requieran el acceso en determinada, escribir la palabra **TODOS**.
* Capturar el folio de la siguiente salida a sucursal, en donde se especifica el folio de la letra que le corresponde al almacén.
* Para grabar los datos, hacer clic en el botón **[Agregar]**. Como se muestra en la siguiente imagen:

![IMG](3.jpg)

2.Marcar Usar Varios Almacenes

Utilerías/Configuración General del Sistema/Inventario

![IMG](4.jpg)

3.Marcar con qué número de almacén iniciará el sistema.

Utilerías/Configuración General del Sistema/Inventario

![IMG](5.jpg)

4.Colocar número de almacén que se usará para trabajar.

Regularmente se ingresa el mismo que en Inventario.

Ingresar al módulo de Utilerías/Configuración General del Sistema/SAIT Distribuido.

![IMG](6.jpg)

5.Colocar series de documentos

Aquí hay que considerar que ya hay documentos, por lo que lo más recomendable es solamente agregar una serie y dejar el consecutivo para identificar la sucursal en cuestión.

Ventas / Siguientes Folios

![IMG](7.jpg)

6.Dar series a tipos de movimiento en inventario

Ingresar al módulo de Inventario / Tipos de movimientos

En esta sección de la configuración es necesario agregar una serie a los folios esto con la finalidad de:

* Identificar los movimientos de cada sucursal en una concentradora
* Que al momento de enviar los movimientos de una sucursal a otra no se sobrescriba.

Seleccionar cada tipo de movimiento de inventario e ingresar la serie en folio siguiente

![IMG](8.jpg)

7.Utilizar utilería convalm.exe

Esta aplicación sirve para preparar la base de datos de la empresa para comenzar a utilizar almacenes o sucursales, cuando anteriormente no se habían usado, esto con la finalidad de no perder la información anterior.

Este es un proceso delicado y debe tener mucho cuidado al realizarlo por favor lea bien las instrucciones descritas a continuación, si tiene alguna duda consúltelo de inmediato con su asesor.

PROCESO PARA UTILIZAR UTILERÍA CONVALM.EXE

1.Si su sistema SAIT está en red, todos deberán salirse del sistema.

2.Descargue la utilería **convalm.exe**: para descargar de clic  [aquí](convalm.exe) 

{{%alert danger%}}
#### IMPORTANTE: Si usted ya manejaba almacenes internos anteriormente, quiere decir que sus movimientos YA cuentan con un número de almacén, es decir esta utilería lo que hace es definir un almacén a las entradas y salidas que se han realizado anteiormente, por lo que si ya manejaba almacenes, este paso no sería necesario.
{{%/alert%}}

3.Guarde el archivo convalm.exe dentro del directorio de la empresa.

![IMG](9.jpg)

4.Vaya al directorio de la empresa y ejecute el archivo convalm.exe
![IMG](10.jpg)


5.Se mostrará el siguiente mensaje, en donde deberá especificar el número de sucursal al que va a pertenecer esta empresa. En este caso:  **1**
![IMG](11.jpg)

6.Posteriormente, solicitará confirmación del proceso a realizar presionando la tecla  **S**  para aceptar o  **N**  para cancelar.
![IMG](12.jpg)

7.Y de nueva cuenta solicitará confirmación de la misma forma.

8.En este momento se comienza a correr el proceso, al terminar aparece un mensaje de LISTO.

9.Clic en **[Aceptar]**
![IMG](13.jpg)

8.Para finalizar el proceso es necesario cerrar y abrir SAIT

Esto con la finalidad de recargar valores, de igual manera abrimos nuestro Enlace de Sucursales para verificar todo esté en orden.