﻿+++
title = "Configurar SDConfig"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=JQ-Wbf0Hdpo&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="3. B. Configuración de SDConfig.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>




### **Indicaciones**:

**Configurar SDConfig**
Como sabemos, uno de los archivos que contiene la carpeta de enlace o SaitDist, es SDCONFIG.exe; el cual nos sirve para colocar la configuración deseada de sincronización.

Primeramente deberá crear un ticket en http://soporte.sait.mx/ solicitando la creación de la cuenta, favor de enviar la solicitud con los siguiente datos:

- Nombre empresa

- Dirección empresa

- RFC

- Número de contrato

- Nombre de la cuenta de SAIT Distribuido

Nos dirigimos a la carpeta contenedora del enlace a configurar.

En nuestro caso se encuentra en: disco local C:\SaitDist. Localizamos la carpeta y damos doble clic al archivo sdconfig

![IMG](1.jpg)

Si nunca hemos abierto este ejecutable, nos aparecerán 2 opciones para configurar los eventos: **Eventos para Matriz y Eventos para Sucursal**.

<h3 class="text-primary">Eventos para Matriz</h3>

Al Seleccionar &quot;Eventos para Matriz&quot; se hace referencia a que la mayor parte de los eventos estarán seleccionados para enviar y recibir. 

En muchas ocasiones, o bien, en la mayor parte, esta configuración se utiliza para las concentradoras, ya que es la ideal al recibir la mayor parte de los movimientos.

<h3 class="text-primary">Eventos para Sucursal</h3>

Por otra parte, al seleccionar &quot;Eventos para Sucursal&quot;, los eventos que se seleccionan son solo para enviar en mayor medida, pero casi no se recibe nada. 

Esta es la configuración ideal para una sucursal, al intentar no saturar de información las bases de datos de la misma (Al saturar no nos referimos a que se llenen las tablas solamente, sino también al no tener información como los cortes, notas de ventas o facturas que no pertenecen a la sucursal). En caso derequerir esta información se podrá consultar directamente en concentradora.

____________________________________________________________________

Para configurar &quot;Eventos para Sucursal&quot;, siga las siguientes indicaciones:

Al abrir el archivo **sdconfig** , le aparecerán las siguientes opciones, dar clic en "Eventos para sucursal"
![IMG](2.jpg)

Le aparecerá una alerta, en la cual deberá dar clic en **[Sí]** al estar de acuerdo en configurar Eventos para Sucursal

![IMG](3.jpg)

Una alerta le aparecerá confirmando que los eventos ya fueron creados.

Clic en **[Aceptar]**

![IMG](4.jpg)

Continuando con los puntos de configuración, el siguiente paso es: Colocar el usuario o cuenta creada con su password correspondiente. Es importante comentar que en el correo se
encuentra el usuario y Password.

![IMG](5.jpg)

Lo siguiente es colocar el directorio donde contienen las bases de datos de Sait; para esto pueden ubicar la carpeta desde el signo de interrogación o bien pegar la ruta.

En este caso vamos a seleccionar la carpeta deseada, dando doble clic sobre C:, vamos a ir a la carpeta de Sait y seleccionamos la empresa correspondiente a los cambios, o bien, 
a la empresa que estamos trabajando.

Clic en **[Select]**

![IMG](6.jpg)

Es necesario colocar la palomita en el check box si la **empresa es concentradora** , con el fin que los saldos en cobranza estén actualizados; esto quiere decir, que una vez que reciba un
cambio en un saldo, este se haga réplica en las demás sucursales.

Podemos seleccionar de una forma rápida si deseamos o no recibir y enviar información de forma generalizada, seleccionado los campos de **[Enviar]** o **[Recibir]** y podemos detallar qué información
deseamos en el botón **[Configurar Eventos]**.

![IMG](7.jpg)

Al dar clic en el botón **[Configurar Eventos]**, nos muestra el catalogo de eventos que podemos modificar según las necesidades de la empresa. No debe preocuparse, pues la configuración
recomendada está lista en el momento en que selecciona eventos para Matriz o Sucursal.

En la siguiente imagen puede ver el ejemplo de los eventos que se seleccionaron para sucursal,también puede notarse que en su mayor parte son para enviar y no para recibir, todo depende
de la configuración que tenga la empresa.

Realizar los cambios necesarios y dar clic en **[Aceptar]**.

![IMG](8.png)

<h3 class="text-danger">NOTA</h3>

**Se recomienda verificar si están agregados los nuevos eventos relacionados al esquema de facturación 3.3 los cuales son indicados en la siguiente imagen, en caso de no estar se deberán agregar tal y como se muestra en la imagen (puede agregarlos al final de los demás eventos)**.

![IMG](33.PNG)

Por último la sección de último evento recibido, menciona el folio del último evento descargado del portal. **No se recomienda modificar esta sección**.

En caso de ser necesario, consulte con su asesor de sistemas o asesor SAIT.

Para finalizar la configuración, solo damos clic en **[Aceptar]**.

![IMG](9.jpg)

### OTROS TIPOS DE EVENTOS

Hay algunos eventos que no vienen agregador por default a la configuración, sin embargo puede agregarlos.

Dependiendo se desea que se envíe y reciba en cada sucursal estos eventos deberá seleccionar con una X la columna.

Algunos eventos adicionakes que puede agregar son:

| Evento     | Descripción                                                              | Enviar  | Recibir |
|------------|--------------------------------------------------------|---------|---------|
| ModPromo       | Agregar y Modificar Promociones                  |    X    |    X    |
| DelPromo         | Borrar Promocion                                            |    X    |    X    |
| ModKit             | Agregar y modificar Kits                                  |    X    |    X    |
| DelKit               | Eliminar Kit                                                       |    X    |    X    |
| Defsust            | Artículos sustitutos                                           |    X    |    X    |
| PagosDig         | Comprobantes de REPS                                   |    X    |    X    |
| CxcPagoDig     | Facturas relacionadas en cada REP                  |    X    |    X    |
| Speis                | Speis recibidos                                                 |    X    |    X    |
| AcusesCanc      | Acuses de cancelación de comprobantes       |    X    |    X    |
| CFDIs                | Información de los CFDIs emitidos                 |    X    |    X    |
| UnidadCat        | Catálogo de unidades                                     |    X    |    X    |
| Gastos              | Gastos                                                              |    X    |    X    |
| ModZonas        | Agregar y Modificar Zonas                             |    X    |    X    |
| DelZonas          | Eliminar Zonas                                                 |    X    |    X    |
| ModClasc1       | Agregar y Modificar Clasificaciones                |    X    |    X    |
| DelClasc1         | Eliminar Clasificación                                       |    X    |    X    |
| ModSucCli        | Agregar y Eliminar Sucursales de los Clientes |    X    |    X    |
| DelSucCli          | Eliminar Sucursales de los Clientes                  |    X    |    X    |
| ModDescCli      | Agrega o Modifica Descuento X Cliente         |    X    |    X    |
| DelDescCli        | Elimina Descuento X Cliente                            |    X    |    X    |
| ModPedim        | Agregar y Modificar Pedimentos                     |    X    |    X    |
| DelPedim          | Eliminar Pedimentos                                        |    X    |    X    |