﻿+++
title = "Actualizar Versión de Enlace de Sucursales (SAIT Distribuido)"
description = ""
weight = 7
+++

Uno de los procesos más importantes es mantener actualizada la versión de enlace de sucursales, ya que de esto depende que los eventos se envíen y reciban de manera correcta.

Es muy importante que todas las sucursales, cuenten con la misma versión del enlace.
Proceso para Actualizar SAIT Distribuido

El proceso es muy sencillo y facil, el cual se explica a continuación:

1.Primeramente descargaremos la nueva versión [clic aquí para descargar](SAITDIST.zip)

2.En automatico se descargará el archivo comprimido con los archivos de SAIT DISTRIBUIDO

3.Diríjase a Descargas para localizar el archivo llamado: **saitdist.zip**

![IMG](descargas.PNG)

4.Una vez descargado el archivo, deberá de descomprimir el contenido, para descomprimir deberá dar clic derecho sobre el archivo comprimido y seleccionar la opción  **Extract to saitdst** 

![IMG](extraer.PNG)

5.Notará que una vez descomprimido se crea una carpeta la cual contiene tres archivos **(saitdist.exe, sdconfiog.exe y bitacora.exe)**, como se muestra a continuación: 

![IMG](descomprimido.PNG)

6.Seleccionar los tres archivos indicados y hacer clic en el boton derecho del Mouse, seleccionar: Copiar.

![IMG](archivos.PNG)

7.Minimiza la ventana y dirigirse al directorio de SAIT distribuido de su máquina. Ese archivo regularmente se encuentra en **C:\SAITDIST**

<h4 class="text-danger">En caso de tener dudas de la ubicación del archivo o carpeta, contacte a su asesor SAIT o distribuidor para obtener mas información de la ubicación.</h4>

8.Hacer clic derecho en el boton del Mouse y seleccionar la opción de: **Pegar**.

Windows le avisará que ya existen los archivos, si desea reemplazar los archivos, como se muestra en la siguiente imagen:

![IMG](reemplazar.PNG)

9.Hacer clic en el botón **[Copiar y reemplazar]** y palomear la opción de: **[Realizar esta opción para los siguientes 2 conflictos]** y con esto se reemplazarán los archivos de enlace.

10.Listo, trate de accesar a SAIT Distribuido, con esto ha quedado actualizada la versión de enlace de sucursales. 

