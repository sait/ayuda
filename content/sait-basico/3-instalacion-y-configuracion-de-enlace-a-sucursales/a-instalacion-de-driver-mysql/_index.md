﻿+++
title = "Instalación de Driver MySQL"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=KNF4NUInAMY" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="3. A1 Instalación de Driver MySQL.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El controlador de MySQL es crítico, ya que sin él, el enlace a sucursales o SaitDist.exe no se podrá conectar al servidor de eventos.

Descargamos el instalador de la siguiente liga:

https://sait.mx/download/mysql-odbc-connector-351-setup.exe

Al dar clic y terminar la descarga, deberemos de ejecutar el programa.
![IMG](1.jpg)

La instalación es muy sencilla, ya que solo es cuestión de dar clic en los botones de **[Siguiente]** como se muestra a continuación:

![IMG](2.jpg)

![IMG](3.jpg)

Por último clic en el botón **[Instalar]**.

![IMG](4.jpg)

Esperar a que se instale el controlador y por último clic en el botón **[Finalizar]**.

![IMG](5.jpg)