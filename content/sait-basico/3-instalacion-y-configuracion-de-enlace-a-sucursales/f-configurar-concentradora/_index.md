﻿+++
title = "Configurar Concentradora"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=sI30DmV4phg&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="3. E. Configurar Concentradora.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


Para realizar la configuración en una concentradora, favor de seguir las siguientes indicaciones:

1.Crear el almacén de Concentradora.

* Dirigirse a Catálogo de Sucursales/Almacenes

* Clic en **[Agregar]**

![IMG](1.PNG)

Le recomendamos utilizar un número muy alto, por ejemplo 99, ya que es muy probable que no se necesite para una sucursal.

* Ingresar la descripción y los usuarios que tendrán acceso, **si va a facturar desde dicha empresa, es muy importante indicar Código Postal**.

* Clic en **[Aceptar]**

![IMG](2.PNG)

2.Crear copia de la carpeta de Bases de Datos.

* Cerrar el sistema

* Dirigirnos al directorio donde se encuentra la empresa y realizar una copia

![IMG](3.jpg)

* Cambiar el nombre de la carpeta haciendo referencia a que es una Concentradora

![IMG](4.jpg)

* Ahora realizaremos el mismo proceso con la carpeta de Enlace a sucursales (SAITDIST)

* Dirigirnos al directorio donde se encuentra la carpeta SAITDIST y realizar una copia.

![IMG](5.jpg)

* Renombrar la carpeta, haciendo referencia a que es de Concentradora.

![IMG](6.jpg)

* Una vez que se hizo el cambio de nombre, podemos entrar a la carpeta y dar clic a &quot;sdconfig&quot;.

![IMG](7.jpg)

3.Tomar nota de los datos.

* Vamos a revisar la configuración con la que contamos actualmente, en este caso, tenemos un directorio que ya no le corresponde a la empresa que vamos a manejar, porque será la concentradora la que comenzaremos a trabajar.

* Tenemos que revisar el último evento recibido y tomamos nota de este dato.

* Cerrar la ventana sin realizar ninguna modificación

![IMG](8.jpg)

4.Borrar de la carpeta CATEVENTOS.DBF y SDCONFIG.DBF

* Eliminaremos las bases de datos CATEVENTOS.DBF y SDCONFIG.DBF

![IMG](9.jpg)

* Una vez eliminado, abriremos sdconfig.

![IMG](10.jpg)

5.Crear eventos para matriz.

* Clic en Eventos para Matriz y configuramos los eventos.

![IMG](11.jpg)

* Colocar usuario, password y ubicar la carpeta correspondiente a la concentradora

* Es de suma importancia ingresar correctamente el último evento recibido.

* Clic en **[Aceptar]**.

![IMG](12.jpg)

* Antes de continuar es necesario volver a entrar a la empresa, en este caso solo contamos con un directorio.

* Agregamos una empresa existente, en **[Catálogo de Empresas]**, posteriormente clic en **[Agregar Empresa Existente]**.

* Ubicar la empresa (Concentradora) y clic en **[Continuar]**.

* Clic en **[Cerrar]**

![IMG](13.jpg)

6.Colocar número de Concentradora.

* Acceder a la empresa que acabamos de dar de alta.

* Dirigirnos al módulo de Utilerías/Configuración General del Sistema.

* Clic en la pestaña  Inventario.

* En la sección &quot;Al iniciar seleccionar el almacen&quot;, ingresar el número 99.

![IMG](14.PNG)

* Dirigirnos a la pestaña de Sait Distribuido.

* En la sección &quot;# de Almacen asociado a la sucursal&quot;, ingresar el número 99.

* Clic en **[Cerrar]**

![IMG](15.PNG)

* SALIR DE LA EMPRESA

* Abrir carpeta de Concentradora

* Clic en SAITDist.

![IMG](16.jpg)

* Nos aparecerá el #99 correspondiente a nuestra concentradora.

![IMG](17.jpg)

* Si quisiéramos realizar alguna modificación a los eventos a recibir, solo deberemos ingresar a: sdconfig.

* Clic en **[Configurar Eventos]**

![IMG](18.jpg)

* Abrir nuevamente Sait.

* Clic en **[Catálogo de Empresas]**

* A la empresa colocar el nombre de &quot;Concentradora&quot; para no confundirla.

![IMG](19.jpg)

* Clic en **[Cerrar]**.

* Entrar a la empresa &quot;Concentradora&quot; y LISTO.



<h3 class="text-danger">IMPORTANTE</h3>

Antes de empezar a utilizar la empresa concentradora deberá modificar los siguientes folios, es decir definir la serie de los folios de esta nueva concentradora, en donde utilizaremos una letra diferente para identificar los movimientos de esta concentradora de las demás:

#### 1.MODIFICAR FOLIOS DE VENTAS

Diríjase a :

**Ventas / Siguientes Folios**  

Modificar todos los siguientes folios a excepción de #Cliente, #ClienteEventual y #Proveedor.

![IMG](ventas.PNG)

#### 2.MODIFICAR FOLIOS DE MOVIMIENTOS DE INVENTARIO

Diríjase a :

**Inventarios / Tipos de Movimientos / Modificar / Sig. Folio**

Hacer esto con todos los conceptos de Movimientos de Inventario

![IMG](inventario.PNG)

