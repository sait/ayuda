﻿+++
title = "Catálogo de Vendedores"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=H9KR1ClvK0o&t=0s&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&index=4" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. D Catalogo de vendedores.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite contar con un catálogo de vendedores, el cual les permite tener una relación de sus agentes de venta.

De esta forma tener un control para la identificación rápida de quién es el responsable de un documento de venta, permite también controlar o restringir a los vendedores a un precio mínimo o descuento mínimo permitido además, permite otorgarles un porcentaje de comisión por las ventas que cada vendedor realice.

**Para acceder a este catálogo deberá ingresar al menú de Ventas / Vendedores.**


<h2 class="text-primary">A.	Agregar un Vendedor</h2>


Ir al menú de Ventas / Vendedores.

![IMG](1.png)


1.Seleccionar la opción de **[Agregar]**.

2.El sistema por default asigna la clave consecutiva del vendedor (se puede modificar).

3.Se recomienda que la clave del vendedor corresponda a la misma del usuario.

4.Ingresar la información necesaria en el formulario y dar clic en **[Agregar]**.

![IMG](2.png)

<h2 class="text-primary">B.	Modificar un Vendedor</h2>


En el menú de Ventas / Vendedores, seleccionar el vendedor que desea modificar.

1.Una vez seleccionado dar clic en la opción **[Modificar]**.

2.Realizar las modificaciones necesarias en el formulario y dar clic en **[Modificar]**.


![IMG](3.png)
 
<h2 class="text-primary">C.	Eliminar un Vendedor</h2>

En el menú de Ventas / Vendedores, seleccionar el vendedor que desea eliminar.

1.Una vez seleccionado dar clic en la opción **[Eliminar]**.

2.De manera informativa se muestra la información del vendedor.

3.Si el vendedor que desea eliminar es el correcto dar clic en **[Eliminar]**.

![IMG](4.png)

Se recomienda NO eliminar vendedores si su clave ya fue utilizada en los documentos de venta.

D.	Realizar Búsquedas de Vendedores

Si desea buscar a un vendedor en particular, dar clic en **[Buscar]** o presionar la tecla **[F2]**.

1.Se mostrará la siguiente pantalla en donde deberá identificar el vendedor deseado.

![IMG](5.png)

2.Dar doble clic o presionar la tecla **[Enter]** sobre el registro deseado para mostrar la información.

<h2 class="text-primary">E.	Exportar Catálogo de Vendedores a Excel</h2>


Este proceso es muy sencillo lo único que se tiene que hacer es:

1.En el menú de Ventas / Vendedores, seleccionar la opción de **[Excel]** y listo.

![IMG](6.png)