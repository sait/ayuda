﻿+++
title = "Recibir Reporte de Ventas Diario al Correo Electrónico"
description = ""
weight = 99
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=opDbw_ud9-U target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="recibir-reporte-de-ventas-diario-al-correo-electronico.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT trae para ti una nueva forma de conocer tus números diariamente sin tener que conectarte a la aplicación de SAIT, ya que puedes consultar cuánto vendiste el día anterior solo con acceder a tu correo electrónico.

Al momento de ingresar la primera vez al sistema SAIT desde la sucursal, se enviará de forma automática un correo electrónico con el reporte Facturas y devoluciones el cual se encuentra actualmente en el módulo de Ventas. 

Pero para esto, previamente deberá configurar los correos a los cuáles estará llegando dicha información, esta configuración la encuentra en Utilerías / Configuración General del Sistema / Pestaña Ventas2

![IMG](correo-ventas.png)

En caso de necesitar más de un correo se podrán separar por medio de comas, una vez lleno este dato damos clic en Cerrar y listo, de esta manera diariamente recibir un correo con el reporte sobre las ventas del día anterior.

El correo llegará de la siguiente manera:

* El asunto del correo siempre será: Reporte de Facturas y Devoluciones (20-Jul-2020) (en caso de manejar sucursales se agregará el nombre de la sucursal al asunto del correo).

* El rango de fechas de la consulta siempre será del día anterior.

* Si tiene enlace de sucursales, le llegará un correo por sucursal.

* El correo se enviará cuando la sucursal entre por primera vez al sistema.

![IMG](correo.png)

Detalle de reporte:

![IMG](reporte.png)



