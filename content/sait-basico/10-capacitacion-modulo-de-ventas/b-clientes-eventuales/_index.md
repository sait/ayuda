﻿+++
title = "Clientes Eventuales"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=14XpxRjtQeM&t=0s&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&index=6" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. B Clientes eventuales.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite llevar el registro de tus clientes eventuales, es decir aquellas empresas o personas físicas que no forman parte de tu cartera general de clientes. 

La información que se permite capturar en este catálogo, solamente hace referencia a los datos fiscales del cliente, que servirán en caso de facturación de contado. 

Para acceder a este catálogo deberá ingresar al menú de Ventas / Clientes Eventuales. 

<h2 class="text-primary">A.	Agregar un Cliente Eventual</h2>

1.Ir al menú de Ventas / Clientes Eventuales. 

![IMG](1.png)

2.Seleccionar la opción de **[Agregar]**. 

3.Ingresar la información necesaria en el formulario y dar clic en **[Agregar]**. 

![IMG](2.png)


<h2 class="text-primary">B.	Modificar un Cliente Eventual</h2>


1.En el menú de Ventas / Clientes Eventuales, seleccionar el cliente que desea modificar. 

2.Una vez seleccionado dar clic en la opción **[Modificar]**. 

3.Realizar las modificaciones necesarias en el formulario y dar clic en [Modificar].

![IMG](3.png)

<h2 class="text-primary">C.	Eliminar un Cliente Eventual</h2>


1.En el menú de Ventas / Clientes Eventuales, seleccionar el cliente que desea eliminar.

2.Una vez seleccionado dar clic en la opción [Eliminar]. 

3.De manera informativa se muestra la información del cliente. 

4.Si el cliente que desea eliminar es el correcto dar clic en **[Eliminar]**. 

![IMG](4.png)

<h2 class="text-primary">Realizar Búsquedas de Clientes Eventuales</h2>



1.Si desea buscar a un cliente en particular, dar clic en [Buscar] o presionar la tecla **[F2]**. 

2.Se mostrará la siguiente pantalla en donde deberá ingresar el nombre o apellido del cliente.

![IMG](5.png)

3.Dar doble clic o presionar la tecla **[Enter]** sobre el registro deseado para mostrar la información. 

<h2 class="text-primary">Exportar el Catálogo de Clientes Eventuales a Excel</h2>

Este proceso es muy sencillo lo único que se tiene que hacer es: 

1.En el menú de Ventas / Clientes Eventuales, seleccionar la opción de **[Excel]** y listo.

![IMG](6.png)