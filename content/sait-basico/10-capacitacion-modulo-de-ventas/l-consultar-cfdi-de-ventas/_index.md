﻿+++
title = "Consultar CFDIs de Ventas"
description = ""
weight = 9
+++
La ventana de consulta de CFDIs de ventas es una herramienta que nos permitira ver los CFDIs emitidos, volver a imprimirlos o reenviarlos a nuestros clientes.

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=fe1abYVwVBM" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>
### **Indicaciones**:
* Ir a Ventas / Consultar CFDI de ventas
* Se abrira la ventana "Consulta CFDIs de Facturas y notas de crédito"
* Dentro de la ventana podra filtrar los resultados de la consulta por medio de cliente, fechas, tipos, folio y UUID de los CFDIs. Clic en <Consultar> para mostrar resultado  
* En esta ventana se tienen los siguientes botones:
	* **Ver XML:** Muestra en pantalla el XML del CFDI seleccionado. En esta ventana podrá copiar a portapapeles el contenido del XML, guardar el archivo físicamente o mandar a imprimir XML.
	* **Ver PDF:** Muestra en pantalla versión impresa de CFDI en formato PDF, en donde podrá dar uso a dicho archivo generado.
	* **Excel:** Manda la consulta en pantalla a un libro Microsoft Excel, en donde podrá dar uso a dicho archivo generado.
	* **Enviar Indiv.** : Envia por correo electrónico el CFDI seleccionado al cliente.
	* **Enviar todos:** Envia todos los CFDIs que se encontraron en la consulta por correo electrónico a los clientes.
	* **Reporte de Envió:** Muestra un historial del envió de correos electrónicos, donde se podrá leer el estado de entrega de los mismos.
	* **Imprimir Indiv.** : Manda a imprimir el CFDI seleccionado a impresora.
	* **Imprimir Todos:** Manda todos los CFDIs que se encontraron en la consulta a impresión.
	* **Copiar:** Copia el contenido del campo 'Acuse de Cancelación'. El cual contiene fecha y hora de certificación, número de certificado y acuse de cancelación solamente cuando el CFDI se encuentra cancelado.
	
![IMG](image011.jpg)



