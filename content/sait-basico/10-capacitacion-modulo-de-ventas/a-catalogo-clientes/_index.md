﻿+++
title = "Catálogo de Clientes"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=pdmx1hnHbRM&t=0s&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&index=5" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. A Catálogo de Clientes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite llevar el registro de tu catálogo de clientes de una manera muy sencilla. 
Básicamente en la pestaña de datos generales deberá ingresar los datos de domicilio fiscal del cliente, necesarios para la facturación electrónica. 

En la pestaña de crédito, deberá ingresar los días y límite de crédito que se le otorgará al cliente, esto con el fin de elaborar facturas a crédito. 

En la pestaña de observaciones, es un campo "abierto" que puede utilizar como más le convenga, y en Métodos de Pago deberá especificar el método de pago que utiliza el cliente por default. 

Para acceder a este catálogo deberá ingresar al menú de Ventas / Clientes, o bien, presionando el acceso directo que se encuentra en la barra de herramientas: 

![IMG](clientes.png)

<h3 class="text-primary">Temario</h3>

* [Registro de Clientes](#registrar-clientes)
* [Modificar Clientes](#modificar-clientes)
* [Eliminar Clientes](#eliminar-clientes)
* [Definir Zonas y Clasificaciones](#definir-zonas-y-clasificaciones)
* [Agregar Zona](#agregar-zona)
* [Modificar Zona](#modificar-zona)
* [Eliminar Zona](#eliminar-zona)
* [Definir Crédito a Cliente](#definir-credito-a-cliente)
* [Configurar tipo de Impuesto / Retención a Cliente](#configurar-tipo-de-impuesto-retencion-por-cliente)
* [Asignar tipo de Impuesto / Retención a Cliente](#asignar-tipo-de-impuesto-retencion-por-cliente)

## REGISTRAR CLIENTES

1.Por default el sistema te asigna la clave automáticamente. 

2.Deberás ingresar el nombre del cliente o de la razón social, además de ingresar sus datos generales.

3.Al finalizar de capturar los datos necesarios, presionar el botón **[Grabar]**.

4.Listo.

![IMG](1.png)


## MODIFICAR CLIENTES

1.En cliente ingresamos el cliente que deseamos modificar, la Búsqueda se podrá realizar por Clave, Nombre, RFC, Clave Adicional, Nombre comercial y Teléfono, seleccionamos el registro y damos clic en **[Modificar]**.

2.Realizar las modificaciones necesarias. 

3.Presionar el botón **[Grabar]**.

4.Listo.

![IMG](2.png)

## ELIMINAR CLIENTES

1.En cliente ingresamos el cliente que deseamos modificar, la Búsqueda se podrá realizar por Clave, Nombre, RFC, Clave Adicional, Nombre comercial y Teléfono, seleccionamos el registro y damos clic en **[Modificar]**.

2.Confirmamos sea el cliente que necesitamos borrar y damos clic en el botón **[Borrar]**.

3.Listo.

<h4 class="text-danger">Se recomienda NO eliminar clientes cuando ya se hayan realizado documentos a su nombre, pues estos no se podrán consultar después.</h4>

![IMG](3.png)


## DEFINIR ZONAS Y CLASIFICACIONES

En SAIT puedes definir a tus clientes por la zona en que se encuentran ubicados (donde viven, zona geográfica, etc.) y/o clasificarlos según su giro de la empresa o trabajo que desempeñan. 

Esto lo puede generar desde el catálogo de Clientes (Ventas / Clientes) y seleccionar la opción de Zonas o Clasificaciones. Esto te proporcionará la siguiente ventana. 

![IMG](4.png)

## AGREGAR ZONA

1.Una vez estando dentro del catálogo de zonas, seleccionar la opción de [Agregar]. 

2.El sistema por default arroja la siguiente clave, pero puede ser modificada si lo desea.

3.Ingresar el nombre o descripción de la Zona. 

4.Seleccionar la opción de **[Agregar]** y listo.


## MODIFICAR ZONA

Una vez estando dentro del catálogo de zonas: 

1.Seleccionar la zona a modificar. 

2.Presionar el botón **[Modificar]**. 

3.Modificar el nombre o descripción de la zona.

4.Seleccionar el botón **[Modificar]** y listo.

## ELIMINAR ZONA

1.Seleccionar la zona a eliminar. 

2.Dar un click en el botón **[Eliminar]** y listo.

**Mismo proceso para las Clasificaciones**

## DEFINIR CREDITO A CLIENTE

En sistema SAIT se cuenta con un apartado especial en el catálogo del cliente que es definir el límite de crédito que se le otorgará al cliente. 

Una vez que te encuentres dentro del catálogo del cliente (Menú Ventas / Clientes), realiza lo siguiente: 

1.Selecciona la pestaña de crédito 

2.Proporciona los días de crédito que le otorgarás al cliente 

3.Proporciona la cantidad monetaria que le otorgarás de crédito 

4.Registra la información del contacto de cobranza y datos adicionales 

5.Seleccionas el botón **[Grabar]** y listo.

![IMG](5.png)

## CONFIGURAR TIPO DE IMPUESTO RETENCION POR CLIENTE


El proceso para configurar el sistema para que pida por cliente la tasa de impuesto / retención es el siguiente: 

1.Dirigirse al menú de: Utilerías / Configuración General del Sistema / Pestaña Ventas2

![IMG](6.png)

2.Palomeé ambas opciones en caso de que desee usarlas.

3.Para guardar los cambios, deberá hacer clic en el botón [Cerrar]. 

4.Es importante que para que los cambios se apliquen, será hacer necesario salir y volver entrar a SAIT. 

## ASIGNAR TIPO DE IMPUESTO RETENCION POR CLIENTE

1.Dirigirse al menú de: Ventas / Clientes y especificar el número de cliente. En caso de ser necesario de realizar una búsqueda por nombre del cliente, hacer clic en el botón **[?]**.

2.Una vez especificado el cliente, en la sección % IVA o % Ret IVA indique las cantidades que desee manejar.


![IMG](7.png)

3.Para guardar los cambios, deberá de hacer clic en el botón **[Grabar]**. 

4.Listo. 

