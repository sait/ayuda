﻿+++
title = "Descuentos y Promociones"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=gPp1YhOVQpI&t=0s&list=PLhfBtfV09Ai5FJx4f3R2A_SaTgItDf9pm&index=2" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. E Descuentos y Promociones.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT te permite aplicar descuentos a clientes en específicos para un artículo en particular o para alguna de las clasificaciones de los productos (Línea, Familia, Categoría, Depto.).

Si lo que desea es aplicar descuentos a <Todos los Productos> lo conveniente es otorgar el descuento directamente en el cliente, esto lo realiza desde el menú de Ventas / Clientes / Pestaña "Crédito"

<h3 class="text-primary">Descuentos por cliente</h3>

Ir al menú de Ventas / Descuentos y Promociones / Descuentos por Cliente.

![IMG](1.png)

1.Seleccionar la opción de **[Agregar]**.

2.Deberá seleccionar el cliente al que desea aplicar el descuento.

3.Deberá especificar el artículo al que SOLAMENTE desea aplicar el descuento (si lo deja en blanco se aplicará a todos los artículos)

4.O bien, si no es un artículo en particular, especificar la línea, familia, categoría o departamento.

5.Especificar la lista de precio a utilizar para los productos.

6.O bien, definir el porcentaje de descuento a aplicar para los productos definidos.

7.Ya que se definieron las restricciones deberá dar clic en **[Agregar]**.

![IMG](2.png)

En esta mismta ventana puede modificar o eliminar los descuentos otorgados a los clientes o realizar una búsqueda de todas las promociones a un cliente en específico.

![IMG](3.png)

<h3 class="text-primary">Definir Promociones</h3>

SAIT permite aplicar promociones para un artículo en particular, o para alguna de las clasificaciones de los productos (Línea, Familia, Categoría, Depto.).

Para acceder a esta opción de descuentos por cliente, deberá ingresar al menú de: 

Ventas / Descuentos y Promociones / Promociones

![IMG](4.png)

* Definir el **período** en el que permanecerá vigente la promoción.

* En caso de contar con el servicio de enlace de sucursales, **puede indicar las sucursales en las que se aplicará la promoción**, para agregarlas deberá separarlas por una coma ejemplo: 1,3,5,7. Si se deja en blanco, la promoción se aplicará en todas las sucursales.

* De ser necesario, **puede definir la promoción para un cliente en específico.** Si no se asigna ningún cliente, se van a considerar todos los clientes para la promoción.

* Indicar si la promoción se va a aplicar **para las ventas de contado y/o de crédito**.

* Indicar si la promoción se va a aplicar **para un Grupo de Articulo o para un Articulo en particular**.

* Grupo de Artículos: En esta opción se podrá aplicar la promoción **para una Línea, Familia, Categoría o Departamento**.

* Un Articulo: En esta opción se podrá aplicar la promoción **para un artículo en particular**, permitiendo definir directamente el precio incluyendo el IVA.

Puede otorgar el descuento a un artículo en específico y a su unidad principal (la que tiene definida en el catálogo de artículos)

![IMG](5.png)

Y si es un artículo que cuenta con unidades adicionales, también puede seleccionarlas para otorgar el descuento

![IMG](6.png)

Para agregar la promoción, deberá hacer clic en el botón **[Grabar]**.

Pueden **aplicar el número de promociones que deseen**, de la forma que la empresa lo requiera

<h3 class="text-primary">Consideraciones Previas</h3>

Antes de empezar a definir y utilizar las promociones dentro de SAIT, deberá de asegurarse que en la configuración general del sistema, este habilitada la facultad para el manejo de las promociones.

Para verificar lo anteriormente descrito, deberá de seguir los siguientes pasos:

Dirigirse al menú de: Utilerías / Configuración General del Sistema.

1.Hacer clic sobre la pestaña de: Ventas2, tal como se muestra en la siguiente imagen:

![IMG](configuracion.png)

En la sección de Manejo de Promociones, hacer clic sobre la opción "Usar Promociones (Versión Mejorada)"

Una vez seleccionada la opción, deberá hacer clic en el botón **[Cerrar]**.

Salir de SAIT y volver a entrar para que los cambios surtan efecto.

