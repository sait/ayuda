+++
title = "Enlazar ordenes asociadas a la compra después de cargar un CFDI"
description = ""
weight = 11
+++

A partir de la versión 2019.2.0 el sistema le permite después de cargar una compra a partir del XML poder enlazar a qué orden de compra pertenece.
El proceso para ingresar una compra a partir del XML sigue siendo el mismo el cual adjuntamos en la siguiente liga: 

**Ingresar compra a partir de un XML** https://ayuda.sait.mx/sait-erp/9-capacitacion-modulo-compras/c-registro-de-compras-desde-xml/

Una vez cargada la compra a partir del XML y dar clic F8 procesar
 
![IMG](1.png)

El sistema preguntará si la compra pertenece a una orden de compra

![IMG](2.png)

Si damos clic en **NO**, no pasa absolutamente nada y el proceso de compra sigue de la manera habitual.

Si damos clic en SI, el sistema mostrará la siguiente imagen en donde podemos relacionar la o las órdenes de compras a esta compra, para relacionar una orden de compra podemos ingresar directamente el folio o dando clic en el signo de interrogación ? para realizar una búsqueda.

Las validaciones para que se pueda enlazar una orden de compra dependen de lo siguiente:

* **Ser del mismo proveedor**

* **Se cuente con misma divisa que la compra que se está ingresando**

* **Las órdenes de compras no se encuentren canceladas o compradas completamente**

![IMG](3.png)

Al relacionar una orden de compra se nos habilitan los siguientes datos en las columnas Diferencia$ y Folio-Seq:

**Diferencia$:** esta columna hace referencia a los costos, es decir al mostrarse en color rojo índica que hubo un aumento en el costo del artículo en relación de la orden de compra y la factura que estamos ingresando, de mostrarse azul significa el costo bajó.

**Folio-Seq:** muestra el folio de la orden de compra, en este caso A10 seguido del número de partida o de renglón que estamos relacionando (en caso de que se requiera, se puede agregar más órdenes)

En este caso, la orden de compra con folio **A10** contaba con menos productos de los que vienen en la factura, por lo cual se pudiera relacionar otra orden de compra hasta completar las cantidades del XML, en este caso donde en la factura vienen más artículos que en la orden de compra, como en la siguiente imagen, en donde se relacionaron las órdenes compras con folio A10 y A9, en el último renglón no aparece la orden de compra **A9** ya que con la orden **A10** quedó completo la cantidad de artículos de la factura.

![IMG](4.png)

* La ventana cuenta con la poción de limpiar órdenes en donde se van a retirar todas las órdenes que se habían relacionado previamente.

* Se puede mandar dicha consulta a Excel en el botón del ícono de la aplicación

* Podemos cancelar el proceso

* Y finalmente podemos dar clic en F8 procesar.


{{<youtube jfHTw3oygKM>}}