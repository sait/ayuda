+++
title = "Usar cantidad por empaque al Ingresar Compras con XML"
description = ""
weight = 14
+++

Si usted compra a su proveedor en una unidad distinta (por lo regular una más grande) a su unidad principal, por ejemplo, si compra toneladas, pero su unidad es saco o compra cajas y usted las ingresa en piezas, puede utilizar esta funcionalidad que se activa en  Configuración General del Sistema / Compras 

![IMG](1.png)

Para tomar cambios deberá salir completamente del sistema y entrar nuevamente para recalcular valores.

Al ingresar la compra se habilitará el campo EMPAQUE que sería la equivalencia 

![IMG](1.1.png)

Para la prueba utilizaremos este XML en donde vienen 10 CAJAS, pero mi unidad principal de venta es PIEZA, aquí tenemos que hacer un cálculo para saber de estas 10 CAJAS cuántas PIEZAS representan, en este caso serían 80 PIEZAS, es por eso en el campo de empaque se puso la cantidad **8**, ya que **8x10**, me da un total de: **80**, que son las piezas que entrarían al inventario, damos clic a [F8=Continuar]

![IMG](2.png)

Observaremos que se llenan todos los datos relacionados a compra en automático al leer el XML y en la columna de EMPAQUE podemos observar el dato  que capturamos previamente, damos clic en [F8=Procesar]

![IMG](3.png)

Y listo, si entramos al kárdex del artículo observamos que el costo es de 15.00 pesos, lo equivalente a dividir $1,200 / 80 piezas.

![IMG](4.png)





