﻿+++
title = "Productos Asociados a Proveedores"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=yvQpSO-B-TQ&index=9&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. D Productos asociados a proveedores.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT es posible agregar una compra a partir de un XML para ello previamente debemos asociar los claves del proveedor con nuestras claves de SAIT, este proceso se puede hacer directamente al momento de capturar una compra o de manera individual por artículo. 

Para realizar este proceso diríjase a:

**Compras / Productos Asociados a Proveedores**

Es importante mencionar un artículo puede estar asociada a uno más proveedores ya que sus claves pueden ser distintas para un mismo artículo.
Para definir un proveedor más en el artículo nos posicionamos en el renglón en blanco e ingresamos:

1.La clave del proveedor

2.La clave del artículo del proveedor.

3.Clic en Grabar.

4.Listo.



![IMG](1.png)
