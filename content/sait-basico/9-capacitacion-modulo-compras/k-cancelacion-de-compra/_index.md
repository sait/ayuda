﻿+++
title = "Cancelación de Compra"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=S07VwZiKXsg&index=7&list=PLhfBtfV09Ai42x7k-fITysNCI98gpRVaI" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="9. K. Cancelar Documentos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El proceso de cancelación de compras es muy sencillo, si usted ha realizado cancelación de facturas este paso le será muy parecido. Para cancelar las compras realice los siguientes pasos: 

1.Realizar la consulta para elegir la compra a cancelar, para este paso diríjase a:

**Compras / Consultas Generales**

Aplique los filtros necesarios para encontrar más rápido la compra a cancelar y de clic en **[Consultar]**. Aparecerá la siguiente consulta en donde deberá seleccionar la compra que se cancelará, para seleccionar el folio de doble clic sobre el folio o presione **[Enter]** sobre el mismo.

![IMG](1.png)

2.Se nos mostrará la siguiente ventana y daremos clic en el botón **[Cancelar]**. 

![IMG](2.png)

3.El sistema nos pedirá confirmar la cancelación, en una ventana como la siguiente en donde deberá: 

* Definir la fecha de cancelación. 

* Escribir Comentarios o motivos de cancelación.

* Seleccionar el motivo. Escriba de nuevo el folio del documento a cancelar. 

* Dar clic en el botón **[Si]**. 

![IMG](3.png)


Listo, en este momento se ha realizado la cancelación de la compra de la forma correcta y se nos mostrará de la siguiente manera.

![IMG](4.png)


<h3 class="text-danger">NOTA</h3>

**Es importante recordar que las existencias de los artículos contenidos en la compra cancelada se verán afectadas a manera de salida por cancelación de compra.**

