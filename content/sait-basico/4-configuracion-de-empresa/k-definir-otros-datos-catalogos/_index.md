﻿+++
title = "Definir Otros Datos en Catálogos"
description = ""
weight = 12
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=nX7xbH1bpgY&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h&index=13&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. L Definir Otros Datos En Catalogos .pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT es posible agregar datos adicionales en los catálogos de Artículos, Clientes, Proveedores y vendedores, campos en cualquier reporte o formato en donde se haga referencia a estos catálogos.

Para configurar nuevos campos diríjase a:

<h4 class="text-primary">Utilerías / Configuración General del Sistema / Datos Adicionales en Catálogos</h4>

![IMG](1.png)

1.Seleccionar el catálogo donde se va a crear el nuevo campo, en este caso será Clientes.

Y dar clic en **[Agregar]**.

![IMG](2.png)

2.Llenar los datos:

* Escribir el título que tendrá el dato adicional.

* Especificar en qué posición estará, ya que se pueden crear varios nuevos campos y determinar el orden en que se van a mostrar.

* Escribir el nombre del campo que se va a capturar desde la ventana. Ejemplo: Fecha de Registro.
	
* Seleccionar el tipo de dato que se va a capturar: Numérico, Fecha, Texto, Texto Largo, Lógico.

* En caso de ser un dato numérico especificar la máscara. Ejemplo: 999,999.99

* Si es un dato obligatorio, activar la casilla [ * ] Obligar a capturar.

* En caso de restringirse a los usuarios, activar la casilla [ * ] Restringir visualización a usuarios autorizados.

* Activar la casilla [ * ] Replicar dato a las demás sucursales para que la información sea enviada por el enlace.

* Si el nuevo dato se trata de una lista de opciones que se pueden elegir, escribirlas en la Lista de Posibles Valores. Ejemplo: 534 8800, 536 3157.

* Se pueden eliminar del catálogo los nuevos campos a usar haciendo clic en [Eliminar Dato] en caso de que ya no se requiera visualizar (solo se borra del catálogo, el campo se sigue conservando en la base de datos si previamente fue creado). 

* Si este dato es totalmente nuevo deberá seleccionar **[Crear campo físicamente]**.

![IMG](3.png)

3.Se mostrará la siguiente pantalla:

![IMG](4.png)

* Deberá indicar nuevamente la tabla a la que desea afectar: Artículos, Clientes, Proveedores, Vendedores. 

* Indicar el nombre del campo como máximo 10 caracteres, sin espacios ni caracteres especiales. 

* Seleccionar el tipo de dato que se va a almacenar: Numérico, Fecha, Texto, Texto Largo, Lógico 

* Especificar el tamaño: la longitud de caracteres que se van a permitir (no aplica para todos los tipos de datos)

* Especificar la cantidad de decimales a utilizar en caso de haber seleccionado dato numérico. 	

* Por último hacer clic en **[Crear]**.

4.Finalmente en el catálogo de clientes podemos observar que este campo ya está disponible para su captura.

![IMG](5.png)