﻿+++
title = "Definir Motivos de Cancelación"
description = ""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=yUwDvQcRZP4&index=10&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. H Definir Motivos de Cancelaciones.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Al momento de cancelar o realizar devoluciones debemos seleccionar el motivo, en SAIT ya contamos con un catalogo de motivos por default. 

Pero en ocasiones es necesario definir algún motivo más específico, según las necesidades de la empresa, en caso de ser necesario agregar o modificar algún motivo diríjase a:

<h4 class="text-primary">Utilerías / Configuración General del Sistema</h4>


1.Clic en **[Motivos de Cancelación]**


![IMG](1.png)

Los folios de las claves están ordenados de la siguiente manera:

* Del 1 al 19 motivos de cancelación para módulo **Ventas**.

* Del 20 al 39 motivos de cancelación para módulo de **Inventarios**.

* Del 40 en adelante motivos de cancelación para módulo de **Compras**.
 

Recomendamos seguir con la misma estructura.

![IMG](2.png)

2.Para agregar uno nuevo, de clic en **[Agregar]**

![IMG](3.png)

3.Definimos la descripción del motivo de cancelación, agregamos la clave según el módulo al que pertenezca (en este caso clave 5), y seleccionamos el módulo en donde se considerará dicho motivo.

Clic en **[Agregar]**

![IMG](4.png)

4.Listo, desde ahora si consultamos algún documento desde el módulo de ventas y damos clic en [Cancelar] en las opciones del motivo de cancelación nos aparecerá el motivo recién agregado el cual definimos como “Refacturación”.

![IMG](5.png)
