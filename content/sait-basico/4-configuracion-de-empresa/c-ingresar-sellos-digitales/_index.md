﻿+++
title = "Ingresar Sellos Digitales"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=-LcOvuX6Qo0&index=4&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. C Ingresar Sellos Digitales.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Ingresar nuestros sellos digitales en SAIT es sumamente sencillo, para realizar este proceso diríjase a:

<h4 class="text-primary">Utilerías / Factura Electrónica / Certificados de Sellos Digitales (CSDs)</h4>


1.Dar clic en **[Agregar]**


![IMG](1.PNG)


2.Agregar los archivos requeridos.

Dar clic en el signo de interrogación para buscar la carpeta en donde están los archivos.

![IMG](2.png)

Nos aparecerá la siguiente ventana, seleccionamos primero el archivo .cer y después el archivo .key

![IMG](3.png)

Una vez agregados ambos archivos nos pedirá la contraseña de los sellos digitales.

Dar clic en **[Agregar]**.

![IMG](4.png)

En caso de que al agregar los sellos nos aparezca el siguiente mensaje (Esperando al archivoTMPWV6FW.TMP) puede ser por los siguientes motivos.


* La carpeta en donde colocamos los archivos .key y .cer en tiene espacios en el nombre.
* Los archivos .cer y .key tienen un espacio en el nombre.

![IMG](5.png)

Cómo deben estar nombrados nuestros archivos .cer  y .key

<center> CORRECTO </center>

![IMG](6.png)

<center> INCORRECTO </center>

![IMG](7.png)

3.Listo

Nuestros certificados de sellos digitales han sido agregados correctamente al sistema.

![IMG](8.png)