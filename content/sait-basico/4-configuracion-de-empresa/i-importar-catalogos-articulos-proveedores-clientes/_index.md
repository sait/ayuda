﻿+++
title = "Importar Catálogos de Artículos, Clientes y Proveedores"
description = ""
weight = 10
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=snENHNsVDbs&index=11&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. I Importar Catalogos de Articulos, clientes y proveedores.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El siguiente proceso describe los pasos para importar información desde Excel (Artículos, Proveedores y Clientes).

Puntos a considerar de las características que debe tener el archivo:

* El archivo de Excel debe convertirse a la versión 95, en caso de usar Excel 97 o 200al momento de grabarlo seleccione en tipo Excel 95.

* Las columnas no deben tener títulos.

* No debe haber renglones o columnas en blanco, ni ocultas.

* El orden de las columnas deben coincidir con el orden de los campos de la tabla en la que se van a colocar los datos

* Respetar el tipo de dato de cada campo (no dejar letras en donde solo se aceptan números o viceversa).

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Utilerías / Importar Información desde / Excel</h4>

1.Seleccione la tabla en la cual desea colocar la información que se va a importar desde Excel.

Clic en **[Siguiente]**


![IMG](1.png)

2.Agregue los campos de la tabla seleccionada. 

En dichos campos es en donde se va a colocar la información que se tiene en las columnas de Excel.

Haga clic en **[Siguiente]**

![IMG](2.png)

3.Seleccione el archivo de Excel que contiene la información que se desea importar a SAIT dando clic en el botón con signo de interrogación.


<h3 class="text-danger">El archivo de excel debe de estar cerrado</h3>

Una vez agregada la ruta donde se encuentra el archivo de excel.

Haga clic en **[Siguiente]**

![IMG](3.png)

4.Se muestra de manera informativa cada columna a importar, verificamos que todo está correcto antes de finalizar.

Haga clic en **[Final]**

![IMG](4.png)

5.Listo, nuestra información ha sido importada a nuestra tabla.
