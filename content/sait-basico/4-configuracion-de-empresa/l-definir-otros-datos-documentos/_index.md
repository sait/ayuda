﻿+++
title = "Definir Otros Datos en Documentos"
description = ""
weight = 14
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=2OEJDkZNzO8&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h&index=14&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. M Definir otros datos en documentos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Agregar Dato Adicional a Documento**:

SAIT permite poder capturar otros datos en los diferentes documentos existentes en el sistema, tales como cotizaciones, facturas, remisiones, pedidos y demás, dichos datos son por documento y son meramente informativos. 

Algunos ejemplos serían: número de guía, encargado, dirigido a.

Para habilitar la captura de Otros Datos en documentos de ventas siga diríjase a:

<h4 class="text-primary">Utilerías / Configuración General del Sistema / Pestaña de Ventas</h4>

1.Palomear la opción: Capturar otros datos en documentos

![IMG](1.png)

2.Una vez habilitar la opción dar clic en **[Otros Datos]**

![IMG](2.png)

3.Capturar el dato en el documento que desee.

Dar clic en **[Grabar]**

Salir del sistema y volver a entrar a SAIT. 

![IMG](3.png)

4.Dirigirse a Ventas / Registro de Ventas F4

5.Seleccionar el tipo de Documento: FACTURA, capturar los datos restantes del documento. 


Notará que de manera automática aparece la opción para capturar el otro dato creado, tal como se muestra en la siguiente ventana: 

![IMG](4.png)

### **Agregar en el formato de documento de factura el dato nuevo**:

(Este proceso aplica para los demás formatos del sistema, cotizaciones, pedidos, notas de venta, etc)

Si usted cuenta con una versión de sait igual o mayor a la 2018.8.0 (en donde se integra un catálogo de los formatos internos) para entrar a la edición del formato diríjase a:

Utilerías / Factura Electrónica / Configurar CFDI

Clic en **[Editar]**

![IMG](5.png)

Si usted cuenta con un formato personalizado diríjase a:

Utilerías / Modificar Formatos / Facturas y Notas de Crédito / Clic en **[Editar]**

O bien solo imprima una factura y de en clic **[Formato]**

![IMG](6.png)

Se nos abrirá la siguiente ventana en donde hacemos la modificación.

![IMG](7.png)

Deberá de localizar la barra de herramientas "Report Controls". En caso de no tenerla visible, para habilitarla debera de dirigirse al menu de: View / Report Controls Toolbar y hacer un clic sobre la opción. Se mostrar la siguiente barra: 

![IMG](8.png)


Deberá de hacer un clic sobre este icono: 


![IMG](10.png)



Dirigirse al área en blanco de la sección Page Header y sin dejar de presionar el boton Izquierdo del Mouse, arrastrarlo y soltar inmediatamente el botón, con esto aparecerá la ventana: Report Expression. 

Hacer un clic sobre el campo Expression y capturar la siguiente linea de comando: Valprop(Docum.OTROSDATOS,"No. de Guía"), tal como se muestra en la siguiente ventana: 

![IMG](9.png)

* Hacer clic en el botón **[OK]**. 

* Guardar los cambios en el reporteador, dirigiendose al menu: File / Save. Salir del reporteador. 

* Deberá procesar una factura e imprimirla en pantalla para asegurarse que se imprima de manera correcta. 

