﻿+++
title = "Definir Formatos de Factura, Nota, Cotización"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=rQKyGK_QKd8&index=7&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. E Definir Formatos de Factura, Nota, Cotizacion.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

<h2 class="text-primary">Facturas</h2>

A partir de la versión 2018.8.0 se integra una mejora dentro del sistema SAIT, la cual consiste en un Catálogo de Formatos Internos para CFDI 4.0

Para  elegir alguno de estos formatos diríjase a:

<h4 class="text-primary">Utilerías / Factura Electrónica / Configurar CFDI</h4>

Para que el sistema tome el diseño estos formatos no debemos contar formatos personalizados de facturas.

Elegimos el formato deseado y damos clic en **[Guardar Configuración]**


![IMG](1.png)

<h2 class="text-primary">Notas de venta</h2>

Por default el sistema cuenta con un formato de nota de venta en texto (no editable el equipos de 64 bits), en versiones más recientes el sistema ya integra un formato gráfico (editable).

Para elegir o modificar el formato de nota de venta diríjase a:

<h4 class="text-primary">Utilerías / Modificar Formatos / Nota de Venta </h4>

Para hacer alguna modificación el formato gráfico damos clic en **[Editar]**

Para guardar los cambios nos dirigimos a File y damos clic en **[Save]**. 

![IMG](2.png)

<h2 class="text-primary">Cotización</h2>

Para modificar el formato de cotizaciones diríjase a:

<h4 class="text-primary">Utilerías / Modificar Formatos / Cotizaciones</h4>

Para guardar los cambios nos dirigimos a File y damos clic en **[Save]**. 

![IMG](3.png)