﻿+++
title = "Configurar Esquema CFDI 4.0 y Formato Default de Factura"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=5PUKSSsqhVk&index=3&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. B Configurar Esquema CFDI 3.3.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Configurar el esquema 4.0 para la facturación es sumamente importante así como obligatorio, para definir este dato diríjase a:


<h4 class="text-primary">Utilerías / Factura Electrónica / Configurar CFDI</h4>


![IMG](1.png)

De igual manera en esta misma sección podemos definir otras opciones como qué formato PDF utilizar para nuestras facturas:

Seleccionamos el formato que deseemos y damos clic en **[Guardar Configuración]**
![IMG](2.png)

