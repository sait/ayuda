﻿+++
title = "Consultar Timbres Disponibles y Activar Alertas de Disponibilidad de Timbres"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=CLztNPvcUXI&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Con esta opción usted puede conocer en todo momento cuántos timbres fiscales tiene disponibles así como un historial de compras de paquetes realizados, para acceder a esta opción deberá seguir las siguientes intrucciones:

Para consultar sus timbres fiscales disponibles en SAIT diríjase a:

<h4 class="text-primary">Utilerías / Factura Electrónica / Timbres Fiscales Disponibles</h4>

También, ponemos a tu disposición una nueva opción con la que podrás **configurar mandar alertas a tu correo** al tener disponible equis cantidad de timbres, de esta manera, puedes prevenir el interrumpir tu proceso de facturación al quedarte sin folios fiscales disponibles, o simplemente, estar prevenido cuando ya estemos cerca de terminarnos nuestro paquete de timbres.

![IMG](disponibles.PNG)

