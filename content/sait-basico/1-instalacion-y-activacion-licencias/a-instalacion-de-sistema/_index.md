+++
title = "Instalación del Sistema"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=Aoo9Ky80eiU&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="Instalacion-de-sistema.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Instaladores:

**Sait Básico:** [https://www.sait.mx/download/sait-basico-instalador.exe](https://www.sait.mx/download/sait-basico-instalador.exe)

Otros paquetes:

**Sait ERP:** [https://www.sait.mx/download/sait-ERP-instalador.exe](https://www.sait.mx/download/sait-ERP-instalador.exe)

**Sait Nómina** [https://www.sait.mx/download/sait-nomina-instalador.exe](https://www.sait.mx/download/sait-nomina-instalador.exe)

**Sait Contabilidad:** [https://www.sait.mx/download/sait-contabilidad-instalador.exe](https://www.sait.mx/download/sait-contabilidad-instalador.exe)

Instrucciones generales a seguir para instalar SAIT:

1.Ejecutar el archivo instalador

Dar clic derecho al archivo que se descargó y seleccionar la opción de **[Ejecutar como Administrador]**

![IMG](instalador.png)

2.Bienvenido al asistente de Instalación de SAIT Básico
Dar clic en **[Siguiente]**.

![IMG](asistentesait.png)

3.Acuerdo de Licencia para el uso del sistema.
<br />
Para aceptarlo, seleccionar la opción de **[Acepto el acuerdo]** y dar clic en **[Siguiente]**.

![IMG](aceptaracuerdo.png)

4.Seleccionar la carpeta de destino.
Dar clic en el botón de **[Siguiente]**.
<br />
Por default se define la ubicación en donde se instalará el sistema, dependiendo del paquete SAIT que se haya elegido: 
<br />
C:\Sait
<br />

Esta ubicación puede ser modificable si se requiere.

![IMG](ruta.png)

5.Seleccionar las tareas adicionales.
<br />
Dejar opciones como aparecen en la siguiente imagen.

![IMG](tareasadicionales.png)

6.Listo para Instalar.
<br />
Dar clic en **[Instalar]**. 

![IMG](listparainstalar.png)

7.Instalando
<br />
Esperar mientras se termina el proceso de instalación del sistema SAIT.

![IMG](instalando.png)

8.Completando la instalación del sistema.
<br />
Dar clic en **[Finalizar]**. 

![IMG](instalacioncompleta.png)






