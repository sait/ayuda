﻿+++
title = "Proceso para Firma de Manifiesto"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=1AwuiJTy3fw&index=6&t=0s&list=PLhfBtfV09Ai79f1OtfJEgXuh4gRMa0p5h" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="4. D.1 Firmar Manifiesto.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para realizar el proceso de firma manifiesto deberá realizar el siguiente proceso:

1.Abrir correo electrónico con remitente service@solucionfactible.com


![IMG](1.JPG)


2.Dar clic en el correo electrónico y abrir enlace que aparece en el mismo

![IMG](MANIFIESTO.png)

3.Se abrirá la siguiente ventana, en la cual deberá subir su Firma Electrónica (Recomendamos utilizar el navegador Internet Explorer para realizar este proceso). 

* Clic en el botón "Examinar" para subir .cer y .key.  

* Posteriormente teclear su contraseña y dar clic en el botón "Ingresar"

![IMG](3.png)

4.Le aparecerá una tabla, en la cual deberá dar clic en el número que le genera de ID y clic en el botón "Firmar".

![IMG](4.png)

* Aceptar la ventana de aviso.

![IMG](5.png)


5.Su manifiesto quedará firmado cuando aparezca en Firmado el estatus de "Sí".

![IMG](6.png)

(Si desea descargar el archivo manifiesto, deberá dar clic en el botón "Descargar")