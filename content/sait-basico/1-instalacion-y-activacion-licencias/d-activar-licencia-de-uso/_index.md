+++
title = "Activar Licencia de Uso SAIT"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=c04q9-OI9Ik&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="activar-licencia-de-uso-SAIT.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para realizar la activación de licencia de uso de SAIT siga las siguientes instrucciones.

1.Para activar la licencia del sistema es necesario iniciar SAIT, seleccionar la empresa y dar clic en **[Accesar Empresa]**. 
 
 ![IMG](accederempresa.png)

2.Al acceder a la empresa se muestra la siguiente ventana en donde de manera informativa muestra la versión actual del sistema.  
<br /> 
Aún no se encuentra activada la licencia de uso del sistema, por ello se muestra el mensaje de Software en evaluación. 
<br />
Para activar el sistema se debe dar clic en **[Registrar Sistema]**. 

 ![IMG](registrarsistema.png)


3.Se mostrará la siguiente ventana donde deberá capturar su número de contrato y contraseña SAIT y un correo electrónico. 
<br />
Dar clic en **[Activar por Internet]**. 

 ![IMG](activarporinternet.png)

4.Posteriormente deberán capturar el nombre o alguna referencia para identificar la licencia que se activará. 
Dar clic en **[Activar Licencia]**.

 ![IMG](nombreequipo.png)

5.En esta ventana ya mostrará el nombre de la razón social a quien ha sido concedido el uso de la Licencia SAIT. 
Dar click en **[Continuar]**. 

 ![IMG](concedidoa.png)
