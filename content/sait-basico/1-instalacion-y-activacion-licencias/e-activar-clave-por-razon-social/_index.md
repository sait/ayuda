+++
title = "Activar Clave de Razón Social"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=z-VX4NzW_z4&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="activar-clave-de-razon-social.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>


### **Indicaciones**:

Al crear una nueva empresa o en algunas ocasiones después de actualizar la versión del sistema, SAIT solicitará la clave de Activación de Empresa. Para realizar esta configuración siga las siguientes instrucciones.

1.Se requiere ingresar el RFC y razón social de la empresa, así como el contrato SAIT y la clave de activación. 
Dar clic en **[Registrar]**.

![IMG](Ingresardatos.png)

2.La clave de activación se genera desde la siguiente página: 
[**http://www.sait.com.mx/claves**] (http://www.sait.com.mx/claves)
<br />
Seleccionar la opción de **[Activar Clave Versión 2015]**. 
<br />
![IMG](saitclaves.png)

3.Se deberán ingresar correctamente los datos que solicita el siguiente formulario. 
Dar clic en **[Activar]** para generar la clave de activación. 

![IMG](clicactivar.png)
 
4.Seleccionarla la clave generada y copiarla para posteriormente pegarla en la ventana donde la solicita.

Dar clic en el botón de **[Registrar]**. 

![IMG](copiarserie.png)
 
De esta manera ya podrás ingresar correctamente a la empresa de SAIT. 
 

![IMG](ingresarsait.png)