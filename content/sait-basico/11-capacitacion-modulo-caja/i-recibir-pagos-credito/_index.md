﻿+++
title = "Recibir Pagos Crédito"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=2PMbRcZkQJ4&index=9&t=0s&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. I Recibir pagos de credito.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Permite registrar pagos de ventas de crédito que han sido procesadas desde Registrar Ventas o Registro de Ventas (F4).

Para realizar este proceso dentro de SAIT siga las siguientes instrucciones:

1.Entre al menú de Caja / Recibir pagos (crédito).

![IMG](1.png)


2.Escriba la clave del cliente. Puede realizar búsquedas al presionar la tecla **[F2]**.

3.Seleccione la forma de pago (dependiendo de la forma el sistema pedirá o no folio/referencia)

4.Especifique la cantidad del pago.

5.Seleccione la divisa.

6.Especifique la cantidad que se va a pagar en cada documento (En caso de tener más de uno).

7.Presione la tecla **[F8]** para procesar el pago.


**En caso de que el concepto de pago tenga definido un formato, se puede imprimir un comprobante del pago recibido.**

![IMG](2.png)