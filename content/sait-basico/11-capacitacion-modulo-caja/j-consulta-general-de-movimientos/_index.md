﻿+++
title = "Consulta General de Movimientos"
description = ""
weight = 9
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=dVMSwv8hbPY&index=10&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&t=3s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. J Consulta General de Movimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

La consulta general de movimientos de caja permite consultar todos los movimientos de entrada o salida en el corte.

1. Entre al menú de Caja / Consulta General de Movimientos.

2. Especifique las restricciones de la consulta:

* Rango de fechas.
* Rango de folios (Es el número de movimiento registrado en el corte).
* Rango de números de corte.
* Sucursal (En caso de que la empresa maneje enlace de sucursales).
* Tipo de movimiento de acuerdo a los Tipos de Movimientos que se tengan configurados.
* Cliente.
* Beneficiario.

3. Haga clic en **[Consultar]**

Se muestran los movimientos que cumplan con las restricciones de la consulta como en la siguiente imagen:

![IMG](1.png)

Puede hacer doble clic o presionar la tecla **[Enter]** sobre el movimiento para hacer una consulta individual:

![IMG](2.png)


La consulta puede ser enviada a Excel.