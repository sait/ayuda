+++
title = "Aplicar Nota de Crédito en Caja como Forma de Pago"
description = ""
weight = 16
+++

Al realizar una nota de crédito en SAIT, usted puede seleccionar la forma de devolución ya sea Efectivo o Crédito

![IMG](1.png)

 Cuando seleccionamos Efectivo, se le devuelve al cliente el efectivo y ahí termina el proceso, si seleccionamos Crédito, se le generará un saldo a favor al cliente en el estado de cuenta y después podrá aplicar ese saldo a favor

 Para conocer el proceso completo de devoluciones, puede der la documentación [aquí](/sait-basico/10-capacitacion-modulo-de-ventas/g-devoluciones-y-notas-de-credito/)

 Para hacer uso de una nota de crédito en Caja siga las siguientes instrucciones

- Vaya a Caja / Registrar Ventas

- Llene el documento de venta de manera normal, de clic en F8=Procesar 

- Seleccione la forma de pago Nota de Crédito

- Indique el importe que usará de la Nota de Crédito (éste puede ser menor al total de la NC y mientras tenga saldo pendiente por aplicar, podrá aplicarla nuevamente)

- Después deberá agregar en la sección de Referencia el folio de la NC y deberá llenarlo de esta forma 

    **DV-FolioNotaDeCrédito** Ejemplo, DV-AA97

- Damos clic en Procesar 

![IMG](aplicacion.png)

- El movimiento en caja se verá como en la imagen

![IMG](caja.png)
En caso de haber hecho una devolución de una Nota de Venta el prefijo cambia a DN-FolioNotaDeCrédito

El proceso es el mismo si usted utiliza Ventas / Registar Ventas