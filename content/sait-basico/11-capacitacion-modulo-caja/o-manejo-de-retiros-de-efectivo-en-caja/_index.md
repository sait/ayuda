+++
title = "Manejo de Retiros de Efectivo en Caja"
description = ""
weight = 14
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>

<div class="card">
<a href="retiros-de-efectivo-en-caja.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El módulo de caja cuenta con la funcionalidad de avisar cuando su cajón de dinero llegue a cierto importe de efectivo, dicha cantidad puede ser definida según las necesidades de la empresa, esto con el objetivo de no manejar grandes cantidades de efectivo en caja por cuestiones de seguridad y a su vez avisarle al cajero y/o vendedor que debe de realizar un retiro de efectivo al llegar a cierto monto.

Este parámetro es independiente por sucursal y no se comparte por enlace en el escenario en que tenga enlazadas sus sucursales.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Utilerías / Configuración General del Sistema / Pestaña Caja - Punto de Venta</h4>

En la parte inferior Izquierda, localizar los campos: "Dar aviso de retiro de efectivo cuando el importe en caja llegue a:" y "No permitir vender cuando llegue a:"

Deberá capturar el importe al cual llegará el corte de caja para que el sistema empiece a dar aviso de realizar el retiro de efectivo en caja, esto posicionado sobre el campo: "Dar aviso de retiro de efectivo cuando el importe en caja llegue a:"

Y también deberá capturar el importe el cual servirá como tope para YA NO permitir al cajero seguir aceptando o realizando ventas y con esto obligarlo a realizar el retiro de efectivo de caja, esto posicionado sobre el campo: "No permitir vender cuando llegue a: ", tal como se muestra en la siguiente imagen.

![IMG](img.png) 

Una vez establecidos los importes, para guardar los cambios es necesario hacer clic sobre el botón [Cerrar]. 


### Funcionamiento de Retiros de Efectivo en Caja

A continuación se explica como registrar retiros de efectivo en caja, con el objetivo de que el cajero no cuente con mucho ingreso en valor en su corte de caja.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Caja / Otras Salidas de Efectivo</h4>

El usuario deberá seleccionar el tipo de movimiento "RETIRO DE EFECTIVO A BÓVEDA"

Deberá de capturar el importe y la divisa de la cantidad a retirar.

Hacer clic en el botón [Procesar].

![IMG](retiro.PNG) 

Listo, con esto hemos procesado de manera exitosa el retiro de efectivo en nuestro corte de caja. 

