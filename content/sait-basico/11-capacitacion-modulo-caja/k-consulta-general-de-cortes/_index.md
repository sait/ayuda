﻿+++
title = "Consulta General de Cortes"
description = ""
weight = 10
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=Cxcw0KBpPQo&index=11&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6&t=2s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. K Consulta General de Cortes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

La consulta general de cortes permite consultar todos los cortes registrados.

1.Entre al menú de Caja / Consulta General de Cortes.

2.Especifique las restricciones de la consulta:

* Rango de fechas.

* Rango de números de corte.

* Sucursal (En caso de que la empresa maneje enlace de sucursales).

* Usuario que abrió el corte.

* Número de caja a la que pertenece.


3.Haga clic en **[Consultar]**

4.Se muestran los cortes que cumplen con las restricciones de la consulta.

La consulta se pude enviar a Excel en caso de ser necesario.
![IMG](1.png)

5.Puede hacer doble clic o presionar la tecla **[Enter]** sobre el corte para hacer una consulta individual (para que muestre la información esta pantalla el corte deberá tener estatus de ENTREGADO).

![IMG](2.png)

De tener el permiso habilitado le será posible modificar lo capturado en su momento al momento de Entregar el Corte.





