﻿+++
title = "Reportes Disponibles"
description = ""
weight = 99
+++

Formato de caja Resumen de Ventas que incluye IVA al 8%: [Descargar](corte-caja-02b-(resumen-de-ventas-2019conIVAal16y8).rpt)

Formato de punto de venta Corte de Cajero que incluye IVA al 8%: [Descargar](corte-de-cajero.rpt)

Formato de punto de venta Corte Z que incluye IVA al 8%: [Descargar](corte-z.rpt)

Formato de nota de venta tamaño 58mm: [Descargar](nota-de-venta-58mm.rpt)

Corte de caja tamaño 58mm: [Descargar](CORTES-58mm.zip)