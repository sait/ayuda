﻿+++
title = "Corte de Caja en 5 Pasos"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=Z5ihjUhmTxQ&t=0s&index=2&list=PLhfBtfV09Ai6ckyrdEilzhmvtCu_lZYz6" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="11. A Corte de caja en 5 pasos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Si usted utiliza el módulo de Caja en SAIT y lleva su operación por cortes, es necesario conozca estos 5 pasos para completar el proceso de corte de caja.

<h2 class="text-primary">PASO 1 / Abrir Corte de Caja</h2>

Primeramente, al abrir corte de caja debemos definir en Pesos la cantidad de dinero que se está dejando como Fondo, para ello diríjase a:

**Caja / Abrir corte**

![IMG](1.PNG)

Dar clic en el botón de **[Abrir Corte]**.

Nos aparecerá siguiente ventana.

![IMG](2.PNG)

Damos clic en **[Si]**.

<h2 class="text-primary">PASO 2 / Registrar Ventas</h2>

Una vez abierto el corte empezaremos a registrar todas las ventas que se presente en nuestro turno, diríjase a:

**Caja / Registrar Ventas**

![IMG](3.PNG)

<h2 class="text-primary">PASO 3 / Registrar Salida de Fondo</h2>

Es importantes ANTES DE CERRAR corte registrar una salida de efectivo por la misma cantidad que recibimos al iniciar nuestro turno, en este caso los mismo $ 500 pesos con los que inicié.

**Caja / Otras Salidas de Efectivo**

![IMG](4.PNG)

Dar clic en el botón de **[Procesar]**

Aparecerá la siguiente ventana

![IMG](5.PNG)

Clic en **[Sí]**

<h2 class="text-primary">PASO 4 / Cerrar Corte de Caja</h2>

Al finalizar nuestro corte es necesario cerrarlo, para realizar este proceso siga las siguientes instrucciones:

**Caja / Cerrar Corte**

Al cerrar le aparecerá primeramente esta imagen

![IMG](6.PNG)

**Si ya realizó este paso los de clic en sí, si no de clic en no y regrese al Paso 3.**

Aparecerá la siguiente ventana

![IMG](7.PNG)

Damos clic en **[Sí]**

![IMG](8.PNG)

Nos aparecerá la ventana en donde nos indica que el corte fue cerrado.

![IMG](9.PNG)

<h2 class="text-primary">PASO 5 / Entregar Corte de Caja</h2>


Finalmente debemos entregar nuestro corte de caja, es decir, todo lo que tenemos físicamente (efectivo pesos y dólares, vauchers, cheques, etc) en caja.

Para entregar el corte de caja siga las siguientes instrucciones:

**Caja / Entregar Corte**

Ingresamos el folio del corte que acabamos de cerrar.


![IMG](10.PNG)

Debemos ingresar todo lo que tenemos en la caja, es muy importante mencionar que siempre que ingresemos un importe se debe especificar el concepto es decir el tipo de pago, en ocasiones se puede dejar como se muestra en la siguiente imagen, lo cual es INCORRECTO y ocasionará una inconsistencia en el corte (ver siguiente imagen).

![IMG](11.PNG)

Listo, una vez ingresado todo lo que tenemos en nuestra caja daremos clic en **[Registrar]** y saldrá impresa la tira del corte de caja.

<h2 class="text-primary">A CONSIDERAR</h2>

* Las cancelaciones de Notas de Venta o Facturas deberán realizarse desde el Módulo de Caja en el apartado:

![IMG](cancelar.png)

Esto con el fin de que se regrese la mercancía al inventario y a su vez se afecte el corte de caja.

* Una vez cerrado el corte ya no se podrán hacer movimientos en él.

<h2 class="text-primary">ACCESOS RÁPIDOS</h2> 

**Tecla F2** Buscar Productos

**Tecla F8** Procesar la nota de venta

**Tecla F4** Abrir Ventana Para Facturas, Cotizaciones, Remisiones, Pedidos y Manejar Listas de precios

**Tecla F5** Buscar Clientes

**Tecla F6** Otras (Hacer Facturas, Apartados, Devoluciones, Limpiar la ventana, Salida de Efectivo)

**Tecla F9** Abrir calculadora

**Tecla Supr** Eliminar partidas

**Tecla TAB** Para moverse entre los campos

**Tecla Barra espaciadora** Cambiar valores en el campo de Efectivo

**Tecla ESC** Cerrar cualquier ventana
