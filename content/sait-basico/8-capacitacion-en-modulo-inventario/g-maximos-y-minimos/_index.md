﻿+++
title = "Definir Máximos y Mínimos"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=bdB_ddbt-iM&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=19&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. G. Calcular Máximos y Mínimos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

<h2 class="text-primary">Definir Máximos y Mínimos</h2>

SAIT cuenta con una herramienta para realizar el cálculo de máximos y mínimos en base al volumen de venta de cierto periodo, lo cual permite tener una definición más aproximada de acuerdo con las ventas mensuales.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Inventario / Calcular Máximos y Mínimos</h4>

1.Puede hacer la consulta por medio de las siguientes restricciones:

* Si su empresa maneja sucursales o almacenes, deberá especificar la sucursal o almacén al que desea realizar el cálculo.

* Período de ventas a considerar para realizar el cálculo del promedio de venta mensual

* Tipo de documento de venta a incluir: Facturas, Notas de Venta, Remisiones

* Artículos a incluir: Línea, Familia, Categoría o Departamento

* Proveedor que tiene asignado el producto dentro del Catálogo de Artículos

* Se pueden incluir artículos que contengan una palabra en su descripción.

* Se puede excluir ventas de clientes en específico, con el objetivo de no incluir grandes 
volúmenes de venta que solo a dichos clientes se les hacen.

2.Debe hacer clic en el botón de **[Consultar]**, para mostrar la información según las restricciones especificadas.

3.Para hacer una nueva consulta puede presionar el botón de **[Limpiar]**.

4.En la consulta se visualizará la información de cada uno de los productos, como el alta, las ventas realizadas en ese rango de fechas, el promedio mensual, la última venta realizada, la ultima compra y la existencia actual. 

Con la barra inferior puede dar clic a la derecha para consultar los nuevos máximos y los máximos, mínimos y punto de reorden actual. En este caso como nunca hemos definido Máximos y Mínimos la información se mostrará de esta manera.


![IMG](1.png)

5.Para hacer el cálculo debe especificar el máximo para “n” cantidad de meses. Es decir, si su consulta es por un rango de fechas por 3 meses, la cantidad máxima la puede necesitar para 3 meses, el punto de reorden a un mes y medio; y el mínimo para un mes, por ejemplo

![IMG](2.png)

Usted va a especificar la cantidad de meses que usted necesite contar para un máximo, punto de reorden y mínimo. En la cuadricula se verá reflejado las nuevas cantidades correspondientes para los máximos y mínimos. 

Así mismo se visualizará sus respectivas diferencias. 

6.Las columnas se pueden reordenar en caso de que lo necesite.

7.Para finalizar deberá presionar el botón de **[F8=Procesar]** para grabar la información en el catálogo de artículos y servicios.

8.Si no maneja sucursales o almacenes, estos nuevos máximos y mínimos se grabarán en el menú de Inventario / Catálogo de Artículos y Servicios.

![IMG](3.png)


9.Si su empresa maneja sucursales o almacenes, estos nuevos máximos y mínimos se grabarán en el menú de **Inventario / Existencias en Sucursales – Almacenes**.

![IMG](4.png)


<h3 class="text-danger">NOTA</h3>

Es importante mencionar que la información que arroja como resultado es un **aproximado**, depende mucho de los movimientos que los productos tengan dentro de su empresa, por ejemplo, afectan los productos de temporada, es decir, si hacemos la consulta en un período en donde ciertos productos se encuentran en punto de venta más alta, obviamente nos arrojaría un máximo o un mínimo distinto respecto a períodos en donde nunca se vende. O bien, también afectan las ventas que se hagan a ciertos clientes, pues el volumen de venta que se realice a dicho cliente, no necesariamente se realizar a cualquier otro cliente que acuda a nuestra empresa. 

Es importante considerar todos los aspectos que influyan en los movimientos de los artículos dentro de su empresa como por ejemplo las existencias negativas.


<b> <h1 class="text-primary" style="color:black">PROCESOS ADICIONALES</h1></b>

### Pasos para Generar Pedido Sugerido de Compras

* **Asignar mínimos, máximos y puntos de reorden a sus artículos**

* **Asignar un proveedor a los artículos**

* **Consultar el auxiliar de pedidos**

<h2 class="text-primary">Asignar un proveedor a los Artículos Pedido Sugerido de Compras</h2>


Para poder realizar este proceso dentro SAIT diríjase a: 

<h4 class="text-primary">Inventario / Catalogo de Artículos </h4>

1.Seleccione la pestaña de "Clasificación" 

2.Para cada artículo debe asignarle el proveedor. Puede ser aquel al que usualmente compran el artículo. 

3.Haga clic en **[Grabar]** 

![IMG](proveedor.png)


<h2 class="text-primary"> Generar Pedido Sugerido de Compras </h2>

Después de haber asignado el proveedor y capturar los máximos y mínimos de cada artículo se puede generar el pedido sugerido de compra.

Para poder realizar este proceso dentro SAIT diríjase a: 

<h4 class="text-primary">Compras / Registro de Compras</h4>

1.Capture la clave del proveedor 

2.Seleccione el tipo de documento a procesar: Orden 

3.Haga clic en **[Pedido]** 

![IMG](compra.PNG)

4.Se muestra la ventana de "Auxiliar de Pedido a Proveedor".

La consulta de los artículos se puede restringir por: 

* Clave del proveedor cuyo pedido sugerido desea generar. Si se omite este dato, se van a incluir los artículos de todos los proveedores. 

* Línea y Familia a la que pertenecen, si omite estos datos, se van a incluir los artículos.

* Rango de fecha de ventas. 

* Seleccione los artículos a incluir: 

* Todos los artículos que tengan definido el máximo: Únicamente se mostrarán los artículos que tienen definido el máximo

* Artículos con existencia menor al mínimo 

* Artículos con existencia menor al punto de reorden 

6.Haga clic en **[Generar Pedido Sugerido]**

![IMG](AUXILIAR.png)

7.Se muestran los artículos que cumplan con las restricciones de la consulta 

* En la columna "Comprar" se puede observar la cantidad que el sistema sugiere ordenar para su compra. 

* Si su nivel de acceso se lo permite, puede editar esta información 

* La sugerencia de compra se puede imprimir o enviarla a excel 

* También se pueden cargar únicamente insumos al activar la casilla 

* Si lo desea, se puede cargar un traspaso para incluirlo dentro de la consulta 

* En caso de que desee generar y enviar la orden de compra al proveedor, presione el botón **[Continuar]**

8.Se muestra la ventana de Registro de Compras con los artículos previamente cargados.

9.Termine de procesar la orden de compra en **[F8 Procesar]** y estará lista para enviarse a su proveedor.

![IMG](orden.PNG)

<h4 class="text-danger">NOTA</h4>

El sistema también cuenta con un reporte para consultar un auxiliar de pedido, el cual se localiza dentro del menú de Inventario / Reporte de Artículos / Stock Mínimo y Máximo.



