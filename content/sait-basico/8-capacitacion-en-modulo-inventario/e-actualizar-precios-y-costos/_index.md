+++
title = "Actualizar Precios y Costos de Artículos"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=BT1MLVLDs_c&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=6&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. E. Actualizar precios y costos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

<h2 class="text-primary">Actualizar Precios</h2>

Esta opción es una alternativa para realizar cambios de precios a sus artículos sin correr el riesgo de modificar algún otro dato.
Esta opción solo se podrá utilizar cuando el manejo de precios sea precios fijos.

1.Deberá dirigirse al menú de Inventario / Actualizar Precios y Costos / Actualizar Lista de Precios

2.Aparecerá la siguiente ventana:


![IMG](1.png)


3.Escriba la clave del artículo que desee actualizar el precio o presione **[F2]** para buscar.

4.Aparecerá la descripción y los datos del articulo

5.Actualice los precios necesarios

6.Haga clic en el botón **[Grabar]**.

<h2 class="text-primary">Actualizar Costos</h2>

El proceso de actualizar costos es un proceso muy delicado, ya que en ello está involucrado el precio de venta de artículos y el valor del inventario. 

<h4 class="text-danger">Por lo general, los costos se actualizan con la captura de las compras y solo en casos muy en específicos será necesario modificarlos</h4>
 
En esta opción se puede establecer el costo de reposición, el cual contablemente se utiliza para valorizar las existencias o inventarios cuando existe Inflación. 

Para realizar la actualización de costos siga los pasos descritos a continuación: 

1.Dirigirse al menú de Inventario / Actualizar Precios y Costos / Actualizar costos 

2.Escriba la clave del artículo que desee actualizar el costo o presione **[F2]** para buscar. 

3.Aparecerá la descripción y los datos del artículo 

4.En la sección de nuevos datos establecer el costo de reposición o máximo costo y la fecha 

5.Haga clic en **[Grabar]**. 

6.Listo

![IMG](2.png)

<h2 class="text-primary">Actualizar Precios por Volumen</h2>

SAIT cuenta con esta configuración especial que le puede servir para dar un precio especial si el cliente lleva cierto número de unidades.

Para utilizar esta opción, deberá de verificar que en Utilerías / Configuración General del Sistema / Ventas2 / Estén habilitadas estas opciones

![IMG](VOL5.png)

Y después  deberá seguir los pasos descritos a continuación: 

1.Dirigirse al menú de Inventario / Actualizar Precios y Costos / Precios por Volumen

2.Escriba la clave del artículo o presione **[F2]** para buscar. 

3.Aparecerá la descripción y los datos del artículo.

Podrá activar las columnas de Precio sin IVA, Precios con IVA o por %Margen de Ganancia según cómo se desea manipular los precios.

![IMG](VOL.png)

4.En la primer columna de Cantidad que debe comprar para obtener un mejor precio, deberá cantidad el número de unidades.

![IMG](VOL2.png)

A continuación unn ejemplo práctico con este artículo que su precio base por unidad es de $200 pesos para otorgar un mejor precio, se aplicarán las siguientes políticas. 

* Entre 5 y 9 unidades el precio será de $180.00 pesos

* Entre 10 y 19 unidades el precio será de $170.00 pesos

* Entre 20 y 24 unidades el precio será de $160.00 pesos

* Más de 25 unidades, el precio será de $150.00 pesos

![IMG](VOL3.png)

Una vez configurado, al momento de vender, dependiento las unidades vendidas, será el precio especial que le sugerirá.

![IMG](VOL4.png)

<h2 class="text-primary">Historial de Precios</h2>

A partir de la versión 2024.23 se agregó la siguiente funcionalidad en donde puede consultar un historial de cambios de precios, dicha ventana la puede consultar desde ‘Inventario / Actualizar precios y Costos / Historial de Precios’

La ventana mostrará los cambios de precios realizados, dicha ventana cuenta con distintos filtros para agilizar la consulta y está preparada para enviar la información a excel

![IMG](historial1.png)

La columna de % Incr. se refiere al porcentaje de incremento en base al precio1 con el cual se calcula el precio público

![IMG](historial4.png)

A partir de que se instala la versión, el sistema empazará a guardar el historial, si se trata de un artículo que no había tenido cambios de precio utilizando esta versión, aparecerá de la siguiente forma, es decir con la columna de "Anterior" vacía

![IMG](historial2.png)

Si es un artículo el cual ya ha tenido cambios de precios, mostrará el Anterior y el Nuevo

![IMG](historial3.png)

La ventana ya está preparada para enlace de sucursales, no es necesario agregar un evento adicional
