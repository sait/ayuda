+++
title = "Cerrar conteo"
description = ""
weight = 7
+++

Para finalizar la toma de inventario, deberá dirigirse al menú de Inventarios / Toma de Inventario / Conteos de Inventario. Al entrar a este ventana, nos mostrará todas las tomas de inventario abiertas.

![Conteos10](conteos10.png)

Seleccione el conteo que desea cerrar y haga clic en [Ver Conteo]

![Conteos14](conteos14.png)

Verifique que los datos sean correctos. Haga clic en [Cerrar Conteo] para finalizar la captura de las cantidades físicas del artículo. El sistema le preguntará si desea cerrar el conteo. Confirme la respuesta.

![Conteos15](conteos15.png)

Listo ! El conteo ha sido cerrado, y los artículos vuelven a quedar disponibles para ser incluidos en una nueva toma de inventario.

Una vez cerrado, se permite visualizar las diferencias entre la cantidad contada contra la existencia actual. Además, se puede ver la valuacion de los artículos según el último costo, precio, pesos y dólares en caso de que maneja artículos en ambas divisas.

![Conteos16](conteos16.png)
