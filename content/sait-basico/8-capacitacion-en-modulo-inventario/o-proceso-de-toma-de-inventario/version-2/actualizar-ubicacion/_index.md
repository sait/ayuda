+++
title = "Actualizar ubicación"
description = ""
weight = 8
+++

En caso de que desee actualizar la ubicación actual de los artículos por la ubicación capturada en el conteo, deberá dirigirse al menú de Inventarios / Toma de Inventario / Conteos de Inventario. Al entrar a este ventana, deberá consultar los conteos con status: Cerrado.

![Conteos17](conteos17.png)

Seleccione el conteo y haga clic en [Ver Conteo]

![Conteos18](conteos18.png)

Para actualizar la ubicación, haga clic en [Actualizar Ubicación]. El sistema le preguntará si desea actualizar la ubicación de los artículos. Confirme la respuesta.
