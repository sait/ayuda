+++
title = "Realizar ajustes"
description = ""
weight = 9
+++

El último paso en la toma de inventario, es el registro de los ajustes para que la existencia actual sea igual a la cantidad contada (inventario físico).

Para ello, es necesario que se registre automáticamente el movimiento de entrada o salida en el inventario de cada artículo según lo determine la diferencia entre la existencia actual del artículo contra la cantidad contada (inventario físico).

Deberá dirigirse al menú de Inventarios / Toma de Inventario / Conteos de Inventario. Al entrar a este ventana, deberá consultar los conteos con status: Cerrado.

![Conteos17](conteos17.png)

Seleccione el conteo que desea ajustar y haga clic en [Ver Conteo]

![Conteos19](conteos19.png)

Verifique que los datos sean correctos. Haga clic en [Realizar Ajustes] para que el sistema registre las entradas y salidas según sea el caso de cada artículo. El sistema le preguntará si desea ajustar el conteo. Confirme la respuesta.

Listo ! Las existencias han quedado ajustadas de acuerdo al conteo realizado.
