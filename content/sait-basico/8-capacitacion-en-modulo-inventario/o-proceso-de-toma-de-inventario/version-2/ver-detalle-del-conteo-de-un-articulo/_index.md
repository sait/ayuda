+++
title = "Ver detalle del conteo de un artículo"
description = ""
weight = 6
+++

Al entrar dentro de la consulta del conteo, se puede ver el detalle de cada articulo posicionando el cursor sobre él y haciendo clic en [Ver detalle]

![Conteos13A](conteos13A.png)

Se muestra el detalle del conteo del articulo seleccionado:

![Conteos13B](conteos13B.png)

Si el conteo aún está abierto, en caso de ocupar hacer correcciones en la cantidad contada deberá eliminar el conteo haciendo clic en [Eliminar] y volver a capturar la cantidad contada.

También se permite capturar observaciones relacionadas al proceso.
