+++
title = "Ver detalle del conteo"
description = ""
weight = 5
+++

Deberá dirigirse al menú de Inventarios / Toma de Inventario / Conteos de Inventario. Al entrar a este ventana, nos mostrará todas las tomas de inventario abiertas.

![Conteos10](conteos10.png)

En caso de que el conteo del cual desea ver el detalle no este abierto, puede cambiar las condiciones de la consulta para poder ver el que se requiere.

Deberá seleccionar el conteo y hacer clic en [Ver Conteo]

![Conteos11](conteos11.png)

Si el conteo está abierto y el nivel del usuario tiene acceso, podrá ver las diferencias previo al cierre del conteo. Sólo deberá hacer clic en [Ver diferencias]

![Conteos12](conteos12.png)

Se podrá ver:

* La cantidad contada en el inventario
* Las entradas realizadas mientras esta abierto el conteo
* Las salidas realizadas durante el conteo
* El conteo final como resultado de: cantidad contada + entradas - salidas
* La existencia actual
* Diferencias entre el conteo y la existencia actual

La consulta se puede imprimir o enviar a Excel.