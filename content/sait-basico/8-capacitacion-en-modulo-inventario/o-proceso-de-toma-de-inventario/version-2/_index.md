+++
title = "Proceso de Toma de Inventario Version 2.0"
description = ""
weight = 2
+++

Este proceso se recomienda realizar cuando se hará el inventario segmentado de sus productos, con la finalidad de seguir vendiendo y al mismo tiempo, realizar la toma de inventario a puertas a abiertas.

## Objetivo

* Permitir vender y comprar mientras se realiza el inventario.
* Facilitar la toma de inventarios paralelos.
* Contar el artículo en cada ubicación.
* Al finalizar el conteo, permitir ver las diferencias antes de realizar los ajustes.

El proceso consta de los siguentes pasos:

{{% children  %}}

Aplicacion movil:

* [SAIT Inventario](/otros-temas/aplicacion-inventario/)
