﻿+++
title = "Proceso de Toma de Inventario Version 1.0"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=pToAXFbPwaA&t=3423s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. O. Proceso de toma de inventario.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El proceso de inventario es uno de los procesos más importantes en el sistema, ya que permite comprar las existencias reales o física contra lo que está en el sistema y con esto, determinar si hay faltantes o sobrantes de artículos.

El proceso de toma de inventario puede resumirse en los siguientes pasos.

1. Contar físicamente los artículos.

2. Capturar las cantidades contadas o transferirlas desde la lectora.

3. Comprar existencias físicas vs las existencias del sistema.

4. Investigar diferencias para corregir conteos erróneos o capturar compras.

5. Guardar archivo de la consulta (para tener un respaldo de las diferencias surgidas en el inventario de esa fecha).

6. Realizar el ajuste para que el sistema cuente con las existencias reales.

<h2 class="text-primary">Temario</h2>

* [Recomendaciones relacionadas con el proceso de inventario](#recomendaciones)
* [Frecuencia y forma de hacer inventario](#frecuencia-y-forma-de-hacer-inventario)
* [Descontar el valor de las mercancías perdidas](#descontar-el-valor-de-las-mercancias-perdidas)
* [Motivos por diferencias entre existencia real y sistema](#motivos-por-diferencias-entre-real-y-sistema)
* [Formas de contar](#formas-de-contar)
* [Ubicaciones](#ubicacion)
* [Documento de toma de inventario](#documento-de-toma-de-inventario)
* [**PROCESO DE TOMA DE INVENTARIO**] (#proceso-de-toma-de-inventario)	
	* [Revisar posibles inconsistencias en el kárdex de los artículos](#revisar-posibles-inconsistencias-en-el-kardex-de-los-articulos)	
	* [Borrar documentos de toma de inventario anteriores](#borrar-documentos-de-toma-de-inventario-anteriores)	
	* [Capturar manualmente un documento de toma de inventario](#capturar-manualmente-un-documento-de-toma-de-inventario)
	* [Modificar un documento de toma de inventario](#modificar-un-documento-de-toma-de-inventario)		
	* [Capturar documento de toma de inventario usando una lectora](#capturar-documento-de-toma-de-inventario-usando-una-lectora)	
	* [Conocer diferencia entre inventario real vs inventario del sistema](#conocer-diferencia-entre-inventario-real-vs-inventario-del-sistema)	
	* [Investigar diferencias](#investigar-diferencias)	
	* [Ajustar existencias según conteo](#ajustar-existencias-segun-conteo)	
	* [Entregar archivo de inventario a contabilidad](#entregar-archivo-de-inventario-a-contabilidad)	
* [Otros procesos relacionados](#otros-procesos-relacionados)
* [Inventario manual (Sin lectora)](#inventario-manual-sin-lectora)
* [Inventario parcial (una línea o una familia de artículos)](#inventario-parcial)
* [Generar un documento con todos los artículos de una familia](#generar-un-documento-con-todos-los-articulos-de-una-familia)
* [Lectora UNITECH HTC630](#lectora-unitech-htc630)	
	* [Para iniciar el programa en la lectora](#para-iniciar-el-programa-en-la-lectora)
	* [Opciones del programa SAIT UNITECH](#opciones-del-programa-sait-unitech)
	* [Para enviar los artículos a la lectora](#para-enviar-los-articulos-a-la-lectora)
	* [Para reinicar la terminal sin borrar la información](#para-reiniciar-la-terminal-sin-borrar-la-informacion)
	* [Para borrar todo de la terminal y volverla a su estado tal y como se compró](#para-borrar-todo-de-la-terminal-y-volverla-a-su-estado-tal-y-como-se-compro)
	* [Archivos necesarios para utilizar la lectora](#archivos-necesarios-para-utilizar-la-lectora)
	* [Cambiar la Velocidad de Transferencia](#cambiar-la-velocidad-de-transferencia)
	* [Mostrar Descripción del Artícuo](#mostrar-descripcion-del-articulo)
    * [Enlazar Lectora con SAIT](#enlazar-lectora-con-sait)


## RECOMENDACIONES

El uso de lectores de código de barras tanto en el registro de compras como al momento de vender facilitan la identificación correcta de los artículos y minimizar los errores. 

También es muy recomendable usar una terminal portátil con lector de códigos de barra integrado para facilitar la toma del inventario. 

SAIT recomienda los productos Unitech, debido a su durabilidad tanto en ambientes normales como en ambientes industriales en donde se les da un uso rudo, como por su facilidad de manejo e interfase con la computadora, aunque pueden usarse lectoras de otras marcas. 


## Frecuencia y forma de hacer inventario


Se recomienda hacer inventario tan frecuentemente como la empresa pueda, ya sea una vez a la semana, una vez al mes, una vez cada trimestre o al menos una vez al año. 

El proceso de toma de inventario se puede hacer: 

1. Para todos los artículos de la tienda 

2. Para cierto grupo de artículos. 

Si la tienda es muy grande se recomienda hacer toma de inventario para cierto grupo de artículos, por ejemplo: a) Los artículos de una línea b) Los artículos de una familia c) Los artículos de un proveedor 

Aquí es importante asegurarse de contar con una muy buena clasificación de artículos, ya que, si existen artículos no clasificados, estos no serán considerados en el inventario. 

## Descontar el valor de las mercancias perdidas

Es muy importante descontar al personal el valor de la mercancía perdida, ya que de otra forma, no le dará la importancia necesaria a la mercancía, ni a los procesos relacionados con su movimiento (registro de compras, ventas, etc.) y siempre se estarán cometiendo errores involuntarios por falta de cuidado, se recomienda descontar en partes iguales a todo el personal con acceso al almacén o que haya laborado en la tienda a partir del ultimo inventario. 


## Motivos por diferencias entre real y sistema

Algunas de las causas por las cuales puede haber diferencias entre el sistema y las existencias reales son: 

1. Error en la captura de compras o en las facturas de venta, se usó otra clave de artículo. 

2. No se contó toda la mercancía. 

3. La mercancía se encuentra en otro lugar no considerado 

4. Falta meter documentos (compras o ventas) 

5. Merma que no se alimentó al sistema 

6. Hurto o Robo de mercancía.

## Formas de contar

El conteo se puede hacer de muchas formas, algunas de ellas son: 

* Con lectora inalámbrica conectada al sistema

* Con lectora portátil descargando en la PC 

* Con reportes impresos, llenando la cantidad (buscando el producto en el listado) 

* En hojas blancas, llenando la clave, descripción, cantidad de cada producto 

La más recomendada es la primera ya que el uso de lectoras inalámbricas ya que es llevar la información de tu sistema en la palma de tu mano y estar alimentando el conteo directamente en el sistema. 

La segunda opción tiene el inconveniente de tener que ir a descargar la lectora portátil en la computadora personal. 

Las 2 últimas opciones son manuales y no se recomiendan mucho. 

## Ubicacion

Cada estante de nuestro almacén lo identificaremos con una clave de ubicación. Por ejemplo: A1, A2, A3 .. A999 a todos los estantes de la primera fila de nuestro almacén. B1, B2, B3, a todos los estantes de la segunda fila y así sucesivamente. 

## Documento de toma de inventario

Llamaremos documento de toma de inventario, a la relación de artículos existentes en un estante o ubicación. De esta forma el documento de toma de inventario del estante A1 contendrá todos los artículos existentes físicamente en el estante A1. Los datos que incluiremos en el documento de toma de inventario son: clave, descripción y cantidad de cada artículo.

Si un artículo se encuentra en 2 estantes distintos, entonces el artículo aparecerá en ambos documentos de toma de inventario. Recuerda, se hará un documento de toma de inventario por cada estante, para facilitar el trabajo. 


## Proceso de toma de inventario


Los pasos para realizar el conteo y verificación de inventario son: 

1. Asegurase que el sistema no presente inconsistencias en los kardex de los artículos 

2. Borrar documentos de toma de inventario, del proceso anterior. 

3. Capturar documentos de toma de inventario 

4. Conocer diferencia entre inventario real contra inventario del sistema 

5. Investigar las diferencias (error de conteo, faltan compras o ventas, etc.)

6. Guardar el archivo de diferencias 

7. Ajustar existencias según conteo 

8. Entregar archivos de diferencias a contabilidad 


## REVISAR POSIBLES INCONSISTENCIAS EN EL KARDEX DE LOS ARTICULOS


Aun y cuando el sistema es 100% seguro en cuanto a la forma de procesar los movimientos del inventario, algún error en la operación del sistema de red o en el funcionamiento del disco duro, pueden causar que algunos movimientos no hayan sido reflejados en el kardex del sistema. 

Para asegurarse que todos los movimientos al inventario han sido reflejados en las existencias de los artículos: 

1. En el menú vaya a Inventario / Verificar Existencias 

2. Aparece la ventana de “Verificar Existencias” 

3. Seleccione la opción de: “Existencias no concuerdan con movimientos” 

4. Haga clic en **[Detectar]**

5. El sistema empieza un proceso que puede ser tardado en donde compara el kardex de todos los artículos con su existencia actual 

6. En caso de presentarse diferencias entre el kardex y la existencia, mostrara los artículos con inconsistencias y dar clic en **[Detectar]**.

Adicionalmente, si utiliza la opción de varios almacenes o el enlace de sucursales por Internet, deberá realizar el mismo proceso anterior, pero seleccionando la segunda opción “Existencias de almacén, no concuerdan con movimiento” 

## BORRAR DOCUMENTOS DE TOMA DE INVENTARIO ANTERIORES


Antes de empezar a capturar las existencias actuales es necesario borrar los documentos de toma de inventario del inventario anterior, realizando lo siguiente: 

1. En el menú ir a: Inventario / Proceso de Toma de Inventario / Documentos de toma de inventario

2. Aparece la ventana de Documentos de toma de inventario 

3. Haga clic en **[Borrar todos]**

4. Aparece la ventana de “Borrar documentos de toma de inventario” 


![IMG](1.png)

5. Haga clic **[Borrar documentos]**.

6. El sistema confirma que desea borrar todos los documentos de toma de inventario 

7. Haga clic en **[Sí]**.

## CAPTURAR MANUALMENTE UN DOCUMENTO DE TOMA DE INVENTARIO


1. En el menú ir a: Inventario / Proceso de Toma de Inventario / Documentos de toma de inventario

2. Aparece la ventana de “Documentos de toma de inventario” 

3. Dar clic en **[Agregar documento]**.

4. Aparece la ventana de: “Agregar documento de toma de inventario”

![IMG](2.png)


5. En el campo de Número de documento, escriba el nombre del estante (ubicación) que va a capturar. 

6. Escriba la clave y la cantidad de los artículos existentes en el estante o ubicación.

7. Si es necesario utilice la tecla F2 en la columna de clave, para localizar el artículo. 

8. Al terminar con todos los artículos del estante, presione F8 o haga clic en <Grabar>

## MODIFICAR UN DOCUMENTO DE TOMA DE INVENTARIO


1. En el menú ir a: Inventario / Proceso de Toma de Inventario / Documentos de toma de inventario

2. Aparece la ventana de “Documentos de toma de inventario” 

3. Dar doble clic en el documento a modificar. 

4. Aparece la ventana de: “Modificar documento de toma de inventario” 

5. Haga las modificaciones necesarias.

![IMG](3.png)

6. Si necesita borrar un renglón, borre la clave y la cantidad 

7. Al terminar con los cambios, presione F8 o haga clic en **[Grabar]**.


## ELIMINAR UN DOCUMENTO DE TOMA DE INVENTARIO


1. En el menú ir a: Inventario / Proceso de Toma de Inventario / Documentos de toma de inventario 

2. Aparece la ventana de “Documentos de toma de inventario” 

3. Dar doble clic en el documento a modificar. 

4. Aparece la ventana de: “Modificar documento de toma de inventario” 

5. Dar clic en **[Borrar Documento]**


![IMG](4.png)

6. El sistema confirma la eliminación del documento 

7. Dar clic en **[Sí]**

## CAPTURAR DOCUMENTO DE TOMA DE INVENTARIO USANDO UNA LECTORA


1. En el menú ir a: Inventario / Proceso de Toma de Inventario / Documentos de toma de inventario

2. Aparece la ventana de “Documentos de toma de inventario”

3. Dar clic en **[Agregar documento]**.

4. Aparece la ventana de: “Agregar documento de toma de inventario” 

5. Escriba el número de estante o número de documento de toma de inventario 

6. Dar clic en el icono de la terminal portátil

7. Aparecerá el asistente para descargar la lectora portátil 

![IMG](5.png)


8. Haga clic en **[Siguiente]**.

9.Aparece la ventana para descargar los datos de la terminal portátil 


10. Descargue la terminal portátil en esa ventana, los datos empezaran a aparecer. (Cada lectora portátil se descarga en forma distinta, revise su manual) 

11. Al terminar de recibir los datos, haga clic en <Siguiente> 

12. El sistema pregunta por el formato en que vienen los datos, seleccione el formato de su lectora. El más común es: codigodebarras,cantidad. 

13. Haga clic en **[Siguiente]**.

14. Se presenta el último paso (la validación de las claves capturadas) el sistema muestra todas las claves descargadas, en caso de que algún código de barras, no se encuentre registrado en el sistema, aparecerá en color rojo. 

![IMG](6.png)


15. Si aparecieron artículos inválidos: 

* Debe localizar físicamente TODOS los artículos y llevarlos a la computadora. (Para facilitar la localización encuentre los artículos cercanos a los que aparece en rojo) 

* Haga clic en **[Cancelar]** para ir a registrar los códigos de barra que faltan. 

* Vaya al catálogo de artículos o a la opción de Códigos Múltiples, para registrar los códigos de barra. 

* Vuelva a realizar el proceso desde el paso 1 

16. Si no aparecieron artículos inválidos, dar clic en **[Finalizar]**.

17. Los artículos aparecerán en la ventana de: “Agregar documento de toma de inventario” 

18. Haga clic en **[Grabar]**.

## CONOCER DIFERENCIA ENTRE INVENTARIO REAL VS INVENTARIO DEL SISTEMA


Después de que se han alimentado TODOS los documentos de toma de inventario, correspondientes a TODOS los estantes de la empresa, se procede a conocer las diferencias entre el inventario real (el que fue contado) y el inventario que tiene el sistema, para esto: 

1. En el menú ir a: Inventario / Proceso de toma de inventario / Ajustar existencias 

2. Aparecerá la ventana de “Ajuste de Existencias” 

![IMG](7.png)


4. Dar clic en **[Cargar Artículos]**.

5. Se presenta la ventana de “Cargar artículos para ajustes” 

6. Si se contaron todos los artículos de la tienda seleccionar la opción: Cargar todos los artículos 

7. Si se hizo un inventario parcial (solo los artículos de una familia) seleccionar la opción: Cargar solamente artículos contados 

8. Seleccionar la opción: Cargar existencia actual 

9. Dar clic en **[Cargar Artículos]**.

10. El sistema regresa a la ventana anterior mostrando la información del inventario: 

![IMG](8.png)


En esta ventana usted puede conocer las diferencias entre el inventario real o físico (columna # 4) que representa la suma de todos los documentos de toma de inventario alimentados en los pasos anteriores, contra la existencia o inventario del sistema **(columna #3)**.

Puede restringir los artículos a mostrar: 

1. Los artículos que hacen falta, es decir que la existencia real es mayor que la existencia del sistema 

2. Los artículos que sobran, es decir que la existencia del sistema es mayor que la existencia real 

3. Los artículos con existencia correcta, es decir que la existencia real y la del sistema concuerdan 

## INVESTIGAR DIFERENCIAS


Para investigar las diferencias se recomienda ordenar los artículos de acuerdo con: Diferencia de Valuación, de esta forma los artículos encontrados al inicio y al final, representan las diferencias más significativas y seguramente se deben a errores de conteo o a que no se haya capturado una compra. 

Puede revisar el kardex de los artículos con mayores diferencias para recordar los movimientos que ha tenido y ver si no se olvidó capturar o cancelar algún documento. 

Guardar archivo de diferencias

La ventana, permite mandar la información a Excel y de ahí imprimirla o guardarlo en un archivo para futuras referencias.

Se recomienda grabar 4 archivos: 

1. Un archivo con todos los artículos 

2. Un archivo con los artículos que hacen falta 

3. Un archivo con los artículos que sobraron 

4. Un archivo con los artículos con existencia correcta 

Para guardar el archivo: 

1. Active las casillas necesarias en la sección de Ver.

2. Haga clic en **[Excel]**  

3. Espere a que se presenten los datos en Excel 

4. Grabe el archivo en su disco duro. 

5. En el nombre del archivo a guardar incluya la fecha del inventario, por ejemplo: Inventario 30junio2018, Inventario 30julio2019

## AJUSTAR EXISTENCIAS SEGUN CONTEO


Después de haber investigado diferencias y GRABADO EL ARCHIVO DE DIFERENCIAS EN EXCEL proceda a ajustar el inventario para que refleje las existencias reales. 

IMPORTANTE una vez realizado el ajuste al inventario será imposible conocer las diferencias. Por eso es importante que antes de ajustar, grabe el archivo de diferencias. 

Damos clic en **[Ajustar Existencias]**

![IMG](9.png)


El sistema preguntará varias veces para estar seguro que desea realizar los ajustes en el inventario. 

Si desactivó algunas de las casillas de ver artículos que faltan, sobran o con existencia correcta, asegurase que estén activadas. 

El proceso de ajustar existencias, lo que hace es agregar un nuevo movimiento al kardex del artículo que presente una diferencia: 

* Los artículos con existencia correcta no son movidos. 

* Los artículos con existencia real mayor que la del sistema, se les agrega una entrada por ajuste 

* Los artículos con existencia real menor que la del sistema, se les agrega una salida por ajuste.

Hay posibilidades de cancelar todos los ajustes realizados, pero se trata de una operación que requiere ser realizada por un asesor especializado. Básicamente lo que hay que hacer es: 

1. Eliminar registros agregados en la tabla de: minv y entradas 

2. Volver a ajustar existencias de acuerdo con movimientos 

3. Ejecutar el proceso de reprocesar movimientos al inventario para arreglar capas que fueron afectadas por las salidas por ajuste 

 
## ENTREGAR ARCHIVO DE INVENTARIO A CONTABILIDAD


Los archivos generados en Excel del proceso de inventario entregarlos a contabilidad, ya que ellos lo utilizaran para ajustar sus cuentas contables. 

## OTROS PROCESOS RELACIONADOS

Otros procesos relacionados con la toma de inventario son: 

* Llenar campo de ubicación de los artículos 
* Inventario manual sin usar lectora portátil 
* Inventario parcial (una línea o familia de artículos) 

Llenar campo de Ubicación de los artículos

El sistema maneja un campo de Ubicación para cada artículo que sirve para indicar en que estante se encuentra ubicado. 

Después de haber terminado el inventario y en caso de que haya usado la sugerencia dada al inicio de hacer un documento de toma de inventario por cada estante, entonces puede indicarle al sistema que utilice el número de documento para llenar el campo de Ubicación de los artículos.

1. En el menú ir a Inventario / Proceso de toma de inventario / Llenar ubicación 

2. Aparece la ventana de “Autollenar ubicación” 

![IMG](10.png)

3. Si hizo inventario de toda la tienda haga clic en **[Limpiar UBICACIÓN de todos los artículos]**

4. Si hizo inventario de un grupo de artículos haga clic en **[Limpiar UBICACIÓN de los artículos en toma de inventario]**

5. El sistema confirma que la ubicación será limpiada 

6. Dar clic en **[Sí]**

7. El sistema indica que el proceso fue terminado 

8. Volver a ir a Inventario / Proceso de toma de inventario / Llenar ubicación 

9. Dar clic en **[Llenar UBICACIÓN con folio de documento de toma de inventario]**

10. El sistema confirma que la ubicación será llenada 

11. Dar clic en **[Sí]**

12. El sistema indica que el proceso fue terminado 


En caso de que un artículo se encuentre en varios estantes, la ubicación será llenada con todos los estantes en donde se encuentra, separados por coma. 

Si usa el sistema con un solo almacén, puede consultar las ubicaciones en Inventario / Catalogo de artículos en la pestaña de Clasificación. 

Si usa el sistema con varios almacenes, puede consultar la ubicación en Inventario / Existencias en Sucursales.

## INVENTARIO MANUAL SIN LECTORA


Si no cuenta con lectora portátil el proceso de inventario se dificulta y debe cuidar mucho que los productos sean identificados correctamente. Si lo desea puede imprimir reportes con los artículos para que el personal escriba a un lado la existencia real. 

Es conveniente imprimir estos reportes separados por línea o familia, para facilitar la localización de los productos. 


Para imprimir un reporte para toda de inventario 

1. En el menú vaya a Inventario / Reporte de Artículos / Inventario Físico 

![IMG](11.png)


2. Seleccione el reporte: “Auxiliar en toma de inventario” 

3. Seleccione la familia de artículos que desea imprimir 

4. En orden seleccione: Descripción 
 
5. Haga clic en **[Imprimir]**

6. Si desea imprimirlo desde Excel, haga clic en **[Hoja]**

7. Después haga clic en **[Excel]**

8. Una vez que abra Excel mándelo a impresora.

## INVENTARIO PARCIAL 


Si su empresa cuenta con muchos artículos (farmacias, ferreterías, refaccionarías, papelerías) lleva mucho tiempo hacer un inventario de todos los artículos, por eso SAIT cuenta con la opción de hacer inventario de un grupo de artículos. 

Puede hacer el inventario parcial de: 

* Los artículos de una familia 
* Los artículos de una línea 
* Los artículos asociados a un proveedor 

Los siguientes ejemplos se darán para cuando se hace el inventario de los artículos pertenecientes a una familia. 

El proceso es muy similar a todo lo explicado anteriormente, con las siguientes diferencias: 

1. Se debe generar un documento de toma de inventario con existencia cero, que contenga a todos los artículos de la familia de la cual se hará inventario. 

2. Al cargar los artículos en la ventana de “Ajustar Existencias” seleccionar la opción “Cargar solamente artículos contados” 

3. Al llenar la ubicación, seleccionar el botón **[Limpiar ubicación de artículos en toma de inventario]** 

<h4 class="text-danger">Al hacer el ajuste seleccione “Cargar solamente artículos contados”, ya que, si selecciona “Cargar todos los artículos” al momento de hacer el ajuste, los artículos pertenecientes a otras familias se irán a cero, ya que no fueron contados. </h4>

![IMG](contados.png)

Es muy importante entender los 3 pasos anteriores para no cometer el ERROR de mandar a cero las existencias todos los artículos no contados.

## GENERAR UN DOCUMENTO CON TODOS LOS ARTICULOS DE UNA FAMILIA


La clave de hacer un inventario parcial es generar un documento de toma de inventario con todos los artículos pertenecientes a una familia. 
Para generar ese documento: 
1. En el menú ir a Inventario / Reporte de Artículos / Inventario físico 

![IMG](12.png)

2. Seleccionar el reporte: Documento de toma de inventario en cero

3. En el campo de familia, escriba la familia de la que hará el inventario 

4. Dar clic en **[Hoja]**.

5. Se mostrará la siguiente consulta


![IMG](13.png)


6. Mandar la información a Excel dando clic en el icono de Excel en la barra de consultas.

7. Una vez estando en Excel la información seleccione la columna A (en donde están los datos) y haga clic en Edición / Copiar 

8. Después regrese a SAIT y siga los pasos de agregar un documento de toma de inventario usando una lectora, con la diferencia que, en lugar de descargar la lectora portátil, presione la tecla CTRL+V para pegar. 

9. Continúe con los pasos ahí indicados. 

10. Esto generara un documento de toma de inventario con todos los artículos de la familia deseada, con existencia cero. 

11.Ahora realice la toma de inventario de todos los artículos de la familia en forma normal. 


## LECTORA UNITECH HTC630

1. Asegurarse que exista el directorio: C:\SAIT\HT630

2. Si no existe deberá seguir los siguientes pasos:  

 * Crear carpeta llamada "HT630" en C:\SAIT\HT630 

 * Deberá descargar el siguiente archivo [para descargar de clic aquí](HT630.zip)  

 * Descomprimir su contenido en C:\SAIT\HT630 

 * Ejecutar los archivos desde ésta localización

3. Conectar la lectora con el cable proporcionado, al puerto serial de la computadora DB9 

4. APAGAR LA LECTORA 

5. En el menú ir a Inventario / Proceso de toma de inventario / Enviar artículos a lectora portátil 

![IMG](14.png)

6. Seleccionar el puerto y address de la lectora 

7. Address siempre es: A 

8. Puerto puede variar dependiendo de tu PC normalmente será COM1 

9. Dar clic en **[Enviar Programas]** 

10. Automáticamente se prenderá la lectora y se empezarán a enviar los programas. 

## PARA INICIAR EL PROGRAMA EN LA LECTORA


1. Encender la lectora 

2. Aparecerá la identificación del BIOS y la cantidad de memoria 

3. Mantenga presionado la tecla **[CMD]** durante 3 segundos 

4. Aparece el menú del sistema operativo 

5. Presione [1] RUN 

6. Con las teclas [↑] y [↑] seleccione el programa: MENU630.EXE 

7. Presione **[ENTER]** 

8. Aparecerá el menú de SAIT UNITECH 

## OPCIONES DEL PROGRAMA SAIT UNITECH

El programa SAIT Unitech que se carga en la lectora portátil HT630 le permite 3 opciones:

1. Inventario 1.0 Permite hacer la toma de inventario SIN VERIFICAR que los códigos de barra existan en el archivo de SAIT 

2. Inventario 2.0 Permite hacer la toma de inventario VERIFICANDO cada código de barra, exista en el archivo de SAIT, de no ser así, marcara un mensaje de error. Para usarlo es necesario enviar los artículos de SAIT a la lectora 

3. Compras. Permite registrar compras usando la lectora portátil y después descargándolas en SAIT.

Para usar las opciones de Inventario2 y Compras, es necesario enviar de SAIT a la lectora portátil los artículos. Dependiendo de la cantidad de artículos que su empresa tenga esto puede ser práctico o no. Adicionalmente puede enviar ciertos artículos no todos. Especificando una restricción. 

## PARA ENVIAR LOS ARTICULOS A LA LECTORA


1. Conectar la lectora con el cable al puerto serial de la computadora 

2. APAGAR LA LECTORA 

3. Vaya a Inventario / Proceso de toma de inventario / Enviar artículos a lectora 

4. Aparece la ventana de Enlace con Lectora 

5. Haga clic en <Enviar Artículos> 

6. Automáticamente se prendera la lectora y se empezaran a enviar los artículos 

## PARA REINICIAR LA TERMINAL SIN BORRAR LA INFORMACION

1. Apagar la terminal 

2. Presionar al mismo tiempo: [Power] [←] [Cmd] 

3. Aparece el menú de inicio 

4. Presionar [2] Warm Start 

## PARA BORRAR TODO DE LA TERMINAL Y VOLVERLA A SU ESTADO TAL Y COMO SE COMPRO

1. Apagar la terminal 

2. Presionar al mismo tiempo: [Power] [←] [Cmd] 

3. Aparece el menú de inicio 

4. Presionar [3] Cold Start 

5. Aparece la confirmación 

6. Presionar [1] 

## CAMBIAR LA VELOCIDAD DE TRANSFERENCIA

Si la pc es lenta debemos cambiar la velocidad de transferencia de datos.

1. En el menú de TOMAR INVENTARIO V3, seleccione la opción 5.

2. Aparecerá la Configuración 1/3.

3. Aparecerá la leyenda: Si la PC es lenta, puede esperar n milisegs antes de enviar el siguiente registro, escribir el tiempo de espera y presione ENT.
    
4. Aparecerá la Configuración 2/3, presione ENT hasta regresar al menú de Tomar Inventario V3. 

## MOSTRAR DESCRIPCION DEL ARTICULO

En esta opción nos permite mostrar la descripción del artículo al momento de estar realizando el inventario, lo cual es muy útil ya que nos permite identificar artículos que no están dados de alta en el sistema o artículos que no tienen asignado el código de barras.

1. En el menú de TOMAR INVENTARIO V3, seleccione la opción 5.

2. Aparecerá la Configuración 1/3, presione ENT hasta llegar a Configuración 3/3

3. Aparecerá la pregunta ¿Mostar descripción de los artículos?, seleccione la opción 1 y presione ENT. 

## ENLAZA LECTORA CON SAIT

Una vez esté programada la lectora lo siguiente es enlazar con SAIT, para ello SIN abrir SAIT deberá conectar la lectora a un puerto USB disponible e identificar con ayuda del Administrador de Disposotivos qué puerto COM nos dio Windows

![IMG](PUERTOCOM.jpg)

Después abriremos SAIT y nos dirijiremos a Utilerías / Configuración General del Sistema / Caja - Punto de Venta y en la sección de Puerto (COM), si el scaner es serial colocaremos el puerto COM que recién identificados, en este caso el 1, damos clic en Cerrar, cerramos por completo el sistema y entramos nuevamente y listo.

![IMG](SERIAL.jpg)