﻿+++
title = "Kárdex del Artículo"
description = ""
weight = 13
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=ZmMx1HGG2TU&t=0s&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=5" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. M. Kardex del Artículo.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT cuenta con la opción de Kárdex del Artículo en donde podemos visualizar todos los movimientos realizados con un artículo en específico. La ventana cuenta con distintos filtros como fecha, capas disponibles, salidas y entradas para facilitar la consulta, además muestra el total de movimientos, existencia del artículo y su divisa, dicha ventana es de suma importancia para supervisores inventaristas o gerentes ya que arroja todo el historial de movimientos que ha tenido el artículo.

Para realizar esta consulta de SAIT diríjase a:

<h4 class="text-primary">Inventario / Kárdex del Artículo</h4>

1.En el campo clave escriba la clave del articulo o presione **[F2]** para buscar 

2.Aparecerá la descripción del producto con cada uno de los movimientos que se han realizado: 


![IMG](1.png)


La consulta se puede imprimir en pantalla, ver la consulta en Hoja o enviar la información a Excel, de igual forma puede visualizar los costos por: Promedio, PEPS, PEPS DLS, Último Costo. 

Si desea realizar el cambio de costo o fecha de caducidad de alguna de las capas, selecciona la capa y oprime la tecla **[F6]**.

<h3 class="text-danger">IMPORTANTE</h3>

Si usted nota que la existencia descrita en la parte superior con la existencia que muestra en la parte inferior de la ventana, le recomendamos correr el proceso de Verificar Existencias para corregir estas diferencias, las cuales son ocasionada por haber eliminado algún movimiento del inventario, haber realizado algún ajuste de inventario o se presentaron problemas con su sistema.  

![IMG](2.png)
