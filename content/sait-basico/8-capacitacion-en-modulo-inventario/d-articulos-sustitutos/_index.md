﻿+++
title = "Artículos Sustitutos"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=yTZF27F1hSQ&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=5&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. D. Artículos Sustitutos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


SAIT cuenta con la opción de poder definir artículos sustitutos, lo cual es un apoyo al vendedor ya que le permitirá ofrecer artículos al cliente cuando no se tiene existencia del producto que el cliente busca.

Para definir artículos sustitutos diríjase a:

<h4 class="text-primary">Inventario / Artículos Sustitutos</h4>

1.Escriba la clave del artículo al cual le desean agregar sustitutos o presione **[F2]** para buscar

![IMG](1.png)


2.En la columna de clave escriba la clave de los artículos que son los sustitutos del producto principal, puede presionar **[F2]**  para buscar.

3.Al terminar de agregar los sustitutos haga clic en el botón **[Cerrar]**
Podrá consultar los artículos sustitutos al momento de vender en la ventana de Ventas / Registro de Ventas **(función solo disponible para Ventas, no para Caja)**

Para esto deberá colocar el cursor en la clave del artículo y presionar la tecla **[F3]** en donde se enlistarán los artículos sustitutos:


![IMG](2.png)