﻿+++
title = "Verificar Existencias"
description = ""
weight = 18
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=YkHf5MteY_s&index=9&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. S. Verificar existencias.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Uno de los puntos más importantes en cuanto a Inventario es el proceso de verificar existencias, la cual nos corrige problemas en la existencia de los artículos ya sean por causa de fallas de red, problemas con el disco duro, fallas en el sistema operativo. También afectan captura de documentos erróneos, movimientos o documentos eliminados de forma intencional o involuntariamente.

En esta ventana se cuenta con tres opciones las cuales son:

1.**Detectar artículos con problemas:** Ayuda a detectar y corregir problemas en la existencia de los artículos.

2.**Reprocesar movimientos del articulo:** Ayuda a corregir problemas en los costos, existencia y cantidad disponible en capas.

3.**Punto de venta:** Ayuda a corregir errores en el total de ventas de los artículos. Esta opción es solamente para las empresas que utilicen punto de venta.


<h2 class="text-primary">Detectar Artículos con problemas</h2>

Esta opción es muy recomendable realizarla antes de cada toma de inventario, ya que si hay problemas con la existencia los ajustes no se realizaran de forma correcta y las diferencias que nos muestre no serán las reales. Le recomendamos que de preferencia se realice este proceso en el servidor ya que puede tardar varios minutos dependiendo de la cantidad de artículos.

Para realizarlo siga con los pasos descritos a continuación:

Deberá dirigirse al menú de Inventario / Verificar existencias

1.Seleccione la pestaña Detectar artículos con problemas

2.Selecciona la opción según sea su caso:

* **Existencia no concuerda con movimientos:** <h4 class="text-danger">Solo se debe utilizar en las empresas que NO TIENEN almacenes o SAIT Distribuido, es decir solo tenemos un almacén general</h4>

* **Existencia almacén no concuerda con movimientos:** 
<h4 class="text-danger">Solo para las empresas que TIENEN almacenes o SAIT Distribuido</h4>

* **Existencia no concuerda con capas disponibles:** Entiéndase por capa a un documento de compra, el sistema evaluará las capas, las sumará y comparará con todos los movimientos del kardex vs la existencia que tenemos en el catálogo de artículos.

3.Seleccione la opción deseada y de clic en **[Detectar]**

![IMG](1.png)

4.Aparecerá la siguiente pregunta: Este proceso puede tardar algunos minutos dependiendo del volumen de movimientos al inventario almacenado. ¿Desea examinar los artículos?

De clic en el botón **[Si]**

![IMG](2.png)

Aparecerán los artículos con problemas, puede dar clic en el botón de copiar   y pegarlo en un archivo de texto para guardar los errores.

A continuación, deberemos dar arreglar las existencias, para ellos de clic en el botón **[Arreglar existencia]** para corregir las fallas.

Aparecerá la siguiente pregunta: ¿Seguro que desea arreglar la existencia en el almacén, para estos artículos?

De clic en el botón **[Si]**

![IMG](3.png)


Listo, la existencia ha sido corregida.

En este momento si damos clic de nuevo en detectas nos aparecerá el siguiente mensaje.


![IMG](4.png)


<h2 class="text-primary">Reprocesar Movimientos del Artículo</h2>

Esta opción reprocesa los movimientos al inventario del artículo, actualizando:

* Costos de las Salidas

* Existencia total del artículo

* Existencia de artículo en cada almacén (si aplica)

* Cantidad disponible en capas de entrada

Para realizar el proceso realice lo pasos siguientes:

1.En SAIT ir a Inventario / Verificar existencias

2.Seleccione la pestaña Reprocesar movimientos de artículo

3.Escriba la clave del artículo

4.De clic en el botón **[Reprocesar movimientos del artículo]**

![IMG](5.png)

5.Aparecerá la pregunta: ¿Seguro que desea reprocesar los movimientos al inventario del artículo?

![IMG](6.png)


6.De clic en el botón **[Si]**

7.Al terminar aparecerá el mensaje: Capas han sido reprocesadas. Costos, existencias disponibles y existencias en almacén, han sido actualizados.

8.De clic en **[Aceptar]**.

9.Listo

![IMG](7.png)

<h2 class="text-primary">Punto de Venta</h2>

El proceso de verificar existencias es exclusivamente para las empresas que utilizan el módulo de punto de venta, ya que esta opción corrige errores en el campo Arts.VENTACORTE, es decir el total de ventas realizadas del artículo en los cortes abiertos. Para corregirlo siga las instrucciones:

1.En SAIT ir a Inventario / Verificar existencias

2.Seleccione la pestaña Punto de Venta

![IMG](8.png)


3.De clic en el botón **[Verificar]**

4.Aparecerá la leyenda HAY ERRORES, imprima el reporte y llame a su asesor.


![IMG](9.png)


5.De clic en el botón **[Aceptar]**, aparecerán los artículos con errores.

![IMG](10.png)


6.Si lo desea puede enviar a imprimir la lista, dando clic en el botón del con el ícono de la impresora.

7.De clic en el botón **[Arreglar]**, para corregir los errores.

8.Listo.

