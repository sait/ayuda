﻿+++
title = "Tipos de Movimientos en Inventario"
description = ""
weight = 12
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=B4Z1_nGQKF8&t=0s&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=4" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. L. Definir tipos de movimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

SAIT cuenta con un catálogo de tipos de movimiento de inventario el cual nos sirve para realizar salidas y entradas al Inventario en donde podemos identificar cada movimiento de una manera sencilla y rápida.

* En el campo Siguiente Folio, en caso de utilizar SAIT Distribuido o Enlace de Sucursales, debe agregar serie según corresponda a la sucursal por ejemplo folio A1. 
De lo contrario se debe establecer, como folio inicial, 1.

* Importante: Los conceptos relacionados con los módulos de compras y de ventas (C, F, DC, DV) se deben utilizar en el menú correspondientes a cada módulo.

* En los conceptos de cancelación los cuales son del C1 al C9, CC, CD, CF, CR, solo debe tener el acceso el grupo que puede cancelar los movimientos.

* En versiones anteriores los grupos de usuario estaban definidos por las claves 1,2,3. Si es su caso en la columna de Grupo reemplace ALM,GCOMP,CONT,SUPER por 1,2,3.


![IMG](1.PNG)

<h2 class="text-primary">MENÚ DE OPCIONES</h2>

* [Agregar un Tipo de Movimiento](#agregar-un-tipo-de-movimiento)
* [Modificar un Tipo de Movimiento](#modificar-un-tipo-de-movimiento)
* [Eliminar un Tipo de Movimiento](#eliminar-un-tipo-de-movimiento)
* [Agregar Formato de Impresión](#agregar-formato-de-impresion)


## AGREGAR UN TIPO DE MOVIMIENTO


SAIT permite definir más tipos de movimientos al inventario según las necesidades de la empresa.

<h4 class="text-danger">Es importante considerar que al agregar un nuevo concepto debemos generar también el concepto de cancelación, el cual debe ser un movimiento contrario al que está definiendo.</h4>

Para agregar un concepto siga las siguientes instrucciones:

1.Entrar a SAIT e ir a Inventario / Tipos de Movimientos /  Clic en **[Agregar]**


![IMG](2.png)

2.Deberá llenar los siguientes campos:

* Clave del nuevo concepto, dos dígitos de longitud.
* Descripción del concepto.
* Tipo de movimiento al inventario, seleccione salida o entrada con la barra espaciadora de su teclado.
* En el campo de capturar seleccione la barra espaciadora Cliente, Proveedor o Ninguno.
* Ingrese el siguiente folio inicial para este nuevo movimiento.
* Indique los grupos de usuario o los niveles que tendrán acceso a dicho movimiento.
* En campo de Concepto a usar para cancelaciones, escriba la clave del concepto de cancelación. Clic en **[Grabar]**.
![IMG](3.png)


3.Definir concepto de cancelación del nuevo concepto que recién creamos.

Este es un proceso importante que se debe considerar al momento de crear un tipo de movimiento al inventario, el crear también un concepto contrario al movimiento que definimos.

En este caso, se creó el concepto USO INTERNO el cuál es una salida, por lo que para su cancelación es necesario crear un tipo de movimiento de entrada. 

Para agregar el concepto de cancelación siga los mismos pasos solo omita el paso de establecer el folio inicial tal y como se muestra en la siguiente imagen.

![IMG](4.png)

## MODIFICAR UN TIPO DE MOVIMIENTO

La opción de modificar nos permite corregir errores en la definición de los conceptos de inventario.

Para modificar un concepto siga las siguientes instrucciones:

1.Ir a Inventario / Tipos de Movimientos / 

Seleccione el motivo que deseamos modificar y de clic en **[Modificar]**
![IMG](5.png)


2.Realice los cambios necesarios en los datos que usted desee.

3.Para guardar los cambios de clic en **[Modificar]**.


![IMG](6.png)

## ELIMINAR UN TIPO DE MOVIMIENTO


En SAIT puede eliminar movimientos al inventario, sin embargo lo más recomendable es no eliminarlos debido a que si ya se han realizado movimientos con ese concepto al consultar el historial de los artículos no sabremos cual fue el movimiento.

Para eliminar los movimientos siga con las instrucciones descritas a continuación:

1.Ir a Inventario / Tipos de Movimientos /  

Seleccione el motivo que deseamos eliminar y de clic en **[Eliminar]**


![IMG](7.png)


2.Clic en el botón Eliminar de la siguiente pantalla.

![IMG](8.png)

3.Confirmamos y damos clic en **[Si]** para eliminar el tipo de movimiento.


![IMG](9.png)

## AGREGAR FORMATO DE IMPRESION


Cuando se agrega un nuevo concepto también puede instalarle un formato para que se envíe a imprimir.

Para instalarle el formato realice los siguientes pasos:

1.Dirigirse al menú de Inventarios / Tipos de movimientos

2.Seleccione el tipo de movimiento en donde se va a instalar el formato

3.Haga clic en el botón **[Modificar]**

4.Haga clic en el botón **[Formatos]**

Aparecerá la siguiente ventana:


![IMG](10.png)


5.clic en el botón **[Nuevo]**

6.En el campo descripción, agregue la descripción del concepto del tipo de movimiento

7.En el campo de Grupos colocar TODOS

8.En el campo de Formato colocar RMovInv

9.Presione la tecla ESC de su teclado, para cerrar la ventana

10.Listo ya puede imprimir el movimiento al Inventario.





