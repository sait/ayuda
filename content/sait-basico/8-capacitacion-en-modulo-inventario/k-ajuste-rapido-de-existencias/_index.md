﻿+++
title = "Ajuste Rápido de Existencias"
description = ""
weight = 11
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=4q4t0jpApCI&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=2&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. K Ajuste Rápido de Existencias.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En SAIT se puede realizar un ajuste rápido y sencillo de existencias de un solo artículo, este proceso agregar en el Kardex un movimiento de: Entrada por Ajuste al Inventario o Salida por Ajuste al Inventario según sea el caso.

Para realizar este proceso diríjase a:

<h4 class="text-primary">Inventario / Ajuste Rápido de Existencias</h4>

* Ingresamos el código del artículo o si no conocemos la clave, damos clic en el signo de interrogación.
* Se mostrarán datos de Fecha del Ajuste y Existencia actual del artículo (antes del ajuste). 
* Capturar en el campo: "Existencia Física" la existencia con la que usted cuenta y desea ver reflejado dentro del sistema. 
* Hacer clic en el botón **[Ajustar]**
* Listo, nuestras existencias han sido ajustadas.


![IMG](1.png)

<h4 class="text-primary">Inventario / Kardex del Artículo</h4>

Ahora en el Kardex del artículo podemos visualizar un movimiento de Entrada por Ajuste al Inventario con clave **EA** y Folio A188 haciendo referencia al ajuste que acabamos de realizar, en caso de haber ingresado una cantidad menor en existencia física a la actual del artículo se generará un movimiento con clave **SA** haciendo referencia a una Salida por Ajuste al Inventario.

Un ajuste de inventario desde esta opción considerará los costos de las siguientes capas disponibles ya que no se puede hacer un ajuste ingresando un costo en específico.

<h4 class="text-danger">Importante</h4>

**No utilice esta opción si maneja Lotes de caducidad o Series, en estos casos deberá hacer una Entrada o Salida desde Inventario / Registrar Salida / Entrada según sea el caso, esto porque tanto como Lotes y Series no solo es corregir una existencia sino dar entrada o salida indicando los Lotes o Series afectados**

![IMG](2.png)



