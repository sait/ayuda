﻿+++
title = "Recibir correo Diariamente de Verificación de Existencias"
description = ""
weight = 19
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=udc83Ycc9Ek target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="recibir-correo-con-verificacion-de-existencias.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Configura esta función en tu aplicación SAIT para conocer los artículos con problemas en las existencias, este es uno de los puntos más importantes ya que nos dará la información necesaria para corregir la existencia de los artículos que tengan algún problema ya sea por causa de fallas de red, problemas con el disco duro o fallas en el sistema operativo. También afectan captura de documentos erróneos, movimientos o documentos eliminados de forma intencional o involuntariamente.

Para configurar esta opción diríjase a Utilerías / Configuración General del Sistema / Pestaña Inventario

Solo deberá palomear la opción de “Verificar diariamente existencias vs movimientos” y también colocar un correo al que deseemos llegue esta información, finalmente daremos clic en **Cerrar**

![IMG](verificar.png)


De esta manera diariamente si el sistema encuentra alguna inconsistencia llegará un correo similar al siguiente con la relación de artículos a corregir:

![IMG](correo.PNG)

Archivo TXT con diferencias

![IMG](diferencias.PNG)


