﻿+++
title = "Consulta Individual de Movimientos"
description = ""
weight = 14
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=xWF7OScksDw&index=3&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. N. Consulta individual de movimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


<h2 class="text-primary">CONSULTA INDIVIDUAL DE MOVMIENTOS</h2>

La consulta individual como su nombre lo indica nos permite consultar un movimiento de inventario de manera individual ya sea para su consulta/impresión o cancelación según sea el caso.

Para realizar dicha consulta dentro de SAIT diríjase a:

<h4 class="text-primary">Inventario / Consulta Individual de Movimientos</h4>

1.Seleccione el tipo de movimiento que desea imprimir 

2.Indique el folio del documento y presione **[Enter]**


![IMG](1.png)

3.Si así lo desea puede imprimir dicho documento, pero es importante mencionar que para poder re-imprimir un movimiento, el concepto debe tener asignado un formato de impresión. 


<h4 class="text-danger">En caso de que presione [Imprimir] y el sistema no le arroje ningún formato de impresión, le recomendamos verificar en Tipos de Movimientos, si se cuenta con un formato, de no ser así deberá agregar uno, para agregar un formato de impresión diríjase a: </h4>

<h4 class="text-primary">Inventarios / Tipos de movimientos</h4>

* Seleccione el tipo de movimiento en donde se va a instalar el formato
* Haga clic en el botón **[Modificar]**
* Haga clic en el botón **[Formatos]**
* Aparecerá la siguiente ventana:

![IMG](2.png)

* Clic en el botón **[Nuevo]**
* En el campo descripción, agregue la descripción del concepto del tipo de movimiento
* En el campo de Grupos colocar TODOS
* En el campo de Formato colocar **RMovInv**

![IMG](formato.PNG)

* Presione la tecla ESC de su teclado, para cerrar la ventana
* Listo ya puede imprimir el movimiento al Inventario.

<h2 class="text-primary">CANCELAR MOVIMIENTOS CONSULTANDO DE MANERA INDIVIDUAL</h2>


SAIT permite cancelar un documento de inventario en caso de algún error de captura o cualquier otro motivo presentado en su empresa, el proceso para dicho proceso es muy sencillo solo debe tomar en cuenta los siguientes puntos.

El usuario debe contar con acceso al movimiento de cancelación. Es recomendable que solamente el supervisor o gerente de almacén tengan acceso a los conceptos de cancelación. 

Puede seguir las instrucciones de Tipos de Movimientos / Modificar Tipos de Movimientos, para dar acceso a los niveles de cancelación. 

Los movimientos de factura, remisión, compra, devolución de compra y devolución de venta no se deben cancelar en esta opción ya que se deberán cancelar en el módulo correspondiente al movimiento. 

Para realizar una cancelación de un movimiento a inventario diríjase a:

<h4 class="text-primary">Inventario / Consulta Individual de Movimientos</h4>

1.Seleccione el tipo de movimiento que desea cancelar.

2.Escriba el folio del documento. 

3.Haga clic en el botón **[Cancelar]** 

![IMG](3.png)

4.Escriba el comentario o motivo de cancelación.

![IMG](4.png)

5.Seleccione el motivo de cancelación.

6.Confirme la cancelación dando clic en **[Si]**.

![IMG](5.png)

7.Listo, el movimiento ha sido cancelado y se mostrará de la siguiente manera en la consulta individual.


![IMG](6.png)