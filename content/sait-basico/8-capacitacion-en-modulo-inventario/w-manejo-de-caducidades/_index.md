+++
title = "Manejo de Lotes y Caducidades"
description = ""
weight = 22
+++

Dependiendo de su giro empresarial puede ser que requiera del manejo de lotes y caducidades de sus productos, si este es su caso, SAIT está preparado para cumplir con esta necesidad, para empezar a hacer uso de estas funciones será necesario siga las siguientes instrucciones. 

<h3 class="text-primary">1.Activar el uso de Lotes y Caducidades en la Configuración del sistema</h3>


1.1 Entre a Utilerías / Configuración General. 

1.2 En la pestaña de Compras, marcar la casilla de: Usar Lotes y Usar Caducidades

**NOTA: puede usar lotes o caducidades o las dos opciones si así lo requiere**

![IMG](1.png)

Si utiliza enlace de sucursales es necesario agregue estos eventos en el archivo sdconfig de cada sucursal 

| Evento      | Descripción                                                                          | Enviar  | Recibir |
|------------|-----------------------------------------------------------------|--------|---------|
| ActDisp     | Actualiza el campo minv.disp y entradas.disp                     |    X      |    X       |
| Entradas   | Enviar y recibir registros de la tabla Entradas                      |    X      |    X       |

1.3 Posteriormente en la pestaña de Ventas, marcar la casilla de: Al usar artículos de lotes, validar que lote tenga existencia.
 
![IMG](2.png)

1.4 Para guardar los cambios solo deberá dar clic en el botón de Cerrar

<h3 class="text-primary">2.Registrar artículos con lote y caducidades</h3>

Antes de registrar documentos de compra o venta, en donde intervienen lotes y caducidades, es necesario marcar los artículos para los cuales llevaremos el control de número de lote y fecha de caducidades según el caso.

**NOTA: Se recomienda que dé de alta nuevos artículos si empezará a controlar lotes y caducidades**

2.1	En el menú, acceder a la opción de Inventario / Catálogo de Artículos y Servicios

2.2	Agregar un artículo nuevo o modificar uno existente 

2.3	Marcar la casilla que dice: Usa lotes y Caducidades según sea el caso y dar clic en Grabar

![IMG](3.png)

<h3 class="text-primary">3.Capturar el lote al registrar compras</h3>

Al registrar compras, deberá capturar el número de lote para los artículos marcados para controlar lotes. 

3.1	En el menú ir a Compras / Registrar Compras 

3.2	Capturar los datos del proveedor y la factura 

3.3	En la cuadricula de artículos, seleccionar el artículo 

3.4	Capturar la columna de Lote y/o la Caducidad 

3.5	Llenar los demás datos de la compra 

![IMG](4.png)

3.6 El sistema no permitirá dar entrada a un artículo que usa lotes, sin que se haya capturado el campo de lote en la compra.

<h3 class="text-primary">4.Al vender seleccionar el lote a usar </h3>

4.1	En el menú ir a Ventas / Registrar ventas 

4.2	En la cuadricula de artículos, indicar la clave del artículo que usa control de lotes 

4.3	Capturar la cantidad y después presionar la tecla F3, para presentar la ventana de captura de lote 

![IMG](5.png)
 
4.4	Presionar F2 para activar la búsqueda de lote y caducidad

![IMG](6.png)

4.5	Seleccionar la capa o lote deseado y de clic en Continuar

![IMG](7.png)

4.6 Para finalizar el proceso de la venta de clic en F8 = Procesar 

![IMG](8.png)

El sistema no permitirá vender artículos que se controlan por lote, sin que se haya indicado el lote o capa de salida a utilizar

<h3 class="text-primary">5.Proceso adicional</h3>

En caso de que usted use sucursales o almacenes, en donde podrá seleccionar algunas de las siguientes opciones al usar el módulo de Inventario:

1.	**Seleccionar capas de inventario al realizar salidas:** Al procesar una salida al inventario, el sistema le va a solicitar que capture la capa de la cual va a salir cada artículo. 
2.	**Obligar control de lotes:** El sistema va a obligar al capturar el número de lote de los artículos que están entrando o saliendo del inventario. 
3.	**Obligar control de caducidad:** El sistema va a obligar al capturar la fecha de caducidad de los artículos que están entrando o saliendo del inventario. 

Para acceder a esta configuración entre al menú de Utilerías / Catálogo de Sucursales.

![IMG](9.png)
 
