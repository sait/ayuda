﻿+++
title = "Consultar Existencias en Almacenes y Sucursales"
description = ""
weight = 17
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=enemD2nFAWM&t=0s&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=7" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. Q. Existencias en sucursales y almacenes.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Por medio del Enlace de Sucursales o SAIT Distribuido usted puede consultar las existencias de sus diferentes ubicaciones (almacenes y sucursales) de manera sencilla y rápida. La consulta le proporcionará el número de sucursal, la existencial individual, así como la existencia global.

Para realizar dicha consulta dentro de SAIT diríjase a:

<h4 class="text-primary">Inventario / Existencias en Sucursales/Almacenes</h4>


Ingrese el código del artículo que desea conocer su existencia, si no conocemos la clave presionamos el botón de interrogación o también presionando la tecla F2 se abrirá la ventana de búsqueda de artículos como se muestra en la siguiente pantalla.

![IMG](1.png)


Para seleccionar el artículo damos clic en **[Enter]** sobre el renglón del registro.


![IMG](2.png)
