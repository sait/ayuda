+++
title="Incrementar precios por porcentaje"
description=""
weight = 3
+++

En caso de que desee aumentar un porcentaje a los  precios que tiene definidos por cliente SAIT cuenta con esta ventana la cual le cargará los precios especiales que usted haya agregado previamente, la ventana cueta con varios filtros de apoyo para poder consultar la información más ágilmente.

Para hacer uso de esta opción, solo debe dar clic en Consultar para que le aparezcan todos los precios, puede filtrar por un artículo en específico, por proveedor, por cliente, por línea, familia, catégoría o departamento.

En el campo de Porcentaje ingresaremos el porcentaje que deseamos aumentar a los artículos que aparecen en la cuadrícula

Para poder visualizar una vista previa de los precios daremos clic en Calcular

![IMG](por-porcentaje1.JPG)

Notaremos el incremento en la columna de Nuevo Precio 

![IMG](por-porcentaje2.png)

En caso de que queramos que los precios se redondeen a precios cerrados, deberá dar clic en el engrane inferior de la ventana, dar nuevamente clic en Calcular y verá en la columna de Nuevo Precio que las cantidades se cerrados
 
![IMG](por-porcentaje3.png)

Para finalizar daremos clic en Procesar y listo, si desea ver los cambios de clic en Consultar y listo.

![IMG](por-porcentaje4.JPG)
