+++
title="Migración"
description=""
weight = 4
+++

La versión 2021.21 requiere actualizar la base de datos cuando se utiliza esta opción por lo que es muy importante realizar un respaldo de la base de datos.

Si usted anteriormente ya contaba con Precios x Cliente definidos notará que la ventana no los muestra, para recuperarlos deberá de dar clic en **[Migración]**
 
![IMG](migracion.JPG)


Tal y como nos notificará la ventana, hay que considerar lo siguiente: 


1. Este proceso solo debe ejecutarse una **SOLO UNA VEZ, SOLO EN UNA SUCURSAL**. El repetir este proceso en alguna sucursal podría provocar duplicidad de registros. 

2. Debe asegurarse que las demás sucursales cuenten con la misma versión de SAIT. Este proceso va a generar eventos que se enviaran por SAITDIST/SAITSYNC, el no contar con la base de datos actualizada en todas las sucursales detendrá los servicios antes mencionados. 
 
![IMG](migracion-proceso.JPG)

![IMG](migracion-exitosa.JPG)

Una vez realizada la migración podremos ver los Precios x Cliente que teníamos definidos, la nueva ventana mostrará dos alternativas para definir Precios x Cliente.