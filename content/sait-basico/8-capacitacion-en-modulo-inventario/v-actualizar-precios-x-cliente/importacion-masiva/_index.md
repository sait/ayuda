+++
title="Importación masiva"
description=""
weight = 2
+++

Como puede observar la ventana cuenta con botón de importación masiva

![IMG](masiva.png)

Esta herramienta nos sirve para hacer el mismo proceso, pero apoyándonos con un archivo de Excel

La estructura del archivo de excel debe ser la siguiente para que la información de importe de manera correcta

CLAVE CLIENTE / CLAVE ARTÍCULO / UNIDAD / PRECIO / DIVISA

![IMG](masiva2.JPG)


**Validaciones al importar desde Excel:**

•	Si se trata de importar un registro de un número de cliente no existe el sistema mostrará la siguiente alerta: 

![IMG](masiva-no-existe-cliente.png)

•	Si se trata de importar una unidad que no existe, el sistema mostrará la siguiente alerta: 

![IMG](masiva-no-existe-unidad.png)

•	Sí se importa un precio especial para un cliente que ya se había importado anteriormente el sistema actualizará los datos

•	Si se trata de importar un registro de un número de artículo que no existe el sistema mostrará la siguiente alerta: 

![IMG](masiva-no-existe-articulo.png)

•	Si se borra una unidad adicional desde Inventario / Unidades Adicionales en Artículos el sistema borrará también la relación que haya en la ventana de Precios por Cliente. 

•	Si se trata de importar un artículo con una unidad adicional que no está previamente asignada al artículo mandará un mensaje de error: 

![IMG](masiva-no-corresponde-unidad.png)


Es decir, para el artículo ABRE solo se podrán importar de manera masiva las unidades PIEZA, CAJA y PAQ.

![IMG](masiva-equivalencia.JPG)

•	Al utilizar la opción de Utilerías / Modificar Claves / Clientes también afectará los precios especiales por cliente y unidades adicionales

•	Al utilizar la opción de Utilerías / Modificar Claves / Artículos también afectará los precios especiales por cliente y unidades adicionales


