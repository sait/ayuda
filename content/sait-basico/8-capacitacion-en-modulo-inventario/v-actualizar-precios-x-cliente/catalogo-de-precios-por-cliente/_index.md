+++
title="Catálogo de precios por cliente"
description=""
weight = 1
+++

En el siguiente documento se mostrará cómo hacer uso del catálogo de precios por cliente, para poder agregar, modificar o eliminar un precio especial.


<h3 class="text-primary">Agregar</h3>


1.Para agregar un nuevo precio especial por cliente deberá dar clic en Agregar

2.Agregue la clave o haga una búsqueda rápida presionando F1

3.El sistema mostrará la unidad principal del artículo (la cual se toma del catálogo de artículos y servicios)

4.Defina el Precio Especial, con barra espaciadora cambie de divisa y la unidad (este campo mostrará la unidad principal del artículo, así como las unidades adicionales que tenga definidas el artículo en Inventario / Unidades Adicionales)

5.Clic en Agregar y listo

![IMG](agregar-precio-especial.png)

<h3 class="text-primary">Modificar</h3>

1.Para modificar un nuevo precio especial por cliente deberá seleccionar el registro y dar clic en Modificar

2.Realice los cambios deseados y de clic en Modificar

![IMG](modificar-precio-especial.JPG)

<h3 class="text-primary">Eliminar</h3>

1.Para eliminar un nuevo precio especial por cliente deberá seleccionar el registro y dar clic en Eliminar

2.De clic en Eliminar nuevamente y listo

![IMG](eliminar-precio-especial.JPG)


