﻿+++
title = "Aceptar Transferencias/Traspasos de Mercancía"
description = ""
weight = 16
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=YzV-ZlLtDv8&t=0s&list=PLhfBtfV09Ai7TqusxvFpUL51CQFS2CfTt&index=6" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="8. P. Aceptar Transferencia de Mercancias.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Cuando se maneja el servicio de Enlace de Sucursales (SAIT Distribuido) es muy común que entre las sucursales pertenecientes se acepten y realicen transferencias de un almacén a otro.

Para realizar un traspaso, deberá dirigirse al menú de Inventario / Registrar Salidas, deberá seleccionar Salida por Traspaso y el número de sucursal a la que enviará la mercancía siempre anteponiendo una S de Salida como se muestra en la siguiente imagen

Ya que esté todo correcto, deberá dar clic en Procesar = F8 y listo

![IMG](ST.jpg)

A continuación, se explicará la manera de aceptar transferencias de mercancías provenientes de cualquier sucursal

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Inventario / Aceptar Transferencias de Mercancía</h4>

Donde nos aparecerá la siguiente ventana:

![IMG](1.png)

1.De ser necesario, para ubicar la transferencia, usted puede realizar un filtro por Rango de Fechas o Sucursal Origen.

2.Seleccionar la opción: **En Tránsito (ya que el estatus en tránsito nos mostrará los traspasos que no han sido aceptados)**.


3.Dar clic en el botón **[Consultar]** para mostrar las trasferencias pendientes de aceptar.

4.Se mostrará la información relacionada de las transferencias con datos como: Folio, Estatus, Saliendo de, Entrando a, con opción de enviar la información a Excel.

![IMG](2.png)



5.Una vez localizada la transferencia a aceptar, deberá seleccionar el registro y posteriormente hacer doble clic sobre el folio o presionar la tecla **[Enter]**.


6.Se abrirá una ventana donde nos mostrará el detalle del traspaso con todos los artículos pertenecientes a la transferencia:

7.Una vez revisados los productos de la transferencia físicamente, podrá aceptarla presionando el botón **[Aceptar Transferencia]**.

![IMG](3.png)

8.Posteriormente se mostrará en pantalla la ventana de Entradas al Inventario por el concepto de Entrada por Traspaso:

![IMG](4.png)


9.Para afectar el inventario y Procesar la Transferencia, deberá hacer clic en el botón **[Procesar F8]**.

10.A manera de confirmación el sistema preguntará si están correctos los datos, a lo cual usted seleccionará **[Si]**.

11.Listo, de esta manera ha aceptado exitosamente la transferencia proveniente de otra sucursal y ha afectado nuestro inventario correctamente.

