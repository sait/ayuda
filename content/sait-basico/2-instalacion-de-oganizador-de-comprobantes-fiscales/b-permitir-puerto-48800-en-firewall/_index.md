+++
title = "Permitir Puerto 48800 en Firewall para la comunicación entre estaciones"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=NLKAstStMy8&feature=youtu.be target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="permitir-puerto-48800-en-firewall.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>


### **Indicaciones**:

A partir de la versión de SAIT 2015, el sistema cuenta con un Organizador de Comprobantes Fiscales (abreviado OCF) que le ayuda a mantener resguardados y en orden todos los CFDIs que le envían sus proveedores así como los emitidos. 

El Organizador de Comprobantes Fiscales **se instala SOLAMENTE en el servidor**. **NO DEBERÁ INSTALARLO en las estaciones**, ya que las estaciones solo requieren hacer peticiones al servidor, para que esto pueda ser posible es necesario crear una regla en el Firewall o Cortafuegos de nuestro servidor.
Para crear esta regla dentro de Windows 7 siga las siguientes instrucciones.

1.Ir a Panel de Control / Sistema y seguridad / Firewall de Windows / Configuración Avanzada

![IMG](configavanzada.png)
 
2.Ir a Reglas de Entrada 

![IMG](reglasentrada.png)

3.Ir a Nueva Regla

![IMG](nuevaregla.png)
 
4.En tipo de reglas, seleccionar Puerto.
Clic en **[Siguiente]**.

![IMG](puertoocf.png)

5.Seleccionar Regla TCP e ingresar el puerto 48800.
Clic en **[Siguiente]**.


![IMG](48800.png)


6.En la siguiente ventana seleccionar Permitir la Conexión.
Clic en **[Siguiente]**.

![IMG](permitirconexion.png)

7.Seleccionar las 3 opciones que aparecen en pantalla.
Clic en **[Siguiente]**.

![IMG](opcionespalomear.png)
 
8.En la ventana de Nombre, ingresar el nombre de: SAITWS. 

El campo descripción es opcional.
Clic en **[Finalizar]**.

![IMG](saitws.png)
