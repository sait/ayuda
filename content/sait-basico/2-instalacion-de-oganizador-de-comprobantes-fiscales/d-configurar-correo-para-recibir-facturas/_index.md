+++
title = "Configurar Correo para Recibir Facturas"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=UJj1VDGssvA target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="configurar-correo-para-recibir-facturas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>


### **Indicaciones**:

Para configurar el Organizador de Comprobantes Fiscales y empezar a descargar archivos XML de nuestras facturas en SAIT ya sea para su consulta o ingreso de compras a partir de un CFDI, siga las siguientes instrucciones.

1.Entrar a SAIT e ir a Utilerías / Configuración General / Otros

Dar clic en **[Configurar Organizador de Comprobantes Fiscales OCF]**

![IMG](OCF.png)

2.Configurar correo

Configuración del correo de recepción:

{{%alert info%}} Por cuestiones de compatibilidad con el sistema se recomienda utilizar Gmail {{%/alert%}}

* Ingresar nuestro correo electrónico de Gmail
* Contraseña del correo electrónico
* Servidor: imap.gmail.com
* Puerto: 993 

Para corroborar que los datos están correctos de clic en **[Comprobar Acceso]**

Para finalizar y guardar los cambios de clic en **[Guardar Configuración]**

![IMG](comprobar.png)
