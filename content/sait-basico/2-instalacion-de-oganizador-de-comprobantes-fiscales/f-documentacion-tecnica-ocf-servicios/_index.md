+++
title = "Documentación Técnica de OCF (Servicios SAITBóveda3 y SAITMariaDB)"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=beKZEPcrJE8&feature=youtu.be target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="documentacion-tecnica-servicios-ocf.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>


### **Indicaciones**:

El Organizador de Comprobantes Fiscales (OCF) necesita de dos servicios para poder funcionar, en caso de que no estén ejecutándose nos mostrará alguno de estos mensajes al momento de querer realizar una factura.

![IMG](mensajeerror.png)

![IMG](mensajeerror2.png)

Para verificar los servicios mencionados siga las siguientes instrucciones.

* Antes que nada hay que revisar que el Servidor o el equipo que tiene instalado el Organizador de Comprobantes se encuentre encendido.

* Verificar si el los servicios se encuentran iniciados, para verificar este detalle hay que ir a los servicios para esto, nos dirigimos al buscador de Windows, tecleamos la palabra services.msc y damos clic en el ícono del engrane.

![IMG](services.msc.png)


* Buscamos los servicios de SAIT Bóveda3 y SAITMariaDB.

* Verificar que los dos servicios estén Iniciados, de lo contrario, detenemos el servicio y lo reiniciamos.

![IMG](servicios.png)



