+++
title = "Configurar Correo para Enviar Facturas y Recibos de Pagos por medio de OCF"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=Pnzh3sZEr48 target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="configurar-correo-para-enviar-facturas-y-recibos-de-pagos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>


### **Indicaciones**:

En SAIT existen dos modalidades en SAIT para enviar facturas vía correo a nuestros clientes, la forma tradicional en donde configuramos un correo en cada uno de los usuarios o por medio del Organizador de Comprobantes Fiscales (OCF).

Además que a partir del esquema de facturación 4.0 en donde ya es requerido emitir un complemento de pago, SAIT requiere del Organizador de Comprobantes para el envío de dichas facturas.

Para realizar esta configuración siga las siguientes instrucciones.

1.Entrar a SAIT e ir a Utilerías / Factura Electrónica / Configurar CFDI 
 
![IMG](configurarcfdi.png)

{{%alert info%}} Por cuestiones de compatibilidad con el sistema se recomienda utilizar Gmail {{%/alert%}}

Configuración del correo de envío:

* Servidor SMTP de salida: smtp.gmail.com
* Puerto: 465
* Ingresar correo Gmail.
* Habilitar las opciones de Usar conexión segura SSL y Usar conexión segura TLS.


![IMG](configurarenvio.png)


2.Probar envío

Escribimos una dirección de correo a donde queremos que llegue la prueba.
 
![IMG](probarenvio.png)

3.Dar clic en sí para confirmar el envío de la factura de prueba.

![IMG](confirmar.png)

4.Listo, al ser exitosa la prueba le aparecerá el siguiente mensaje.

![IMG](pruebaexitosa.png)

5.Como información adicional se puede verificar el correo de prueba en la bandeja de entrada.

![IMG](correoprueba.png)