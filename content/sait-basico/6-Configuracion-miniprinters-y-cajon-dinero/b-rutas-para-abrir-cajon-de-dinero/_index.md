﻿+++
title = "Rutas para abrir cajón de dinero"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=0-RODhM03J8&t=7s&list=PLhfBtfV09Ai4MQoaeJGNBhDgggvtGbQk9&index=3" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="6 B. Rutas para abrir cajón de dinero.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


Después de procesar un documento por ejemplo una nota de venta es necesario que el cajón de dinero se abra de manera automática al imprimir el documento. 

Esto con la finalidad de no tener la llave del cajón de dinero a la vista sino que el cajón se abra únicamente al procesar una venta.

Secuencias para abrir cajón de dinero dependiendo el modelo de la miniprinter:

Para impresora Epson: 

```
chr(27)+chr(112)+chr(0) 
Chr(27)+'p'+Chr(0)+Chr(100)+Chr(100) 
```

Para Impresora Star: 
```
Chr(7) 
```


En caso de que su impresora no esté listada, habría que hacerse pruebas con las secuencias.

La miniprinter siempre debe estar como recurso compartido.

Para agregar la expresión y abrir automáticamente nuestro cajón de dinero siga las siguientes instrucciones:

1.Entrar a SAIT e ir a Utilerías / Modificar Formatos / Nota de venta 

Clic en **[Editar]**

![IMG](1.png)

2.Se nos abrirá la pantalla de edición de formato.

Dar clic en el botón indicado en la siguiente imagen y dar clic en algún punto del formato.

![IMG](2.png)

3.Se nos abrirá la siguiente ventana en dónde ingresaremos la secuencia, en este caso tomaremos de ejemplo una impresora Epson

```
EnviarCadenaApuerto(chr(27)+chr(112)+chr(0),'\\Caja\Notas')
```

![IMG](3.JPG)

Dar clic en **[OK]**

![IMG](4.png)

4.Para finalizar guardamos cambios

Ir a File y dar clic en **[Save]**

![IMG](5.png)