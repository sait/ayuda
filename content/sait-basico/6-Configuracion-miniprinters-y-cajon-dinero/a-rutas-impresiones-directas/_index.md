﻿+++
title = "Rutas para Impresiones directas de documentos"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=EzcrFgdweqw&t=1s&list=PLhfBtfV09Ai4MQoaeJGNBhDgggvtGbQk9&index=2" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="6 A. Rutas para Impresiones directas.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Al momento de imprimir un documento ya sea una nota de venta, factura, cotización y demás nos aparecerá la siguiente pantalla en donde podemos elegir la opción que deseemos.

![IMG](1.png)

En caso de que no queramos que nos aparezca dicha pantalla al momento de procesar deberá configurar la ruta directa de impresión para que automáticamente al procesar un documento se mande a imprimir.

Para realizar esta configuración siga las siguientes instrucciones:

1.Entrar a SAIT e ir a Utilerías / Modificar Formatos / Nota de Venta

![IMG](2.png)

La ruta se conforma por el nombre de la máquina en donde está instalada físicamente la impresora y por el nombre del recurso compartido (no del nombre de la impresora)

![IMG](3.JPG)

Para conocer el nombre del equipo donde se encuentra la impresora así como conocer el nombre del recurso compartido siga las siguientes instrucciones.

1.Inicio de Windows, clic derecho en Equipo y dar clic en **[Propiedades]**

![IMG](4.png)

2.Tomar nota del nombre del equipo.

![IMG](5.png)

3.Para conocer el nombre del recurso compartido de la impresora

Diríjase al inicio de Windows, clic en Dispositivo e Impresoras.

Clic derecho sobre la impresora de la cual deseamos conocer el nombre del recurso compartido.

Seleccionamos Propiedades de Impresora.

![IMG](6.png)

4.Ir a la pestaña Compartir y tomar nota del nombre del Recurso Compartido en este caso es HPPrint.

![IMG](7.png)


