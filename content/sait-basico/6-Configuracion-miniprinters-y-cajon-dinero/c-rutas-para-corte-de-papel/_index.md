﻿+++
title = "Secuencias ASCII para corte de papel automático"
description = ""
weight = 3
+++

Ciertas impresoras tienen la funcionalidad de cortar el papel desde las mismas propiedades de la miniprinter, sin embargo si este no es el caso, por medio de comandos se puede configurar que al momento de procesar la nota de venta, se imprima y a la vez corte el papel en automático.

Para esto, será necesario agregar un campo al final del formato con la siguiente expresion:

Para impresora Epson: 

```
CHR(27)+'i'
```
Para Impresora Star: 

```
Chr(27)+'d'+'1'
```

En ambos casos, el ancho del campo debera ser de 5 

Para agregar la expresión y cortar automáticamente siga las siguientes instrucciones:

1.Entrar a SAIT e ir a Utilerías / Modificar Formatos / Nota de venta 

Clic en **[Editar]**

![IMG](1.png)

2.Se nos abrirá la pantalla de edición de formato.

Dar clic en el botón indicado en la siguiente imagen y después dar clic con el mouse al final del formato.

![IMG](2.png)

3.Se nos abrirá la siguiente ventana en dónde ingresaremos la secuencia, en este caso tomaremos de ejemplo una impresora Epson

![IMG](5.png)

![IMG](3.png)

Dar clic en **[OK]**

4.Para finalizar guardamos cambios, yendo al menú de File y dar clic en **[Save]**

![IMG](4.png)


