﻿+++
title = "Registrar Pagos de los Clientes"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=lICFoDKgie0&index=3&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. A Abonos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El siguiente proceso describe los pasos para registrar con un solo pago el abono a varias facturas del mismo cliente.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Registrar Pagos de los Clientes</h4>


1.Escriba la **fecha** del pago.

2.Escriba la clave del cliente. Hacer clic en **[?]** para buscar.

3.Seleccione el concepto de la **forma de pago**.

4.Especifique el **folio** del pago.

5.Capture el **importe** del abono.

6.Seleccione con la barra espaciadora la **divisa** (si mezclará divisas, es decir pagar una factura de dólares con pesos, o una factura de pesos pagarla con dólares, deberá usar la ventana de Cobranza / Abonos)

7.En caso de que el pago sea en dólares, especifique el **Tipo de Cambio**.

8.**Seleccione los documentos que se van a pagar**. En caso de que el documento no se pague en su totalidad, especifique el pago en la columna Abono de cada documento.

9.Presione la tecla **[F8]** para procesar el pago.

![IMG](1.png)

<h3 class="text-danger">NOTA</h3>

En caso de que el concepto que usted requiera para el abono no se encuentre definido, consulte la siguiente dirección en la cual se especifican los pasos para configurar conceptos de cobranza:  

https://ayuda.sait.mx/sait-basico/12-capacitacion-modulo-de-cobranza/k-conceptos-de-cxc/