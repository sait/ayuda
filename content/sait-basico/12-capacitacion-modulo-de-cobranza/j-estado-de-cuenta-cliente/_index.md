﻿+++
title = "Estado de Cuenta"
description = ""
weight = 10
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=mAvvhrML4Rw&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&index=8&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. J Estado de Cuenta.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

En el estado de cuenta se permiten consultar todos los movimientos que ha tenido el cliente.

Cada documento de crédito o cada pago realizado puede ser consultado desde esta ventana.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Estado de Cuenta</h4>

Escriba la clave del cliente o parte del nombre del cliente. Hacer clic en **[?]** o presionar la tecla **[F2]** para buscar.


Se muestran todos los movimientos del cliente.

La información se puede ordenar por Fecha y Factura.

![IMG](1.png)

Se puede enviar la consulta a Excel al hacer clic en icono de Excel.

Al hacer doble clic sobre el folio del documento, se muestra la consulta individual para su revisión.

La información que se muestra es:

* Fecha del movimiento

* Concepto del movimiento

* Folio del Documento

* Referencia: Si es un abono hace referencia al cargo al que pertenece. Los cargos hacen referencia hacia sí mismos.

* Importe del Cargo

* Importe del Abono

* Divisa del movimiento

* Tipo de Cambio

* Saldo del Documento

* UUID del documento CFDI o pago electrónico.


<h4 class="text-primary">Los anticipos o notas de crédito (saldo a favor del cliente) pendientes de aplicar se muestran en color azul</h4> 
<h4 class="text-danger">Los cargos pendientes de pago se muestran en color rojo</h4>  
<h4> Los abonos o cargos pagados se muestran en color negro </h4>

_________________________________

<h2 class="text-primary">Eliminar  Movimientos</h2>

Además de consultar movimientos, desde esta ventana es posible eliminar movimientos en caso de haberse registrado de manera incorrecta o que no corresponda al cliente.

1.Ir al menú de Cobranza / Estado de Cuenta.

2.Debe localizar el movimiento que se desea eliminar.

Puede ordenar la información por Fecha y Factura para que sea más fácil de localizar el movimiento.

3.Una vez localizado, lo selecciona y hace clic en [Eliminar Movimiento] en la parte inferior de la ventana.

El sistema pregunta si desea borrarlo.

![IMG](2.png)

4.Haga clic en [Si].

5.Listo

Nota: En caso de que el tipo de movimiento que se va a eliminar sea un cargo y éste tenga abonos recibidos se mostrará el siguiente mensaje:


![IMG](3.png)

Para esto es necesario que se eliminen previamente los abonos recibidos al documento.