﻿+++
title = "Cancelación de Anticipos y Notas de Crédito"
description = ""
weight = 8
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=7UKSADZ9bVM&index=7&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. H Cancelacion de Anticipos y Notas de Crédito.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Para cancelar un anticipo o nota de crédito es necesario que previamente haya sido registrado. La cancelación se realiza sobre el saldo actual del documento.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Cancelación de Anticipos y Notas de Crédito</h4>

1. Escriba la clave del cliente. Hacer clic en **[?]** para realizar búsquedas de clientes.

2. Seleccione con la barra espaciadora el concepto a cancelar.

3. Capture el folio del documento.

6. Especifique la fecha de cancelación. Se muestra el monto inicial, divisa y tipo de cambio. También se muestra el saldo actual y la fecha en que se registró el documento.

7. Haga clic en **[Cancelar]**.

![IMG](1.png)

8. El sistema pregunta si está seguro de la cancelación. Haga clic en **[Si].**

![IMG](2.png)

9. El estado de cuenta se verá afectado de la siguiente manera

![IMG](3.png)

<h2 class="text-danger">Importante</h2>

#### Si bien usted puede simplemente eliminar el movimiento directo del estado de cuenta del cliente es mejor realizar este proceso de cancelación ya que así queda el registro.