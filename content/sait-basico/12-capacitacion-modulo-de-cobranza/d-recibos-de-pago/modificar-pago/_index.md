+++
title="Modificar Detalles de un Pago"
description=""
weight = 10
+++

Tal vez necesite modificar los detalles de un pago como lo son, la cuenta origen, o el forma de pago.

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=hmbD07jQ1eM" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:
- Nos dirigimos a Cobranza/Emitir CFDI de pagos.
- Hacemos la consulta para ver los pagos del cliente.
- Hacemos doble clic o presionamos la tecla Intro en el Pago que modificaremos.
- Se nos presentara la ventana detalle de pago.
- Aquí podemos modificar los detalles del pago, la forma del pago, el folio, cuenta origen, cuenta destino y datos de Comprobante de método de pago en caso de ser una transferencia.
- Guardamos los cambios con el botón  &lt;F8 Guardar&gt;
![IMG](Detallepago.PNG)
