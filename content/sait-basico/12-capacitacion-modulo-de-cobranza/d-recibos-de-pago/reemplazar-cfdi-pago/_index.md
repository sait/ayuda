+++
title="Cancelar REP y reemplazarlo con uno nuevo"
description=""
weight = 11
+++

Si realizamos la cancelación de un REP y se volverá a timbrar, es necesario realizar la relación del mismo a continuación se detallan los pasos: 

<h3 class="text-primary">Cancelar CFDI de Pago</h3>

- Vaya a Cobranza / Consultar CFDIs de Pagos, ingreamos los filtros y damos clic en Consultar

 ![](A1.png)

- Seleccionamos el folio a cancelar y damos clic en Cancelar CFDI

 ![](A2.png)

- Ingresamos los datos de cancelación como folio, motivo y seleccionamos el motivo 02 y damos clic en Sí para cancelar el REP, nos aparecerán ventanas de confirmación y damos clic en Sí para continuar.

 ![](A3.png)

 - Una vez cancelado aparecerá de la siguiente forma

 ![](A4.png)


 <h3 class="text-primary">Borrar el abono de Cobranza</h3>

 - Vaya a Cobranza / Estado de Cuenta / Ingrese el cliente, ubicamos el abono y lo seleccioamos de manera que aparezca en azul el registro y damos clic en Eliminar Mov.

  ![](A5.png)

 <h3 class="text-primary">Aplicar el abono nuevamente</h3>

- Vaya a Cobranza / Registrar Pagos de los Cliente / Ingrese el número de cliente, forma de pago, fecha de pago y seleccione la factura a abonar

  ![](A6.png)

 <h3 class="text-primary">Relacionar REP cancelado</h3>

- Iremos a Cobranza / Emitir CFDI de pagos, hacemos la consulta para ver los pagos del cliente, seleccionamos el registro al que le vamos a relacionar el REP 

 ![](A7.png)

- Hacemos doble clic o presionamos la tecla Intro en el Pago que volveremos a timbrar, aquí se nos presentará la ventana detalle de pago, daremos clic en Relacionar CFDI

 ![](A8.png)

- Y nos aparecerá la ventana de REPS cancelados, realizamos la consulta y se nos dará un listado de los CFDIs cancelados del cliente y deberemos seleccionar el que será reemplazado, presionamos Intro o hacemos clic sobre este y damos clic en Continuar

 ![](A9.png)

 - Notaremos que ya se mostrará el UUID relacionado y finalmente daremos clic en F8 Guardar

  ![](A10.png)

- Nos aparecerá el siguiente mensaje en donde nos indique que se agregará un CFDI relacionado en el XML, damos clic en Sí para continuar

  ![](A11.png)

 <h3 class="text-primary">Timbrar nuevo REP</h3>

- Regresaremos a la ventana de Emitir Recibo de Pagos, seleccionmos el pago a timbrar y damos clic en Emitir un  CFDI por cada Pago

  ![](A12.png)

 - Y listo, al consultar su REP se incluirá el UUID del pago cancelado y el tipo de relación de manera automática.

  ![](A13.png)