+++
title="Consultar CFDI de Pagos"
description=""
weight = 8
+++

La ventana de consulta de CFDIs de pagos es una herramienta que nos permitira ver los CFDIs emitidos, volver a imprimirlos o reenviarlos a nuestros clientes.

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=43Vdehrza50" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:
* Ir a Cobranza / Consultar CFDI de pagos
* Se nos presentará la ventana &#39;Consulta CFDIs de Pagos&#39;
* Dentro de la ventana podremos filtrar los resultados de la consulta por medio de cliente, fechas, folio de documentos y UUID de los cfdis. Clic en &lt;Consultar&gt; para mostrar resultado
* En esta ventana se tienen las siguientes opciones:
  * **Ver XML:** Mostrar en pantalla el XML del CFDI de pago  seleccionado. En esta ventana podra copiar a portapapeles el contenido del XML, guardar el archivo físicamente o mandar a imprimir XML.
  * **Ver PDF:** Mostrar en pantalla versión impresa de CFDI de pago en formato PDF, en donde podra dar uso a dicho archivo generado.
  * **Excel:** Mandar consulta en pantalla a un libro Microsoft Excel, en donde podra dar uso a dicho archivo generado.
  * **Enviar Indiv.** : Mandar por medio de correo electrónico el CFDI al cliente seleccionado.
  * **Enviar todos:** Mandar todos los CFDIs de pago que se encontraron en la consulta, por medio de correo electrónico a los clientes.
  * **Reporte de Envió:** Muestra un historial del envio de correos electronicos, donde se podrá leer el estado de entrega de los mismos.
  * **Imprimir Indiv.** : Mandar a imprimir el CFDI de pago seleccionado a impresora.
  * **Imprimir Todos:** Mandar todos los CFDIs de pagos que se encontraron en la consulta a impresión.
  * **Cancelar CFDI:** Cancelar CFDI de Pago seleccionado, puede revisar la siguiente documentación: **&lt;Enlace a Cancelar Pagos&gt;**
  * **Copiar:** Copiar el contenido del campo &#39;Acuse de Cancelación&#39;. El cual contiene fecha y hora de certificación, número de certificado y acuse de cancelación solamente si el CFDI se encuentra cancelado.
![IMG](1.PNG)
