+++
title="Anexar Comprobante electrónico de pago SPEI a pago con Transferencia Bancaria (Uso Opcional)."
description=""
weight = 5
+++

Estas son las instrucciones para agregar el comprobante electrónico de pago en los pagos de sus clientes.

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=hmbD07jQ1eM" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:
* Nos dirigimos a Cobranza/Emitir CFDI de pagos.
* Hacemos la consulta para ver los pagos del cliente.
* Hacemos doble clic o presionamos la tecla Intro en el Pago que reemplaza el anterior.
* Se nos presentará la ventana detalle de pago.
* Al ser el método de pago una transferencia, aparecerá el recuadro donde podemos añadir o eliminar  el CEP de  nuestro pago.
* Para anexar usamos el botón < Anexar CEP>
* Se nos presentará una ventana para seleccionar el XML del comprobante.
* Se cargará el XML en el recuadro donde podemos visualizarlo.
* Para borrar el CEP usamos el botón  < Eliminar CEP>
* Guardamos los cambios con el botón  < F8 Guardar>
![IMG](anexarspei.PNG)
