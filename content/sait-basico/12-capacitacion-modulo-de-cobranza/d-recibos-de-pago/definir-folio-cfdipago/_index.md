+++
title="Definir Folio del siguiente CFDI de Pago"
description=""
weight = 4
+++
Para control interno todos los "CFDIs de Pago" o "Recibo Electrónico de Pago" que se emiten en SAIT, tienen asignado un folio interno y opcionalmente una serie, por eso antes de empezar a emitirlos usted deberá definir cual será el folio interno a usar.


<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=JhK5RDMduUE" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:
* Ir al menú de "Ventas - Siguientes Folios"
* Se abre la ventana de "Actualizar Siguientes Folios"
* Busque el campo de "Sig Folio" para pagos Electrónicos 
* Escriba el folio interno que llevará el siguiente CFDI de Pagos que se emita.
* Si además del folio desea definir la serie:
	* Deberá anteponer la serie de la sucursal que emite el CFDI.
	* Se entiende por serie, a la combinación de letras que permite la identificación de la sucursal, por ejemplo: MTY para Monterrey, GDL para Guadalajara, CDM para Ciudad de México, MXL para Mexicali o cualquier combinación que usted le parezca conveniente.
	* Quedando el siguiente folio conformado por la serie y el folio, unidos así: MTY1761 o GDL1001 o A99
* Haga clic en "Actualizar"

![IMG](folio.PNG)

### Video:
{{<youtube 0NXPxEqITrw>}}

