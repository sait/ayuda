+++
title="Asignar Clave SAT a Conceptos de Cobranza"
description=""
weight = 2
+++

Usted deberá asignar la nueva clave de sat a sus conceptos de cobranza para poder emitir los CFDI de pago.

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=T_YNirYHVI8" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:
* Ir a Cobranza / Conceptos de Cuentas por Cobrar
* Se nos presentara el Catálogo de Conceptos de Cuentas por Cobrar, enlistando todos los conceptos que tenemos registrados.
* Seleccionar el Concepto que deseamos actualizar y clic en &lt;Modificar&gt;
* Se nos presentara el formulario referente al concepto ya registrado, notaremos que se encuentra en nuevo campo &#39;Clave SAT&#39; en base al catálogo de Formas de Pago publicado por el SAT.
* Asignar la clave SAT correspondiente
* Clic en &lt;Modificar&gt;
![IMG](CONCEPTOS.PNG)

