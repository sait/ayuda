+++
title="Emitir CFDI de Pagos"
description=""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=7BWicj-fb1c" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>
</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:


* Ir a Cobranza / Emitir CFDI de Pagos
* Si la opción aparece en gris es porque el usuario no tiene configurado el permiso a Emitir CFDI de pagos, puede verlo [Aquí{{%icon fa-external-link%}}](/sait-basico/12-capacitacion-modulo-de-cobranza/d-recibos-de-pago/habilitar-cfdi-pagos/)
* Se nos presentará el formulario 'Emitir CFDIs de Pagos'
* En el campo de 'Cliente' ingresar la clave de cliente en caso de que deseemos emitir cfdi por el pago de un cliente en específico
* Establecer el período de fechas de los pagos capturados en los campos de 'Período'
* Marcar la casilla 'Pendientes de Timbrar' si deseemos que solo se muestren los pagos que aún no cuentan con cfdi de pago
* Clic en < Consultar>
* Se nos presenta el listado de los abonos / pagos pendientes de timbrar
* En el campo 'Fecha de Emisión del CFDI' podremos elegir la fecha de emisión que quedará registrada en en XML del cfdi
* Marcar la casilla 'Incluir en CFDI la cuenta en donde se depositó el pago' si deseamos anexar en XML del cfdi el campo de Cuenta de Beneficiario.
* Dentro de la tabla de los abonos / pagos pendientes, podremos seleccionar y dar doble clic, o bien, teclear 'Enter' para modificar el Detalle de Pago. < Enlace a Docum detalle de pago.>
* Una vez teniendo el detalle del pago correctamente actualizado, podremos seleccionar uno o varios abonos / pagos listos para timbrar, marcando la casilla 'Timbrar' dentro de la tabla.
* Clic en <Generar un CFDI por Pago> para emitir un cfdi por cada abono / pago.

{{%alert danger%}}IMPORTANTE: si su pago no tiene folio emitira un cfdi por cada operacion, para ver como registrar correctamente un paqo consultar [Aquí](/sait-basico/12-capacitacion-modulo-de-cobranza/b-registrar-pagos-de-los-clientes/){{%/alert%}}


 ![IMG](image027.jpg)
