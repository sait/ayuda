﻿+++
title = "Generar Póliza de Ingreso"
description = ""
weight = 6
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=qK1lj7WRmsc&index=5&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. F Generar Poliza de Ingreso.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Este proceso le permite afectas las cuentas de bancos con los pagos realizados del día a facturas que se otorgaron a crédito.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Generar Póliza de Ingresos</h4>

1.Indique el día del cuál desea generar la póliza de ingresos

2.De clic en Consultar

3.En la columna Cve, ingresamos la clave de la cuenta bancaría que deseamos afectar.

![IMG](1.png)

4.Damos clic en Generar póliza 

5.Aparecerá la siguiente pantalla, verificamos la información y damos clic en Guardar **[F8]**.

![IMG](2.png)

6.Listo, la póliza de ingresos fue generada correctamente.

![IMG](3.png)