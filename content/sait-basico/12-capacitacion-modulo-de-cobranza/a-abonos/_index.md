﻿+++
title = "Abonos"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=0Ixxo6mlPcA&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&index=5&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. A Abonos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El siguiente proceso describe los pasos para registrar un pago a un solo documento.

Esta opción de gran utilidad cuando se desea abonar mezclando divisas, es decir, pagar una factura de dólares con pesos, o una factura de pesos pagarla con dólares.

Para poder utilizar esta opción dentro de SAIT diríjase a:

<h4 class="text-primary">Cobranza / Abonos</h4>

* Seleccione el concepto de la **forma de pago**.

* Escriba la clave del cliente. Hacer clic en **[?]** para buscar.

* Escriba la referencia del documento a pagar anteponiendo la palabra **FA** seguido del número de factura. Hacer clic en **[?]** para seleccionar un documento en caso de no recordar el folio.

* Capture el **folio del pago** en el campo de #Documento.

* Especifique la **fecha del pago**.

* Indique el **importe** a abonar o a pagar.

* Seleccione con la barra espaciadora la **divisa**.

* En caso de que el pago sea en dólares, se deberá especificar el **Tipo de Cambio**.

* De forma opcional puede especificar **observaciones**.

* De forma opcional puede indicar la **cuenta de origen y el banco de depósito**.

* Presione la tecla **[F8]** para procesar el pago


![IMG](1.png)

<h3 class="text-danger">NOTA</h3>

En caso de que el concepto que usted requiera para el abono no se encuentre definido, consulte la siguiente dirección en la cual se especifican los pasos para configurar conceptos de cobranza:  

https://ayuda.sait.mx/sait-basico/12-capacitacion-modulo-de-cobranza/k-conceptos-de-cxc/