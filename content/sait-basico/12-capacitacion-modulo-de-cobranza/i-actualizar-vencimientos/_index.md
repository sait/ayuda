﻿+++
title = "Actualizar Vencimientos"
description = ""
weight = 7
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href=https://www.youtube.com/watch?v=dsyUnpML5cA&index=9&list=PLhfBtfV09Ai6fVXzDC4hF9hMJZQU7PbLu&t=0s" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="12. I Actualizar Vencimientos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

El vencimiento de un documento a crédito se determina en el momento en que se procesa en base a la fecha del documento y los días de crédito del cliente.

En ocasiones los clientes no pueden cubrir el pago del documento a la fecha de vencimiento, por lo cual se negocia el pago y se llega a otra fecha de vencimiento.

**Es importante mantener los vencimientos actualizados de los clientes para que, al consultar reportes de saldos, el documento aparezca con la fecha correcta.**

Para poder actualizar esta fecha, deberá dirigirse al menú de:

<h4 class="text-primary">Cobranza / Actualizar Vencimientos</h4>


1. Escriba la clave del cliente. Haga clic en **[?]** o presione la tecla **[F2]** para buscar.

2. Colóquese en la columna “Vencimiento” y actualice la fecha de vencimiento del documento.

3. Habilite la casilla “Entregada” si ya entrego la factura al cliente y éste proporcionó un contrarrecibo.

4. Haga clic en **[Borrar Fechas]** para borrar todas las fechas de vencimiento de todos los documentos.

5. Haga clic en **[Modificar Fechas]** para actualizar el vencimiento de aquellos documentos que se vencen en la misma fecha que el documento seleccionado.

6. Presione la tecla **[F8]** para procesar la actualización de los vencimientos.
 

![IMG](1.png)

