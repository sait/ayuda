+++
title = "Inventario a Puertas Abiertas"
weight = 3
+++

Ya no tienes que cerrar tu sucursal para poder realizar Inventario, conoce la nueva forma de hacer Inventario con SAIT.

Opción disponible a partir de la Versión 2018.24.0


### Para descargar la presentación de Webinar de clic [aquí](PresentacionInventariosaPuertasAbiertas.pdf)

{{<youtube AnsBVbo8OeE>}}




