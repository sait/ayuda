+++
title = "Complemento de pago"
weight = 4
+++



### Para descargar la presentación de Webinar de clic [aquí](presentacion-rep-2018.pdf)

{{<youtube v4pl5QJnVJE>}}



### **PREGUNTAS FRECUENTES**:


1.¿Puedo generar un Recibo Electrónico de pago (REP) por todos los 8 meses?

* R.- NO, El SAT establece que se puede hacer uno por pago o uno por mes.


2.¿Cuál es el fundamento legal o la resolución del SAT donde dice que se tienen que generar los recibos electrónicos de pagos de Enero A Agosto?

* R.- El Art. 29-A, Fracción VII y La tercera Resolución de modificaciones a la Resolución Miscelánea Fiscal para 2016 publicada el 05 de diciembre del 2016, donde señala que la vigencia sería a partir del 1 de julio del 2017, solo con la versión 3.3 de factura electrónica, obligatoria a partir del 1 de diciembre del 2017. Y el 16 de febrero público la Primera RMF para 2018 en la que establecen que los contribuyentes puedan pueden optar por no expedir la factura con complemento para recepción de pagos (recibo electrónico) y en su lugar sigan expidiendo una factura de ingreso por los pagos parciales o diferidos siempre que se relacione con la factura que se está pagando, de acuerdo a las siguientes claves de relación: 08 Factura generada por pagos en parcialidades - 09 Factura generada por pagos diferidos

3.¿Se debe emitir un REP por la factura Global del día o por las emitidas al público en General?


* R.- NO, las facturas a público en general se deben emitir de contado y NO se les hace REP.

4.¿Los contribuyentes RIF tienen que emitir Recibo Electrónico de Pago?

* R.- Los RIF ESTÁN OBLIGADOS POR Ley, sin embargo dentro de las
facilidades para estos contribuyentes para el 2018, está la de NO elaborar los CFDI de pagos.

5.¿Cuándo debo emitir los CFDI de pago?

* R.- Es obligación emitirlos antes de los primeros 10 días del siguiente mes, pero si tu cliente lo solicita antes deberás emitirlos ya que estés seguro que
el dinero está en tus cuentas de bancos, Sin embargo si no los emitiste a tiempo y no te ha llegado alguna revisión de parte de la Autoridad los debes emitir antes de que tu cliente haga la denuncia en el SAT y así evitarte multas que van desde los 12 mil hasta los 60 mil pesos.


5.¿Cuándo se registra un complemento que se pagó con transferencia y en esa transferencia se hizo un pago de 3 facturas por ejemplo, me emite un pago por cada factura o sea un complemento por factura en vez de hacer un solo complemento por esas 3 facturas, a diferencia de si es pago con cheque que si hace un solo complemento por las 3 facturas, qué diferencia hay?


* R.- En todos los casos de pago en transferencia, cheque, efectivo u otro, el REP de un pago incluye las tres facturas que te paga o si te paga 10, incluye las diez, es decir timbras un CFDI y te incluye las 3 facturas que tepaga.

6.¿Es obligatorio incluir los datos de la cuenta de depósito y de la cuenta de banco del cliente?


* R.- NO es obligatorio incluirlo, es un dato opcional y es tan válido como uno que si traiga esos datos.

7.¿Y cuando un cliente nos paga una factura que es en DOLARES nos las paga en PESOS como se hace?

* R.- El Recibo Electrónico de pago se hace en la moneda del pago, solo iría como dato informativo el tipo de cambio que se utilizó para la conversión del abono, el tipo de cambio cuando un documento es diferente a la moneda Nacional siempre va en el REP.
Cuando se registra el Abono de una divisa distinta al de la factura original, en SAIT en la ventana de cobranza/abonos, automáticamente te hace la conversión y ya te quedan los datos listos para poder emitir el REP.


8.¿Cuál es el límite de días o fechas para poder emitir un Recibo
Electrónico de Pago?

* R.- El SAT habla que tienes hasta los primeros 10 días del mes siguiente para poder emitir este Recibo Electrónico de Pago.