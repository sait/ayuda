+++
title = "Aguinaldos y Tips para tu Cierre Anual"
weight = 11
+++

________________

### Documentación para el Cálculo de impuestos de ingresos extraordinarios (Aguinaldo y PTU) Aguinaldo, para descargar de clic [aquí](Calculo-de-impuestos-de-ingresos-extraordinario-Aguinaldo-y-PTU.pdf))

#### Reportes a instalar en el sistema (de clic sobre los enlaces para descargar)

* [Reporte-isr-art174.rpt](Reporte-isr-art174.rpt)  

* [Art174.rar](Art174.rar)  

Para verificar el cálculo del ISR usando el art. 174 puede descargar el siguiente archivo: [Calculo-isr-art-174.xls](Calculo-isr-art-174.xls)  

<h4 class="text-danger">Importante mencionar que si usted maneja salarios mínimos y tiene la condición para NO retener ISR deberá quitarla momentáneamente para que le retenga ISR en esta nómina de aguinaldos</h4>

Para realizar este proceso siga estos pasos: 

1) Vaya a Catálogos  / Conceptos 

2) Ubicar concepto 51 ISR 

3) Clic en [Modificar]

4) Elimine la condición: con(501)>con(101)

5) Clic en [Grabar]

________________


### Documentación para generar Aguinaldo, para descargar de clic [aquí](Aguinaldo.pdf)

_________________

### Documentación consultar Cálculo Anual de ISPT, para descargar de clic [aquí](NominaOtros-ReportesCalculo-Anual-de-ISPT.pdf)

#### Reportes a instalar en el sistema (de clic sobre los enlaces para descargar)

Agregar al sistema desde el menú de utilerías / recibir reportes: 

* [calculo-anual-de-ispt.rpt](calculo-anual-de-ispt.rpt) 


Descargar el siguiente archivo y descomprimirlo dentro del directorío de la empresa:  

* [ConcAnio.rar](ConcAnio.rar)  

__________________

#### Para descargar la presentación de Webinar de clic [aquí](WebinarAguinaldos.ppsx)

{{<youtube RAQx_QyaOso>}}







