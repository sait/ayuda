+++
title="Configuración de Eventos para CFDI de Traslado y Carta Porte"
description=""
weight = 6
+++


Si usted maneja Enlace de Sucursales es necesario agregue los siguientes eventos a la configuración de enlace para poder compartir los catálogos.

### **Instrucciones**:

- Ir a la carpeta raíz de instalación de SAIT Distribuido. Ejem: C:\SAITDIST
- Ejecutar 'sdconfig.EXE'
- Clic en [Configurar Eventos]
- Seleccionar un espacio disponible y dar clic en [Insertar]
- Capturar la siguiente información en su columna correspondiente
- Clic en [Aceptar] para salir finalmente


| Evento     | Descripción              | Enviar  | Recibir |
|------------|--------------------------|--------------------|------------------|
| CPCamion   | Camiones                 | X | X |
| CPOperador | Operadores               | X | X |
| CPRemolque | Remolques                | X | X |
| ModDirCliO | Agregar y modificar dirección origen del cliente  | X | X |
| DelDirCliO | Eliminar dirección origen del cliente             | X | X |
| ModDirCliD | Agregar y modificar dirección destino del cliente | X | X |
| DelDirCliD | Eliminar dirección destino del cliente            | X | X |


{{% children  %}}
