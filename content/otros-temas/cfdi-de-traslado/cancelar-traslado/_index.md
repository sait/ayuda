+++
title="Cancelación de un CFDI de Traslado"
description=""
weight = 5
+++

El proceso de cancelación de un CFDI de Traslado es muy sencillo y a continuación se detalla.

Para hacerlo siga las siguientes [instrucciones](#instrucciones).

### **Instrucciones**:

{{%alert info%}}La cancelación de CFDI traslados solo se puede hacer desde la sucursal donde se timbro.{{%/alert%}}

* Dirigirse al menú de: **Inventarios / CFDI'S de Traslados de Mercancía / Consultar CFDI'S de Traslados.**
* Se nos presentará la ventana de: **Consulta de CFDI de Traslado.**
* Dentro de la ventana podremos filtrar los resultados de la consulta por medio de Tipo de Documento, Rango de Folios, Fechas, Origen, Destino o por UUID. Hacer Clic en **[Consultar]** para mostrar resultado.
* Deberá de seleccionar el registro o CFDI de Traslado a **Cancelar**, simplemente hacer doble clic o Enter sobre el registro.
* Se visualizará la ventana de consulta individual de Traslados, aquí podemos revisar los artículos incluidos en el traslado el origen y el destino.
![IMG](canctras1.png)
* Para cancelarlo debemos hacer clic en el botón **[Cancelar]** que se encuentra en la parte baja de la ventana. 
* Se nos presentará la ventana de confirmación de cancelación, aquí colocamos algún comentario sobre la cancelación, seleccionamos el motivo y debemos escribir el folio del documento donde se indica, hacer clic en **[SI]**
![IMG](canctras2.png)
* Una vez cancelado el Status del documento cambiará a **"Cancelado"** y se mostrará en texto rojo, el acuse de cancelación aparece en la ventana de consulta de traslados.
![IMG](canctras3.PNG)



{{% children  %}}
