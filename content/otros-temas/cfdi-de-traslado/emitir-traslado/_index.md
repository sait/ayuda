+++
title="Emitir CFDI de Traslado"
description=""
weight = 2
+++

El Traslado se emite a partir de un traspaso realizado en la ventana de Registro de salidas o a partir de una factura, es necesario que tenga a la mano el folio del documento. Para este ejemplo, utilizaremos una **SALIDA POR TRASPASO.**


Para emitir un CFDI de traslado siga las siguientes [instrucciones](#instrucciones).

### **Instrucciones**:
* Dirigirse al menu de: **Inventario / CFDIs de Traslados de Mercancía / Emitir CFDIs de Traslado.**
* Se nos presentará el formulario **'Emitir CFDI de Traslado'**
* En el campo de **'Emitir a partir de:'** debe elegir si se trata de: **Traspaso, Factura, Salida a Sucursal o Remisión.**
* En el campo **'Folio'** debe teclear el folio del documento del cual emitirá un traslado.
* Se cargarán los datos del **Origen y el Destino**, así como los artículos a transportar.
* Hacer clic en el botón: **[Emitir CFDI=F8]**
![IMG](traslado1.png)
* Si el timbrado fue un exito, deberá seleccionar el destino de impresión deseado. **Se comparte representación impresa:**
![IMG](muestra-traslado.png)

