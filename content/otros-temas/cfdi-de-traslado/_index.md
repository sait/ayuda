+++
title="CFDI de Traslado"
description=""
weight = 16
+++

El SAT a solicitado que se emitan CFDI con la información de los traslados de mercancía necesario para acreditar la tenencia o posesión legal de las mercancías objeto de transporte durante su recorrido hasta el lugar de destino. El Traslado es muy parecido a un ingreso o un egreso, se omiten algunas propiedades, las cantidades monetarias quedan en ceros y se eliminan los impuestos.

Este complemento es un título legal del contrato entre un transportista y un expedidor o usuario que contrata su servicio. Este será el comprobante de la recepción o entrega de mercancías, su legal posesión y traslado, el cual es obligatorio a partir del día **1 de enero de 2022.**


Te compartimos a continuación los pasos para poder generarlo dentro de: **SAIT ERP o SAIT Básico**:


<div style="display: flex; justify-content: space-around; flex-wrap: wrap;">

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Configuración Previa</h4>
	<img src="engranes.png" style="max-height: 150px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/otros-temas/cfdi-de-traslado/configuracion-previa">Ver mas..</a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Emitir CFDI de Traslado</h4>
	<img src="qr.png" style="max-height: 150px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/otros-temas/cfdi-de-traslado/emitir-traslado">Ver mas..</a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">CFDI con Complemento Carta Porte</h4>
	<img src="transporte.png" style="max-height: 150px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/otros-temas/cfdi-de-traslado/emitir-carta-porte">Ver mas..</a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Consultar CFDI de Traslado</h4>
	<img src="reportes.png" style="max-height: 150px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/otros-temas/cfdi-de-traslado/consultar-cfdis-traslado">Ver mas..</a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-body">
	<h4 style="text-align: center;">Cancelar CFDI de Traslado</h4>
	<img src="cancelar.png" style="max-height: 150px;margin: auto !important;">
</div>
<div class="panel-footer">
	<a class="btn btn-primary btn-block " href="/otros-temas/cfdi-de-traslado/cancelar-traslado">Ver mas..</a>
</div>
</div>


</div>

{{% children  %}}