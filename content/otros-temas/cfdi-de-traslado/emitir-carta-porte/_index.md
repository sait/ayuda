+++
title="Emitir CFDI de Traslado con Carta Porte"
description=""
weight = 3
+++

El 1 de mayo de 2021 el SAT publicó en su página el complemento Carta Porte el cual es un título legal del contrato entre un transportista y un expedidor o usuario que contrata su servicio. Este será el comprobante de la recepción o entrega de mercancías, su legal posesión y traslado.

Para emitir un CFDI de traslado siga las siguientes [instrucciones](#instrucciones).

### **Instrucciones**:

* Dirigirse al menú de: **Inventario / CFDIs de Traslados de Mercancía / Emitir CFDIs de Traslado.**
* Se nos presentará el formulario **'Emitir CFDI de Traslado'**
* En el campo de **'Emitir a partir de'** debe elegir si se trata de: **Traspaso, Factura, Salida a Sucursal o Remisión.** En este caso lo elaboraremos a partir de una FACTURA.
* En el campo **'Folio'** debe teclear el folio de la FACTURA del cual emitirá un traslado.
* Se cargaran los datos del Origen y el Destino, así como los artículos a transportar.
* En la cuadrícula de artículos, la columna PESO se puede modificar para indicar el Peso del Artículo en KG.
* Debe de marcar la casilla **'Incluir Complemento'**, como se muestra a continuación:
![IMG](cporte1.PNG)
* Hacer clic en el botón **[Compl. Carta Porte]**. Se presenta el formulario **'Información de Carta Porte'.**
* De la sección de:**'Datos de Origen'** Al dar clic en el icono **Editar** tiene opción de modificar la dirección del origen y los demas datos. Debe de asegurarse en capturar la Fecha y Hora de la salida de la mercancía.
* De la sección de:**'Datos de Destino'** Al dar clic en el icono **Editar** tiene opción de modificar la dirección del destino y los demas datos en caso de ser necesario. Debe de asegurarse en capturar la Fecha y Hora de la llegada de la mercancía.
* Capturar los datos solicitados en **'Datos del Transporte'**, en donde deberá de indicar: Tipo de Traslado, Camión,  Configuración, Total de Distancia recorrida, así como tambien en caso de aplicar, la selección de hasta 2 Remolques.
![IMG](cportenew.PNG)
* Hacer Clic en el botón **[Continuar]**.
* Una vez ya finalizada con la captura y verificada la información, hacer clic en **[Emitir CFDI=F8]**.
![IMG](cportenew1.PNG)
* Ya timbrado, debe de seleccionar el destino de impresión deseado. Se anexa muestra el CFDI:
![IMG](cfditras1.png)









{{% children  %}}