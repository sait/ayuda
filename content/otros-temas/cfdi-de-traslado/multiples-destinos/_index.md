+++
title="Emitir un CFDI de Traslado con múltiples destinos"
description=""
weight = 7
+++
 
A partir de la versión 2024.2 y con la adquisición de una licencia de uso, SAIT le permite emitir un CFDI de Traslado con múltiples destinos (varias facturas) a continuación se explica de manera detallada este nuevo funcionamiento.

Primeramente deberá capturar el peso de sus camiones en el menú de Inventario / CFDIs de Traslados de Mercancia / Emitir CFDI de Traslado / Clic en el botón de **[Camiones]** 

![IMG](CAMIONES.png)   

Seleccionamos el camión y daremos clic en **[Modificar]**

![IMG](CAT-CAMIONES.png)   

Agragaremos el peso del camión y daremos clic en **[Modificar]** y listo.

![IMG](PESO.png)   

Para hacer uso de esta funcionalidad primeramente deberá contar ya con las facturas elaboradas, para efectos de la prueba se utilizarán las siguientes 2 facturas

![IMG](1.png)   

Ahora iremos a Inventario / CFDIs de Traslados de Mercancia / Emitir CFDI de Traslado, se nos presentará la nueva ventana y damos clic en el la opción de **Incluir Complemento Crta Porte** y llenaremos los datos de Transporte así como fecha y hora de salida y daremos clic en **[Continuar]**

![IMG](2.png)   

Seleccinamos tipo de documento **Factura** y teclearemos un folio de factura y daremos clic en **[Cargar]**

![IMG](3.png)   

Una vez cargado el documento notaremos se agregó el primer destino a la cuadrícula de destinos, la direción la tomará del catálogo del cliente, y en la cuadrícula se agregaron los artículos que venian en la factura

![IMG](4.png)  

Importante mencionar que todos estos campos son editables 

![IMG](5.png)  

Para agregar otra factura más, solo es cuestión de teclear el folio y dar clic en **[Cargar]**

Notará que se agregó el nuevo destino y los artículos de la nueva factura

![IMG](6.png)  

La columna de Distancia Recorrida se deberá llenar de la siguiente forma:

* El primer registro se llenará del punto de origen al primer destino 

* El segundo registro se llenará con la distancia recorrida del primer destino al segundo destino y así sucesivamente en caso de tener más destinos

![IMG](7.png)  

Se recomienda que los destinos se agreguen de acuerdo a la ruta que tomará el camión, por lo que se nos equivamos en el orden agregamos, podemos eliminar un destino desde esta seccción, seleccionaremos el folio del documento 

![IMG](8.png)  

Y damos clic en DELETE o SUPRIMIR en su teclado y notaremos se quitaron los registros relacionados a la factura AA762, y de esta manera ponemos reordenar los puntos de llegada

![IMG](9.png)  

Revisamos nuevamente los datos capturados y damos clic en **[Emitir CFDI = F8]** y listo, si abrimos nuestro xml notaremos los múltiples destinos

![IMG](11.png)  

{{% children  %}}
