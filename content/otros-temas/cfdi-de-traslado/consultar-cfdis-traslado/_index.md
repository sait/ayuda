+++
title="Consulta de CFDIS de Traslados"
description=""
weight = 4
+++

La ventana de consulta de Traslados es una herramienta que nos permitirá: Ver CFDIS de Traslados Emitidos, Reimprimirlos Nuevamente o Bien descargar en formato .ZIP.

Para hacerlo siga las siguientes [instrucciones](#instrucciones).

### **Instrucciones**:

* Dirigirse al menú de:"  **Inventario / CFDIs de Traslados de Mercancía / Consultar CFDIs de Traslados**
* Se nos presentará la ventana **"Consulta CFDIs de Traslados"**
* Dentro de la ventana podremos filtrar los resultados de la consulta por medio de Tipo de Documento, Rango de Folios, Fechas, Origen, Destino o por UUID. Hacer Clic en **[Consultar]** para mostrar resultado.
![IMG](consultatras1.PNG)
* Al hacer doble clic o Presionar Enter sobre una partida se abrirá la ventana con el detalle del traslado. Con opción de: **[Cancelar]** o **[Imprimir]**.
![IMG](consultatras2.png)
* En esta ventana se tienen las siguientes opciones:
![IMG](consultatras1.PNG)
  * **Ver XML:** Mostrar en pantalla el XML del CFDI de traslado  seleccionado. En esta ventana podrá copiar a portapapeles el contenido del XML, guardar el archivo físicamente o mandar a imprimir XML.
  * **Ver PDF:** Mostrar en pantalla versión impresa de CFDI de traslado en formato PDF, en donde podrá dar uso a dicho archivo generado.
  * **Excel:** Mandar consulta en pantalla a un libro Microsoft Excel, en donde podrá dar uso a dicho archivo generado.
  * **Imprimir Indiv.** : Mandar a imprimir el CFDI de traslado seleccionado a impresora.
  * **Imprimir Todos:** Mandar todos los CFDIs de traslados que se encontraron en la consulta a impresión.
  * **Copiar:** Copiar el contenido del campo "Datos de Timbrado". El cual contiene fecha y hora de certificación, número de certificado y acuse de cancelación solamente si el CFDI se encuentra cancelado.
  * **Guardar Todos en ZIP:** Guardar todos los PDF y XML de los CFDIs de traslados que se encontraron en la consulta a un Archivo ZIP.
