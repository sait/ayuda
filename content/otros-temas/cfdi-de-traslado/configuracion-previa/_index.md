+++
title="Configuración Previa"
description=""
weight = 1
+++

Antes de empezar a emitir CFDI de traslado con Complemento de Carta Porte, es necesario realizar las siguientes actividades en las plataformas de SAIT Básico y SAIT ERP respectivamente.

* [Contar con versión actuallizada de SAIT](/otros-temas/descargar-version/)
* [Colocar versión de Complemento Carta Porte](#colocar-version-de-complemento-carta-porte)
* [Activar Módulo de Complemento Carta Porte](#activar-módulo-de-complemento-carta-porte)
* [Habilitar Siguiente Folio en CFDI de Traslado](#habilitar-siguiente-folio-de-traslado) 
* [Habilitar Timbrado de Traslado](#habilitar-timbrado-de-traslado)
* [Capturar Peso en Artículos](#capturar-peso-en-artículos)
* [Capturar Catálogos de: Camiones, Remolques y Operadores](#capturar-catálogos-del-sat)
* [Habilitar Traslado por Salida por Traspaso y Salida a Sucursal](#habilitar-traslado-por-salida-por-traspaso-y-salida-a-sucursal)
* [Configurar Sucursal o Almacén como Cliente](#revisar-datos-en-catálogo-de-clientes)

### Colocar versión de Complemento Carta Porte

Ir a Utilerías / Factura Electrónica / Configurar CFDI / y colocar la versión 3.1

![IMG](version.png)

### Activar Módulo de Complemento Carta Porte

Antes de iniciar con el uso del CFDI de Traslado con Complemento de Carta Porte 2.0, es indispensable que SAIT Software le proporcione la clave de activación del mismo. 

Los pasos para activarlo, son los siguientes:

* Ir al menú de: **Inventarios - CFDI'S de traslados de Mercancía - Emitir CFDI de Traslado.**
* En la ventana de: **Emitir CFDI de traslado** hacer clic en el botón **[COMPL. CARTA PORTE]**, se visualiza la siguiente ventana:

![IMG](activa1.PNG)

* Debe de **"Pegar"** o Ingresar la clave de activación proporcionada por SAIT.
* Al finalizar, hacer clic en el botón **[Aceptar]**.

{{%alert success%}} NOTA: Si usted desea probar el módulo en una empresa DEMO, te compartimos la clave de activación para el RFC AAA010101AAA: hSbKDYfOHU8YfQo3nz/8dESuyVriofYzcjJCVGFCtWewgHmNLZTg{{%/alert%}}

### Habilitar Siguiente Folio de Traslado

Para control interno todos los **“CFDIs de Traslado”** que se emiten en SAIT, tienen asignado un folio interno y opcionalmente una serie, por eso antes de empezar a emitirlos usted deberá definir cual será el folio interno a usar.

* Ir al menú de: **"Ventas - Siguientes Folios"**
* Se abre la ventana de **"Actualizar Siguientes Folios"**
* Busque el campo de "Sig. Folio" para Comprobantes de traslados. 
* Escriba el folio interno que llevará el siguiente CFDI de Traslados que se emita.
* Si además del folio desea definir la serie:
	* Deberá anteponer la serie de la sucursal que emite el CFDI.
	* Se entiende por serie, a la combinación de letras que permite la identificación de la sucursal, por ejemplo: MTY para Monterrey, GDL para Guadalajara, CDM para Ciudad de México, MXL para Mexicali o cualquier combinación que usted le parezca conveniente.
	* Quedando el siguiente folio conformado por la serie y el folio, unidos así: MTY1761 o GDL1001 o A99
* Haga clic en **"Actualizar"**

![IMG](sigtraslado.png)

### Habilitar Timbrado de Traslado

El acceso a emitir CFDI de traslados se da desde el menú de inventario pero si el usuario no tiene acceso le aparecerán las opciones en gris, por lo que será necesario que se le den los permisos.

* Ir al menú de: **Utilerías/Grupos de usuarios/Catálogo de niveles.**
* Se  mostrará una ventana donde tenemos que seleccionar el nivel de usuario que modificaremos.
* Nos movemos a la pestaña: **Inventarios.**
* En la parte baja encontraremos los atributos que debemos marcar.

* El permitir **¨Emitir/Cancelar CFDI de Traslado¨ y el ¨Consultar CFDI de traslados¨** son permisos por separado.

* Damos clic en **[Grabar]** y listo.

![IMG](permisotras.png)


### Capturar Peso en Artículos

Uno de los campos a considerar dentro del CFDI de traslado, es precisamente el peso del artículo en cuestión, por lo que será necesario que se capture dentro de su respectivo catálogo.

* Dirigirse al menú de: **Inventarios/Catálogo de Artículos y Servicios.**
* Seleccionar o Buscar el artículo en cuestión.
* Hacer clic en la pestaña **"Ecommerce"** y dirigirse al campo de "Peso" para capturarlo.
* Ya capturado, hacer clic en el botón **[Grabar].**

![IMG](capturapeso.png)


### Capturar Catálogos del SAT

Como parte de la información obligatoria a considerar CFDI de traslado con Carta Porte, es indicar el **Camión** que se utilizará para el traslado de la mercancía o bienes, al igual que el **Operador** y en su caso si aplica la especificación del **Remolque**.

#### CAMIONES

* Dirigirse al menú de: **Inventarios/CFDI'S de Traslados de Mercancía/Emitir CFDI de Traslado.**
* Hacer clic en el botón **[Camiones].**
* Aparece la ventana Catálogo de Camiones, Clic en **[Agregar].**
* El sistema asigna de manera automática el sig. Folio consecutivo.
* Capturar los datos relacionados a las características del camión.
* Para finalizar y guardar, hacer clic en el botón **[Agregar].**

![IMG](camiones.PNG)

#### REMOLQUES

* Dirigirse al menú de: **Inventarios/CFDI'S de Traslados de Mercancía/Emitir CFDI de Traslado.**
* Hacer clic en el botón **[Remolques]**.
* Aparece la ventana Catálogo de Remolques, Clic en **[Agregar].**
* El sistema asigna de manera automática el sig. Folio consecutivo.
* Capturar los campos de: **'No. de Placa'** y seleccionar el listado el: **'Tipo'.**
* Para finalizar y guardar, hacer clic en el botón **[Agregar].**

![IMG](remolques.PNG)

#### OPERADORES

* Dirigirse al menú de: **Inventarios/CFDI'S de Traslados de Mercancía/Emitir CFDI de Traslado.**
* Hacer clic en el botón **[Operadores]**.
* Aparece la ventana Catálogo de Operadores, Clic en **[Agregar].**
* El sistema asigna de manera automática el sig. Folio consecutivo.
* Capturar los campos de: **'Licencia'**, **Nombre** y **RFC**.
* En caso de que el RFC capturado sea el de Extranjeros **(XEXX010101000)** o bien se deje en blanco, la ventana solicitará que capture los campos de: **Indetificación Fiscal** y **País** respectivamente.
* Si se trata que el RFC Sea: **XAXX010101000** o bien alguno registrado ante el SAT, se dejará en blanco los campos de: **Indetificación Fiscal** y **País**.
* Para finalizar y guardar, hacer clic en el botón **[Agregar].**


![IMG](operadores.PNG)



### Habilitar Traslado por Salida por Traspaso y Salida a Sucursal

Es posible Emitir un traslado inmediatamente al Generar una Salida por Traspaso o Salida a Sucursal en Inventarios siempre y cuando se haga la siguiente configuración.

* Ir al menú de: **Utilerías/ Factura electrónica / Configurar CFDI.**
* Se mostrará una ventana donde se realizan las distintas configuraciones para CFDI.
* Marcamos las siguientes opciones:
	* Generar CFDI de Traslado al registrar Salidas por Traspaso. **(Cuando maneja SAIT Distribuido / SAIT SYNC).**
	* Generar CFDI de Traslado al registrar Salidas a Sucursal. **(Cuando maneja almacenes internos).**
* Hacer clic en **[Guardar Configuración]** y listo.

![IMG](conftras.png)


### Revisar datos en Catálogo de Clientes

Es importante revisar y corroborar que para la emisión del CFDI de Traslado con Carta Porte, se debe de asegurar ciertos datos en el catálogo de clientes. Esto aplica para todos los escenarios.


- Ir al menú de: **Ventas/ Clientes.**
- En el catálogo de clientes seleccionamos el cliente especial que representa nuestra sucursal o Cliente de Facturación.
- Debemos llenar los datos del domicilio sin olvidar el Código postal. Es importante asegurarse que sea un Código Válido.
- En el campo de 'PAIS' debemos de colocar el valor correspondiente al catálogo de Países del SAT. Ejemplo para MEXICO es: MEX''  <p>Para **Consultar/Descargar** el catálogo de países del SAT, <a href="http://omawww.sat.gob.mx/tramitesyservicios/Paginas/documentos/catCFDI.xls" title="Clic aquí">
Clic Aquí</a></p>

- Debemos asignarle el RFC Genérico **'XAXX010101000'** o en su caso el RFC que le corresponda al cliente en cuestión.
- Definir el campo de **'Uso del CFDI'** y seleccionar **'P01 - Por Definir'.**
- Damos clic en **[Guardar]** y listo.

![IMG](clientesuc.png)
