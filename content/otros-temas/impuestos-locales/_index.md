+++
title="Configuración de Impuestos Locales"
description=""
weight = 14
+++

Ahora en tus CFDIs emitidos desde SAIT puedes utilizar el Complemento de Impuestos Locales, agregando derechos como impuestos locales o retenciones de impuestos federales ya sea de manera automática o manualmente.

Siga las siguientes instrucciones:


#### Configurar Impuestos Locales

- Ir a Utilerías / Configuración General del Sistema / Ventas2
- Clic en [Impuestos Locales]
![IMG](ImpLocal1.JPG)
- Se nos presentara el formulario 'Configurar Impuestos Locales'
- Marcar casilla [Usar Impuestos Locales]
- Ingresar los posibles impuestos locales que se pueden llegar a utilizar
- En la columna 'Tipo', con la barra espaciadora, podremos elegir si es un impuesto de Traslado o Retención
- Marcar casilla [Auto.] si deseamos que los impuestos locales se apliquen automáticamente al realizar un CFDI
- Clic en [Grabar]
![IMG](ImpLocal2.JPG)
- Confirmar que están correcto los datos

#### Configurar artículos y servicios aplicables

- Ir a Inventario / Catálogo de Artículos y Servicios / Precios
- Marcar casilla [Aplica Impuestos Locales]
![IMG](ImpLocal3.JPG)
- Clic en [Grabar]


#### Elaborar Factura con Impuestos Locales

- Ir a Ventas / Registrar Ventas
- Capturar la información necesaria:

 - Definir el vendedor que realiza la venta.

 - Seleccionar el tipo de documento a procesar: en este caso es FACTURA

 - Definir la forma de pago del documento: contado o crédito, modificando este valor con la tecla de barra espaciadora.

 - Definir la divisa del documento: pesos o dólares.

 - Ingresar la clave del cliente al que se va a realizar el documento de venta o presionar tecla [F2] para realizar una búsqueda del cliente.

 - Agregar los artículos que se van a incluir en la venta, el cual previamente fue marcado como [Aplica Impuestos Locales]

 - Dar clic en botón de  los artículos que se van a incluir en la venta, el cual previamente fue marcado como [Aplica Impuestos Locales]
![IMG](FACTURA.png)

- Se abrirá una nueva ventana en donde agregará los impuestos locales necesarios [Aplica Impuestos Locales], damos clic en Continuar
![IMG](APLICAR.png)

- De esta manera podrá observar en los totales las retenciones o impuestos según sea el caso
![IMG](FINAL.png)

- Finalmente al procesar la factura podrá ver que el formato ya está listo para mostrar el desglose de las cantidades.
![IMG](factura-final.png)