+++
title = "Otros Datos de Artículos para Compl. Carta Porte"
description = ""
weight = 4
+++

Al emitir [CFDI de Traslados con Complemento Carta Porte](/otros-temas/cfdi-de-traslado/emitir-carta-porte/) se puede llegar a presentar el caso en que alguno de los artículos requiera un dato especial adicional según los requerimientos el complemento. Por lo tanto, te mostramos algunos datos especiales que se podrían llegar a requerir.

<h3 class="text-primary">Tipo de Materia y Descrpición</h3>

Los siguientes campos sólo se requieren cuando se está realizando un CFDI de Importación/Exportación.

Al ser nuevos campos en la base de datos, debe asegurarse que ningún usuario esté dentro del sistema.

### Configuración
* Ir a Utilerías / Configuración general del sistema / Empresa
* Clic en [Datos Adicionales en Catálogos]
* En ‘Archivo’ seleccionar ‘Artículos’
* Clic en [Agregar Dato]
* Primero agregamos el dato Tipo de Materia **cptipomat** Capturar los siguientes valores y después dar clic en [Crear Dato Físicamente]
![1](A1.png)
* Capturar los siguientes valores y dar clic en [Crear]:
![2](A2.png)   
* Aparecerá este mensaje de confirmación y damos clic en aceptar:    
![3](A3.png)   	
* Repetimos el proceso para dar de alta el otro dato de Descrpición de Materia **cpdescmat** con los siguientes valores:
![4](A4.png)   
![5](A5.png)   


### Captura
* Ir a Inventario / Catálogo de Artículos y Servicios / Otros Datos
* En ‘Clave’ capturar clave de artículo
* En ‘Cve Tipo Mat’ capturar clave de tipo de materia
![6](A6.png) 

Los valores que puede capturar son los siguientes:

Clave | Descripción | 
---|---| ---
01|Materia Prima|
02|Materia Procesada|
03|Materia Terminada (producto terminado)|
04|Materia Para la Industria Manufacturera|
05| Otra|

**IMPORTANTE** Solamente si selecciona la clave 05 deberá llenar el campo de Descrpición de Materia **cpdescmat**, de otra manera, va vacío.

<h3 class="text-primary">Fracción Arancelaria</h3>

### Configuración
* Ir a Utilerías / Configuración general del sistema / Empresa
* Clic en [Datos Adicionales en Catálogos]
* En ‘Archivo’ seleccionar ‘Artículos’
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP4](CCP4.jpg)      	
* Clic en [Cerrar] para salir

### Captura
* Ir a Inventario / Catálogo de Artículos y Servicios / Otros Datos
* En ‘Clave’ capturar clave de artículo
* En ‘Fracc. Arancelaria’ capturar la clave de  la fracción arancelaria que corresponde con la descripción de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
![CCP5](CCP5.JPG) 
* Clic en [Grabar]

<h3 class="text-primary">Guía de Identificación</h3>

### Configuración
* Ir a Utilerías / Configuración general del sistema / Empresa
* Clic en [Datos Adicionales en Catálogos]
* En ‘Archivo’ seleccionar ‘Artículos’
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP6](CCP6.JPG)
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP7](CCP7.JPG)       	
* Clic en [Cerrar] para salir

### Captura
* Ir a Inventario / Catálogo de Artículos y Servicios / Otros Datos
* En ‘Clave’ capturar clave de artículo
* En ‘Numero Guia ID’ capturar el número de guía de cada paquete que se encuentra asociado con el traslado  de  los  bienes  y/o  mercancías  en  territorio nacional.
* En ‘Descripcion Guia ID’ capturar la  descripción  del contenido del paquete o carga registrada en la guía, o en el número de identificación, que se encuentra asociado con el traslado de los bienes y/o mercancías dentro del territorio nacional.
![CCP8](CCP8.JPG) 
* Clic en [Grabar]

<h3 class="text-primary">Material Peligroso y Embalaje</h3>

### Configuración
* Ir a Utilerías / Configuración general del sistema / Empresa
* Clic en [Datos Adicionales en Catálogos]
* En ‘Archivo’ seleccionar ‘Artículos’
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP1](CCP1.jpg)
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP2](CCP2.jpg)       	
* Clic en [Cerrar] para salir

### Captura
* Ir a Inventario / Catálogo de Artículos y Servicios / Otros Datos
* En ‘Clave’ capturar clave de artículo
* En ‘Cve Mat Peligroso’ capturar clave de tipo de material peligroso
* En ‘Cve Embalaje’ capturar clave de tipo de embalaje que se requiere para transportar el material o residuo peligroso.
![CCP3](CCP3.JPG) 
* Clic en [Grabar]

## Fracción Arancelaria

### Configuración
* Ir a Utilerías / Configuración general del sistema / Empresa
* Clic en [Datos Adicionales en Catálogos]
* En ‘Archivo’ seleccionar ‘Artículos’
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP4](CCP4.jpg)      	
* Clic en [Cerrar] para salir

### Captura
* Ir a Inventario / Catálogo de Artículos y Servicios / Otros Datos
* En ‘Clave’ capturar clave de artículo
* En ‘Fracc. Arancelaria’ capturar la clave de  la fracción arancelaria que corresponde con la descripción de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
![CCP5](CCP5.JPG) 
* Clic en [Grabar]

## Guía de Identificación

### Configuración
* Ir a Utilerías / Configuración general del sistema / Empresa
* Clic en [Datos Adicionales en Catálogos]
* En ‘Archivo’ seleccionar ‘Artículos’
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP6](CCP6.JPG)
* Clic en [Agregar Dato]
* Capturar los siguientes valores:
![CCP7](CCP7.JPG)       	
* Clic en [Cerrar] para salir

### Captura
* Ir a Inventario / Catálogo de Artículos y Servicios / Otros Datos
* En ‘Clave’ capturar clave de artículo
* En ‘Numero Guia ID’ capturar el número de guía de cada paquete que se encuentra asociado con el traslado  de  los  bienes  y/o  mercancías  en  territorio nacional.
* En ‘Descripcion Guia ID’ capturar la  descripción  del contenido del paquete o carga registrada en la guía, o en el número de identificación, que se encuentra asociado con el traslado de los bienes y/o mercancías dentro del territorio nacional.
![CCP8](CCP8.JPG) 
* Clic en [Grabar]


