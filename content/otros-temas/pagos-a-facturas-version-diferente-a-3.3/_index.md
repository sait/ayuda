+++
title = "Recibos de Pagos a Facturas con versión diferente a 3.3"
description = ""
weight = 7
+++


<h2 class="text-primary"> - FACTURAS EMITIDAS CON CFDI 3.2 - </h2> 

### Opción 1: Emitir un Recibo electrónico de pagos ya con la versión 3.3

<h4 class="text-primary">PASO 1:</h4>Aplicar los abonos de la o las facturas en cuestión: 

https://ayuda.sait.mx/sait-erp/12-capacitacion-modulo-de-cobranza/b-registrar-pagos-de-los-clientes/

<h4 class="text-primary">PASO 2:</h4>Después de aplicar los abonos deberá timbrar los recibos de pago: 

https://ayuda.sait.mx/sait-erp/12-capacitacion-modulo-de-cobranza/d-recibos-de-pago/emitir-cfdi-de-pagos/

### Opción 2: Emitir un comprobante de ingreso con la versión 3.3


 Usted deberá emitir un comprobante de ingreso (factura) con la versión 3.3

Deberá dar de alta un artículo en Inventario / Catálogo de Artículos y servicios

- La clave del artículo quedará como PAGO 

- La descripción se incluye la información de la factura, número de factura, monto del total de la operación y numero de la parcialidad que se está pagando.

- La unidad como SERV de servicio

- Palomear opción Es un Servicio

- La clave SAT 84111506 correspondiente a Servicios de Facturación

- Impuesto IVA al 16

- Precio en blanco para que al momento de generar la factura usted pueda indicar el importe de la operación

El concepto quedaría de la siguiente manera:

![IMG](PAGO.PNG)

Para evitar que se duplique el efecto ingreso, es necesario establecer la relación con el CFDI de origen:

- Con la CLAVE 08 la cual corresponde a: Factura generada por pagos en parcialidade o 
- Con la CLAVE 09 la cual corresponde a: Factura por pagos diferidos.


![IMG](08.PNG)


Posteriormente ingresará el folio fiscal de la factura a relacionar o el folio interno, si no conoce cuál es el folio deberá dar clic en el botón de interrogación [?]

![IMG](1.png)

En donde le aparecerá la ventana de búsqueda de facturas, puede filtras por rango de fecha, por folio en específico, por UUID o por sucursal, de clic en [Buscar] y deberá palomear la factura 3.2 que va a relacionar dando clic en [Relacionar]


![IMG](2.PNG)

Finalmente verificamos los datos y damos clic en F8 Continuar para timbrar el comprobante.

![IMG](3.PNG)

____________________________________________

<h2 class="text-primary"> - FACTURAS EMITIDAS EN PAPEL O CON CÓDIGO BIDIMENSIONAL - </h2> 


Usted deberá emitir un comprobante de ingreso (factura) con la versión 3.3

Deberá dar de alta un artículo en Inventario / Catálogo de Artículos y servicios

- La clave del artículo quedará como PAGO 

- La descripción se incluye la información de la factura, número de factura, monto del total de la operación y numero de la parcialidad que se está pagando.

- La unidad como SERV de servicio

- Palomear opción Es un Servicio

- La clave SAT 84111506 correspondiente a Servicios de Facturación

- Impuesto IVA al 16

- Precio en blanco para que al momento de generar la factura usted pueda indicar el importe de la operación

El concepto quedaría de la siguiente manera:

![IMG](PAGO.PNG)