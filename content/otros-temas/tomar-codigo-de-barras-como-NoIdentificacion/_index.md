+++
title = "Tomar código de barras como NoIdentificacion en XML"
description = ""
weight = 19
+++

SAIT cuenta con la opción de que en el XML en el atributo NoIdentificacion en lugar que cargue la clave del artículo, se cargue el código de barras que tiene capturado el articulo, esto es configurable por cliente.

**Importante que en este punto nadie esté usando el sistema**

1.Ir al menú de Utilerias / Configuración General del Sistema / Datos Adicionales en Catálogos:

![IMG](1.png)

2.Agrega un nuevo dato dado clic en el [Agregar Dato] 

![IMG](2.png)

Llenamos los datos como aparece en la siguiente imagen y damos clic en [Crear Campo Físicamente]

![IMG](2.1.png)

3.Llenamos los datos como aparecen a continuación y damos clic en [Crear]

![IMG](3.png)

4.Si se creó corractamente, nos aparecerá esta alerta

![IMG](4.png)

5.Ahora deberá dirigirse al catálogo de clientes y en la pestaña de Otros Datos con barra espaciadora deberá cambiar a SÍ damos clic en [Grabar]

![IMG](5.png)

6.Realizamos la factura de manera habitual y si consultamos el XML notaremos que en el nodo NoIdentificacion está el código de barras del artículo

![IMG](6.png)

![IMG](7.png)
