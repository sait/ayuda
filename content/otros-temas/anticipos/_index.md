+++
title="Manejo de Anticipos en SAIT"
description=""
weight = 15
+++

En esta sección se encuentran los temas necesarios para emitir una factura de anticipo y la forma de aplicar el anticipo como pago.


#### Previo

Se deben agregar 2 nuevas claves en el catálogo de artículos y servicios para el control de anticipos:

| Clave        | Descripción            | *Unidad | Clave SAT | Activar casilla | % IVA |
|--------------|------------------------|--------|-----------|-----------------|--------|
| ANTICIPO     | Anticipo               | ACT    | 84111506  | Es un servicio  |   16   |
| APLANTICIPO  | Aplicación de anticipo | ACT    | 84111506  | Es un servicio  |   16   |


**Nota: La unidad "ACT" debe estar creada en el "Catálogo de Unidades" y la clave SAT de la unidad debe ser: ACT**

{{% children  %}}

### Vídeo
{{<youtube -8iPjYhcFvo>}}