+++
title = "Facturar Anticipo"
description = "Guia para Emitir Facturas de Anticipos en CFDI 4.0"
weight = 1
+++

En este proceso se explica como emitir la factura de un anticipo recibido.

Siga las siguientes [instrucciones](#instrucciones).

### **Instrucciones**:
- Registrar los artículos como se vio en la [pagina anterior](../).
- Ir al menú Ventas / Registro de Ventas
- Seleccione el tipo de documento: Factura
- La factura deberá ser a crédito, ya que de esta forma se podrá llevar el control de lo recibido.
- Capture el cliente del cual recibió el anticipo
- En el detalle de la factura UNICAMENTE deberá capturar la clave: ANTICIPO
- En cantidad escriba: 1
- En el precio, capture el importe del anticipo recibido antes de impuestos
- Por último haga clic en el botón [Procesar]

![IMG](img-factant.png)

Una vez procesada la factura, en el estado de cuenta se genera el anticipo con un movimiento en color azul,  con el monto del saldo a favor del cliente el cual se aplicará como pago en la(s) factura(s) del(los) bien(es) adquirido(s).


![IMG](img-edoctaant1.png)

**Importante abonar el pago en Cobranza para capturar la forma en que su cliente pagó este anticipo**.

Esto lo puede realizar desde Cobranza / Abonos o Cobranza / Registrar Pagos de los clientes, quedando de esta manera el estado de cuenta.

Como paso final, deberá generar el REP correspondiente en Cobranza / Emitir REP - CFDI de Pago

![IMG](img-edoctaant2.png)

