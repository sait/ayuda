+++
title = "Aplicar Anticipo"
description = "Guía para Emitir Facturas de Anticipos en CFDI 4.0"
weight = 2
+++

En este documento se explica cómo aplicar un anticipo como pago a una factura mediante un CFDI de egreso (nota de crédito) aplicado a la factura de la operación.

Siga las siguientes [instrucciones](#instrucciones).

### **Instrucciones**:

Genere la factura a la cual aplicar el anticipo.

- Ir al menú Ventas / Registro de Ventas
- Seleccione el tipo de documento: Factura
- La factura deberá ser a crédito
- Capture el cliente
- Capture los artículos y/o servicios adquiridos
- Antes de procesar, deberá hacer clic en [Aplicar Anticipos]

![IMG](img-aplant1.png)

- Se muestran los anticipos con saldo pendiente de aplicar
- Los anticipos mostrados corresponden a la misma divisa de la factura
- Seleccionar con una palomita el anticipo a aplicar y capture el importe a aplicar
- Puede aplicar varios anticipos a una misma factura
- Haga clic en [Aplicar]

![IMG](img-aplant2.png)

- Por último haga clic en el botón [Procesar] para grabar la factura

El sistema SAIT, al procesar, automáticamente va a elaborar la nota de crédito respectiva y hará las aplicaciones necesarias en el estado de cuenta del cliente para reflejar la cancelación y aplicación de anticipo.


{{%alert warning%}}El Uso del CFDI de la Nota de Crédito lo tomará del Catálogo de Clientes{{%/alert%}} 


![IMG](img-aplant3.png)

{{%alert danger%}} Nota: El tipo de cambio de la nota de crédito informado en el CFDI es el del día anterior, para efectos de contabilidad revisar su configuración o considerar el 
que su asesor fiscal le indique.{{%/alert%}} 

Una vez procesada la factura, en el estado de cuenta se generan los siguientes movimientos:

En donde podemos ver, se generó la NC automáticamente y se canceló el saldo del anticipo.

![IMG](img-edoctaant2.png)



