+++
title = "Apl. Ant. Manual"
description = "Guía para Emitir Facturas de Anticipos en CFDI 4.0"
weight = 3
+++

El sistema SAIT, al aplicar anticipos, tiene la opción de hacer en forma automática la nota de crédito o CFDI de egresos, pero es de uso general, 
si usted necesita otras opciones, la aplicación se puede hacer  en forma manual.

### I) Elaborar la factura de Anticipo tal y como lo indica en [Facturar Anticipo](/otros-temas/anticipos/facturar-anticipo/)

### 2) Elaborar la factura del bien, o de la mercancía 

- Ir al menú Ventas / Registro de Ventas
- Seleccione el tipo de documento: Factura
- La factura deberá ser a crédito
- Capture el cliente
- Capture los artículos y/o servicios adquiridos
- De clic en [procesar] para grabar la factura y se abrirá una ventana para que agregue los datos de forma y método de pago, uso de CFDI 

![IMG](ANTICIPO2.png)

- Deberá relacionar la factura del anticipo

![IMG](ANTICIPO3.png)

- Clic en continuar para procesar la factura, listo ya se procesó la factura ahora debe aplicar el anticipo.

### 3) Nota de Crédito

- Ir a Ventas / Devoluciones y nota de crédito
- En Forma seleccionar: EN EFECTIVO.
- Clic en el botón Capturar artículos.
- Agregue un comentario.
- En artículos seleccione el articulo "APLANTICIPO" con Descripción: Aplicación de anticipo y clave SAT: 84111506 .
- Con cantidad 1 y precio será el importe del anticipo por aplicar  antes de impuestos.
- De clic en procesar y se abrirá un ventana, en la cual deberá:
- Relacionar la factura del bien, es decir donde se factura la mercancía por la cual se hizo el anticipo.
- Clic en continuar para terminar de procesar el comprobante.

![IMG](img-aplantm2.png)

### 4) Aplicar ANTICIPO en el Estado de Cuenta

- Ir a cobranza / Registrar Pagos de los Clientes
- Seleccionamos el Clientes y en Forma de pago seleccionamos el concepto "APLI. ANTICIPO"
- En el campo folio de la ventana que esta enseguida de la forma de pago coloque el folio del anticipo
- En Importe del pago se nos cargara el total del anticipo, debe ingresar el total del pago que hara.
- Seleccionamos la factura y damos clic en [Procesar].

![IMG](img-aplantm4.png)

Con esto podemos ver que el estado de cuenta del cliente los movimientos realizados.

![IMG](img-aplantm3.png)

{{%alert danger%}} Nota: El tipo de cambio de la nota de crédito informado en el CFDI es el del día anterior, para efectos de contabilidad revisar su configuración o considerar el 
que su asesor fiscal le indique.{{%/alert%}} 


