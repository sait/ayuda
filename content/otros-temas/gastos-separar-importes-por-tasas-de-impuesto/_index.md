+++
title = "Definir variables para separar los importes Exentos, Gravados0, Gravados8, Gravados16, Iva8, Iva16"

description = ""
weight = 12
+++

A partir de la versión 2019.1.0 el módulo de gastos en SAIT ERP, cuenta con la opción de las variables para poder separar los importes que son gravados al 8% y por consecuencia poder separar el IVA al 16% y el IVA al 8%. 0% y Exento.

Esta nueva funcionalidad es de gran utilidad para poder registrar contablemente los gastos separando las diferentes TASAS de IVA.

Por ejemplo, este XML en donde tenemos dos tipos de IVA al 8 y 16

 
![IMG](1.png)

La variables que se venían manejando hasta antes de esta versión ( y que siguen vigentes para los casos que así lo requieran)  si registrábamos dichos gastos a partir del XML , el sistema solo tenía variables para subtotal e IVA y juntaba los dos IVA (40.59) 
 
![IMG](2.png)

A partir de la versión 2019.2.0 usted puede separar los impuestos por su tasa, en donde quedarían de la siguiente manera:


![IMG](3.png)


Para ello deberá modificar los asientos para contabilizar documentos de gastos en el módulo de  Gastos / Tipos de Gastos

En donde agregará las nuevas variables del IVA al 8%.

![IMG](4.png)

**NOTA: Si usted ya proceso varios cheques con gastos con el IVA junto deberá BORRAR estos cheques para posteriormente modificar los gastos y rehacer el cheque.**

Si separa las tasas desde un inicio será más sencillo llenar los datos a la hora de realizar la DIOT, en donde en recientes versiones ya se agrega el campo de 8%.

![IMG](5.png)

{{<youtube W3CbWG3oS4Y>}}
	