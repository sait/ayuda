+++
title="Nuevo Enlace de Sucursales SAIT Sync"
description=""
weight = 13
+++


Sait Sync es el nuevo sistema de enlace de sucursales de SAIT, se compone de dos elementos principales, un Servidor en la Nube y un Servicio Local instalado en la máquina servidor de casa sucursal. Los eventos quedarán guardados en la nube donde pueden ser consultados a través de un portal.

En este apartado encontraras los temas relacionados con SAIT Sync.

Selecciona uno de los temas que se encuentran en el listado para empezar.

{{% children  %}}