+++
title="Ubicaciones"
description=""
weight = 1
+++

Una ubicación, es sinónimo de sucursal o almacén en el sistema SAIT, dicho esto, es necesario crear una ubicación por cada sucursal en SAIT.

Dirigirse al apartado de Ubicaciones.

![IMG](menu-ubicaciones.png)

Da click en agregar nuevo y aparecera la ventana para registrar la ubicación.

![IMG](nueva-ubicacion.png)

Ingresar los datos de la ubicación y guardar.

![IMG](modal-nueva-ubicacion.png)


Una vez guardado, tiene la opción de elegir de que tipo de ubicación, las opciones disponibles son los siguientes.

{{%alert info%}} 
**Ubicación Normal:** Establecer una configuración default para todas las ubicaciones normales.

**Ubicación Especial:** Configurar eventos especificos a descargar en cada ubicación.

**Ubicación Concentradora** Configuración predeterminada que descarga todos los eventos.
{{%/alert%}} 


En la columna Apikey se encuentra la llave de acceso que se usara para dar acceso al Sync a las aplicaciones que lo requieran.

También en esta vista se puede verificar el estado del procesamiento de los eventos de esa ubicación, cual fue el último evento procesado, podemos recrear la llave (tendremos que cambiarla en la aplicación también), y por último podemos editar el nombre o borrar la ubicación.