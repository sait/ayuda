+++
title="SMTP"
description=""
weight = 5
+++

Configura las credenciales de SMTP para enviar correos en caso de que ocurra algún error.

Consulta la configuración de los proveedores de correo mas comunes.

https://support.office.com/es-es/article/configuraci%C3%B3n-de-correo-electr%C3%B3nico-pop-e-imap-para-outlook-8361e398-8af4-4e97-b147-6c6c4ac95353

![](smtp.png)
