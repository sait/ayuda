+++
title="Auditoria"
description=""
weight = 3
+++

Visualiza los eventos que han llegado al servidor de SAIT Sync, todos estos eventos serán redistribuidos a las aplicaciones que se les dio acceso desde el panel de ubicaciones. Se puede filtrar por tipo de evento, sucursal, usuario, etc.

![IMG](auditoria.png)

Al hacer clic en el ID del evento se desplegara una ventana con el XML del evento donde se puede ver toda la información que afecto a las tablas de la base de datos.

![IMG](evento.png)
