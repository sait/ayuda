+++
title="Esquema personalizado"
description=""
weight = 5
aliases = [
          "otros-temas/sait-sync/server/esquema/",
	     ]
+++


En caso de que su empresa cuente con desarrollos especiales que requieran de campos y/o tablas que no estén incluidas en el sistema base. Esto puede ocasionar errores al procesar los eventos recibidos. Para esto se debe utilizar la herramienta para generar un esquema personalizado de la base de datos de su empresa. La generación de esquemas consta de los siguientes pasos.

Descargar la herramienta.

https://sait.mx/download/sait-sync/esquemajson.zip

Descomprimir el contenido del archivo zip dentro de la empresa con desarrollos especiales.

![IMG](descomprimido.png)

Ejecutar el archivo esquemajson.exe que se encuentra dentro de la carpeta descomprimida.

![IMG](ejecutar-esquema.png)

Se genera un archivo con el nombre esquemasait.json

![IMG](esquema-generado.png)

Dirigirse a la sección de esquemas dentro del portal de SAIT Sync.

![IMG](seccion-esquemas.png)

Buscar el archivo generado en el equipo y subirlo.

![IMG](subir-esquema.png)

{{%alert warning%}}
IMPORTANTE: Reiniciar los servicios de SAIT Sync cliente después de subir esquema para que se actualice la base de datos.
{{%/alert%}}
