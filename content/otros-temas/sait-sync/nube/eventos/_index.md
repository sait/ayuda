+++
title="Catálogo de Eventos"
description=""
weight = 2
+++

Muestra los eventos que puede aceptar el enlace, en esta sección podemos ver los eventos por defecto que tiene SAIT, puede filtrar por categoría de eventos y hacer búsquedas. También permite agregar eventos especiales que tenga la empresa, dando clic en "Agregar Nuevo".

![IMG](catalogo-eventos.png)


