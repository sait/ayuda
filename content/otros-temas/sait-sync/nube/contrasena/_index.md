+++
title="Contraseña"
description=""
weight = 4
+++

En la Sección Admin se puede cambiar el usuario y la contraseña del administrador del servidor.

{{% alert warning %}}
El correo que se configure es al que se le van a mandar avisos cuando ocurra algún error. Para que funcione correctamente asegúrese de configurar SMTP.
{{% /alert %}}

![IMG](menu-password.png)

Llenar el formulario para cambiar las credenciales.

![IMG](formulario.png)

