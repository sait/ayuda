+++
title="Servidor en la Nube"
description=""
weight = 1
+++

En este apartado se encuentran los temas relacionados con la configuración del Servidor en la nube de SAIT Sync de una empresa.

![IMG](sait-sync.png)

{{% children  %}}