+++
title="Actualización"
description=""
weight = 2
+++

Para actualizar la versión de SAIT Sync debe seguir los siguientes pasos.

Descargar los ejecutables desde el siguientes enlaces.

https://sait.mx/download/sait-sync/saitsync.exe

https://sait.mx/download/sait-sync/saitsync-eventos.exe

https://sait.mx/download/sait-sync/saitsync-http.exe

Descargar la dll de SAITX.

https://sait.mx/download/sait-sync/saitx.dll

Abrir los servicios de Windows y ubicar el servicio de SAIT Sync.

![IMG](servicios-windows.png)

Detener el servicio.

![IMG](detener-servicio.png)

Abrir en el explorador de archivos de Windows la carpeta donde está instalado SAIT Sync. Normalmente es C:\Program Files (x86)\SAIT Sync

![IMG](carpeta-sait-sync.png)

Copiar los archivos descargados saitsync.exe, saitsync-eventos.exe, saitsync-http.exe y saitx.dll reemplazando los anteriores.

{{% alert warning %}}
Asegurarse que se encuentren los archivos msvcr71.dll, vfp9renu.dll y vfp9t.dll. Si no están descargarlos y agregarlos al folder. Se pueden encontrar en el siguiente enlace. 

https://sait.mx/download/sait-sync/dlls-fox.zip
{{% /alert%}}

Abrir un ```cmd como administrador``` y cambiarse de directorio a donde está instalado SAIT Sync.

![IMG](cmd-admin.png)

Ejecutar el siguiente comando como administrador para registrar la dll como servicio.

```sh
regsvr32 saitx.dll
```

Debe salir el siguiente mensaje.

![IMG](dll-instalada.png)

Iniciar el servicio de SAIT Sync.

![IMG](iniciar-sait-sync.png)
