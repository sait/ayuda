+++
title="Bitácora de Cambios SAIT Sync"
description=""
weight = 99
+++

Bitácora de mejoras y correcciones al SAIT Sync

Versiones 3. X

| Versión | Fecha | Descripción |
| ------- | ----------- | ----------------------------------------------------------------------------------------------------------------------- |
| 3.5.0  | 19-Jun-2024 | Mejora. SyncLinux, se agrega la acción "Unificar Artículos" |
| 3.4.0  | 01-Nov-2022 | Mejora. Se agrega la depuración de tabla eventosv2, para liberar espacio. |
| 3.3.11 | 01-May-2021 | Mejora. Se integra plugin de webhooks para sync linux |
| 3.3.10 | 29-Abr-2021 | Corrección. No procesaba los eventos de kits correctamente, se duplicaban o no se eliminaban. (#28) |
| 3.3.9 | 26-Abr-2021 | Mejora. Reportar error en el panel de sait sync cuando no se pueda iniciar correctamente. (#27) |
| 3.3.8 | 31-Mar-2021 | Corrección. No sincronizaba direcciones de envío correctamente. (#25) |
| 3.3.7 | 17-Feb-2021 | Corrección. Se soluciona un problema al procesar el costopro en algunos eventos. |
| 3.3.6 | 11-Feb-2021 | Mejora. Se evalúa el campo costopro y si es menor a cero (negativo) se establece en cero. |
| 3.3.5 | 03-Feb-2021 | Corrección. Se perdían eventos cuando el servicio intentaba arrancar pero no había. |
| 3.3.4 | 29-Ene-2021 | Mejora. Cuando ocurren errores los reintenta cada minuto. |
| 3.3.4 | 29-Ene-2021 | Mejora. Se imprime el numero de versión en los logs. |
| 3.3.4 | 29-Ene-2021 | Mejora. Se cambian las fechas de los nombres de logs, se quitan los guiones. |
| 3.3.3 | 01-Dic-2020 | Mejora. Crear indices para la tabla eventosv2. |
| 3.3.3 | 01-Dic-2020 | Mejora. Sí no se utiliza mariadb al insertar eventos como ya procesados. |
| 3.3.3 | 01-Dic-2020 | Mejora. Se insertan eventos dentro de una transacción. |
| 3.3.3 | 01-Dic-2020 | Mejora. Se cambia el tiempo de espera de sincronización a 3 segundos. |
| 3.3.2 | 05-Sep-2020 | Corrección. Se soluciona un problema que hacía que el servicio consuma memoria gradualmente. |
| 3.3.1 | 04-Sep-2020 | Corrección. Se soluciona un problema al crear instancias de saitx. |
| 3.3.0 | 03-Sep-2020 | Mejora. Se generan salidas al inventario para el evento 'ADDNOTAREM' |
| 3.2.5 | 12-Ago-2020 | Corrección. Solamente generaba la existencia global para el primer artículo del evento ACTEXIST. |
| 3.2.5 | 04-Ago-2020 | Corrección. No procesaba correctamente eventos especiales cuando el tipo de evento tenía letras en minúscula. |
| 3.2.4 | 17-Jul-2020 | Corrección. Se corrige un problema que marcaba error al procesar el saldo de los clientes y proveedores. |
| 3.2.3 | 02-Jul-2020 | Mejora. Siempre se procesan los eventos en MariaDB en linux. |
| 3.2.2 | 23-Jun-2020 | Mejora. Logs de errores mas limpios. |
| 3.2.2 | 23-Jun-2020 | Mejora. Se cambia el formato de los logs de eventos procesados. |
| 3.2.1 | 22-Jun-2020 | Mejora. Se cambia el formato de los logs de eventos procesados. |
| 3.2.1 | 22-Jun-2020 | Corrección. El servicio http se colgaba cuando se rompían los pipelines. |
| 3.2.1 | 22-Jun-2020 | Mejora. Se insertan los eventos de forma asíncrona. |
| 3.2.1 | 22-Jun-2020 | Corrección. Se soluciona un problema que no detenía correctamente los procesos al detener el servicio. |
| 3.2.0 | 20-Jun-2020 | Mejora. Se desactiva por default el procesamiento de eventos en MariaDB. |
| 3.1.0 | 19-Jun-2020 | Corrección. Marcaba error al procesar traspasos grandes. |
| 3.1.0 | 19-Jun-2020 | Mejora. Si los procesos se detienen los vuelve a iniciar en 5 segundos. |
| 3.1.0 | 19-Jun-2020 | Mejora. Se separan los procesos de recibir eventos de las estaciones y procesar eventos de otras ubicaciones. (#731) |
| 3.0.3 | 18-Jun-2020 | Corrección. Se corrige un problema que hacía que logs logs se encimaran. |
| 3.0.3 | 18-Jun-2020 | Mejora. Se usa exclusión mutua (mutex) al procesar evento para evitar conflictos. |
| 3.0.2 | 17-Jun-2020 | Mejora. En la auditoria de procesar eventos registra cuando empieza y cuando termina. |
| 3.0.2 | 17-Jun-2020 | Mejora. En los logs ahora se muestran los segundos. |
| 3.0.2 | 17-Jun-2020 | Mejora. Se crea una instancia de saitx cada que se va a procesar eventos. (#731) |
| 3.0.1 | 08-Jun-2020 | Corrección. Se utiliza exclusión mutua para el acceso a las rutinas, esta posiblemente sea la causa de la detención del servicio. |
| 3.0.0 | 29-May-2020 | Mejora. Se agrega soporte para multiples plugin por empresa. |

Versiones 2. X

| Versión | Fecha | Descripción |
| ------- | ----------- | ----------------------------------------------------------------------------------------------------------------------- |
| 2.0.2 | 19-May-2020 | Mejora. Notificar a SAIT Sync ws cuando no pueda enviar eventos. |
| 2.0.1 | 18-May-2020 | Corrección. Vaciaba la tabla de cfdi mensual cuando procesaba facturas. |
| 2.0.1 | 18-May-2020 | Mejora. Sí no puede descargar el esquema de sait.mx trabaja con el esquema custom. |
| 2.0.0 | 13-May-2020 | Corrección. No se procesaban correctamente los eventos ModKit y DelKit. |
| 2.0.0 | 13-May-2020 | Mejora. Se actualiza el saldo de proveedores en cuentas por pagar. |
| 2.0.0 | 13-May-2020 | Mejora. Se actualiza el saldo de clientes cobranza con eventos. |
| 2.0.0 | 13-May-2020 | Mejora. Se Actualiza existencia global de artículos con eventos. |
| 2.0.0 | 13-May-2020 | Corrección. No procesaba correctamente los eventos de CFDIS cuando tenían xml grandes. |
| 2.0.0 | 13-May-2020 | Mejora. El procesamiento de DBFs se hace con VFP9 a tráves de Activex. (#684) |

Versiones 1. X

| Versión | Fecha | Descripción |
| ------- | ----------- | ----------------------------------------------------------------------------------------------------------------------- |
| 1.21.0 | 05-Mar-2020 | Corrección. Se enviá correctamente la referencia del evento. |
| 1.20.4 | 21-Feb-2020 | Mejora. Cuando se procesa un evento y ocurre algún detalle hace un debug en un archivo. |
| 1.20.3 | 20-Feb-2020 | Corrección. No actualizaba el estatus en el portal de SAIT SYNC correctamente, hasta que llegara el siguiente evento. (#684) |
| 1.20.2 | 31-Ene-2020 | Corrección. No procesaba de forma correcta los eventos de la tabla cfdiaamm. |
| 1.20.1 | 31-Ene-2020 | Corrección. No procesaba de forma correcta los eventos de la tabla promo. |
| 1.20.0 | 29-Ene-2020 | Mejora. Se elimina archivos de logs mayores a 72 días.|
| 1.20.0 | 29-Ene-2020 | Mejora. Se agregó log para eventos procesados.|
| 1.20.0 | 29-Ene-2020 | Mejora. Se calcula la existencia global para los eventos ‘ACTEXIST’.|
| 1.19.2 | 21-Ene-2020 | Corrección. Al procesar tipos de datos booleanos siempre los marcaba en false.|
| 1.19.0 | 16-Dic-2019 | Mejora. Se agrega soporte para plugins, esto permite agregar desarrollos especiales como en usrevent.fxp.|
| 1.18.1 | 09-Dic-2019 | Mejora. Se espera has 5 minutos para iniciar cada ubicación, en caso de que el internet o mariadb no estén disponibles.|
| 1.18.1 | 02-Dic-2019 | Corrección. No procesaba eventos que usaban la tabla de CIA.|
| 1.18.1 | 02-Dic-2019 | Corrección. No generaba correctamente los esquemas de la tabla cfdi, esto impedía iniciar el servicio.|
| 1.18.0 | 29-Oct-2019 | Mejora. Obtiene el esquema base de sait.mx.|
