+++
title="Configuración"
description=""
weight = 3
+++


{{%alert warning%}}
La configuración solo debe hacerse en la máquina servidor de la sucursal, las demás estaciones solo deberán reiniciar SAIT para aplicar la configuración, hay que tener en cuenta que el enlace anterior dejará de funcionar.
{{%/alert %}}

En la máquina servidor, abrir SAIT y dirigirse a Utilerías / Configuración General del Sistema.

![IMG](configurar-sistema.png)

Dentro de Configuración dirigirse a SAIT Distribuido / Configurar SAIT Sync

![IMG](configurar-sait-sync.png)

{{%alert warning%}}
IMPORTANTE: Si no está instalado y ejecutándose el servicio SAIT Sync, no se abrirá la ventana.
{{%/alert%}}

Capturar en la ventana los siguientes datos.

![IMG](capturar-datos.png)

**URL servicio SAITSync en la nube:** Es el dominio de la empresa para SAIT Sync, se le facilita al contratar el servicio, sin la diagonal al final y con el protocolo, por ejemplo https://provlimpieza.saitsync.com

**Llave de acceso de Sucursal/Ubicación (APIKEY):** Se genera al crear una ubicación en el portal de SAIT Sync.

![IMG](copiar-apikey.png)


**IP servidor con la instalación de servicio SAIT Sync:** Es la dirección IP o nombre del host de la máquina, se recomienda configurar una IP fija para el servidor, en caso de que sea solo una máquina sin servidor puede utilizar localhost, sin la diagonal al final y no debe capturar el procotolo (http://) por ejemplo: 192.168.0.122

**Directorio de la empresa:** Es el directorio de la empresa accesible desde el servidor, se puede utilizar discos conectados al servidor por ejemplo: C:\SAIT\DEMO o E:\EMPRESAS\PROVLIMPIEZA. NO puede utilizar rutas de red por ejemplo: \\\\SAIT\DEMO o \\\\EMPRESAS\PROVLIMPIEZA


Una vez configurada se ve de la siguiente forma.

![IMG](datos-capturados.png)


Dar clic en ```Probar enlace Web``` esto hará una conexión con el servidor y validara la ubicación. El acceso a SAIT Sync es validado a través de la llave y el RFC de la empresa.

Si todo salió bien mostrara el mensaje "La ubicación ha sido creada".

![IMG](ubicacion-creada.png)

Dar clic en ```Guardar``` y reiniciar SAIT para aplicar los cambios.


{{%alert warning%}}
Una vez configurada debe reiniciar SAIT en las estaciones y reiniciar el servicio de SAIT Sync. Hacer una prueba agregando un artículo para asegurarse que este funcionando correctamente. El evento debe aparecer en la [Auditoría](/otros-temas/sait-sync/nube/auditoria/) del portal de SAIT Sync.
{{%/alert %}}
