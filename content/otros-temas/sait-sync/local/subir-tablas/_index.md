+++
title="Herramientas para subir tablas"
description=""
weight = 6
+++

Este proceso nos permite subir toda la información contenida en una tabla de SAIT a SAIT NUBE

Primeramente, deberá descargar la herramienta con el siguiente link https://sait.mx/download/sait-nube/enviarnube.zip


Posteriormente deberemos descomprimir el contenido de la carpeta y entramos a la carpeta llamada **empresa** y tomaremos los archivos llamados 

* enviarnube.SCT
* enviarnube.SCX

Y los colocaremos dentro de la carpeta de la empresa, en mi caso la ruta de la empresa se encuentra aquí: 

![IMG](RUTA.png)

Si no conoce la ruta de su empresa, abra su sistema SAIT y de clic en Catálogo de Empresa y en la columna directorio se le indica en dónde están guardados los archivos de su empresa.

![IMG](EMPRESA.png)

Posteriormente copiaremos los archivos de la carpeta **sait** 

![IMG](CARPETA-SAIT.png)

Y los pegaremos dentro de la instalación de SAIT en mi caso es en 

C:\SistemaSAIT\SAIT

Imporante, puede ser que algunas librerías ya existan por lo que si le pide reemplazar archivos deberá primero cerrar SAIT y dar clic Sí para reemplazar

Finalmente abriremos nuestro sistema SAIT al menú de Utilerías / Ejecutar Módulos Especiales y damos doble clic en Subir datos a SAIT Nube

![IMG](EJECUTAR.png)

Se abrirá la siguiente ventana que es la ventana de inicio de sesión de SAIT NUBE y capuraremos los datos del Dominio, Usuario y Contraseña 

Importante mencionar que al link se le deberá agregar el **.saitnube**, quedando de esta forma

https://provlimpieza.saitnube.com

![IMG](EJECUTAR2.png)

Si todo está correcto nos aparecerá un mensaje como este a la derecha

![IMG](EJECUTAR3.png)

Ahora solo teclearemos el nombre de la tabla que deseamos subir, ejemplo SatCerti y damos clic en Enviar, si el proceso se completó exitosamente se mostrará un mensaje así

![IMG](EJECUTAR4.png)

Hacer esto mismo con las tablas necesarias

**Si fue un proceso de subir tablas por motivo de actualización de sellos digitales con un autofacturador, deberá informar al área de soporte para nosotros finalizar el proceso**