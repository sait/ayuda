+++
title="Instalación"
description=""
weight = 1
+++

Los requerimientos mínimos para utilizar SAIT Sync son los siguientes:

* Windows 7 o superior, Windows Server 2008 R2 o superior
* SAIT versión 2020.13.0 o superior

Descargar el instalador desde el siguiente enlace:

https://sait.mx/download/sait-sync/saitsync-installer.exe

{{%warning%}}
En una sucursal solo se debe instalar en el equipo servidor, si solo se tiene un equipo, instalarlo en el mismo.
{{%/warning%}}

Ejecutar archivo y aceptar el acuerdo de licencia.

![IMG](aceptar-acuerdo.png)

Dar clic en el botón instalar.

![IMG](instalar-programa.png)

El proceso debe tardar pocos segundos.

![IMG](finalizar-instalacion.png)

{{%warning%}}
* Se recomienda agregar una excepción al directorio C:\Program Files (x86)\SAIT Sync en el antivirus para evitar errores.
* Debe actualizar después de instalar.
{{%/warning%}}

