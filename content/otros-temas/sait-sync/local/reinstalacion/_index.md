+++
title="Reinstalación de servicio SAIT Sync"
description=""
weight = 4
+++

En caso de ser necesario reinstalar localmente el servicio de SAIT Sync en la sucursal es necesario realizar los siguientes procesos para no perder el último evento recibido de dicha ubicación.

Para realizar el proceso correcto deberá seguir las siguientes instrucciones:

1.Entrar a la ruta C:\Program Files (x86)\SAIT Sync y hacer respaldo de los archivos: config.json y status

![IMG](1.png)

2.Desinstalar el SAIT Sync desde el Panel de control\Programas\Programas y características

![IMG](2.png)

3.Entramos de nuevo a la ruta C:\Program Files (x86) y borramos por completo la carpeta de SAIT Sync}

![IMG](3.png)

4.Corremos el instalador descargándolo de esta ruta:

https://sait.mx/download/sait-sync/saitsync-installer.exe

Ejecutar archivo y aceptar el acuerdo de licencia.

![IMG](aceptar-acuerdo.png)

Dar clic en el botón instalar.

![IMG](instalar-programa.png)

El proceso debe tardar pocos segundos.

![IMG](finalizar-instalacion.png)

5.Detener servicio SAIT Sync

![IMG](4.png)

6.Copiar los archivos config.json y status que previamente respaldamos y los pegamos en la ruta: 

C:\Program Files (x86)\SAIT Sync

![IMG](reemplazar.png)

7.Descargar los ejecutables desde el siguientes enlaces.

https://sait.mx/download/sait-sync/saitsync.exe

https://sait.mx/download/sait-sync/saitsync-eventos.exe

https://sait.mx/download/sait-sync/saitsync-http.exe

Descargar la dll de SAITX.

https://sait.mx/download/sait-sync/saitx.dll

Abrir los servicios de Windows y ubicar el servicio de SAIT Sync.

![IMG](servicios-windows.png)

Detener el servicio.

![IMG](detener-servicio.png)

Abrir en el explorador de archivos de Windows la carpeta donde está instalado SAIT Sync. Normalmente es C:\Program Files (x86)\SAIT Sync

![IMG](carpeta-sait-sync.png)

Copiar los archivos descargados saitsync.exe, saitsync-eventos.exe, saitsync-http.exe y saitx.dll reemplazando los anteriores.

{{% alert warning %}}
Asegurarse que se encuentren los archivos msvcr71.dll, vfp9renu.dll y vfp9t.dll. Si no están descargarlos y agregarlos al folder. Se pueden encontrar en el siguiente enlace. 

https://sait.mx/download/sait-sync/dlls-fox.zip
{{% /alert%}}

Abrir un ```cmd como administrador``` y cambiarse de directorio a donde está instalado SAIT Sync.

![IMG](cmd-admin.png)

Ejecutar el siguiente comando como administrador para registrar la dll como servicio.

```sh
regsvr32 saitx.dll
```
Debe salir el siguiente mensaje.

![IMG](dll-instalada.png)

Iniciar el servicio de SAIT Sync.

![IMG](iniciar-sait-sync.png)

8.Reiniciar servicio SAIT Sync.


