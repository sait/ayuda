+++
title="Servicio Local"
description=""
weight = 2
+++


En este apartado se encuentran los temas relacionados a la instalación y configuración del servicio de sait sync en una ubicación.

{{% children  %}}

![IMG](servicio-local.png)
