+++
title = "Cambiar IVA al 8% de manera masiva por medio de Excel"
description = ""
weight = 8
+++


<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="https://www.youtube.com/watch?v=dkNdIN7nWzs&feature=youtu.be" target="_blank">
<img src="/img_video_tutorial.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-play-circle"></i></div>
</div>
<h4 class="title">Ver Video Tutorial</h4>
</a>
</div>

<div class="card">
<a href="aplicar-iva-8-porciento.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>


</div>
<br>
<hr>
<div id="indicaciones"></div>



<h3 class="text-primary">PASO 1:</h3>

Solicitud de Estímulo Fiscal


<h3 class="text-primary">PASO 2:</h3>

Actualizar versión del Sistema

https://ayuda.sait.mx/otros-temas/descargar-version/


<h3 class="text-primary">PASO 3:</h3>

Exportar a Excel su catálogo de artículos

* Diríjase a Inventario / Reporte de Artículos / Reporte Precios Normales
* De clic en Hoja


![IMG](1.png)

* Se mostrará el reporte de esta manera:

![IMG](2.png)


* Para agregar la columna de Impuesto1 (columna de IVA) deberá habilitar la opción de Diseño de consultas en el Menú de Utilerías / Barras de Herramientas / Área de Trabajo

![IMG](3.png)

* De tal manera que en el reporte de Artículos de muestre la siguiente opción: 

![IMG](4.png)

* Ahora en el reporte podrá agregar la Columna de Impuesto1 (o IVA) para ello de clic en la siguiente opción 

![IMG](5.png)
 
* Y se abrirá la siguiente ventana en dónde localizará el campo Porcentaje de Impuesto 1 y dará clic en Insertar.

![IMG](6.png)

* De esta manera la columna se agregó al reporte y estamos listos para exportar la información a Excel dando clic en el ícono indicado.

![IMG](7.png)

<h3 class="text-primary">NOTA</h3>

Si usted desea actualizar otros datos además del IVA deberá agregar dichos datos al reporte de la misma manera.


* Una vez que la información esté en nuestro archivo de Excel podemos empezar a trabajar en el.


![IMG](8.png)


* Una vez actualizado el dato del IVA deberá dejar el archivo solo con los datos que vamos a importar al sistema, quitando descripción, línea, divisa, encabezados del reporte, etc, dejándolo de la siguiente manera:


![IMG](9.png)

* Al Guardarlo es necesario lo guarde con el formato Libro de Microsoft Excel 5.0/95(*.xls)

![IMG](10.png)

* Cerramos el archivo y nos dirigimos a SAIT y antes de hacer algún movimiento realizamos un respaldo de información.

* Finalmente debemos importar la información, para realizar este proceso diríjase a Utilerías / Importar Información desde / Excel

![IMG](11.png)

* Se nos mostrará la siguiente ventana en dónde deberemos seleccionar la opción de Artículos y daremos clic en Siguiente

![IMG](12.png)


* Localizaremos el campo de Impuesto1 y daremos clic en Agregar y después clic en Siguiente >>

![IMG](13.png)

* Aparecerá la siguiente ventana en donde localizaremos en archivo con la información a importar dado clic en el botón de ?

![IMG](14.png)

* Localizamos el archivo, en mi caso lo dejé en Escritorio, lo seleccionamos y damos clic en Abrir

![IMG](15.png)

* Daremos clic en Siguiente >>

![IMG](16.png)

* Verificamos la información y damos clic en Final

![IMG](17.png)

* Confirmamos dando Clic en Sí

![IMG](18.png)
 
* Espere se complete el proceso

![IMG](19.png)


* Al finalizar el sistema mostrará el siguiente mensaje

![IMG](20.png)

* Ahora podemos verificar la información en el Catálogo de Artículos y Servicios.
En la siguiente imagen podemos ver cómo se afecta el precio púbico antes y después de modificar el IVA al 8%

![IMG](21.png)

<h3 class="text-primary">NOTA</h3>

Si usted lo que desea es modificar el Precio Público a cierta cantidad deberá importar desde Excel la columna de Precio1, o si usa más listas de precio también las deberá actualizar estos campos además de los campos CLAVE e IMPUESTO1.


