+++
title = "Complemento IEDU"
description = ""
weight = 11
+++

Opción disponible a partir de la versión 2018.15.0

<h3 class="text-primary">PASO 1: Activar Complemento de Instituciones Educativas</h3>

1.Ir a Utilerías / Factura Electrónica / Configurar CFDI

2.Se nos presentara el formulario 'Configuración de Factura Electrónica'

3.Activar casilla ‘Usar Complemento IEDU’

![IMG](1.png)

4.Nos mostrará un aviso donde se nos comenta que deberemos actualizar la base de datos para usar este complemento correctamente. Clic en [Aceptar] en el mensaje

![IMG](2.png)


5.Ingresar la Clave de Centro de Trabajo de la institución educativa (este dato lo proporciona la Institución)

**NOTA: En caso de que su institución educativa maneje mas de una Clave, deberá crear una nueva empresa donde pueda configurar la Clave adicional.**

![IMG](3.png)

6.Clic en [Guardar configuración]

7.Ir a Utilerías / Actualizar Tablas de Base de Datos

8.Se recomienda hacer un respaldo antes de realizar este proceso, para ello, clic en [Respaldar información]

9.Clic en [Actualizar base de datos]

10.Aceptamos los mensajes que nos aparecen y listo, se crearán los campos necesarios, y nuestro sistema está preparado para usar el complemento de instituciones educativas 

![IMG](4.png)

<h3 class="text-primary">PASO 2: Configurar conceptos aplicables a Complemento IEDU</h3>

1.Ir a Inventario / Catálogo de Artículos y Servicios

2.Se nos presentara el formulario ‘Catálogo de Artículos y Servicios’

3.Ingresar los datos del concepto, artículo o servicio que será aplicable para el uso de este complemento

4.Clic en la pestaña ‘Otros Datos’

5.En el campo ‘Complemento IEDU’, con la barra espaciadora, seleccionar ‘SI’

![IMG](5.png)

6.Clic en [Grabar]

<h3 class="text-primary">PASO 3: Configurar datos de alumno </h3>

1.Ir a Ventas / Clientes

2.Se nos presentara el formulario ‘Catálogo de Clientes’

3.Ingresar los datos del padre o tutor como si fuera un cliente habitual del sistema

4.Clic en la pestaña ‘Otros Datos’

5.Ingresar **Nombre de Alumno, Apellido Paterno, Apellido Materno, CURP de Alumno y seleccionar el Nivel Educativo al que pertenece el alumno**

![IMG](6.png)

6.Clic en [Grabar]

7.Listo, desde ahora, cuando realicemos una factura a este cliente (padre o tutor) en nuestros CFDIs se adjuntaran los datos del alumno


<h3 class="text-primary">PASO 4: Realizar factura con Complemento IEDU </h3>


1.Ir a Ventas / Registrar Ventas

2.Se nos presentara el formulario ‘Documentos de Venta’

3.En los campos referente a ‘Tipo’, seleccionar ‘Factura’, Contado o crédito, y la divisa correspondiente

4.En ‘Cliente’, ingresar un cliente (padre o tutor) que tenga registrado los datos del alumno

5.Capturar los datos de la factura como habitualmente se realiza

6.Capturar algún artículo o servicio que aplique para el complemento IEDU

![IMG](7.png)


7.Clic en [Procesar = F8] para emitir el CFDI

8.Una vez ya emitido el CFDI con Complemento IEDU, al generar un PDF en la sección de los conceptos podremos apreciar los datos referentes a este complemento, así como los datos del alumno

![IMG](8.PNG)


{{<youtube 3HzrJw3Ix6A>}}

