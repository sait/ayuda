+++
title="Complemento Carta Porte 3.1"
description=""
weight = 20
+++

La nueva versión para el complemento Carta Porte versión 3.1 entrará en vigor el 17 de junio de 2024 y en SAIT ya estamos preparados para dicho cambio, para poder utilizarla deberá contar con lo siguiente:

- **1) Contar con una versión igual o mayor a la 2024.20** 

	Para comprobar la versión que tiene instalada, puede ir a Ayuda / Acerda de 

![IMG](acerda-de.png)

- **2) Colocar la versión 3.1 de Carta Porte**

	Vaya a Utilerías / Factura Electrónica / Configurar CFDI / y colocar la versión 3.1

![IMG](version.png)

Si es la primera vez que emite Carta Porte, de clic [aquí](/otros-temas/cfdi-de-traslado/)

Si la primera vez que actualiza versión, de clic [aquí](/sait-erp/1-instalacion-y-activacion-licencias/b-actualizar-version-de-sistema/)