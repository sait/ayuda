+++
title = "Checador de Precios"
description = ""
weight = 3
+++

El checador de precios es un producto de SAIT que le puede ayudar a aumentar sus ventas otorgándole la facilidad a sus clientes de verificar los precios desde el pasillo de su tienda.

### Requerimientos

Dispositivo con Sistema Operativo Windows CE.NET 5.0 y con Escáner incluido.

La instalación y configuración consta de los siguientes pasos:

1. [Instalar SAIT API en el servidor.](#instalar-sait-api-en-el-servidor)
1. [Configuración de Dispositivo (MK500).](#configuración-de-dispositivo-mk500)
1. [Instalar Aplicación en Dispositivo.](#instalación)
1. [Solución de Problemas.](#solución-de-problemas)
1. [App Android](#aplicación-android)
1. [Configuración de Dispositivo ANDROID (CC600).](#configuración-de-dispositivo-android-cc600)
1. [Instalar Aplicación en Android](#instalación-android)
1. [Configurar Aplicación](#configurar-aplicación)
1. [Galeria de imagenes](#galeria-de-imagenes)
1. [Configurar modo Kiosko](#configurar-modo-kiosko)


## Instalar SAIT API en el Servidor

Para poder hacer la comunicación entre el checador de precios y el servidor es necesario instalar esta API en una computadora que tenga previamente instalado SAIT con los códigos de barras ya capturados en los artículos, link de descarga: https://sait.mx/download/sait-api-instalador.exe

* Abrir el instalador de SAIT API
![instaladorAPI](22.png)

* Aceptar los términos y condiciones, después dar clic en siguiente. Seleccionar el directorio de instalación.
![Directorio](23.png)

* Si le indica que la carpeta ya existe dar clic en “SI”, dar clic en Instalar.
![Confirmar](24.png)

* Una vez que termine la instalación se abrirá la ventana de configuración del API.
![VentanaConf](25.png)

* Primero seleccionamos el directorio donde tenemos instalado SAIT.
![DirSAIT](26.png)

* Luego elegimos una empresa de la lista.
![DirEmpresa](27.png)

* Después indicamos la sucursal (en caso de no tener no hay ningún problema).
![Sucursal](28.png)

* Luego el usuario con el que se harán las consultas.
![User](29.png)

* El apartado de correo no es necesario para el checador, dar clic en guardar y cerrar la configuración.

{{%alert warning%}}IMPORTANTE: Asegurarse de que ningún firewall o antivirus este bloqueando el puerto donde está trabajando el API.{{%/alert%}}
{{%alert warning%}}Es recomendado que este servidor tenga una IP estática debido que cuando cambia el checador la busca y no la encuentra provocando errores.{{%/alert%}}

## Configuración de Dispositivo (MK500)
### Calibración

* La primera vez que se enciende aparece una pantalla donde se debe calibrar el táctil. Realizar la configuración y guardar tocando la pantalla como se indica.
![Calibracion](1.png)

* Una vez realizado presionar en la pantalla para guardar la calibración.
![CalibracionGuardada](2.png)

### Activar Escáner
Se debe activar el escáner y mantenerlo activado para el uso de la aplicación. 

* En la barra de tareas aparece un icono de código de barras llamado `DataWedge`. 
![Barra](3.png)

* Dar clic sobre el – Basic Configuration. 
![Basic](4.png)

* Abrirá una página en el navegador de internet, seleccionar la opción Barcode input.
![BarcodeInput](5.png)

* Seleccionar el dispositivo (normalmente es el 1).
![BlockBuster](6.png)

* habilitar la opción “Enabled”.
![Enable](7.png)

* Abrir la opción “Auto trigger”.
![AutoTrigger](8.png)

* Habilitar la opción “Enabled”.
![EnableTrigger](9.png)

* Dar clic en “0. Back” para regresar hasta llegar a la página principal y salir del navegador dando clic en "0. Exit".
![FinishScanner](10.png)

* El escáner está activado.
![Activado](11.png)


## Instalación

El dispositivo de Windows CE viene “congelado” por lo que se tendrá que realizar un procedimiento especial para instalar la aplicación y esta quede persistente. El objetivo es que al momento de reiniciar el dispositivo la aplicación se ejecute automáticamente y escanear precios sin problemas. Además de poder realizar conexiones remotas.


### Instalar desde la PC

* Conectar el dispositivo a la PC usando un cable USB, debe estar conectado a la corriente y a la computadora. NOTA: Sí el dispositivo no es detectado consulte [como solucionar la conexión a a la PC.](#la-pc-no-detecta-el-dispositivo)
![ConectadoaPC](30.png)

* En la carpeta "\" copiar el archivo `StartUpCtl.CAB`.
![TransferirStartPC](31.png)

* Realizar la instalación de la Aplicación copiada directamente desde el dispositivo. Para ello debemos abrir `My Device` desde el escritorio.
![AbrirDevice](32.png)

* Localizar el archivo que se copió e instalarlo dando doble clic sobre él y realizar la instalación.
![InstalarSTRUPCTL](33.png)

{{%alert warning%}}**IMPORTANTE:** No cambiar la ruta de instalación del programa.{{%/alert%}}
![rutaPrograma](13.png)

Ahora debemos configurar el StartUpCtl para que instale el framework e inicie nuestro checador cada que se encienda el dispositivo.

* Debemos copiar en la carpeta `Application\StartUpCtl\OnRestore\` el archivo `OnRestore.txt` que contiene las instrucciones para instalar lo necesario y ejecutar la aplicación automáticamente al encender el dispositivo.
![OnRestorePC](34.png)

{{%alert warning%}}**NOTA:** Sí ya hay un archivo “OnRestore.txt” reemplazarlo.{{%/alert%}}

* El siguiente paso es copiar los archivos `NETCFv2.wce5.armv4i.cab` e `INSTALLLS.CAB` en la carpeta de "Application".
![CopiarPC](35.png)

{{%alert info%}}El archivo “NETCFv2.wce5.armv4i.cab” son librerías necesarias para el funcionamiento, el segundo archivo “INSTALLLS.CAB” es la aplicación de checador.{{%/alert%}}

* Para poder realizar conexiones remotas al checador es importante transferir los archivos `vncconfig.exe` y `winvnc.exe` que se encuentran en VNC a la carpeta de "Application". NOTA: Sí desea realizar una conexión remota al dispositivo consulte [como conectarse remotamente.](#conectarse-remotamente-al-checador)
![VNC](41.png)

{{%alert info%}}El archivo "vncconfig.exe" establecerá la configuración de conexión del servicio VNC y "winvnc.exe" permitirá conectarse desde una PC de la red local.{{%/alert%}}

El dispositivo contiene una aplicación llamada App Launcher que viene preinstalada de fabrica la cual inicia automáticamente cada que se enciende el dispositivo. Es importante evitar su inicio automático para evitar problemas en el futuro.
![APPLauncher](36.png)

Copiar y reemplazar el archivo `STARTMENU.RUN` en la carpeta `\Application\Startup`
![StartRunPC](37.png)

Con esto la aplicación ya queda instalada, ahora hace falta `reiniciar el dispositivo` para asegurarse de que se inicie automáticamente. Desconectar el dispositivo y volverlo a conectar. Debería iniciar aproximadamente en 5 minutos debido a la poca capacidad de procesamiento del dispositivo.

* La primera vez que se inicie pedirá la contraseña y el puerto por el cual establece la comunicación con el API.
![PrimeraVez](38.png)

* Ingresar la dirección IP del servidor donde está instalada la API y el puerto que se estableció, luego dar clic en Aceptar.
![PrimeraIP](39.png)

Una vez ingresada la IP debe aparecer la pantalla de la aplicación.
![Landing](17.png)

Escanear un producto ya registrado y se mostrará el precio en pantalla. NOTA: Si en la pantalla se muestra un error de conexión consulte [como solucionar conexión a la API.](#error-de-conexión)
![PrimerEscaneo](40.png)

* Sí se cierra la Aplicación y se desea volver a iniciarla puede reiniciar el dispositivo o bien ejecutándola, esta se encuentra en `\Program Files\LECTORX\LECTOR SAIT1.exe`.
![IniciarDeNuevo](42.png)

{{%alert success%}}Listo con eso ya queda configurado y funcionando el checador de precios.{{%/alert%}}

## Solución de Problemas

Estos son los problemas que se pueden presentar:

* [Error de Conexión.](#error-de-conexión)
* [La PC no detecta el Dispositivo.](#la-pc-no-detecta-el-dispositivo)
* [Conectarse remotamente al checador.](#conectarse-remotamente-al-checador)


### Error de Conexión

Este error lo muestra el checador cuando no puede conectarse al servidor.
![ErrorConexion](43.png)

Esto ocurre en los siguientes casos:

* No está configurada la IP correcta
* Bloqueo de Firewall
* El checador no se encuentra en la misma red del servidor
* El cable Ethernet del checador no está correctamente conectado
* SAIT API no está ejecutándose
* El servidor está apagado
* El servidor no tiene IP estática

Lo primero es asegurarse que el dispositivo este correctamente conectado a la red, después comprobar que el dispositivo tenga la IP del servidor correcta. Para ver la configuración del checador dar doble clic sobre la pantalla de bienvenida. Pedirá una contraseña que debemos ingresar después dar clic en aceptar.
![Pass](45.png)

Nos llevará a otra pantalla donde se muestra información de la configuración. La IP Dispositivo es la dirección que tiene en la red. En el siguiente campo se muestra la dirección IP y el puerto del servidor.

{{%alert warning%}}NOTA: Sí la IP es 127.0.0.1 significa que no está conectado a ninguna red.{{%/alert%}}

![Config](48.png)

Asegurarse que el checador se encuentre en el mismo segmento de red que el servidor, la dirección IP y el puerto del servidor estén correctos. Para guardar la configuración dar clic en el botón `Aceptar`, parar regresar a la pantalla de bienvenida SIN GUARDAR dar clic en `Cancelar` y para cerrar la Aplicación dar clic en `Cerrar`.

Realizar un ping de conexión desde el servidor hacia el checador con la IP que se mostró en la configuración.
![ping](47.png)

Sí el ping se realizó exitosamente significa que hay conectividad entre el dispositivo y el servidor. Sí no, asegurarse que se encuentren en la misma red y desbloquear el puerto en el que está trabajando la API luego hacer el ping de nuevo hasta que tenga respuesta.

Una vez seguros de lo anterior, realizar el escaneo de un producto registrado.
![Resulto](49.png)


### La PC no detecta el Dispositivo

Para realizar la instalación es indispensable transferir los archivos al dispositivo. En caso de que la PC no lo detecte seguir los siguientes pasos.

Al conectar el dispositivo por USB, Windows instalará automáticamente en el panel de control el `Centro de Dispositivos de Windows Mobile`.

{{%alert warning%}}NOTA: En caso de que el Centro de Dispositivos de Windows Mobile no se encuentre en el panel de control, descargarlo desde el siguiente enlace:

32 bits:

https://www.microsoft.com/es-mx/download/details.aspx?id=14

64 bits:

https://www.microsoft.com/es-ES/download/details.aspx?id=3182
{{%/alert%}}

![panel](57.png)

Sí intentamos abrirlo se queda la pantalla de bienvenida y no inicia.
![Noinicia](58.png)

Para solucionarlo debemos ir a los servicios de windows y buscar `Conectividad de dispositivo basado en Windows Mobile 2003`.
![Servicos](59.png)

Ver las propiedades del servicio, ir a la pestaña de `Iniciar Sesión` seleccionar la cuenta del sistema local y permitir que el servicio interactué con el escritorio, guardar los cambios.
![ConfigServicio](60.png)

Iniciar el servicio.
![iniciarServicio](61.png)

Ahora abrir el centro de dispositivos desde el panel de control y dar clic en `Conectar sin configurar el dispositivo`.
![Conectar](62.png)

Dar clic sobre `Examinar el contenido del dispositivo`.
![Examinar](63.png)

Ahora podemos ver el contenido del dispositivo.
![Contenido](64.png)


### Conectarse remotamente al checador

Sí se llega a presentar algún problema, se va a hacer una configuración o actualización se puede hacer de manera remota. Desde la pantalla de bienvenida pedirle a su cliente que de dos toques sobre la pantalla, se mostrará una pantalla que pide una contraseña.
![Pass](45.png)

El cliente deberá ingresar una contraseña, para mover el teclado táctil arrastrarlo por la parte gris del input panel. Una vez ingresada la contraseña dar clic sobre el botón `Aceptar`.

Pedirle el cliente que le indique la IP del dispositivo, después dar clic en el botón `Cerrar`.
![Cerrar](50.png)

Aproximadamente un minuto después de cerrar la aplicación aparecerá la ventana de configuración de VNC. Pedirle al cliente que seleccione `No Authentication` y después de clic en el botón `OK`.
![ConfigVNC](51.png)

Esto llevará el escritorio al checador, lo que significa que el acceso remoto está activado.
![Desktop](52.png)

{{%alert warning%}}IMPORTANTE: Solo se puede acceder al checador desde la red local, por lo que tendrá que conectarse remotamente a una computadora que se encuentre en la misma red a través de TeamViewer y desde ahí conectarse al checador mediante VNC.{{%/alert%}}

Desde un visor de VNC ingresar la dirección del checador.
![ConectVCN](54.png)

Sí se muestra un mensaje de conexión no cifrada dar clic en `continuar`.
![Cerfiticado](55.png)

Listo ya está conectado al checador.
![Conectado](56.png)


## Aplicación Android

El checador tambien esta disponible para ser usado en dispositivos android.

![](77.png)

## Configuración de Dispositivo ANDROID (CC600)

![Dispositivo](65.jpg)

El dispositivo Zebra CC600 funciona con el sistema operativo android, y viene preinstalado con algunas aplicaciones para poder interactuar con el hardware que tiene integrado.

Una de estas aplicaciones y la mas importante para el uso del checador es la aplicacion DataWedge que controla el escaner de codigos, debemos configurarla para que lo que lea lo envie a la aplicación como texto.

![DataWedge](66.png)

Al entrar a la aplicacion vemos los diferentes perfiles.

![Perfiles](67.png)

Seleccionamos Profile0(default)

![Pulsacion](68.png)

Buscamos la seccion de salida de pulsacion y nos aseguramos de que este activada.
Entramos a la seccion Key event options.

![Send Characters](69.png)

Aqui debemos marcar la opcion Send Characters as Events.

![Send Characters](69.1.png)

## Instalación Android

Descargar o copiar el archivo APK de intalacion al dispositivo.
![](70.png)
Buscarlo en el explorador de archivos y ejecutarlo
![](71.png)
Si en algun momento da advertencia de aplicación desconocida dar clic en INSTALAR DE TODAS FORMAS


## Configurar aplicación

La seccion de configuracion se encuenta oculta.

![](72.png)

Se debe hacer clic en el logo de la empresa varias veces hasta que aparezca la pantalla para introducir contraseña.

![](73.png)

La contraseña es una contraseña dinamica solicitarla al area de soporte.

![](74.png)

En la ventan de configuración debemos ingresar la direccion del sevidor donde consultaremos los precios.

Tambien podemos cambiar los colores del fondo y el logotipo de la empresa.

![](75.png)

Con el boton de licencia aparecera una ventana registrar la licencia del dispositivo.

![](76.png)
Con el boton Cerrar kiosko podemos seleccionar el launcher por defecto del dispositivo en caso de ser requerido.

![](77.png)




## Galeria de imagenes

![](81.png)

Es posible colocar imagenes que se mostraran en el checador cuando este inactivo.

para ello se debe ingresar al directorio donde se instalo la aplicacion del servidor, normalmente C:\Program Files (x86)\SAIT API, se debe crear la carpeta "promos" si no exste.

![](82.png)

Dentro de la carpeta promos se pueden colocar imagenes de cualquier tamaño estas se adaptaran a la pantalla.




## Configurar modo Kiosko

El modo kiosko de la aplicación permite que la app se inicie en cuanto el dispositivo se encienda y evitamos que los usuarios intenten salir de la aplicación.

![](78.png)
Digirse a la sección de Apps en la configuración y entrar a la sección de Apps predeterminadas
![](79.png)
Entrar a la seccion de Pagina principal.
![](80.png)
Seleccionar VerificadorAndroid

Al dar clic en el Circulo, para volver a la pantalla principal se abrira la aplicación en su lugar en modo kiosko.
![](72.png)



Para salir del modo kiosko podemos hacerlo desde la configuracion.


![](74.png)
![](76.png)
Con el boton Cerrar kiosko podemos seleccionar el launcher por defecto del dispositivo en caso de ser requerido.
