+++
title = "SAIT Inventario v1"
description = ""
weight = 2
+++

{{%alert success%}} AVISO: Esta documentación corresponde a la antigua versión de toma de inventarios, para ver la nueva versión siga el siguiente enlace [Version 2](../).{{%/alert%}} 


La Aplicación SAIT Inventario le permite hacer los ajustes de existencias de forma rápida y sencilla. Le evita gastar en costosos dispositivos de escaneo difíciles de usar y con memoria limitada.
Con dicha app los cambios se reflejan al instante e inalámbricamente a diferencia de un lector que necesita ir físicamente a la computadora para vaciar los resultados.

## Vídeo

{{<youtube 4SgZqoAEkbE>}}

Los requisitos son los siguientes:

* SAIT Software Administrativo Versión 2016 o posterior
* Un teléfono con Android 4.4 o posterior 
* Una red WI-FI en su empresa
* Número de vendedor debe de ser al igual al número de usuario

La instalación y configuración consta de los siguientes pasos:

* [Configurar un usuario](#configurar-un-usuario)
* [Instalación (Servidor)](#instalación-servidor)
* [Instalación (SmartPhone)](#instalación-smartphone)
* [Configuración](#configuración)
* [Captura](#captura)
* [Hacer Ajustes En Inventario](#hacer-ajustes-en-inventario)

## Configurar un Usuario
Es recomendable configurar un usuario exclusivo para el uso de la aplicación, para ello se deben seguir estos pasos:

1. Dirigirse a Utilerías - Catálogo de Usuarios
![CatalogoUsuarios](33.png)

1. Agregar un nuevo usuario con un nombre que pueda identificar.
![NuevoUsuario](34.png)

1. Dirigirse a Ventas - Vendedores.
![Vendedores](35.png)

1. Agregar un Vendedor con la misma clave que el Usuario anteriormente creado.
![NuevoVendedor](36.png)

1. Deben quedar de la siguiente manera.
![VendedorUsuario](37.png)

## Instalación (Servidor)

Antes que nada, se debe preparar un servidor para poder generar remotamente los documentos de inventario.

Asumiendo que ya se cuenta con SAIT instalado se deben realizar los siguientes pasos:

1. Primero descargar SAIT Móvil Server desde el vínculo de abajo:
https://sait.mx/download/sait-movil-instalador.exe
![descarga](1.png)

1. Aceptar el Acuerdo de Licencia y dar clic en siguiente.
![Licencia](2.png)

1. Definir el Directorio donde se va a instalar el servidor.
![DirSaitMovil](3.png)

1. Seleccionar la Ubicación donde se encuentra instalado SAIT Software Administrativo (Este paso es muy importante).
![DirSait](4.png)

1. Corroborar que la información este correcta.
![VerificarInformacion](5.png)

1. Listo con esto el Servidor queda preparado.
![SevidorListo](6.png)


{{%alert warning%}}IMPORTANTE: Establecer una dirección IP estática para no tener problemas de conectividad.

Las carpetas de las empresas deben estar en el mismo equipo donde se instaló el servicio de lo contrario no se podrá acceder.

Los directiorios de las empresas no deben estar en red si no en las mismas empresas por ejemplo:


\\\\192.168.0.123\empresas\demo 

Es incorrecto

C:\\empresas\\demo

Es correcto
{{%/alert%}}


## Instalación (SmartPhone)

Una vez preparado lo anterior siguiente paso es instalar la aplicación en el SmartPhone.

1. Para ello en la [Play Store](https://play.google.com/store) buscar la app "SAIT Inventario" o bien ingresar directamente desde el vínculo de abajo:
https://play.google.com/store/apps/details?id=inventario.sait.mx
![AppPlayStore](7.png)

1. Ya instalada abrir la app.
![AppPrimeraVez](8.png)


## Configuración

1. Para comenzar, indicar la dirección del Servidor, para ello ingresarla en el primer campo y usar el botón Test para probar conectividad, no olvide seleccionar que se quiere usar la version 1 de toma de inventarios.
![ConfigIP](9.png)

1. Ya que esté Conectado el siguiente paso es seleccionar la Empresa.
![SeleccionarEmpresa](10.png)

1. Luego Elegir una ubicación o almacén donde se harán los ajustes.
![SeleccionarUbicacion](11.png)

1. Ingresar el Usuario y Contraseña del sistema luego iniciar sesión.
![PrimerLogin](12.png)
{{%alert info%}}NOTA: La próxima vez que se inicie sesión solo pedirá Usuario y Contraseña, pero si se desea cambiar la Ubicación o a una Empresa diferente debe cerrar sesion y en el menu seleccionar cambiar empresa o servidor.{{%/alert%}}

1. El usuario ya está conectado remotamente, ahora se muestran los documentos de toma de inventario.
![PrimerLoginExitoso](13.png)


## Captura

1. Con todo lo anterior listo ya se pueden generar Documentos de Toma de Inventario desde el SmartPhone, con el botón de nuevo crear un documento.
![NuevoDocumento](14.png)

1. Ingresar un nombre para poder identificarlo.
![NombreDocumento](15.png)

1. Una vez asignado un nombre genera un documento vacío donde se capturan los artículos.
![DocumentoVacio](16.png)
{{%alert info%}}Para empezar a tomar inventario es recomendable usar un lector de códigos de barras que se pueda vincular mediante bluetooth al SmartPhone, o bien se pueden capturar de forma manual usando la clave o el código de barras. La segunda opción hace el proceso muy lento.{{%/alert%}}
Lo siguiente es comenzar a escanear los artículos.

1. Asegurarse que este seleccionado el campo de Clave o Código, para comenzar a agregar productos.
![Escanear](17.png)

1. Utilizando un lector o bien capturando manualmente se mostrará la descripción del artículo si este se encuentra registrado, de lo contrario simplemente se visualiza el código.
![Escaneado](18.png)

1. Ahora se debe definir la cantidad ya sea escribiéndola o usando los controles de restar y sumar.
![DefinirCantidad](19.png)

1. Sí hubo una equivocación en captura se puede usar opción Descartar, de lo contrario usar el botón agregar para insertar el artículo al registro.
![PrimerRegistro](20.png)

1. Capturar todos los artículos sucesivamente, estos se irán agregando en la tabla de abajo.
![ArticulosCapturados](21.png)

1. Ya con todo listo ahora solo resta enviar el documento al servidor, para ello desplegar el menú de los tres puntitos y seleccionar `Grabar`.
![TresPuntitos](22.png)
![Grabar](23.png)

1. Se mostrará un mensaje de "Grabado Correctamente" lo que indica que el documento ya se encuentra en el sistema.
![GrabadoCorrectamente](24.png)


## Hacer Ajustes en Inventario

1. Ya finalizada la captura desde la Aplicación es hora de hacer el Ajuste en el Sistema.

1. Para ubicar los documentos, dirigirse al módulo de Inventario - Proceso de Toma de Inventario - Documentos de Toma de Inventario.
![DocumentosDeInventario](25.png)

1. Se listarán todos los documentos creados en el almacén.
![DocumentosAlmacen](26.png)

1. Seleccionar dando doble clic sobre uno, se mostrará el contenido de este.
![COntenidoDocumento](27.png)
{{%alert warning%}}IMPORTANTE: Si se capturo un artículo que no se encuentra en inventario es importante registrarlo.{{%/alert%}}

1. Una vez seguros de los artículos capturados es momento de hacer los ajustes, se debe usar la ventana de Inventario - Proceso de Toma de Inventario - Ajustar Existencias.
![VentanaAjustes](28.png)

1. Dar clic en "Cargar Artículos".
![CargarArticulos](29.png)

1. Configurar la ventana de existencias como mejor convenga y dar clic en "Cargar Artículos".
![CargarArticulos](30.png)

1. Ahora se visualizan los artículos de todos los documentos de inventario.
![NuevosArticulos](31.png)

1. Estando seguros del ajuste a realizar y de haber aplicado los filtros necesarios es hora de grabar los cambios, para ello dar clic en "Realizar Ajuste", luego en "SI" a las alertas que nos del sistema.
![AjusteRealizado](32.png)

{{%alert success%}}LISTO con eso quedan actualizadas las existencias del inventario.{{%/alert%}}