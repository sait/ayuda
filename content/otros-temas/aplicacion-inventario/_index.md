+++
title = "Aplicación SAIT Inventario "
description = ""
weight = 10
+++

La Aplicación SAIT Inventario le permite hacer los conteos de existencias de forma rápida y sencilla. Le evita gastar en costosos dispositivos de escaneo difíciles de usar y con memoria limitada.
Con nuestra app los cambios se reflejan al instante e inalámbricamente a diferencia de un lector que necesita ir físicamente a la computadora para vaciar los resultados.

{{%alert success%}} Esta aplicación se usa en conjunto con la versión 2 del Proceso de Toma de Inventarios la cual permite hacer inventarios a puertas abiertas [ver documentación](/sait-erp/8-capacitacion-en-modulo-inventario/o-proceso-de-toma-de-inventario/version-2/) {{%/alert%}}

{{%alert danger%}}El periodo de prueba de la aplicación es de 2 días, la demo
esta limitada a 4 dispositivos por empresa, después de este periodo se le solicitara una licencia.{{%/alert%}}

Los requisitos son los siguientes:

* SAIT Software Administrativo Versión 2018.24.0 o posterior
* Un teléfono con Android 4.4 o posterior 
* Una red WI-FI en su empresa
* Número de vendedor debe de ser al igual al número de usuario

Opcional:

* Scanner de Bolsillo
![Scanner](scanner.jpg)

La instalación y configuración consta de los siguientes pasos:

* [Configurar un usuario](#configurar-un-usuario)
* [Instalación (Servidor)](#instalación-servidor)
* [Instalación (SmartPhone)](#instalación-smartphone)
* [Activación de la aplicación](#activación-de-la-aplicación)
* [Configuración](#configuración)
* [Captura](#captura)
* [Hacer Ajustes En Inventario](#hacer-ajustes-en-inventario)


## Configurar un Usuario
Es recomendable configurar un usuario exclusivo para el uso de la aplicación, para ello se deben seguir estos pasos:

1. Dirigirse a Utilerías - Catálogo de Usuarios
![CatalogoUsuarios](33.png)

1. Agregar un nuevo usuario con un nombre que pueda identificar.
![NuevoUsuario](34.png)

1. Dirigirse a Ventas - Vendedores.
![Vendedores](35.png)

1. Agregar un Vendedor con la misma clave que el Usuario anteriormente creado.
![NuevoVendedor](36.png)

1. Deben quedar de la siguiente manera.
![VendedorUsuario](37.png)

## Instalación (Servidor)

Antes que nada, se debe preparar un servidor para poder generar remotamente los documentos de inventario.

Asumiendo que ya se cuenta con SAIT instalado se deben realizar los siguientes pasos:

1. Primero descargar SAIT Móvil Server desde el vínculo de abajo:
https://sait.mx/download/sait-movil-instalador.exe
![descarga](1.png)

1. Aceptar el Acuerdo de Licencia y dar clic en siguiente.
![Licencia](2.png)

1. Ingresar el puerto de escucha en la red que se utilizara.
![Puerto](puerto.png)

1. Definir el Directorio donde se va a instalar el servidor.
![DirSaitMovil](3.png)

1. Seleccionar la Ubicación donde se encuentra instalado SAIT Software Administrativo (Este paso es muy importante).
![DirSait](4.png)

1. Corroborar que la información este correcta.
![VerificarInformacion](5.png)

1. Listo con esto el Servidor queda preparado.
![SevidorListo](6.png)


{{%alert warning%}}IMPORTANTE: Establecer una dirección IP estática para no tener problemas de conectividad.

Las carpetas de las empresas deben estar en el mismo equipo donde se instaló el servicio de lo contrario no se podrá acceder.

Los directiorios de las empresas no deben estar en red si no en las mismas empresas por ejemplo:


\\\\192.168.0.123\empresas\demo 

Es incorrecto

C:\\empresas\\demo

Es correcto
{{%/alert%}}


## Instalación (SmartPhone)

{{%alert danger%}}Es necesario pasar por el periodo de prueba de la palicacion, el periodo de prueba de la aplicación es de 2 días a partir de la fecha de instalación en su dispositivo, la demo
esta limitada a 4 dispositivos por empresa, después de este periodo se le solicitara una licencia. Tenga esto en cuenta para planear las pruebas.{{%/alert%}}

Una vez preparado lo anterior siguiente paso es instalar la aplicación en el SmartPhone.

1.Para ello deberá bajar la aplicación del siguiente link https://sait.mx/download/sait-movil/saitinventario.apk

2.Ya instalada abrir la app.
![AppPrimeraVez](8.png)

3.Aceptar el periodo de prueba.

![Periodo de prueba.](prueba.png)

## Activación de la aplicación.

*	La activación esta ligada al dispotivo. 
*	La activación se hace de forma remota, no requiere claves, usamos la identificación del dispositivo y la empresa para activar la aplicación.
*	La aplicación debe pasar por el periodo de prueba y debe tener conexión a internet, el usuario debe iniciar sesión con su usuario/vendedor de SAIT, con esto se enviara a nuestros servidores la identificación de este dispositivo y podremos activarlo de forma remota.

## Configuración

4.Para comenzar, indicar la dirección del Servidor (en caso de usar un puerto diferente al 80 se debe ingresar la direccion seguida de ':' seguido de el puerto elegido "direccion:puerto"), para ello ingresarla en el primer campo y usar el botón Test para probar conectividad, dejar desmarcada la opción de "Usar versión  1".
![ConfigIP](9.png)

5.Ya que esté Conectado el siguiente paso es seleccionar la Empresa.
![SeleccionarEmpresa](10.png)

6.Luego Elegir una ubicación o almacén donde se harán los ajustes.
![SeleccionarUbicacion](11.png)

7.Ingresar el Usuario y Contraseña del sistema luego iniciar sesión.
![PrimerLogin](12.png)
{{%alert info%}}NOTA: La próxima vez que se inicie sesión solo pedirá Usuario y Contraseña, pero si se desea cambiar la Ubicación o a una Empresa diferente debe cerrar sesion y en el menú seleccionar cambiar empresa o servidor.{{%/alert%}}

8.El usuario ya está conectado remotamente, ahora se muestran la pantalla del conteo de artículos.
![ConteoArt](13.png)

## Captura

1.Antes de empezar a capturar vamos a identificar el menú, aquí podemos consultar la información del dispositivo y el servidor, cambiar de posición los botones y capturar la ubicación de esta toma de inventario. 
![MenuHamburguesa](14.png)

2.Para actualizar la ubicación, capturamos la ubicación y presionamos “OK”. 
![Ubicacion](15.png)

3.Asegurarse que este seleccionado el campo de Clave o Código, para comenzar a agregar productos.
![Enviar](13.png)

4.Al escanear la clave o código, nos mostrara la descripción del artículo, también se mostrara la descripción del conteo al que pertenece el articulo en la barra superior. 
![CapturaArticulo](16.png)

5.En el campo de cantidades capturamos la cantidad contada y grabamos.
![Cantidad](17.png)

6.De manera inmediata la información se envía al sistema SAIT, se limpia la pantalla y podemos seguir con el conteo de otros artículos. 
![Enviar](18.png)



