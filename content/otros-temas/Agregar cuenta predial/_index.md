+++
title = "Agregar Cuenta Predial"
description = ""
weight = 18
+++

En SAIT es posible emitir recibos de arredanmiento, para ello tendrá que realizar una sencilla configuración para poder incluir la cuenta predial en la facturar:

1.Ir al menú de Utilerias / Configuración General del Sistema / Datos Adicionales en Catálogos:

![IMG](1.jpg)

2.Agrega un nuevo dato dado clic en el [Agregar Dato]

![IMG](2.jpg)

3.Llenamos los datos como aparece en la imagen y damos clic en [Crear Campo Físicamente]

![IMG](3.jpg)

4.Nos apacerá la siguiente ventana y creamos el campo de la siguiente manera con estas especificaciones, el campo se llamará CTAPREDIAL será tipo texto de 20 caracteres y damos clic en [Crear]

**Importante que en este punto nadie esté usando el sistema**

![IMG](4.jpg)

Si se creó el campo correctamente, aparecerá este mensaje:

![IMG](5.jpg)

Si por algún motivo el campo ya existía, aparecerá el siguiente mensaje, daremos clic en [Aceptar] y [Cancelar]

![IMG](5.1.jpg)

5.La cuenta predial se podrá capturar en el catálogo de artículos y servicios en la pestaña OTROS DATOS.

![IMG](6.jpg)

6.Una vez capturada la cuenta, realice la factura de manera normal en Ventas / Registrar Ventas

7.El pdf normalmente ya viene preparado para mostrar este dato.

![IMG](7.jpg)

Si su formato no lo tiene agregado, agregue esta expresión al formato de factura:

```
iif(type('Arts.CTAPREDIAL')=='C',iif(empty(Arts.CTAPREDIAL),'','Cuenta Predial: '+(Arts.CTAPREDIAL)),'')
```
