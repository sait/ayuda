+++
title="Descargar Última Versión "
description=""
weight = 1
+++

[ Bitácora de cambios SAIT ERP, Básico y Contabilidad. ](/sait-erp/bitacora/) 

[ Bitácora de cambios SAIT Nómina ](/sait-nomina/bitacora/)

Para descargar la última versión del sistema SAIT Software Administrativo realice los siguientes pasos: 

### Indicaciones:

* Deberá acceder al navegador de su preferencia: Google Chrome, Internet Explorer, Mozilla.
* Dependiendo el paquete que tenga instalado, deberá dar clic derecho sobre el link y seleccionar Guardar Vínculo Como...
	*	Sait ERP: [https://sait.mx/download/2025/sait.exe {{%icon fa-external-link%}}](https://sait.mx/download/2025/sait.exe)
	*	Sait Básico: [https://sait.mx/download/2025/saitbasico.exe {{%icon fa-external-link%}}](https://sait.mx/download/2025/saitbasico.exe)
	*	Sait Nómina: [https://sait.mx/download/2025/saitnom.exe {{%icon fa-external-link%}}](https://sait.mx/download/2025/saitnom.exe)
	*	Sait Contabilidad: [https://sait.mx/download/2024/saitcont.exe {{%icon fa-external-link%}}](https://sait.mx/download/2024/saitcont.exe)
* El sistema le preguntará dónde desea gudardarlo
![IMG](guardarcomo.jpg)
* Se presentará la ventana de "Guardar como"
* Navegue hasta la carpeta C:\SAIT (Para versión SAIT ERP)
* Navegue hasta la carpeta C:\SAITBasico (Para versión SAITBasico)
* El sistema indicará que el archivo ya existe si desea remplazarlo 	
* Seleccione SI
* El archivo de la actualización sait.exe se empezará a descargar y sustituirá el ejecutable anterior. 
![IMG](guardarcomosait.jpg)
