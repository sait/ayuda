+++
title = "Error al activar licencia Property COMPUTERNAME is not found"
description = ""
weight = 15
+++

Si al momento de activar su licencia SAIT le aparece el siguiente mensaje de error
 
![IMG](computername.png)
 
Significa que una propiedad de windows la cual nos arroja el nombre del equipo está dañada.

Para corregir este detalle deberá seguir los siguientes pasos:

<h2 class="text-primary">Confirmar efectivamente esté dañada esta propiedad</h2>

Para revisar esto se dirigirá al inicio de Windows e ingresar el siguiente texto: WMIMGMT.MSC

Dar clic derecho en el Control WMI(Local) y después seleccionar Propiedades

![IMG](propiedades.png)

Si la clase está CORRECTA y funcional te mostrará información como en la siguiente imagen:
 
![IMG](correcta.png)

Si está DAÑADA se mostrará de esta manera:

![IMG](incorrecta.png)

<h2 class="text-primary">SOLUCIÓN 1</h2>

Para corregirlo deberá abrir la ventana de CMD como administrador y colocará este comando:

```
CD C:\Windows\System32\WBEM 

dir /b *.mof *.mfl | findstr /v /i uninstall > moflist.txt & for /F %s in (moflist.txt) do mofcomp %s
```

Cuando empiece a correr, la pantalla se verá de esta manera

![IMG](correccion.png)

Se tardará unos minutos en completar el proceso, una vez que termine puede volver a consultar el WMIMGMT.MSC y observará ya se muestran las propiedades y con esto podrá activar la licencia SAIT

![IMG](corregida.png)

### En caso de que al correr el comando le aparezca este error

![IMG](error.png)

Deberá copiar el siguiente script, pegarlos y ejecutarlos en la ventana del CMD

```
Echo Rebuilding WMI.....Please wait. > c:\wmirebuild.log
net stop sharedaccess >> c:\wmirebuild.log
net stop winmgmt /y >> c:\wmirebuild.log
cd C:\WINDOWS\system32\wbem >> c:\wmirebuild.log
del /Q Repository >> c:\wmirebuild.log
c:
cd c:\windows\system32\wbem >> c:\wmirebuild.log
rd /S /Q repository >> c:\wmirebuild.log
regsvr32 /s %systemroot%\system32\scecli.dll >> c:\wmirebuild.log
regsvr32 /s %systemroot%\system32\userenv.dll >> c:\wmirebuild.log
mofcomp cimwin32.mof >> c:\wmirebuild.log
mofcomp cimwin32.mfl >> c:\wmirebuild.log
mofcomp rsop.mof >> c:\wmirebuild.log
mofcomp rsop.mfl >> c:\wmirebuild.log
for /f %%s in ('dir /b /s *.dll') do regsvr32 /s %%s >> c:\wmirebuild.log
for /f %%s in ('dir /b *.mof') do mofcomp %%s >> c:\wmirebuild.log
for /f %%s in ('dir /b *.mfl') do mofcomp %%s >> c:\wmirebuild.log
mofcomp exwmi.mof >> c:\wmirebuild.log
mofcomp -n:root\cimv2\applications\exchange wbemcons.mof >> c:\wmirebuild.log
mofcomp -n:root\cimv2\applications\exchange smtpcons.mof >> c:\wmirebuild.log
mofcomp exmgmt.mof >> c:\wmirebuild.log
net stop winmgmt >> c:\wmirebuild.log
net start winmgmt >> c:\wmirebuild.log
gpupdate /force >> c:\wmirebuild.log
```

<h2 class="text-primary">SOLUCIÓN 2</h2> 

Otra alternativa es, reconstruir por completo el repositorio dañado, para ello deberá seguir las siguientes instrucciones:

1.Presione las teclas Win + R para abrir el cuadro **Ejecutar** después escriba la palabra CMD y de clic derechos para abrirlo como administrador:


2.Ingrese el siguiente comando: **net stop winmgmt** para detener el servicio servicio WMI y de ENTER en su teclado para ejecutar el comando, después escriba la tecla S de SÍ o Y de YES, dependiento el idioma del sistema operativo.

![IMG](1.png)

3.Abra el explorador de archivos y diríjase a esta ruta: **C:\Windows\System32\wbem** busque la carpeta **Repository** y cámbiele el nombre a Repository.old

![IMG](repository.jpg)

![IMG](repository2.jpg)

4.Una vez hecho esto, vuelva al cmd y escriba el comando **net start winmgmt** y presione la tecla ENTER para ejecutar el comando, después escriba la tecla S de SÍ o Y de YES, dependiento el idioma del sistema operativo.

![IMG](2.png)

5.Reinice su equipo y verifique si se reestableció la librería, si se corrigió deberá de verse así, izquierda incorrecto, derecha correcto:

![IMG](3.png)

<h4 class="text-danger">Si no funciona ninguna de estas opciones, significa que esta propiedad está dañada irremediablemente y la única alternativa para corregirlo, es un formateo del equipo</h4>