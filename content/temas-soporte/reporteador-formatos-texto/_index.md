+++
title = "Reporteador de formatos tipo texto"
description = ""
weight = 26
+++


Como ya sabemos en Windows con 64 bits al momento de querer editar un reporte nos marca que el programa ó característica **"\\??\C:\SAIT\UtilF.exe" no se puede iniciar o ejecutar porque no es compatible con versiones de 64 bits.**

![IMG](imagen1.PNG)

{{%alert success%}}Dado este caso, utilizaremos la herramienta DOSBox, para esto siga los pasos a continuación descritos:{{%/alert%}}

* Descargar el programa: **DOSBOX**, [Clic Aquí](Dosbox.zip)
* Una vez descargado el archivo, deberá de descomprimir el archivo.
* Ejecutar el instalador y seguir las instrucciones de Instalación.
* Descargar el programa: **Foxprox.exe**, [Clic Aquí](fox.zip)
* Deberá de descomprimir el archivo, y colocarlo dentro de la instalación del sistema. (Ejemplo: C:\SAIT).
* Ejecutar o Abrir el programa DOSBOX, ya sea haciendo clic en el icono de acceso directo o bien desde el botón de inicio.
* Deberás colocar el programa: **foxprox** en el folder c:\sait
* Abrir el programa DOSBOX tal como se muestra a continuación:
* ![IMG](imagen2.PNG)
* Después deberás de escribir la siguiente línea, al final dar un: **[ENTER]** **mount y: c:\sait**, tal como se muestra a continuación:
* ![IMG](imagen3.PNG)
* Posterior a eso, escribir **Y:** y dar un **[ENTER]**, como se muestra a continuación:
* ![IMG](imagen4.PNG)

* Escribir la siguiente línea ya final dar un **[ENTER]**, como podemos observar:
* ![IMG](imagen5.PNG) 

* Una vez que se ejecutó la app de **Foxprox**, escribir la siguiente línea: "SET DEFA TO DEMO" (DEMO es directorio de la empresa de pruebas, pero en tu caso colocar el nombre de la carpeta de empresa)., al final dar un [ENTER] tal como se muestra:
* ![IMG](imagen6.PNG) 
* Posterior a esto, deberás de escribir: MODI REPORT NOTA (NOTA es el nombre del formato a modificar):
* ![IMG](imagen7.PNG) 
* Presionar la tecla **[ENTER]** y en esos momentos se abrirá la ventana de modi de edición en texto del reporte:
* ![IMG](imagen8.PNG) 
* Realizar las modificaciones al formato. Al finalizar ir al menú de: **FILE / SAVE**.
* Para salirse de la edición, dirigirse a: **FILE / EXIT**.
* Para salir de foxpro, escriba el comando: **QUIT**
* Escriba **EXIT**, para cerrar **DOSBox**.
* Entrar a SAIT y probar el cambio realizado en el formato.
* Listo!!!, con esto hemos modificado de manera exitosa el formato en cuestión.



<h4 class="text-primary">REPORTEADOR GRÁFICO</h4>

Sí se trata de la edición de un reporte gráfico aquí encontrará la documentación:

https://ayuda.sait.mx/temas-soporte/reporteador-grafico/