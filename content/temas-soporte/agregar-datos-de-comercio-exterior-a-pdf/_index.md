+++
title = "Expresiones para mostrar datos de Comercio Exterior en PDF"
description = ""
weight = 53
+++

<h3 class="text-primary">Datos en detallado de articulos</h3>

```
FraccionArancelaria = Arts.FracAranc
CantidadAduana      = round(Movim.Cant*Arts.EquiAduana,3)
UnidadAduana        = Arts.UnidAduana
ValorUnitarioAduana = round(Movim.Precio/Arts.EquiAduana,2) && Docum.DIVISA==D
ValorUnitarioAduana = round(round((Movim.Precio/Arts.EquiAduana)/Docum.TCComExt,2),2)  && Docum.DIVISA==P
ValorDolares        = round((Movim.Cant*Arts.EquiAduana)*(Movim.Precio/Arts.EquiAduana),2) && Docum.DIVISA==D
ValorDolares        = round(Movim.Cant*round((Movim.Precio/Arts.EquiAduana)/Docum.TCComExt,2),2)  && Docum.DIVISA==P
```

<h3 class="text-primary">Datos del complemento</h3>

```
Reg. fiscal destinatario       = Clientes.NumIdTrib
Funge como Certificado origen  = iif(Allt(ValProp(Docum.OTROSDATOS,'CertOrigen'))=='0','NO','SI') 
No. Certificado origen         = ValProp(Docum.OTROSDATOS,'NumCertOrigen')
Subdivisión                    = iif(Allt(ValProp(Docum.OTROSDATOS,'SubDivision'))=='0','NO','SI')
Clave pedimento                = ValProp(Docum.OTROSDATOS,'ClavePedim')
Tipo de operación              = ValProp(Docum.OTROSDATOS,'TipoOperacion')
Clave Incoterm                 = ValProp(Docum.OTROSDATOS,'IncoTerm')
Para el nombre Incoterm se necesita un campo área =
                                                    En el Page Header: iif(Select('incoterm')==0,DoCMD('Use incoterm In 0 Order CLAVE'),' ') 
                                                    En el detallado: CampoArea(allt(valprop(docum.otrosdatos,'incoterm')),'incoterm','NOMBRE') 
```

Para mostrar el nombre de la clave Incoterm se necesita un índice adicional: [clic aquí para descargar](incoterm.key) este archivo deberá agregarlo dentro de la carpeta de la CIA.

Formato de comercio exterior [clic aquí para descargar](formato-factura-comercio-exterior.rpt)

![IMG](EJEMPLO.png)

