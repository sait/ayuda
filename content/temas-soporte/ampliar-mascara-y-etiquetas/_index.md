+++
title = "Ampliar máscaras y etiquetas en formatos"
description = ""
weight = 29
+++

En ocasiones algunos reportes en el sistema puede que se muestren con asteriscos, como se puede apreciar en el siguiente ejemplo:

![IMG](asteriscos.png)

### AMPLIAR MÁSCARA

En estas situaciones solo basta con ampliar la máscara, el motivo se debe a que las cantidades que nosotros estamos manejando son más grandes que la de la máscara por default.
Para ampliarla deberá seguir las siguientes instrucciones:

1.Consultar el reporte

2.Seleccionar la opción de Formato

![IMG](editar-formato.png)
 
3.Daremos doble clic sobre el campo que deseamos ampliarle la máscara y se nos abrirá la siguiente ventana

![IMG](sin-ampliar.png)
 
4.En donde en la parte de Format le ampliaremos la máscara de la siguiente forma: 99,999,999.99

![IMG](ampliada.png)
 
5.Damos clic en OK

6.Nos dirigimos a File y damos clic en Save

![IMG](save.png)
 
7.Listo

### AMPLIAR ETIQUETA

También es importante en caso de manejar cantidades muy grandes, ampliar la etiqueta como tal. Para ampliarla deberá seguir las siguientes instrucciones:

1.Consultar el reporte

2.Seleccionar la opción de Formato
 
![IMG](editar-formato.png)

3.Seleccionaremos la etiqueta a ampliar, por ejemplo en este caso y para efectos de la demostración, las ultimas etiquetas del reporte de los formatos son las de los totales y comúnmente éstas las que necesitan ser ampliadas, con el mouse daremos clic del lado derecho de la etiqueta y la extendemos a la derecha como se muestra en la siguiente imagen:

![IMG](etiqueta-importe.png)
 
4.Nos dirigimos a File y damos clic en Save

![IMG](save.png)
 
5.Listo

