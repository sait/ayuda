+++
title = "Pasar datos de tabla DBF a archivo de Excel"
description = ""
weight = 44
+++

### **Indicaciones**:

Cuando se está migrando de otro sistema a SAIT y el programa anterior utilizaba tablas tipo DBF una opción que nos puede ayudar a importar más rápido la información de los catálogos (clientes, proveedores, artículos) es convertir esos archivos dbf a excel para posteriormente importarlos a SAIT con la utileria ubicada en Utilerías / Importar información desde / Excel.

1.Primeramente deberá descargar el archivo comprimido Vfp6.zip [descargar](vfp6.rar)

2.Descomprima el contenido en la ubicación que usted prefiera

3.Cree una carpeta en el disco C: de su equipo, para efectos de la tabla yo le llamaré "TABLAS" y coloque ahí los archivos DBF que deseamos pasar a excel

![IMG](1.png) 

4.Abriremos el archivo VFP6.EXE dando doble clic en el mismo ![IMG](vfp6.png) 

5.Se nos abrirá el programa e ingresaremos los siguientes comandos

![IMG](2.png) 

6.Ahora exportaremos la información a excel yendo al menú File y después dando clic en Export...

![IMG](3.png) 

6.Seleccionaremos el tipo Microsoft Excel 5.0 (XLS)

![IMG](4.png) 

7.Después indicaremos donde queremos guardar el archivo en la parte de "To" 

![IMG](to.png) 

8.Escribimos el nombre al archivo de excel que estamos por crear y damos clic en Save

![IMG](5.png) 

9.Damos clic en OK

![IMG](6.png) 

10.Listo, el archivo de excel está creado 

![IMG](7.png) 

11.Regresamos al programa VFP6 y daremos clic en la X para cerrar la consulta

![IMG](8.png) 

12.Finalmente ingresamos los comandos USE y QUIT aparece en la siguiente imagen y listo.

![IMG](9.png) 



