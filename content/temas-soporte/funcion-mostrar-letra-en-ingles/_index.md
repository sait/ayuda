+++
title = "Función para imprimir cantidad con letra en inglés"
description = ""
weight = 43
+++

Para mostrar en inglés la cantidad con letra en un formato deberá bajar este archivo complimido [clic aquí para descargar](letraing.zip)

Después deberá locar la ruta de la empresa, para ello abra SAIT y de clic en [Catálogo de Empresas]

![IMG](1.png)

Selecciona con el mouse el directorio y presionará CTRL + C para copiar la ruta

![IMG](2.png)

Después nos dirigimos al explorador de winwodws y pegamos la ruta copiada con las teclad CTRL + V y damos [Enter]

![IMG](3.png)

Se nos mostrarán los archivos de la base de datos, abriremos el archivo comprimido, seleccionamos el archivo letraing.FXP y lo arrastramos a la carpeta

![IMG](4.png)

Deberá modificar el formato donde desees imprimir la cantidad y agregar un nuevo campo con la siguiente expresion:
LetraIng(<campos con los importes>)
 
Ejemplo:

Para una cotización la expresión será:

LetraIng(Docum.IMPORTE-Docum.DESCUENTO+Docum.IMPUESTO1+Docum.IMPUESTO2)

![IMG](5.png)

Mostrándose de esta manera:

![IMG](6.png)

