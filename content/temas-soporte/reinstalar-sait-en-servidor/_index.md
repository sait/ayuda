+++
title = "Reinstalación de SAIT en Servidor"
description = ""
weight = 2
+++

Si en algún momento se tiene la necesidad de cambiar de equipo o formatearlo ya sea por virus, fallas en el sistema operativo, etc, será necesario reinstalar el sistema y todos los servicios de SAIT que se encuentren en él. 

Es IMPORTANTE mencionar que se solicitará de nuevo la clave de registro, por ello se debe asegurar tener desactivada la licencia antes de continuar con el procedimiento. 

### I.	Puntos previos a considerar 

Contar con el número de contrato y contraseña para poder solicitar la desactivación y activación de la licencia en SAIT. 

Solicitar la desactivación de la licencia del server anterior al departamento de claves al correo **ventas@sait.com.mx** o llamar directo a la oficina con ventas al: **653-534-8800 / 01-800-025-5357**

#### Realizar un respaldo de las carpetas del sistema: 

* Respaldar el directorio C:\SAIT para el sistema Administrativo 

* Respaldar el directorio C:\SAITNOM para el sistema de Nómina 

* Respaldar el directorio de C:\SAITDIST para la configuración del enlace de sucursales. 

* Respaldar el directorio de C:\Program Files (x86)\SAIT\Boveda3 para respaldar los XML que guarda el Organizador de Comprobantes Fiscales. 

Dependiendo de la configuración de su empresa y sus datos deberá asegurarse de la ubicación de las carpetas mencionadas. 

### II.	Reinstalación de paquete SAIT

Descargar el programa de instalación del sistema dependiendo de su paquete SAIT: 


**Sait ERP:** https://www.sait.mx/download/sait-ERP-instalador.exe

**Sait Básico:** https://www.sait.mx/download/sait-basico-instalador.exe

**Sait Nómina:** https://www.sait.mx/download/sait-nomina-instalador.exe

**Sait Contabilidad:** http://www.sait.com.mx/download/sait-contabilidad-instalador.exe

### III.	Reinstalación driver MYSQL para uso de Enlace de Sucursales (SAITDIST)

SAIT Distribuido (si utiliza Enlace de Sucursales): https://sait.mx/download/mysql-odbc-connector-351-setup.exe

### IV.	Reinstalación de Organizador de Comprobantes Fiscales

Organizador de Comprobantes Fiscales: https://ayuda.sait.mx/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/

### V.	Activación de Licencia

Una vez colocado el respaldo de las carpetas con la información de la empresa se deberá ingresar a activar la licencia: https://ayuda.sait.mx/sait-basico/1-instalacion-y-activacion-licencias/d-activar-licencia-de-uso/


### VI. Compartir carpeta SAIT y Reconectar unidad de red en estaciones

Una vez colocado el respaldo en el servidor deberá compartir la carpeta asegurándose de que tenga todos los permisos así como de lectura y escritura

Para ello deberá dar clic derecho sobre la carpeta de SAIT y dar clic en **[Propiedades]**, posteriormente deberá ir a la pestaña de Compartir y dará clic en el botón de **[Compartir]**

![IMG](1.png)

Deberá seleccionar la opción de Todos y dar clic en **[Agregar]**

![IMG](2.png)
 
Un paso muy importante que no debemos omitir es dar permisos de Lectura y Escritura para que los demás equipos no tengas problemas para grabar y consultar información.

Después daremos clic en **[Compartir]** para completar el proceso
 
![IMG](3.png)

El sistema nos dará el siguiente mensaje lo cual nos indica la carpeta ya está compartida de manera correcta, damos clic en **[Listo]**

![IMG](4.png)
 
Finalmente hay que reconectar las unidades de red en las estaciones ya que al cambiar el servidor la ruta no es la misma y puede que en las otras máquinas se nos muestre el siguiente mensaje de error:
 
![IMG](5.jpg)

Para este paso deberá ir a algún equipo estación e ir a Mi Equipo y dar clic en **[Conectar a unidad de red]**

![IMG](6.png) 

Aquí es donde lo primero que tenemos que conocer es a donde nos queremos conectar, es decir el nombre del servidor.

Ejemplo: NOMBRE DEL EQUIPO \ CARPETA COMPARTIDA 

Tal y como se muestra en la siguiente imagen, una vez colocada a ruta de la información a la que deseamos acceder daremos clic en **[Finalizar]**.
 
![IMG](7.png) 

Como podemos observar, la unidad de red ha sido creada de manera correcta.

![IMG](8.png) 
 
Finalmente, debemos redireccionar los directorios en SAIT, una vez que tenemos conectada la unidad de red a la carpeta correcta, debemos de ubicar exactamente donde se encuentran los archivos de nuestra empresa para poder acceder a ellos desde SAIT.

1.Accedamos a SAIT dando doble clic en el icono del programa.

2.Damos clic en el botón de Catalogo de Empresas.

![IMG](9.png)  

3.Después clic en agregar empresa existente y seleccionamos el directorio donde se encuentra la empresa y damos clic en continuar.

![IMG](10.png) 
 
Por último el directorio debe aparecer en el catalogo de empresas. Seleccionamos el directorio con ruta anterior y damos clic en borrar para no tenerla disponible ya que no podemos acceder a ella.
Listo. Ya puede usar SAIT su equipo.

### VII. Instalación de Consultas SQL Server 

En empresas grandes donde el volumen de información que se procesa es muy grande, es muy importante instalar las consultas SQL para agilizar el rendimiento óptimo de las consultas que se hacen en el sistema. 

Cabe de mencionar, que este servidor de consultas se debe de instalar en la máquina que residen los datos, es decir el servidor. 
 
#### Pasos de Instalación de SQL SERVER

1.Crear una carpeta llamada SAITSQL, al mismo nivel al que se encuentra la carpeta de SAIT.

EJEMPLO: Si la carpeta SAIT se encuentra en C:


C:\SAIT

La carpeta SQL SERVER será C:\SAITSQL\

Si la carpeta SAIT se encuentra en: C:\SISTEMAS\SAIT\

La carpeta del SQL SERVER será C:\SISTEMAS\SAITSQL\ 

2.Descargar el archivo SQLSERVER.EXE desde la siguiente dirección: https://www.sait.mx/download/sqlserver.exe

3.Copiarlo a la carpeta SAITSQL. 

4.Crear un acceso directo del servidor de consultas en el escritorio del servidor.

También se puede agregar al grupo de Inicio de Windows para que se ejecute al encender el equipo. 

5.Configurar el uso del servidor de consultas SQLSERVER en cada máquina donde este instalado SAIT: 

1.Dirigirse al menú de Utilerías \ Configuración General del Sistema. 

2.Seleccionar la pestaña de **[Otros]**. 

3.Activar la casilla de **[ * ]** Hacer llamadas a servidor remoto SQL Server. 

4.Hacer clic en el botón **[Cerrar]**. 
 
#### Consultas lentas con (SQL SERVER)

Adicionalmente al estar manejando las consultas por medio de SQL SERVER en SAIT y la empresa cuenta con un servidor con Windows server 7 u 8 y estaciones con Windows 7, es decir distintos Sistemas Operativos, esto provoca que las consultas sean lentas ya que hay una falla de comunicación entre los Windows. 

Para solucionar este problema realice los siguientes pasos: 

1.En la estación ejecutar: regedit

2.Navegar a la siguiente ruta: 

3.HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters 

4.Tomar una imagen JPG de los valores actuales 

5.Editar los valores siguientes: 

6.FileInfoCacheLifetime 

7.FileNotFoundCacheLifetime 

8.DirectoryCacheLifetime 

9.A todos ponerles el valor de 0 

10.Si no existen crearlos usando el tipo: Reg_DWord (32 bits) 

* En el espacio en blanco dar click derecho. 

* Seleccionar la opción crear Reg_DWord (32 bits) 

* Ingresas el nombre del parámetro y listo. 

11.Tomar otra imagen JPG de la ventana 

12.Reiniciar la estación 

13.Probar la búsqueda 

14.Si el problema persiste favor de comunicarse con el área de soporte o mandar un correo a soporte@sait.com.mx 

**NOTA: Este proceso se realiza por estación**

#### En caso de que prefiera ver el video con las instrucciones del proceso de Reinstalación de SAIT en server puede dar clic en el siguiente video: 


{{<youtube QGJpRUBBrqE>}}
