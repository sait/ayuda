+++
title = "Ajustar a Cero la Existencia de Todos los Artículos"
description = ""
weight = 40
+++

La siguiente documentación explica cómo poder ajustar a cero la existencia de todos los artículos, para realizar este proceso es necesario se tomen en cuenta las siguientes recomendaciones:

* Antes de ajustar a ceros las existencias recomendamos realizar un respaldo de la empresa desde Utilerías / Respaldar Información.

* Considerando es un proceso que afecta todo mi catálogo de artículos puede ser que sea algo tardado, por lo que se recomienda realizarse desde el equipo servidor.

* Si usted utiliza series o lotes dentro del sistema, esta opción no es factible, la recomendación en estos casos es darle salida a esta mercancía de manera manual, desde Inventario / Registrar Salidas / Salida por Ajuste a Inventario.

### Instrucciones

1.En el menú ir a Inventario / Proceso de toma de inventario / Documentos de toma de inventario 

2.Se presenta la ventana de "Documentos de toma de inventario" 

3.Hacer clic en el botón de **[Borrar todos]** 

![IMG](1.png)

4.Se abre una ventana de confirmación 

5.Hacer clic en **[Borrar documentos]**

![IMG](confirmar.png)

6.Esto eliminará todos los documentos de toma de inventario. Cerrar la ventana de "Documentos de Toma de Inventario" 

7.Ir al Menú ir a Inventario / Proceso de toma de inventario / Ajustar existencias 

8.Hacer clic en **[Cargar Artículos]**

![IMG](cargar.png)

9.Seleccionar: Cargar todos los artículos (insumos o no insumos según sea el caso) y Cargar existencia actual 

10.Hacer clic en el botón de **[Cargar artículos]**

![IMG](no-insumos.png)

11.Los artículos son cargados en la ventana de "Ajuste de Existencias" y se muestran todos los artículos, la existencia del sistema y la existencia física, **como podemos observar toda la columna de existencia física (conteo) debe de estar en ceros**, ya que no se capturo ningún documento de toma de inventario.

![IMG](columna-en-0.png)

12.Activar las 3 casillas que aparecen en la parte superior: 

* Artículos que hacen falta

* Artículos que sobran 

* Artículos con existencia correcta 

Esto con el fin de enviar la consulta a Excel para tener una copia del inventario que había en el sistema antes de realizar los ajustes, damos clic en el botón de Excel y guardamos el archivo.

![IMG](excel.png)

13.Hacer clic en **[Realizar Ajustes]**

![IMG](realizar-ajustes.png)

14.Se presenta una ventana de confirmación (dos veces) 

![IMG](confimacion-ajustes.png)

15.Se realizan los ajustes (en la parte superior derecha podrá ver el avance del proceso, tome en cuenta que puede ser un proceso tardado dependiendo que tan grande sea su catálogo de artículos).

![IMG](ajustando.png)

16.Ahora todos los artículos habrán pasado a tener existencia cero 

17.Para comprobarlo puede consultar algún reporte de existencias en: Inventario / Reporte de Artículos / Existencias y costos

Como podemos observar la columna de existencia ahora está en 0 a excepción de un artículo, ya que éste es un insumo.

![IMG](reporte-existencias.PNG)

En caso que la existencia no se muestre en 0, importante verificar la expresión de la consulta de la columna Existencia esté bien definida tal y como se muestra en la siguiente imagen:

![IMG](multialm.png)

Si consultamos el kárdex de algún artículo podemos observar que el sistema hizo una Salida por Ajuste o Entrada por Ajuste según sea el caso para poder colocar la existencia en 0.

![IMG](kardex.png)
