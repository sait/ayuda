+++
title = "Error: Ya existe la salida para el corte en PV"
description = ""
weight = 9
+++

### **Indicaciones**:

Al utilizar el menú de Punto de Venta en ocasiones puede suceder que al cerrar el corte el sistema marque el siguiente mensaje debido a elementos externos como problemas de red o apagones de luz.

![IMG](corte-no-se-ha-cerrado.png) 

![IMG](ya-existe-la-salida.png) 

De ser este el caso deberá seguir las siguientes instrucciones para poder cerrar el corte y reprocesar las existencias de los artículos.

1.Primeramente se recomienda generar un respaldo completo de la información en Utilerías / Respaldar Información

2.Tomar nota del corte en cuestión y agregar un 0 al final, ejemplo, el corte que estaremos utilizando para la prueba es el 12256 por lo que quedaría como 122560

3.Entrar a los comendados de VFP, abrir la tabla de CORTES y veriricar los siguientes campos se encuentres vacíos, si están llenos, los deberá borrar

![IMG](tabla-campos-fin.png) 

4.Borrar nota generada de DOCUM y MOVIM con los suiguientes comandos

```
USE DOCUM SHARED

DELETE FOR ALLT(TIPODOC)=='N' AND ALLT(NUMDOC)=='122560'

USE

USE MOVIM SHARED

DELETE FOR ALLT(TIPODOC)=='N' AND ALLT(NUMDOC)=='122560'

USE

5.Borrar salida de inventarios de MINV

USE MINV SHARED

DELETE FOR ALLT(TIPODOC)=='N' AND ALLT(NUMDOC)=='122560'

USE
```

6.Actualizar existencias de acuerdo a los movimientos 

- En Inventario / Verificar Existencias

- Si usa almacenes o sucursales, seleccionar la opcion 'Existencia de almacenes no concuerda con movimientos'

- Si no usa almacenes o sucursales, seleccionar la opcion 'Existencia no concuerda con movimientos' 

- Dar clic en el boton Detectar

    **En este punto es importante copiar con el botón de Clipboard (a la izquierda del botón de la impresora) la lista de artículos que arroje la ventana y pegarlos en un archivo de excel o de texto, para después de dar clic en Arreglar, verificar físicamente las existencias de dichos artículos, ya que puede haber una diferencia entre lo físico y del sistema**

- Haga clic en Arreglar Existencias

![IMG](VERIFICAR.png) 

7.Actualizar el campo Arts.VENTACORTE con las ventas del corte activo

- En Inventario / Verificar Existencias

- Seleccionar la pestaña Punto de Venta

- Hacer clic en 'Verificar'

![IMG](VERIFICAR2.png) 

- Hacer clic en 'Arreglar'

![IMG](ARREGLAR.jpg) 





