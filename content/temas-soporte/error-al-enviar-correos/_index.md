+++
title = "Error al Enviar Correos"
description = ""
weight = 12
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="error-al-enviar-correos.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Si al momento de enviar correos desde SAIT se le muestra el siguiente mensaje de error:

![IMG](error.png)
 
Se entiende hay algún detalle el cual está bloqueando la salida de correos de manera habitual, el cual puede ser ocasionado por distintitos motivos, por lo que a continuación las posibles soluciones:

<h3 class="text-primary">MENÚ</h3>

* [Cambio de contraseña del correo](#cambio-de-contraseña-del-correo)
* [Habilitar contraseña para aplicaciones de terceros](#habilitar-contrasena-para-aplicaciones-de-terceros)
* [Agregar exclusiones en antivirus ](#agregar-exclusiones-en-antivirus )
* [Aumentar tiempo de espera](#aumentar-tiempo-de-espera)

## Cambio de contraseña del correo

Uno de los motivos principales por el cual se nos muestra un error al tratar de enviar un correo es que se haya modificado la contraseña del correo directamente desde el portal de Gmail, sin embargo, nunca se modificó en SAIT, es muy importante modificar también la contraseña en el sistema para no tener inconvenientes al enviar correos por medio de SAIT.

Para modificar la contraseña del correo en SAIT siga las siguientes instrucciones:

1.En el menú ir a Utilerías / Catálogo de Usuarios

2.Se presenta la ventana de Catálogo de Usuarios, Seleccione el usuario al que desea actualizar la contraseña del correo y de clic en **[Modificar]**

![IMG](modificar-usuario.png)

3.Se presenta la ventana de Modificar Usuario y daremos clic en **[Configurar Correo]**

![IMG](configurar-correo.png)
 
4.Se muestra la ventana de Configurar Envío de Correo y en el campo de Contraseña ingresamos la nueva contraseña de nuestro correo electrónico, y daremos clic en **[Continuar]**

![IMG](contrasena.png)

5.Finalmente damos clic en **[Modificar]** y listo.

**En caso de que sean varios usuarios los que tienen el mismo correo, hay que actualizar también la contraseña en cada uno de ellos.**


## Habilitar contrasena para aplicaciones de terceros

A partir del mes de junio de 2022 Google hizo cambio en sus políticas para el envío de correos utilizando aplicaciones de terceros, tal como lo es SAIT

Por lo que para poder seguir usando una cuenta de GMAIL con SAIT deberá de realizar el siguiente proceso:

PASO 1) Activar el acceso 2FA en su cuenta Google (2FA significa: 2 Factores de Autenticación)

PASO 2) Crear una contraseña para la aplicación que utiliza el correo.

PASO 3) Agregar la contraseña en su aplicación SAIT	

Es importante notar:

*	Para activar 2FA en Google, usted debe proporcionar un celular que reciba mensajes SMS.

*	Una vez activado 2FA en su cuenta Google, cada vez que quiera acceder a su GMail en un dispositivo nuevo, deberá:

*	Escribir usuario y contraseña

*	Verificar un código que recibirá mediante un SMS 

Enseguida se detallan los pasos a seguir, mediante imágenes.

### PASO 1) Activar el acceso 2FA en su cuenta Google

![IMG](11.png)

![IMG](22.png)

![IMG](33.png)

![IMG](44.png)

![IMG](55.png)

![IMG](66.png)

### PASO 2) Crear una contraseña para la aplicación que utiliza el correo.

![IMG](77.png)

![IMG](88.png)

![IMG](99.png)

![IMG](100.png)

![IMG](110.png)

### PASO 3) Agregar la contraseña en su aplicación SAIT	

<h3 class="text-primary">Agregar contraseña por usuario</h3>


-	Ir a Utilerías/Catálogo de Usuarios
-	Seleccionar el Usuario
-	Clic en el botón de “Modificar”
-	Clic en el botón de “Configurar Correo”
-	Escribir la nueva contraseña
-	Clic en “Continuar”
-	Clic en "Modificar"

![IMG](correo-usuario.png)

<h3 class="text-primary">Agregar contraseña en OCF para recibir facturas de proveedores</h3>

- Ir a Utilerías/Configuración General de Sistema/Pestaña Otros
- Clic en el botón “Configurar Organizador de Comprobantes Fiscales (OCF)”
- Pegar contraseña
- Click en “Guardar Configuración

![IMG](correo-recibir.png)

<h3 class="text-primary">Agregar contraseña en OCF para el envío automático de facturas y envío de REPS</h3>

- Ir a Utilerías/Factura Electrónica/Configurar CFDI
- Pegar contraseña
- Clic en “Guardar Configuración”

![IMG](correo-enviar.png)

## Agregar exclusiones en antivirus 

Un paso importante para que SAIT funcione sin interferencias o bloqueos externos es agregar exclusiones en el antivirus que tenga instalado en cada uno de sus estaciones y servidor.
Dependiendo del antivirus la sección de exclusiones se encuentra en distintas ubicaciones, para efectos del ejemplo mostraremos cómo agregar exclusiones en el antivirus Avast.


### El primer paso es deshabilitar la opción de Analizar correos electrónicos salientes (SMTP):

![IMG](1.png)

![IMG](2.png)

![IMG](correos-smtp.png)
 
### Y después agregar las exclusiones de manera convencional de la carpeta donde esté la instalación de SAIT:

![IMG](1.png)

![IMG](2.png)

![IMG](3.png)

![IMG](5.png)

![IMG](6.png)

![IMG](7.png)

## Aumentar tiempo de espera

Si no ha cambiado la contraseña de su correo y solo dejó de enviar correos, en ocasiones puede suceder que el Internet esté más lento de lo normal y esto provoque que los correos no se envíen, si este es el caso, se recomienda aumentar de 20 a 26 segundos el tiempo de espera

![IMG](26.png)
 
 

 
 
 

 
 
 
 
 


