+++
title = "Cannot locate de Microsoft Visual FoxPro support library"
description = ""
weight = 25
+++

Para algunos procesos, como configuracion de respaldos automáticos con la utilería SAITBKUP o convertir a multialmacenes puede aparecer el siguiente mensaje de error: 
 
![IMG](cannot.png)
 
Para corregir debeará seguir los siguientes pasos:

1.Abrir su sistema SAIT como Administrador

![IMG](admin.jpg)

2.Clic en Catálogo de Empresas 

3.Clic en Agregar Empresa Existente

4.Seleccionamos la DEMO ubicada en el disco C

![IMG](demo.png)

5.Ingresamos a la empresa, probablemente le pida actualizar la base de datos, le decimos que sí, y de igual forma si pide bajar librerías daremos clic en SÍ

6.Una vez dentro, nos dirigiremos a Utilerías / Configuración General del Sistema y daremos clic aquí (recordar que para este punto se tuvo que abrir como administrador el sistema SAIT)

![IMG](dlls.jpg)

7.Nos aparecerá la ventana de confirmación y daremos clic en SÍ

![IMG](confirmar.jpg)

8.Finalmente, si deseamos asegurarnos las librerías se hayan copiado de manera correcta, volvemos a dar clic en Copiar DLLs, si aparece este mensaje es que si se copiaron 

 ![IMG](renombrar.jpg)
