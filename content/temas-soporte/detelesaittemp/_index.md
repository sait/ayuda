+++
title = "Error DELETESAITTEMP"
description = ""
weight = 22
+++

Este mensaje de error puede aparecer al cerrar el sistema y sucede porque no se están borrando los temporales correctamente, el sistema por sí solo, los va eliminando en automático, pero si no se borraron y se fueron acumulando, este mensaje se estará mostrando

![IMG](DELETE.png)

![IMG](DELETE2.png)

Para solucionarlo deberá entrar a la carpeta de instalación de SAIT, para esto deberá dar clic derecho en el ícono del sistema y dar clic en Abrir Ubicación del Archivo

![IMG](1.png)

Entrar a la carpeta TEMP y borre todos archivos

![IMG](2.png)

Al entrar y cerrar el sistema ya no debe aparecer el error

Para que no suceda de nuevo es necesario verificar que SAIT esté bien instalado en el panel de control y que tenga todos los permisos

