+++
title = "Configurar Leer Peso en Bascula"
description = ""
weight = 32
+++


El sistema SAIT puede comunicarse con varias básculas utilizando el puerto serial COM. Actualmente se soportan las siguientes marcas:

* CAS PD-II ECR TYPE 2
* Metrologic MS2320 Prtcl:NCR-unicable
* Torrey
* Cardinal AS-330D 333D y 334D
* Genéricas

{{%alert success%}}Los puntos básicos a conocer previamente son:{{%/alert%}}

* Puerto COM a usar
* Configuración de Comunicacion con la bascula (Velocidad, Paridad, Handshaking)
* Protocolo a usar o caracteres a enviar como comando a la bascula para solicitar peso

En el manual de la báscula normalmente se encuentra todo eso, pero se puede investigar por otros lados.

* [Configurar en SAIT Leer Peso](#configurar-en-sait-leer-peso)
* [Configuración de Basculas de Forma Genérica](#configuración-de-basculas-de-forma-genérica)
* [Configuración para Magellan PSC 384](#configuración-para-magellan-psc-384)
* [Configuración para CAS PD-II](#configuración-para-cas-pd-ii)
* [Configuración para CAS ER Plus](#configuración-para-cas-er-plus)
* [Configuración para Torrey L-EQ Series](#configuración-para-torrey-l-eq-series)



### Configurar en Sait leer Peso

Para definir el tipo de báscula en su sistema vaya a: **Utilerías / Configuración General / Caja-Punto de Venta / Config Bascula**

![IMG](imagen1.PNG)


{{%alert success%}}Puerto COM a usar{{%/alert%}}

Revisar si la PC tiene puerto serial Revisar en Sistema/Administrador de Dispositivos los puertos seriales que tiene la PC, tomar nota de los números de puerto (COM1, COM2, etc).


{{%alert success%}}Configuracion de Comunicacion con la bascula{{%/alert%}}

Revisar el manual de la bascula para saber la configuracion de comunicacion, anotar: Velocidad en Bauds por segundos (bps): normalmente 9600 bps Tipo de paridad: N None / Ninguna (Default) E Even / Par O Odd / Impar S Space / Espacio M Mark / Marcador Data Bit / Tamaño de la palabra en bis Normalmente: 8 bits Stop Bit (Bits de parado) normalmente 1 Handshaking (Saludo inicial): None = 0 (Default) SW Xon/Xoff = 1 HW RTS/CTS = 2 Ambas = 3 En caso de no encontrar en el manual la configuracion de velocidad usar: 9600,N,8,1 Handshaking: 0

{{%alert success%}}Protocolo de Comunicación con la báscula{{%/alert%}}

* Es lo más dificil de encontrar, ya que en los manuales normalmente NO lo ponen los fabricantes.

* Puede probar usando el tipo de bascula: CAS PD-II Protocolo Type 2 Comando para LeerPeso: Chr(87) o "W" La bascula responde con chr02 wwwww chr13 en donde wwwww es el peso en kilos utilizando 3 decimales

* Otro protocolo es el usado por Torrery (Mexico) Comando para LeerPeso: "K" La bascula responde con: * 32 32 48 46 49 49 53 32 107 103 13 * sp sp 0 . 1 1 5 sp k g CR es decir el peso en Kilo gramos y algunos caracteres de espacio Chr(32)

* Para hacer pruebas del protocolo use la ventana de:Utilerías / Config General / Punto de Venta / Config Bascula / Prueba de Comunicacion.

* Para enviarle algunos comandos a la bascula para analizar lo que responde.

* Previamente debe cargar la bascula con algun peso fijo por decir una botella de 500 ml de agua.

* Con eso podra revisar la respuesta que arroja la bascula y confirmar con el manual

![IMG](imagen2.PNG)


**Es importante mencionar que algunas basculas tienen la opción de cambiar el protocolo a usar. Esa configuracion se hace por:**

* Dip Switches en la parte trasera.
* Panel de control en la parte delantera.
* Esa información tambien se debe encontrar en el manual.

### Configuración de Basculas de Forma Genérica

Si la bascula que desea utilizar no se encuentra en la lista que se menciono anteriormente, existe la opción de configuración genérica.

{{%alert success%}}Los puntos básicos a conocer previamente son:{{%/alert%}}

* Puerto COM a usar
* Configuración de Comunicacion con la bascula (Velocidad, Paridad, Handshaking)
* Protocolo a usar o caracteres a enviar como comando a la bascula para solicitar peso

En el manual de la báscula normalmente se encuentra todo eso, pero se puede investigar por otros lados.

Para definir el tipo de báscula en su sistema vaya a: Utilerías / Configuración General / Caja-Punto de Venta / Config Bascula.

![IMG](imagen3.PNG)


{{%alert success%}}Puerto COM a usar{{%/alert%}}

Revisar si la PC tiene puerto serial Revisar en Sistema/Administrador de Dispositivos los puertos seriales que tiene la PC, tomar nota de los números de puerto (COM1, COM2, etc)

{{%alert success%}}Configuración de Comunicación con la bascula{{%/alert%}}

Revisar el manual de la bascula para saber la configuracion de comunicacion, anotar: Velocidad en Bauds por segundos (bps): normalmente 9600 bps Tipo de paridad: N None / Ninguna (Default) E Even / Par O Odd / Impar S Space / Espacio M Mark / Marcador Data Bit / Tamaño de la palabra en bis Normalmente: 8 bits Stop Bit (Bits de parado) normalmente 1 Handshaking (Saludo inicial): None = 0 (Default) SW Xon/Xoff = 1 HW RTS/CTS = 2 Ambas = 3

En caso de no encontrar en el manual la configuracion de velocidad usar: **9600,N,8,1 Handshaking: 0**

![IMG](imagen4.PNG)


{{%alert success%}}Protocolo de Comunicación con la báscula{{%/alert%}}

Es lo más difícil de encontrar, ya que en los manuales normalmente NO lo ponen los fabricantes.

Para hacer pruebas del protocolo use la ventana de:Utilerías / Config General / Caja-Punto de Venta / Config Bascula / Prueba de Comunicación.

Para enviarle algunos comando a la bascula para analizar lo que responde.

Previamente debe cargar la bascula con algún peso fijo por decir un objeto de 0.200 Kg.

Con eso podrá revisar la respuesta que arroja la bascula y confirmar con el manual.

![IMG](imagen5.PNG)

El ejemplo esta basado en la bascula CAS ERplus

Al mandar el protocolo de comunicación la bascula nos da la respuesta: 10 32 48 46 50 48 48 32 107 103 3 (sp 0.200 sp Kg CR ) esta secuencia te informar el peso en kilogramos.

**Es importante mencionar que algunas basculas tienen la opción de cambiar el protocolo a usar. Esa configuración se hace por:**

* Dip Switches en la parte trasera
* Panel de control en la parte delantera
* Esa información también se debe encontrar en el manual.

{{%alert success%}}Configuración de Protocolo ( Genérica ){{%/alert%}}

Una vez que la comunicación de la bascula con SAIT generó la respuesta de manera satisfactoria, se tiene que configurar el protocolo de comunicación genérico de la siguiente manera:

De la respuesta generada en el ejemplo:10 32 48 46 50 48 48 32 107 103 3 se tienen que obtener los datos que te solicita la ventana.

* Cadena pedir peso: este dato se obtiene del código ASCII enviado de comunicación en este caso es: 87,13 Y quedaría de la siguiente manera CHR(87)+ CHR(13)

* Cadena fin respuesta: Este valor se obtiene del último número de la respuesta de la basculas:10 32 48 46 50 48 48 32 107 103 **3**, en este caso es el 3 y quedaría CHR(3)

* Pos Inicial de peso: este dato se obtiene de las posiciones de los valores de la respuesta:10 **32** 48 46 50 48 48 32 107 103 3, en este caso el valor del peso inicia en la posición 2

* Pos Final de peso: este dato se obtiene de las posiciones de los valores de la respuesta:10 32 48 46 50 48 **48** 32 107 103 3, en este caso el valor del peso final es la posición 7

* Cantidad de Decimales: este dato se obtiene de las posiciones de los valores de la respuesta:10 32 48 46 **50 48 48** 32 107 103 3, en este caso el valor del peso final es la posición 3

Estos valores quedarían definidos de la siguiente manera:

![IMG](imagen6.PNG)

Esta es la manera de realizar una configuración genérica de comunicación de su báscula con SAIT.

{{%alert success%}}Configuración de Puerto COM para Bascula Genérica{{%/alert%}}

El sistema SAIT puede comunicarse con varias básculas utilizando el puerto serial COM y la configuración en modo "Genérica" como se comenta anteriormente.

Antes que nada debemos saber como cambiar la configuración de los puertos COM para el modelo de bascula deseado. (Propiedades a configurar: Baud rate, parity, data bit and stop bit)

**Configuración de Puerto COM**

* Ir a Administrador de Dispositivos y a las propiedades del puerto.
* ![IMG](imagen7.PNG)
* Seleccionar la pestaña de Configuración
* ![IMG](imagen8.PNG)
* Por ultimo es colocar la configuración requerida para la bascula a utilizar.

### Configuración para Magellan PSC 384

![IMG](imagen9.PNG)

{{%alert success%}}Cadena pedir peso: CHR(87)<br>Cadena fin respuesta: CHR(13)<br>Pos Inicial de peso: 2<br>Pos Final de peso: 7<br>Cantidad de Decimales: 3<br>Baud rate, parity, data bit and stop bit: 9600,E,7,1{{%/alert%}}

### Configuración para CAS PD-II

![IMG](imagen10.PNG)

{{%alert success%}}Cadena pedir peso: CHR(87)<br>Cadena fin respuesta: CHR(13)<br>Pos Inicial de peso: 2<br>Pos Final de peso: 7<br>Cantidad de Decimales: 3<br>Baud rate, parity, data bit and stop bit: 9600,E,7,1{{%/alert%}}

### Configuración para CAS ER Plus

![IMG](imagen11.PNG)

{{%alert success%}}Cadena pedir peso: CHR(87)+ CHR(13)<br>Cadena fin respuesta: CHR(13)<br>Pos Inicial de peso: 2<br>Pos Final de peso: 7<br>Cantidad de Decimales: 3<br>Baud rate, parity, data bit and stop bit: 9600,N,8,1{{%/alert%}}



### Configuración para Torrey L-EQ Series

![IMG](imagen12.PNG)

{{%alert success%}}Cadena pedir peso: CHR(80)<br>Cadena fin respuesta: CHR(32)<br>Pos Inicial de peso: 2<br>Pos Final de peso: 6<br>Cantidad de Decimales: 3<br>Baud rate, parity, data bit and stop bit: 9600,N,8,1{{%/alert%}}







