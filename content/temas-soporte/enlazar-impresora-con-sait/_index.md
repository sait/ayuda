+++
title = "Enlazar impresora de tickets con SAIT"
description = ""
weight = 54
+++

A continuacion se detallan los pasos a seguir para configurar su miniprinter con SAIT y habilitar la impresión automática de formatos como nota de venta, comprobante de movimiento en caja y cortes.


1.Ir a Panel de control \ Hardware y sonido \ Dispositivos e impresoras \ Ubique su impresora

![IMG](1.png)

2.Dar clic derecho y seleccionar Propiedades de Impresora

![IMG](2.png)

3.Clic en Cambiar opciones de uso compartido

![IMG](3.png)

4.Indicar el nombre del recurso compartido, para efectos de la prueba le llamaremos NOTAS, habilite la opción de Compartir esta impresora, clic en Aceptar para guardar cambios

![IMG](4.png)

5.Ir a SAIT a Utilerías / Configuración General del Sistema / Puertos MS-DOS

![IMG](5.png)

6.Dar clic en Nuevo y en Modificar destino colocar lo siguiente

 \\\ NOMBRE DEL EQUIPO \ NOMBRE DEL RECURSO COMPARTIDO

![IMG](6.png)

7.Si no conoce el nombre del equipo, en esta misma ventana, de clic en la pestaña Otros y ahí se indica el nombre 

![IMG](7.png)

8.Agregar ruta de impresión directa a notas de venta en Utilerías / Modificar Formatos / Notas de Venta y en la sección de Esta computadora coloque la ruta de la impresora.

![IMG](8.png)

9.Agregar ruta de impresión directa a nota de venta en Utilerías / Modificar Formatos /Corte de Caja y en la sección de Esta computadora coloque la ruta de la impresora (hace lo mismo con cada uno de los reportes ya que la tira del corte está conformada por pequeños reportes).

![IMG](9.png)

10.Agregar ruta de impresión directa movimientos de caja, diríjase a Caja / Tipos de Movimientos y Seleccione el primer movimiento de RETIRO DE EFECTIVO A BÓVEDA y de clic eb Modificar

![IMG](10.png)

11.De clic en Formatos

![IMG](11.png)

12.Finalmente en la sección de Esta computadora coloque la ruta de la impresora, de clic en la X para salir de la ventana y asegúrese de dar clic en Modificar en la ventana de Modificar Tipos de Movimientos de Caja

![IMG](12.png)



