+++
title = "Exportar e Importar Reportes"
description = ""
weight = 28
+++

SAIT permite la creación / instalación de reportes y formatos, que en algunos casos es necesario copiar a otra empresa, o bien, a otra sucursal. Por tal motivo, el sistema se encuentra preparado para la exportación de los formatos ya una vez instalados. 

Cabe aclarar que por el momento esta opción no se encuentra en una sección en particular en el sistema, sino que se localiza en cada ventana de Configuración de Reportes. 

### EXPORTAR REPORTES

A continuación, se presentará un ejemplo de la exportación de un reporte que se encuentra en ventas: 

1.Ir a Ventas / Reportes de Ventas / Configurar reportes de documentos de ventas 

2.Seleccionar un reporte de la sección 'Reportes disponibles'

3.Clic en **Exportar**
 
![IMG](exportar.png)

4.El sistema nos preguntara donde guardar el archivo, clic en **Guardar**

![IMG](guardar.PNG)
 
5.Si el proceso se hizo correctamente nos mostrara "Reporte ha sido grabado" 

![IMG](grabado.PNG)
 
### IMPORTAR REPORTES

1.Deberá dirigirse al menú de Utilerías / Recibir Reportes 

2.El sistema le pide localizar el archivo con terminación RPT 

![IMG](importar.png)
 
3.Se presenta la ventana de "Recibir Reportes" con el nombre del archivo seleccionado. 

![IMG](recibir.png)
 
4.Haga clic en el botón de **Recibir**

5.Si el reporte ya existe el sistema confirma que lo va a sustituir 

6.Se cierra la ventana de "Recibir Reportes" 

7.Listo el reporte ha sido instalado en su sistema 

8.Haga un cambio de usuario para que el menú de reportes se actualice. 
