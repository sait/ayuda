+++
title = "Configurar Monitoreo de Artículos"
description = ""
weight = 34
+++

Esta opción le permite monitorear artículos que la empresa considere como importantes, este monitoreo se imprimirá al cerrar el corte de caja.

Dicha configuración le permite visualizar las ventas de estos artículos y puede ser de mucha utilidad para tener un control más preciso sobre las existencias y ventas de artículos que son de vital importancia para la empresa ya sea por el tipo de artículo del que se trate, costo, tiempos de entrega al ordenarlos al proveedor, etc. 

Para poder realizar esta configuración en SAIT deberá seguir las siguientes instrucciones en SAIT: 

### Configurar manejo de artículos monitoreados 

1.	Entre al menú de Utilerías / Configuración General del Sistema 

2.	Seleccione la pestaña de Inventario 

3.	Active la casilla [ * ] Monitorear en Corte Y 

4.	Haga clic en **[Cerrar]** 

![IMG](config.JPG)

### Activar artículos a monitorear

1.	Entre al menú de Inventario / Catálogo de Artículos 

2.	Capture la clave del artículo 

3.	Active la casilla [ * ] Monitorear en Corte Y 

4.	Haga clic en **[Grabar]**

![IMG](articulo.JPG)


Imprimir reporte de artículos monitoreados en corte
Una vez que se han identificados los artículos a monitorear, al imprimir el corte de caja se imprime al final del corte un reporte con el detalle de artículos monitoreados: 
 
![IMG](corte.JPG)