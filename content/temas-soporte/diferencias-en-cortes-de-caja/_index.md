+++
title = "Diferencias en Cortes de Caja, posibles causas y soluciones"
description = ""
weight = 46
+++

### PROBABLES MOTIVOS EN DIFERENCIAS DE IMPORTES TOTALES DE UN CORTE DE CAJA

Posiblemente en más de una ocasión se ha presentado el problema de que los importes totales de su corte de caja en SAIT no coinciden y no logra identificar con exactitud el motivo por el que sucedió esto.

Previamente recomendamos que descargue el siguiente reporte (auxiliar-de-corte-de-caja.rpt) dando clic [aquí](auxiliar-de-corte-de-caja.rpt)

Posteriormente deberá instalarlo desde el menú de utilerías / recibir reportes.

Este reporte lo encontrará en el menú de Ventas / Reporte de ventas / Auxiliar de corte de caja. 
 
A continuación le enlistamos una serie de probables motivos y las recomendaciones que SAIT le sugiere tomar en cuenta para identificar y en todo caso prevenir los inconvenientes presentados:

* [Tasa de impuesto incorrecta](#tasa-de-impuesto-incorrecta)
* [Articulos eliminados](#articulos-eliminados)
* [Movimiento de caja no cancelado](#movimiento-de-caja-no-cancelado)
* [Notas con importe en 0](#notas-con-importe-en-0)
* [Descuentos en PjeDesc2, PjeDesc3, PjeDesc4](#descuento-en-pjedesc2-pjedesc3-pjedesc4)
* [Articulos duplicados](#articulos-duplicados)
* [Sobrantes](#sobrantes)

#### TASA DE IMPUESTO INCORRECTA

Es importante identificar si recientemente se registró un nuevo artículo o se modificaron los datos de algún artículo dentro del sistema, porque de ser así, puede ser que el motivo de la diferencia es porque el usuario definió incorrectamente la tasa de impuesto del IVA o del IEPS de ese producto en específico. 
La diferencia en el corte de caja se visualiza entre el ‘Resumen de Ventas por Tipo de Impuesto’, y ‘Ventas por Línea’, como se muestra a continuación:

![IMG](1.png)
   
Para confirmar si este es el caso, le sugerimos apoyarse del reporte auxiliar de corte de caja. 

Podrá filtrar la información por rango de fechas y consultar en [Hoja], éste desglosará cada uno de los productos vendidos en esa fecha en específico. Deberá revisar los valores en las columnas de IVA, IEPS, EXENTO.

![IMG](2.png)
 
En caso de encontrar el artículo con tasa de impuesto incorrecta deberá anotar el folio del documento y cancelarlo desde el módulo de ventas. Le sugerimos registrar la venta nuevamente en un nuevo corte, una vez que la tasa de impuesto del producto haya sido corregida.

Es importante mencionar que el corte de caja quedará registrado de forma incorrecta y que ya no hay forma de ser corregido. Se recomienda tener mucho cuidado al capturar o modificar los datos de los productos.
 
#### ARTICULOS ELIMINADOS 

Otro de los motivos por los que no concuerdan los totales de la tira de corte puede ser porque se haya eliminado de manera intencional o erróneamente uno de los productos que se incluyó en ese corte de caja. 
La diferencia en el corte de caja se visualiza entre el ‘Resumen de Ventas por Tipo de Impuesto’, y ‘Ventas por Línea’, como se muestra a continuación:

![IMG](3.png)
   
Para confirmar si este es el caso, le sugerimos apoyarse del reporte auxiliar de corte de caja. 

Podrá filtrar la información por rango de fechas y consultar en [Hoja], éste desglosará cada uno de los productos vendidos en esa fecha en específico. Deberá revisar la columna de descripción, e identificar si marca algún dato como NULL, aquellos productos que se muestren con descripción vacía quiere decir que fueron eliminados del sistema. 

![IMG](4.png)
 
Se sugiere volver a registrar el producto con la misma clave y las mismas especificaciones (precios, impuestos, etc.) con el fin de que los importes en la tira del corte de caja se muestren correctamente y puedan reimprimir la tira del corte de caja.

Para evitar que se vuelva a presentar este detalle, SAIT le sugiere estar actualizado a la versión más reciente del sistema, ya que a partir de la versión **2018.55** se agregó la mejora para restringir que al eliminar artículos, solo se permita cuando cuenten con existencia 0.

![IMG](5.png)

![IMG](6.png)

 
#### MOVIMIENTO DE CAJA NO CANCELADO

Otro de los motivos por el que puede haber una diferencia en la tira del corte de caja es porque el usuario realizó una cancelación de un documento de forma incorrecta. 

Es IMPORTANTE mencionar que al manejar CAJA las cancelaciones de documentos de venta de contado deben realizarse desde el módulo de Caja / Cancelar Movimientos para que se dé retroceso al pago recibido y al documento emitido y no desde el módulo de ventas como comúnmente se realiza el cual afecta únicamente al documento emitido pero en ningún momento afecta el pago.

Este detalle es muy fácil de identificar ya que en la tira del corte se muestra la relación de los documentos cancelados y la relación de los pagos cancelados. 

Cuando la cancelación se realiza desde el módulo de CAJA siempre deberá estar el documento en ambos apartados, de lo contrario solamente aparecerá la relación solamente del documento pero no del pago. Por lo tanto en la tira del corte saldrá como faltante este importe, como se aprecia en la siguiente imagen:

![IMG](7.png)   

A partir de la versión **2019.22** se incluyó la mejora en SAIT de que si se está manejando CAJA obligue al usuario a realizar las cancelaciones de contado desde el módulo de CAJA y detiene al usuario si desea realizar este proceso desde ventas, con el fin de evitar este tipo de errores por parte del usuario. 
En este caso la recomendación es que cuente con la versión más reciente del sistema.
 
![IMG](8.png)   
 
#### NOTAS CON IMPORTE EN 0

A partir de la versión 2019.8 se incluyó la corrección para grabar correctamente documentos con importe en 0 desde caja.

#### DESCUENTO EN PJEDESC2 PJEDESC3 PJEDESC4 

La tira del corte de caja que el sistema trae por default SAIT considera el porcentaje de descuento normal, pero si se otorga un segundo o tercer descuento el formato del corte no lo va a considerar y por lo tanto no van a cuadrar los importes totales. 

Cuando el motivo de la diferencia es un porcentaje de descuento no considerado, la tira del corte se presenta de la siguiente manera:

![IMG](9.png)   
   
Para confirmar si este es el caso, le sugerimos apoyarse del reporte **auxiliar-de-corte-de-caja.rpt**

Podrá filtrar la información por rango de fechas y consultar en [Hoja], éste desglosará cada uno de los productos vendidos en esa fecha en específico. Deberá revisar las columnas de descuento2, descuento3 o descuento4, e identificar si alguna de estas columnas tiene un valor que concuerde con la diferencia en el corte de caja. 

![IMG](10.png)   

Le recomendamos descargar los formatos actualizados del corte de caja con el desglose correcto, considerando los porcentajes de descuentos adicionales. En este caso, solo deberá reemplazar el formato y reimprimir la tira del corte para confirmar que la información ya se muestre correctamente. 

* corte caja 02a (ventas por linea) - con descuentos 2,3,4.rpt - Para descargar el reporte de clic [aquí](corte-caja-02a-ventas-por-linea-con-descuentos-2,3,4.rpt)

* corte caja 02a (ventas por linea) - con ieps y descuentos 2,3,4.rpt - Para descargar el reporte de clic [aquí](corte-caja-02a-ventas-por-linea-con-ieps-y-descuentos-2,3,4.rpt)

* corte caja 02b (resumen-de ventas) - con descuentos.rpt - Para descargar el reporte de clic [aquí](corte-caja-02b-resumen-de-ventas-con-descuentos.rpt)

#### ARTICULOS DUPLICADOS

También, en ocasiones se puede presentar que en la base de datos de artículos se encuentre duplicada la clave de un artículo, lo que ocasiona que en la tira del corte se considere dos veces el importe total arrojando erróneamente la información entre la sección de ‘Resumen de Ventas por Tipo de Impuesto’ contra los ‘Movimientos de Caja’ registrados.

![IMG](11.png)  
   
Para identificar este detalle deberá descargar el siguiente reporte como apoyo para identificar fácilmente si este es el problema: Articulos duplicados.rpt

Para instalarlo, deberá ir al menú de utilerías / recibir reportes.

Una vez instalado, deberá ir al Menú de Inventario / Reporte de Artículos / Consultas Avanzadas, y ejecutar el reporte en [Hoja] de “Artículos Duplicados”.

![IMG](12.png) 

Para descargar el reporte de clic [aquí](articulos-duplicados.rpt)
 
Si el reporte te muestra algún resultado, deberá tomar nota de las claves que aparecen duplicadas y la recomendación es que un ASESOR SAIT o personal de sistema de su empresa ingrese vía base de datos en el menú de Ayuda / Comandos de Visual FoxPro para eliminar manualmente el o los registros duplicados.

**IMPORTANTE: Se recomienda que antes de realizar este proceso genere un respaldo de la información y tome en cuenta que si maneja enlace de sucursales deberá hacer este proceso en cada sucursal.**

Teclear la siguiente expresión en la ventana de comandos de VFP:

USE ARTS SHARED

SET FILTER TO ALLT(NUMART)==’CLAVE DEL PRODUCTO’

BROW 

Y marcar la casilla sobre el renglón con el articulo incorrecto o duplicado en color negro o presionar CTRL+T

USE

SALIR

![IMG](13.png) 

#### SOBRANTES

Cuando se detecta un importe considerable de sobrante, y se ha verificado que ninguno de los puntos mencionados anteriormente se ha presentado, el último punto a verificar es que no haya se haya tenido pérdida de información por motivos de apagones de luz, fallas de red o de equipo.

Para verificar este punto solo es necesario que se dirija al menú de Ventas / Consultas Generales y asegurarse que el folio consecutivo de las notas de venta y facturas estén correctos y no hagan falta folios de documentos.

En caso de que sí haya presentado pérdida de información deberá comunicarse con su ASESOR SAIT o personal de sistema de su empresa.

Si este no es el caso, el sobrante puede deberse a un error del usuario, ya sea al contabilizar las monedas y billetes al momento de entregar el corte de caja o que haya realizado una transacción incorrecta en las operaciones del día. 

