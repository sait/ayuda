+++
title = "Error abortando Problema en getmsl() imposible abrir archivo: config.msl en modo monitoreo"
description = ""
weight = 18
+++

Si al momento de ejecutar su sistema SAIT le aparece el siguiente mensaje de error y después no le permite continuar

![IMG](abortando.png)

Deberá seguir las siguientes recomendaciones.

<h3 class="text-primary">Recomendaciones SAIT para corregir este mensaje de error</h3>

- **Antivirus**: Corroborar que el antivirus que está instalado, tanto en estaciones como en servidor tenga agregadas las exclusiones del sistema SAIT de manera correcta, si usted ya había agregado previamente estas exclusiones, pero el sistema le arroja este error es posible que el antivirus se haya actualizado por lo que deberá agregarlas nuevamente.

- **Permisos de escritura en carpeta compartida**: Corroborar la carpeta donde se encuentra la base de datos NO esté sólo como lectura, para corregirlo deberá dar todos los permisos de lectura y escritura, de igual forma si se conecta por medio de usuario la unidad de red de las estaciones y se tiene un usuario de Windows con acceso restringido, se deberá cambiar a usuario tipo administrador. Si la carpeta donde está base de datos no tiene el permiso de TODOS, deberá agregarla, dado clic derecho sobre la carpeta / Propiedades / Uso Compartido / Compartir

![IMG](todos.jpg)

- **Red privada**: Asegurarse todos los equipos de la red, estén con la configuración de red PRIVADA

![IMG](privada.jpg)

- **Permisos para compartir archivos**: La ubicación de esta configuración la encuentra en Panel de Control / Redes e Internet / Centro de redes y recursos compartidos / Cambiar configuración de uso de compartido avanzado, habilite todos los permisos indicados:

![IMG](centro.png)

![IMG](activar.png)








