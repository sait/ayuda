+++
title = "Error: Object file sait.exe was compiled in a different version of FoxPro"
description = ""
weight = 11
+++
![IMG](1.png)

1.Ir a directorio donde se encuentra instalado sistema. Ejemplo: C:\SAIT 

Si no sabe la ubicación puede dar clic derecho sobre su ícono SAIT que está en el escritorio y seleccionar la opción de Abrir Ubicación
 
![IMG](2.png)

2.Ubicar el ejecutable del sistema con extensión .exe 

* **SAIT ERP:** ---------> sait.exe 

* **SAIT Básico:** ------> saitbasico.exe 

* **SAIT Nómina:** ----> saitnom.exe 

* **SAIT Cont:** -------> saitcont.exe 

* **SAIT Dist:** --------> saitdist.exe 

3.Renombrar archivo a **sait1.exe** dando clic derecho en el ejecutable y cambiar el nombre.
 
![IMG](3.png)

4.Crear acceso directo en escritorio o desde cualquier parte de donde se vaya acceder a SAIT normalmente. 

Ejemplo: Clic derecho en archivo / Enviar a / Escritorio (crear acceso directo) 

![IMG](4.png)

5.Probar nuevamente iniciar SAIT 

Posiblemente el mensaje se vuelva a presentar al acceder de nuevo al sistema por primera vez de aplicar lo anterior, pero solo debe abrir de nuevo el ejecutable.