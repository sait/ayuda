+++
title = "Símbolos raros en lugar de acentos en el sistema"
description = ""
weight = 50
+++

Si en su sistema aparecen los acentos de la siguiente manera, lo más probable es que tenga habilitada una opción que provoca este detalle

![IMG](ACENTOS.png)

A continación explicamos como deshabilitar la opción "Beta: Usar Unicode UTF-8 para compatibilidad mundial con idiomas" 

### Deshabilitar opción en antiguo Panel de Control 

1) Panel de Control / Reloj y Región / Cambiar formats de fecha, hora o número

![IMG](1.png)

2) Pestaña Administración y clic en Cambiar configuración regional del sistema

![IMG](2.png)

3) Deshabilitar la opción indicada y dar clic en ACEPTAR: 

![IMG](3.png)

### Deshabilitar opción en nuevo Panel de Control 

1) Configuración / Hora e Idioma 

![IMG](4.png)

2) Idioma y Región 

![IMG](5.png)

3) Configuración de idioma administrativo 

![IMG](6.png)

4) Pestaña Administración y clic en Cambiar configuración regional del sistema

![IMG](2.png)

5) Deshabilitar la opción indicada y dar clic en ACEPTAR: 

![IMG](3.png)
