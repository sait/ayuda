+++
title = "Reestablecer función de copiar con clipboard"
description = ""
weight = 40
+++

Si cuando usted intenta usar la opción de copiar en clipboard y no está funcionando el pegado, deberá seguir las siguientes intrucciones para reestablecer esta función

![IMG](funcion.png) 


1.Ingrese a su sistema de SAIT y de clic en **[Catálogo de Empresas Empresa Existente]**

![IMG](1.png) 

2.Después deberá seleccionar la opción de **[Agregar Empresa Existente]** 

![IMG](2.png) 

3.Y después seleccionará el signo de integrrogación para agregar la empresa demo que viene por default en la instalación, seleccione la empresa llamada DEMO y después de clic en **[Select]** 

![IMG](3.png) 

4.Clic en **[Continuar]** 

![IMG](4.png) 

5.Clic en **[Cerrar]** 

![IMG](5.png) 

6.Al entra r SAIT le aparecerá la empresa demo, la seleccionará y dara clic en **[Accesar Empresa]** 

![IMG](6.png) 

7.Entrar a los comandos de VFP en Ayuda / Comandos de Visual Fox Pro, la clave se calcula de la siguiente manera MESX3 DIAX2 MESX3, la ingresamos y damos Enter

8.Ingresará las siguientes instrucciones (después de cada instrucción darás ENTER en tu teclado):

**use cia shared** - comando para abrir tabla cia

**browse** - comando para ver consulta

Deberá localizar estos campos, si da doble clic sobre los campos notará los campos están llenos con información, si en la empresa REAL uno de esos campos están vacío (se puede notar fácilmente el campo dañado ya que en lugar de Memo dirá memo), si este es el caso, deberá copiar el contenido del campo lleno de la empresa demo y pegar el contenido a el campo dañado de la empresa real.

![IMG](7.jpg) 

Para copiar solo debe dar doble clic sobre el campo, seleccionar todo el contenido del campo, dar clic en COPIAR y PEGARLO en el campo dañado de la empresa real, para abrir la tabla de la empresa real vaya al paso 7.

![IMG](8.jpg) 

**use** - comando para cerrar tabla

**salir** - comando para salir de la ventana de comandos de fox

9.De esta manera si copiamos ya podremos pegar en otro documento 

![IMG](9.jpg) 