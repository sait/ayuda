+++
title = "Error: Usted está tratando de usar SAIT con diferente versión"
description = ""
weight = 10
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="diferente-version.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Al tener sistema SAIT en red (Servidor - Estación) se necesita que los ejecutables de SAIT concuerden con la versión instalada en el servidor. 

En dado caso de que se ejecute una versión diferente a la del servidor, se mostrara la siguiente alerta: 

![IMG](1.png)
 
NOTA: Este problema también se puede presentar al tratar de utilizar un respaldo de una empresa utilizando una versión del sistema anterior a la de la empresa. En este caso, si no se cuenta con la versión exacta de la empresa, será necesario actualizar la base de datos. 

Con objetivo de arreglar este detalle, es necesario copiar directamente la versión del ejecutable desde el servidor tal:

<h2 class="text-primary">Instrucciones</h2>

**Dentro del servidor**

1.Dirigirse a ubicación de instalación de SAIT. Ejemplo: C:\SAIT\

Si no sabe la ubicación puede dar clic derecho sobre su ícono SAIT que está en el escritorio y seleccionar la opción de Abrir Ubicación

![IMG](2.png)
 
2.Ubicar el ejecutable del sistema con extensión .exe: 

* SAIT ERP: sait.exe 

* SAIT Básico: saitbasico.exe 

* SAIT Nómina: saitnom.exe 

* SAIT Contabilidad: saitcont.exe 

3.Copiar dicho archivo en un dispositivo USB o a una carpeta compartida. 

**Dentro de la estación**

1.Dirigirse a ubicación de instalación de SAIT. Ejemplo: C:\SAIT

2.Tomar el archivo copiado del servidor y sobreescribir el que se encuentra en esta carpeta. 

3.Volver a probar entrar al sistema

**Verificación de versiones**

Una manera rápida comparar las versiones de los ejecutables de los equipos es cuando usted entra al sistema en la siguiente ventana le arroja la versión del ejecutable

![IMG](3.png)
 
También puede consultarla desde SAIT en el menú de Utilerías / Ayuda

![IMG](4.JPG)
 
