+++
title = "Reportes adicionales"
description = ""
weight = 99
+++

En esta sección encontrará algunos reportes que podrá instalar en su sistema con las siguientes [instrucciones](/temas-soporte/exportar-e-importar-reportes/#importar-reportes/)

<h3 class="text-primary">Caja</h3>

**Nombre reporte:** Fondo de caja recibido por monedas [Vista previa](desglose.png)

 * Ruta de consulta: Caja / Abrir corte o Fondo entregado al siguiente cajero [Clic aquí para descargar](fondo-de-caja-recibido-por-monedas.rpt)

<h3 class="text-primary">Inventario</h3>

**Nombre reporte:** Auxiliar en toma de inventario [Vista previa](auxiliar-en-toma-de-inventario.pdf)

 * Ruta de consulta: Inventario / Reporte de Artículos / Inventario Físico [Clic aquí para descargar](auxiliar-en-toma-de-inventario.rpt)

**Nombre reporte:** Existencia actual en almacenes[Vista previa](existencia-actual-en-almacenes.pdf)

 * Ruta de consulta: Inventario / Reporte de Movimientos [Clic aquí para descargar](existencia-actual-en-almacenes.rpt)


