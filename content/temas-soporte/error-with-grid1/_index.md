+++
title = "Error With grid1.width: expression evaluated to an ilegal value"
description = ""
weight = 19
+++

En SAIT la ventana de Inventario / Registrar Salidas se puede ajustar al tamaño de acuerdo a sus necesidades. La configuración del tamaño tanto de la ventana como de las columnas se graba por cada máquina.

En ocasiones, se daña el registro en windows en el cual se graba esta información y al tratar de entrar a la ventana aparece el mensaje:

![IMG](error.png)


Este error puede aparever en distintos módulos del sistema, en ventas, inventarios o gastos.


Para solucionar el problema se debe colocar el cursor del mouse en la esquina inferior derecha y arrastrarlo hacia la derecha para hacer las grande la ventana. Una vez que la ventana tiene mayor tamaño, se deberá ajustar el tamaño de cada columna en el detalle del documento de la misma forma: se debe colocar el cursor del mouse en el margen derecho de cada columna y arrastrarlo hacia la derecha para hacer mas grande la columna.

![IMG](VENTANA.jpg)

De este modo, se graba nuevamente en el registro de Windows el tamaño de la ventana y las columnas, la próxima vez que se abra la ventana se mostrará de tamaño normal. 
 


