+++
title = "Mostrar etiqueta segun una fecha indicada"
description = ""
weight = 52
+++

Esta configuración le puede ayudar cuando haya algún cambio en sus datos fiscales, como lo es cambio de domicilio fiscal, si a partir de cierta fecha el domicilio es distinto, hay clientes que requieren que aparezca el domicilio anterior si consulto documentos con fecha anterior al cambio y otro domicilio (es decir el acual) cuando consulto documentos con fecha posterior al cambio, esto se puede realizar en el sistema con los siguientes pasos:

1.Abra el formato a modificar en Utilerías / Formatos / Nota de venta, para efectos de la prueba utilizaremos este formato

![IMG](EDITAR.png)

2.La información con los datos de la empresa son variables y los troma directamente de Utilerías / Configuración General del Sistema, esta será la información fiscal que se tuvo que modificar para actualizar la nueva dirección

![IMG](DATOS-EMPRESA.png)

3.Lo siguiente será a estas etiquetas agregarles una condición para que se muestre esa dirección a partir de la fecha se hizo el cambio, supongamos el cambio se hizo hoy 2 de noviembre de 2023, dando doble clic sobre la etiqueta **Alltrim(Cia.CALLE)+' #'+AllTrim(Cia.NUMEXT)+' '+AllTrim(Cia.COLONIA)** y se abrirá el detallado de la expresión y daremos clic en el botón de PRINT WHEN... el cual nos controla cuándo se mostrará la etiqueta, ahí colocaremos la expresión **DOCUM.FECHA>=DATE(2023,11,02)** es decir, mostrar etiqueta cuando la fecha de los documentos sea mayor o igual al 2 de noviembre de 2023.

![IMG](ETIQUETA-DE-EMPRESA.png)

Realizar lo mismo que las demás etiquetas

![IMG](ETIQUETAS-A-MODIFICAR.png)

4.Ahora agregaremos las etiquetas con la dirección fiscal antigua, para esto deberá habilitar la barra de reportes dirigiéndose al menu: View / Report Controls Toolbar. 

![IMG](BARRA.png)

5.Haga clic sobre el icono [A] y de clic en el encabezado del reporteador y capture la antigua dirección fiscal:

![IMG](ANTIGUA.png)

6.Agregar condición de cuándo mostrar etiqueta, de igual forma daremos doble clic sobre la etiqueta y después daremos clic en el botón de PRINT WHEN..., ahí colocaremos la expresión **DOCUM.FECHA<DATE(2023,11,02)** es decir, mostrar etiqueta cuando la fecha de los documentos sea menos al 2 de noviembre de 2023, fecha que se cambió el domicilio fiscal, acomodaremos la etiqueta arrastándola y colocándola encima de la dirección actual.

![IMG](ENTRALAZAR.png)

7.Guarde cambios dirigiéndose a File / Save y salga de la edición del formato.

![IMG](GUARDAR.png)

8.Listo, si consultamos una nota veremos las etiquetas no se muestran encimadas, ya que las notas con fecha igual o mayor al 2 de noviembre se mostrará la dirección de la Configuración General del Sistema y si consultamos una nota con fecha anterior al 2 de noviembre, mostrará la dirección fiscal anterior.

![IMG](FIN.png)