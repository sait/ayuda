+++
title = "Configuración de Tareas Automáticas"
description = ""
weight = 30
+++

Si usted cuenta con equipos servidores que se mantienen encendidos todo el tiempo y desea crear tareas programadas para que los procesos de SAIT se ejecuten automáticamente, deberá seguir las siguientes indicaciones:

1.Presionar el botón de inicio de Windows     

![IMG](iconos.JPG)

2.Buscar y seleccionar el ‘Programador de Tareas’

![IMG](1.png)

3.Seleccionar la opción ‘Crear Tarea Básica’.

![IMG](2.png)
 
4.Especificar el nombre de la tarea y descripción general del proceso. De clic en **[Siguiente]**.

![IMG](3.png)

5.Seleccionar que la tarea se ejecute ‘Diariamente’ y de clic en **[Siguiente]**.

![IMG](4.png)

6.Definir el horario en que desea se ejecute el proceso automáticamente. Se recomienda se coloque una hora antes de que se inicien operaciones  en la empresa. De clic en **[Siguiente]**.

![IMG](5.png)

7.Seleccione la opción ‘Iniciar un Programa’ y de clic en **[Siguiente]**.

![IMG](6.png)

8.Especifique lo siguiente:

•	Programa o script a ejecutar: Seleccione la ubicación en donde se encuentra el ejecutable de SAIT, por ejemplo C:\SistemaSAIT\SaiteERP\sait.exe

•	Agregar argumentos (opcional): colocar la palabra INDEXAR

•	Iniciar en (opcional): colocar el directorio de la empresa, por ejemplo 

C:\SistemaSAIT\SaiteERP\Demo6\\\

De clic en **[Siguiente]**.

![IMG](7.png)

9.De clic en **[Finalizar]**.

![IMG](8.png)

10.Si desea confirmar que el proceso se haya definido correctamente, puede dar clic derecho sobre la tarea programada y dar clic en **[Ejecutar]**. 

![IMG](9.png)

11.Si la tarea fue definida correctamente, y si el sistema se encuentra cerrado en todos los equipos, se deberán generar los índices sin ningún problema.

![IMG](10.png)



