+++
title = "Error: Not Enough Memory for file map"
description = ""
weight = 6
+++

Este mensaje se puede presentar en cualquier proceso que se esté realizando dentro del sistema, por ejemplo en la consulta de algún reporte, cerrar corte de caja o también al actualizar base de datos.

Es muy importante considerar que si se está manejando SAIT en una empresa en donde se genera una gran cantidad de información y no se ha realizado ninguna depuración, puede provocar que los procesos se generen con lentitud (por ejemplo consultas, reportes, facturas, notas), que la generación de índices tarde mucho y en general se complique el trabajar con el sistema. 

Los motivos de este mensaje de error están relacionados a que la Memoria en caché o RAM esté saturada o también, si su base de datos tiene muchos registros o su peso ya aumentó bastante con el paso de los años (peso cercano a los 2 GB), también esto puede provocar este mensaje de error.

<h3 class="text-primary">Recomendaciones SAIT para este mensaje de error</h3>

* Cerrar cualquier otra aplicación que se encuentre abierta en el equipo, por ejemplo los exploradores de Internet suelen consumor muchos recursos del equipo.

* Depurar información del sistema para que le permita realizar los procesos sin ningún problema. (En caso de optar por esta opción, se recomienda que el proceso de depuración lo realice un asesor SAIT o el personal de sistemas de su empresa).




