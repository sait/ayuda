+++
title = "Configuración de SQL SERVER (para consultas lentas)"
description = ""
weight = 31
+++

El servidor de consultas SQL-SERVER de SAIT, es una herramienta que le permite optimizar el rendimiento de las consultas que se realizan en el sistema, obteniendo rápidamente el resultado. 

En empresas en donde el volumen de la información que se procesa diariamente es muy grande, proporciona resultados muy satisfactorios ya que las consultas se obtienen en cuestión de segundos.

### Cuestiones a considerar:

* Esta herramienta de consultas solamente se instala en el equipo en donde residen los datos, es decir el servidor.

* La estructura del directorio de SAIT debe ser la siguiente: C:\SISTEMAS\SAIT 

* Si no cuenta con esta estructura debe crear el directorio en disco local C como SISTEMAS y mover la carpeta de SAIT tal cual está dentro de este nuevo directorio.
 
![IMG](1.png)

* En propiedades de la carpeta de SISTEMAS, dar clic en compartir con TODOS con todos los permisos (lectura y escritura).

![IMG](2.png)
 
* Importante: En caso de mover el directorio de SAIT considerar también que debe actualizar cada una de las estaciones de trabajo la unidad de red y la ruta de acceso en SAIT, considerando el nuevo directorio.

* El nombre de SISTEMAS es indistinto, puede ser SistemaSAIT, DISCOF, etc.

### Proceso de Instalación de SQL SERVER

1.Crear una carpeta llamada SAITSQL, al mismo nivel al que se encuentra la carpeta de SAIT.

EJEMPLO: Considerando que la carpeta de SAIT se encuentra en C:\SISTEMAS\SAIT\\

La carpeta del SQL SERVER será C:\SISTEMAS\SAITSQL\\

![IMG](7.png)

2.Descargar el archivo SQLSERVER.EXE en el siguiente enlace: https://www.sait.mx/download/sqlserver.exe

3.Copiarlo a la carpeta de SAITSQL.

![IMG](8.png)
 
4.Crear un acceso directo del servidor de consultas en el escritorio del servidor.

También se puede agregar al grupo de Inicio de Windows para que se ejecute al encender el equipo.

5.Debe ejecutar el archivo por el número de estaciones que estén trabajando con SAIT al mismo tiempo. Por ejemplo, si cuenta con 3 estaciones, debe ejecutarse el archivo SQLServer 3 veces, como se muestra a continuación: 

![IMG](9.png)
 
### Proceso de Configuración de SQL SERVER

Configurar el uso del servidor de consultas SQLSERVER en cada equipo donde esté instalado SAIT:

1.Dirigirse al menú de Utilerías / Configuración General del Sistema.

2.Seleccionar la pestaña de **[Otros]**.

3.Activar la casilla de [ * ] Hacer llamadas a servidor remoto SQL Server.

![IMG](10.png)
 
4.Hacer clic en el botón **[Cerrar]**.
 
Para verificar que las consultas realizadas desde la estación estén entrando al SQL Server debe realizar una búsqueda de cualquier tipo, por ejemplo una búsqueda de un producto, y revisar si en la ventana de SAIT SQL Server arroje información, como se muestra en la siguiente imagen:
 
### Consultas lentas aún con (SQL SERVER)

**NOTA:** Si lo desea también puede descargar el siguiente archivo el cual puede ejecutar en el equipo y creará los parámetros en el regedit con solo dar doble clic sobre el: [clic aquí para descargar](REGEDIT-CONSULTAS-LENTAS.zip)

Si ya tiene instalado y configurado el SQL Server y aún se siguen presentando consultas lentas, puede intentar solucionar este problema con las siguientes indicaciones.

1.En la estación ejecutar: RegEdit

2.Navegar a la siguiente ruta:

3.HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters

4.Tomar una imagen JPG de los valores actuales, oprimiendo la techa Imp_Pant

5.Editar los valores siguientes:

* FileInfoCacheLifetime

* FileNotFoundCacheLifetime

* DirectoryCacheLifetime

![IMG](parametros.PNG)

6.A todos ponerles el valor de 0

7.Si no existen crearlos usando el tipo: Reg_DWord (32 bits)

* En el espacio en blanco dar clic derecho.

* Seleccionar la opción crear Reg_DWord (32 bits)

* Ingresas el nombre del parámetro y listo.

8.Tomar otra imagen JPG de la ventana

9.Reiniciar la estación

10.Probar la búsqueda

11.Si el problema persiste favor de comunicarse con su asesor SAIT o mandar un correo a soporte@sait.com.mx

En ocasiones se presenta este detalle cuando la empresa cuenta con un servidor con Windows Server 7 u 8 y estaciones de trabajo con Windows 7, esto provoca que las consultas sean lentas ya que hay una falla de comunicación entre diferentes sistemas.


#### En caso de que prefiera ver el video con las instrucciones del proceso de Configuración de SQL server puede dar clic en el siguiente video: 

{{<youtube xot-MosNxRs>}}


