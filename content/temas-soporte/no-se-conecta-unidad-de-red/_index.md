+++
title = "Error al ingresar a SAIT (no se conecta a unidad de red)"
description = ""
weight = 17
+++

Si en dado momento al intentar entrar al sistema se muestran algunos de estos errores deberá de revisar la siguiente documentación con el fin de solucionar el problema presentado.

![IMG](2.png)

![IMG](1.png)

### Posibles Motivos 

Este inconveniente se presenta cuando la estación de trabajo no se conecta a la unidad de red, y por lo tanto, no permite ingresar al sistema. 

Algunos de los motivos por los que se presenta este detalle, son los siguientes:

* El servidor se encuentra apagado

* No hay acceso a internet

* El directorio de la empresa ha cambiado

* Actualización del sistema operativo Windows 10 modificó las propiedades de recursos compartidos.

* La red de acceso de la estación es distinta a la red de acceso del servidor

* Se han modificado las propiedades de recursos compartidos en el servidor


### 1.	Revisar Equipo Servidor  e Internet

-	Lo primero que hay que verificar es que el servidor se encuentre encendido y trabajando correctamente. También debe asegurarse que el servicio de internet esté funcionando sin problemas.

### 2.	Revisar el directorio de la empresa

-	Si el directorio de la empresa ha cambiado, deberá actualizar la unidad de red, y el directorio de la empresa en la estación de trabajo: 

https://ayuda.sait.mx/temas-soporte/reinstalar-sait-en-estacion/4-redireccionar-directorios/


### 3.	Revisar el acceso a la unidad de red

-	Ir a Equipo y dar doble clic sobre la unidad de red que está presentando el detalle.

![IMG](3.png)

-	Si no da acceso, puede intentar conectar de nuevo la unidad de red:
https://ayuda.sait.mx/temas-soporte/reinstalar-sait-en-estacion/3-conectar-unidad-de-red/

-	Si persiste el problema, deberá verificar las propiedades de recursos compartidos. 


### 4.	Revisar propiedades de recursos compartidos

-	En el equipo servidor y en la estación de trabajo, deberá ir a ‘Panel de Control / Redes e Internet’.

-	Seleccionar ‘Centro de Redes y Recursos Compartidos’.

![IMG](4.png)

-	Seleccionar la opción de ‘Cambiar configuración de uso compartido avanzado’.

![IMG](5.png)

-	Deberá asegurarse de que se encuentren habilitadas las opciones para la detección de redes, y compartir archivos para lectura y escritura. También debe verificar si se compartirá por contraseña, en caso de ser así, debe tener a la mano ese dato.

![IMG](6.png)

Debe dar clic en [Guardar Cambios] y volver a intentar abrir la unidad de red.

Para este punto, ya debe tener acceso a la unidad de red, y por ende acceso a SAIT.

### 5.	Revisar red de acceso de estación de trabajo y del servidor

-	En el equipo servidor y estación de trabajo, deberá ir a ‘Panel de Control / Redes e Internet’.

-	Seleccionar ‘Centro de Redes y Recursos Compartidos’.

-	Deberá asegurarse que la red con la que se encuentre trabajando el equipo, sea la misma en ambos equipos.

-	Ambos equipos deben estar bajo la modalidad ‘Red de Trabajo’.

![IMG](7.png)

-	Si usted utiliza dos módems, debe verificar que tanto la estación de trabajo como el servidor se encuentren trabajando sobre la misma red compartida (solo aplica si tiene más de 1 módem instalado en su empresa o varias redes inalámbricas).