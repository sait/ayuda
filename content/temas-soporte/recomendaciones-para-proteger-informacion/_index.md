+++
title = "Recomendaciones para proteger Información al usar Microsoft Windows"
description = ""
weight = 98
+++

El sistema operativo Microsoft Windows debe ser configurado correctamente si lo utiliza para mantener información importante.

A continuación explicaremos algunas acciones que debe realizar para proteger la información que almacena su computadora si es que utiliza el sistema operativo Microsoft Windows


<h3 class="text-primary">Desactivar el caché del disco duro</h3>

Microsoft Windows siempre tiene activado la directiva de usar caché de escritura en el disco duro, porque mejora la velocidad de la computadora, pero una perdida de energia puede ocasionar dano en los datos, por tal motivo si en tu ciudad presentan fallas frecuentes de energia electrica, es sumamente importante desactivar esta directiva.

1) Vaya a Panel de control - Sistema - Administracion de Dispositivos 

2) Localice la unidad de disco

3) Abra las propiedades

4) En la pestana de Directivas

5) Desactive la directiva de cache de escritura

![IMG](cache.png)

<h3 class="text-primary">Desactivar funcionalidad “Archivos sin Conexion”</h3>

Si en su empresa utilizan una red de computadoras y comparten documentos entre ellas, Microsoft Windows cuenta con una opción para usar esos archivos aún y cuando no haya conexión

Básicamente realiza una copia de los archivos originales en su computadora, para que estén disponibles cuando la computadora original se encuentre apagada o desconectada

Esto NO es muy buena idea, porque al final de cuentas usted terminará con múltiples copias de un mismo archivo, siendo difícil identificar cuál es el el correcto.

La configuración de “Archivos sin conexion”, le permite acceder a copias de sus archivos de red incluso cuando no puede conectarse a la red

Sin embargo, esta configuración puede causar varios problemas incluyendo:

- Lentitud al guardar documentos en la red

- Incoherencias en los documentos y/o archivos

- Corrupción de la bases de datos

Puede resolver estos problemas deshabilitando la funcionalidad de archivos fuera de línea para todas las PCs de su red

- Siga estos pasos para desactivar la sincronización de archivos

- Asegúrese de estar conectado a su red.

- Guarde todo lo que tenga abierto en su computadora.

- Abrir el Panel de Control. 

- En la ventana Panel de control, escriba Centro de Sincronización en el cuadro de búsqueda y presione Entrar.

- Haga clic Centro de Sincronización.

- Desde la izquierda de la pantalla, seleccione Administrar archivos fuera de línea.

- Seleccionar Desactivar archivos fuera de línea.

- Haga clic OK para cerrar la ventana.

- Cuando se le solicite reiniciar, cierre cualquier programa abierto y haga clic en Sí para reiniciar.

Ver imagenes en la siguiente página

Acceder:

![IMG](1.png)

Desactivar directiva:

![IMG](2.png)

Despues de reiniciar el equipo, asi debe quedar:

![IMG](3.png)

