+++
title = "Reinstalación de Organizador de Comprobates Fiscales"
description = ""
weight = 3
+++

En caso de que se vea en la necesidad de reinstalar el Organizador de Comprobantes Fiscales deberá de seguir las siguientes instrucciones:

<h3 class="text-primary">1.En el servidor anterior hacer respaldo de carpeta DATA</h3>


Para ello deberá entrar al servidor y ubicar la carpeta donde está la base de datos del OCF
Generalmente se encuentra en la siguiente ruta (aunque esta ruta puede cambiar)

C:\Program Files (x86)\SAIT\Boveda3\

La carpeta que debe de respaldar se llama data, si en su empresa usted maneja más de un RFC aquí aparecerán más de una carpeta con el RFC de cada empresa, deberá respaldarlas todas.

![IMG](1.png)


<h3 class="text-primary">2.Instalar y Actualizar OCF</h3>

Instrucciones paso a paso [aquí ](/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/a-ejecutar-instalador-y-actualizar-version/)

<h3 class="text-primary">3.Conectar OCF a base de datos</h3>

Diríjase a Utilerías / Configuración general del sistema / Otros / Configuración de Organizador de Comprobantes Fiscales (OCF)

Deberá colocar la IP del servidor y posteriormente deberá dar **[Enter]**

![IMG](2.png)

Si el cursor se pasa al siguiente campo como se muestra en la imagen quiere decir la conexión se hizo de manera correcta

![IMG](3.png)

Si tiene más de una razón social, deberá hacer esto por cada empresa.

**NOTA: Si no conocemos cuál es la IP del servidor siga las siguientes instrucciones:** 

En el servidor, ir al Buscador de Windows y teclear la palabra CMD 
 
![IMG](4.png)

Escribir ipconfig y dar [Enter] en la ventana de comandos

![IMG](5.png)

La dirección IPv4 es el dato que necesitamos tomar y definir en el OCF dentro de SAIT.

![IMG](6.png)

<h3 class="text-primary">4.Abrir puerto 48800 para que las estaciones se conecten al servidor</h3>

Instrucciones paso a paso [aquí ](/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/b-permitir-puerto-48800-en-firewall/)

<h3 class="text-primary">5.Configurar correo para recibir facturas desde correo</h3>

Instrucciones paso a paso [aquí ](/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/d-configurar-correo-para-recibir-facturas/)

<h3 class="text-primary">6.Ingresar los XML para que OCF los lea nuevamente</h3>

En el servidor abra la carpeta ubicada

C:\BuzonSait

![IMG](7.png)

Y también la ventana de data que fue respaldada, ahora entre a la carpeta con el RFC de la empresa, ahí podrá observar varias carpetas de los XMLs divididos por años.

![IMG](8.png)

Si damos clic en el 2022 podremos ver estan divididos en 12 que hace referencia a los meses del año.

![IMG](9.png)

Para no entrar mes por mes, en la barra de búsqueda de la derecha podemos escribir .XML de esta manera me arrojará todos los XML encontrados en las 4 carpetas

Recomendamos copiar de 700 en 700 los XML y pegarlos en la carpeta de C:\BuzonSait y esperamos a que el OCF absorba los comprobantes

![IMG](10.png)

Deberá hacer este proceso con todos los XML tanto emitidos como recibidos.

De esta manera ya podrá hacer uso de los XML que tenía en su sistema SAIT.
