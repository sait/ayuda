+++
title = "Configurar Retención de IVA e ISR por artículo"
description = ""
weight = 38
+++

Existen dos formas de realizar la retención de IVA e ISR en SAIT, por cliente [ver documentación](/sait-erp/10-capacitacion-modulo-de-ventas/a-catalogo-clientes/) en donde se hará la retención a todos los artículos de la factura y retención por artículo, en la siguiente documentación se explicará a detalle cómo hacer retenciones por un artículo en específico

1.Primeramente se recomienda realizar un respaldo de información en Utilerías / Respaldar Información, una vez realizado este paso deberá asegurarse no haya usuarios utilizando el sistema

2.Ir a Utilerías / Configuración General del Sistema, clic en **[Datos Adicionales en Catálogos]**

![IMG](DATOSADIC.png)

3.Clic en **[Agregar Dato]**

![IMG](AGREGARDATO.png)

Primero agregaremos el campo para la Retención de IVA, llene los datos como aparecen en la siguiente pantalla

![IMG](CONFIG.png)

4.Clic en **[Crear Campo Físicamente]**

Dejar los datos tal y como aparecen en la siguiente pantalla y dar clic en **[Crear]**

![IMG](CREAR.png)

Aparecerá el siguiente mensaje 

![IMG](CREADO.png)

**Repetir exactamente los mismos pasos para crear el campo RETENCION2**

Importante mencionar que aunque sólo vaya a utilizar una de las dos retenciones, es importante cree ambos campos, de otra manera no funcionará

5.Ir a Inventario / Catálogo de Artículos y Servicios / Otros Datos, Capturar % de retención en artículo en se pondrá observar ya aparecen los datos **Ret IVA** y **Ret ISR** donde deberemos capturar los porcentajes a retener en cada venta en este artículo en específico.

![IMG](CAPTURARRET.png)

6.Al realizar la factura e ingresar el artículo en cuestión en la parte de totales podrá observar la retención dependiendo el % capturado en el catálogo del artículo.

![IMG](FACTURA.png)


<h3 class="text-danger">Importante</h3>

Normalmente el formato default de factura está preparado para mostrar el importe referente a las retenciones como se muestra en el siguiente ejemplo

![IMG](FormatoOriginal.png)

Sin embargo, si usted cuenta con un formato de factura personalizado y no se muestra este dato deberá de agregarlo de manera manual, para ello, deberá seguir las siguientes instrucciones:

1.Vaya a Ventas / Consultas Individual / Ingrese el folio de la factura realizada, clic en Imprimir y de clic en **[Formato]**

![IMG](editar-formato.png)

2.Verifique la barra de herramientas "Report Controls" esté habilitada en View / Report Controls Toolbar

![IMG](view-controls.png)

Para la prueba, agregaremos el campo de la Retención de IVA, de clic en la opción de **[Field]**:

![IMG](field.png)

3.Después deberá ir al final del formato en la sección de totales y dar clic, aparecerá la siguiente ventana y llenaremos la información de la siguiente forma, la expresión DOCUM.RETENCION1 es para la Retención de IVA y DOCUM.RETENCION2 para la Retención de ISR, de clic OK

![IMG](etiquetaretencion.png)

4.Adicionalmente podemos agregar la etiqueta dando clic en el siguiente botón:

![IMG](etiquetatexto.png)

Y daremos clic en la parte en donde queramos agregar la leyenda, también puede seleccionarla y arrastrar los elementos a donde usted desee:

![IMG](etiquetaretiva.png)

Si lo desea, puede personalizar la etiqueta y el campo, seleccionándolos y dando clic en Format / Font 

![IMG](font.png)

Para finalizar deberá ir a al menú de  File y dar clic en Save

![IMG](save.png)

De esta manera su formato personalizado ya mostrará las cantidades correspondientes a retención de IVA o retención de ISR según sea el caso.


