+++
title = "Error: Fatal Error Excepcion code: C0000005 / Record is out of range "
description = ""
weight = 4
+++

Al entrar al sistema y generar índices aparece el mensaje de error y no le permite entrar. Puede aparecer alguno de los siguientes mensajes:

![IMG](FatalError.JPG) 

Eso se debe a que está dañada la base de datos debido a algún apagón de luz, se quedó bloqueada la maquina al estar ejecutando un proceso en el sistema o se salieron mal de SAIT mientras procesaban información. 

### Proceso para recuperar una base de datos dañada 

1.Para este proceso ningún usuario debe de estar dentro del sistema

2.Entre a SAIT y cuando se presente la ventana de selección de la empresa, hacer clic en el botón **[Catalogo de Empresas]**

![IMG](1.png) 

3.Se muestra una ventana con 2 columnas: el nombre y directorio de la empresa. Revisar cual es el directorio 4.Hacer clic en el botón de **[Utilerías]**

![IMG](2.png) 

5.Especificar el directorio de la empresa y presionar la tecla **[Enter]**

![IMG](3.png) 

6.Se mostraran varios archivos con extensión .DBF

![IMG](4.png) 

Seleccionar el nombre de la base de datos que esta dañada. 

Generalmente cuando una base de datos está dañada las tablas a recuperar son:

* DOCUM.DBF

* MOVIM.DBF

* MINV.DBF

Clic en el botón **[Recuperar base de datos]** (hacer esto por cada tabla)

![IMG](recuperar.png) 

**Es importante mencionar que dependiendo el tamaño de la base de datos y de la tabla este proceso puede ser tardado por ello se recomienda siempre realizar la recuperación desde el Servidor.**

7.Cerrar la ventana 

8.Entrar a SAIT y Generar Índices desde el servidor