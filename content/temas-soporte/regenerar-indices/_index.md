+++
title = "Error: Index file does not match the table / Table has no index order set"
description = ""
weight = 5
+++

Los archivos índices, son muy importantes para la operación del sistema, ya que ordenan toda la información que ha sido almacenada, para que pueda ser localizada rápidamente. Estos archivos pueden dañarse en caso de que alguna computadora del sistema haya sido apagada incorrectamente como suele pasar cuando hay un apagón de luz. 


Algunas de los mensajes que puede arrojar el sistema cuando los archivos índices están dañados son: 

* Problemas para abrir Usuarios.DBF ERROR 1707: Structural CDX File is not found 

* Index file does not match the table. Delete the index file an recreate the index 

* File is not Open 

O también algunos de estos errores: 

![IMG](1.png)

![IMG](2.JPG)

![IMG](3.JPG)

Antes realizar cualquier proceso tenga en cuenta lo siguiente:

* Se recomienda hacer este proceso desde el servidor

* Este proceso NO funciona con SAIT Nómina, en ese caso se debe seguir un proceso distinto

* Se recomienda realizar un respaldo de información antes de correr este proceso

### Instrucciones

1.Entrar a SAIT 

2.Aparece la ventana de "Seleccionar Empresa" 

3.Hacer clic en el botón **[Catalogo de Empresas]** 

![IMG](catalogo.png)

4.Se muestra una ventana con 2 columnas: el nombre y directorio de la empresa. Revisar cual es el directorio del cual vamos a regenerar los archivos índices, seleccionamos la ruta y la copiamos con CTRL + C

![IMG](ruta.png)

5.De clic en el botón de **[Utilerías]** 

![IMG](utilerias.png)

6.En donde dice Directorio pegaremos la  ruta de la empresa con CTRL + V y presionanamos la tecla **[Enter]** 

![IMG](pegaruta.png)

7.Se muestran los archivos de las bases de datos 

8.Hacer en el botón **[Borrar archivos índices (.cdx )]**

![IMG](borrar.png)

Se le mostrará un mensaje de alerta indicando los archivos índices fueron borrados, de clic en **[Aceptar]** 

![IMG](confirmacion.JPG)

9.Cerrar la ventana 

10.Entrar a SAIT nuevamente 

Se le mostrarán los siguientes mensajes de error, pero no se preocupe, es normal ya que borramos estos archivos para regenerarlos, de clic en Aceptar hasta que se quiten estos mensajes

![IMG](errores.JPG)

![IMG](errores2.JPG)

11.Y el sistema al no detectar los archivos CDX, los volverá a generar automáticamente. 

![IMG](generacion.png)
 
