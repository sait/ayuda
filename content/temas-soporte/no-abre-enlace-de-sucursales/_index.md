+++
title = "Error al abrir SAIT Distribuido o Enlace de Sucursales"
description = ""
weight = 14
+++

El Enlace de Sucursales es una aplicación que trabaja de la mano con la aplicación de SAIT la cual permite tener sus sucursales en distintas ubicaciones enlazadas siempre y cuando cuenten con servicio de Internet.

En ocasiones puede ser que este servicio se detenga, a continuación mostraremos algunos de los erroes más comunes por el que puede ser el enlace no esté funcionando de manera correcta así como sus posibles soluciones.

<h4 class="text-primary">Error 1: OLE IDispatch exception code 0 from Microsoft OLE DB Provider for ODBC Drivers: [MySQL][ODBC 3.51 Driver]Can't connect to MySQL server on 'sd.sait.mx'</h4>

#### Posibles Causas y soluciones

* **No hay acceso a internet:** deberá asegurarse si tenga conexión estable a internet

* **No hay acceso al dominio sd.sait.mx:** debe asegurarse de que desde que el explorador de Internet pueda acceder al dominio sd.sait.mx

* **Reiniciar módem:** se recomienda desconectar el módem de la corriente de luz por al menos 10 segundos y volverlo a conectarlo.

* **Puerto 3060 bloqueado en el Firewall de Windows:** debe asegurarse de abrir el puerto 3060 en caso de que se bloqueen puertos en el Firewall de Windows

<h4 class="text-primary">Error 2: cError=OLE IDispatch exception code 0 from Microsoft Cursor Engine: La operación en varios pasos generó errores. Compruebe los valores de estado...</h4>

Este error se genera debido a que la tabla de eventos.dbf guardó uno o más registros en blanco, esto debido a problemas de red o problemas con el disco duro de alguno de los equipos.

Para corregirlo deberá entrar a la base de datos en SAIT:

1.Ir a Ayuda / Comandos de Visual FoxPro

2.Ingresará la contraseña 

3.Abrimos la tabla con los siguientes comandos y el registro en blanco se mostrará al inicio de la tabla:

![IMG](1.png)

4.Borraremos el registro en blanco, “sombreando” el cuadrito de la izquierda como se muestra en la siguiente imagen:

![IMG](2.png)

5.Presionaremos la tabla Ctrl + W para guardar los cambios, después ingresamos el comando de USE para cerrar la tabla y finalmente ingresamos el comando SALIR para salir de los comandos de Visual FoxPro.

<h4 class="text-primary">Error 3: No es posible accesar cuenta de Sait distribuido</h4>

Para corregirlo deberá de revisar lo siguiente:

#### FIREWALL BLOQUEANDO APPLICACIÓN

Si tiene un firewall instalado, permita acceso total a internet al programa saitdist.exe 

Para ello deberá entrar al Panel de Control del equipo / Sistema y Seguridad / Firewall de Windows y seleccionará la opción de “Permitir un programa o una característica a través de Firewall de Windows”.

![IMG](3.png)

Posteriormente dará clic en la opción de “Permitir otro Programa”.

![IMG](4.png)

Y después clic en Examinar 

![IMG](5.png)

Selecciona el ejecutable saitdist.exe y damos clic en Abrir

![IMG](6.png)

Damos clic en Agregar 

![IMG](7.png)

Y finalmente clic en Aceptar

![IMG](8.png)

#### ACCESO DESDE EXPLORADOR

También debe de revisar que su explorador tenga acceso al sitio sd.sait.mx 

![IMG](acceso.JPG)

#### QUITAR OPCIÓN TRABAJAR SIN CONEXIÓN

Deberá asegurarse de que su navegador no tenga activada la opción de archivo /trabajar sin conexión 

![IMG](9.png)

<h4 class="text-primary">Error 4: Error Cannot update the cursor</h4>

Si se presenta el error Cannot update the cursor en el enlace de sucursales al procesar eventos. 

#### Posibles Causas y soluciones

* Pruebe desactivando el antivirus 

* Si funciono entonces, asegurarse de hacer las exclusiones correspondientes a SAIT en su antivirus. 

* Si no funciono pruebe cambiando de antivirus y haciendo las exclusiones correspondientes, ESET NOD32 presenta este problema comúnmente recomendamos que utilice otro antivirus. 

Si el problema persiste favor de mandar un correo con las imágenes del error a soporte@sait.com.mx 
