+++
title = "Inconsciencias en Bases de Datos"
description = ""
weight = 51
+++

Este proceso realiza una verificación de los datos en las tablas de SAIT para localizar posibles inconsistencias como duplicidad de datos en catálogos (artículos, clientes, proveedores) asíc como en existencias de almacenes, llaves vacías o desalineadas, documentos duplicados, partidas duplicadas o partidas sin encabezado, dicha herramienta está disponible a partir de la versión 2023.25.

<h2 class="text-primary">Configuración previa</h2>

1.Ir a Utilerías / Configuración General del Sistema, a la pestaña de otros

2.Dar Clic en el botón [Inconsistencias]

![IMG](01.jpg)

3.En la ventana: Inconsistencias en Base de Datos, ir a la pestaña Configuración

![IMG](02.jpg)

4.En consistencias a revisar, habilitar las opciones a verificar 

* Catálogos:
	* Artículos
	* Clientes
    * Proveedores
	* Existencias (multialm)
	* Llaves vacías o desalineadas

* Movimientos
	* Documentos duplicados
	* Partidas duplicadas
	* Partidas sin encabezados

* También puede configurar revisión de inconsistencias diarias y que éstas se envíen a un correo
	* Habilitar la opción: Ejecutar la revisión de inconsistencias (al iniciar el día)
	* Capture el correo electrónico a donde se enviará el informe
	* Importante para el funcionamiento de esta opción debe contar con la configuración del envío de correos por medio del OCF. Puede seguir la guía para configurarlo [ver documentación](/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/e-configurar-correo-para-enviar-facuras-y-recibos-de-pago/)
	* Llegará un correo igual al siguiente:

	![IMG](03.jpg)

5.Una vez seleccionando las inconsistencias a revisar

![IMG](04.jpg)

6.Dar clic en el botón [Guardar]

<h2 class="text-primary">Ejecutar revisión</h2>

1.Ir a Utilerías / Configuración General del Sistema, a la pestaña de otros

2.Dar clic en el botón [Inconsistencias]

3.En la ventana: Inconsistencias en Base de Datos, ir a la pestaña Inconsistencias

4.Dar clic en el botón [Ejecutar Revisión]

![IMG](05.jpg)

6.Si al ejecutar la revisión se encontraron inconsistencias favor de reportarlo con su asesor de sistemas, puede generar ticket en la página: https://soporte.sait.mx/

<h2 class="text-primary">Consultar Archivo de Errores</h2>

1.Ir a Utilerías / Configuración General del Sistema, a la pestaña de otros

2.Seleccionar la pestaña Errores

3.Nos mostrará el archivo de la siguiente manera

![IMG](06.jpg)


