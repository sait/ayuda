+++
title = "Realizar entradas o salidas de Inventario desde Excel"
description = ""
weight = 42
+++

SAIT le permite realizar hacer entradas o salidas de Inventario apoyándonos con un archivo de excel, esta opción básicamente lo que hace es emular el uso de una lectora de datos.

Esta opción nos puede servidor para varios procesos, por ejemplo, ingresar una entrada de Inventario Inicial, hacer una salida por ajuste de manera masiva, etc.

A continuación mostraremos los pasos a seguir.

1.Primeramente debemos de contar con un archivo de excel en donde contemos con 

* La clave del artículo

* La descripción del artículo

* Una columna llamada CANTIDAD en donde indicaremos la cantidad con la que haremos la afectación de la entrada o salida.

![IMG](1.PNG) 

2.Agregamos una columna más llamada **Concatenar** seleccionamos la primer celda de la columna en cuestión y damos clic en el símbolo de fórmulas

![IMG](2.png) 

3.En la categoría seleccionamos la opción de **Todo** y después seleccionaremos la función de **CONCATENAR**

![IMG](3.png) 

4.Se abrirá la ventana en donde nos pide indicar los argumentos de la función:

* En el **Texto1** deberá seleccionar la celda **A7** la cual hace referencia al primer registro de la columna de **Clave**

* En el **Texto2** deberá ingresar una COMA 

* En el **Texto3** deberá seleccionar la celda **C7** la cual hace referencia al primer registro de la columna de **Cantidad**

Dará clic en **[Aceptar]**

![IMG](4.png) 

5.Veremos que la columna **Concatenar** muestra la clave,cantidad ahora solo arrastraremos de la esquina inferior derecha para aplicar esa fórmula a toda la columna.

![IMG](5.png) 

6.Ya que se vea de esta manera la columna, la seleccionaremos y daremos clic en **Copiar**

![IMG](6.png) 

7.Nos dirigiremos a Inventario / Registrar Entrada / Entrada x Ajuste a Inventario y daremos clic en el ícono de la lectora en la parte inferior.

![IMG](7.png) 

8.Se abrirá esta ventana y daremos clic en **Siguiente**

![IMG](8.png) 

9.Se nos mostrará la ventana en donde se descargan los datos y presionaremos CTRL+V para **Pegar** los datos que copiamos de Excel.

![IMG](9.png) 

10.Dejamos el formato como aparece en la siguiente imagen **Código de barra,Cantidad** y damos clic en **Siguiente**.

![IMG](10.png) 

11.Validamos la información y damos clic en **Finalizar**.

![IMG](11.png) 

12.De esta manera vemos que la ventana de Entradas al Inventario ya cargó las claves y su respectiva cantidad de acuerdo a la información de Excel. Damos clic en **Procesar = F8** y listo

![IMG](12.png) 

13.Si revisamos el kárdex podemos ver se hizo la entrada por la cantidad indicada.

![IMG](13.png) 

<h4 class="text-primary">Este proceso es el mismo si se requiere hacer una salida de Inventario o cualquier entrada y salidas.</h4>