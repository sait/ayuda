+++
title = "Reporteador gráfico"
description = ""
weight = 27
+++

SAIT está preparado para poder editar los formatos que trae por default el sistema, así como crear reportes nuevos en caso de que se requiera. De manera general podemos dividir los reportes en 2 grupos, los reportes gráficos y los reportes tipo texto. A continuación, mostraremos cómo utilizar ambas opciones dentro de SAIT.

Para modificar un reporte se puede hacer dos maneras, al momento de imprimirlo y ver las siguientes opciones:

![IMG](1.png)

En caso de que aparezca inactivo, deberá de darle el permiso para esta opción en Utilerías / Grupos de Usuarios y Catálogo de Niveles 

![IMG](2.png)

Una vez dentro del reporte podemos empezar a realizar modificaciones, aquí los principales elementos que tiene el reporteador.

![IMG](3.png)

Para efectos de la demostración agregaremos una imagen a nuestro formato de cotización:

1.Ir a Ventas / Cotización / Consulta Individual / Imprimir

![IMG](4.png)

2.Clic en Formato

![IMG](5.png)
 
3.Se nos desplegará el diseñador de reportes y para agregar una imagen es necesario dar clic en el ícono indicado en la siguiente imagen:

![IMG](6.png)

4.Al dar clic se nos mostrará este puntero y daremos clic en la parte del reporte donde deseamos colocar la imagen.

![IMG](7.png)

5.Se nos abrirá este cuadro de diálogo en donde tendremos que seleccionar la imagen que deseamos agregar:

* La imagen debe de estar en formato JPG

* La imagen debe de estar dentro de el directorio donde está la información de la empresa esto para que sea accesible desde cualquiera de los equipos y se pueda mostrar de manera correcta, para saber la ruta de la información basta con entrar al sistema y dar clic en [Catálogo de Empresas] y ahí podemos visualizar el Directorio en el que necesitamos poner la imagen que deseamos mostrar en nuestro formato de cotización

![IMG](8.png)

Solo es cuestión de copiarla y pegarla en el explorador de archivos: 

![IMG](9.png)

![IMG](10.png)

<h4 class="text-primary">REPORTEADOR TEXTO</h4>

Sí se trata de la edición de un reporte de texto aquí encontrará la documentación:

https://ayuda.sait.mx/temas-soporte/reporteador-formatos-texto/