+++
title = "Configurar mostrar fotografia en formatos"
description = ""
weight = 36
+++

En la siguiente documentación explicaremos cómo agregar la foto capturada en el catálogo de artículos con fotografías, específicamente en la sección del reporte detalle de algún reporte, para efectos de la prueba lo agregaremos a un formato de cotización.

**Puntos previos a considerar:**

* La imagen de tener la extensión: JPG o BMP. 

* Las imágenes deben estar almacenas dentro del directorio de la empresa en una carpeta llamada: FOTOS

Por ejemplo como se muestra en la siguiente imagen:

![IMG](1.png)

* Si no conoce cuál es el directorio de datos de la empresa, vaya a Utilerías / Configuración General, ahí se indica en que carpeta o directorio están almacenados los datos de su empresa. Adicionalmente encontrará el directorio de FOTOS, en la cual debe estar contenida todos los archivos de los artículos. 


![IMG](2.png)


<h3 class="text-primary">Proceso para Agregar la imagen del artículo al formato de cotización</h3>

Agregar la fotografía al artículo ingresando a Inventario / Catálogo de con Fotografía

Posicionará el cursor en donde se indica en la siguiente imagen y presionará F2 en su teclado para buscar la imagen que desea seleccionar

Clic en Grabar y listo.

![IMG](3.png)

Diríjase al menú de: Utilerías / Modificar Formatos / Cotizaciones / Editar

![IMG](4.png)

Deberá ampliar el área del Detalle (Detail), arrastrando la barra, tal como se muestra a continuación:

![IMG](5.png)

Verifique si la barra de herramientas de "Report Controls" aparece en el reportador.

![IMG](6.png)

En caso de que la barra de "Report Controls" no aparezca en su pantalla, seleccione el menú de View / Report Control Toolbar.

![IMG](7.png)

Active el botón de OLE Picture Activex, como se muestra abajo: 

![IMG](8.png)

Aparece el cursor en forma de cruz, c el cursor en el area del Detail en donde desea imprimir la imagen o foto y haga clic en el lugar en donde desea que aparezca la imagen. 

Aparece la ventana de "Report Picture" 

En el campo File, deberá escribir la siguiente instrucción que es la que llamará a la fotografía: 

```
'FOTOS/'+Arts.FOTO
```

![IMG](9.png)

Hacer clic sobre el botón [Print When] y en el campo "Print only when expression is true" deberá capturar la siguiente instrucción: not empty(Arts.foto) 

Deberá seleccionar la opción: Scale Picture, retain shape 

![IMG](10.png)

Posteriormente deberá hacer clic en el botón [OK]. Se mostrará la siguiente ventana:

![IMG](11.png)

Dirigirse al menú: File/Save, para guardar los cambios realizados en el reporte. Cerrar el reporteador. 

Al consultar la cotización se mostrará de la siguiente manera: 

![IMG](12.png)