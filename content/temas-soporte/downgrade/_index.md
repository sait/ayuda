+++
title = "Realizar downgrade de versión"
description = ""
weight = 49
+++

Para realizar un downgrade de versión deberá seguir las siguientes intrucciones:

1.Tener disponible contrato y contraseña en caso de que el sistema pida activarla de nuevo.

2.Tener ejecutable de la versión anterior listo.

3.Identificar la carpeta donde está localizada la base de datos, para esto deberá ejecutar su sistema SAIT y dar clic en [Catálogo de Empresas].

![IMG](1.png)

Después seleccionará el directorio de la empresa y lo copiará usando las teclas CTRL + C

![IMG](2.png)


4.Posteriormente deberá bajar el siguiente programa: vfp6.zip [clic aquí para descargar](vfp6.rar) una vez descargado puede descomprimirlo en donde guste, ejecute el programa y teclee los siguientes comandos, importante dar ENTER en su teclado después de cada línea, si notamos la primer linea es la ruta que hemos copiado.

![IMG](3.png)

5.Listo, ya podrá entrar al sistema con una versión anterior sin que le muestre el mensaje de que se está utilizando una versión distinta.
