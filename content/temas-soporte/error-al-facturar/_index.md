+++
title = "Errores más recurrentes de timbrado y soluciones"
description = ""
weight = 13
+++

En la siguiente documentación mencionaremos algunos de los motivos más comunes por las cuales en ocasiones no podemos timbrar algún comprobante fiscal, de igual manera se mostrarán las soluciones a estos casos. 

<h3 class="text-primary">1.Imposible Conectarse con el Servicio Organizador de Comprobantes Fiscales (OCF)</h3>
**Mensaje de error**

![IMG](ocf.png)

**Posibles causas**

* La dirección IP del servidor cambió a partir de que se reinició o se actualizó el equipo.

* La computadora en donde se encuentra instalado el OCF está apagado.

* Los servicios de SAIT Bóveda se encuentran detenidos (en el servidor).

* El firewall o el antivirus del servidor está bloqueando el acceso a sait.exe o a SAITBovedaWin32

* No se colocó la regla  en el firewall  del servidor para aceptar peticiones de las estaciones de trabajo

**Posibles soluciones**

1.	Revise que la dirección IP del servidor no haya cambiado. 
2.	Revise que la computadora: SERVER-PC se encuentre encendida.
3.	Revise que el servicio SAITBoveda se encuentre iniciado.
4.	Revise que el antivirus no esté bloqueando a SAIT.exe o a SaitBovedaWin32
5.	Revise que en el servidor se haya colocado la regla en el firewall para permitir peticiones desde estaciones de trabajo.

<h3 class="text-primary">2.Este RFC del receptor no existe en la lista de RFC inscritos no cancelados del SAT</h3>

**Mensaje de error**

![IMG](rfc.png)

**Posibles causas**

* El RFC capturado en el cliente está incorrecto

* El RFC del cliente ha sido cancelado por el SAT

Posibles soluciones

1.	Verificar con el cliente el RFC, modificarlo en el menú de ‘Ventas / Clientes’, y reintentar de nuevo.

2.	Revisar el estatus del RFC en el portal del SAT en el siguiente enlace:
https://portalsat.plataforma.sat.gob.mx/ConsultaRFC/

<h3 class="text-primary">3.El emisor con RFC ‘XAXX010101000’ se encuentra inactivo</h3>

**Mensaje de error**
 
![IMG](xxx010101xxx.png)

**Posibles causas**

* No ha firmado liga de manifiesto proporcionada por SAIT.

* Ha capturado erróneamente el RFC en la configuración general del sistema.

* Su RFC ha sido cancelado por parte del SAT.

**Posibles soluciones**

1.	Ingresar a la liga que SAIT le debió proporcionar para firmar el manifiesto. Si aún no lo tiene en su correo electrónico, deberá comunicarse con su asesor de ventas SAIT.
2.	Verificar el RFC capturado en el menú de ‘Utilerías / Configuración General del Sistema’.
3.	Verificar su estatus ante el portal del SAT.

<h3 class="text-primary">4.Emisor no cuenta con timbres</h3>

**Mensaje de error**

![IMG](emisornocuenta.png)
 
**Posibles causas**

* Se ha terminado el paquete de timbres adquiridos.

* El RFC del emisor está capturado erróneamente en la configuración general.

**Posibles soluciones**

1.	Comunicarse con su asesor de ventas SAIT para verificar el estatus de sus timbres o bien, adquirir un nuevo paquete de timbres.
2.	Verificar el RFC capturado en el menú de ‘Utilerías / Configuración General del Sistema’.

<h3 class="text-primary">5.El rango de la fecha de generación no debe ser mayor a 72 horas para la emisión del timbre</h3>

**Mensaje de error**

![IMG](72hrs.png)

**Posibles causas**

* La fecha de la computadora está a muy atrasada o adelantada

* La hora de la computadora se encuentra adelantada por horas, minutos o segundos

* Está intentando facturar con más de 72 horas de anterioridad. 

**Posibles soluciones**

1.	Corregir la fecha y hora de la computadora.
2.	Verificar la fecha y hora de emisión capturada en el documento.

<h3 class="text-primary">6.Fecha fuera de Periodo</h3>

**Mensaje de error**

![IMG](fueraperiodo.png)
 
**Posibles causas**

* El rango de fechas de período de trabajo está restringido

* La fecha de la computadora es incorrecta

* La fecha que usted está ingresando en el sistema  es incorrecta.

**Posibles soluciones**

1.	Actualizar el rango de fechas de período de trabajo en el menú de ‘Utilerías / Definir Períodos de Trabajo’.
2.	Verificar la fecha de su computadora.
3.	Verificar la fecha que está capturando en el sistema.

<h3 class="text-primary">7.Si se especifica el nodo ‘Traslados’ debe contener al menos un nodo Traslado</h3>

**Mensaje de error**

![IMG](nodo.PNG)

**Posibles causas**

* Al timbrar la factura global del día, se está incluyendo notas de ventas vacías.

* Fallas por apagones de luz, fallas en la red o fallas en el disco duro del equipo.

**Posibles soluciones**

1.Identificar la(s) nota(s) de venta que presentan el detalle para cancelarlas.

1.2. Abrir el archivo XML de error que genera el sistema.

1.3. Presionar las teclas CTRL + B para buscar el nodo de ‘Traslados’.

1.4. Cada nota de venta debe tener este nodo, si no viene especificado, deberá tomar nota del nodo de ‘NoIdentificación’, ya que ese es el folio de nota de venta que presenta el problema.

1.5. En SAIT, ‘Ventas / Consulta Individual’, consulte el folio de la nota y presione la opción de [Cancelar]. 

1.6. Intente de nuevo el timbrado de la factura global.

Validador de Solución Factible:
https://solucionfactible.com/sfic/validador_factura_electronica/validacionCFD.jsp 

<h3 class="text-primary">8.El valor de ‘TasaOCuota’ que corresponde a Traslado no contiene un valor del catálogo</h3>

**Mensaje de error**

![IMG](tasa2.png)

**Posibles causas**

* Está intentando facturar un producto con tasa de impuesto (IVA o IEPS) incorrecta.

**Posibles soluciones**

Revisar y corregir la tasa de impuesto de IVA o IEPS de cada uno de los artículos, directamente en el documento, o en el menú de ‘Inventario / Catálogo de Artículos y Servicios’.

<h3 class="text-primary">9.Errores con código 400 y 500</h3>

**Mensaje de error**

![IMG](sat.png)

**Posibles causas**

Los mensajes de error con códigos 400 y 500 son los tipos de errores que son devueltos por los PACs, en estos casos se deben a intermitencias de conexión con el SAT. 

**Posibles soluciones**

En estos casos la recomendación es intentar más tarde, en intervalos de 1 hora.

<h3 class="text-primary">10.Problemas al generar cadena oridinal del CFDI. Error en xsltproc.exe</h3>

**Mensaje de error**

![IMG](xslproc.png)

**Posibles soluciones**

* Revisar en la factura si existe algún valor extraño o carácter raro en los artículos, en el cliente, en las observaciones, etc.

![IMG](caracter.png)

* Cambiar nombre de carpeta C:\SAIT\LIBXSLT a LIBXSLTX para que al entrar el sistema baje las librerías nuevamente

* Si ninguna de las opciones anteriores dio resultados, hay que revisar que algún antivirus no esté bloqueando los archivos de sellado
