+++
title = "Error otro usuario grabando en estos momentos"
description = ""
weight = 16
+++

Si al usar su sistema SAIT le aparece el siguiente mensaje de error 

![IMG](error.png)

Deberá seguir las siguientes recomendaciones


- **Carpeta donde esta la base de datos tenga TODOS los permisos**: Corroborar la carpeta donde se encuentra la base de datos NO esté sólo como lectura, para corregirlo deberá dar todos los permisos de lectura y escritura, de igual forma si se conecta por medio de usuario la unidad de red de las estaciones y se tiene un usuario de Windows con acceso restringido, se deberá cambiar a usuario tipo administrador. Si la carpeta donde está base de datos no tiene el permiso de TODOS, deberá agregarla, dado clic derecho sobre la carpeta / Propiedades / Uso Compartido / Compartir

![IMG](permisos.jpg)

- **Agregar exclusiones de antivirus en todos los equipos**: Dependiento el antivirus que tenga instalado en sus equipos, se deberá asegurar que estén agregadas las exclusiones correspondientes tanto de la carpeta de SAIT como el ejecutables.

- **Tener equipos con cable y no wifi**: Si bien el sistema se puede utilizar por medio de wifi, es de suma importancia se cambie la red a cable por eser esta mucho más estable que inalámbrico. 

- **Identificar cuál es el equipo que suele mostrar más este error, porque puede ser tenga algún detalle y se tarde en grabar**: Si detecta que es un equipo el que marca este error, se recomienda revisarla en específico ya que puede ser la red de la misma esté lenta o tenga un problema físico en el disco duro que hace que tarde en grabar en el servidor y por ende "retenga" la tabla más de lo debido y ocasiona que otros equipos no puedan grabar en la base de datos.

