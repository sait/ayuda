+++
title = "No solicitar contraseña al conectarse por red"
description = ""
weight = 58
+++

En ocasiones aunque en el servidor y estación ya se haya deshabiliado el pedir contraseña para conectarnos por red, en algunas versiones de windows,  te obliga a definir contraseña en el servidor y así poderse conectar

Esta configuración le permitirá que NO solicite contraseña y pueda conectarse a la unidad de red donde se encuentra la base de datos

1.En el servidor presionar teclas "WIN + R" y escribir "secpol.msc"

![IMG](ejecutar.png)

2.Se abrirá esta ventana 

![IMG](1.png)

3.Daremos clic en "directivas locales"

![IMG](2.png)

4.Clic en "Opciones de Seguridad"

![IMG](3.png)

5.Buscaremos la opción que diga "Cuentas: limitar el uso de cuentas locales con contraseña en blanco solo para iniciar sesión en la consola" y daremos clic derecho para seleccionar Propiedades

![IMG](4.png)

6.Palomeamos la opción de Deshabilitada y daremos clic en Aceptar

![IMG](5.png)