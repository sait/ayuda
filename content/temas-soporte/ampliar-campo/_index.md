+++
title = "Ampliar un campo en la base de datos"
description = ""
weight = 45
+++

### **Indicaciones**:

En ocasiones es necesario ampliar un campo en la base de datos ya que no es sufuciente para la información que estamos intentando guardar

Algunos campos que se suelen ampliar son el nombre del cliente, la colonia del cliente, el campo de email del cliente para agregar dos o más correos electrónicos

Es importante mencionar que NO todos los campos se pueden ampliar ya que algunos son campos llaves y no se puede modificar su estructura, si desea ampliar algún campo en específico y tiene la duda si se puede ampliar o no, favor de contactar al área de soporte.

<h4 class="text-primary">Puntos a considerar</h4>

* Primero deberá realizar un respaldo desde Utilerías / Respaldar Información

* Los campos solo se pueden ampliar a 200 caracteres

* Al hacer el proceso NINGÚN usuario deberá estar usando el sistema, ya que si alguien está dentro del sistema marcará este mensajes

![IMG](denied.png) 

### INSTRUCCIONES

1.Entrar a los comandos de VFP en Ayuda / Comandos de Visual Fox Pro 

![IMG](comandos.png) 

La clave se calcula de la siguiente manera MESX3 DIAX2 MESX3, la ingresamos y damos Enter

![IMG](clave.png) 

2.Ingresará las siguientes instrucciones (después de cada instrucción darás ENTER en tu teclado): 

use clientes exclusive

alter table clientes alter email1 c(200) 

alter table clientes alter email2 c(200) 

use

salir

3.Generar índices en Utilerías / Generar Índices

4.Listo, de esta manera se amppliaron los 2 correos de clientes (el de la pestaña de ventas y el de cobranza).

![IMG](CORREOS.png) 


<h4 class="text-danger">

Este proceso amplía los correos electrónicos del catálogo de clientes, si requiere ampliar otro campo, recomendamos contactar al área de soporte para indicar el campo que desea ampliar, de esta manera le podemos indicar el nombre y también confirmar si es un campo que se puede expandir ya que no todos los campos pueden ampliarse.

</h4>



