+++
title = "Error Las unidades del articulo: XXX ya no existen"
description = ""
weight = 21
+++

Si al momento de enlazar una cotización, le aparece el siguiente mensaje, deberá seguir las siguientes recomendaciones para corregirlo

![IMG](1.png)

Primero debe saber que este mensaje aparece porque se hizo una cotización con un artículo que tenia una unidad de medida y después de hacer la cotización, en el catálogo de artículos se cambió la medida (acción que no debe de realizar puesto que si ya tenemos movimientos en el kárdex, los costos se verán afectados erróneamente)

Una forma de identificar qué unidad tenía el artículo, es consultar la cotización en Ventas / Consulta Individual y consultar a la vez el artículo en Inventario / Catálogo de Artículos y Servicios

Aquí se observa claramente que el artículo tenia unidad PZA y se cambió a CAJA, en este caso lo más correcto y para que le permita enlazar la cotizáción a la factura, es volver a poner la unidad PZA en el artículo ABRE, si cambió la unidad de un artículo lo más recomendable es crear una nueva clave con la unidad nueva.

![IMG](2.png)


