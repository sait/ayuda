+++
title = "Configurar Ventas - Mostrador"
description = ""
weight = 37
+++

Esta opción al vender se suele utilizar en giros en donde se atiende el cliente llega a mostrador y los vendedores agregan los artículos que el cliente pide procesan la nota, pero ellos no se encargan de cobrarla sino que se dirigen a caja y el cajero se encarga de cobrar la nota y recibir el dinero del cliente. 

A continuación mostraremos las configuraciones que deberá realizar si desea utilizar esta funcionalidad en su sistema SAIT:

<h2 class="text-primary">Puntos a considerar</h2>

* En esta opción aparecerán únicamente las ventas realizadas desde Ventas / Registrar Ventas F4, las ventas hechas desde Caja / Registrar Ventas no aparecen ya que aquí si se realiza el cobro de la venta al momento de procesar la venta

* Solo aplica para los tipos de venta tipo: facturas y nota, no se consideran las remisiones puesto que no es un documento fiscal como tal.

<h2 class="text-primary">Configuraciones a realizar</h2>

* [1) Borrar registros de ventas antiguas](#borrar-registros-de-ventas-antiguas)

* [2) Activar opción de imprimir notas / factura](#activar-opcion-de-imprimir)

* [3) Configurar opción de capturar pago antes de procesar](#configurar-opcion-de-capturar-pago-antes-de-procesar)

* [4) Configurar formato de nota a nivel de usuario vendedor](#configurar-formato-de-nota-a-nivel-de-usuario-vendedor)


### Borrar registros de ventas antiguas

Es necesario asegurarse la tabla cajarem esté completamente vacía antes de iniciar esta modalidad, para ello deberá de asegurarse nadie esté utilizando el sistema y después se dirigirá a Ayuda / Comandos de Visual Foxpro, se le pedirá una clave, esta clave cambia de día en día y se genera de esta forma MESX3 DIAX2 MESX2, ingresa la clave y después en la ventana de comandos, teclee los siguientes comandos danto ENTER después de cada línea:

USE CAJAREM EXCLUSIVE

ZAP

USE 

SALIR

Si había registros Caja / Recibir Pagos (Contado) con esto se habrán borrado.

![IMG](1.png)

### Activar opcion de imprimir

Procese una venta desde Ventas / Registrar Ventas F4 y diríjase a Caja / Recibir Pagos (Contado) de [Enter] sobre el registro y de clic en [Configurar]

![IMG](2.jpg)

Y active las opciones deseadas:


* Imprimir Factura / Nota: esta opción obligatoriamente debe estar activa o al cobrar NO se imprimirá la nota / factura

* Imprimir formato de pago: esta opción es opcional, si se activa, se imprimira el formato que tenga definido en Caja / Tipos de Movimientos

![IMG](3.jpg)

### Configurar opcion de capturar pago antes de procesar

Es importante mencionar que debe haber al menos 2 grupos o niveles de usuarios, ejemplo CAJERO y VENDEDOR, esto es importante porque al VENDEDOR (o cualquier otro nivel que NO sea el cajero) se deberá verificar NO tenga activa la opción de Capturar Pago, antes de procesar notas y facturas de contado en el menú de Utilerías/Grupos de Usuarios-Catálogo de Niveles

En el nivel VENDEDOR debe estar deshabilitada

![IMG](4.jpg)

En el nivel CAJERO debe estar habilitada

![IMG](5.jpg)

### Configurar formato de nota a nivel de usuario vendedor

Finalmente verificaremos los permisos del formato de nota o factura, para esto deberá ir a Utilerías / Modificar Formatos / Seleccionaremos el formato de Notas de Venta y en la sección de Grupos, dejaremos solo el nivel de Cajero, en este caso es el 3

De esta manera si los vendedores o cualquier otro usuario, procesa una nota de venta solo se procesará, pero no se imprimirá el formato, ya que eso se hará hasta que el Cajero cobre dicha venta.

![IMG](6.png)

Listo, ahora puede utilizar la modalidad Ventas - Mostrador explicada [aquí](/sait-basico/11-capacitacion-modulo-caja/g-recibir-pagos-contado/)  




