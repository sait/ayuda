+++
title = "Error: Reading File"
description = ""
weight = 8
+++

En ocasiones, sucede que cuando estamos trabajando con el sistema nos muestra muestra el siguiente:

![IMG](error.png)

Y al dar click en [Aceptar] se cierra el sistema, esto sucede por cuestiones de red y por lo general en equipos estación.

**Es importante tomar en cuenta, que este tipo de interrupciones en los procesos que se realizan el sistema, pueden provocar pérdida de información o que se guarden registros incorrectos.**

Otra manera de comprobar si el sistema está teniendo constantemente pérdida de la señal, es dirigiéndose al directorio de la empresa.

Si no conoce el directorio de su empresa, solo tiene que ejecutar SAIT y dar clic en el botón de [Catálogo de Empresas]

Y copiar la siguiente ruta y colocarla en el explorador de archivos de Windows

![IMG](catalogo.png) 

Una vez dentro deberá localizar el archivo llamado errores.txt

Si en su empresa hay errores de este tipo, es URGENTE un asesor en hardware revise su red

![IMG](errorestxt.png)


<h3 class="text-primary">Recomendaciones SAIT para este mensaje de error</h3>

* Revisar puntas de los cables tanto los que van al CPU de las computadoras, como las que van al módem o switch.

* Revisar cables de red de todos los equipos.

* Se recomienda que la red esté por medio de cables Utp y NO por red inalámbrica, ya que este tipo de red es muy inestable y esto provoca que la más mínima interrupción de la señal, se pierda la conectividad con el servidor.

* Revisar equipos de la red como switch, módem, alguna tarjeta de red o el cableado, ya que también puede ser que estén fallando. 




