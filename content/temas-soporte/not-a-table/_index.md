+++
title = "Error: ERROR NOT A TABLE"
description = ""
weight = 7
+++

En empresas que manejan grandes volumenes de información y tienen varios años utilizando SAIT, es recomendable respaldar y depurar la información para evitar que las bases de datos lleguen a su máximo y llegar a la situación que ya no permita acceder a ellas.

Cuando una tabla llega a su capacidad máxima de almacenamiento, pueden ser 2 archivos lo que se llenan:

* Archivo .DBF: Llega a su limite cuando tiene varios millones de registros o se llega al tamaño de 2 GB.

* Archivo .FPT: En este tipo de archivos se graban los campo tipo Memo, debido a que este tipo de campos no tiene un espacio limitado al momento de grabar datos, normalmente se utiliza para grabar información extensa relacionada con el registro. En este tipo de campo se graba la información de factura electrónica: cadena original, sello digital y el XML.

* En el momento que en el sistema se intenta acceder a una tabla que ha llegado a su límite, se presenta el mensaje: **ERROR NOT A TABLE**

![IMG](does-not-match.png)

El siguiente proceso, lo que hace es corregir el problema de manera momentánea, es decir permite acceder a la tabla, pero es necesario depurar inmediatamente ya que si se sigue usando, de nuevo marcará el error.

## Programa para Reparar la Tabla

* Realize un respaldo de su información.
* Descargue el programa para reparar la tabla. [Descargar Aquí](Fixtabla.zip).
* Grabe el programa dentro de la carpeta de la empresa. Ejemplo: **C:\SAIT\CIA001**
* Descargue VFP6. [Descargar Aquí](http://www.sait.com.mx/download/vfp6.zip).
* Descomprima el archivo directamente en la unidad C:\ de su computadora.
* Ejecute VFP6.
* Se muestra la ventana de comandos.
* Coloquese en el directorio de la empresa donde se encuentra la tabla a recuperar. Ejemplo: CD C:\SAIT\CIA001.
* Escriba la siguiente instruccion: do fixtabla.
* Se solicita el nombre de la tabla reparar. Ejemplo: Docum (presione la tecla [Enter]).
* Se muestra la información de la tabla. Al preguntar si desea repararla, escriba **SI** y presione la tecla **[Enter]**.
* * ![IMG](imagen1.PNG)
* Una vez terminado el proceso, se muestra la ventana de comandos.

{{%alert success%}}NOTA: Es importante que una vez que se ha reparado la tabla, entre a SAIT para generar índices y realice el proceso de depuración de información, si cuenta con enlace de sucursales se recomienda cerrar el enlace en lo que realice todo el proceso.{{%/alert%}}

 











