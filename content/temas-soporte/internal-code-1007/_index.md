+++
title = "Error Internal Code 1007 al entrar al sistema"
description = ""
weight = 20
+++

Si cuando ingresa a su empresa le aparece el siguiente mensaje de error deberá seguir las siguientes recomendaciones para corregirlo.

![IMG](internal.png)


<h3 class="text-primary">Recomendaciones SAIT para este mensaje de error</h3>

**Verificar que la ruta de la empresa esté correcta:** para ello deberá abrir el sistema y dar clic en [Catálogo de Empresas]

![IMG](cat-empresa.png)

Ahora deberá seleccionar la ruta de la empresa (lo que aparece en azul) y deberá presionar la combinación de teclas CTRL+C para copiar la ruta en el portapapeles

![IMG](seleccionar-empresa.png)

En su explorador de archivos de windows, en la barra pegará la ruta que copiamos con las teclas CTRL+V y dará Enter en su teclado

![IMG](explorador.jpg)

Si la ruta está correcta, le abrirá la base de datos y le mostrará las tablas del sistema

Si está incorrecta, le mostrárá el siguiente mensaje de error, si este este el caso deberá localizar la carpeta correcta en donde está la base de datos y corregirla en el Catálogo de Empresas

![IMG](error.jpg)

**Permisos de escritura en carpeta compartida:** Corroborar la carpeta donde se encuentra la base de datos NO esté sólo como lectura, para corregirlo deberá dar todos los permisos de lectura y escritura, de igual forma si se conecta por medio de usuario la unidad de red de las estaciones y se tiene un usuario de Windows con acceso restringido, se deberá cambiar a usuario tipo administrador. Si la carpeta donde está base de datos no tiene el permiso de TODOS, deberá agregarla, dado clic derecho sobre la carpeta / Propiedades / Uso Compartido / Comparti

![IMG](todos.jpg)

**Antivirus:** Corroborar que el antivirus que está instalado, tanto en estaciones como en servidor tenga agregadas las exclusiones del sistema SAIT de manera correcta, si usted ya había agregado previamente estas exclusiones, pero el sistema le arroja este error es posible que el antivirus se haya actualizado por lo que deberá agregarlas nuevamente.

**Índices:** En ocasiones puede suceder que mientras se estén generando los índices en el sistema por por primea vez, un usuario trate de entrar al sistema lo que puede ocasiones el mensaje Internal Code 1007, en estos casos solo es esperar qu se terminen de generar los índices para después entrar con normalidad.

**Tabla eliminada:** Otro motivo por el cual se nos puede mostrar este error es que alguien por error haya borrado alguna tabla del sistema, si este es el caso se deberá de recuperar, dependiendo de la tabla borrada varía el procedimiento de reestablecimiento, recomendamos consultar con su asesor de sistemas SAIT sobre el proceso a seguir.

**Tabla empresas.dbf está dañada o borrada:** En la carpeta de instalación de SAIT, se encuentra una tabla llamada empresas.dbf, aquí se guardán las empresas y las rutas de la base de datos de todas las empresas que a usted le aparecen al ejecutar el sistema, esta tabla se puede dañar por algún elemento externo como apagones de luz o un archivo malicioso, para corregirlo deberá descargar la tabla vacia [clic aquí para descargar](empresas.dbf) y colocarla reemplánzola con la que existente en la carpeta de instalación del sistema, ejemplo: C:\SAIT O C:\SAITBASICO 