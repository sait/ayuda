+++
title = "Configuracion de Lector de Huella Digital"
description = ""
weight = 33
+++

Como ustedes saben, dentro de los paquetes que conforman la familia **SAIT Software Administrativo**, se manejan usuarios operadores de dichas aplicaciones, en donde los usuarios dependiendo del nivel o grupo de usuario pueden realizar ciertas acciones, por cuestión de seguridad.

En muchas ocasiones los mismos usuarios se saben las contraseñas de niveles de supervisión general del sistema y/o gerentes generales, lo que dificulta realmente llevar un control en cuanto a la seguridad de los módulos.

Parecería lógico utilizar algún identificador que no se pudiese perder, cambiar o falsificar. Las técnicas de la biometría se aprovechan del hecho de que las características del cuerpo humano son únicas y fijas, tal como es el caso de la HUELLA DIGITAL.

{{%alert success%}}Es por eso que **SAIT SOFTWARE ADMINISTRATIVO** viene preparado para el acceso a cada una de nuestras soluciones y en ciertos procesos de confirmación, a través de la HUELLA DIGITAL.{{%/alert%}}


* [Módelo de Lector de Huella Digital](#módelo-de-lector-de-huella-digital)
* [Área de Descarga](#área-de-descarga)
* [Instalación de Aplicaciones](#instalación-de-aplicaciones)
* [Configurar y Ejecutar aplicación FREADER para probar lector](#configurar-y-ejecutar-aplicación-freader-para-probar-lector)
* [Configurar en el grupo de inicio aplicación FREADER](#configurar-en-el-grupo-de-inicio-aplicación-freader)
* [Configurar en SAIT el acceso de Huella Digital](#configurar-en-sait-el-acceso-de-huella-digital)
* [Acceso a SAIT a través de lector de huella digital](#acceso-a-sait-a-través-de-lector-de-huella-digital)
* [Procesos Adicionales de Uso de Huella Digital](#procesos-adicionales-de-uso-de-huella-digital)


### Módelo de Lector de Huella Digital

Antes de entrar de lleno con la configuración de nuestro lector de huella digital, a continuación ponemos a tu disposición las especificaciones de los modelos compatibles con las soluciones SAIT:

<h2 class="text-primary">U. are U. 4500</h2>

| **Modelo:** | **U. are U. 4500 Fingerprint Reader.** |
| --- | --- |
| Foto del Lector: | ![IMG](imagen1.PNG)|
| Fabricante: | Digital Persona |
| Luz: | Luz LED en color Azul. |
| Sistemas Operativos Compactibles | Microsoft Windows 8.1, 7, 10, Vista y XP. |

<h2 class="text-primary">U. are U. 5300</h2>

| **Modelo:** | **U. are U. 5300 Fingerprint Reader.** |
| --- | --- |
| Foto del Lector: | ![IMG](NUEVOMODELO.png)|
| Fabricante: | Digital Persona |
| Luz: | Luz LED en color Azul. |
| Sistemas Operativos Compactibles | Microsoft Windows 8.1, 7, 10|

### Área de Descarga de cada modelo

Una vez que hemos adquirido el lector de huella digital U.are.U 4500 FingerPrint Reader, es necesario descargar los siguientes paquetes para su instalación:

<h2 class="text-primary">U. are U. 4500</h2>

| Nombre del Archivo | Comentarios sobre la aplicación | Descarga |
| --- | --- | --- |
| DigitalPersonaRTE | Instalador de Driver para lector de huella digital 4500 FingerPrint Reader. | [Descargar](4500-drivers.zip) |
| Freader | Aplicación para establecer interface y comunicación con los paquetes de SAIT Software Administrativo. | [Descargar](4500-instalador-freader.zip) |

<h2 class="text-primary">U. are U. 5300</h2>

| Nombre del Archivo | Comentarios sobre la aplicación | Descarga |
| --- | --- | --- |
| DigitalPersonaRTE | Instalador de Driver para lector de huella digital 4500 FingerPrint Reader. | [Descargar](5300-drivers.zip) |
| Freader | Aplicación para establecer interface y comunicación con los paquetes de SAIT Software Administrativo. | [Descargar](5300-instalador-freader.zip) |

### Instalación de Aplicaciones

Una vez descargado los archivos, deberá de descomprimir cada uno de ellos. A continuación explicaremos como instalar ambos archivos:

{{%alert success%}}Instalación FREADER{{%/alert%}}

La carpeta FREADER deberá de colocarla dentro del disco local C:.

{{%alert success%}}Instalación de Driver{{%/alert%}}

* Entrar a la carpeta DigitalPersonaRTE y ejecutar el archivo: Setup.exe, para esto deberá hacer doble clic o [ENTER] sobre dicho archivo.
* Se mostrará el asistente de instalación, tal como se visualizar a continuación:
![IMG](imagen2.PNG)
* Hacer Clic en el botón [Next >]. A continuación deberá aceptar los términos de uso de licencia, tal como se muestra a continuación:
![IMG](imagen3.PNG)
* Hacer Clic en el botón [Next >]. Posteriormente deberá de confirmar el directorio sobre el cual el instalador, utilizara para almacenar las librerías, deberá de dejar el que viene por default, como se muestra a continuación:
![IMG](imagen4.PNG)
* Hacer Clic en el botón [Next >]. De igual manera nos informa el listado de programas a instalar, a lo que nuevamente deberá de presionar el botón [Next >]. Se mostrará la siguiente ventana:
![IMG](imagen5.PNG)
* Deberá hacer clic en el botón [Install] para dar inicio con el proceso de instalación. En esos momentos se ejecutara la instalación y través de una barra de progreso, usted podrá visualizar el estado de la instalación.
* Una vez finalizado el proceso, se visualizará la siguiente pantalla, a lo cual usted deberá de hacer clic sobre el botón [Finish].
![IMG](imagen6.PNG)
* En estos momentos, windows nos mostrará la siguiente ventana:
![IMG](imagen7.PNG)
* A lo cual nos menciona que para guardar los cambios y configurar de manera exitosa el driver, es necesario REINICIAR EL EQUIPO, a lo cual usted deberá hacer clic en el botón [YES].
![IMG](imagen8.PNG)
* Listo!!! Con estas instrucciones has instalado de manera exitosa el driver del lector de huella digital.



### Configurar y Ejecutar aplicación FREADER para probar lector

* Una vez instalado ambos archivos, el siguiente paso es conectar al equipo nuestro lector de huella digital y en esos momentos windows automáticamente lo configura, tal como se muestra en la siguiente imagen:
![IMG](imagen9.PNG)
* Como siguiente paso, debemos de ejecutar la aplicación freader misma que se encuentra en la ubicación C:\freadre\freader.exe.
* Para ejecutarlo deberá hacer doble clic o **[ENTER]** sobre el archivo freader.exe y en la parte inferior derecha de windows, se deberá de mostrar lo siguiente:
![IMG](imagen10.PNG)
* 	Deberá hacer clic derecho sobre el icono mostrado en la barra de herramientas y elegir a opción "Reader Finger". Tal como se muestra a continuación:
![IMG](imagen11.PNG)
* Una vez seleccionada, aparecerá una ventana en la cual a manera de prueba le pedirá capturar una huella digital, esto como prueba de la comunicación con nuestro equipo, tal como se muestra a continuación:
![IMG](imagen12.PNG)
* Usted deberá de colocar su dedo en 4 ocasiones, para tomar lectura y ver que nuestro dispositivo si funciona de manera correcta.

### Configurar en el grupo de inicio aplicación FREADER

Este paso solo aplica para el modelo 4500

* Hacer Clic en el botón [Inicio].
* Seleccionar la opción "Todos los Programas" y posteriormente seleccionar la opción "Inicio" y hacer un doble clic. Seleccionar la opción ABRIR. Se visualizará la siguiente ventana:
![IMG](imagen13.PNG)
* Deberá hacer clic derecho y dirigirse a la sección **"NUEVO"** y posteriormente hacer clic sobre la opción "Acceso Directo", tal como se muestra en la siguiente imagen:![IMG](imagen14.PNG)
* Se visualizará la siguiente ventana, en la cual usted deberá hacer clic sobre el botón [Examinar].
![IMG](imagen15.PNG)
* Posteriormente ubicaremos el directorio FREADER que se encuentra en nuestro disco local C:, de tal manera que seleccionaremos el archivo freader.exe, tal como se muestra:![IMG](imagen16.PNG)
* Hacer clic en el botón [Aceptar]. Windows Nos pregunta algún nombre a mostrar, a lo cual lo dejaremos intacto tal como se muestra en la imagen. Hacer clic en el botón [Finalizar].
![IMG](imagen17.PNG)
* Listo!!! Con eso hemos configurado windows para que cada vez que se encienda el equipo, se ejecute el programa de nuestro lector de huella digital.


### Configurar en SAIT el acceso de Huella Digital

* Accesar a SAIT y colocar su respectivo usuario y contraseña.
* Dirigirse al menú de: Utilerías / Catálogo de Usuarios. En ese momento se visualizará el catalogo de usuarios. Deberá de seleccionar algún usuario en el listado y posteriormente hacer clic en el botón **[Modificar]**.
* Posicionarse sobre el campo "MÉTODO DE ACCESO" y seleccionar "Usar Huella Digital". Tal como se muestra en la siguiente imagen:
![IMG](imagen20.PNG)
* Posteriormente, deberás de hacer clic sobre el botón **[Leer Huella Digital]** para grabar la primera huella, en este caso se visualizará la siguiente ventana:
![IMG](imagen21.PNG)
* Deberá pasar el mismo dedo para su lectura en 4 ocasiones. Como se muestra a continuación:
![IMG](imagen22.PNG)
* Una vez leída las 4 veces, deberá hacer clic en el botón [Aceptar Huella]. Como se visualiza en la siguiente imagen:
![IMG](imagen23.PNG)
* En caso de ser necesario, usted puede definir hasta dos huellas para determinar el acceso.
* Para finalizar con el proceso, hacer clic en el botón **[GRABAR]**.
* Esta acción la deberá de repetir para los usuarios que se encuentre grabados en dicho catálogo.


### Acceso a SAIT a través de lector de huella digital

* Ejecutar la aplicación SAIT.
* Colocar el número de usuario y dar presionar la tecla **[ENTER]**. En ese momento se visualizará la siguiente ventana:
![IMG](imagen18.PNG)
* El usuario deberá de colocar su huella digital para verificar el ingreso. Si esta es correcta, usted ingresara sin ningún problema a SAIT. Pero si la huella no corresponde, se mostrará el siguiente mensaje:
![IMG](imagen19.PNG)
* Si el acceso es correcto, se iniciara SAIT sin ningún problema.

### Procesos Adicionales de Uso de Huella Digital

A continuación enlistaremos procesos adicionales en los que el usuario le puede dar uso a la autorización por medio de huella digital:

* En usuarios o niveles que tengan restringida la opción de ELIMINAR PARTIDAS en la ventana de: **Caja/Registrar Ventas**.
* En usuarios o niveles que requieran autorización para salir de la ventana de: **Caja/Registrar Ventas**.





