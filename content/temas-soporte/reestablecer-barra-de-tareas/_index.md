+++
title = "Reestablecer barra de tareas"
description = ""
weight = 48
+++

Es muy comun que el usuario mueva la barra de herramientas principal de SAIT y esta se pierda en la posición asignada. Para recuperar dicha barra, deberas seguir los siguientes pasos, previamente asegurarse salirse del sistema SAIT en la maquina en donde se encuentre el detalle.

1. Hacer al botón [Inicio] de Microsoft Windows

2. Seleccionar la opción de [Ejecutar], tal como se muestra en la siguiente ventana: 

![IMG](1.jpg) 

3. En la ventana de Ejecutar, deberá de escribir REGEDIT y hacer clic en el botón [Aceptar], tal como se muestra en la siguiente ventana: 

![IMG](2.jpg) 

4. Diríjase a Equipo / HKEY_CURRENT_USER / Software / SAIT / tal como se muestra en la siguiente ventana: 

![IMG](3.jpg) 

5. Seleccionar la variable con nombre: POSMAINLBR, tal como se mostró en la ventana anterior, y presione la tecla [Del].

6. A manera de confirmación, Windows preguntara si desea eliminar el valor, a lo cual deberá seleccionar en [Si].

![IMG](4.jpg) 

7. Una vez eliminada la variable, cerrar la ventana del Editor de Registros.

8. Posteriormente deberá de accesar a SAIT Normalmente y notará que la barra de herramientas principal se restablece de manera normal. 