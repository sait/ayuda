+++
title = "Reestablecer tamaño de ventanas de SAIT"
description = ""
weight = 47
+++

Instrucciones para restablecer las propiedades predeterminadas del ancho y alto de las columnas de las ventanas del sistema.

1.Entrará al registro de windows, yendo al inicio y tecleando "regedit"

![IMG](3.png) 

2.Después se dirigirá a la siguiente ruta, localizará los siguientes registros y los eliminará.

![IMG](1.png) 

3.Le aparecerá un mensaje de confirmación y dará clic en sí.

![IMG](2.PNG) 

4.Entre de nuevo a SAIT de forma habitual y listo.



