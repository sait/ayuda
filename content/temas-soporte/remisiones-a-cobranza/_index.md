+++
title = "Configurar mandar remisiones al estado de cuenta del cliente"
description = ""
weight = 35
+++

Si usted maneja remisiones para llevar el control de saldos de sus clientes es necesario habilitar la opción en:

Utilerías / Configuración General del Sistema / Pestaña Ventas / Palomear la opción de "Mandar remisiones a Cobranza" 

![IMG](configuracion.png)

En caso caso de que haya hecho ya remisiones SIN tener dicha opción habiiltada, entonces tendrá que agregar esos cargos de manera manual al edo. de cuenta con las siguientes instrucciones:

Primero deberán de crear el formato de remisión en Cobranza / Conceptos de Cuentas por Cobrar / Agregar
 
Dar de alta el concepto tal y como aparece aquí:

![IMG](concepto.png)

Para el ejemplo usaremos esta remisión con folio AA70 que como se puede ver no está en el estado de cuenta

![IMG](factura-sin-agregar.png)

Después deberás agregar el cargo en el menú de Cobranza / Cargos / Selecciona el Concepto Remisión

Y llenarán los campos de clave de cliente, num. documento, fecha elaboración, fecha vencimiento, importe del documento, clic Procesar=F8

![IMG](factura-a-agregar.png)

De esta manera la factura AA70 ya aparecerá en el estado de cuenta.

![IMG](factura-ya-agregada.png)