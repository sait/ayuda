+++
title = "Ejecutar Instalador"
description = ""
weight = 1
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="Ejecutar Instalador.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Instaladores:

**Sait Básico:** [https://www.sait.mx/download/sait-basico-instalador.exe](https://www.sait.mx/download/sait-basico-instalador.exe)

**Sait ERP:** [https://www.sait.mx/download/sait-ERP-instalador.exe](https://www.sait.mx/download/sait-ERP-instalador.exe)

**Sait Nómina** [https://www.sait.mx/download/sait-nomina-instalador.exe](https://www.sait.mx/download/sait-nomina-instalador.exe)

**Sait Contabilidad:** [https://www.sait.mx/download/sait-contabilidad-instalador.exe](https://www.sait.mx/download/sait-contabilidad-instalador.exe)

Instrucciones generales a seguir para Reinstalar SAIT en una estación de trabajo:

1.Ejecutar el archivo instalador

Dar clic derecho al archivo que se descargó y seleccionar la opción de **[Ejecutar como Administrador]**.

![IMG](0.png)

2.Bienvenido al asistente de Instalación de SAIT ERP Dar clic en **[Siguiente]**.

![IMG](1.png)

3.Acuerdo de Licencia para el uso del sistema.

Para aceptarlo, seleccionar la opción de [Acepto el acuerdo] y dar clic en **[Siguiente]**.

![IMG](2.png)

4.Seleccionar la carpeta de destino. Dar clic en el botón de **[Siguiente]**.

Por default se define la ubicación en donde se instalará el sistema, dependiendo del paquete SAIT que se haya elegido en este caso SAIT ERP: C:\Sait

Esta ubicación es modificable si se requiere.

![IMG](3.png)

5.Seleccionar las tareas adicionales.

Dejar opciones como aparecen en la siguiente imagen.

![IMG](4.png)

6.Listo para Instalar.

Dar clic en **[Instalar]**

![IMG](5.png)

7.Instalando

Esperar mientras se termina el proceso de instalación del sistema SAIT.

![IMG](6.png)


8.Completando la instalación del sistema.

Dar clic en **[Finalizar]**.

![IMG](7.png)