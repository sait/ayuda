+++
title = "Actualizar Versión SAIT"
description = ""
weight = 5
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="Actualizar Versión de SAIT.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Todos los equipos con el sistema de SAIT que se encuentren conectados por unidades de red, deben de contar con la misma versión de SAIT que se tiene en el servidor para poder ingresar correctamente a la empresa. Para esto, debe de COPIAR el archivo sait.exe del directorio de SAIT dentro del equipo servidor. 

(Ej. C:\SistemaSait\Sait\sait.exe) y REEMPLAZARLO dentro del directorio de SAIT en la estación de trabajo.

1.El archivo sait.exe lo puede colocar dentro del recurso compartido para que todas las estaciones tengan acceso a él, o bien, puede grabarlo en un dispositivo USB para llevarlo a la estación de trabajo. 

En cualquier caso, en la estación de trabajo en donde se está realizando el servicio de reinstalación de SAIT, hay que seleccionar el archivo (ya sea dentro del recurso compartido o dentro del dispositivo USB), dar clic derecho y **[Copiar]**, o presionar las teclas CTRL C.

![img](6.png)

2.Identificar el directorio del sistema y abrir la carpeta (Ej. C:\Sait)

![img](7.png)

3.Presionar CTRL V **[Pegar]** o clic derecho dentro de la carpeta y dar clic en **[Pegar]**.

![img](8.png)

4.Como este archivo ya existe, hay que reemplazarlo.

5.Dar doble clic en el icono de SAIT para abrir el sistema.

6.Se mostrará la siguiente ventana y se deberá inhabilitar la opción de **[Preguntar siempre antes de abrir este archivo]**.

7.Presionar el botón de **[Ejecutar]**.


<h2 class="text-primary">Instalación Manual de librerías</h2>

En ciertas versiones el sistema le pedirá descargar nuevas librerías, las cuales deberían de descargarse de manera automática, pero en caso de no ser así y se muestre alguno de los siguientes mensajes deberá seguir las instrucciones que se indicarán a continuación dependiendo el mensaje de error.

## Librerías libxslt

Si se muestra el siguiente mensaje deberá seguir estos pasos:

![IMG](LIBXSLT.png)

1.Descargar el archivo http://contabilidad.digital/descarga/libxslt20181109.zip 

2.Diríjase a sus descargas y descomprima su contenido en una carpeta llamada 'libxslt'

2.1 Para descomprimir los archivos deberá dar clic derecho sobre el archivo comprimido y seleccionará Extract Files …

![IMG](extraer.PNG)

2.2 Posteriormente deberá seleccionar la carpeta a la cual nombramos como 'libxslt' y dar clic en **[Aceptar]**.

![IMG](extraer2.PNG)

3.Después deberá colocar dicha carpeta en el directorio de instalación del sistema, ejemplo: C:\SAIT o C:\SaitBasico 

De tal forma que quede así: **C:\SAIT\libxslt** 

![IMG](SAITLIBXSLT.PNG)

Si el sistema le indica que ya hay una carpeta con dicho nombre, como en la siguiente imagen deberá borrar la carpeta anterior y posteriormente pasar de nuevo la carpeta libxslt al directorio C:\SAIT 

![IMG](reemplazar.png)

## Librerías SATPROD

Si se muestra el siguiente mensaje deberá seguir estos pasos:

![IMG](SATPROD.png)

1.Descargar el archivo http://contabilidad.digital/descarga/satprod.zip 

2.Diríjase a sus descargas y descomprima su contenido en la carpeta donde se encuentra la información de la empresa.

2.1	Para conocer la ruta en donde están los archivos de su empresa deberá entrar a SAIT y dar clic en **[Catálogo de Empresas]**.

![IMG](catalogoempresas.png)

2.3 Copiar la ruta de la empresa, por ejemplo: Z:\SAIT\CIA004

![IMG](copiarruta.png)

2.4 Vaya al inicio de la computadora en el ícono de Windows, pegue la ruta y presione **[Enter]** en su teclado.

![IMG](iniciowindows.png)

2.5 De tal manera que se abra la carpeta con los archivos de la empresa, aquí deberá arrastrar los archivos descomprimidos de la carpeta satprod.zip 

El sistema le va a preguntar si desea reemplazar los archivos y deber dar clic en **[Si]**

![IMG](reemplazarsatprod.png)

