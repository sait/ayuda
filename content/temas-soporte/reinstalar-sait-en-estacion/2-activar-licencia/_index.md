+++
title = "Activar Licencia"
description = ""
weight = 2
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="Activar Licencia.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Indicaciones:

Para realizar la activación de licencia de uso de SAIT siga las siguientes instrucciones.

1.Para activar la licencia del sistema es necesario iniciar SAIT, seleccionar la empresa y dar clic en **[Accesar Empresa]**.

![IMG](1.png)

2.Al acceder a la empresa se muestra la siguiente ventana en donde de manera informativa muestra la versión actual del sistema.

Aún no se encuentra activada la licencia de uso del sistema, por ello se muestra el mensaje de Software en evaluación.

Para activar el sistema se debe dar clic en **[Registrar Sistema]**.

![IMG](2.png)


3.Se mostrará la siguiente ventana donde deberá capturar su número de contrato, contraseña SAIT y un correo electrónico.

Dar clic en **[Activar por Internet]**.

![IMG](3.png)

4.Posteriormente deberán capturar el nombre o alguna referencia para identificar la licencia que se activará. Dar clic en **[Activar Licencia]**.

![IMG](4.png)

5.En esta ventana ya mostrará el nombre de la razón social a quien ha sido concedido el uso de la Licencia SAIT. Dar clic en **[Continuar]**.

![IMG](5.png)