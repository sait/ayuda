+++
title = "Reinstalación  de SAIT en una estación de trabajo"
description = ""
weight = 1
+++

Si en algún momento se tiene la necesidad de cambiar de equipo o formatearlo ya sea por virus, fallas en el sistema operativo, etc. será necesario reinstalar el sistema en una estación de trabajo.

El proceso que debe realizar para reinstalar SAIT en una estación de trabajo es el siguiente:


{{% children  %}}

#### En caso de que prefiera ver el video con las instrucciones del proceso de reinstalación de una estación de trabajo puede dar clic en el siguiente video: 


{{<youtube ZHPBTUngO-c>}}
