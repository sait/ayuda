+++
title = "Redireccionar Directorios"
description = ""
weight = 4
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="Redireccionar Directorios.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Una vez que tenemos conectada la unidad de red a la carpeta correcta, debemos de ubicar exactamente donde se encuentran los archivos de nuestra empresa para poder acceder a ellos desde SAIT.

1.Accedamos a SAIT dando doble clic en el icono del programa.

2.Damos clic en el botón de **[Catalogo de Empresas]**.

![img](1.png)

3.Después clic en **[Agregar Empresa Existente]**:

![img](2.png)

4.Debe definir la ruta de la empresa o presionar el botón [?] para buscar el directorio:

![img](3.png)

5.Deberá buscar y seleccionar el directorio donde se encuentra la empresa:

![img](4.png)

![img](5.png)

6.Deberá dar clic en **[Continuar]**.

![img](6.png)

7.Y dar clic en **[Cerrar]** la ventana de “Catálogo de Empresas”.

![img](7.png)

8.Y listo, ya podrá ingresar a la empresa correctamente.

![img](8.png)


