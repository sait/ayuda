+++
title = "Conectar Unidad de Red"
description = ""
weight = 3
+++

<div class="card-row">
<div class="card">
<a href="#indicaciones">
<img src="/img_documento.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-file-text"></i></div>
</div>
<h4 class="title">Leer Documentación</h4>
</a>
</div>
<div class="card">
<a href="Conectar Unidad de Red.pdf"  target="_blank">
<img src="/img_pdf_download.png" class="image">
<div class="overlay">
<div class="text"><i class="fa fa-cloud-download"></i></div>
</div>
<h4 class="title">Descargar PDF</h4>
</a>
</div>

</div>
<br>
<hr>
<div id="indicaciones"></div>

### **Indicaciones**:

Indicaciones:

Puntos previos a considerar

1.Identificar claramente el directorio en donde se encuentra la información de las empresas.

2.Contar con los permisos necesarios dentro de Windows para leer y escribir en el directorio.

3.Si la carpeta es de un recurso compartido en la red y esta se comparte por medio de credenciales, requiere tener las credenciales a la mano.

## PROCEDIMIENTO PARA RECONECTAR UNIDADES DE RED

La conexión de unidades de red en Windows es sencilla, siempre y cuando se tenga acceso al equipo que comparte información.

Esta conexión puede ser por medio del nombre del equipo o por medio de la dirección IP.

A continuación se explican los procedimientos para conectar unidades de red:

## Tipo de Conexión 1

1.Lo primero es conocer el directorio al que se desea conectar ya sea por dirección IP o nombre del servidor.

Nombre del Equipo (Servidor de archivos): Server

IP del equipo (Servidor de archivos): 192.168.0.2

Carpeta Compartida: discof

2.Dar clic en el botón de inicio, después clic derecho a equipo y clic en conectar unidad de red como se muestra en la siguiente imagen:


![img](1.jpg)


3.Esto nos llevará a la ventana de conexión donde colocaremos la dirección del recurso compartido a donde nos deseamos conectar, podemos dar clic en examinar para buscar como se muestra en la siguiente imagen:

![img](2.jpg)

4.También se puede colocar la ruta del directorio en el campo en blanco, recordando que la ubicación de un recurso compartido debe de colocarse de la siguiente manera: 

**\\\NombredelEquipo\NombreRecursoCompartido**

El resultado se mostraría así:

![img](3.jpg)

5.Después de haber definido la ruta o haber seleccionado el equipo y la carpeta correspondiente solo damos clic en **[Finalizar]** y listo.


## Tipo de Conexión 2

1.Lo primero es conocer el directorio al que se desea conectar ya sea por dirección IP o nombre del servidor.

Nombre del Equipo (Servidor de archivos): Server

IP del equipo (Servidor de archivos): 192.168.0.2

Carpeta Compartida: discof

2.Presionamos la teclas de "Windows + E", o bien entramos a Inicio y clic en equipo nos aparecerá la ventana de ‘Equipo’ y ahí damos clic en la opción conectar unidad de red en la parte superior de la ventana o bien en menú de Herramientas.

![img](4.jpg)

3.Esto nos llevará a la ventana de conexión donde colocaremos la dirección del recurso compartido a donde nos queremos conectar, podemos dar clic en ‘Examinar’ para buscar la ruta, como se muestra en la siguiente imagen.
4.También se puede colocar la ruta del directorio en el campo en blanco, recordando que la ubicación de un recurso compartido debe de colocarse de la siguiente manera: 

**\\\NombredelEquipo\NombreRecursoCompartido**

El resultado se mostraría así:

![img](5.jpg)

5.Después de seleccionar el equipo y la carpeta correspondiente solo damos clic en **[Finalizar]** y listo.

## Tipo de Conexión 3

1.Lo primero es conocer el directorio al que se desea conectar ya sea por dirección IP o nombre del servidor.

Nombre del Equipo (Servidor de archivos): Server

IP del equipo (Servidor de archivos): 192.168.0.2

Carpeta Compartida: discof

2.Presionamos la teclas de "Windows + R", o bien entramos a Inicio y buscamos la aplicación de ejecutar.

![img](7.jpg)

3.Ahora colocamos la dirección IP o el nombre de la maquina que tiene el recurso compartido con el directorio de las empresas (Ej: \\\Server) y damos clic en aceptar:

![img](8.jpg)

4.Esto nos llevará a la ventana donde se encuentran los directorios y archivos compartidos del equipo en cuestión, como se muestra a continuación. 


5.Deberá seleccionar la carpeta a la que se desea conectar, dar clic derecho y seleccionar la opción “Conectar a unidad de red”.

![img](9.jpg)

6.Lo mejor de esta opción es que no se ocupa escribir ni buscar el directorio a conectar ya que el directorio se selecciona automáticamente a la apertura de la ventana y esto nos ayuda a no equivocarnos en la ruta.


El resultado se mostraría así:

![img](11.jpg)

7.Después de seleccionar el equipo y la carpeta correspondiente solo damos clic en **[Finalizar]** y listo.