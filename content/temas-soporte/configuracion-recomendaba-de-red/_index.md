+++
title = "Configuración de red recomendada"
description = ""
weight = 56
+++

A continuacion se recomiendan las configuraciones recomendadas que debe tener en su empresa para el funcionamiento óptimo de sus sistema SAIT, dichas configuraciones se deben realizar tanto como en servidor como estaciones.

<h3 class="text-primary">Red privada</h3>

Asegurarse todos los equipos de la red, estén con la configuración de red PRIVADA y no pública

1.Clic derecho sobre el ícono de red en la barra de tareas y clic en Configuración de Red e Internet

![IMG](iconored.png)

2.Clic en Propiedades

![IMG](propiedadesred.png)

3.Cambiar a Red Privada

![IMG](redprivada.png)

<h3 class="text-primary">Activar protocolo SMB</h3>

Configuración no necesaria si su Sistema Operativo es de tipo Server

1.Ir a Panel de Control \ Programas \ Programas y características \ Activar o desactivar las características de Windows

2.Activar todas las funciones y dar clic en Aceptar

![IMG](smb.png)

3.Considerar que al terminar le pedirá reiniciar el equipo.

<h3 class="text-primary">Permisos para compartir archivos</h3>

La ubicación de esta configuración la encuentra en Panel de Control / Redes e Internet / Centro de redes y recursos compartidos / Cambiar configuración de uso de compartido avanzado

![IMG](centro.png)

Habilite todos los permisos indicados en todos los perfiles:


![IMG](activar.png)

<h3 class="text-primary">Permisos de escritura en carpeta compartida</h3>

Corroborar la carpeta donde se encuentra la base de datos NO esté sólo como lectura, ubique la carpeta en donde está la base de datos de SAIT y de clic derecho sobre la carpeta / Propiedades / Uso Compartido / Compartir

![IMG](compartir.png)



