+++
title = "Facturar usando ObjetoImp=01"
description = ""
weight = 57
+++

A partir de la versión 2024.6 SAIT le permite realizar facturas utilizando el  ObjetoImp=01 "no objeto de impuesto"

Importante mencionar que esta configuración es general por lo que si sólo necesita realizar una factura con estas características, después deberá desactivar esta opción

Para activar esta opción en el sistema deberá seguir las siguientes instrucciones

1.Ir a Utilerías / Factura Electrónica / Configurar CFDI 

Active la siguiente opción y de clic en Guardar Configuración

![IMG](Activar1.png)

Le aparecerá el siguiente mensaje, de clic en Sí

![IMG](Activar2.png)

2.Realice la factura 

![IMG](Factura1.png)

Al consultar el XML notará que no existe la sección de Traslados

![IMG](Factura2.png)

A diferencia de una factura normal

![IMG](Factura3.png)






