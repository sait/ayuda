+++
title = "Módulo Cliente Frecuente"
description = ""
weight = 4
+++

Este módulo está diseñado para empresas en cuya promoción desean incluir la acumulación de puntos sobre ventas a los clientes frecuentes, para posteriormente pagar con esos puntos en futuras ventas.

La forma de usar puntos en la venta es aplicando un descuento equivalente a los puntos sobre el total de la venta.

<h3 class="text-primary">Funcionamiento</h3>

* La aplicación de puntos está preparada para funcionar tanto desde Ventas / Registrar Ventas y Caja / Registrar Ventas 

* Los puntos  ganados y usados se ven reflejados cuando registre el pago al procesar la venta 
	
* Sí utiliza la modalidad ventas mostrador, es decir realiza la venta desde Ventas / Registrar Ventas y no tiene habilitado el permiso de * Capturar pago, antes de procesar notas y facturas de contado, los puntos ganados y usados se ven reflejados hasta que se paga el documento desde Caja / Recibir pagos (contado) F12

* En caja, en el momento que se captura el cliente se identifica si es frecuente o no. En registro de ventas, se puede configurar manejarlo así, o bien, leer tarjeta para identificarlos.

* Los puntos se calculan automáticamente cuando se procesa una Nota de Venta de Contado o Factura de Contado.

* Se permite definir el porcentaje de la venta que acumula puntos de forma global para todos los clientes, o bien, un porcentaje especial por cliente.

* El valor de cada punto únicamente funciona bajo la divisa: Pesos.

* Se permite definir un descuento adicional si el cliente cumple años en el mes o en el día de la venta. Esta promoción queda a consideración según se configure el sistema.

<h3 class="text-primary">Procesos</h3>

* [Uso de porcentaje2 para aplicar equivalencia de puntos usados](#uso-de-porcentaje2)
* [Actualizar formatos de corte de caja](#actualizar-formatos-de-corte-de-caja)
* [Configurar puntos](#configurar-puntos)
* [Agregar artículo con clave PUNTOS](#agregar-articulo-con-clave-puntos)
* [Identificar Clientes Frecuentes](#identificar-clientes-frecuentes)
* [Registrar Ventas / Leer Tarjeta](#registrar-ventas-y-leer-tarjeta)
* [Usar puntos](#usar-puntos)
* [Devolver puntos](#devolver-puntos)
* [Facturar Nota](#facturar-nota)

### Uso de porcentaje2

Debido a que la forma de usar puntos será en base al porcentaje de descuento al que equivalen, el sistema deberá estar configurado para usar el porcentaje de descuento2 para que ahí se vean reflejados los puntos a usar.

Para habilitarlo deberá ir a Utilerías / Configuración General del Sistema / Pestaña Ventas2

Para que tome los cambios deberá cerrar por completo el sistema y entrar nuevamente

Si maneja sucursales este proceso se hace en cada ubicación

![img](1.jpg)

### Actualizar formatos de corte de caja

Si usted utiliza cortes de caja, deberá actualizar los siguientes formatos ya los formatos default no consideran el descuento2 

[corte-caja-02a-(ventas-por-linea).rpt](corte-caja-02a-(ventas-por-linea).rpt)

[corte-caja-02b-(resumen-de-ventas-2014).rpt](corte-caja-02b-(resumen-de-ventas-2014).rpt)

Descargue ambos reportes y recíbalos en Utilerías / Recibir Reportes

Importante borrar ambos reportes desde Utilerías / Modificar Reportes / Cortes de Caja para que aparezcan duplicados

### Configurar puntos

Permite especificar el valor de cada punto en pesos y el porcentaje de la venta a otorgar en puntos.

![img](2.jpg)

Además, se pueden configurar las siguientes opciones:

* Leer tarjeta de cliente frecuente en registro de ventas: Al activar ésta opción, al vender dentro de Ventas / Registrar Ventas deberán leer la tarjeta del cliente para poder acumular / usar puntos. Si no se activa ésta opción, la acumulación de puntos en registro de ventas será al identificar el cliente frecuente desde la captura del cliente. En caja no aplica este proceso.

* Devolver puntos ganados/usados en devoluciones: Al activar ésta opción, al devolver una nota-factura con puntos ganados/usados se devolverán los puntos según el porcentaje de la venta al que equivale la devolución.

### Agregar articulo con clave puntos

Es necesario agregar un artículo (servicio) con la siguiente configuración:

![img](3.png)

* Clave: PUNTOS

* Descripción: PUNTOS DE CLIENTE FRECUENTE

* Habilitar el campo "Es un Servicio"

### Identificar Clientes Frecuentes

Dentro del catálogo de clientes, la clave del cliente frecuente se captura en el campo "Clave Adicional". Si van a manejar tarjetas de cliente frecuente con código de barras, ahí se deberá de capturar dicha clave para escanearlo con el lector óptico.

![img](4.png)

En la pestaña de “Otros Datos” se permite capturar la siguiente información:

* Fecha de Nacimiento del cliente para otorgarle un % de descuento especial en su cumpleaños.

* % de Puntos: Equivalente al porcentaje de la venta que aplica para puntos para este cliente.

* % Descuento Mes: Es el equivalente al porcentaje de descuento que se otorga al cliente si realiza una compra en el mes que cumple años.

* % Descuento Día: Corresponde al % de descuento al cliente si realiza una compra en el día en que cumple años.

### Registrar Ventas y Leer Tarjeta

En registro de ventas, si el sistema está configurado para leer tarjeta para acumular/usar puntos, se muestra el siguiente botón:

![img](5.jpg)

Deberá leer la tarjera para identificar al cliente frecuente y así mostrará la información de puntos:

![img](6.jpg)

### Usar puntos

Al registrar la venta, se deben capturar los artículos que el cliente desea comprar. Para usar los puntos disponibles, debe capturar el artículo **PUNTOS** para que se muestre la ventana de aplicar puntos:

![img](7.jpg)
 
De ese modo, al aplicar los puntos el porcentaje de equivalencia se verá reflejado en la columna de descuento2 de la venta, si no se cubrió todo el importe de la nota con los puntos, lo demás se deberá pagar con cualquier otro método de pago.

![img](8.png)

Aún cuando los puntos usados en la venta se reflejen como un descuento, en el estado de cuenta de puntos se descontarán los puntos usados:

![img](9.jpg)

### Devolver puntos

Si el sistema está configurado para devolver puntos, al hacer la devolución de una factura-nota con puntos, se hará la devolución de puntos ganados/usados en relación al porcentaje al que equivale la devolución respecto al total de la venta:

![img](10.jpg)

En caso de cancelar la devolución, los puntos devueltos también se cancelan.

![img](10.1.png)

Así mismo, la columna de puntos usados en el estado de cuenta de puntos se muestra en rojo para que sean más identificados:

![img](11.jpg)

### Facturar nota

Al facturar una nota que tuvo aplicación de puntos, la información se muestra con el correspondiente descuento aplicado:

![img](12.jpg)

![img](13.jpg)
