+++
title = "Módulo Escolar"
description = ""
weight = 7
+++

* [Catálogo de Alumnos](#catalogo-de-alumnos)
* [Definir Grados](#definir-grados)
* [Actualizar Tipo de Cambio](#actualizar-tipo-de-cambio)
* [Abrir Corte](#abrir-corte)
* [Generar Cargos por Colegiaturas](#generar-cargos-por-colegiaturas)
* [Generar Recargos por Colegiaturas](#generar-recargos-por-colegiaturas)
* [Pagos Adelantados](#pagos-adelantados)
* [Inscripciones](#inscripciones)
* [Recibir Pagos](#recibir-pagos)
* [Consulta General de Movimientos](#consulta-general-de-movimientos)
* [Consulta Individual de Movimientos](#consulta-individual-de-movimientos)
* [Cerrar Corte](#cerrar-corte)
* [Entregar Corte de Caja](#entregar-corte-de-caja)
* [Conultar Cortes](#consultar-cortes)
* [Consulta Individual de Cortes](#consulta-individual-de-cortes)
* [Estado de Cuenta](#estado-de-cuenta)

## Catalogo de alumnos

Permite registrar los datos de nuevos alumnos y modificar a los que ya están registrados.
También permite realizar una búsqueda de alumnos, enviar el catalogo a Excel y en caso de que el alumno esté inactivo, se cuenta con la opción de activarlo y viceversa.
Entre a Escolar / Catalogo de Alumnos. Se presenta la siguiente ventana:

![img](1.png)

<h4 class="text-primary">Para dar de Alta a un Alumno:</h4>

1. Haga en [Agregar]

2. El número de matrícula es proporcionado automáticamente por el sistema para evitar confusión o que se repita dicho número, pero si lo desea, usted puede cambiarlo y a partir de este último, el sistema se lo proporcionará.

3. Llene los datos que se piden del alumno

4. Haga clic en [Grabar]

![img](2.png)

<h4 class="text-primary">Para Modificar a un Alumno:</h4>

1. Colóquese sobre el alumno que desea modificar y haga clic en [Modificar]

2. Modifique los datos que sean necesarios

3. Haga clic en [Modificar]

![img](3.png)


<h4 class="text-primary">Para Activar / Inactivar a un Alumno:</h4>

Para Activar / Inactivar un Alumno:

1. Colóquese sobre el alumno que desea Activar / Inactivar y haga clic en [Modificar]

2. Haga clic en [Activar] o [InActivar] segun el caso, es decir, si el alumno esta Activo sobre el botón aparecerá la palabra Inactivar, en caso de que desee poner al alumno como Inactivo y viceversa

3. Haga clic en [Cerrar]

![img](4.png)


<h4 class="text-primary">Manejo de Colegiaturas</h4>

Se consideran 3 criterios para el manejo de la colegiatura del alumno:

1. Si el alumno tiene beca completa, capturar -1 en el campo Colegiatura

2. Si el alumno tiene media beca o el pago de su colegiatura no es completo, capturar el importe de la colegiatura que debe pagar.

3. Si el alumno paga la colegiatura completa, dejar en blanco en campo Colegiatura.

## Definir grados

Permite definir los grados que se manejan en el colegio además de asignarles el monto de la inscripción y colegiatura que se va a pagar para cada grado del alumno.

Entre a Escolar / Definir Grados. Se presenta la siguiente ventana

![img](5.png)

Para dar de alta un nuevo grado:

1. En la columna "Grado" escriba el nombre del grado que se va a dar de alta

2. Presione [Enter]

3. En la columna "Inscripción" escriba el monto de la inscripción para ese grado

4. Presione [Enter]

5. En la columna "Colegiatura" escriba el monto de la colegiatura para ese grado

6. Haga clic en cerrar

Para borrar un grado:

1. Colóquese sobre la columna "Grado" y en el renglon que desea eliminar

2. Presione [Delete], posteriormente presione [Enter]

3. Haga clic en Cerrar

Los grados que se dan de alta en esta ventana, son los mismos que aparecen en la ventana de Agregar o Modificar Alumno del Catálogo de alumnos ![img](9.png)

## Actualizar tipo de cambio

Permite actualizar el Tipo de Cambio que se va a manejar en el sistema.

Para actualizar en Tipo de Cambio:

1. Entre a Ventas / Tipo de Cambio

2. Especifique el tipo de cambio en el campo: “TC a considerar al recibir dólares en punto de venta y en caja”

3. Haga clic en [Aceptar]

![img](6.png)

## Abrir corte

Permite abrir un nuevo corte de caja para poder recibir los pagos del dia.
Para abrir un nuevo corte de caja:

1. Entre a Caja / Abrir Corte

2. Registre el fondo de caja recibido en pesos y dólares

3. Hagalic en [Abrir Corte]

![img](7.png)

## Generar cargos por colegiaturas

Permite generar los cargos correspondientes a la colegiatura mensual de los alumnos por determinado periodo.

Para generar el cargo por colegiatura mensual para todos los alumnos:

1. Entre a Escolar / Generar Cargos

2. Seleccione el mes y el año para generar el cargo

3. Haga clic en [Generar]

<h4 class="text-danger">Cabe señalar que no debe haber alumnos sin grado y grados sin el importe en la colegiatura. Para generar el cargo se toma en cuenta los 3 criterios según sea el caso.</h4>

![img](8.png)

Cargo agregado en el estado de cuenta

![img](cargo-agregado.png)

## Generar recargos por colegiaturas

Para generar recargos por atrasos en las colegiaturas primeramente deberá definir a partir de cuántos días aplicará el recargo y de qué porcentaje será.

Para denifir este dato diríjase a Escolar / Configurar Recargos por Colegiaturas

![img](recargos.png)

Después para aplicar los recargos deberá ir al menú Escolar / Generar Recargos por Colegiaturas

![img](recargos-por-aplicar.png)

Recargo aplicado al estado de cuenta

![img](recargosaplicado.png)


## Pagos Adelantados

Permite efectuar pagos adelantados a las colegiaturas de los alumnos por la cantidad de meses deseados, y como mínimo se le solicita que debe de pagar un mes por adelantado.

Para generar pagos adelantados a un alumno:

1. Entre a Escolar / Pagos Adelantados

2. Proporcione el # del Alumno que va a generar pagos adelantados y presione [Entrar]
3. Seleccione el mes y año inicial del pago

4. Especifique la cantidad de meses a pagar por adelantado

5. Le muestra el total a pagar. Puede haber descuentos dependiendo la cantidad de meses a pagar

6. Haga clic en [Pagar]

![img](10.png)

7. Se muestra la ventana de “Pagar Créditos”

8. Seleccione la forma de Pago

9. Especifique el total de pagar

10. Seleccione la divisa del pago

11. De ser necesario, especifique la cantidad del pago en cada cargo

12. Haga clic en [F8 Pagar]

13. Imprima el recibo de pago donde se incluye el nombre del alumno, la fecha, el número de documento,el total a pagar y los meses que se pagaron por adelantado.

![img](11.png)

<h4 class="text-danger">Para poder registrar un pago por adelantado es necesario que exista un corte abierto. Si generó el cargo por colegiatura para un alumno en esta opción, aquí mismo se debe efectuar el pago del mismo. No es necesario generar la colegiatura del alumno en cargos por colegiaturas.</h4>

Para que se imprima el recibo deberá agregar al tipo de movimiento en caja el formato llamado RECIBO

![img](12.png)

El recibo se verá de esta manera

![img](13.png)

## Inscripciones

Permite generar los cargos correspondientes al período de inscripción deseado para los alumnos.

![img](14.png)

Para generar el cargo por Inscripción para el alumno:

1. Entre a Escolar / Inscripciones

2. Proporcione la matricula del alumno que se va a inscribir

3. Especifique el periodo escolar al que se inscribe

4. Seleccione el grado al cual va a ingresar el alumno

5. El importe por inscripción aparece según el grado

6. Haga clic en [Pagar]

7. Se muestra la ventana de “Pagar Créditos”

8. Seleccione la forma de Pago

9. Especifique el total de pagar

10. Seleccione la divisa del pago

11. De ser necesario, especifique la cantidad del pago en el cargo de Inscripción

12. Haga clic en [F8 Pagar]

13. Imprima el recibo

![img](15.png)


<h4 class="text-danger">Para poder registrar un pago por adelantado es necesario que exista un corte abierto. Si generó el cargo por colegiatura para un alumno en esta opción, aquí mismo se debe efectuar el pago del mismo. No es necesario generar la colegiatura del alumno en cargos por colegiaturas.</h4>

## Recibir pagos

Permite efectuar pagos a los cargos que tiene el alumno, en donde podrá observar una pequeña consulta sobre los movimientos del alumno, y la forma en que será distribuido el pago. En esta ventana es en donde se van a pagar las deudas que tiene el alumno, así como las colegiaturas que se generaron en la ventana de Generar Cargos por colegiaturas.

![img](16.png)

Para registrar un pago:

1. Entre a Caja / Recibir Pagos (Créditos)

2. Especifique el numero de matricula del alumno

3. Se muestran todos los cargos pendientes de pago y el saldo en pesos y dólares

4. Seleccione la forma de Pago

5. Especifique el total de pagar

6. Seleccione la divisa del pago

7. De ser necesario, especifique la cantidad del pago en cada cargo

8. Haga clic en [F8 Pagar]

9. Imprima el recibo

## Consulta general de movimientos

Permite hacer consultas generales de los movimientos hechos en el corte activo.

Para realizar consultas generales de movimientos:

1. Entre a Caja / Consulta General de Movimientos

2. Limite la consulta según sus necesidades

3. Haga clic en Consultar

Se puede hacer la consulta por:

- Rango de fechas
- Rango de folio de los movimientos
- Rango de Cortes
- Tipo de Movimiento
- Cliente (Alumno) o Beneficiario

Después de hacer la consulta puede presionar la tecla [Enter] para consultar individualmente el movimiento.

También se puede enviar la consulta a Excel.

![img](17.png)

## Consulta individual de movimientos

Permite consultar de manera individual los movimientos hechos en el corte.

Para consultar individualmente un movimiento:

1. Entre a Caja / Consulta Individual de Movimientos

2. Escriba el folio de movimiento a consultar

3. Se muestra la información del movimiento

Desde esta ventana se puede:

- Cancelar el movimiento
- Imprimir el movimiento

Por ejemplo si queremos consultas un recibo en específico, podemos localizar el folio en la parte superior y ese es el folio que ingresamos en la consulta individual

![img](18.png)


## Cerrar corte

Permite cerrar el corte de caja activo. Se debe hacer cuando ya no se vaya a registrar ningún movimiento en el corte, ya que de lo contrario no se podrá hacer ninguna modificación después de haberlo cerrado.

Para cerrar el corte:

1. Primeramente diríjase a Caja / Otras Salidas de Efectivo y seleccinamos el Tipo [FONDO ENTREGADO AL SIGUIENTE CAJERO] e ingresamos la misma cantidad con la que abrimos corte

![img](19.png)

2. Posteriormente entrará a Caja / Cerrar Corte

3. Haga clic en [Cerrar Corte]

## Entregar corte de caja

Permite registrar la cantidad de efectivo, cheques, etc que tiene el usuario al final de su corte.

Para entregar el corte de Caja:

1. En el menú ir a Caja / Entregar Corte

2. Escriba el numero de corte

3. Se muestra la fecha y hora de inicio y cierre del corte asi como el usuario inicial y final

4. Especifique la cantidad de dinero en cada forma de pago que haya recibido

5. Haga clic en [Registrar]

6. Imprima el Corte de Caja

![img](20.png)

## Consultar cortes

Permite hacer consultas generales de los cortes hechos.

Para realizar consultas generales de cortes:

1. Entre a Caja \ Consultar Cortes

2. Limite la consulta según sus necesidades

3. Haga clic en Consultar

Se puede hacer la consulta por:

- Rango de fechas

- Rango de Cortes

- Usuarios que abrieron y cerraron el corte

- Numero de Caja donde se hizo el corte

Después de hacer la consulta puede presionar la tecla [Enter] para consultar individualmente el corte.

También se puede enviar la consulta a Excel.

![img](21.png)

## Consulta individual de cortes

Permite consultar de manera individual el corte.

Para consultar individualmente un corte:

1. Entre a Caja / Consulta Individual de Cortes

2. Escriba el numero de corte a consultar

3. Se muestra la información del corte

Desde esta ventana se puede:

- Modificar el corte

- Imprimir el corte de caja

![img](22.png)

## Estado de Cuenta

Permite consultar el estado de cuenta de los alumnos, en donde podrá ver todos los cargos y abonos que se le han hecho. Los movimientos en rojo son cargos pendientes de pago.

Para consultar el Estado de Cuenta de un Alumno:

1. Entre a Cobranza / Estado de Cuenta

2. Esciba el numero de matricula del alumno

3. Se muestran los cargos y abonos hechos asi como el saldo

4. Puede ordenar los datos por Fecha o por Factura (# de Documento)

5. Tambien se puede enviar la consulta a Excel

![img](23.png)

