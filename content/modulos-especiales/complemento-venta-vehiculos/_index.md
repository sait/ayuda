+++
title = "Complemento Venta de Vehículos"
description = ""
weight = 12
+++

Si eres fabricante, ensamblador o distribuidor autorizado de automóviles nuevos, o importas automóviles para permanecer de manera definitiva en la franja fronteriza norte del país y en los estados de Baja California, Baja California Sur y la región parcial del estado de Sonora, este complemento te permite incorporar a la factura la clave vehicular que corresponda a la versión de la venta y el número de identificación vehicular que corresponda al automóvil vendido.

Esta opción está disponible a partir de la versión 2024.6, a continuación las instrucciones:

1.Primeramente deberá definir otros datos en Utilerías / Configuración General del Sistema / Pestaña Ventas / Deberá habilitar la opción de * Capturar otros datos en documentos y después dar clic en [Otros Datos]

![IMG](1.png)

2.En la sección de Factura, agregaremos los siguientes datos

Los últimos 3 datos solo se deben capturar en caso que el vehículo haya sido importado, si no es así, solo los primeros 2

![IMG](2.png)

3.Al realizar la factura en Ventas / Registrar Ventas notaremos ya se encuentran los campos disponibles para su captura

![IMG](3.png)

4.Realizamos la factura de manera normal y capturamos los datos relacionados al complemento

**Importante, el complemento solo se agregará a la primer partida capturada en la factura**

![IMG](4.png)

5.La factura y el XML saldrían de esta forma 

![IMG](5.png)

![IMG](6.png)

6.Para agregar dichos campos al formato de factura tendrá que definir las siguientes expresiones

```
ClaveVehicular = 'Clave Vehicular: '+ValProp(Docum.OTROSDATOS,'ClaveVehicular')
Serie          = 'Serie: '+ValProp(Docum.OTROSDATOS,'Serie')
```

![IMG](7.png)


Agregar en el Print When las siguientes expresiones para que solo se impriman estas etiquetas cuando estén capturados estos datos relacionados al complemento:

```
not empty(ValProp(Docum.OTROSDATOS,'ClaveVehicular'))
not empty(ValProp(Docum.OTROSDATOS,'Serie'))
```

![IMG](8.png)

