+++
title = "Módulo de Rentas"
description = ""
weight = 8
+++

El módulo de rentas permite registrar la renta de equipos y tener el control sobre quien lo tiene rentado, su ubicación y la fecha de entrega así como poder obtener fácilmente la cantidad de equipos disponibles, también está preparado para la facturación de la renta en caso de ser necesario.

**Consideraciones Especiales**

- Los clientes y artículos se dan de alta previamente.

- Para un mejor control, se pueden manejar números de serie para identificar a cada artículo

- Al realizar una renta se genera la salida al inventario de los artículos

- Al entregar una renta se regresan al inventario los artículos 

<h2 class="text-primary">TEMARIO</h2>

* [1) Definir movimientos de salida y entrada por renta](#definir-movimientos-de-salida-y-entrada-por-renta)
* [2) Siguientes Folios](#siguientes-folios)
* [3) Catálogo de Rentas](#catalogo-de-rentas)
	* [3.1) Agregar Renta](#agregar-renta)
	* [3.2) Cancelar Renta](#cancelar-renta)
	* [3.3) Eliminar Renta](#eliminar-renta)
* [4) Entregar Renta](#entregar-renta)
* [5) Facturar Renta](#facturar-renta)
* [6) Consultas Generales](#consultas-generales)
* [7) Reporte](#reportes)


## DEFINIR MOVIMIENTOS DE SALIDA Y ENTRADA POR RENTA

Es necesario registrar 2 tipos de movimiento al inventario con el cual se va a registrar la salida cuando se renta el equipo o la entrada cuando se entrega la renta.

Entre al menú de Inventario / Tipos de Movimientos / Agregar

![img](1.jpg)

1) **CONCEPTO SALIDA POR RENTA**

* Clave: SR
* Concepto: SALIDA POR RENTA
* Movimiento: SALIDA
* Capturar: CLIENTE
* Sig. Folio: Dejarlo en blanco
* Grupos que tendrán acceso: Dar de alta los niveles que tendrán acceso. Ejemplo: SUPER,CONT,ALM,GCOMP
* Concepto a usar para cancelaciones: S1
* Haga clic en [Grabar] 

2) **CONCEPTO CANC SALIDA POR RENTA**

* Clave: S1
* Concepto: CANC SALIDA POR RENTA
* Movimiento: ENTRADA
* Capturar: CLIENTE
* Sig. Folio: Dejarlo en blanco
* Grupos que tendrán acceso: Dar de alta los niveles que tendrán acceso. Ejemplo: SUPER,CONT,ALM,GCOMP
* Concepto a usar para cancelaciones: Dejarlo en blanco
* Haga clic en [Grabar] 


3) **CONCEPTO ENTRADA POR RENTA**

* Clave: ER
* Concepto: ENTRADA POR RENTA
* Movimiento: ENTRADA
* Capturar: CLIENTE
* Sig. Folio: Dejarlo en blanco
* Grupos que tendrán acceso: Dar de alta los niveles que tendrán acceso. Ejemplo: SUPER,CONT,ALM,GCOMP
* Concepto a usar para cancelaciones: E1
* Haga clic en [Grabar] 

4) **CONCEPTO CANC ENTRADA POR RENTA**

* Clave: E1
* Concepto: CANC ENTRADA POR RENTA
* Movimiento: SALIDA
* Capturar: CLIENTE
* Sig. Folio: Dejarlo en blanco
* Grupos que tendrán acceso: Dar de alta los niveles que tendrán acceso. Ejemplo: SUPER,CONT,ALM,GCOMP
* Concepto a usar para cancelaciones: Dejarlo en blanco
* Haga clic en [Grabar] 


## SIGUIENTES FOLIOS

Permite definir el siguiente folio para agregar una renta.

Entre al menú de Rentas / Siguientes Folios.

Especifique el siguiente folio.

En caso de que la empresa maneje enlace de sucursales, se recomienda anteponer la letra correspondiente a los folios de la sucursal.

Haga clic en [Grabar] para grabar la información. 

![img](2.jpg)

## CATALOGO DE RENTAS

Permite agregar una nueva renta, cancelar una ya existente o eliminarla en caso de haberse equivocado en la captura.

Entre al menú de Rentas / Catálogo de Rentas 

![img](3.jpg)

## AGREGAR RENTA

1.Haga clic en [Agregar]
    
2.El número de renta se toma de los siguientes folios. Puede ser cambiada en caso de ser necesario.

3.Capture la fecha de inicio de la renta.

4.Especifique los días a rentar para obtener el vencimiento.

5.Capture la clave del cliente al que se registra la renta, el cual previamente se debió haber registrado en el catalogo de clientes. Puede hacer clic en [?] o presionar la tecla [F2] para realizar búsquedas.

6.Especifique el lugar de la obra. En ocasiones el domicilio donde se va a llevar el equipo es distinto al del cliente.

7.Seleccione el tipo de comprobante del domicilio.

8.Capture observaciones en caso de ser necesario.

9.Por cada artículo rentado:

10.Capture la clave. Se pueden realizar las búsquedas al presionar la tecla [F2]

11.Escriba la cantidad a rentar. Presione la tecla [F3] para seleccionar los números de serie que se van a rentar en caso de que el artículo maneje series.

12.Capture el precio final de la renta por todos los artículos. 

13.Haga clic en [Agregar] para guardar la renta

![img](4.jpg)

14.Finamente de clic en [Imprimir Contrato] para imprimir el contrato de renta. 

![img](5.jpg)

El cual se verá así, este formato es editable.

![img](6.jpg)


## CANCELAR RENTA

1.Seleccione de la lista de rentas la que se desea cancelar.

![img](cancelar.jpg)

2.Haga clic en [Cancelar], se mostrará la información relacionada a la renta. Verifique que se trate de la renta correcta, si es la renta correcta, de clic en [Cancelar]

![img](cancelar1.jpg)

3.Confirme que se desea cancelarla

![img](cancelar2.jpg)

4.Listo, el registro ahora aparece cancelado.

![img](cancelar3.jpg)


## ELIMINAR RENTA

1.Seleccione de la lista de rentas la que se desea eliminar.

![img](eliminar1.jpg)

2.De clic en [Eliminar]. Se mostrará la información relacionada a la renta. Verifique que se trate de la renta correcta, si es la renta correcta, de clic en [Eliminar]

![img](eliminar2.jpg)

3.Confirme que se desea eliminarla. 

![img](eliminar3.jpg)

![img](eliminar4.jpg)

4.Listo, el registro ya no aparecerá.

![img](eliminar5.jpg)

**Nota: La opción de "Eliminar Renta" solo esta disponible si la empresa NO maneja enlace de sucursales.**

## ENTREGAR RENTA

Esta opción permite entregar la renta del equipo para regresarlo al inventario y que ya no siga apareciendo en las rentas pendientes por entregar.

Entre al menú de Rentas / Entregar Renta

1.Capture el número de renta

2.Se muestra su información. Verifique que se trate de la renta correcta.

3.Hacer clic en [Entregar]

4.Confirme que se desea entregarla y listo, se dará entrada al inventario.

![img](entregar1.jpg)

## FACTURAR RENTA

Permite generar la factura de contado o crédito de una renta que haya sido entregada previamente.

Entre al menú de Rentas / Facturar Renta

1.Capture el número de renta y verifique que se trate de la renta correcta, de clic en [Facturar] 

![img](facturar1.jpg)

2.De clic en [Si] 

![img](facturar2.jpg)

3.Puede indicar si la factura será a crédito al habilitar la casilla correspondiente.

![img](facturar-credito.jpg)

4.Haga clic en [Facturar]

![img](facturar3.jpg)

5.Llene los datos relacionados al CFDI y [Entregar], de clic en [F8 = Procesar] y listo.

![img](facturar4.jpg)

## CONSULTAS GENERALES

Permite obtener una consulta general de las rentas registradas en el sistema que cumplan con las restricciones.

Entre al menú de Rentas / Consultas Generales

Es esta sección puede hacer consultar personalizadas con los filtros disponibles como lo son: clave de artículo, rango de fechas y rango de vencimiento, estatus de renta (abiertas, canceladas, entregadas, todas).

Nota: La consulta puede ser enviada a Excel. 

![img](consultas-generales.jpg)

## REPORTES


Permite consultar reportes relacionados con los movimientos de rentas.

Entre al menú de Rentas / Reportes

El sistema cuenta con 2 reportes, por resumen y detallado

Nota: Las consultas puede ser enviada a Excel. 

![img](resumen.jpg)



   

