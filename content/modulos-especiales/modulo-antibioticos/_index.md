+++
title = "Módulo para Control de Antibióticos"
description = ""
weight = 5
+++

Este módulo se desarrollo para empresas del giro farmacéutico, ya que a partir del 2010 se requiere receta médica para poder adquirir antibióticos.

El proceso desarrollado permite capturar determinada información al vender artículos que requieren receta médica para su venta.

Este módulo funciona para la venta en el menú de Caja / Registrar Ventas y también para Ventas / Registrar Ventas

Si usted utiliza el Módulo de antibióticos junto con el Módulo del Círculo de la Salud deberá de usar la función de Ventas / Registrar Ventas F4 ya que le permite llevar el control de caducidades y lotes además de que le permite elegir el lote más próximo a vencer.

## 1. Identificar los artículos que requieren receta médica para su venta

Se van a identificar los artículos que requieren receta médica para su venta mediante un campo especial dentro del catálogo de artículo en el menú de Inventario.

1.Entre al menú de Inventario / Catalogo de Artículos.

2.Capture la clave del artículo antibiótico

3.Seleccione la pestaña de “Otros Datos” 


![img](articulo.JPG)


4.En el campo “Requiere Receta” seleccione con la barra espaciadora SI o NO según sea el caso

5.Grabe la modificación. 

En el campo “Fórmula” se va a especificar una misma clave para los artículos que manejen el mismo componente activo (fórmula) pero tengan diferente clave debido al laboratorio que las fabrica. 


## 2. Catálogo de Doctores

Permite agregar, modificar o eliminar un doctor para tener almacenada la cédula profesional del doctor que expide la receta del antibiótico.

![img](agregar.JPG)

<h3 class="text-primary">Agregar doctor</h3>

Permite agregar un nuevo doctor

A) Entre al menú de Ventas \ Catálogo de Doctores

B) Haga clic en **[Agregar]**

C) La información a capturar es:
        
- Cédula Profesional la cual debe confirmarse 2 veces para asegurar que este correctamente capturada.
- Nombre del doctor
- Domicilio
- Teléfono del consultorio o celular 

D) Haga clic en **[Agregar]** para grabar la información 

![img](nuevodoctor.JPG)

<h3 class="text-primary">Modificar doctor</h3>

Permite modificar la información de un doctor previamente capturado

A) Entre al menú de Ventas \ Catálogo de Doctores

B) Seleccione el registro del doctor cuyos datos se van a modificar

C) Haga clic en **[Modificar]**

D) Se muestra la información capturada

E) Haga los cambios que sean necesarios

F) Haga clic en **[Modificar]** para actualizar la información 

![img](modificar.JPG)

<h3 class="text-primary">Eliminar doctor</h3>

Permite eliminar la información de un doctor previamente capturado

A) Entre al menú de Ventas \ Catálogo de Doctores

B) Seleccione el registro del doctor cuyos datos se van a eliminar

C) Haga clic en **[Eliminar]**

D) Se muestra la información capturada

E) Verifique que se trate del doctor correcto

F) Haga clic en **[Eliminar]** para borrar del catalogo al doctor 

![img](eliminar.JPG)

## 3. Venta de Antibióticos

En la ventana de Registro de Ventas el usuario va a capturar los artículos que se van a vender.

A) Entre al menú de Caja / Registrar Ventas

B) Capture los artículos a vender 

C) Antes de procesar la venta, en caso de que alguno de los artículos requiera receta médica, se muestra el siguiente mensaje: 

![img](requierereceta.JPG)

E) Posteriormente se muestra la ventana para la captura de la información de la receta requerida para su venta: 

Capture los siguientes datos:

* Cédula Profesional

* Num. Receta

* Cantidad Recetada 

Si el doctor no esta agregado al catálogo de doctores, automáticamente se muestra la ventana para agregar un nuevo doctor. 

Especifique el número de receta y la cantidad recetada para cada medicamento. El sistema muestra la cantidad pendiente de surtir para ese medicamento en futuras ventas:

![img](articulos-especiales.JPG) 

Para finalizar la captura, presione la tecla [F8] para regresar a la ventana de “Registro de Ventas”.

La venta esta lista para ser procesada. 

![img](listo.JPG) 

**NOTA: Después de capturar la información de la receta, no se podrán agregar o eliminar artículos a la venta**

La próxima vez que el cliente acuda a surtir de nuevo la receta, el sistema llevará el control de la cantidad que tiene pendiente de surtir en base a la última venta de la receta. 