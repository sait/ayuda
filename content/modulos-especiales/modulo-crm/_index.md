+++
title="Módulo CRM"
description=""
weight = 2
+++

**El módulo de CRM (Client Relationship Management)**,  es un módulo que le permite manejar la relaciones con sus clientes, llevar el historial de llamadas y servicios realizados así como generar ordenes para realizar servicios que los clientes solicitan.

{{%alert success%}} AVISO: El módulo lo deberá de solicitar a su ejecutivo de ventas SAIT o Distribuidor autorizado más cercano. {{%/alert%}}


* [Configuración de Catálogos Iniciales](#configurar-catálogos-iniciales)
* [Registrar Actividades](#registrar-actividades)
* [Agregar una Orden de Servicio](#agregar-orden-de-servicio)
* [Registrar Servicios a una Orden](#registrar-servicios-a-una-orden)
* [Seguimiento a Actividades](#seguimiento-a-actividades)
* [Consulta Individual de Ordenes](#consulta-individual-de-orden-de-servicio)
* [Consulta General de Ordenes](#consulta-general-de-ordenes)
* [Reportes del Módulo](#reportes)
* [Reportes de Ordenes](#reportes-de-ordenes)
* [Módulo de OPORTUNIDADES](#módulo-de-oportunidades)

### Configurar Catálogos Iniciales

Antes de iniciar con el uso del Módulo de CRM, usted deberá capturar los catálogos iniciales de: Categorías, Contactos y Proyectos.

#### Categorías

Permite configurar las categorías para clasificar a las actividades que se van a registrar.

![IMG](imagen1.PNG)

**Agregar Categorías.**

* En el menú ir a: CRM \ Configurar Categorías
* Especificar el nombre de la(s) actividades que se van a registrar
* Hacer clic en **[Grabar]**.

**Modificar Categorías.**

* En el menú ir a: CRM \ Configurar Categorías
* Seleccionar el nombre de la actividad que se va a modificar
* Hacer la modificación
* Hacer clic en **[Grabar]**.

**Eliminar Categorías.**

* En el menú ir a: CRM \ Configurar Categorías
* Seleccionar el nombre de la actividad que se va a eliminar
* Hacer clic en **[Eliminar]**
* ¿Seguro que desea eliminar la categoría? Hacer clic en **[Si]**.

#### Contactos

Permite Agregar, Modificar y Eliminar contactos de los clientes. Con opción de exportar a Excel en caso de ser necesario.

![IMG](imagen2.PNG)

**Agregar Contacto.**

* En el menú ir a: CRM \ Catálogo de Contactos.
* Hacer clic en **[Agregar]**.
* El sistema le proporciona la clave del contacto. Es un numero consecutivo y se incrementa a partir del ultimo contacto que se agregó.
* Especificar el nombre y los demás datos del contacto.
* Hacer clic en **[Agregar]**.

**Modificar Contacto.**

* En el menú ir a: CRM \ Catálogo de Contactos.
* Seleccionar el contacto posicionandose sobre el registro deseado.
* Hacer clic en **[Modificar]**.
* Hacer las modificaciones necesarias.
* Hacer clic en **[Modificar]**.

**Eliminar Contacto.**

* En el menú ir a: CRM \ Catálogo de Contactos.
* Seleccionar el contacto posicionandose sobre el registro deseado.
* Hacer clic en **[Eliminar]**.
* Verificar que sea el contacto correcto.
* Hacer clic en **[Eliminar]**.
* ¿Seguro que desea borrar el contacto? Hacer clic en **[Si]**.

#### Proyectos

Permite registrar proyectos al sistema de los cuales pueden surgir varias órdenes y servicios.

![IMG](imagen3.PNG)

**Agregar Proyecto.**

* En el menú ir a: CRM \ Catálogo de Proyectos. Hacer clic en [Agregar].
* El sistema le proporciona la clave del proyecto. Es un numero consecutivo y se incrementa a partir del ultimo proyecto que se agregó.
* Especificar la clave del cliente al que pertenece el proyecto.
* Especificar el nombre del proyecto.
* Especificar la fecha de inicio y meta del proyecto.
* Seleccionar el usuario encargado del proyecto.
* Especificar el contacto de la empresa encargado del proyecto.
* Hacer clic en **[F8 = Procesar]**.


** Modificar Proyecto.**

* En el menú ir a: CRM \ Catálogo de Proyectos.
* Seleccionar el proyecto deseado, posicionandose sobre el registro. Hacer clic en **[Modificar]**.
* Hacer las modificaciones necesarias.
* Se pueden consultar las Pendientes y Completas generadas a partir del proyecto.
* Al hacer doble clic o presionar la tecla [Enter] en el # de orden se muestra la ventana para modificarla.
* Hacer clic en **[F8 = Procesar]**.


**Eliminar Proyecto.**

* En el menú ir a: CRM \ Catálogo de Proyectos.
* Seleccionar el proyecto deseado, posicionandose sobre el registro. Hacer clic en **[Eliminar]**.
* Hacer clic en **[F8 = Procesar]**. Aparecerá el mensaje: ¿Seguro que desea borrar el proyecto? Hacer clic en **[Si]**.
* Listo!!! Con eso se elimina la información del Proyecto.

{{%alert success%}} NOTA: Un proyecto se completa cuando todas sus órdenes se hayan cerrado y no haya actividades pendientes por realizar. {{%/alert%}}


### Registrar Actividades

Permite agregar actividades (servicios) realizadas a los clientes. Si después de realizar la actividad surge un nuevo servicio, se pueden agregar tareas u ordenes a partir de ésta.

![IMG](imagen5.PNG)

**Agregar Actividad**

* En el menú ir a: CRM \ Agregar Actividad o presionar las teclas CTRL+D.
* Especificar los datos del contacto al que se le dio un servicio.
* Seleccionar el tipo de actividad.
* Especificar el detalle de la actividad realizada.
* Agregar una Tarea u Orden de servicio en caso de quedar algo pendiente de esa actividad.
* Hacer clic en **[F8 = Procesar]** o presionar la tecla F8.

**Modificar Actividad**

* En el menú ir a: CRM \ Consultar Actividad.
* Especificar los rangos de las actividades a consultar.
* Hacer clic en **[Consultar]**.
* La consulta se puede enviar a Excel.

**Eliminar Actividad**

* En el menú ir a: CRM \ Consultar Actividad.
* Después de haber consultado las actividades, seleccionar la que se desea modificar.
* Hacer clic en [Modificar] o hacer doble clic o presionar la techa [Enter].
* Modificar la información necesaria.
* Hacer clic en **[F8 = Procesar]** o presionar la tecla F8.


### Agregar Orden de Servicio

Este proceso permite asignar una orden directa a algun usuario de la empresa.

![IMG](imagen7.PNG)

Los pasos para agregarla son los siguientes:

* En el menú ir a: CRM \ Agregar Orden de Servicio o presionar las teclas CTRL+S
* Capturar la clave del proyecto al que pertenece la orden (Opcional).
* Especificar los datos del contacto al que se le va a programar la orden.
* Indicar la prioridad de la orden.
* Seleccionar el usuario al que se le va a asignar la orden.
* Especificar la fecha y hora de la cita.
* Seleccionar el tipo de actividad.
* Especificar el detalle de la orden.
* Especificar el equipo recibido (Opcional).
* Hacer clic en **[F8 = Procesar]** o presionar la tecla F8.


### Registrar Servicios a una Orden

![IMG](imagen4.PNG)

* En el menú ir a: CRM \ Agregar Servicio o presionar las teclas **CTRL+R**.
* Escribir el número de orden.
* Registrar la actividad realizada.
* Si la orden ya se termino activar la casilla [x] Servicio Terminado.
* Hacer clic en **[F8 = Procesar]** o presionar la tecla F8.

### Seguimiento a actividades

Permite llevar el control de las actividades realizadas por el usuario para saber que seguimiento se le dio.

![IMG](imagen6.PNG)

* En el menú ir a: CRM \ Seguimiento de Actividades
* Se muestran la fecha, hora, contacto, empresa y el status de las actividades del usuario a las que no se le ha dado seguimiento.
* En caso de querer cargar actividades de otro usuario o actividades terminadas:
* Hacer clic en **[Cargar Actividades]**.
* Seleccionar el usuario.
* Especificar el cliente.
* Especificar el contacto.
* Seleccionar el rango de fechas de las actividades a mostrar.
* Seleccionar el status.
* Hacer clic en [Cargar].
* Se puede crear una nueva actividad al hacer clic en **[Nueva actividad]**.
* Por cada actividad cargada se muestra la información del contacto, la descripción y el seguimiento que se le dio.
* En caso de que la actividad este terminada hacer clic en **[Ok]**.
* En caso de que una actividad terminada se desee poner pendiente, hacer clic en **[Pend]**.
* Se puede copiar la información de la actividad y pegarla en alguna otra aplicación.

### Consulta Individual de Orden de Servicio

Permite consultar de manera directa una orden de servicio en específica.

![IMG](imagen8.PNG)

* En el menú ir a: CRM \ Consulta Individual de Ordenes.
* Escribir el número de orden.
* Se muestra la información para hacer las correspondientes modificaciones.

### Consulta General de Ordenes

Permite consultar de manera genealizada a traves de ciertos filtros, ubicar más facilmente un rango de ordenes de servicios.

![IMG](imagen9.PNG)

* En el menú ir a: CRM \ Consulta General de Ordenes.
* Especificar el status de las ordenes
* Seleccionar el usuario.
* Especificar el cliente.
* Escribir el contacto. 
* Seleccionar el proyecto.
* Especificar el rango de fechas en que fue creada y terminada.
* Hacer clic en [Consultar].
* Se muestra el resultado de la consulta.
* En caso de querer consultar individualmente una orden se debe seleccionar y hacer clic en **[Modificar]**.


**Consulta en base a Conceptos.**

Permite consultar órdenes de servicios en base a la información capturada en la descripción del servicio a realizar o en las actividades de la orden.

* En el menú ir a: CRM \ Consulta General de Ordenes.
* Escribir alguna palabra o frase contenida en la orden.
* Hacer clic en **[Buscar]**.

**Verificar una orden terminada.**

* En el menú ir a: CRM \ Consulta General de Ordenes.
* Después de haber consultado las ordenes, seleccionar a la que se desea verificar. Hacer clic en [Verificar].
* Registrar los comentarios del cliente.
* Si la orden ya se cerró activar la casilla [x] Servicio Terminado.
* Especificar la calificación que le da el cliente al servicio brindado.
* Si no se cerró la orden se volverá a reabrir.
* Hacer clic en **[F8 = Procesar]** o presionar la tecla F8.

**Verificar una orden terminada.**

* En el menú ir a: CRM \ Consulta General de Ordenes.
* Después de haber consultado las ordenes, seleccionar a la que se desea reabrir. Hacer clic en [Reabrir].
* Registrar los comentarios del cliente.
* Si la orden ya se cerró activar la casilla [x] Servicio Terminado.
* Especificar la calificación que le da el cliente al servicio brindado.
* Si no se cerró la orden se volverá a reabrir.
* Hacer clic en **[F8 = Procesar]** o presionar la tecla F8.

{{%alert success%}}Al consultar órdenes se muestran los siguientes Status: <br>PEND: Orden pendiente de realizar. No se ha terminado. <br>OK: Orden terminada. No se ha cerrado. <br>OK2: Orden cerrada. . {{%/alert%}}


### Reportes

Permite imprimir reportes de Actividades y Tareas realizadas.

* En el menú ir a: CRM \ Reportes.
* Seleccionar el reporte que se desea consultar.
* Especificar las restricciones de la consulta.
* Hacer clic en **[Imprimir]** si desea ver el reporte en pantalla o enviarlo a la impresora.
* Hacer clic en **[Hoja]** para enviar la consulta a Excel.



### Reportes de Ordenes

Permite imprimir reportes de órdenes de servicio realizadas.

* En el menú ir a: CRM \ Reportes de Ordenes
* Seleccionar el reporte que se desea consultar.
* Especificar las restricciones de la consulta
* Hacer clic en **[Imprimir]** si desea ver el reporte en pantalla o enviarlo a la impresora.
* Hacer clic en **[Hoja]** para enviar la consulta a Excel.


### Módulo de Oportunidades

El siguiente apartado del modulo CRM presenta la gestión de las Oportunidades detectadas, en donde existe la posibilidad de concretar una venta o un servicio a un determinado cliente.

![IMG](imagen10.PNG)

#### Agregar Oportunidad

* Ir a CRM \ Agregar Oportunidad.
* Seleccionar el Contacto en el cual se detecto la Oportunidad. Al seleccionar dicho Contacto, se llenaran los datos referentes a los almacenados en el.
* Seleccionar el Tipo de Oportunidad y el Agente a quien se va asignar la Oportunidad.
* Los campos de 'Asunto', 'Observaciones' y '¿Cómo nos conoció?' son los datos informativos sobre las observaciones de la Oportunidad.
* Hacer clic en **[Crear]** para guardar.

#### Consultar Oportunidad

* Ir a CRM \ Consultar Oportunidades.
* Se nos presentara la ventana de 'Oportunidades' con los registros en Status de 'Nuevas'.

![IMG](imagen11.PNG)

* En base a los filtros de búsqueda, ingresar las características con las cuales se registro la Oportunidad.
* Clic en [Consultar].
* Se nos mostrarán los resultados de la búsqueda realizada.

#### Seguimiento a Oportunidad

* Ir a CRM \ Consultar Oportunidades.
* Se nos presentara la ventana de 'Oportunidades'.
* En base a los filtros de búsqueda, ingresar las características con las cuales se registro la Oportunidad.
* Clic en [Consultar].
* Se nos mostraran los resultados de la búsqueda realizada. Clic en el renglón de la oportunidad a dar seguimiento.
* Una vez seleccionado el registro, clic en [Seguimiento], o bien, dar doble clic sobre el registro.
* Se nos presenta la ventana 'Seguimiento de Oportunidades', en donde registraremos el desarrollo de las Oportunidades creadas.
* Seleccionar uno de las siguientes procesos por registrar en la Oportunidad.

{{%alert success%}}ESPERAR{{%/alert%}}

* Para mantener la Oportunidad aun por iniciar, dar clic en [Esperar].
* En el campo <Seguimiento Realizado> ingresar los comentarios referentes al estado de la Oportunidad.
* Se nos abrirá el campo de <Volver a Llamar>, ingresar la fecha en que se le volverá a comunicar al contacto.
* Clic en [Guardar].

{{%alert success%}}TRABAJANDO{{%/alert%}}

* Una vez iniciado el desarrollo y constante seguimiento a la Oportunidad, clic en **[Trabajando]**.
* Previamente se tuvo que haber ingresado el Seguimiento y los Comentarios del prospecto.

{{%alert success%}}CERRADA{{%/alert%}}

* En dado caso que se haya logrado el objetivo de la Oportunidad, podemos dar por cerrada la Oportunidad.
* Clic en [Cerrada].
* Se nos abrirá el campo de <Factura No.>, ingresar el folio de la factura en la cual se realizo la venta lograda.
* Clic en [Guardar].


{{%alert success%}}PERDIDA{{%/alert%}}

* Sí la Oportunidad no se llego a concretar y por tal motivo no se le dará mas seguimiento, clic en [Perdida].
* Se nos abrirán los campos **<¿Por qué perdidos>** y **<¿Qué podemos hacer para mejorar?>**, ingresar las respuestas a estas preguntas.
* Clic en [Guardar].


#### Historial de Oportunidades

* Ir a: CRM \ Consultar Oportunidades.
* Se nos presentara la ventana de 'Oportunidades'.
* En base a los filtros de búsqueda, ingresar las características con las cuales se registro la Oportunidad.
* Clic en **[Consultar]**.
* Se nos mostraran los resultados de la búsqueda realizada. Clic en el renglón de la oportunidad a dar seguimiento.
* Una vez seleccionado el registro, clic en **[Seguimiento]**, o bien, dar doble clic sobre el registro.
* Se nos presenta la ventana 'Seguimiento de Oportunidades', clic en **[Historial]**.
* Se nos mostrará la ventana 'Historial de Contacto'.
* En esta ventana podremos visualizar los movimientos registrados en este oportunidad, así como copiarlos a clipboard, mandar a excel o mandar a imprimir.

































