+++
title = "Envio manual de estado de cuenta"
description = ""
weight = 16
+++

El siguiente módulo especial permite enviar manualmente en el momento que usted desee, el estado de cuenta de las facturas pendientes a sus clientes 

<h3 class="text-primary"> Consideraciones técnicas </h3>

- Al instalar el módulo deberá asegurarse nadie esté usando el sistema ya que se crearán campos adicionales en las tablas

- El correo se enviará por medio del usuario que esté logueado en sistema, por lo que deberá asegurarse que el correo que tiene el usuario en  Utilerías / Catálogo de usuarios, esté funcionando de manera correcta

<h3 class="text-primary"> Configuración </h3>

- Asegúrese de que en el catálogo de clientes se encuentre capturado al menos un correo en la pestaña de Crédito 

![IMG](1.png)

- Para enviar el estado de cuenta deberá ir a Cobranza / Enviar Estado de Cuenta

![IMG](2.png)

- Aparecerá la siguiete ventana y deberá llenar los datos

	- **Cliente Inicial y Cliente Final:** Si dejamos la consulta como aparece, con el primer campo vacío y el segundo lleno, el estado de cuenta se enviará a todo nuestro catálogo de clientes, si ponemos la misma clave de cliente en ambos campos, solo se enviará a ese cliente en específico

	- **Clasificación y Zona:** esta parte es opcional y se puede utilizar si solo desea enviar el correo con el estado de cuenta a ciertos clientes, para ver la documentación de clasificaciones de los clientes de clic [aquí](/sait-erp/10-capacitacion-modulo-de-ventas/a-catalogo-clientes/#definir-zonas-y-clasificacion) 

![IMG](3.png)

El correo llegará de la siguiente manera:

![IMG](4.png)

El reporte llegará de la siguiente manera:

![IMG](5.png)