+++
title = "Complemento INE"
description = ""
weight = 10
+++

Complemento para facturar a partidos políticos y candidatos con complemento INE SAIT

En SAIT es posible facturar a partidos políticos y candidatos con adenda INE (Instituto Nacional Electoral), cumpliendo con todos los requerimientos que establece el SAT.

### PASO 1: Activar complemento INE

1.Instalar el formato de addenda para cada cliente al que se ocupe facturar con el complemento desde Utilerías / Modificar Formatos / CFD Comprobantes Fiscales Digitales con Addendas

Dará clic en Agregar y llenará los campos solicitados, al finalizar dará clic en **Agregar**

![img](0.JPG)

1.Ir al módulo de Ventas / Clientes .

![img](1.png)

2.Buscar al cliente que se generará factura con complemento INE.

3.Seleccionar la pestaña "Otros Datos".

4.Se observa un renglón que dice "INE", deberá estar seleccionada la opción "Sí".

![img](2.png)

### PASO 2: Facturar complemento INE

1.Ir al módulo de Ventas / Registrar ventas .
 
![img](3.png)

2.Ingrese Vendedor, Tipo y Divisa.

3.Ingrese al cliente al que le facturará complemento INE.

4.Podemos observar que al seleccionar al cliente con complemento INE activado aparecerá en la ventana un botón "Complemento INE".
 
![img](4.png)

5.Dar clic en botón **"Complemento INE"**.

6.Realizar el llenado de Tipo de Proceso, Tipo de Comité, IdContabilidad, Entidad, Ámbito y por último IdContabilidad Entidad (Dependiendo del tipo de proceso se activarán únicamente casillas que serán obligatorias).

7.Clic en **F8 Continuar**.

![img](5.png)

8.Ingresar producto a facturar.

9.Clic en **Procesar F8** y seleccionar Forma de pago, Método y Uso del CFDI.

10.Clic en **F8 Continuar**.

![img](6.png)

11.Si visualiza en pantalla la factura, podrá observar en "Cadena Original del Complemento de Certificación Digital del SAT" los datos capturados correspondientes al complemento INE

![img](7.png)

12.Si consulta el XML, la información del Complemento INE le aparecerá de la siguiente manera:

![img](8.png)