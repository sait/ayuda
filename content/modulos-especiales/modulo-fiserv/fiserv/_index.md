+++
title = "Módulo FISERV"
description = ""
weight = 1
+++

En la siguiente documentación se detalla los pasos para activar el módulo FISERV con su sistema SAIT.

<h3 class="text-primary">Requerimientos</h3>

- Sistemas Operativos Actualizados Windows. Versión 10 en adelante, PRO o HOME (64 bits)
- No Windows modificados, alterados, etc
- Contar con todas las actualizaciones de Windows a la fecha
- Memoria RAM mínimo de 4GB
- 1 Puerto USB
- Contar con una versión SAIT 2022 en adelante
- Funcionalidad disponible para módulo de Caja

<h3 class="text-primary">Configuración Previa</h3>

**Agregar parámetro FISERV en el archivo CONFIG.MSL**

1.Abrir SAIT y dar clic en [Catálogo de Empresas]

![IMG](1.png)

2.Copiar ruta del Directorio (lo que aparece en azul)

![IMG](2.png)

3.Pegar la ruta copiada en el explorador

![IMG](RUTA.png)

4.Ubicar archivo Config.msl y abrilo con un bloc de notas

![IMG](3.png)

5.Agregar el siguiente parámetro

![IMG](4.png)

6.Ir a Archivo / Guardar

![IMG](5.png)

**Configurar conceptos de tarjetas en Caja / Tipos de Movimientos**

1.Seleccionamos algún concepto de tarjeta (TB o TC) y Damos clic en Modificar y notaremos, que se agregaron nuevos elementos al final de la ventana

Seleccionaremos el Proveedor de Pago, para ambiente de pruebas usaremos el Simulador

![IMG](6.png)

2.Agregar porcentaje de comisión en conceptos de tarjeta

![IMG](7.png)

3.En el concepto TC Tarjeta de Crédito puede activar la opción de Plan de Pagos y puede configurar la tabla de Mensualidades, Monto y %Comisión

![IMG](8.png)

**Agregar los artículos para cobro con tarjeta**

Deberá crear los siguientes artículos para terminar con la configuración de cobro de comisión con tarjeta, para esto deberá entrar a Inventario / Catálogo de Artículos y Servicios

![IMG](9.png)

<h3 class="text-primary">Funcionamiento</h3>

Al procesar una venta con tarjeta, aparecerá la nueva ventana en donde el usuario podrá colocar el importe a cobrar 

![IMG](13.png)

Cuando el importe a cobrar sea igual al que de la nota, si se cobra correctamente el pago en la terminal, se procesará la nota en automático

Pero si el importe a cobrar es menor al de la nota, se hará el cobro en la terminal y se regresará a la ventana de Caja Pagar para agregar más formas de pago

![IMG](14.png)

Si el montó es igual a mayor al total indicado para aplicar meses sin intereses, aparecerá la ventana donde sugiere aplicarlo, si da clic en SI se mostrará la siguiente ventana en donde debe seleccionar la facilidad de pago deseada.

![IMG](10.png)

![IMG](11.png)

![IMG](12.png)

Si NO se seleccionó la opción de incremento de precios, las parcialidades de pagos se reflejarán de la siguiente manera: 
