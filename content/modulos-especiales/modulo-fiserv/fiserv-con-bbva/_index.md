+++
title = "Módulo FISERV con BBVA"
description = ""
weight = 2
+++

En la siguiente documentación se detallan los pasos para activar el módulo BBVA con su sistema SAIT.

<h3 class="text-primary">Requerimientos</h3>

- Sistemas Operativos Actualizados Windows. Versión 10 en adelante, PRO o HOME (64 bits)
- No Windows modificados, alterados, etc
- Contar con todas las actualizaciones de Windows a la fecha
- Memoria RAM mínimo de 4GB
- 1 Puerto USB
- Contar con una versión SAIT 2022 en adelante
- Funcionalidad disponible para módulo de Caja

<h3 class="text-primary">Configuración Previa</h3>

1. Instalar Drivers terminal [descargar]( https://sait.mx/download/terminal/PAX%20SP30.zip)

	Al finalizar ajustar el puerto del dispositivo debe quedar como COM9 ![IMG](comm.png)

2. Instalar Dll De comunicación [descargar](https://sait.mx/download/terminal/sait-bbvabridge-instalador.exe)

3. Descargar Formatos Vouchers [descargar](https://sait.mx/download/terminal/formatosbbva.zip) descargue el contenido y colóquelo en el directorio de la empresa

4. Agregar parámetro FISERV en el archivo CONFIG.MSL

	- Abrir SAIT y dar clic en [Catálogo de Empresas] ![IMG](1.png)

	- Copiar ruta del Directorio (lo que aparece en azul) ![IMG](2.png)

	- Pegar la ruta copiada en el explorador ![IMG](RUTA.png)

	- Ubicar archivo Config.msl y abrilo con un bloc de notas ![IMG](3.png)

	- Agregar el siguiente parámetro ![IMG](4.png)

	- Ir a Archivo / Guardar ![IMG](5.png)

5. Configurar conceptos de tarjetas en Caja / Tipos de Movimientos

	- Seleccionamos algún concepto de tarjeta (TB o TC) y Damos clic en Modificar y notaremos, que se agregaron nuevos elementos al final de la ventana

	- Seleccionaremos el Proveedor de Pago, para ambiente de pruebas usaremos el Simulador ![IMG](bbva.png)

6. Funcionamiento

	- Al procesar una venta con tarjeta, aparecerá la nueva ventana en donde el usuario podrá colocar el importe a cobrar ![IMG](bbva-ejemplo.png)

	Cuando el importe a cobrar sea igual al que de la nota, si se cobra correctamente el pago en la terminal, se procesará la nota en automático, pero si el importe a cobrar es menor al de la nota, se hará el cobro en la terminal y se regresará a la ventana de Caja Pagar para agregar más formas de pago 

7. Configuración

	La configuración de la terminal se encuentra en la configuración general del sistema ![IMG](config-general.png)

	Los parámetros de configuración serán proporcionados por el banco

	El campo de Comercio Terminal y Licencia son obligatorios, estos serán proporcionados por SAIT

	 ![IMG](configuracion-terminal.png)

















