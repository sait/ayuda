+++
title = "Módulo CONAGRO"
description = ""
weight = 13
+++

El módulo de CONAGRO es una herramienta que puede ser de mucha utilidad si usted se dedica al giro de venta de agroquímicos y tiene contrato con Syngenta, este módulo le permite cumplir con el requerimiento de Syngenta el cual consiste en enviar información de Ventas e Inventarios directamente desde su sistema SAIT a los servidores de Syngenta

<h3 class="text-primary">BENEFICIOS</h3>

![IMG](beneficios.png)

<h3 class="text-primary">FLUJO DE INFORMACIÓN</h3>

![IMG](flujo-datos.png)

<h3 class="text-primary">PROCESO EN SAIT</h3>

### Ventana envío de información de ventas (sellout)

- Proceso a realizar diariamente
- Sólo se tomarán en cuenta los artículos que estén clasificados con el proveedor que el usuario indique
- Sólo se tomará en cuenta el documento tipo remisión
- Si remisiona y después enlaza la remisión a factura, sólo se considerará el documento remisión
- Los registros en negativo y en rojo hacen referencia a devoluciones y cancelaciones
- El usuario deberá seleccionar todos los registros y dar clic en Enviar y posteriormente asegurarse que estén marcados como Enviado
- De no ser exitoso el envío aparecerá con estatus Pendiente, al seleccionar todos, la ventana sólo seleccionará los registros Pendiente para volver a dar clic en Enviar

![IMG](1.png)

![IMG](2.png)

### Ventana envío de información de inventarios (stock)

- Proceso a realizar una vez al mes
- Sólo se tomarán en cuenta los artículos que estén clasificados con el proveedor que el usuario indique
- La existencia se cargará al día actual cuando se haga la consulta
- El usuario deberá seleccionar todos los registros y dar clic en Enviar y posteriormente asegurarse que estén marcados como Enviado
- De no ser exitoso el envío aparecerá con estatus Pendiente, al seleccionar todos, la ventana sólo seleccionará los registros Pendiente para volver a dar clic en Enviar

![IMG](3.png)