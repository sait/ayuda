+++
title = "Módulo Importador Nadro"
description = ""
weight = 6
+++

Este módulo trabaja en conjunto con SAIT para importar desde Nadro las últimas actualizaciones del catálogo de artículos, precio público y último costo con información de Nadro.

1.Deberá contar con un archivo AUTOIIC.DAT, el cual contiene la base de datos de Nadro y ellos lo proporcionan en su página de internet. 

El archivo debe de estar grabado directamente en la unidad C:

![img](archivo-c.JPG)

2.Una vez colocada la aplicación dentro de la carpeta de la empresa se activarán las siguientes opciones en el menú de Compras:

![img](modulo.png)

* **Importar artículos desde Nadro:** Agrega nuevos artículos, actualiza precio público

Esta opción sirve para agregar NUEVOS artículos de Nadro a su catálogo de artículos SAIT de manera masiva, si el sistema detecta que algún artículos ya existía actualizará el precio público, si no existe, lo dará de alta.

![img](1.JPG)

* **Actualizar costo desde Nadro:** Actualiza el último costo de los artículos

![img](2.JPG)

Ejemplo de actualización de último costo con importador:

![img](actualizar-ult-costo.png)

* **Actualizar último costo con precio público desde Nadro:** Actualiza el último costo con el precio público de acuerdo a la información del archivo AUTOIIC.DAT

![img](3.JPG)

![img](4.JPG)

Podrá ver en pantalla una barra de estado que irá avanzando conforme se vayan actualizando los precios públicos

![img](proceso.png)

En caso de que en el archivo venga algún artículo que no exista se mostrará el siguiente mensaje:

![img](no-existe.JPG)

