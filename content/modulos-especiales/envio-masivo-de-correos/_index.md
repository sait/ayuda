+++
title = "Envio masivo de correos"
description = ""
weight = 15
+++

El siguiente módulo especial permite enviar correos masivos a su catálogo de clientes, puede adjuntar imágenes o documentos en general

<h3 class="text-primary"> Consideraciones técnicas </h3>

- Al instalar el módulo deberá asegurarse nadie esté usando el sistema ya que se crearán campos adicionales en las tablas

- El correo se enviará por medio del usuario que esté logueado en sistema, por lo que deberá asegurarse que el correo que tiene el usuario en  Utilerías / Catálogo de usuarios, esté funcionando de manera correcta

<h3 class="text-primary"> Configuración </h3>

- Asegúrese de que en el catálogo de clientes se encuentre capturado al menos un correo (el módulo funciona con el correo principal de la pestaña Datos Generales, no con el de la pestaña de Crédito)

![IMG](correo-cat.png)

- Diríjase al menú de Ventas / Envío de Correo Masivos

![IMG](1.png)

- LLene los campos con el que se formará el correo, para agregar un archivo deberá dar clic en el botón de Archivo y se abrirá la ventana donde podrá seleccionar el o los archivos a adjuntar en el correo

![IMG](2.png)

- Una vez agregados se verán en esta sección, si es más de uno, se verán separados por comas

![IMG](3.png)

- Si dejamos el cliente inicial y final tal y como aparecen, se enviará el correo a todo mi catálogo de clientes, puede optar por mandar a una clasificación o zona de los clientes para solo enviarlo a un grupo de clientes y no todops

![IMG](4.png)

- Después de dar clic en Enviar Emai verá la barra de progreso de envío

![IMG](5.png)

- El correo llegará como se muestra en la siguiente imagen

![IMG](6.png)


