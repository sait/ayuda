+++
title = "Módulo de Obras"
description = ""
weight = 9
+++

El modulo de obras permite cotizar de manera correcta las obras a los clientes permitiendo comparar el material y mano de obra cotizada contra el material y mano de obra usada, además de saber la ganancia total de la obra al comparar el total facturado contra el material y mano de obra usada. El funcionamiento en general del modulo se puede definir en los siguientes puntos:

1. Agregar la obra al catalogo de obras. El status de la obra quedara como “Cotizado”.
2. Cotizar el material y mano de obra que se estima se va a utilizar.
3. En caso de que el cliente autorice la cotización, el status de obra deberá cambiarse a “Abierta”.
4. Procesar remisiones del material utilizado.
5. Capturar la mano de obra de cada trabajador que participa de la obra.
6. Registrar las facturas de la obra.
7. Una vez que se haya terminado la obra, deberá cerrarse.
8. Cuando una obra ha sido cerrada, no se podrá seguir registrando remisiones, mano de obra y facturas relacionadas con la obra.
9. En caso de ser necesario, se puede reabrir la obra para poder registrar remisiones, mano de obra y facturas. 


<h2 class="text-primary">TEMARIO</h2>

* [1) Capturar Trabajadores](#capturar-trabajadores)
* [2) Capturar servicios de Mano de Obra](#capturar-servicios-de-mano-de-obras)
* [3) Consulta General de Obras](#consulta-general-de-obras)
* [4) Agregar, Modificar y Eliminar Obra](#agregar-modificar-eliminar-obra)
* [5) Consulta Individual de Mano de Obra](#consulta-individual-de-mano-de-obra)
* [6) Remisionar Compra](#remisionar-compra)
* [7) Registrar Cotizaciones, Remisiones y Facturas de una obra](#registrar-cotizaciones-remisiones-facturas-de-una-obra)
* [8) Cerrar una Obra](#cerrar-una-obra)
* [9) Reabrir una Obra](#reabrir-una-obra)
* [10) Configuración](#configuracion)


## CAPTURAR TRABAJADORES

Permite capturar a los trabajadores de las obras para permitir la captura de la mano de obra.

1.Entre al menú de Ventas / Vendedores

2.Clic en [Agregar].

3.Capturar la clave y nombre del trabajador.

* Especificar el tipo de mano de obra. Ejemplo: Ingeniero, Técnico, Obrero, Etc.

* Escribir el costo por hora trabajada normal, doble y triple.

* Hacer clic en [Agregar]. 

![img](1.png)

## CAPTURAR SERVICIOS DE MANO OBRAS

Por cada tipo de mano de obra capturada, se deberá agregar un servicio al catalogo de artículos que haga referencia a cada tipo, con el fin de facilitar la cotización de la mano de obra y poder comparar la mano de obra cotizada y la que realmente se invirtió en cada obra. 

1.Entre al menú de Inventario / Catálogo de Artículos.

2.En la clave del artículo se deberá anteponer el prefijo MO_ seguido del tipo de mano de obra. 

Ejemplo: MO-INGENIERO, MO_TECNICO, MO_OBRERO, etc.

* Especificar la descripción de la mano de obra.

* Habilitar la casilla - Es un Servicio.

* Hacer clic en [Grabar]. 

![img](2.jpg)


## CONSULTA GENERAL DE OBRAS

Permite consultar las obras en las que se esta trabajando actualmente o aquellas que fueron terminadas. Además, permite agregar, modificar y eliminar obras.

1. Entre al menú de Obras / Consulta General de Obras y de clic en Consultar, se muestran todas las obras cuyo status es: Abierta, también se puede restringir la consulta por cliente, rango de fechas en que se inicio y el status, al hacer doble clic o presionar la tecla [Enter] se puede consultar individualmente cada obra. 

![img](3.jpg)

## AGREGAR MODIFICAR ELIMINAR OBRA

Permite agregar una nueva obra, modificar o eliminar una obra ya existente.

### Agregar

1. Entre al menú de Obras / Consulta General de Obras.

2. Se muestran todas las obras cuyo status es: Abierta.

3. Hacer clic en [Agregar].
  
    El número de obra es un consecutivo que el sistema proporciona a partir de la última obra que se haya agregado.
    
    Especificar el status:
    
    **Abierta**: es una obra que el cliente ya autorizo y se esta trabajando en ella.
    
    **Cotizada**: se refiere cuando se esta cotizando la obra y esta en proceso de negociación con el cliente. Aun no esta autorizada.
    
    **Terminada**: es una obra que el cliente autorizo ya se termino de trabajar en ella.
    
    **Cancelada**: es una obra que el cliente no autorizo. 

    Capturar el % de ganancia que se desea obtener.

    Escribir la clave del cliente. En caso de tratarse del cliente 0 se pueden capturar sus datos. (nombre,dirección, teléfono).

    Especificar la descripción de la obra.

    Escribir los datos de los encargados de la obra por parte del cliente y por parte de nosotros.

    Capturar la fecha en que se inicio la obra.

    Se puede especificar el porcentaje de avance y algún comentario.

    La información de las cotizaciones, material usado (remisiones) y facturas se muestran automáticamente después de procesar alguno de estos documentos de venta y que esten relacionados con la obra.

    La mano de obra se muestra automáticamente después de haber capturado la mano de obra relacionada. 

![img](4.jpg)

### Modificar

1. Entre al menú de Obras / Consulta General de Obras.

2. Se muestran todas las obras cuyo status es: Abierta.

3. Seleccionar la obra.

4. Hacer clic en [Modificar].

5. Hacer las modificaciones necesarias. No se puede modificar el cliente si la obra ha tenido remisiones.

6. Clic en [Modificar]. 

### Eliminar

1. Entre al menú de Obras / Consulta General de Obras.

2. Se muestran todas las obras cuyo status es: Abierta.

3. Seleccionar la obra.

4. Hacer clic en [Eliminar].

5. Verificar que se trate de la obra correcta. No se puede eliminar si la obra ha tenido remisiones.

6. Hacer clic en [Eliminar]. 

## CONSULTA INVIDIDUAL DE MANO DE OBRA

Permite consultar por cada trabajador el total de mano de obra invertida en una sola obra.

1. Entre al menú de Obras / Consulta General de Obras.

2. Se muestran todas las obras cuyo status es: Abierta.

3. Seleccionar la obra.

4. Hacer clic en [Modificar].

5. Localizar la sección “Mano de Obra”.

6. Hacer doble clic en el nombre del trabajador.

7. Se presenta la ventana de Consulta Individual de Mano de Obra. Se puede observar el total de horas normales, dobles y triples trabajadas por cada dia y en total. La consulta puede ser enviada a Excel. 

![img](5.jpg)


## CAPTURA DE MANO DE OBRA

Permite agregar, modificar y eliminar la mano de obra de cada trabajador involucrada en cada obra. 

![img](6.jpg)


### Agregar

1. Entre al menú de Obras / Captura de Mano de Obra.

	Hacer clic en [Agregar].

    Escribir la fecha en que laboro el trabajador.

    Especificar el número de obra.

    Capturar la clave del trabajador. Se pueden realizar búsquedas al presionar la tecla [F2].

    Capturar el total de horas trabajadas normales, dobles y triples.

    Hacer clic en [Agregar]. 

    ![img](7.jpg)

### Modificar

1. Entre al menú de Obras / Captura de Mano de Obra.

	Localizar y seleccionar la mano de obra que se desea modificar.
     
    Hacer clic en [Modificar].

    Verificar que sea la mano de obra correcta. No se pueden hacer modificaciones si la obra a la que pertenece esta cerrada.
    
    Hacer clic en [Modificar]. 

    ![img](8.jpg)

### Eliminar

1. Entre al menú de Obras / Captura de Mano de Obra.
    
    Localizar y seleccionar la mano de obra que se desea eliminar.
    
    Hacer clic en [Eliminar].

    Verificar que sea la mano de obra correcta. No se pueden hacer modificaciones si la obra a la que pertenece esta cerrada.
    
    Hacer clic en [Modificar]. 

    ![img](9.jpg)

## REMISIONAR COMPRA

Genera una remisión a partir de una compra en caso de que una compra vaya dirigida en su totalidad a una obra en específico.


1. Entrar al menú de Compras / Remisionar Compra.
	
	Especificar el número de la compra.
    
    Se muestra la fecha actual. En caso de ser necesario se puede cambiar.
    
    Escribir la clave del cliente. Se pueden realizar búsquedas al presionar la tecla [F2].
    
    Capturar el número de la obra.
    
    Seleccionar la forma de pago: Contado, Crédito, eso indicará si se va a generar la remisión de contado o crédito
    
    Presionar la tecla [F8] para procesar la remisión. 

    ![img](10.jpg)

## REGISTRAR COTIZACIONES REMISIONES FACTURAS DE UNA OBRA

Después de haber agregado la obra al catalogo de obras, se podrán registrar cotizaciones, remisiones y facturas relacionadas con ella para poder realizar su análisis una vez que se haya cerrado.

1. Entre al menú de Ventas / Registro de Ventas

    Seleccionar el tipo de documento: Cotización, Factura, Remisión
    
    Capturar la clave del cliente.
    
    En el campo # Orden de Obra especificar el numero de obra a la que pertenece el 
    documento. Ejemplo: 1, 2, 3, etc.
    
    En caso de tratarse de una cotización y se vaya a incluir la mano de obra, se deberá capturar el servicio correspondiente según el tipo de mano de obra. Ejemplo: MO_INGENIERO para cotizar mano de obra de ingenieros, MO_TECNICO para cotizar mano de obra de técnicos, etc.
    
    Terminar de procesar el documento. 

    ![img](11.jpg) 

## CERRAR UNA OBRA

Una vez que se termino la obra, se debe cerrar para validar que no se puedan seguir registrando cotizaciones, remisiones, facturas y mano de obra relacionadas con ella.

1. Entre al menú de Obras / Consulta General de Obras.

    Se muestran todas las obras cuyo status es: Abierta.
    
    Seleccionar la obra que se desea cerrar.
    
    Hacer clic en [Modificar].
    
    Verificar que se trate de la obra correcta.
    
    Hacer clic en [Cerrar Obra]
    
    Aparecerá la siguiente ventana:

    ![img](12.jpg) 

    Capturar la fecha en que se terminó.

    ![img](13.jpg) 

    El sistema confirma si esta seguro de cerrarla. Hacer clic en [Si]
    
    La obra ha sido cerrada y no se podrán seguir registrando documentos de venta y mano de obra. 

## REABRIR UNA OBRA

En caso de que se haya cerrar por error una obra o se haya omitido registrar algún documento de venta o mano de obra, se puede reabrir.

1. Entrar al menú de Obras / Consulta General de Obras.
    
    Consultar las obras cuyo status es: Terminada.
    
    Seleccionar la obra que se desea reabrir.
    
    Hacer clic en [Modificar].

    ![img](14.jpg) 
  
    Verificar que se trate de la obra correcta.

    Hacer clic en [ReAbrir Obra]

    ![img](15.jpg) 
    
    El sistema confirma si desea abrirla. Hacer clic en [Si]
   
    La obra ha sido reabierta y esta lista para que se pueda seguir registrando documentos de 
    venta y mano de obra. 

## CONFIGURACION

Permite configurar los niveles de acceso a las diferentes opciones del modulo.

1. Entrar al menú de Obras /  Configurar.
    
    Escribir la clave de los niveles que tendrán acceso a cada opción del modulo, separados por coma(,). Ejemplo SUPER,ALM,1,5,8
    
    Hacer clic en [Grabar]. 

    ![img](16.jpg) 