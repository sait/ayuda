+++
title = "Envio masivo de estado de cuenta programable"
description = ""
weight = 15
+++

El siguiente módulo especial permite enviar automáticamente el estado de cuenta de las facturas pendientes a sus clientes en el día de la semana que usted seleccione


<h3 class="text-primary"> Consideraciones técnicas </h3>

- Al instalar el módulo deberá asegurarse nadie esté usando el sistema ya que se crearán campos adicionales en las tablas

- El correo se enviará por medio del OCF por lo que la empresa debe tenerlo configurado, en el siguiente link  podrá ver la documentación [ver](/sait-erp/2-instalacion-de-oganizador-de-comprobantes-fiscales/a-ejecutar-instalador-y-actualizar-version/)

- El correo se enviará al momento que el primer usuario entre al sistema, si nadie entra a SAIT el correo NO se enviará

<h3 class="text-primary"> Configuración </h3>

- Asegúrese de que en el catálogo de clientes se encuentre capturado al menos un correo 

![IMG](correo-cliente.png)

- Configurar OCF para el envío automático

![IMG](ocf.png)

- Para activar el envío deberá ir a Cobranza / Configurar Envío de Estado de Cuenta

![IMG](menu.png)

- Aparecerá la siguiente ventana y deberá llenar los datos

	- **Enviar el día:** día de la semana en que se enviará

	- **Mensaje:** mensaje que llegará al correo junto al estado de cuenta

	- **Enviar a clientes con clasificación:** esta parte es opcional y se puede utilizar si solo desea enviar el correo con el estado de cuenta a ciertos clientes, para ver la documentación de clasificaciones de los clientes de clic [aquí](/sait-erp/10-capacitacion-modulo-de-ventas/a-catalogo-clientes/#definir-zonas-y-clasificacion) 

![IMG](1.png)

El correo llegará de la siguiente forma

![IMG](reporte.png)