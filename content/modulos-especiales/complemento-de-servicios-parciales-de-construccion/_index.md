+++
title = "Complemento de Servicios Parciales de Construcción"
description = ""
weight = 11
+++

Este complemento será utilizado por el prestador de servicios parciales de construcción de inmuebles destinados a casa habitación, de conformidad con el “Decreto por el que se otorgan medidas de apoyo a la vivienda y otras medidas fiscales”, publicado en el Diario Oficial de la Federación el 26 de marzo de 2015 (incisos a) y b), de la fracción II, del artículo 2 del citado Decreto).
 
Este complemento permite incorporar a una factura los siguientes datos:


* Número de permiso, licencia o autorización de construcción proporcionado por el prestatario de los servicios parciales de construcción.

* Información del inmueble en el que se proporcionan los servicios parciales de construcción.

### PASO 1: Definir formato al cliente

1.Entrar a Utilerías / Modificar Formatos / Catálogo de CFDIS con Addendas y Formatos pdf

![img](1.jpg)

2.Clic en Agregar, aparecerá la siguiente ventana y deberá agregar el número de cliente, el comentario para identificar el complemento, y después dará clic en el botón de interrogación para cargar el formato

![img](2.jpg)

3.Ubicamos el formato fto.servparc.xml, lo seleccionamos y damos clic en Aceptar

![img](3.jpg)

4.Clic en Aceptar y listo

![img](4.jpg)

### PASO 2: Agregar Inmueble

1.Ir al menú de Ventas / Catálogo de Inmuebles

![img](5.jpg)

2.Clic en Agregar y llenamos los datos correspondientes al inmuble, clic en Agregar

![img](6.jpg)

### PASO 3: Realizar factura

1.Dirigirse a Ventas /  Registrar Venta

2.Seleccionar el cliente al que se agregó el complemento e indicar el No.Permiso del Inmueble, agregar el o los artículos y procesar la factura de manera habitual

![img](7.jpg)

Listo, si consultamos el XML, notaremos se agregó el complemento servicioparcial:parcialesconstruccion.

![img](8.jpg)

### Variables a agregar para que formato pdf muestre la información del inmueble

Si desea que en el formato pdf se muestren los datos correspondientes al módulo deberá agregar las siguientes variables:

```
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.NumPerLicoAut')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.Calle')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.NoExterior')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.NoInterior')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.Colonia')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.Localidad')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.Municipio')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.Estado')
eval2('oCfdi.Comprobante.Complemento.ParcialesConstruccion.Inmueble.CodigoPostal')
```

Para mostrar el nombre de del estado se necesita la siguiente función

[clic aquí para descargar FXP](obtnomestado.FXP) 

[clic aquí para descargar PRG](obtNomEstado.PRG) 

Una vez descargados los archivos deberá agregarlos dentro de la carpeta de la CIA.

Formato Tamaño Carta Servicios Parciales[clic aquí para descargar](formato-normal-tamaño-carta-servicios-parciales.rpt)

![IMG](EJEMPLO.png)
