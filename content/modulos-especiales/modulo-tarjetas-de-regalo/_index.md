+++
title = "Módulo Tarjetas de Regalo (Giftcards)"
description = ""
weight = 3
+++

Este módulo maneja el concepto de tarjetas de regalo **Gift Cards**, mediante el cual el cliente pueda comprar una tarjeta precargada con dinero y regalarla a algún familiar o conocido. Dicho módulo está preparado para poder facturar con el esquema de facturación CFDI 4.0

También permite realizar recargas de dinero en caso de que desee conservar la tarjeta.

<h3 class="text-primary">Consideraciones previa</h3>

1.El saldo de las tarjetas se llevará en pesos.

2.Funciona para la venta en el menú de Caja / Registrar Ventas

3.Si un cliente frecuente compra una tarjeta de regalo, acumulará puntos.

4.Si un cliente pierde una tarjeta, perderá su dinero.

5.Las tarjetas se podrán reusar. Si un cliente se agota su tarjeta el cajero le pregunta que la puede recargar o la puede regresar. Si el cliente la regresa, entonces esa misma tarjeta se podrá volver a activar para otro cliente.

6.Al abonar a tarjeta, o al utilizarla, asegurarse que sea leída con el scanner.

7.Se deben agregar los siguientes artículos estén dados de alta y asegúrese que sean marcados como servicios: 

* **TR** - TARJETAS DE REGALO
* **USOTR** - PAGO CON TARJETA DE REGALO
* **RECTR** - RECARGAR TARJETA DE REGALO
* **DEVTR** - DEVOLUCION EFECTIVO TARJETA DE REGALO 

8.También es importante mostrar la columna de descuento2 ya que ahí es donde se verá reflejado el descuento al pagar con la Gift Card, para realizar este proceso deberá ir al menú de Utilerías / Configuración General del Sistema / Ventas2

![1](descuento2.png)

9.Si usted utiliza cortes de caja, deberá actualizar los siguientes formatos ya los formatos default no consideran el descuento2 

[corte-caja-02a-(ventas-por-linea).rpt](corte-caja-02a-(ventas-por-linea).rpt)

[corte-caja-02b-(resumen-de-ventas-2014).rpt](corte-caja-02b-(resumen-de-ventas-2014).rpt)

Descargue ambos reportes y recíbalos en Utilerías / Recibir Reportes

Importante borrar ambos reportes desde Utilerías / Modificar Reportes / Cortes de Caja para que aparezcan duplicados

<h3 class="text-primary">Menú</h3>

* [Vender Tarjeta](#vender-tarjeta)
* [Pagar con Tarjeta](#pagar-con-tarjeta)
* [Abonar Dinero a Tarjeta](#abonar-dinero-a-tarjeta)
* [Devolver Dinero de la Tarjeta](#devolver-dinero-de-la-tarjeta)
* [Consultar Saldo y Movimientos de una Tarjeta](#consultar-saldo-y-movimientos-de-una-tarjeta)

### Vender Tarjeta

Entrar al menú de Caja / Registrar Ventas
   
Teclear la clave: TR (Tarjeta de Regalo)
   
Aparece la ventana de Opciones de Tarjeta de Regalo

Hacer clic en **[Vender Tarjeta]**

![1](opciones-tarjeta.PNG)
  
Se presenta la ventana "Vender con Tarjeta de Regalo"
   
Se debe escanear el código de la tarjeta
   
Especificar el importe que se le va a cargar

![2](vender-tarjeta.PNG)
   
Hacer clic en **[Aceptar]**
  
Procesar la venta 

![3](vender-pagar-tarjeta.PNG)

### Pagar con Tarjeta

Entrar al menú de Caja / Registrar Ventas

Teclear la clave: TR (Tarjeta de Regalo)

Aparece la ventana de Opciones de Tarjeta de Regalo

Hacer clic en **[Pagar con Tarjeta]**

Se presenta la ventana "Pagar con Tarjeta de Regalo"

![3](pagar-con-tarjeta.PNG)

Automáticamente aparece el dinero a usar de la tarjeta de regalo, como podemos observar el importe a usar se traduce a un porcentaje de descuento equivalente.

Hacer clic en **[Aceptar]**
    
Se regresa a la ventana de Registro de Ventas, con la nota bloqueada (similar a como se hace con los puntos)
    
Procesar la venta 

![img](pagar-con-tarjeta-descuento.PNG)

La nota de venta se ve de la siguiente manera:

![img](nota-de-venta.png)

La facturación de dicha nota de venta se ve de la siguiente manera:

![img](factura-de-nota.PNG)

### Abonar Dinero a Tarjeta

Entrar al menú de Caja / Registrar Ventas

Teclear la clave: TR (Tarjeta de Regalo)

Aparece la ventana de Opciones de Tarjeta de Regalo

Hacer clic en **[Recargar Tarjeta]**

![img](opciones-tarjeta-recargar.PNG)

Se presenta la ventana "Recargar Tarjeta de Regalo"

Se debe escanear el código de la tarjeta

Automática aparece el saldo con el que cuenta

Especificar el importe que se le va a recargar

Hacer clic en **[Aceptar]**

![img](recargar-tarjeta-1.PNG)

Procesar la venta 

![img](recargar-tarjeta-2.PNG)

### Devolver Dinero de la Tarjeta

Entrar al menú de Caja / Registrar Ventas

Teclear la clave: TR (Tarjeta de Regalo)

Aparece la ventana de Opciones de Tarjeta de Regalo

Hacer clic en **[Devolver Dinero]**

![img](opciones-tarjeta-devolver.PNG)

Se presenta la ventana "Devolver Tarjeta de Regalo"

Se debe escanear el código de la tarjeta

Automática aparece el saldo con el que cuenta

Se debe capturar la identificación de la persona que solicita la devolución del dinero así como el motivo por el cual lo solicita

Hacer clic en **[Aceptar]**

![img](devolver-dinero-opciones-id.png)
    

Deberá marcar la opción de Devolver Efectivo 

Y finalmente solo falta procesar la devolución del efectivo 

![img](devolver-dinero-last.PNG)

### Consultar Saldo y Movimientos de una Tarjeta

Entrar al menú de Caja / Consultar Saldo de Tarjeta de Regalo

Se debe escanear el código de la tarjeta

![img](consultar-movimientos.PNG)
    
Automática aparecen todos los movimientos realizados con la tarjeta 

